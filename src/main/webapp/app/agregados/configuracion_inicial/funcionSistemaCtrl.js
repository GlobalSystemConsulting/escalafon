app.controller("funcionSistemaCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    $scope.subModulos = [];
    $scope.nuevaFuncion = {nombre:"",descripcion:"",url:"",clave:"",controlador:"",interfaz:"",icono:"",estado:'A',subModuloID:0};
    $scope.funcionSel = {};
    
    var paramsFuncion = {count: 10};
    var settingFuncion = { counts: []};
    $scope.tablaFuncion = new NgTableParams(paramsFuncion, settingFuncion);
    
    $scope.listarFunciones = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('funcionSistema',1,'listarFunciones');
        crud.listar("/configuracionInicial",request,function(data){
            settingFuncion.dataset = data.data;
            iniciarPosiciones(settingFuncion.dataset);
            $scope.tablaFuncion.settings(settingFuncion);
        },function(data){
            console.info(data);
        });
    };
    $scope.agregarFuncion = function(){
        
        var request = crud.crearRequest('funcionSistema',1,'insertarFuncion');
        request.setData($scope.nuevaFuncion);
        
        crud.insertar("/configuracionInicial",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.nuevaFuncion.funcionID = response.data.funcionID;
                $scope.nuevaFuncion.subModuloID = Number($scope.nuevaFuncion.subModuloID);
                $scope.nuevaFuncion.fecha = response.data.fecha;
                
                //insertamos el elemento a la lista
                insertarElemento(settingFuncion.dataset,$scope.nuevaFuncion);
                $scope.tablaFuncion.reload();
                //reiniciamos las variables
                $scope.nuevaFuncion = {nombre:"",descripcion:"",url:"",clave:"",controlador:"",interfaz:"",icono:"",estado:'A',subModuloID:0};
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarFuncion = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){
            
            var request = crud.crearRequest('funcionSistema',1,'eliminarFuncion');
            request.setData({funcionID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingFuncion.dataset,i);
                    $scope.tablaFuncion.reload();
                }

            },function(data){
                console.info(data);
            });
            
        });
        
        
        
    };
    $scope.prepararEditar = function(t){
        $scope.funcionSel = JSON.parse(JSON.stringify(t));
        $('#modalEditar').modal('show');
    };
    $scope.editarFuncion = function(){
        
        var request = crud.crearRequest('funcionSistema',1,'actualizarFuncion');
        request.setData($scope.funcionSel);
                
        crud.actualizar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                
                //actualizando
                settingFuncion.dataset[$scope.funcionSel.i] = $scope.funcionSel;
                $scope.tablaFuncion.reload();
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.eliminarModulos = function(){
            
        
    };
    
    listarSubModulos();
    
    function listarSubModulos(){
        //preparamos un objeto request
        var request = crud.crearRequest('subModuloSistema',1,'listarSubModulos');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.subModulos = data.data;
        },function(data){
            console.info(data);
        });
    }; 
    
    
}]);