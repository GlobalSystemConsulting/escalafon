app.controller("catalogoCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
                
    $scope.catalogo = {nombretabla:"",nombreclase:"",estado:'A'};
    $scope.registro = {Nombre:"",Descripcion:"",estado:'A'};
    $scope.catalogoSel = {};
    
    $scope.resgitroSel = {};
    $scope.catalogoBusqueda;
    
    
    $scope.contenidoTabla = {id:null,nombre:"",descripcion:""};
    $scope.catalogos = [];
    
    $scope.resgistros = [];
    $scope.resgitroSel;
    $scope.nombretabla;
    $scope.nombreclase;
    
    var params = {count: 10};
    var setting = { counts: []};    
    $scope.miTabla = new NgTableParams(params, setting);
    
    var paramsMapeoCatalogo = {count: 10};
    var settingMapeoCatalogo = { counts: []};    
    $scope.miTablaMapeoCatalogo = new NgTableParams(paramsMapeoCatalogo, settingMapeoCatalogo);

    
    $scope.listarCatalogos= function(){
        //preparamos un objeto request
        var request = crud.crearRequest('catalogo',1,'listarCatalogo');
        crud.listar("/configuracionInicial",request,function(data){
            $scope.catalogos = data.data;
            settingMapeoCatalogo.dataset = data.data;                
            iniciarPosiciones(settingMapeoCatalogo.dataset);
            $scope.miTablaMapeoCatalogo.settings(settingMapeoCatalogo);
        },function(data){
            console.info(data);
        });
    };
    
    $scope.listarContenidoEntidad = function(id){
        
         $scope.nombretabla;
         $scope.nombreclase;
         var ida = parseInt(id)
         
         for (i = 0; i < $scope.catalogos.length; i++) {
                        
             if( $scope.catalogos[i].catalogoID === ida ){
                $scope.nombretabla = $scope.catalogos[i].nombretabla;
                $scope.nombreclase = $scope.catalogos[i].nombreclase;
                break;
             }
               
          }
         
        
        var request = crud.crearRequest('catalogo',1,'listarRegistros');
        request.setData({nombretabla:$scope.nombretabla,nombreclase:$scope.nombreclase});
        crud.listar("/configuracionInicial",request,function(data){
            $scope.resgistros = data.data;
            setting.dataset = data.data;                
            iniciarPosiciones(setting.dataset);
            $scope.miTabla.settings(setting);
            
            console.log( data.data)
        },function(data){
            console.info(data);
        });
    };
    
    $scope.agregarCatalogo= function(){
        
        var request = crud.crearRequest('catalogo',1,'insertarCatalogo');
        request.setData($scope.catalogo);
        
        crud.insertar("/configuracionInicial",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.catalogo.catalogoID = response.data.catalogoID;
                //insertamos el elemento a la lista
                $scope.catalogos.push($scope.catalogo);
                //reiniciamos las variables
                $scope.catalogo = {nombretabla:"",nombreclase:"",estado:'A'};
              
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarCatalogo = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el Catalogo",function(){
            
            var request = crud.crearRequest('catalogo',1,'eliminarCatalogo');
            request.setData({catalogoID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    $scope.catalogos.splice(i,1);
            },function(data){
                console.info(data);
            });
        });
    };
    $scope.prepararEditar = function(i,t){
        
        $scope.catalogoSel = JSON.parse(JSON.stringify(t));
        $scope.catalogoSel.i = i;
        $('#modalEditar').modal('show');
    };
    
       
    $scope.editarCatalogo = function(){
        
        var request = crud.crearRequest('catalogo',1,'actualizarCatalogo');
        request.setData($scope.catalogoSel);
                
        crud.actualizar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                $scope.catalogos[$scope.catalogoSel.i] = $scope.catalogoSel;
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
       
    $scope.agregarRegistro= function(){
        
        $scope.registro.nombretabla = $scope.nombretabla;
        $scope.registro.nombreclase = $scope.nombreclase;
        var request = crud.crearRequest('catalogo',1,'insertarEntidad');
        request.setData($scope.registro);
        
        crud.insertar("/configuracionInicial",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.registro.id = response.data.registroID;
                //insertamos el elemento a la lista
                $scope.resgistros.push($scope.registro);
                //reiniciamos las variables
                $scope.registro = {nombre:"",descripcion:"",estado:'A'};
                 $scope.miTabla.reload();
                //cerramos la ventana modal
                $('#modalNuevoRegistro').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    
    $scope.eliminarRegistro = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el Catalogo",function(){
            
            var request = crud.crearRequest('catalogo',1,'eliminarEntidad');
            request.setData({regsitroID:idDato, nombretabla:$scope.nombretabla ,nombreclase:$scope.nombreclase});

            crud.eliminar("/configuracionInicial",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    $scope.resgistros.splice(i,1);
                    $scope.miTabla.reload();
            },function(data){
                console.info(data);
            });
        });
    };
    $scope.prepararRegistro = function(i,t){
        
        $scope.resgitroSel = JSON.parse(JSON.stringify(t));
        console.log("JSON.parse(JSON.stringify(t))",JSON.parse(JSON.stringify(t)))
        $scope.resgitroSel.i = i;
        console.log($scope.resgitroSel)
        $('#modalEditarRegistro').modal('show');
    };
    $scope.editarRegistro = function(){
       
        $scope.resgitroSel.nombretabla = $scope.nombretabla;
         $scope.resgitroSel.nombreclase = $scope.nombreclase;
        var request = crud.crearRequest('catalogo',1,'actualizarEntidad');
        request.setData($scope.resgitroSel);
                
        crud.actualizar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                $scope.resgistros[$scope.resgitroSel.i] = $scope.resgitroSel;
                $scope.miTabla.reload();
                $('#modalEditarRegistro').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
}]);