app.controller("perfilUsuarioCtrl",["$scope","crud","modal", function ($scope,crud,modal){
    
    $scope.usuario = {};
    $scope.persona = {};
        
    $scope.buscarOrganizacion = function(usuID){
        //preparamos un objeto request
        var request = crud.crearRequest('usuarioSistema',1,'buscarPersona');
        request.setData({dni:"0",usuarioID:usuID});
        crud.listar("/configuracionInicial",request,function(res){
            $scope.persona = res.data.persona;
            $scope.usuario = res.data.usuario;
        },function(data){
            console.info(data);
        });
    };
    $scope.editarPassword = function(){
        
        if(!$scope.usuario.nuevoPassword || $scope.usuario.nuevoPassword=='' ){
                var request = crud.crearRequest('usuarioSistema',1,'actualizarPassword');
                    
                    request.setData({usuario:$scope.usuario, persona:$scope.persona});
                    $scope.usuario.nuevoPassword="---";
                    crud.actualizar("/configuracionInicial",request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                            //actualizando
                            $scope.usuario.nuevoPassword = "";
                            $scope.usuario.nuevoPassword2 = "";
                        }
                    },function(data){
                        console.info(data);
                    });
            return;
        }
        if(!$scope.usuario.nuevoPassword2 || $scope.usuario.nuevoPassword2=='' || $scope.usuario.nuevoPassword2!=$scope.usuario.nuevoPassword){
            modal.mensaje("ADVERTENCIA","el password nuevo no coincide");
            return;
        }
        
        modal.mensajeConfirmacion($scope,"Esta seguro que desea cambiar de contraseña!!",function(){
        
            var request = crud.crearRequest('usuarioSistema',1,'actualizarPassword');
            request.setData({usuario:$scope.usuario, persona:$scope.persona});

            crud.actualizar("/configuracionInicial",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    //actualizando
                    $scope.usuario.nuevoPassword = "";
                    $scope.usuario.nuevoPassword2 = "";
                }
            },function(data){
                console.info(data);
            });
        
        },"400");
    };
    
    $scope.cancelar = function(){
        //$scope.org = JSON.parse(JSON.stringify(organizacion));
    };
    
}]);