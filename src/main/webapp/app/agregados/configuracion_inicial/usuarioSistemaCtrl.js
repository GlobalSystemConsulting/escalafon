var app = angular.module('app');
app.requires.push('angularModalService');
app.requires.push('ngAnimate');

app.filter('startFromGrid', function() {
  return function(input, start) {
    start = +start;
    return input.slice(start);
  };
}).filter('capitalize', function () {
    return function (input, char) {
        if (isNaN(input)) {
            // If the input data is not a number, perform the operations to capitalize the correct letter.
            var char = char - 1 || 0;
            var letter = input.charAt(char).toUpperCase();
            var out = [];
            for (var i = 0; i < input.length; i++) {
                if (i == char) {
                    out.push(letter);
                } else {
                    out.push(input[i]);
                }
            }
            return out.join('');
        } else {
            return input;
        }
    };
});

app.controller("usuarioSistemaCtrl", ["$scope","$rootScope", "NgTableParams", "crud", "modal", function ($scope,$rootScope, NgTableParams, crud, modal) {
        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.pages = [];
        $scope.canTotUsu = 0;
        $scope.opcionesFiltro=false;
        $scope.f = {nombreUsuario:"",DNI:"",nombre:"",apePat:"",apeMat:"",orgNombre:"",rolNombre:"",estId:""};
        $scope.metaDataFiltro = {tabla:"",campo:"",data:""};
        
        $scope.confFiltros = [
            {nomFil:"nom", est:false, data: "", abrTab:"u"},
            {nomFil:"dni", est:false, data: "", abrTab:"p"},
            {nomFil:"apePat", est:false, data: "", abrTab:"p"},
            {nomFil:"apeMat", est:false, data: "", abrTab:"p"},
            {nomFil:"nom", est:false, data: "", abrTab:"p"},
            {nomFil:"nom", est:false, data: "", abrTab:"o"},
            {nomFil:"nom", est:false, data: "", abrTab:"r"},
            {nomFil:"estId", est:false, data: "", abrTab:"s"},
        ];
        
        $scope.filtrar = function(){  
            var cont = 0;
            $scope.confFiltros.forEach(function(item){
                if(item.data.length>0 ){
                    item.est = true;
                    cont ++;
                }else{
                    item.est = false;
                }
            });
            
            if (cont === 0){
                $scope.opcionesFiltro = false;
                $scope.listarUsuarios(10,($scope.currentPage-1)*10);
            }else{
                $scope.opcionesFiltro = true;
                $scope.listarUsuxFiltros(10,(($scope.currentPage-1)*10),$scope.confFiltros);
            } 
        };
        
        $scope.configPages = function() {
            $scope.pages.length = 0;
            var ini = $scope.currentPage - 4;
            var fin = $scope.currentPage + 5;
            if (ini < 1) {
                ini = 1;
                if (Math.ceil($scope.canTotUsu / $scope.pageSize) > 10)
                    fin = 10;
                else
                    fin = Math.ceil($scope.canTotUsu / $scope.pageSize);
            } else {
                if (ini >= Math.ceil($scope.canTotUsu / $scope.pageSize) - 10) {
                    ini = Math.ceil($scope.canTotUsu / $scope.pageSize) - 10;
                    fin = Math.ceil($scope.canTotUsu / $scope.pageSize);
              }
            }
            if (ini < 1) ini = 1;
            for (var i = ini; i <= fin; i++) {
                $scope.pages.push({no: i});
            }

            if ($scope.currentPage >= $scope.pages.length)
                $scope.currentPage = $scope.pages.length - 1;
        };

        $scope.setPage = function(index) {
            $scope.currentPage = index - 1;
            if(!$scope.opcionesFiltro){
                $scope.listarUsuarios(10,(index-1)*10); 
            }else{
                $scope.listarUsuxFiltros(10,((index-1)*10),$scope.confFiltros);
           }
        };
        
        
 
        $scope.filter_by = function(tabla, field,data) {
            $scope.metaDataFiltro.tabla = tabla;
            $scope.metaDataFiltro.campo = field;
            $scope.metaDataFiltro.data = data;
            if (data.length === 0){
                $scope.opcionesFiltro = false;
                $scope.listarUsuarios(10,($scope.currentPage-1)*10);
            }else{
                $scope.opcionesFiltro = true;
                $scope.listarUsuariosxFiltro(10,(($scope.currentPage-1)*10),tabla,field,data);
           }
        };
        
        $scope.mostrarDataPagAnt = function() {
            var cont = 0;
            $scope.confFiltros.forEach(function(item){
                if(item.data.length>0 ){
                    item.est = true;
                    cont ++;
                }else{
                    item.est = false;
                }
            });
            
            if (cont === 0){
                $scope.opcionesFiltro = false;
                $scope.listarUsuarios(10,($scope.currentPage)*10);
            }else{
                $scope.opcionesFiltro = true;
                $scope.listarUsuariosxFiltro(10,(($scope.currentPage)*10),$scope.confFiltros);
            } 
        };
        
        $scope.mostrarDataPagSig = function() {
            var cont = 0;
            $scope.confFiltros.forEach(function(item){
                if(item.data.length>0 ){
                    item.est = true;
                    cont ++;
                }else{
                    item.est = false;
                }
            });
            
            if (cont === 0){
                $scope.opcionesFiltro = false;
                $scope.listarUsuarios(10,($scope.currentPage)*10);
            }else{
                $scope.opcionesFiltro = true;
                $scope.listarUsuariosxFiltro(10,(($scope.currentPage)*10),$scope.confFiltros);
            } 
        };
        
        $scope.roles = [];
        $scope.organizaciones = [];
        $scope.tipDocs = ["Resolución decanal", "Resolución rectoral", "Resolución subdirectoral", "Resolución de consejo universitario",
            "Resolución directoral (DIGA)", "Memorándum", "Oficio", "Constancia", "Otros"];
        $scope.jorLabs = ["Completa", "Parcial", "Otros"];
        $scope.nuevoUsuario = {nombre: "", password: "", estado: 'A', rolID: 0, organizacionID: 0, existe: true};
        $scope.nuevaPersona = {dni: "", nombre: "", materno: "", paterno: "", nacimiento: "", email: "", numero1: "", numero2: "", existe: true};
        $scope.nuevoDespla = {ficEscId: "", tip: "1", numDoc: "", fecDoc: "", tipDoc: "", insEdu: "", car: "", jorLab: "", fecIni: "", fecTer: "", fecDocTer: "", numDocTer: "", motRet: ""};
        $scope.esTrabajador = 0;
        $scope.usuarioSel = {};
        $scope.sessionToUpdate = {};
        $scope.oSel = {};

        //tabla de usuarios
        var paramsUsuario = {count: 10};
        var settingUsuario = {counts: []};
        $scope.tablaUsuario = new NgTableParams(paramsUsuario, settingUsuario);

        //filtros
        $scope.estadosSelect = [{id: 'A', title: "activo"}, {id: 'I', title: "inactivo"}, {id: 'E', title: "eliminado"}];
        $scope.organizacionesSelect = [];
        $scope.rolesSelect = [];
        $scope.estadoFormNewRole = false;
        $scope.esTrabajadorNewRole = 0;
        $scope.estadoBotonNewRole = true;
        
        $scope.actualizarEstadoFormNewRole = function (){
            $scope.estadoFormNewRole = true;
            $scope.estadoBotonNewRole = false;
        };
        
        $scope.actualizarEstadoBotonNewRole = function (){
            $scope.estadoFormNewRole = false;
            $scope.estadoBotonNewRole = true;
        };
        
        $scope.actualizarEstadoBotonNewRole2 = function (){
            $scope.estadoFormNewRole = false;
            $scope.estadoBotonNewRole = true;
        };
        
        $scope.buscarPersona = function () {

            if ($scope.nuevaPersona.dni == '') {
                modal.mensaje("ALERTA", "ingrese DNI");
                return;
            }
            //preparamos un objeto request
            var request = crud.crearRequest('usuarioSistema', 1, 'buscarPersona');
            request.setData({dni: $scope.nuevaPersona.dni});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/configuracionInicial", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                if (data.responseSta) {
                    $scope.nuevaPersona = data.data.persona;
                    if (data.data.usuario)
                        $scope.nuevoUsuario = data.data.usuario;
                    else {
                        $scope.nuevoUsuario.nombre = $scope.nuevaPersona.dni;
                        $scope.nuevoUsuario.existe = false;
                    }
                } else {
                    $scope.nuevaPersona.existe = false;
                    /*$scope.nuevaPersona.nombre="";
                     $scope.nuevaPersona.paterno="";
                     $scope.nuevaPersona.materno="";
                     $scope.nuevaPersona.nacimiento="";
                     $scope.nuevaPersona.email="";
                     $scope.nuevaPersona.numero1="";
                     $scope.nuevaPersona.numero2="";  */
                    $scope.nuevoUsuario.nombre = $scope.nuevaPersona.dni;
                    $scope.nuevoUsuario.existe = false;
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.listarUsuarios = function (limitI,offsetI) {
            $rootScope.showLoading();
            console.log("Listado sin filtro");
            //preparamos un objeto request
            var request = crud.crearRequest('usuarioSistema', 1, 'listarUsuarios');
            request.setData({limit: limitI, offset: offsetI, opcion:0, nomTabla: "", nomCampo:"", dataCampo:""});
            crud.listar("/configuracionInicial", request, function (response) {
                if (response.responseSta)
                {
                    console.log(response);
                    settingUsuario.dataset = response.data.dataUsuario;
                    iniciarPosiciones(settingUsuario.dataset);
                    $scope.tablaUsuario.settings(settingUsuario);
                    $scope.tablaUsuario.reload();
                    $scope.canTotUsu = response.data.canTotReg;
                    $scope.configPages();
                    $rootScope.hideLoading();
                }
            }, function (response) {
                console.info(response);
            });
        };
        
        $scope.listarUsuariosxFiltro = function (limitI,offsetI,tablaI,campoI,dataI) {
            //preparamos un objeto request
            var request = crud.crearRequest('usuarioSistema', 1, 'listarUsuarios');
            request.setData({limit: limitI, offset: offsetI, opcion:1, nomTabla: tablaI, nomCampo:campoI, dataCampo:dataI});
            crud.listar("/configuracionInicial", request, function (response) {
                if (response.responseSta){
                    console.log(response.data);
                    settingUsuario.dataset = response.data.dataUsuario;
                    iniciarPosiciones(settingUsuario.dataset);
                    $scope.tablaUsuario.settings(settingUsuario);
                    $scope.tablaUsuario.reload();
                    $scope.canTotUsu = response.data.canTotReg;
                    $scope.configPages();
                }
            }, function (response) {
                console.info(response);
            });
        };
        
        $scope.listarUsuxFiltros = function (limitI,offsetI,confFiltros) {
            //preparamos un objeto request
            var request = crud.crearRequest('usuarioSistema', 1, 'listarUsuxFiltros');
            request.setData({limit: limitI, offset: offsetI, opcion:1, filtros:confFiltros});
            crud.listar("/configuracionInicial", request, function (response) {
                if (response.responseSta){
                    console.log(response.data);
                    settingUsuario.dataset = response.data.dataUsuario;
                    iniciarPosiciones(settingUsuario.dataset);
                    $scope.tablaUsuario.settings(settingUsuario);
                    $scope.tablaUsuario.reload();
                    $scope.canTotUsu = response.data.canTotReg;
                    $scope.configPages();
                }
            }, function (response) {
                console.info(response);
            });
        };
        
        $scope.changePage = function(numberPage) {
            $scope.currentPage = numberPage;
        };

        $scope.reiniciarDatos = function () {
            var p = $scope.nuevaPersona;
            var u = $scope.nuevoUsuario;
            p.existe = true;
            p.nombre = "";
            p.paterno = "";
            p.materno = "";
            p.nacimiento = "";
            p.email = "";
            p.numero1 = "";
            p.numero2 = "";
            u.nombre = "";
            u.password = "";
            u.existe = true;
            $scope.oSel = {};
        };
        $scope.prepararAgregar = function () {
            $scope.nuevoUsuario = {nombre: "", password: "", estado: 'A', rolID: 0, organizacionID: 0, existe: true};
            $scope.nuevaPersona = {dni: "", nombre: "", materno: "", paterno: "", nacimiento: "", email: "", numero1: "", numero2: "", existe: true};
            $scope.oSel = {};

            $("#modalNuevo").modal('show');
        };
        $scope.agregarUsuario = function () {
            var ft = new Date();
            ft = ft.toISOString();
            $scope.nuevoUsuario.organizacionID = $scope.oSel.organizacionID;
            $scope.nuevoUsuario.fecIng = ft;
            console.log("esTrabajador: "+$scope.esTrabajador);


            var request = crud.crearRequest('usuarioSistema', 1, 'insertarUsuario');
            request.setData({persona: $scope.nuevaPersona, usuario: $scope.nuevoUsuario, rOpcionRegUsuario:$scope.esTrabajador});

            crud.insertar("/configuracionInicial", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //recuperamos las variables que nos envio el servidor
                    var u = {};

                    u.usuarioID = response.data.usuarioID;
                    u.password = response.data.password;

                    u.nombreUsuario = $scope.nuevoUsuario.nombre;
                    u.estado = $scope.nuevoUsuario.estado;

                    u.nombres = $scope.nuevaPersona.nombre + " " + $scope.nuevaPersona.paterno + " " + $scope.nuevaPersona.materno;
                    u.DNI = $scope.nuevaPersona.dni;

                    u.sessiones = [];
                    var session = {};
                    session.organizacionID = $scope.oSel.organizacionID;
                    session.organizacion = $scope.oSel.nombre;
                    session.rol = buscarContenido($scope.roles, "rolID", "nombre", $scope.nuevoUsuario.rolID);
                    session.rolID = $scope.nuevoUsuario.rolID;
                    session.estado = 'A';
                    session.sessionID = response.data.sessionID;

                    u.sessiones.push(session);

                    // Agregamos un desplazamiento ya que inicia su periodo de trabajo
                    var request = crud.crearRequest('desplazamiento', 1, 'agregarDesplazamiento');
                    $scope.nuevoDespla.ficEscId = response.data.ficEscId;
                    $scope.nuevoDespla.insEdu = $scope.oSel.nombre;
                    $scope.nuevoDespla.fecIni = ft;
                    for (var i = 0; i < $scope.oSel.roles.length; i++) {
                        if ($scope.oSel.roles[i].rolID === $scope.nuevoUsuario.rolID)
                            $scope.nuevoDespla.car = $scope.oSel.roles[i].nombre;
                    }
                    request.setData($scope.nuevoDespla);
                    console.log("entra des1 with fic: " + JSON.stringify($scope.nuevoDespla));
                    crud.insertar("/sistema_escalafon", request, function (response) {
                        if(response.responseSta){
                            modal.mensaje("CONFIRMACION", "El ingreso fue registrado con éxito");
                        }else{
                            modal.mensaje("ERROR", "El ingreso no fue registrado");
                        }
                    }, function (data) {
                        console.info(data);
                    });

                    //insertamos el elemento a la lista
                    insertarElemento(settingUsuario.dataset, u);
                    $scope.tablaUsuario.reload();
                    //reiniciamos las variables
                    //$scope.nuevoUsuario = {nombre:"",password:"",estado:'A',rolID:0,organizacionID:0};
                    //$scope.nuevaPersona = {dni:"",nombre:"",materno:"",paterno:"",nacimiento:"",email:"",numero1:"",numero2:"",existe:true};
                    //cerramos la ventana modal
                    $('#modalNuevo').modal('hide');


                }
            }, function (data) {
                console.info(data);
            });

        };
        $scope.eliminarUsuario = function (i, idDato) {

            modal.mensajeConfirmacion($scope, "seguro que desea eliminar este registro", function () {

                var request = crud.crearRequest('usuarioSistema', 1, 'eliminarUsuario');
                request.setData({usuarioID: idDato});

                crud.eliminar("/configuracionInicial", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        eliminarElemento(settingUsuario.dataset, i);
                        $scope.tablaUsuario.reload();
                    }
                }, function (data) {
                    console.info(data);
                });
            });
        };
        $scope.prepararEditar = function (t) {
            $scope.usuarioSel = JSON.parse(JSON.stringify(t));
            
            $scope.usuarioSel.nombreUsuario = $scope.usuarioSel.nombreUsuario.replace(/ /g, "");
            console.log($scope.usuarioSel);

            $('#modalEditar').modal('show');
        };
        $scope.editarUsuario = function () {
            var ft = new Date();
            ft = ft.toISOString();
            console.log("edituser: " + $scope.usuarioSel);
            var request = crud.crearRequest('usuarioSistema', 1, 'actualizarUsuario');
            request.setData($scope.usuarioSel);

            crud.actualizar("/configuracionInicial", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    // Once the user is updated and its sessions have been set, add the new entries according to the roles that have been added
                    // First get the ficEscId
                    var trabajadorSel = {opcion: 2, perDni: $scope.usuarioSel.DNI};
                    var request = crud.crearRequest('datos_personales', 1, 'buscarFichaPorDNI');
                    request.setData(trabajadorSel);
                    crud.listar('/sistema_escalafon', request, function (response) {
                        // Then add a new displacement for each new role (session) added to the user
                        $scope.newSessions.forEach(function (item) {
                            // Don't worry about the new entry fecDoc or numDoc that will be added on the laboral exp, cuz' it can be
                            // edited on the laboral exprience module
                            var newDisplacement = {ficEscId: response.data.ficEscId, tip: "1", numDoc: "", fecDoc: "", tipDoc: "", insEdu: item.organizacion, car: item.rol, jorLab: "", fecIni: ft, fecTer: "", fecDocTer: "", numDocTer: "", motRet: ""};

                            var request = crud.crearRequest('desplazamiento', 1, 'agregarDesplazamiento');
                            request.setData(newDisplacement);
                            //console.log("entra des1 with fic: "+JSON.stringify(newDisplacement));
                            crud.insertar("/sistema_escalafon", request, function (response) {
                                modal.mensaje("CONFIRMACION", response.responseMsg);
                            }, function (data) {
                                console.info(data);
                            });
                        });
                        $scope.newSessions = [];
                    }, function (data) {
                        console.info(data);
                    });

                    //$scope.usuarioSel.organizacion = buscarContenido($scope.organizaciones,"organizacionID","nombre",$scope.usuarioSel.organizacionID);
                    $scope.usuarioSel.estado = response.data.usuarioAct.estado;
                    $scope.usuarioSel.nombreUsuario = response.data.usuarioAct.nombreUsuario;
                    $scope.usuarioSel.sessiones = response.data.usuarioAct.sessiones;
                    settingUsuario.dataset[$scope.usuarioSel.i] = $scope.usuarioSel;
                    $scope.tablaUsuario.reload();
                    $('#modalEditar').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.actualizarUsuario = function (opcion) {
            console.log("opcion: " + opcion);
            var ft = new Date();
            ft = ft.toISOString();
            console.log("edituser: " + JSON.stringify($scope.usuarioSel));
            var request = crud.crearRequest('usuarioSistema', 1, 'updateUsuario');
            switch(opcion){
                case 0: request.setData({usuario:$scope.usuarioSel,opcion:opcion, orgId:0, rolId:0, tipoUsuario: -1});
                       break;
                case 1: request.setData({usuario:$scope.usuarioSel,opcion:opcion,orgId:$scope.sessionToUpdate.orgSel.organizacionID, rolId:$scope.sessionToUpdate.rolSel.rolID, tipoUsuario: $scope.esTrabajadorNewRole});
                       break;
            };
            
            crud.actualizar("/configuracionInicial", request, function (response) {
                if (response.responseSta) {
                    if(opcion===1 && $scope.esTrabajadorNewRole){
                        var newDisplacement = {ficEscId: response.data.ficEscId, tip: "1", numDoc: "", fecDoc: "", tipDoc: "", insEdu: $scope.sessionToUpdate.orgSel.nombre, car: $scope.sessionToUpdate.rolSel.nombre, jorLab: "", fecIni: ft, fecTer: "", fecDocTer: "", numDocTer: "", motRet: ""};
                        var request = crud.crearRequest('desplazamiento', 1, 'agregarDesplazamiento');
                        request.setData(newDisplacement);
                        //console.log("entra des1 with fic: "+JSON.stringify(newDisplacement));
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            if(response.responseSta)
                                modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }else
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                    //$scope.usuarioSel.organizacion = buscarContenido($scope.organizaciones,"organizacionID","nombre",$scope.usuarioSel.organizacionID);
                    settingUsuario.dataset[$scope.usuarioSel.i] = $scope.usuarioSel;
                    $scope.tablaUsuario.reload();
                    $('#modalEditar').modal('hide');
                }
            }, function (data) {
                console.info(data);
            });
        };

        listarRoles();
        listarOrganizaciones();
        $scope.newSessions = [];

        $scope.agregarSession = function () {
            if (!$scope.session || !$scope.session.org || !$scope.session.rolID)
                return;

            $scope.session.organizacion = $scope.session.org.nombre;
            $scope.session.organizacionID = $scope.session.org.organizacionID;
            $scope.session.rol = buscarContenido($scope.roles, "rolID", "nombre", $scope.session.rolID);

            $scope.session.estado = 'A';

            $scope.usuarioSel.sessiones.push($scope.session);
            $scope.newSessions.push($scope.session);
            $scope.session = {};
        };

        $scope.editarSession = function (i, s) {
            //si estamso editando
            if (s.edi) {
                s.copia.organizacion = s.copia.org.nombre;
                s.copia.organizacionID = s.copia.org.organizacionID;
                s.copia.rol = buscarContenido($scope.roles, "rolID", "nombre", s.copia.rolID);
                $scope.usuarioSel.sessiones[i] = s.copia;
            }
            //si queremos editar
            else {
                s.copia = JSON.parse(JSON.stringify(s));
                for (var i = 0; i < $scope.organizaciones.length; i++) {
                    if ($scope.organizaciones[i].organizacionID == s.copia.organizacionID) {
                        s.copia.org = $scope.organizaciones[i];
                        break;
                    }
                }
                s.edi = true;
            }
        };

        $scope.eliminarSession = function (i, r) {
            //si estamso cancelando la edicion
            if (r.edi) {
                r.edi = false;
                delete r.copia;
            }
            //si queremos eliminar el elemento
            else {
                // before deleting it, check if this session that is been removed is within the newSessions array
                var ind = 0, counter = 0;
                $scope.newSessions.forEach(function (item) {
                    if ($scope.usuarioSel.sessiones[i].sessionId === item.sessionId)
                        ind = counter;
                    counter++;
                });
                $scope.newSessions.splice(ind, 1);
                $scope.usuarioSel.sessiones.splice(i, 1);
            }
        };
        
        function listarRoles() {
            //preparamos un objeto request
            var request = crud.crearRequest('rol', 1, 'listarRoles');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/configuracionInicial", request, function (data) {
                console.log(data);
                $scope.roles = data.data;
                data.data.forEach(function (item) {
                    var o = item;
                    o.id = item.nombre;
                    o.title = item.nombre;
                    $scope.rolesSelect.push(o);
                });
            }, function (data) {
                console.info(data);
            });
        }
        ;
        function listarOrganizaciones() {
            //preparamos un objeto request
            var request = crud.crearRequest('organizacion', 1, 'listarOrganizacionesConRoles');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/configuracionInicial", request, function (data) {
                console.log("listarOrganizaciones");
                console.log(data);
                $scope.organizaciones.push({nombre: ""});
                data.data.forEach(function (item) {
                    var o = item;
                    o.id = item.nombre;
                    o.title = item.nombre;
                    $scope.organizacionesSelect.push(o);
                    $scope.organizaciones.push(o);
                });
            }, function (data) {
                console.info(data);
            });
        }
        ;

        function validarDatos(persona, usuario) {

        }


    }]);
