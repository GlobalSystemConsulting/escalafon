app.controller("tipoOrganizacionCtrl",["$scope","crud","modal", function ($scope,crud,modal){
        
    $scope.tipoOrganizaciones = [];
    $scope.nuevoTipoOrganizacion = {codigo:"",nombre:"",descripcion:"",estado:'A'};
    $scope.tipoOrganizacionSel = {};
    
    $scope.roles = [];
    $scope.rolesSel = [];
    
    $scope.rolesEdi= [];
    $scope.rolesSelEdi= [];
    
    $scope.listarTipoOrganizaciones = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('tipoOrganizacion',1,'listarTipoOrganizaciones');
        request.setData({listar:true});
        crud.listar("/configuracionInicial",request,function(data){
            $scope.tipoOrganizaciones = data.data;
        },function(data){
            console.info(data);
        });
    };    
    $scope.agregarTipoOrganizacion = function(){
        
        var request = crud.crearRequest('tipoOrganizacion',1,'insertarTipoOrganizacion');
        
        $scope.nuevoTipoOrganizacion.roles = [];        
        for(var i=0;i<$scope.rolesSel.length;i++ )
            $scope.nuevoTipoOrganizacion.roles.push( $scope.rolesSel[i] );
        
        request.setData($scope.nuevoTipoOrganizacion);
        
        crud.insertar("/configuracionInicial",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.nuevoTipoOrganizacion.tipoOrganizacionID = response.data.tipoOrganizacionID;
                $scope.nuevoTipoOrganizacion.fecha = response.data.fecha;
                
                //insertamos el elemento a la lista
                $scope.tipoOrganizaciones.push($scope.nuevoTipoOrganizacion);                
                //reiniciamos las variables
                $scope.nuevoTipoOrganizacion = {codigo:"",nombre:"",descripcion:"",icono:"",estado:'A'};
                $scope.rolesSel = [];
                $scope.roles.forEach(function(item){
                    item.ver = false;
                });
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarTipoOrganizacion = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el tipo de organizacion",function(){
            
            var request = crud.crearRequest('tipoOrganizacion',1,'eliminarTipoOrganizacion');
            request.setData({tipoOrganizacionID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    $scope.tipoOrganizaciones.splice(i,1);

            },function(data){
                console.info(data);
            });
            
        });
    };
    $scope.prepararAgregar = function(){
        $scope.roles.forEach(function(item){
            item.ver = false;
        });
        
        $('#modalNuevo').modal('show');
    };
    $scope.prepararEditar = function(i,t){
        $scope.tipoOrganizacionSel = JSON.parse(JSON.stringify(t));
        $scope.tipoOrganizacionSel.i = i;
        
        $scope.rolesSelEdi= [];
        $scope.rolesEdi = JSON.parse(JSON.stringify($scope.roles));
        $scope.rolesEdi.forEach(function(item){
            item.ver = false;
        });
        
        for(var i=0; $scope.tipoOrganizacionSel.roles && i < $scope.tipoOrganizacionSel.roles.length;i++ ){            
            $scope.rolesEdi.forEach(function(item){                
                if( item.rolID === $scope.tipoOrganizacionSel.roles[i].rolID){
                    item.ver = true;
                    $scope.rolesSelEdi.push(item);
                }
            });
        }
        
        $('#modalEditar').modal('show');
    };
    $scope.editarTipoOrganizacion = function(){
        
        var request = crud.crearRequest('tipoOrganizacion',1,'actualizarTipoOrganizacion');
        
        $scope.tipoOrganizacionSel.roles = [];        
        for(var i=0;i<$scope.rolesSelEdi.length;i++ )
            $scope.tipoOrganizacionSel.roles.push( $scope.rolesSelEdi[i] );
        
        request.setData($scope.tipoOrganizacionSel);
                
        crud.actualizar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                
                //actualizando
                $scope.tipoOrganizaciones[$scope.tipoOrganizacionSel.i] = $scope.tipoOrganizacionSel;
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.eliminarTipoOrganizaciones = function(){
            
        
    };
    
    $scope.seleccionarRol = function(i){
        $scope.rolesSel.push( $scope.roles[i] );
        $scope.roles[i].ver = true;
    };
    $scope.desSeleccionarRol = function(i){
        $scope.rolesSel[i].ver = false;
        $scope.rolesSel.splice(i,1);
    };
    $scope.seleccionarRol2 = function(i){
        $scope.rolesSelEdi.push( $scope.rolesEdi[i] );
        $scope.rolesEdi[i].ver = true;
    };
    $scope.desSeleccionarRol2 = function(i){
        $scope.rolesSelEdi[i].ver = false;
        $scope.rolesSelEdi.splice(i,1);
    };
    
    listarRoles();
    
    function listarRoles(){
        //preparamos un objeto request
        var request = crud.crearRequest('rol',1,'listarRoles');        
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las usuarios de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.roles = data.data;
        },function(data){
            console.info(data);
        });
    };
    
    
}]);