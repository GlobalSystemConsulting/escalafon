app.controller("agregarEstudianteCtrl", ["$scope", "crud", "modal", "NgTableParams", "$http", function ($scope, crud, modal, NgTableParams, $http) {

        //tabla domicilios
        var paramsDomicilio = {count: 10};
        var settingDomicilio = {counts: []};
        $scope.tablaDomicilio = new NgTableParams(paramsDomicilio, settingDomicilio);

        //tabla domicilios
        var paramsPariente = {count: 10};
        var settingPariente = {counts: []};
        $scope.tablaParientes = new NgTableParams(paramsPariente, settingPariente);

        $scope.lenguas = [];
        $scope.paises = [];
        $scope.tiposParentescos = [];
        $scope.gradoInstruccion = [];

        $scope.busqueda = {
            codigoBusqueda: "",
            tipoBusqueda: "1"
        };

        $scope.busquedaPariente = {
            parDniBus: ""
        };

        $scope.datosPariente = {
            parPerId: -1,
            parNom: "",
            parApePat: "",
            parApeMat: "",
            parDni: "Desconocido",
            parTel: "Desconocido"
        };

        $scope.datosNuevoPariente = {
            parPerIdEst: -1,
            parPerIdpar: -1,
            parViv: true,
            parVivEst: true,
            parTipParCod: "1"
        };

        $scope.datosNuevaPersona = {
            perDni: -1,
            perNom: "",
            perApePat: "",
            perApeMat: "",
            perFecNac: new Date(),
            perSex: "M",
            perEstCiv: "1",
            perGraIns: "1",
            perOcu: "",
            perPai: "1",
            perDir: "",
            perTel: ""
        };

        $scope.flags = {
            Usuarioactual: true,
            disableAccessPestanias: true,
            buttonShow: true,
            loadPariente: true,
            disableAccessBuscarPariente: true,
            ParienteActual: true,
            disableDniPariente: false
        };

        $scope.estudianteActual = {
            nombreCompleto: "",
            DNI: ""
        };

        $scope.arrays = {
            parientes: [],
            documentos: [],
            domicilios: [],
            saludEstados: [],
            saludActividads: [],
            laborals: [],
            saludControleses: []
        };

        $scope.datosGenerales = {
            persona: {
                perId: -1,
                lenguaByLenMat: {
                    lenId: "1"
                },
                lenguaByLenSeg: {
                    lenId: "1"
                },
                nom: "",
                apePat: "",
                apeMat: "",
                dni: "",
                perDir: "",
                sex: "M",
                estCiv: "1"
            },
            estudiante: {
                codEst: "",
                datosNacimiento: {
                    pais: {
                        paiId: "1"
                    },
                    ubiCod: "",
                    fecNac: new Date(),
                    nacReg: false
                }
            }
        };

        $scope.datosDomicilio = {
            perId: -1,
            anyo: new Date().getFullYear(),
            domDir: "",
            domRef: "",
            domTel: "",
            domUBI: ""
        };

        $scope.openModalCrearPersona = function () {
            $("#modalAgregarPersona").modal("show");
        };

        $scope.closeCreate = function () {
            $scope.flags.loadPariente = true;
            $scope.flags.disableDniPariente = false;
        };

        $scope.checkDNI = function () {
            var dni = $scope.busquedaPariente.parDniBus;
            if (!isANumber(dni)) {
                modal.mensaje('Error', 'El DNI debe ser numerico');
                return;
            }

            $scope.busquedapariente = {
                codigoBusqueda: dni
            };
            var request = crud.crearRequest('agregarEstudiante', 1, 'buscarPersona');
            request.setData($scope.busquedapariente);
            crud.listar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    $scope.flags.loadPariente = true;

                    $scope.datosPariente.parPerId = data.data.perId;
                    $scope.datosPariente.parNom = data.data.nom;
                    $scope.datosPariente.parApePat = data.data.apePat;
                    $scope.datosPariente.parApeMat = data.data.apeMat;
                    $scope.datosPariente.parDni = data.data.dni;
                    $scope.datosPariente.parTel = data.data.num1;

                    //Datos Nuevo Pariente
                    $scope.datosNuevoPariente.parPerIdEst = $scope.datosGenerales.persona.perId;
                    $scope.datosNuevoPariente.parPerIdpar = $scope.datosPariente.parPerId;

                } else {
                    modal.mensaje('Error', 'No Se encuentra ese DNI en el Sistema');
                    $scope.datosPariente.parPerId = -1;
                    $scope.datosPariente.parNom = "";
                    $scope.datosPariente.parApePat = "";
                    $scope.datosPariente.parApeMat = "";
                    $scope.datosPariente.parDni = "";
                    $scope.datosPariente.parTel = "";

                    $scope.flags.loadPariente = false;
                    $scope.datosNuevaPersona.perDni = $scope.busquedaPariente.parDniBus;
                    $scope.flags.disableDniPariente = true;
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.crearPersona = function () {
            var request = crud.crearRequest('agregarEstudiante', 1, 'guardarNuevaPersona');
            $scope.datosNuevaPersona.perFecNac = convertirFecha2($scope.datosNuevaPersona.perFecNac);
            request.setData($scope.datosNuevaPersona);
            crud.insertar("/matriculaInstitucional", request, function (data) {

                if (data.responseSta) {
                    modal.mensaje('Creacion Exitosa', 'Se Registro la Persona satisfactoriamente');
                    $scope.checkDNI();
                    $scope.flags.disableDniPariente = false;
                    $("#modalAgregarPersona").modal("hide");

                } else {
                    modal.mensaje('Error', 'Error al Guardar Datos de la Nueva Persona');
                }


            }, function (data) {
                console.info(data);
            });
        };

        $scope.buscarEstudiante = function () {
            var tempN;
            var tempD;
            var request = crud.crearRequest('agregarEstudiante', 1, 'buscarEstudiante');
            request.setData($scope.busqueda);
            crud.listar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    $scope.datosGenerales = data.data;
                    $scope.datosGenerales.estudiante.datosNacimiento.fecNac = new Date($scope.datosGenerales.estudiante.datosNacimiento.fecNac);
                    tempN = $scope.datosGenerales.persona.nom + " " +
                            $scope.datosGenerales.persona.apePat + " " +
                            $scope.datosGenerales.persona.apeMat;
                    tempD = $scope.datosGenerales.persona.dni;
                    $scope.flags.disableAccessPestanias = false;
                    $scope.flags.buttonShow = false;
                    $scope.datosDomicilio.perId = $scope.datosGenerales.persona.perId;
                    $scope.cargarDomicilios();
                    $scope.cargarParientes();
                } else {
                    $scope.clearEstudiante();
                    modal.mensaje('Error', 'No se encontro ningun Estudiante');
                    $scope.flags.disableAccessPestanias = true;
                    tempN = "Desconocido";
                    tempD = "Desconocido";
                    $scope.flags.buttonShow = true;
                }

                $scope.estudianteActual.nombreCompleto = tempN;
                $scope.estudianteActual.DNI = tempD;


            }, function (data) {
                console.info(data);
            });
        };

        $scope.clearEstudiante = function () {
            var clear = {
                persona: {
                    perId: -1,
                    lenguaByLenMat: {
                        lenId: "1"
                    },
                    lenguaByLenSeg: {
                        lenId: "1"
                    },
                    nom: "",
                    apePat: "",
                    apeMat: "",
                    dni: "",
                    perDir: "",
                    sex: "M",
                    estCiv: "1"
                },
                estudiante: {
                    codEst: "",
                    datosNacimiento: {
                        pais: {
                            paiId: "1"
                        },
                        ubiCod: "",
                        fecNac: new Date(),
                        nacReg: false
                    }
                }
            };
            $scope.datosGenerales = clear;
        };

        $scope.buscarUbigeo = function () {
        };

        $scope.cargarDatos = function () {
            var request_1 = crud.crearRequest('agregarEstudiante', 1, 'listarLenguas');
            crud.listar("/matriculaInstitucional", request_1, function (lenguaData) {
                $scope.lenguas = lenguaData.data;
            }, function (lenguaData) {
                console.info(lenguaData);
            });

            var request_2 = crud.crearRequest('agregarEstudiante', 1, 'listarPaises');
            crud.listar("/matriculaInstitucional", request_2, function (paisData) {
                $scope.paises = paisData.data;
            }, function (paisData) {
                console.info(paisData);
            });

            var request_3 = crud.crearRequest('agregarEstudiante', 1, 'listarTipoPariente');
            crud.listar("/matriculaInstitucional", request_3, function (tipoParData) {
                $scope.tiposParentescos = tipoParData.data;
            }, function (tipoParData) {
                console.info(tipoParData);
            });

            var request_4 = crud.crearRequest('agregarEstudiante', 1, 'listarGradoInstruccion');
            crud.listar("/matriculaInstitucional", request_4, function (graInsData) {
                $scope.gradoInstruccion = graInsData.data;
            }, function (graInsData) {
                console.info(graInsData);
            });
        };

        $scope.cargarDomicilios = function () {
            var request = crud.crearRequest('agregarEstudiante', 1, 'listarDomicilios');
            request.setData($scope.datosGenerales);
            crud.listar("/matriculaInstitucional", request, function (data) {
                $scope.arrays.domicilios = data.data;
                settingDomicilio.dataset = data.data;
                iniciarPosiciones(settingDomicilio.dataset);
                $scope.tablaDomicilio.settings(settingDomicilio);
                if (data.responseSta) {
                } else {
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.cargarParientes = function () {
            var request = crud.crearRequest('agregarEstudiante', 1, 'listarParientes');
            request.setData($scope.datosGenerales);
            crud.listar("/matriculaInstitucional", request, function (data) {
                $scope.arrays.parientes = data.data;
                settingPariente.dataset = data.data;
                iniciarPosiciones(settingPariente.dataset);
                $scope.tablaParientes.settings(settingPariente);
                if (data.responseSta) {
                } else {
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.guardarDatosGenerales = function () {
            var request = crud.crearRequest('agregarEstudiante', 1, 'guardarDatosGenerales');
            $scope.datosGenerales.estudiante.datosNacimiento.fecNac = convertirFecha2($scope.datosGenerales.estudiante.datosNacimiento.fecNac);
            request.setData($scope.datosGenerales);
            crud.insertar("/matriculaInstitucional", request, function (data) {


                if (data.responseSta) {
                    if ($scope.flags.buttonShow) {
                        modal.mensaje('Creacion Exitosa', 'Se Registro al estudiante satisfactoriamente');
                    } else {
                        modal.mensaje('Modificacion Exitosa', 'Se Actualizaron los datos del estudiante satisfactoriamente');
                    }

                    $scope.busqueda.codigoBusqueda = $scope.datosGenerales.persona.dni;
                    $scope.busqueda.tipoBusqueda = "1";
                    $scope.buscarEstudiante();
                } else {
                    modal.mensaje('Error', 'Error al Guardar Datos del Estudiante');
                    $scope.flags.disableAccessPestanias = true;
                }


            }, function (data) {
                console.info(data);
            });
        };

        $scope.guardarDomicilio = function () {
            var request = crud.crearRequest('agregarEstudiante', 1, 'guardarDomicilio');
            request.setData($scope.datosDomicilio);
            crud.insertar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    modal.mensaje('Guardado Exitoso', 'Se Actualizaron los datos del estudiante satisfactoriamente');
                    $scope.cargarDomicilios();
                } else {
                    modal.mensaje('Error', 'Error al Guardar Datos del Estudiante');
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.guardarPariente = function () {
            var request = crud.crearRequest('agregarEstudiante', 1, 'guardarParienteEstudiante');
            request.setData($scope.datosNuevoPariente);
            crud.insertar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    modal.mensaje('Guardado Exitoso', 'Se Actualizaron los datos del estudiante satisfactoriamente');
                    $scope.cargarParientes();
                } else {
                    modal.mensaje('Error', 'Error al Guardar Datos del Estudiante');
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.disableButtonCheckDNI = function () {
            var numLetter = $scope.busquedaPariente.parDniBus.length;
            if (numLetter !== 8) {
                $scope.flags.disableAccessBuscarPariente = true;
            } else {
                $scope.flags.disableAccessBuscarPariente = false;
            }
        };

        $scope.eliminarDomicilio = function (domCod, perId) {
            var myData = {
                domCod: domCod,
                perId: perId
            };

            modal.mensajeConfirmacion($scope, "Seguro que desea eliminar este Domicilio ?", function () {
                var request = crud.crearRequest('agregarEstudiante', 1, 'eliminarDomicilio');
                request.setData(myData);

                crud.eliminar("/matriculaInstitucional", request, function (data) {
                    if (data.responseSta) {
                        modal.mensaje('Eliminacion Exitosa', 'Se Eliminio el domicilio seleccionado');
                        $scope.cargarDomicilios();
                    } else {
                        modal.mensaje('Error', 'Error al Eliminar el domicilio del Estudiante');
                    }
                }, function (data) {
                    console.info(data);
                });

            });
        };

        $scope.eliminarPariente = function (parId, perId) {
            var myData = {
                parId: parId,
                perId: perId
            };
            modal.mensajeConfirmacion($scope, "Seguro que desea eliminar este Pariente ?", function () {
                var request = crud.crearRequest('agregarEstudiante', 1, 'eliminarParienteEstudiante');
                request.setData(myData);

                crud.eliminar("/matriculaInstitucional", request, function (data) {
                    if (data.responseSta) {
                        modal.mensaje('Eliminacion Exitosa', 'Se Eliminio el Pariente seleccionado');
                        $scope.cargarParientes();
                    } else {
                        modal.mensaje('Error', 'Error al Eliminar el Pariente del Estudiante');
                    }
                }, function (data) {
                    console.info(data);
                });

            });
        };

        $scope.generarFichaMatricula = function () {
            var myData = {
                estId: $scope.datosGenerales.persona.perId
            };
            var request = crud.crearRequest('matriculaIndividual', 1, 'generarFichaMatriculaExcel');
            request.setData(myData);

            crud.listar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    $scope.descargarFichaMatricula();
                    modal.mensaje('Generacion Exitosa Exitosa', 'Se Genero la Ficha Unica de Matricula Satisfactoriamente');
                } else {
                    modal.mensaje('Error', 'Error al generar la Ficha Unica de Matricula');
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.descargarFichaMatricula = function () {
            var path = "../archivos/matricula_institucional/fichaMatricula/fichaMatricula_" + $scope.datosGenerales.persona.dni + ".xlsx";
            window.open(path);
        };

        function isANumber(str) {
            return !/\D/.test(str);
        }
        
        $scope.datosUbigeo = {
            departamentos: [],
            provincias: [],
            distritos: [],
            dpto: -1,
            prov: -1,
            dist: -1
        };

        $scope.loadDpto = function () {
            $http.get('../recursos/json/departamentos.json').success(function (data) {
                $scope.datosUbigeo.departamentos = data;
            });
            $scope.datosUbigeo.dpto = 01;
            $scope.datosUbigeo.prov = -1;
            $scope.datosUbigeo.dist = -1;
        };

        $scope.loadProv = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.datosUbigeo.departamentos.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.datosUbigeo.dpto;
                })[0];
                $scope.datosUbigeo.provincias = data[dep.id_ubigeo];
                $scope.datosUbigeo.distritos = [];
                $scope.datosUbigeo.prov = -1;
                $scope.datosUbigeo.dist = -1;
            });
        };

        $scope.loadDist = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.datosUbigeo.provincias.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.datosUbigeo.prov;
                })[0];
                $scope.datosUbigeo.distritos = data[pro.id_ubigeo];
                $scope.datosUbigeo.dist = -1;
            });
        };

        $scope.modificarUbigeo = function () {
            if ($scope.datosUbigeo.dpto === -1 ||
                    $scope.datosUbigeo.prov === -1 ||
                    $scope.datosUbigeo.dist === -1) {
                modal.mensaje("Error", "Seleccione adecuadamente los campos de ubicacion");
                return;
            }
            $scope.datosGenerales.estudiante.datosNacimiento.ubiCod = $scope.datosUbigeo.dpto.toString() +
                    $scope.datosUbigeo.prov.toString() + $scope.datosUbigeo.dist.toString();
            $('#modalUbigeo').modal('hide');
        };

        $scope.buscarUbigeo = function () {
            $scope.loadDpto();
            $('#modalUbigeo').modal('show');
        };
        
        $scope.buscarUbigeoSel = function () {
            $scope.loadDpto();
            $('#modalUbigeoSel').modal('show');
        };
        
        $scope.modificarUbigeoSel = function () {
            if ($scope.datosUbigeo.dpto === -1 ||
                    $scope.datosUbigeo.prov === -1 ||
                    $scope.datosUbigeo.dist === -1) {
                modal.mensaje("Error", "Seleccione adecuadamente los campos de ubicacion");
                return;
            }
            $scope.datosDomicilio.domUBI = $scope.datosUbigeo.dpto.toString() +
                    $scope.datosUbigeo.prov.toString() + $scope.datosUbigeo.dist.toString();
            $('#modalUbigeoSel').modal('hide');
        };
        
        
    }]);


