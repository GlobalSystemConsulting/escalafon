app.controller("miMatriculaCtrl", ["$scope", "$rootScope", "crud", "NgTableParams", "modal", function ($scope, $rootScope, crud, NgTableParams, modal) {

        $scope.myusuario = $rootScope.usuMaster.usuario.usuarioID;
        $scope.myOrganizacion = $rootScope.usuMaster.organizacion.organizacionID;

        $scope.myEstudiante;

        $scope.matriculaLoad = {
            matId: -1,
            conMat: -1,
            conMatNom: "Desconocido",
            matSit: -1,
            sitMatNom: "Desconocido",
            matEst: -1,
            matEstNom: "Desconocido",
            apoId: -1,
            apoNom: "Desconocido",
            obs: "",
            matFec: new Date(),
            regId: -1,
            regNom: "Desconocido",
            estId: -1,
            estNom: "Desconocido",
            orgOriId: -1,
            orgOriNom: "Desconocido",
            orgDesId: -1,
            orgDesNom: "Desconocido",
            graId: -1,
            graNom: "Sin Antecedentes",
            secId: "-",
            planNivId: -1,
            planNivNom: "Desconocido"
        };

        $scope.matriculaCreate = {
            matEstId: -1,
            matRegId: -1,
            matParId: -1,
            orgOriId: -1,
            orgDesId: -1,
            matEstNom: "EN PROCESO",
            matFec: new Date(),
            matSit: "1",
            conMat: "1",
            matObs: "",
            nivId: -1,
            jorEscId: -1,
            turId: -1,
            graId: -1,
            usuMod: $scope.myusuario
        };

        $scope.datosVacantes = {
            vacantesxGrado: 0,
            estIdSelect: -1,
            estOrgOriSelect: -1,
            ultGraCul: 0,
            sigGra: 1
        };

        $scope.arrays = {
            gradosySecciones: [],
            niveles: [],
            jornadas: [],
            turnos: [],
            grados: [],
            secciones: [],
            aulas: [],
            parientes: []
        };

        $scope.flags = {
            flagModificarMatricula: true
        };

        var paramsMatriculaInd = {count: 10};
        var settingMatriculaInd = {counts: []};
        $scope.tablaPrincipal = new NgTableParams(paramsMatriculaInd, settingMatriculaInd);


        $scope.generarMatricula = function () {
            if ($scope.matriculaCreate.matParId === -1) {
                modal.mensaje("Error", "Seleccione un Pariente Responsable");
                return;
            }

            if ($scope.matriculaCreate.nivId === -1 ||
                    $scope.matriculaCreate.jorEscId === -1 ||
                    $scope.matriculaCreate.turId === -1 ||
                    $scope.matriculaCreate.graId === -1) {
                modal.mensaje("Error", "Seleccione un Grado correctamente!");
                return;
            }

            var request = crud.crearRequest('matriculaIndividual', 1, 'generarMatriculaAutomatica');
            $scope.matriculaCreate.matEstId = $scope.myusuario;
            $scope.matriculaCreate.orgOriId = $scope.myOrganizacion;
            $scope.matriculaCreate.matRegId = $scope.myusuario;
            $scope.matriculaCreate.orgDesId = $scope.myOrganizacion;

            request.setData($scope.matriculaCreate);
            crud.insertar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    modal.mensaje("Operacion Exitosa", "Matricula Generada");
                    $('#modalMatriculaCreate').modal('hide');
                } else {
                    modal.mensaje("Error", "Error al Matricular al Estudiante");

                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.buscarEstudiantebyPerCod = function () {
            var itmRequest = {
                perId: $scope.myusuario
            };
            var request = crud.crearRequest('matriculaIndividual', 1, 'buscarEstudiantePropio');
            request.setData(itmRequest);
            crud.listar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    $scope.myEstudiante = data.data;
                } else {
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.buscarMatricula = function () {
            var itmRequest = {
                estId: $scope.myusuario
            };
            var request = crud.crearRequest('matriculaIndividual', 1, 'buscarMatricula');
            request.setData(itmRequest);
            crud.listar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    $scope.matriculaLoad = data.data;
                    $('#modalMatriculaLoad').modal('toggle');
                } else {
                    $scope.cargarGradosySecciones();
                    $scope.cargarParientes($scope.myusuario);
                    $scope.datosVacantes.ultGraCul = parseInt($scope.myEstudiante[0].ultGraCul);
                    $scope.datosVacantes.sigGra = $scope.datosVacantes.ultGraCul + 1;
                    $scope.datosVacantes.vacantesxGrado = 0;
                    $scope.matriculaCreate.matParId = -1;
                    $scope.matriculaCreate.nivId = -1;
                    $scope.matriculaCreate.jorEscId = -1;
                    $scope.matriculaCreate.turId = -1;
                    $scope.matriculaCreate.graId = -1;
                    $scope.cargarNiveles();
                    $('#modalMatriculaCreate').modal('toggle');
                }

            }, function (data) {
                console.info(data);
            });

        };

        $scope.cargarParientes = function (perId) {
            var myEstudiante = {
                persona: {
                    perId: perId
                }
            };
            var request = crud.crearRequest('agregarEstudiante', 1, 'listarParientes');
            request.setData(myEstudiante);
            crud.listar("/matriculaInstitucional", request, function (data) {
                $scope.parientes = data.data;
                if (data.responseSta) {
                } else {
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.cargarGradosySecciones = function () {
            var itmRequest = {
                orgIdUser: $scope.myOrganizacion
            };
            var request = crud.crearRequest('matriculaIndividual', 1, 'listarGradosySecciones');
            request.setData(itmRequest);
            crud.listar("/matriculaInstitucional", request, function (data) {
                $scope.arrays.gradosySecciones = data.data;
            }, function (data) {
                console.info(data);
            });
        };

        $scope.cargarNiveles = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var tempID, tempNom, obj, itmRepeat, graDis;
            var flagRepeat = true;
            $scope.arrays.niveles = [];
            $scope.arrays.jornadas = [];
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];

            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                tempID = $scope.arrays.gradosySecciones[i].nivelId;
                tempNom = $scope.arrays.gradosySecciones[i].nivelNom;
                graDis = $scope.arrays.gradosySecciones[i].gradoId;

                if (parseInt(graDis) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                obj = {
                    nivelId: tempID,
                    nivelNom: tempNom
                };
                for (var j = 0; j < $scope.arrays.niveles.length; j++) {
                    itmRepeat = $scope.arrays.niveles[j];
                    if (obj.nivelId === itmRepeat.nivelId) {
                        flagRepeat = false;
                    }
                }
                if (flagRepeat) {
                    $scope.arrays.niveles.push(obj);
                }
                flagRepeat = true;
            }
            $scope.cargarJornada();
        };

        $scope.cargarJornada = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.jornadas = [];
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];

            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                if (itm.nivelId === $scope.matriculaCreate.nivId) {
                    if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
                        continue;
                    }
                    obj = {
                        jornadaId: itm.jornadaId,
                        jornadaNom: itm.jornadaNom
                    };
                    for (var j = 0; j < $scope.arrays.jornadas.length; j++) {
                        itmRepeat = $scope.arrays.jornadas[j];
                        if (obj.jornadaId === itmRepeat.jornadaId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.jornadas.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarTurno();
        };

        $scope.cargarTurno = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.turnos = [];
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];

            $scope.datosVacantes.vacantesxGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                if (itm.jornadaId === $scope.matriculaCreate.jorEscId) {
                    obj = {
                        turnoId: itm.turnoId,
                        turnoNom: itm.turnoNom
                    };
                    for (var j = 0; j < $scope.arrays.turnos.length; j++) {
                        itmRepeat = $scope.arrays.turnos[j];
                        if (obj.turnoId === itmRepeat.turnoId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.turnos.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarGrados();
        };

        $scope.cargarGrados = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var obj, itm, itmRepeat;
            var flagRepeat = true;
            $scope.arrays.grados = [];
            $scope.arrays.secciones = [];

            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (parseInt(itm.gradoId) !== $scope.datosVacantes.sigGra) {
                    continue;
                }
                if (itm.turnoId === $scope.matriculaCreate.turId) {
                    obj = {
                        gradoSecId: itm.gradoSecId,
                        planNivelId: itm.planNivelId,
                        gradoId: itm.gradoId,
                        gradoNom: itm.gradoNom
                    };
                    for (var j = 0; j < $scope.arrays.grados.length; j++) {
                        itmRepeat = $scope.arrays.grados[j];
                        if (obj.gradoId === itmRepeat.gradoId) {
                            flagRepeat = false;
                        }
                    }
                    if (flagRepeat) {
                        $scope.arrays.grados.push(obj);
                    }
                    flagRepeat = true;
                }
            }
            $scope.cargarVacantes();
        };

        $scope.cargarVacantes = function () {
            var size = $scope.arrays.gradosySecciones.length;
            var itm;
            var vacantesXGrado = 0;
            for (var i = 0; i < size; i++) {
                itm = $scope.arrays.gradosySecciones[i];
                if (itm.gradoId === $scope.matriculaCreate.graId &&
                        itm.turnoId === $scope.matriculaCreate.turId &&
                        itm.jornadaId === $scope.matriculaCreate.jorEscId &&
                        itm.nivelId === $scope.matriculaCreate.nivId) {
                    vacantesXGrado = vacantesXGrado + (itm.gradoSecNumMax - itm.gradoSecMat);
                }
            }
            $scope.datosVacantes.vacantesxGrado = vacantesXGrado;
        };

        $scope.descargarConstanciaMatricula = function () {
            var itmRequest = {
                estId: $scope.myusuario,
                estDNI: -1
            };
            var request = crud.crearRequest('matriculaIndividual', 1, 'buscarMatricula');
            request.setData(itmRequest);
            crud.listar("/matriculaInstitucional", request, function (data) {
                if (data.responseSta) {
                    itmRequest.estDNI = data.data.estDNI;
                    var path = "../archivos/matricula_institucional/constanciaMatricula/constanciaMatricula_" + itmRequest.estDNI + ".pdf";
                    window.open(path);
                } else {
                    modal.mensaje("Error", "No se Encuentra Matriculado!");
                }
            }, function (data) {
                console.info(data);
            });

        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.cargaInicial = function () {
            $scope.buscarEstudiantebyPerCod();
            $scope.cargarGradosySecciones()
        };

    }]);


