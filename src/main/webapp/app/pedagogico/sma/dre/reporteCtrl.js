app.controller("reporteCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
    
    //agarra la fecha actual del sistema
    var inicio = new Date();
    //Pone la  fecha del mes al mes 0 enero
    inicio.setMonth(0);
    //Pone el dia del mes de la fecha al dia 1
    inicio.setDate(1);
    //Objeto que tiene 2 atributos:
    //hasta: dia1 del mes1 del año actual
    //hasta: la fecha actual
    $scope.busqueda = { desde: inicio , hasta:new Date("dd/mm/aaaa")};
    
    
    
    //metas de atencion
    var metas = [];
    
    $scope.generarEstadistica= function(){
        //almacena la data del grafico
        var datos = [];
        //
        metas.forEach(function(item){
            //Se compara el nivel guardado con el nivel seleccionado
            if( $scope.busqueda.nivelID == item.nivelID  ){
                item.metas.forEach(function(ite){
                    datos.push(ite);
                });                
            }
        });
        
        //var nivel = buscarObjeto(metas,"nivelID",$scope.busqueda.nivelID);
        var grados = buscarObjetos($scope.disenoActual.grados,"nivelID",Number($scope.busqueda.nivelID));
        var labels = [];
        grados.forEach(function(item){
            labels.push(item.abreviacion);
        });
        
        
        var años = [];
        
        var actual = (new Date()).getFullYear();
        
        for(var i = 2016;i<=actual;i++){            
            var añometas = buscarObjetos(datos,"año",""+i);
            var data = [];
            var data2 = [];
            var dataT = 0;
            var data2T = 0;
            grados.forEach(function(item){
                var g = buscarObjeto(añometas,"gradoID",item.gradoID);
                if(g){
                    dataT+=g.meta;
                    data2T+=g.meta2;
                    data.push(g.meta);
                    data2.push(g.meta2);
                }
                else{
                    data.push(0);
                    data2.push(0);
                }
                    
            }); 
            
            console.log(JSON.stringify({nombre:""+i,data:data,data2:data2,dataT:dataT,data2T:data2T}));
            años.push({nombre:""+i,data:data,data2:data2,dataT:dataT,data2T:data2T});
        }
              
        crearGraficoBarras("metasAtencion",labels,años);
        
    };

    $scope.imprimirGrafico = function(){
        var grafico = new MyFile("Reporte Metas de Atencion");
        grafico.parseDataURL( document.getElementById("metasAtencion").toDataURL("image/png") );
        
        var request = crud.crearRequest('planEstudios',1,'reporte');
        request.setData(grafico);
        crud.insertar("/cuadroHoras",request,function(response){
            if(response.responseSta){                
                verDocumento( response.data.reporte );
            }            
        },function(data){
            console.info(data);
        });       
    };
    
    listarDatos();
    function listarDatos(){
        request = crud.crearRequest('organizacion',1,'listarOrganizaciones');
        crud.listar("/configuracionInicial",request,function(data){
            $scope.organizaciones = data.data;
        },function(data){
            console.info(data);
        });
    };
    
    
    //Sirve para listar los niveles
    $scope.diseoCurricularVigente = function(){
        var request = crud.crearRequest('diseñoCurricular',1,'buscarVigentePorOrganizacion');
        request.setData({organizacionID:$scope.busqueda.orgID});
        crud.listar("/cuadroHoras",request,function(response){
            if(response.responseSta){
                $scope.disenoActual = response.data;
            }
        },function(data){
            console.info(data);
        });
        
        
    
        //Esto sirve para llenar metas
        request = crud.crearRequest('planEstudios',1,'listarMetasAtencion');
        request.setData({organizacionID:$scope.busqueda.orgID,desde:convertirFecha($scope.busqueda.desde),hasta:convertirFecha($scope.busqueda.hasta)});
        crud.listar("/cuadroHoras",request,function(response){            
            if(!response.responseSta){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                return;
            }
            if(response.data)
                metas = response.data;
            
        },function(data){
            console.info(data);
        });
        
    };
    
    $scope.options = [true, false, false, false];
        $scope.updateOptions = function (numOption) {
            for (var i = 0; i < $scope.options.length; i += 1) {
                $scope.options[i] = false;
            }
            $scope.options[numOption] = true;
        };
        
        //Grafica de barras numero docentes totales

    $scope.labels = ['2017'];
    $scope.series = ['Docentes Totales', 'Docentes Monitoreados'];

    $scope.data = [
        [30, 0],
        [10, 0]
    ];
        //Grafica de barras numero por niveles
    $scope.labels1 = ['2017'];
    $scope.series1 = ['Inicial', 'Primaria', 'Secundaria'];

    $scope.data1 = [
        [2, 0],
        [3, 0],
        [5, 0]
    ];
    
            //Grafica de barras numero inicial y primaria
    $scope.labels2 = ['2017'];
    $scope.series2 = ['Inicial', 'Primero', 'Segundo','Tercero', 'Cuarto', 'Quinto', 'Sexto'];

    $scope.data2 = [
        [2, 0],
        [0, 0],
        [1, 0],
        [0, 0],
        [1, 0],
        [0, 0],
        [1, 0]
    ];
    
                //Grafica de barras numero secundaria
    $scope.labels3 = ['2017'];
    $scope.series3 = ['Primero', 'Segundo','Tercero', 'Cuarto', 'Quinto'];

    $scope.data3 = [
        [1, 0],
        [0, 0],
        [0, 0],
        [1, 0],
        [3, 0]
    ];
    
                //grafica de pastel
    $scope.labels4 = ["En Inicio", "En Progreso", "Logrado"];
    $scope.data4 = [20, 10, 30];
    
     	
              //grafica de pastel
    $scope.labels5 = ["Entrada", "Proceso", "Salida"];
    $scope.data5 = [10, 30, 20]; 
    
              //GRAFICO DE CRECIMIENTO
    $scope.labels6 = ["1era Ficha de Evaluación", "2da Ficha de Evaluación", "3era Ficha de Evaluación"];
  $scope.series6 = ['Puntaje ficha de monitoreo'];
  $scope.data6 = [
    [14, 20, 17]
  ];
  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };
  $scope.datasetOverride6 = [{ yAxisID: 'y-axis-1' }];
  $scope.options6 = {
    scales: {
      yAxes: [
        {
          id: 'y-axis-1',
          type: 'linear',
          display: true,
          position: 'left'
        }
      ]
    }
  };
    
            // grafica de crecimiento roja
    $scope.colors7 = ['#ff6384'];

    $scope.labels7 = ['1era Ficha de Evaluación', '2da Ficha de Evaluación', '3era Ficha de Evaluación'];
    $scope.data7 = [
      [60, 80, 72, 72]
    ];
    $scope.datasetOverride7 = [
      {
        label: "Porcentaje",
        borderWidth: 3,
        hoverBackgroundColor: "rgba(255,99,132,0.4)",
        hoverBorderColor: "rgba(255,99,132,1)",
        type: 'line'
      }
    ];
    
    //Grafica de barras numero docentes totales

    $scope.labels8 = ['2017'];
    $scope.series8 = ['Compromiso Número 1', 'Compromiso Número 2','Compromiso Número 3', 'Compromiso Número 4','Compromiso Número 5', 'Compromiso Número 6'];

    $scope.data8 = [
        [9, 0],
        [5, 0],
        [6, 0],
        [2, 0],
        [10, 0],
        [1, 0]
    ];
    
    //tablas
    var paramsMonitoreos = {count: 10};
    var settingMonitoreos = {counts: []};
    $scope.tablaMonitoreos = new NgTableParams(paramsMonitoreos, settingMonitoreos);
    
    
        //Grafica de barras numero docentes totales 9

    $scope.labels9 = ['2017'];
    $scope.series9 = ['Unidad de Aprendizaje 1', 'Unidad de Aprendizaje 2', 'Unidad de Aprendizaje 3'];

    $scope.data9 = [
        [10, 0],
        [20, 0],
        [40, 0]
    ];
    
        //Grafica de barras numero docentes totales 10

    $scope.labels10 = ['2017'];
    $scope.series10 = ['Trimestre 1', 'Trimestre 2', 'Trimestre 3'];

    $scope.data10 = [
        [50, 0],
        [30, 0],
        [60, 0]
    ];
    
    
    
}]);

