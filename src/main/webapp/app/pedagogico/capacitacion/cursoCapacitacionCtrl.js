document.write('<script src="pedagogico/capacitacion/evaluacionCapacitacionCtrl.js"></script>');
var mcApp = angular.module('app');
mcApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/cursoCapacitacion/evaluaciones/:temCod',{
            templateUrl:'pedagogico/capacitacion/evaluacionCapacitacion.html',
            controller:'evaluacionCapacitacionCtrl',
            controllerAs:'evaCtrl'
        }).when('/cursoCapacitacion/certificacion/:curCod',{
            templateUrl:'pedagogico/capacitacion/gestionCertificados.html',
            controller:'certificacionCtrl',
            controllerAs:'certCtrl'
        });
}]);

app.controller("cursoCapacitacionCtrl", ["$rootScope", "$scope", "$http", "NgTableParams", "crud", "modal", "$location", '$uibModal', function ($rootScope, $scope, $http, NgTableParams, crud, modal, $location, $uibModal) {
    $scope.tipoEvaluacion = [{id: 'P', nom: "Evaluación Parcial"}, {id: 'E', nom: "Encuesta"}];
    $scope.tipoCriterio = ["Porcentual", "Nominal"];
    $scope.opcionesFecha = {};
    $scope.opcionesFechaTem = {};

    var settingCapacitacion = {counts: []};
    $scope.tablaCapacitaciones = new NgTableParams({count: 10}, settingCapacitacion);    

    $scope.listarCursosCapacitacion = function () {
        var request = crud.crearRequest('cursos', 1, 'listarCursos');
        request.setData({opt: 1,
                         org: $rootScope.usuMaster.organizacion.organizacionID,
                         usu: $rootScope.usuMaster.usuario.usuarioID,
                         rol: $rootScope.usuMaster.rol.rolID});

        crud.listar('/curso_capacitacion', request, function (success) {
            settingCapacitacion.dataset = success.data;
            iniciarPosiciones(settingCapacitacion.dataset);
            $scope.tablaCapacitaciones.settings(settingCapacitacion);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los cursos de capacitación " + error.responseMsg);
        });
    };

    $scope.listarOrganizaciones = function() {
        var request = crud.crearRequest('cursos', 1, 'listarOrganizaciones');
        request.setData({opt: 0});
        
        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {                
                $scope.organizaciones = success.data;
            } else
                modal.mensaje("MENSAJE", "No existen organizaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las organizaciones " + error.responseMsg);
        });
    };
    
    $scope.nuevaCapacitacion_1 = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'coursePh1.html',
            controller: 'coursePh1Ctrl',
            controllerAs: 'course'
        });
        
        modalInstance.result.then(function(objCap) {
            $scope.listarOrganizaciones();
            $scope.listarCursosCapacitacion();
            if (objCap.est === 'F') {
                $('#modalNuevoCursoCapacitacion_1').modal('toggle');
                modal.mensaje("MENSAJE", "La capacitación registrada empezó el " + (new Date(objCap.ini)).toLocaleDateString() + " y terminó el " + (new Date(objCap.fin)).toLocaleDateString() + " por lo tanto se encuentra en estado finalizada");
            } else
                $scope.ir1_2(objCap);
        }, function(){});        
    };

    $scope.nuevaCapacitacion_2 = function (capacitacion) {
        $scope.capActual = capacitacion;

        var request = crud.crearRequest('cursos', 1, 'listarSedes');
        request.setData({opt: 0, cod: $scope.capActual.cod});

        crud.listar('/curso_capacitacion', request, function (success) {
            $scope.capActual.sedes = success.data;
            $scope.capActual.estBusCap = false;
            $scope.opcionesFecha = {
                maxDate: new Date($scope.capActual.fin),
                minDate: new Date($scope.capActual.ini),
                initDate: new Date($scope.capActual.ini),
                startingDay: 1
            };
            $scope.material = {
                cod: 1,
                des: "",
                cos: ""
            };
            $scope.capacitador = {
                cod: 1,
                id: 0,
                sed: "",
                dni: "",
                nom: "",
                apePat: "",
                apeMat: "",
                email: "",
                car: "",
                aPaMn: ""
            };
            $scope.tema = {
                cod: 1,
                est: ($scope.capActual.tip === 'Online'),
                sed: "",
                ini: "",
                fin: "",
                tem: ""
            };
            $scope.metodologia = {
                cod: 1,
                des: "",
                tecnicas: []
            };
            $scope.tecnica = {
                cod: 0,
                des: ""
            };
            $scope.materiales = [];
            $scope.contenidos = [];
            $scope.capacitadores = [];
            $scope.metodologias = [];
            $scope.tecnicas = [];
            
            if($scope.capActual.tip === 'Online') {
                $scope.opcionesFechaTem = {
                    maxDate: new Date($scope.capActual.sedes[0].fin),
                    minDate: new Date($scope.capActual.sedes[0].ini),
                    initDate: new Date($scope.capActual.sedes[0].ini),
                    startingDay: 1
                };
            }
            
            $("#modalNuevoCursoCapacitacion_2").modal('show');
            $scope.status_2 = {
                close: true,
                statusMe: true,
                statusMa: false,
                statusE: false,
                statusC: false
            };
            
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las organizaciones " + error.responseMsg);
        });
    };

    $scope.nuevaCapacitacion_3 = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'coursePh3.html',
            controller: 'coursePh3Ctrl',
            controllerAs: 'course',
            resolve: {
                course: function() {
                    return {
                        current: $scope.capActual
                    };
                }
            }
        });
        
        modalInstance.result.then(function() {
            modal.mensaje("MENSAJE", "La capacitación fue creada exitosamente");
        }, function(){});      
    };
    
    $scope.crearCapacitacion_2 = function () {
        if ($scope.metodologias.length > 0) {
            for (var i = 0; i < $scope.tecnicas.length; i++)
                $scope.metodologias[$scope.tecnicas[i].cod - 1].tecnicas.push($scope.tecnicas[i]);

            var request = crud.crearRequest('cursos', 1, 'registrarMetodologia');
            request.setData({cod: $scope.capActual.cod,
                metodologias: $scope.metodologias});

            crud.insertar('/curso_capacitacion', request, function (successMethodology) {
            }, function (errorMethodology) {
                modal.mensaje("MENSAJE", errorMethodology.responseMsg);
            });
        }

        if ($scope.materiales.length > 0) {
            var request = crud.crearRequest('cursos', 1, 'registrarMaterial');
            request.setData({cod: $scope.capActual.cod,
                materiales: $scope.materiales});

            crud.insertar('/curso_capacitacion', request, function (successMaterial) {
            }, function (errorMaterial) {
                modal.mensaje("MENSAJE", errorMaterial.responseMsg);
            });
        }

        if ($scope.capacitadores.length > 0) {
            var request = crud.crearRequest('cursos', 1, 'registrarCapacitadores');
            request.setData({tip: $scope.capActual.tip,
                sed: $scope.capActual.sedes[0].cod,
                capacitadores: $scope.capacitadores});

            crud.insertar('/curso_capacitacion', request, function (successTrainner) {
            }, function (errorTrainner) {
                modal.mensaje("MENSAJE", errorTrainner.responseMsg);
            });
        }

        if ($scope.contenidos.length > 0) {
            var request = crud.crearRequest('cursos', 1, 'registrarTemario');
            request.setData({usuMod: $rootScope.usuMaster.usuario.usuarioID,
                            contenidos: $scope.contenidos});

            crud.insertar('/curso_capacitacion', request, function (successMaterial) {
            }, function (errorMaterial) {
                modal.mensaje("MENSAJE", errorMaterial.responseMsg);
            });
        }

        $scope.ir2_3();
    };

    $scope.cerrar_2 = function () {
        if (($scope.formNuevoCursoCapacitacion_2.$pristine))
            $('#modalNuevoCursoCapacitacion_2').modal('toggle');
        else {
            if ($scope.formNuevoCursoCapacitacion_2.$dirty) {
                modal.mensajeConfirmacion($scope, "Los cambios no se quedarán registrados. ¿Desea continuar?", function () {
                    $('#modalNuevoCursoCapacitacion_2').modal('toggle');
                }, '400');
            }
        }
    };

    $scope.cerrar_3 = function () {
        if (($scope.formNuevoCursoCapacitacion_3.$pristine))
            $('#modalNuevoCursoCapacitacion_3').modal('toggle');
        else {
            if ($scope.formNuevoCursoCapacitacion_3.$dirty) {
                modal.mensajeConfirmacion($scope, "Los cambios no se quedarán registrados. ¿Desea continuar?", function () {
                    $('#modalNuevoCursoCapacitacion_3').modal('toggle');
                }, '400');
            }
        }
    };

    $scope.ir1_2 = function (capacitacion) {
        modal.mensajeConfirmacion($scope, "Aún hay información por completar. ¿Desea continuar?", function () {
            $scope.nuevaCapacitacion_2(capacitacion);
        }, '400');

        $('#modalNuevoCursoCapacitacion_1').modal('toggle');
    };

    $scope.ir2_3 = function () {
        modal.mensajeConfirmacion($scope, "Aún hay información por completar. ¿Desea continuar?", function () {
            $scope.nuevaCapacitacion_3();
        }, '400');

        $('#modalNuevoCursoCapacitacion_2').modal('toggle');
    };

    $scope.agregarMaterial = function () {
        if ($scope.material.des === "" || $scope.material.cos === "")
            modal.mensaje("ERROR", "Se debe ingresar la descripción y costo del material");
        else {
            if ($scope.formNuevoCursoCapacitacion_2.costEquip.$valid) {
                $scope.materiales.push($scope.material);
                var cod = $scope.material.cod;
                $scope.material = {
                    cod: cod + 1,
                    des: "",
                    cos: ""
                };
            } else
                modal.mensaje("ERROR", "El costo debe ingresarse bajo el formato: ddd.ddd");
        }
    };

    $scope.agregarTema = function () {
        if(($scope.capActual.tip === 'Presencial' && $scope.tema.sed !== "") || $scope.capActual.tip === 'Online')
            if ($scope.tema.ini !== "" && $scope.tema.fin !== "" && $scope.tema.fin.getTime() >= $scope.tema.ini.getTime())
                if ($scope.tema.tem !== "") {
                    if($scope.capActual.tip === 'Online')
                        $scope.tema.sed = $scope.capActual.sedes[0].cod;
                    
                    $scope.contenidos.push($scope.tema);
                    var cod = $scope.tema.cod;
                    $scope.tema = {
                        cod: cod + 1,
                        ini: "",
                        fin: "",
                        tem: "",
                        est: ($scope.capActual.tip === 'Online')
                    };
                } else
                    modal.mensaje("ERROR", "Es necesario ingresar la descripción del tema");
            else
                modal.mensaje("ERROR", "La fecha de inicio debe ser mayor o igual a la fecha de fin");
        else
            modal.mensaje("ERROR", "Es necesario seleccionar una sede");
    };

    $scope.completarDatos = function () {
        if ($scope.formNuevoCursoCapacitacion_2.dniCap.$valid && $scope.capacitador.dni !== "") {
            var request = crud.crearRequest('cursos', 1, 'listarPersonas');
            request.setData({opt: 0, dni: $scope.capacitador.dni});

            crud.listar('/curso_capacitacion', request, function (success) {
                if (angular.equals(success.data, {}))
                    $scope.capActual.estBusCap = false;
                else {
                    $scope.capacitador.id = success.data.id;
                    $scope.capacitador.nom = success.data.nom;
                    $scope.capacitador.apePat = success.data.apePat;
                    $scope.capacitador.apeMat = success.data.apeMat;
                    $scope.capacitador.aPaMn = success.data.apePat + " " + success.data.apeMat + " " + success.data.nom;
                    $scope.capacitador.email = success.data.email;
                    $scope.capActual.estBusCap = true;
                }

            }, function (error) {
                modal.mensaje("MENSAJE", "No se pudo buscar al capacitador " + error.responseMsg);
            });
        } else {
            $scope.capacitador.nom = "";
            $scope.capacitador.apePat = "";
            $scope.capacitador.apeMat = "";
            $scope.capacitador.aPaMn = "";
            $scope.capacitador.email = "";
            $scope.capacitador.car = "";
            $scope.capActual.estBusCap = false;
        }
    };

    $scope.verificarCapacitador = function () {
        var objCap;
        if ($scope.capActual.tip === 'Presencial')
            objCap = $scope.capacitadores.filter(function (obj) {
                return obj.dni === $scope.capacitador.dni && obj.sed == $scope.capacitador.sed;
            })[0];
        else
            objCap = $scope.capacitadores.filter(function (obj) {
                return obj.dni === $scope.capacitador.dni;
            })[0];

        return (objCap === undefined);
    };

    $scope.normalizarCapacitador = function () {
        $scope.capacitadores.push($scope.capacitador);
        var cod = $scope.capacitador.cod;
        $scope.capacitador = {
            cod: cod + 1,
            id: 0,
            sed: "",
            dni: "",
            nom: "",
            apePat: "",
            apeMat: "",
            email: "",
            car: "",
            aPaMn: ""
        };
    };

    $scope.agregarCapacitador = function () {
        if ($scope.capActual.tip === 'Presencial' && $scope.capacitador.sed === "")
            modal.mensaje("ERROR", "Es necesario seleccionar una sede");
        else
        if ($scope.formNuevoCursoCapacitacion_2.dniCap.$valid && $scope.capacitador.dni !== "")
            if ($scope.capacitador.aPaMn !== "")
                if ($scope.capacitador.car !== "") {
                    var aPaMn = $scope.capacitador.aPaMn.split(" ");

                    if (aPaMn.length >= 2) {
                        $scope.capacitador.apePat = aPaMn.splice(0, 1).join("");
                        $scope.capacitador.apeMat = aPaMn.splice(0, 1).join("");
                        $scope.capacitador.nom = aPaMn.join(" ");

                        if ($scope.verificarCapacitador()) {
                            if(!$scope.capActual.estBusCap) {
                                var request = crud.crearRequest('cursos', 1, 'registrarPersona');
                                request.setData({persona: $scope.capacitador});
                                
                                crud.insertar('/curso_capacitacion', request, function (successPerson) {
                                    $scope.capacitador.id = successPerson.data.cod;
                                    $scope.normalizarCapacitador();
                                }, function (errorProfessor) {
                                    modal.mensaje("MENSAJE", errorProfessor.responseMsg);
                                });
                            } else {
                                $scope.normalizarCapacitador();
                                $scope.capActual.estBusCap = false;
                            }                      
                        } else
                            modal.mensaje("ERROR", "El capacitador ya se encuentra se encuentra registrado");
                    } else
                        modal.mensaje("ERROR", "Es necesario ingresar el campo el apellidos y nombres bajo el formato: APaterno AMaterno Nombre");
                } else
                    modal.mensaje("ERROR", "Es necesario ingresar el cargo del capacitador");
            else
                modal.mensaje("ERROR", "Es necesario ingresar los apellidos y nombres");
        else
            modal.mensaje("ERROR", "Es necesario ingresar el dni bajo el formato de 8 digitos");
    };

    $scope.agregarMetodologia = function () {
        if ($scope.metodologia.des !== "") {
            $scope.metodologias.push($scope.metodologia);
            var cod = $scope.metodologia.cod;
            $scope.metodologia = {
                cod: cod + 1,
                des: "",
                tecnicas: []
            };
        } else
            modal.mensaje("ERROR", "Es necesario ingresar la descripción de la metodología");
    };

    $scope.agregarTecnica = function () {
        if ($scope.tecnica.cod !== 0)
            if ($scope.tecnica.des !== "") {
                $scope.tecnicas.push($scope.tecnica);
                var cod = $scope.tecnica.cod;
                $scope.tecnica = {
                    cod: cod + 1,
                    des: ""
                };
            } else
                modal.mensaje("ERROR", "Es necesario ingresar la descripción de la técnica");
        else
            modal.mensaje("ERROR", "Es necesario seleccionar la metodología");
    };

    $scope.mostrarInfoSedeCap = function () {
        var obj = $scope.capActual.sedes.filter(function (obj) {
            return obj.cod == $scope.capacitador.sed;
        })[0];

        modal.mensaje("MENSAJE", "Sede seleccionada: PROVINCIA = " + obj.pro + ", DISTRITO = " + obj.dis);
    };
    
    $scope.mostrarInfoSedeTem = function () {
        var obj = $scope.capActual.sedes.filter(function (obj) {
            return obj.cod == $scope.tema.sed;
        })[0];

        modal.mensaje("MENSAJE", "Sede seleccionada: PROVINCIA = " + obj.pro + ", DISTRITO = " + obj.dis);
        $scope.opcionesFechaTem = {
            maxDate: new Date(obj.fin),
            minDate: new Date(obj.ini),
            initDate: new Date(obj.ini),
            startingDay: 1
        };
        $scope.tema.est = true;
    };
    
    $scope.eliminarCapacitacion = function (index) {
        modal.mensajeConfirmacion($scope, "¿Estás seguro de eliminar el curso de Capacitación: " + settingCapacitacion.dataset[index].nom + " ?", function () {
            var request = crud.crearRequest('cursos', 1, 'eliminarCurso');
            request.setData({cod: settingCapacitacion.dataset[index].cod});

            crud.eliminar('/curso_capacitacion', request, function (successMaterial) {
                modal.mensaje("MENSAJE", successMaterial.responseMsg);
                settingCapacitacion.dataset.splice(index, 1);
                $scope.tablaCapacitaciones.reload();
            }, function (errorMaterial) {
                modal.mensaje("ERROR", errorMaterial.responseMsg);
            });
        }, '400');
    };

    $scope.editarCapacitacion = function (index) {
        var request = crud.crearRequest('cursos', 1, 'listarCursos');
        request.setData({opt: 4,
            cod: settingCapacitacion.dataset[index].cod});

        crud.listar('/curso_capacitacion', request, function (success) {
            $scope.obtenerOrganizaciones(function () {
                $scope.cursoCapacitacion = success.data;
                $scope.cursoCapacitacion.usuMod = $rootScope.usuMaster.usuario.usuarioID;
                $scope.cursoCapacitacion.modo = true;
                $scope.cursoCapacitacion.organizacion = $scope.cursoCapacitacion.organizacion.toString();
                $scope.cursoCapacitacion.organizacionAut = $scope.cursoCapacitacion.organizacionAut.toString();

                if ($scope.cursoCapacitacion.tip === 'Online') {
                    $scope.cronograma = $scope.cursoCapacitacion.sedes[0].cronograma;
                    $scope.horario = $scope.cursoCapacitacion.sedes[0].horario;
                }

                $("#modalNuevoCursoCapacitacion_1").modal('show');
                $scope.status_1 = {
                    close: true,
                    statusGD: true,
                    statusLD: false,
                    statusS: false,
                    statusSh: false
                };
            });
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los cursos de capacitación " + error.responseMsg);
        });
    };
    
    $scope.evaluacionesCapacitacion = function(sede, capacitacion) {
        $rootScope.dataCap = {
            capNom: capacitacion.nom,
            capTip: capacitacion.tip,
            sedCod: sede.id,
            sedNom: sede.nom,
            sedIni: sede.iniC,
            sedFin: sede.finC,
            sedEst: sede.est
        };
        
        $location.url('/cursoCapacitacion/evaluaciones/' + sede.id);
    };
    
    $scope.generarCertificacion = function(capacitacion) {
        $rootScope.dataCap = {
            capCod: capacitacion.cod,
            capNom: capacitacion.nom,            
            capSer: "EA-01000101",
            capTip: capacitacion.tip,
            capSed: capacitacion.sedes
        };
        
        $location.url('/cursoCapacitacion/certificacion/' + capacitacion.cod);        
    };
    
    $scope.generaReportes = function() {
        $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'report.html',
            controller: 'reportCtrl',
            controllerAs: 'report'
        });
    };
    
    $scope.gestionarEncuestas = function() {
        $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'survey.html',
            controller: 'surveyCtrl',
            controllerAs: 'survey'
        });
    };
}]);

mcApp.controller('coursePh1Ctrl',['$uibModalInstance', '$rootScope', 'crud', 'modal', 'NgTableParams', '$http', '$scope', '$uibModal', function($uibModalInstance, $rootScope, crud, modal, NgTableParams, $http, $scope, $uibModal) {
    var self = this;
    self.status = {
        close: true,
        statusGD: true,
        statusLD: false,
        statusS: false,
        statusSh: false
    };    
    
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    self.dateOptions = {
        minDate: tomorrow,
        initDate: tomorrow
    };
    self.tiposCapacitacion = ["Presencial", "Online"];
    self.dias = [{id: '1', nom: "Domingo"}, {id: '2', nom: "Lunes"}, {id: '3', nom: "Martes"}, {id: '4', nom: "Miércoles"}, {id: '5', nom: "Jueves"}, {id: '6', nom: "Viernes"}, {id: '7', nom: "Sábado"}];
            
    $scope.$watch('course.course.tip', function(newValue) {
        self.tablePlaces = [];
        self.tableSchedules = [];
        self.tableShifts = [];
            
        if(newValue === 'Online') {
            self.tablePlaces.push({cod: 1, pro: '', dis: '', loc: '', dir: '', ref: ''}); 
            self.tableSchedules.push({codSed: 1, fecIni: '', fecFin: '', dniRes: '', nomRes: ''});
            self.tableShifts.push({codSed: 1, shifts: [{cod: 1, dia: '', turIni: '', turFin: ''}]});
        }
    });
    
    self.listarOrganizaciones = function() {
        var request = crud.crearRequest('cursos', 1, 'listarOrganizaciones');
        request.setData({opt: 0});
        
        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                self.organizaciones = success.data;
                self.cargarProvincias();
            } else
                modal.mensaje("MENSAJE", "No existen organizaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las organizaciones " + error.responseMsg);
        });
    };
    
    self.cargarProvincias = function () {
        $http.get('../recursos/json/provincias.json').success(function (data) {
            self.provincias = data['4180'];
        });
    };

    self.cargarDistritos = function () {
        $http.get('../recursos/json/distritos.json').success(function (data) {
            var provincia = self.provincias.filter(function (obj) {
                return obj.nombre_ubigeo === self.location.pro;
            })[0];

            self.distritos = data[provincia.id_ubigeo];
            self.location.estPro = true;
        });
    };
    
    self.agregarSede = function() {
        if(self.verificarSede(self.location, false)) {
            self.tablePlaces.push(angular.copy(self.location));     
            self.tableSchedules.push({codSed: self.location.cod, fecIni: '', fecFin: '', dniRes: '', nomRes: ''});   
            self.tableShifts.push({codSed: self.location.cod, shifts: [{cod: 1, dia: '', turIni: '', turFin: ''}]});
            self.location = {cod: self.location.cod + 1, pro: '', dis: '', loc: '', dir: '', ref: '', estPro: false};
        } else
            modal.mensaje("MENSAJE", "Ya existe una sede que posee los valores ingresados (PROVINCIA, DISTRITO, LOCALIDAD)");
    };
    
    self.agregarHorario = function(codSed) {
        if(self.validarHorario(self.shift))
            if(self.verificarHorario(codSed, self.shift, false)) {
                self.tableShifts.filter(function(item){
                    return item.codSed === codSed;
                })[0].shifts.push(angular.copy(self.shift));
                self.shift = {cod: self.shift.cod + 1, dia: '', turIni: '', turnFin: ''};
            } else
                modal.mensaje("MENSAJE", "Ya existe un horario asignado con los valores ingresados");
        else
            modal.mensaje("MENSAJE", "No existe coherencia con el periodo del horario a ingresar");
    };
    
    self.verificarSede = function(objPlace, state) {
        var array = self.tablePlaces.filter(function(obj) {
            return  obj.pro === objPlace.pro &&
                    obj.dis === objPlace.dis &&
                    obj.loc === objPlace.loc;
        });
        
        if(state)
            return (array.length === 1);
        else
            return (array.length === 0); 
    };
    
    self.verificarHorario = function(codSed, objShift, state) {
        var objHor = self.tableShifts.filter(function(obj){
            return obj.codSed === codSed;
        })[0];
        
        var turIni = new Date(objShift.turIni);       
        var turFin = new Date(objShift.turFin);

        var array = objHor.shifts.filter(function(obj) {
            var turIniObj = new Date(obj.turIni);
            var turFinObj = new Date(obj.turFin);

            return  turIni.getHours() === turIniObj.getHours() && 
                    turIni.getMinutes() === turIniObj.getMinutes() &&
                    turFin.getHours() === turFinObj.getHours() && 
                    turFin.getMinutes() === turFinObj.getMinutes() &&
                    obj.dia === objShift.dia;
        });
            
        if(state)
            return (array.length === 1);
        else
            return (array.length === 0);         
    };
    
    self.verificarCronograma = function(objCro) {
        return objCro.fecFin.getTime() > objCro.fecIni.getTime()
    };
    
    self.validarHorario = function(objShift) {
        var turIni = objShift.turIni;
        var turFin = objShift.turFin;
        
        if(turFin.getHours() > turIni.getHours())
            return true;
        else if(turFin.getHours() === turIni.getHours()){
            if(turFin.getMinutes() >= turIni.getMinutes())
                return true;
            else
                return false;
        } else
            return false;
    };
    
    self.agregarPerfiles = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'goalProfile.html',
            controller: 'goalProfileCtrl',
            controllerAs: 'goalProfile',
            resolve: {
                state: function() {
                    return false;
                }
            }
        });
        
        modalInstance.result.then(function(obj) {
            if (!angular.equals(obj, {})) {
                self.course.per.est = true;
                self.course.per.obj = obj;
            }
        }, function(){});
    };
    
    self.agregarObjetivos = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'goalProfile.html',
            controller: 'goalProfileCtrl',
            controllerAs: 'goalProfile',
            resolve: {
                state: function() {
                    return true;
                }
            }
        });
        
        modalInstance.result.then(function(obj) {
            if (!angular.equals(obj, {})) {
                self.course.obj.est = true;
                self.course.obj.obj = obj;
            }
        }, function(){});
    };
    
    self.getName = function(id) {
        if(id === '')
            return "";
        else
            return self.dias.filter(function(day){
                return day.id === id;
            })[0].nom;
    };
    
    self.cancelar = function(row, rowForm, type, cod) {
        var originalRow = self.buscar(row, rowForm, type, cod);
        angular.extend(row, originalRow);        
    };
    
    self.buscar = function(row, rowForm, type, cod){
        switch(type) {
            case 'S':
                row.isEditing = false;
                rowForm.$setPristine();
                return self.tablePlaces.filter(function(r) {
                    return r.i === row.i;
                })[0];
                break;
                
            case 'C':
                row.isEditing = false;
                rowForm.$setPristine();
                return self.tableSchedules.filter(function(r) {
                    return r.codSed === row.codSed;
                })[0];
                break;
                
            case 'H':
                row.isEditing = false;
                rowForm.$setPristine();
                return self.tableShifts.filter(function(obj){
                    return obj.codSed === cod;
                })[0].shifts.filter(function(item){
                    return item.cod === row.cod;
                })[0];
                break;
        }
    };

    self.guardar = function(row, rowForm, type, cod) {
        switch(type) {
            case 'S':
                if(self.verificarSede(row, true)) {
                    var originalRow = self.buscar(row, rowForm, type, cod);
                    angular.extend(originalRow, row);  
                } else
                    modal.mensaje("MENSAJE", "Ya existe una sede que posee los valores ingresados (PROVINCIA, DISTRITO, LOCALIDAD)");
                break;
            
            case 'C':
                if(self.verificarCronograma(row)) {
                    var originalRow = self.buscar(row, rowForm, type, cod);
                    angular.extend(originalRow, row);
                } else 
                    modal.mensaje("MENSAJE", "La fecha de inicio debe ser mayor a la fecha de fin");
                break;
                
            case 'H':
                if(self.validarHorario(row))
                    if(self.verificarHorario(cod, row, true)) {
                        var originalRow = self.buscar(row, rowForm, type, cod);
                        angular.extend(originalRow, row);  
                    } else
                        modal.mensaje("MENSAJE", "Ya existe un horario asignado con los valores ingresados");
                else
                    modal.mensaje("MENSAJE", "No existe coherencia con el periodo del horario en edición");
                break;
        }
    };
        
    self.eliminar = function(row, type, cod) {
        switch(type) {
            case 'S':
                modal.mensajeConfirmacion($scope, "Se eliminarán el cronograma y los horario asociados. ¿Desea continuar?", function () {
                    _.remove(self.tableSchedules, function (item) {
                        return row.cod === item.codSed;
                    });

                    _.remove(self.tableShifts, function (item) {
                        return row.cod === item.codSed;
                    });

                    _.remove(self.tablePlaces, function (item) {
                        return row === item;
                    });
                }, '400');
                break;
                
            case 'H':
                var objShift = self.tableShifts.filter(function(obj){
                    return obj.codSed === cod;
                })[0];
                
                if(objShift.shifts.length !== 1) {
                    _.remove(objShift.shifts, function (item) {
                        return row === item;
                    });                    
                } else
                    modal.mensaje("MENSAJE", "Solo hay un horario disponible para esta sede; por lo tanto no se puede eliminar");
                
                break;
        }
    };
            
    self.aceptar = function () {
        var state_S = true;
        var state_H = true;
        var i = 0;
        var placesFix = [];
        
        for (; i < self.tableSchedules.length && state_S; i++) {
            var objSede = self.tablePlaces[i];
            if(self.tableSchedules[i].fecIni === '' || self.tableSchedules[i].fecFin === '') {
                state_S = false;
                break;
            } else {
                objSede.cro = self.tableSchedules[i];
                state_H = true;
                
                if(self.tableShifts[i].shifts[0].dia === '' || self.tableShifts[i].shifts[0].turIni === '' || self.tableShifts[i].shifts[0].turFin === '') {
                    state_H = false;
                    state_S = false;
                    break;
                } else {
                    objSede.hor = self.tableShifts[i];
                    placesFix.push(objSede);
                }
            }
        }

        if(!state_S) {
            var message = '';
            if(!state_H) {
                message = (self.course.tip === 'Presencial')?
                                    "El primer horario de la sede " + (i+1) + " no se ha completado correctamente":
                                    "No se ha completado el horario de la capacitación";
                modal.mensaje("ERROR", message);
            } else {
                message = (self.course.tip === 'Presencial')?
                                    "El cronograma correspondiente a la sede " + (i+1) + " no se ha completado correctamente":
                                    "No se ha completado el cronograma de la capacitación";
                modal.mensaje("ERROR", message);
            }
        } else {
            if($scope.myForm.type.$error.required || $scope.myForm.name.$error.required || $scope.myForm.organized.$error.required || $scope.myForm.authorized.$error.required)
                modal.mensaje("ERROR", "Existen campos requeridos que no han sido completados");
            else {
                self.course.sed = placesFix;
                self.course.usuMod = $rootScope.usuMaster.usuario.usuarioID;

                var request = crud.crearRequest('cursos', 1, 'registrarCurso');
                request.setData(self.course);
                
                crud.insertar('/curso_capacitacion', request, function (success) {
                    if (success.response === 'OK') {
                        var curso = success.data;
                        curso.organizacion = self.course.organizacion;
                        $uibModalInstance.close(curso);
                    } else if (success.response === 'BAD')
                        modal.mensaje("ERROR", success.responseMsg);                        
                }, function (errorTraining) {
                    modal.mensaje("MENSAJE", errorTraining.responseMsg);
                });
            }            
        }
    };
    
    self.salir = function(){
        $uibModalInstance.dismiss('cancel');
    };   
}]);

mcApp.controller('goalProfileCtrl',['$uibModalInstance', 'crud', 'modal', '$scope', 'state', function($uibModalInstance, crud, modal, state) {
    var self = this;
    self.state = state.$resolve.state;
    self.title = (self.state)?"DEFINIR OBJETIVOS DE LA CAPACITACION":"DEFINIR PERFILES DEL CAPACITADOR";
    self.mode = "Nuevo";
        
    self.listarOrganizaciones = function() {
        var request = crud.crearRequest('cursos', 1, 'listarOrganizaciones');
        request.setData({opt: 0});
        
        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                self.fuentes = success.data;
                self.cambiarModo();
            } else
                modal.mensaje("MENSAJE", "No existen organizaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las organizaciones " + error.responseMsg);
        });
    };
    
    self.cargarAnos = function() {
        self.limpiar('A');
        var request = crud.crearRequest('cursos', 1, 'listarCursos');

        if(self.state)
            request.setData({opt: 2, org: self.objetivo.fuente});
        else
            request.setData({opt: 2, org: self.perfil.fuente});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                self.anos = success.data;
            } else
                modal.mensaje("MENSAJE", "No existen años para listar");            
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los años de las capacitaciones " + error.responseMsg);
        });
    };
    
    self.cargarCapacitaciones = function () {
        self.limpiar('C');
        var request = crud.crearRequest('cursos', 1, 'listarCursos');

        if (self.state)
            request.setData({opt: 3, org: self.objetivo.fuente, ano: self.objetivo.ano});
        else
            request.setData({opt: 3, org: self.perfil.fuente, ano: self.perfil.ano});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                self.capacitaciones = success.data;
            } else
                modal.mensaje("MENSAJE", "No existen capacitaciones para listar");          
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las capacitaciones " + error.responseMsg);
        });
    };
    
    self.buscarObjetivos = function () {
        var request = crud.crearRequest('cursos', 1, 'listarObjetivos');
        request.setData({cod: self.objetivo.cap});

        crud.listar('/curso_capacitacion', request, function (success) {
            if (!angular.equals(success.data, {})) {
                self.generales = success.data.generales;
                self.especificos = success.data.especificos;
                self.metas = success.data.metas;
                self.genState = true;
            } else {
                modal.mensaje("MENSAJE", "La capacitación seleccionada no tiene objetivos definidos");
            }
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los objetivos " + error.responseMsg);
        });
    };

    self.buscarPerfiles = function () {
        var request = crud.crearRequest('cursos', 1, 'listarPerfiles');
        request.setData({cod: self.perfil.cap});

        crud.listar('/curso_capacitacion', request, function (success) {
            if (!angular.equals(success.data, {})) {
                self.perfiles = success.data.perfiles;
                self.funciones = success.data.funciones;
                self.requisitos = success.data.requisitos;
                self.genState = true;
            } else {
                modal.mensaje("MENSAJE", "La capacitación seleccionada no tiene perfiles definidos");
            }
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los perfiles " + error.responseMsg);
        });
    };
        
    self.cambiarModo = function() {
        if(self.state) {
            self.objetivo = {};
            self.generales = [];
            self.especificos = [];
            self.metas = [];
        } else {
            self.perfil = {};
            self.perfiles = [];
            self.funciones = [];
            self.requisitos = [];
        }
            
        if(self.mode === 'Busqueda') {
            self.modeChange = "Busqueda";
            self.genState = true;
            self.mode = "Nuevo";
        } else {            
            self.modeChange = "Nuevo";
            self.genState = false;
            self.mode = "Busqueda";
        }
    };
        
    self.agregar = function(type) {
        if(self.state) {
            switch(type) {
                case 'G':
                    self.generales.push(angular.copy(self.objetivo.general));
                    self.objetivo.general = {cod: self.objetivo.general.cod + 1, des: '', tip: 'G'};
                    break;

                case 'E':
                    self.especificos.push(angular.copy(self.objetivo.especifico));
                    self.objetivo.especifico = {cod: self.objetivo.especifico.cod + 1, des: '', tip: 'E'};
                    break;

                case 'M':
                    self.metas.push(angular.copy(self.objetivo.meta));
                    self.objetivo.meta = {cod: self.objetivo.meta.cod + 1, des: '', tip: 'M'};
                    break;
            }
        } else {
            switch(type) {
                case 'P':
                    self.perfiles.push(angular.copy(self.perfil.perfil));
                    self.perfil.perfil = {cod: self.perfil.perfil.cod + 1, des: '', tip: 'P'};
                    break;

                case 'F':
                    self.funciones.push(angular.copy(self.perfil.funcion));
                    self.perfil.funcion = {cod: self.perfil.funcion.cod + 1, des: '', tip: 'F'};
                    break;

                case 'R':
                    self.requisitos.push(angular.copy(self.perfil.requisito));
                    self.perfil.requisito = {cod: self.perfil.requisito.cod + 1, des: '', tip: 'R'};
                    break;
            }
        }        
    };
    
    self.cancelar = function(row, rowForm, type) {
        var originalRow = self.buscar(row, rowForm, type);
        angular.extend(row, originalRow);        
    };
    
    self.buscar = function(row, rowForm, type){
        row.isEditing = false;
        rowForm.$setPristine();
        
        if(self.state) {
            switch(type) {
                case 'G':                    
                    return self.generales.filter(function(r) {
                        return r.cod === row.cod;
                    })[0];
                    break;

                case 'E':
                    return self.especificos.filter(function(r) {
                        return r.cod === row.cod;
                    })[0];
                    break;

                case 'M':
                    return self.metas.filter(function(r) {
                        return r.cod === row.cod;
                    })[0];
                    break;
            }
        } else {
            switch(type) {
                case 'P':
                    return self.perfiles.filter(function(r) {
                        return r.cod === row.cod;
                    })[0];
                    break;

                case 'F':
                    return self.funciones.filter(function(r) {
                        return r.cod === row.cod;
                    })[0];
                    break;

                case 'R':
                    return self.requisitos.filter(function(r) {
                        return r.cod === row.cod;
                    })[0];
                    break;
            }
        }
    };

    self.guardar = function(row, rowForm, type) {
        var originalRow = self.buscar(row, rowForm, type);
        angular.extend(originalRow, row);
    };
        
    self.eliminar = function(row, type) {
        if(self.state) {
            switch(type) {
                case 'G':
                    _.remove(self.generales, function (item) {
                        return row.cod === item.cod;
                    });
                    break;

                case 'E':
                    _.remove(self.especificos, function (item) {
                        return row.cod === item.cod;
                    });
                    break;

                case 'M':
                    _.remove(self.metas, function (item) {
                        return row.cod === item.cod;
                    });
                    break;
            }
        } else {
            switch(type) {
                case 'P':
                    _.remove(self.perfiles, function (item) {
                        return row.cod === item.cod;
                    });
                    break;

                case 'F':
                    _.remove(self.funciones, function (item) {
                        return row.cod === item.cod;
                    });
                    break;

                case 'R':
                    _.remove(self.requisitos, function (item) {
                        return row.cod === item.cod;
                    });
                    break;
            }
        }
    };
    
    self.limpiar = function(opt) {
        switch(opt) {
            case 'A':
                self.anos = [];
                self.capacitaciones = [];
                self.genState = false;
                
                if(self.state) {
                    self.objetivo.ano = '';
                    self.objetivo.cap = '';
                } else {
                    self.perfil.ano = '';
                    self.perfil.cap = '';
                }
                break;
                
            case 'C':
                self.capacitaciones = [];
                self.genState = false;
                
                if(self.state)
                    self.objetivo.cap = '';
                else
                    self.perfil.cap = '';
                
                break;
        }        
    };
        
    self.aceptar = function() {        
        var array = [];
        
        if(self.state) {
            if(self.mode === 'Busqueda') {
                self.generales.forEach(function (obj) {if (obj.sel) array.push(obj.id);});
                self.especificos.forEach(function (obj) {if (obj.sel) array.push(obj.id);});
                self.metas.forEach(function (obj) {if (obj.sel) array.push(obj.id);});
            } else {
                self.generales.forEach(function (obj) {array.push(obj);});
                self.especificos.forEach(function (obj) {array.push(obj);});
                self.metas.forEach(function (obj) {array.push(obj);});
            }
        } else {
            if(self.mode === 'Busqueda') {
                self.perfiles.forEach(function (obj) {if (obj.sel) array.push(obj.id);});
                self.funciones.forEach(function (obj) {if (obj.sel) array.push(obj.id);});
                self.requisitos.forEach(function (obj) {if (obj.sel) array.push(obj.id);});
            } else {
                self.perfiles.forEach(function (obj) {array.push(obj);});
                self.funciones.forEach(function (obj) {array.push(obj);});
                self.requisitos.forEach(function (obj) {array.push(obj);});
            }
        }
        
        var object = {};
        
        if (array.length > 0) {
            object.modo = self.mode;
            object.array = array;
        }
        
        $uibModalInstance.close(object);
    };
    
    self.salir = function() {
        $uibModalInstance.dismiss('cancel');
    };
}]);

mcApp.controller('coursePh3Ctrl',['$uibModalInstance', '$rootScope', 'crud', 'modal', 'NgTableParams', '$http', '$scope', '$uibModal', 'course', function($uibModalInstance, $rootScope, crud, modal, NgTableParams, $http, $scope, $uibModal, course) {
    var self = this;
    self.status = {
        close: true,
        statusE: true,
        statusC: false
    }; 
    self.current = course.current;
    self.tipoEvaluacion = [{id: 'P', nom: "Evaluación Parcial"}, {id: 'E', nom: "Encuesta"}];
    self.criterios = [  {id: 'P', nom: "Evaluaciones Parciales", est: 'N'}, 
                        {id: 'C', nom: "Evaluaciones Continuas", est: 'N', notMin: 11},
                        {id: 'H', nom: "Tareas", est: 'N', notMin: 11},
                        {id: 'A', nom: "Asistencias", est: 'N'},
                        {id: 'T', nom: "Tardanzas (Asistencia)", est: 'N'},
                        {id: 'F', nom: "Faltas (Asistencia)", est: 'N', mot: []}                        
                    ];
    self.estCri = false;
    self.tipoConfiguracion = [{id: 'P', nom: "Porcentual", max: 100}, {id: 'N', nom: "Nominal"}]
    self.tests = [];
    
    if(self.current.tip === 'Online') {
        self.dateOptions = {
            maxDate: new Date(self.current.sedes[0].fin),
            minDate: new Date(self.current.sedes[0].ini),
            initDate: new Date(self.current.sedes[0].ini),
            startingDay: 1
        };
    }
    
    self.showInfo = function(){
        var obj = self.current.sedes.filter(function (obj) {
            return obj.cod == self.test.sed;
        })[0];

        modal.mensaje("MENSAJE", "Sede seleccionada: PROVINCIA = " + obj.pro + ", DISTRITO = " + obj.dis);
        self.dateOptions = {
            maxDate: new Date(obj.fin),
            minDate: new Date(obj.ini),
            initDate: new Date(obj.ini),
            startingDay: 1
        };
        self.test.est = false;
    }; 
    
    self.agregarEvaluacion = function() {
        if (self.test.fin.getTime() >= self.test.ini.getTime()) {
            if(self.current.tip === 'Online')
                self.test.sed = self.current.sedes[0].cod;
            
            self.test.dateOptions = self.dateOptions;
            self.tests.push(angular.copy(self.test));
            self.test = {
                cod: self.test.cod + 1,
                ini: "",
                fin: "",
                tip: "",
                nom: "",
                est: (self.current.tip === 'Presencial')
            };
        } else
            modal.mensaje("ERROR", "No existe coherencia en las fechas registradas");
    };
    
    self.configurarCriterio = function(index) {
        self.criteria = angular.copy(self.criterios[index]);
        self.criteria.idx = index;
        self.estCri = true;
    };
    
    self.cambiarConf = function() {
        self.criteria.eva = self.tipoConfiguracion.filter(function(obj){
            return obj.id === self.criteria.evaCod;
        })[0];
        
        self.criteria.estDis = false;
        
        if(self.criteria.id === 'T' || self.criteria.id === 'F') {
            self.criteria.title = "Máximo";
        } else
            self.criteria.title = "Mínimo";
    };
    
    self.getMessage = function(){
        switch(self.criteria.id) {
            case 'P': return ("El participante debe aprobar como mínimo " + self.criteria.val + ((self.criteria.evaCod === 'P')?"% de sus evaluaciones parciales":" evaluaciones parciales"));
            case 'C': return ("El participante debe aprobar como mínimo " + self.criteria.val + ((self.criteria.evaCod === 'P')?"% de sus evaluaciones continuas":" evaluaciones continuas"));
            case 'H': return ("El participante debe aprobar como mínimo " + self.criteria.val + ((self.criteria.evaCod === 'P')?"% de sus tareas":" tareas"));             
            case 'A': return ("El participante debe tener como mínimo " + self.criteria.val + ((self.criteria.evaCod === 'P')?"% de asistencias (presente)":" asistencias (presente)"));
            case 'T': return ("El participante debe tener a lo mucho " + self.criteria.val + ((self.criteria.evaCod === 'P')?"% de asistencias (tarde)":" asistencias (tarde)"));
            case 'F': return ("El participante debe tener a lo mucho " + self.criteria.val + ((self.criteria.evaCod === 'P')?"% de asistencias (falta)":" asistencias (falta)"));                
        }  
    };
    
    self.guardarConf = function() {
        self.criteria.est = 'A';
        angular.extend(self.criterios[self.criteria.idx], self.criteria);
        self.estCri = false;
    };
        
    self.agregarMotivos = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'motive.html',
            controller: 'motiveCtrl',
            controllerAs: 'motive',
            size: 'sm',
            resolve: {
                motive: function() {
                    return {
                        mots: self.criteria.mot
                    }
                }
            }
        });
        
        modalInstance.result.then(function(mots) {
            self.criteria.mot = mots;
        }, function(){});
    };
        
    self.cancelar = function(row, rowForm) {
        var originalRow = self.buscar(row, rowForm);
        angular.extend(row, originalRow);
    };
    
    self.buscar = function(row, rowForm){
        row.isEditing = false;
        rowForm.$setPristine();
        return self.tests.filter(function(r) {
            return r.cod === row.cod;
        })[0];
    };

    self.guardar = function(row, rowForm) {
        if (row.fin.getTime() >= row.ini.getTime()) {
            var originalRow = self.buscar(row, rowForm);
            angular.extend(originalRow, row);
        } else
            modal.mensaje("ERROR", "No existe coherencia en las fechas registradas");
    };
    
    self.eliminar = function(row) {
        _.remove(self.tests, function (r) {
            return row.cod === r.cod;
        });
    };
    
    self.aceptar = function() {
        if (self.tests.length > 0) {
            var request = crud.crearRequest('cursos', 1, 'registrarEvaluacion');
            request.setData({evaluaciones: self.tests});

            crud.insertar('/curso_capacitacion', request, function (success) {
            }, function (error) {
                modal.mensaje("MENSAJE", error.responseMsg);
            });
        }
        
        var request = crud.crearRequest('cursos', 1, 'registrarCriterio');
        request.setData({cod: self.current.cod, criterios: self.criterios});

        crud.insertar('/curso_capacitacion', request, function (success) {
            $uibModalInstance.close();
        }, function (error) {
            modal.mensaje("MENSAJE", error.responseMsg);
        });
    };
    
    self.salir = function() {
        $uibModalInstance.dismiss('cancel');
    };
}]);

mcApp.controller('motiveCtrl',['$uibModalInstance', 'motive', function($uibModalInstance, motive) {
    var self = this;
    self.motives = motive.mots;

    self.agregarMotivo = function() {
        self.motives.push(angular.copy(self.mot));
        self.mot = {cod: self.mot.cod + 1, mot: '', jus: ''};
    };
    
    self.aceptar = function() {
        $uibModalInstance.close(self.motives);
    };
    
    self.salir = function() {
        $uibModalInstance.dismiss('cancel');
    };
    
    self.cancelar = function(row, rowForm) {
        var originalRow = self.buscar(row, rowForm);
        angular.extend(row, originalRow);
    };
    
    self.buscar = function(row, rowForm){
        row.isEditing = false;
        rowForm.$setPristine();
        return self.motives.filter(function(r) {
            return r.cod === row.cod;
        })[0];
    };

    self.guardar = function(row, rowForm) {
        var originalRow = self.buscar(row, rowForm);
        angular.extend(originalRow, row);
    };
    
    self.eliminar = function(row) {
        _.remove(self.motives, function (r) {
            return row.cod === r.cod;
        });
    };
}]);

mcApp.controller('certificacionCtrl',['$rootScope', 'crud', 'modal', 'variablesTest', '$location', '$uibModal', 'NgTableParams', '$scope', '$window', function($rootScope, crud, modal, variablesTest, $location, $uibModal, NgTableParams, $scope, window) {
    var self = this;
    
    self.verify = function() {
        if($rootScope.dataCap === undefined) {
            $location.url('/cursoCapacitacion');
        } else {
            var request = crud.crearRequest('cursos', 1, 'listarOrganizaciones');
            request.setData({opt: 2, codCap: $rootScope.dataCap.capCod});

            crud.listar('/curso_capacitacion', request, function (success) {
                self.codCap = $rootScope.dataCap.capCod;
                self.capOrg = success.data.capOrg;
                self.capAut = success.data.capAut;
                self.capNom = $rootScope.dataCap.capNom;
                self.tipCap = $rootScope.dataCap.capTip;
                self.capSer = $rootScope.dataCap.capSer;
                self.capSed = $rootScope.dataCap.capSed;
                
                if(self.tipCap === 'Online') {
                    self.sedCod = self.capSed[0].id;
                    self.cargarEvaluacion();
                }
            }, function (error) {
                modal.mensaje("MENSAJE", "No se pudo listar las organizaciones " + error.responseMsg);
            });
        }
    };
    
    self.verify();  
    
    var settingCompetitor = {counts: []};
    self.tablaParticipantes = new NgTableParams({count: 10}, settingCompetitor);
    
    self.cargarEvaluacion = function() {
        self.state = false;
        var request = crud.crearRequest('cursos', 1, 'calcularCriterios');
        request.setData({codCap:self.codCap, sedCod: self.sedCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                self.cri = success.data.crit;
                settingCompetitor.dataset = success.data.part;
                iniciarPosiciones(settingCompetitor.dataset);
                self.tablaParticipantes.settings(settingCompetitor);
                self.state = true;
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las organizaciones " + error.responseMsg);
        });
    };
    
    self.seleccionarTodos = function() {
        self.tablaParticipantes.settings().dataset.forEach(function(obj) {
            if(obj.genEst) {
                obj.sel = true;
            }
        });
    };
    
    self.generarCertificados = function() {
        var parts = self.tablaParticipantes.settings().dataset.filter(function(obj) {
            return obj.sel === true;
        });
        
        if(parts.length > 0) {
            var request = crud.crearRequest('cursos', 1, 'generarCertificados');
            request.setData({capCod: self.codCap, parts: parts, sedCod: self.sedCod});

            crud.listar('/curso_capacitacion', request, function (success) {
                window.open(success.data.route);
            }, function (error) {
                modal.mensaje("MENSAJE", "No se pudo buscar al capacitador " + error.responseMsg);
            });
        } else
            modal.mensaje("MENSAJE", "No se ha seleccionado a ningún participante");
    };
}]);

mcApp.controller('reportCtrl',['$uibModalInstance', 'crud', 'modal', '$window', function($uibModalInstance, crud, modal, window) {
    var self = this;    
    self.estTip = false;
    self.state = {tipCod: '', tipEst: false, modCod: '', modEst: false, capCod: '', capTip: '', capEst: false, sedCod: '', sedEst: false, docDni: '', docCod: '', docNom: '', docEst: false};
    self.types = [{cod: 'A', nom: 'Asistencia'},{cod: 'R', nom: 'Rendimiento'}];
    self.modes = [{cod: 'I', nom: 'Individual'},{cod: 'G', nom: 'Grupal'}];
    
    self.listarModos = function() {
        self.limpiar('T');
        self.state.tipEst = true;
    };
    
    self.listarCapacitaciones = function(){
        self.limpiar('M');
        var request = crud.crearRequest('cursos', 1, 'listarCursos');
        request.setData({opt: 8});

        crud.listar('/curso_capacitacion', request, function (success) {
            self.courses = success.data;
            self.state.modEst = true;
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los cursos de capacitación " + error.responseMsg);
        });
    };
    
    self.listarSedes = function() {
        self.limpiar('C');
        
        self.state.capTip = self.courses.filter(function(r){
            return r.cod === parseInt(self.state.capCod);
        })[0].tip;
        
        var request = crud.crearRequest('cursos', 1, 'listarSedes');
        request.setData({opt: 1, codCap: self.state.capCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                self.state.capEst = true;
                if(self.state.capTip === 'Presencial') {
                    self.places = success.data;
                } else {
                    self.state.sedCod = success.data[0].cod;
                    self.verificar();
                }
            } else
                modal.mensaje("MENSAJE", "No existen capacitaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las capacitaciones " + error.responseMsg);
        });
    };
    
    self.verificar = function() {
        self.limpiar('S');
        self.state.sedEst = true;
        
        if(self.state.modCod === 'G') {
            self.state.docEst = true;
        }
    };
    
    self.completarDatos = function() {
        if(self.state.docDni.length === 8) {            
            var request = crud.crearRequest('cursos', 1, 'listarPersonas');
            request.setData({opt: 1, dni: self.state.docDni, codSed: self.state.sedCod});

            crud.listar('/curso_capacitacion', request, function (success) {
                if (angular.equals(success.data, {}))
                    self.state.docEst = false;
                else {
                    self.state.docCod = success.data.cod;
                    self.state.docNom = success.data.pat + " " + success.data.mat + " " + success.data.nom;                    
                    self.state.docEst = true;
                }
            }, function (error) {
                modal.mensaje("MENSAJE", "No se pudo buscar al capacitador " + error.responseMsg);
            });
        } else {
            self.state.docEst = false;
            self.state.docNom = '';
        }
    };
    
    self.aceptar = function() {
        var request = crud.crearRequest('cursos', 1, 'generarReportes');
        request.setData({docCod: self.state.docCod, 
                         sedCod: self.state.sedCod, 
                         capCod: self.state.capCod,
                         tipCod: self.state.tipCod,
                         modCod: self.state.modCod
                     });

        crud.listar('/curso_capacitacion', request, function (success) {
            window.open(success.data.report);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo buscar al capacitador " + error.responseMsg);
        });
    };
    
    self.salir = function() {
         $uibModalInstance.dismiss('cancel');
    };
    
    self.limpiar = function(opt) {
        switch(opt) {            
            case 'T':
                self.state.tipEst = false;
                self.state.modCod = '';
                self.state.modEst = false;
                self.state.capCod = '';
                self.state.capTip = '';
                self.state.capEst = false;
                self.state.sedCod = '';
                self.state.sedEst = false;
                self.state.docDni = '';
                self.state.docCod = '';
                self.state.docNom = '';
                self.state.docEst = false;
                break;
                
            case 'M':
                self.state.modEst = false;
                self.state.capCod = '';
                self.state.capTip = '';
                self.state.capEst = false;
                self.state.sedCod = '';
                self.state.sedEst = false;
                self.state.docDni = '';
                self.state.docCod = '';
                self.state.docNom = '';
                self.state.docEst = false;
                break;
                
            case 'C':
                self.state.capEst = false;
                self.state.sedCod = '';
                self.state.sedEst = false;
                self.state.docDni = '';
                self.state.docCod = '';
                self.state.docNom = '';
                self.state.docEst = false;
                break;
                
            case 'S':
                self.state.sedEst = false;
                self.state.docDni = '';
                self.state.docCod = '';
                self.state.docNom = '';
                self.state.docEst = false;
                break;
        }
    };
}]);

mcApp.controller('surveyCtrl',['$uibModalInstance', 'crud', 'modal', '$window', '$scope', function($uibModalInstance, crud, modal, window, $scope) {
    var self = this;        
    self.state = {capCod: '', capTip: '', capEst: false, sedCod: '', sedEst: false, surCod: '', surEst: false};
    
    self.listarCapacitaciones = function(){
        self.limpiar('C');
        var request = crud.crearRequest('cursos', 1, 'listarCursos');
        request.setData({opt: 8});

        crud.listar('/curso_capacitacion', request, function (success) {
            self.courses = success.data;
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar los cursos de capacitación " + error.responseMsg);
        });
    };
    
    self.listarSedes = function() {
        self.limpiar('L');
        
        self.state.capTip = self.courses.filter(function(c){
            return c.cod === parseInt(self.state.capCod);
        })[0].tip;
        
        var request = crud.crearRequest('cursos', 1, 'listarSedes');
        request.setData({opt: 1, codCap: self.state.capCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                self.state.capEst = true;
                if(self.state.capTip === 'Presencial') {
                    self.places = success.data;
                } else {
                    self.state.sedCod = success.data[0].cod;
                    self.listarEncuestas();
                }
            } else
                modal.mensaje("MENSAJE", "No existen capacitaciones para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las capacitaciones " + error.responseMsg);
        });
    };
    
    self.listarEncuestas = function() {
        self.limpiar('S');
        var request = crud.crearRequest('cursos', 1, 'listarEvaluaciones');
        request.setData({opt: 4, sedCod: self.state.sedCod});

        crud.listar('/curso_capacitacion', request, function (success) {
            if(success.data.length > 0) {
                self.state.sedEst = true;
                self.surveys = success.data;
            } else
                modal.mensaje("MENSAJE", "No existen encuestas para listar");
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las capacitaciones " + error.responseMsg);
        });
    };
    
    self.listarPreguntas = function() {
        self.limpiar('E');
        var request = crud.crearRequest('cursos', 1, 'listarEvaluaciones');
        request.setData({opt: 5, evaCod: self.state.surCod});
        
        crud.listar('/curso_capacitacion', request, function (success) {
            if (success.response === 'OK') {
                self.questions = success.data;
                self.state.surEst = true;
            } else if (success.response === 'BAD')
                modal.mensaje("ERROR", success.responseMsg);
        }, function (error) {
            modal.mensaje("MENSAJE", "No se pudo listar las preguntas " + error.responseMsg);
        });
    }
    
    self.limpiar = function(type) {
        switch(type) {
            case 'C':
                self.state.capCod = '';
                self.state.capTip = '';
                self.state.capEst = false;
                self.state.sedCod = '';
                self.state.sedEst = false;
                self.state.surCod = '';
                self.state.surEst = false;                
                break;
                
            case 'L':
                self.state.capTip = '';
                self.state.capEst = false;
                self.state.sedCod = '';
                self.state.sedEst = false;
                self.state.surCod = '';
                self.state.surEst = false;  
                break;
                
            case 'S':
                self.state.sedEst = false;
                self.state.surCod = '';
                self.state.surEst = false;
                break;
                
            case 'E':
                self.state.surEst = false;
                break;
        }
    };
    
    self.salir = function() {
         $uibModalInstance.dismiss('cancel');
    };
}]);