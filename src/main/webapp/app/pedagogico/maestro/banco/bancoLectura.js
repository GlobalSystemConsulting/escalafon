/**
 * Created by Administrador on 20/12/2016.
 */
angular.module('app')
    .config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/banco/detalle/:est',{
            templateUrl:'pedagogico/maestro/banco/detalle_banco.html',
            controller:'detalleBancoCtrl',
            controllerAs:'ctrl'
        })
    }])
    .controller('bancoLectura',['$log','$location','$rootScope','NgTableParams','modal','UtilAppServices','crud',function($log,$location,$rootScope,NgTableParams,modal,util,crud){
        var self = this;
        self.lecturas = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        listarGrados();
        function listarGrados(){
            var request = crud.crearRequest('anecdotario',1,'listarGrados');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, doc:$rootScope.usuMaster.usuario.usuarioID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.niveles = response.data.niveles;
                    //self.grados = response.data.grados;
                    self.areas = response.data.areas;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarGrados =  function(nivel){
            self.grados = nivel.grados;
        }
        self.mostrarSecciones =  function(grado){
            self.secciones = grado.secciones;

        }
        self.buscarLecturas = function(){
            var request = crud.crearRequest('banco_lectura',1,'listarLecturas');
            request.setData({
                doc:$rootScope.usuMaster.usuario.usuarioID,
                org:$rootScope.usuMaster.organizacion.organizacionID,
                gra:self.grado.id,
                are:self.area.cod,
                sec:self.seccion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.lecturas.settings().dataset = response.data;
                    self.lecturas.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.registrarLectura = function(){
            var resolve = {
                data : function () {
                    return {
                        edit: false,
                        gra:self.grado.id,
                        are:self.area.cod,
                        sec:self.seccion
                    }
                }
            }
            var modalInstance = util.openModal('nueva_lectura.html','registrarLecturaCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(lectura){
                self.lecturas.settings().dataset.push(lectura);
                self.lecturas.reload();
            },function(cdata){});
        }
        self.editarLectura = function(row){
            var resolve = {
                data : function () {
                    return {
                        edit: true,
                        lec: angular.copy(row)
                    }
                }
            }
            var modalInstance = util.openModal('nueva_lectura.html','registrarLecturaCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(lectura){
                angular.extend(row,lectura);
            },function(cdata){});
        }
        self.eliminarLectura = function($event,row){
            util.openDialog('Eliminar Lectura ','¿Seguro que desea eliminar la lectura?',$event,
                function (response) {
                    var request = crud.crearRequest('banco_lectura',1,'eliminarLectura');
                    request.setData({id:row.id});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(self.lecturas.settings().dataset,function(item){
                                return row.id === item.id;
                            });
                            self.lecturas.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }

    }])
    .controller('registrarLecturaCtrl',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        var self = this;
        self.abrirCalendar = false;
        self.openCalendar = function () {self.abrirCalendar = true;}
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2040, 5, 22),
            minDate: new Date(1980, 0, 1),
            startingDay: 1
        };
        if(data.edit){
            self.lec = data.lec;
            self.lec.fpub = new Date(data.lec.fpub);
        }
        self.titulo = !data.edit ? 'Nueva Lectura' : 'Editar Lectura';

        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }

        self.save = !data.edit ? guardar : editar;
        function editar(){
            if(self.lec.fpub != undefined)
                self.lec.fpub = self.lec.fpub.getTime();
            var request = crud.crearRequest('banco_lectura',1,'editarLectura');
            request.setData(self.lec);
            crud.actualizar('/maestro',request,function(response){
                if(response.responseSta){
                    angular.extend(self.lec,response.data);
                    $uibModalInstance.close(self.lec);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
        function guardar() {
            self.lec.org = $rootScope.usuMaster.organizacion.organizacionID;
            self.lec.doc = $rootScope.usuMaster.usuario.usuarioID;
            self.lec.grad = data.gra;
            self.lec.are = data.are;
            self.lec.sec = data.sec;
            if(self.lec.fpub != undefined)
                self.lec.fpub = self.lec.fpub.getTime();


            var request = crud.crearRequest('banco_lectura',1,'registrarLectura');
            request.setData(self.lec);
            crud.insertar('/maestro',request,function(response){
                if(response.responseSta){
                    angular.extend(self.lec,response.data);
                    self.lec.cre = new Date();
                    $uibModalInstance.close(self.lec);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
    }]);