/**
 * Created by Administrador on 20/12/2016.
 */
angular.module('app')
    .config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/acomp_adm/docente/:doc',{
            templateUrl:'pedagogico/maestro/acompan/acomp_admin_det.html',
            controller:'acompAdmDetCtrl',
            controllerAs:'ctrl'
        })
    }])
    .controller('acompAdm',['$log','$location','$rootScope','NgTableParams','modal','UtilAppServices','crud','$q',function($log,$location,$rootScope,NgTableParams,modal,util,crud,$q){
        var self = this;
        self.docentes = new NgTableParams({count:20},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        listarUGELES();
        function listarUGELES(){
            $log.log('ugel',$rootScope.usuMaster.organizacion);
            var request = crud.crearRequest('acomp',1,'listarUGELES');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.ugeles = response.data;
                    //self.grados = response.data.grados;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarIes =  function(ugel){
            var request = crud.crearRequest('acomp',1,'listarOrganizaciones');
            request.setData({org:ugel.id});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.ies = response.data;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });

        }
        self.listarDocentesIE = function(ie){
            var request = crud.crearRequest('acomp',1,'listarDocentesIE');
            request.setData({org:ie.id});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.docentes.settings().dataset = response.data;
                    self.docentes.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.verAcompanamientosDocente = function(docente){
            docente.org = self.ie.id;
            $location.url('/acomp_adm/docente/'+btoa(JSON.stringify(docente)));
        }

    }])
    .controller('acompAdmDetCtrl',['$log','$routeParams','NgTableParams','modal','crud','UtilAppServices',function($log,$routeParams,NgTableParams,modal,crud,util){
        var self = this;
        self.doc = JSON.parse(atob($routeParams.doc));
        var resultadosArr = [{cod:0,nom:'LOGRADO',abr:'LO'},{cod:1,nom:'EN PROCESO',abr:'EP'},{cod:2,nom:'NO LOGRADO',abr:'NL'}];
        self.acompanamientos = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        listarAcompanamientos();
        function listarAcompanamientos(){
            var request = crud.crearRequest('acomp',1,'listarAcompDocente');
            request.setData({doc:self.doc.id});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.acompanamientos.settings().dataset = response.data;
                    self.acompanamientos.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
        self.eliminaAcompanamiento = function($event,row){
            util.openDialog('Eliminar Acompañamiento ','¿Seguro que desea eliminar el acompañamiento?',$event,
                function (response) {
                    var request = crud.crearRequest('acomp',1,'eliminarAcomp');
                    request.setData({id:row.id});
                    crud.eliminar('/maestro',request,function(response){
                        if(response.responseSta){
                            _.remove(self.acompanamientos.settings().dataset,function(item){
                                return row.id === item.id;
                            });
                            self.acompanamientos.reload();
                        }else {
                            modal.mensaje("ERROR",response.responseMsg);
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
                },function () {
                    //modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.nuevoAcompanamiento = function(){
            var resolve = {
                data : function () {
                    return {
                        edit: false,
                        doc: self.doc,
                        results : resultadosArr
                    }
                }
            }
            var modalInstance = util.openModal('nuevo_acompanamiento.html','registrarAcompDocCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(acomp){
                self.acompanamientos.settings().dataset.push(acomp);
                self.acompanamientos.reload();
            },function(cdata){});
        }
        self.editarAcompanamiento = function(row){
            var resolve = {
                data : function () {
                    return {
                        edit: true,
                        doc: self.doc,
                        results : resultadosArr,
                        acomp: angular.copy(row)

                    }
                }
            }
            var modalInstance = util.openModal('nuevo_acompanamiento.html','registrarAcompDocCtrl','lg','ctrl',resolve);
            modalInstance.result.then(function(acomp){
                angular.extend(row,acomp);
            },function(cdata){});
        }
        self.showRes = function(res){
            var index = _.findIndex(resultadosArr,function(o){
                return o.cod == res;
            });
            if(index != -1) return resultadosArr[index].nom;
            return ""
        }
    }])
    .controller('registrarAcompDocCtrl',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        var self = this;
        self.results = data.results;
        self.abrirCalendar = false;
        self.openCalendar = function () {self.abrirCalendar = true;}
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2040, 5, 22),
            minDate: new Date(2016, 11, 22),
            startingDay: 1
        };
        self.titulo = !data.edit ? 'Nuevo Acopañamiento' : 'Editar Acompañamiento';
        if(data.edit){
            self.acomp = new Object();
            self.acomp.fec = new Date(data.acomp.fec);
            self.acomp.res = _.find(self.results,function(obj){
                return obj.cod === data.acomp.res
            });
        }
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }
        listarNiveles();
        function listarNiveles(){
            var request = crud.crearRequest('anecdotario',1,'listarGrados');
            request.setData({doc:data.doc.id,org:data.doc.org});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                  self.niveles = response.data.niveles;
                    if(data.edit){
                        self.acomp.nivel = _.find(self.niveles,function(obj){
                            return obj.id === data.acomp.nivel.id;
                        });
                        self.grados = self.acomp.nivel.grados;
                        self.acomp.grado = _.find(self.grados,function(obj){
                            return obj.id === data.acomp.grado.id;
                        });
                        self.secciones = self.acomp.grado.secciones;
                        self.acomp.seccion = _.find(self.secciones,function(obj){
                            return obj  === data.acomp.seccion;
                        });
                        self.mostrarAreas(self.acomp.seccion);
                    }
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.mostrarGrados =  function(nivel){
            self.grados = nivel.grados;

        }
        self.mostrarSecciones =  function(grado){
            self.secciones = grado.secciones;

        }
        self.mostrarAreas = function(seccion){
            var request = crud.crearRequest('acomp',1,'listarCursosDocente');
            request.setData({doc:data.doc.id,org:data.doc.org,gra:self.acomp.grado.id,secc:seccion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.areas = response.data;
                    if(data.edit){
                        self.acomp.area = _.find(self.areas,function(obj){
                            return obj.id === data.acomp.area.id;
                        });
                    }
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        var accionesSave = [];
        self.tablaCompromisos = new NgTableParams({count:5},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: !data.edit ? [] : data.acomp.comp
        });
        self.addAccion = function() {
            self.isSaving = true;
            self.tablaCompromisos.settings().dataset.push({
                cod: self.tablaCompromisos.settings().dataset.length + 1,
                nom: "",
                est:true,
                edit:true,
                save:true
            });

            self.tablaCompromisos.sorting({});
            self.tablaCompromisos.page(1);
            self.tablaCompromisos.reload();
        }
        self.eliminarAccion =  function(row){

            var currCod = row.cod;

            _.remove(self.tablaCompromisos.settings().dataset,function(o){
                return o.cod === row.cod;
            });
            _.remove(accionesSave,function(o){
                return o.cod === row.cod;
            });
            angular.forEach(self.tablaCompromisos.settings().dataset,function(obj,key){
                if(obj.cod >= currCod + 1){
                    obj.cod -= 1;
                    accionesSave[key].cod -= 1;
                }
            });
            self.tablaCompromisos.reload();
        }
        self.editarAccion = function(row){
            row.edit = true;
        }
        self.aceptarEdicionAccion = function(row){
            if(!row.des || row.des ===""){
                modal.mensaje("Error", "El campo no puede estar vacio");
                return;
            }
            if(row.save !== undefined && row.save){
                //angular.extend(row,response.data);
                row.des = row.des.toUpperCase();
                accionesSave.push(angular.copy(row));
                row.edit = false;
                row.save = false;
                self.isSaving = false;
                return;
            }else if(row.edit){
                var index = _.findIndex(accionesSave,function(o){
                    return row.cod === o.cod;
                });
                row.des = row.des.toUpperCase();
                row.edit = false;
                row.save = false;
                angular.extend(accionesSave[index],angular.copy(row));
            }
        }
        self.cancelarEdicionAccion = function(row){
            if(row.save !== undefined && row.save){
                _.remove(self.tablaCompromisos.settings().dataset,function(o){
                    return o.save;
                });
                self.tablaCompromisos.reload();
                self.isSaving = false;
                return;
            }

            var index = _.findIndex(accionesSave,function(o){
                return row.cod === o.cod;
            });
            angular.extend(row,accionesSave[index]);
            row.edit = false;
        }
        self.save = guardar;
        function guardar() {
            var dataSend ={
                doc:data.doc.id,
                org:data.doc.org,
                usu:$rootScope.usuMaster.usuario.usuarioID,
                niv:self.acomp.nivel.id,
                gra:self.acomp.grado.id,
                sec:self.acomp.seccion,
                are:self.acomp.area.id,
                res:self.acomp.res.cod,
                fec: self.acomp.fec != undefined? self.acomp.fec.getTime() : new Date().getTime(),
                comp : self.tablaCompromisos.settings().dataset
            }
            if(data.edit){
                dataSend.id = data.acomp.id;
                var request = crud.crearRequest('acomp',1,'editarAcomp');
                request.setData(dataSend);
                crud.actualizar('/maestro',request,function(response){
                    if(response.responseSta){
                        angular.extend(self.acomp,response.data);
                        $uibModalInstance.close(self.acomp);
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
            }
            else{
                var request = crud.crearRequest('acomp',1,'registrarAcomp');
                request.setData(dataSend);
                crud.insertar('/maestro',request,function(response){
                    if(response.responseSta){
                        angular.extend(self.acomp,response.data);
                        $uibModalInstance.close(self.acomp);
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
            }

        }
    }]);

