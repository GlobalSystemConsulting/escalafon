/**
 * Created by Administrador on 20/12/2016.
 */
angular.module('app')
    .controller('acompDocente',['$log','$location','$rootScope','NgTableParams','modal','UtilAppServices','crud','$q',function($log,$location,$rootScope,NgTableParams,modal,util,crud,$q){
        var self = this;
        var resultadosArr = [{cod:0,nom:'LOGRADO',abr:'LO'},{cod:1,nom:'EN PROCESO',abr:'EP'},{cod:2,nom:'NO LOGRADO',abr:'NL'}];
        self.acompanamientos = new NgTableParams({count:15},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        listarAcompanamientos();
        function listarAcompanamientos(){
            var request = crud.crearRequest('acomp',1,'listarAcompDocente');
            request.setData({doc:$rootScope.usuMaster.usuario.usuarioID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.acompanamientos.settings().dataset = response.data;
                    self.acompanamientos.reload();
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){modal.mensaje('ERROR','El servidor no responde');});
        }
        self.verDetalleAcompanamiento = function(row){
            var resolve = {
                data : function () {
                    return {
                        edit: true,
                        results : resultadosArr,
                        acomp: angular.copy(row)

                    }
                }
            }
            var modalInstance = util.openModal('detalle_acompanamiento.html','verDetalleAcompCtrl','lg','ctrl',resolve);
        }
        self.showRes = function(res){
            var index = _.findIndex(resultadosArr,function(o){
                return o.cod == res;
            });
            if(index != -1) return resultadosArr[index].nom;
            return ""
        }

    }])
    .controller('verDetalleAcompCtrl',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        var self = this;
        self.results = data.results;
        self.abrirCalendar = false;
        self.openCalendar = function () {self.abrirCalendar = true;}
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2040, 5, 22),
            minDate: new Date(2016, 11, 22),
            startingDay: 1
        };
        self.titulo = 'Detalle Acompañamiento';
        self.acomp = new Object();
        self.acomp.fec = new Date(data.acomp.fec);
        self.acomp.res = _.find(self.results,function(obj){
            return obj.cod === data.acomp.res
        });

        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }
        listarNiveles();
        function listarNiveles(){
            var request = crud.crearRequest('anecdotario',1,'listarGrados');
            request.setData({doc:$rootScope.usuMaster.usuario.usuarioID,org:$rootScope.usuMaster.organizacion.organizacionID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.niveles = response.data.niveles;
                    self.acomp.nivel = _.find(self.niveles,function(obj){
                        return obj.id === data.acomp.nivel.id;
                    });
                    self.grados = self.acomp.nivel.grados;
                    self.acomp.grado = _.find(self.grados,function(obj){
                        return obj.id === data.acomp.grado.id;
                    });
                    self.secciones = self.acomp.grado.secciones;
                    self.acomp.seccion = _.find(self.secciones,function(obj){
                        return obj  === data.acomp.seccion;
                    });
                    mostrarAreas(self.acomp.seccion);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        function mostrarAreas(seccion){
            var request = crud.crearRequest('acomp',1,'listarCursosDocente');
            request.setData({doc:$rootScope.usuMaster.usuario.usuarioID,org:$rootScope.usuMaster.organizacion.organizacionID,gra:self.acomp.grado.id,secc:seccion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.areas = response.data;
                    self.acomp.area = _.find(self.areas,function(obj){
                        return obj.id === data.acomp.area.id;
                    });
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.tablaCompromisos = new NgTableParams({count:5},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset: data.acomp.comp
        });
    }]);

