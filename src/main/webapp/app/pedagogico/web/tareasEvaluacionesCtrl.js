app.controller("tareasEvaluacionesCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    $scope.plan = {};
    $scope.jornadaSel = {};
    $scope.gradoSel = {};
    
    
    //tabla de tareas
    var paramsTarea = {count: 10};
    var settingTarea = { counts: []};
    $scope.tablaTarea = new NgTableParams(paramsTarea, settingTarea);
    
    $scope.estados = [{id:'N',title:"nuevo"},{id:'E',title:"enviado"},{id:'C',title:"calificado"}];
    
       
    $scope.calcularGrados = function(nivel){
        
        if($scope.plan ){
            $scope.grados = buscarObjetos($scope.plan.grados,"nivelID",nivel.nivelID);
            $scope.gradoSel = '';
            $scope.seccionSel = '';
        }
    
    };
    $scope.calcularSecciones = function(grado){
        
        if(grado){
            $scope.seccionSel = '';
            grado.secciones = [];
            var l = 65;        
            for(var i=0;i<grado.meta;i++)
                grado.secciones.push({seccionID:String.fromCharCode(l+i)});
            
            grado.areas = [];
            
            grado.areas = buscarObjetos($scope.plan.areas,"gradoID",grado.gradoID);
            //agregando tutoria como un area curricular
            grado.areas.push({areaID:-1,area:"tutoria",gradoID:grado.gradoID,grado:grado.grado});
        }
    };
    $scope.filtrarSecciones = function(){
        if($scope.gradoSel){
            if($scope.areaSel)
                $scope.tablaTarea.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel, 'areaID': $scope.areaSel });
            else
                $scope.tablaTarea.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel });
        }
    };
    $scope.filtrarAreas = function(){
        
        if($scope.gradoSel){
            if($scope.seccionSel)
                $scope.tablaTarea.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel, 'areaID': $scope.areaSel });
            else        
                $scope.tablaTarea.filter({'gradoID': $scope.gradoSel.gradoID, 'areaID': $scope.areaSel });
        }
    };
    $scope.buscarPlanEstudios = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('planEstudios',1,'buscarPlanEstudios');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            
            if(response.responseSta){
                $scope.plan = response.data;
                
                request = crud.crearRequest('tarea',1,'listarTareas');
                request.setData({planID:$scope.plan.planID});
                crud.listar("/web",request,function(response){

                    if(response.responseSta){
                        tareas = response.data;
                        
                        tareas.forEach(function (item){
                           item.grado = buscarContenido($scope.plan.grados,"gradoID","grado",item.gradoID);
                           
                           item.area = buscarContenido($scope.plan.areas,"areaID","area",item.areaID);
                        });
                        
                        settingTarea.dataset = tareas;
                        
                        $scope.tablaTarea.settings(settingTarea);
                    }
                },function(data){
                    console.info(data);
                });
                
            }
        },function(data){
            console.info(data);
        });
    };
    
}]);
