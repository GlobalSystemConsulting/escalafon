app.controller("bandejaEvaluacionesCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
    
    $scope.plan = {};    
    $scope.tarea = {};
    $scope.documentoSel = {archivo:{}};
    
    var tareas = [];
    
    //tabla de tareas
    var paramsTarea = {count: 10};
    var settingTarea = { counts: []};
    $scope.tablaTarea = new NgTableParams(paramsTarea, settingTarea);
    
    $scope.estados = [{id:'N',title:"nuevo"},{id:'E',title:"enviado"},{id:'C',title:"calificado"},{id:'F',title:"fuera tiempo"}];
    
    $scope.prepararEnviar = function(o){
        $scope.tarea = JSON.parse(JSON.stringify(o));
        $('#modalEnviarTarea').modal('show');
    };
    $scope.enviarTarea = function(){
        
        if($scope.tarea.documentos.length == 0){
            modal.mensaje("CONFIRMACION","Debe enviar por lo menos un archivo");
            return;
        }
        
        modal.mensajeConfirmacion($scope,"seguro que enviara la tarea",function(){

            var request = crud.crearRequest('tarea',1,'resolverTarea');
            request.setData($scope.tarea);        
            crud.insertar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.tarea.estado = res.data.estado;
                    
                    res.data.documentos.forEach(function(item,i){
                        $scope.tarea.documentos[i].tareaDocumentoID = item.tareaDocumentoID;
                        $scope.tarea.documentos[i].documento = item.documento;
                        $scope.tarea.documentos[i].url = item.url;
                    });
                    
                    settingTarea.dataset[$scope.tarea.i] = $scope.tarea;
                    $scope.tablaTarea.reload(); 
                    //cerramos la ventana modal
                    $('#modalEnviarTarea').modal('hide');
                }
                else{
                    $scope.tarea.estado = res.data.estado;
                    settingTarea.dataset[$scope.tarea.i] = $scope.tarea;
                    $scope.tablaTarea.reload(); 
                    //cerramos la ventana modal
                    $('#modalEnviarTarea').modal('hide');
                }
            },function(data){
                console.info(data);
            });
            
        },"400");        
    };
    
    $scope.agregarDocumento = function(){
        if(!$scope.documentoSel.documento || $scope.documentoSel.documento =="" ){
            modal.mensaje("CONFIRMACION","seleccione un documento adjunto");
            return;
        }
        if($scope.tarea.numeroDoc <= $scope.tarea.documentos.length ){
            modal.mensaje("CONFIRMACION","NO debe superar el numero de documentos permitidos");
            return;
        }
        
        $scope.tarea.documentos.push($scope.documentoSel);
        $scope.documentoSel = {archivo:{}};  
    };
    $scope.editarDocumento = function(i,d){
        //si estamso editando
        if(d.edi){
            $scope.tarea.documentos[i] = d.copia;
        }
        //si queremos editar
        else{
            d.copia = JSON.parse(JSON.stringify(d));
            d.edi =true;            
        }
    };
    $scope.eliminarDocumento = function(i,d){
        //si estamso cancelando la edicion
        if(d.edi){
            d.edi = false;
            delete d.copia;
        }
        //si queremos eliminar el elemento
        else{
            $scope.tarea.documentos.splice(i,1);
        }
    };
    
    $scope.filtrarAreas = function(){
              
        $scope.tablaTarea.filter({'areaID': $scope.tarea.areaID });
    };
    $scope.buscarPlanEstudios = function(orgID,usuarioID){
        //preparamos un objeto request
        var request = crud.crearRequest('planEstudios',1,'buscarPlanEstudios');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            
            if(response.responseSta){
                $scope.plan = response.data;                
                
                request = crud.crearRequest('tarea',1,'listarTareasPorAlumno');
                request.setData({planID:$scope.plan.planID,alumnoID:usuarioID});
                crud.listar("/web",request,function(response){

                    if(response.responseSta){
                        tareas = response.data;
                        
                        tareas.forEach(function (item){
                           item.area = buscarContenido($scope.plan.areas,"areaID","area",item.areaID);
                        });
                        
                        settingTarea.dataset = tareas;
                        
                        $scope.tablaTarea.settings(settingTarea);
                    }
                },function(data){
                    console.info(data);
                });
            }
        },function(data){
            console.info(data);
        });
        
        
    };
    
}]);
