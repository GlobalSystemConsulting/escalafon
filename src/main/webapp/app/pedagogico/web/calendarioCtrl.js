app.controller("calendarioCtrl",["$scope","crud","modal", function ($scope,crud,modal){
        
    $scope.actividad = {};
    
    var calendario ;
    var actividadesP ;
    var actividadesF ;
    
    $scope.prepararAgregar = function(start,end){
        
        var diaSel = new Date(convertirFechaUTC(start._d));
        var hoy = new Date();
        hoy.setHours(0);
        hoy.setMinutes(0);
        hoy.setSeconds(0);
        hoy.setMilliseconds(0);
        
        if(diaSel < hoy){
            modal.mensaje("ADVERTENCIA","No se puede añadir una actividad este dia");
            return
        }
        $scope.actividad = {};        
        $scope.$apply(function () {
            $scope.actividad = {tipo:'N'};
            $scope.actividad.horaInicio = diaSel;
            //$scope.actividad.horaFin = new Date(convertirFechaUTC(end._d));
            
            $scope.dateOptions.minDate = $scope.actividad.horaInicio;
        });
        $('#modalActividad').modal('show');
    };
    $scope.prepararEditar = function(event){
        $scope.actividad = {};
        $scope.$apply(function () {
            $scope.actividad.i = event.i;
            $scope.actividad.actividadID = event.actividadID;
            $scope.actividad.id = event._id;
            $scope.actividad.titulo = event.title;
            $scope.actividad.descripcion = event.descripcion;
            $scope.actividad.tipo = event.tipo;
            $scope.actividad.horaI = event.horaI;
            $scope.actividad.horaF = event.horaF;
            $scope.actividad.horaInicio = $scope.actividad.horaI?event.start._d: new Date(convertirFechaUTC(event.start._d));            
            if(event.end)
                $scope.actividad.horaFin = $scope.actividad.horaI?event.end._d: new Date(convertirFechaUTC(event.end._d));
            
            $scope.dateOptions.minDate = $scope.actividad.horaInicio;
        });
        $('#modalActividad').modal('show');
    };
    
    $scope.mergeActividad = function(){
        
        //editando una nueva actividad
        if($scope.actividad.id){
            
            var eventData = {
                i: $scope.actividad.i,
                actividadID: $scope.actividad.actividadID,
                title: $scope.actividad.titulo,
                descripcion:$scope.actividad.descripcion,
                tipo:$scope.actividad.tipo,
                start: convertirFecha2($scope.actividad.horaInicio)+($scope.actividad.horaI?" "+convertirHora($scope.actividad.horaInicio):''),
                horaI:$scope.actividad.horaI,
                horaF:$scope.actividad.horaF,
                color: $scope.actividad.tipo=='A'?'#257e4a':'#3a87ad'
            };
            if($scope.actividad.horaFin)
                eventData.end = convertirFecha2($scope.actividad.horaFin)+($scope.actividad.horaF?" "+convertirHora($scope.actividad.horaFin):'');  
            
            $scope.actividad.horaInicio = convertirFecha2($scope.actividad.horaInicio)+" "+($scope.actividad.horaI?convertirHora($scope.actividad.horaInicio):'00:00:00');
            if($scope.actividad.horaFin)
                $scope.actividad.horaFin = convertirFecha2($scope.actividad.horaFin)+" "+($scope.actividad.horaF?convertirHora($scope.actividad.horaFin):'00:00:00');
            
            var request = crud.crearRequest('calendario',1,'actualizarActividad');
            
            request.setData($scope.actividad);
            crud.actualizar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    calendario.fullCalendar('removeEvents', [$scope.actividad.id]);
                    calendario.fullCalendar('renderEvent', eventData, true); // stick? = true
                    calendario.fullCalendar('unselect');
                    $('#modalActividad').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
            
        }
        //agregando un nueva actividad
        else{
            
            var eventData = {
                i: $scope.actividad.i,
                title: $scope.actividad.titulo,
                descripcion:$scope.actividad.descripcion,
                tipo:$scope.actividad.tipo,
                start:convertirFecha2($scope.actividad.horaInicio)+($scope.actividad.horaI?" "+convertirHora($scope.actividad.horaInicio):''),
                horaI:$scope.actividad.horaI,
                horaF:$scope.actividad.horaF,
                color: $scope.actividad.tipo=='A'?'#257e4a':'#3a87ad'
            };            
            if($scope.actividad.horaFin)
                eventData.end = convertirFecha2($scope.actividad.horaFin)+($scope.actividad.horaF?" "+convertirHora($scope.actividad.horaFin):'');
            
            $scope.actividad.horaInicio = convertirFecha2($scope.actividad.horaInicio)+" "+($scope.actividad.horaI?convertirHora($scope.actividad.horaInicio):'00:00:00');
            if($scope.actividad.horaFin)
                $scope.actividad.horaFin = convertirFecha2($scope.actividad.horaFin)+" "+($scope.actividad.horaF?convertirHora($scope.actividad.horaFin):'00:00:00');
            
            var request = crud.crearRequest('calendario',1,'crearActividad');
            
            request.setData($scope.actividad);
            crud.insertar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    $scope.actividad = eventData.actividadID = res.data.actividadID;
                    insertarElemento(actividadesP,$scope.actividad);
                    calendario.fullCalendar('renderEvent', eventData, true); // stick? = true                     
                    calendario.fullCalendar('unselect');
                    $('#modalActividad').modal('hide');
                }            
            },function(data){
                console.info(data);
            });                       
        }
        
        
        
    };
    
    $scope.quitarActividad = function(){
        
        modal.mensajeConfirmacion($scope,"seguro que desea quitar esta actividad",function(){
            
            var request = crud.crearRequest('calendario',1,'eliminarActividad');
            request.setData({actividadID:$scope.actividad.actividadID});
            crud.eliminar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    
                    eliminarElemento(actividadesP,$scope.actividad.i);
                    calendario.fullCalendar('removeEvents', [$scope.actividad.id]);
                    $('#modalActividad').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
            
            
            
        },"400");
    };
    
    $scope.dateOptions = {
        formatYear: 'yy',
        minDate: new Date(),
        startingDay: 1
    };
    $scope.mostrarCalendario = function(){
        calendario = $('#calendar').fullCalendar({
            header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaDay,listMonth'
            },
            timezone:'local',
            defaultDate: new Date(),
            navLinks: true, // can click day/week names to navigate views
            businessHours: false, // display business hours
            selectable: true,
            selectHelper: true,
            select: $scope.prepararAgregar,
            eventLimit: true, // allow "more" link when too many events
            editable: false,
            events: [
                    /*{
                            title: 'Meeting',
                            start: '2016-12-13 13:00:00',
                            end: '2016-12-16',
                            color: '#257e4a'
                    },
                    {
                            title: 'Conference',
                            start: '2016-12-18',
                            end: '2016-12-20'
                    },*/
                    // red areas where no events can be dropped
                    {
                            start: '2016-12-24',
                            end: '2016-12-28',
                            overlap: false,
                            rendering: 'background',
                            color: '#ff9f89'
                    }
            ],/*
            dayClick: function(date, jsEvent, view) {

                // change the day's background color just for fun
                $(this).css('background-color', 'red');
                
                console.log(view);

            }*/
            eventRender: function(event, element) {
                element.bind("click",function(){
                    $scope.prepararEditar(event);
                });
            }
        });
        
        console.log(calendario);
        
        //preparamos un objeto request
        var request = crud.crearRequest('calendario',1,'listarActividades');
        crud.listar("/web",request,function(res){
            actividadesP = res.data.actividadesPropias;
            actividadesF = res.data.actividadesForaneas;
            
            if(actividadesP)
            actividadesP.forEach(function(item){
                var event = {
                    i: item.i,
                    actividadID: item.actividadID,
                    title: item.titulo,
                    descripcion:item.descripcion,
                    tipo:item.tipo,
                    start: item.horaInicio,
                    end: item.horaFin,
                    horaI:item.horaI,
                    horaF:item.horaF,
                    color: item.tipo=='A'?'#257e4a':'#3a87ad'
                };
                calendario.fullCalendar('renderEvent', event, true);
                
            });
            
            
            
            
        },function(data){
            console.info(data);
        });
        
    };
    
    listarActividades = function(){
        
    }; 
    
}]);
