app.controller("tareasCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    $scope.plan = {};
    $scope.gradoSel = {};
    $scope.seccionSel = '';
    $scope.areaSel = '';
    $scope.tarea = {};
    
    var tareas = [];
    
    //tabla de tareas
    var paramsTarea = {count: 10};
    var settingTarea = { counts: []};
    $scope.tablaTarea = new NgTableParams(paramsTarea, settingTarea);
    
    $scope.estados = [{id:'N',title:"nuevo"},{id:'E',title:"enviado"},{id:'C',title:"calificado"}];
    
    //tabla de alumnos
    var paramsAlumno = {count: 10};
    var settingAlumno = { counts: []};
    $scope.tablaAlumno = new NgTableParams(paramsAlumno, settingAlumno);
    
    $scope.prepararAgregar = function(seccionID,areaID){
        $scope.tarea = {seccionID:seccionID,areaID:areaID};
        $('#modalNuevaTarea').modal('show');
    };
    $scope.agregarTarea = function(usuarioID){
        
        if(!$scope.tarea.tareaID){
            $scope.tarea.planID = $scope.plan.planID;
            $scope.tarea.gradoID = $scope.gradoSel.gradoID;
            $scope.tarea.seccionID = $scope.seccionSel.seccionID;
            $scope.tarea.areaID = $scope.areaSel;
            $scope.tarea.docenteID = usuarioID;
            
            var request = crud.crearRequest('tarea',1,'insertarTarea');
            request.setData($scope.tarea);        
            crud.insertar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.tarea.adjunto = res.data.adjunto;
                    $scope.tarea.tareaID = res.data.tareaID;
                    $scope.tarea.estado = res.data.estado;

                    $scope.tarea.grado = buscarContenido($scope.plan.grados,"gradoID","grado",$scope.tarea.gradoID);
                    $scope.tarea.area = buscarContenido($scope.plan.areas,"areaID","area",$scope.tarea.areaID);

                    //insertamos el elemento a la lista            
                    insertarElemento(settingTarea.dataset,$scope.tarea);
                    $scope.tablaTarea.reload();
                    //reiniciamos las variables
                    $scope.tarea = {seccionID:$scope.tarea.seccionID,areaID:$scope.tarea.areaID};
                    //$scope.tablaGrupo.reload();
                    //cerramos la ventana modal
                    $('#modalNuevaTarea').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
        }
        else{
            var request = crud.crearRequest('tarea',1,'actualizarTarea');
            request.setData($scope.tarea);        
            crud.actualizar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    $scope.tarea.adjunto = res.data.adjunto;
                    settingTarea.dataset[$scope.tarea.i] = $scope.tarea;
                    $scope.tablaTarea.reload();
                    //cerramos la ventana modal
                    $('#modalNuevaTarea').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
        }
        
        
    };
    $scope.prepararEditar = function(o){
        $scope.tarea = JSON.parse(JSON.stringify(o));
        $('#modalNuevaTarea').modal('show');
    };
    $scope.eliminarTarea = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar la tarea",function(){
            
            var request = crud.crearRequest('tarea',1,'eliminarTarea');
            request.setData({tareaID:idDato});

            crud.eliminar("/web",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingTarea.dataset,i);
                    $scope.tablaTarea.reload();                }

            },function(data){
                console.info(data);
            });
            
        },"400");
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(),
        minDate: new Date(),
        startingDay: 1
      };
    $scope.prepararEnviar = function(o){
        $scope.tarea = JSON.parse(JSON.stringify(o));
        $scope.fechaMinima = new Date();
        $scope.dateOptions.minDate = $scope.fechaMinima;
        
        var fechaMax = new Date();        
        fechaMax.setMonth(11);
        fechaMax.setDate(31);
        fechaMax.setHours(23);
        $scope.dateOptions.maxDate = fechaMax;
        
        $scope.tarea.fecha = new Date();
        $scope.tarea.fecha.setSeconds(0);
        $('#modalEnvioTarea').modal('show');
    };
    $scope.enviarTarea = function(orgID){
        
        //$scope.tarea.fechaEnvio = $scope.tarea.fecha.toLocaleString();
        $scope.tarea.fechaEntrega = convertirFecha2($scope.tarea.fecha) +" "+convertirHora($scope.tarea.fecha);
        $scope.tarea.organizacionID = orgID;
        
        var request = crud.crearRequest('tarea',1,'enviarTarea');
        request.setData($scope.tarea);        
        crud.insertar("/web",request,function(res){            
            modal.mensaje("CONFIRMACION",res.responseMsg);
            if(res.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.tarea.estado = res.data.estado;
                settingTarea.dataset[$scope.tarea.i] = $scope.tarea;
                $scope.tablaTarea.reload(); 
                //cerramos la ventana modal
                $('#modalEnvioTarea').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
    };
    
    $scope.prepararTarea = function(tarea){
        
        $scope.tarea = JSON.parse(JSON.stringify(tarea));
        
        //preparamos un objeto request
        var request = crud.crearRequest('tarea',1,'listarAlumnosCumplieronTarea');
        request.setData({tareaID:tarea.tareaID});
        crud.listar("/web",request,function(res){
            
            if(res.responseSta){
                
                settingAlumno.dataset = res.data;
                $scope.tablaAlumno.settings(settingAlumno);
                
                $scope.tareaSel = tarea;
                $scope.active = 1;
            }
            
        },function(data){
            console.info(data);
        });
        
    };
    
    $scope.prepararCalificar = function(alumno){
        
        $scope.alumno = JSON.parse(JSON.stringify(alumno));
        $scope.alumno.nombres = alumno.nombres+" "+alumno.apellidos;
        
        //preparamos un objeto request
        var request = crud.crearRequest('tarea',1,'verDocumentosDeTarea');
        request.setData({bandejaTareaID:alumno.bandejaTareaID});
        crud.listar("/web",request,function(res){
            
            if(res.responseSta){
                
                $scope.documentos = res.data;
                $scope.active = 2;
            }
            
        },function(data){
            console.info(data);
        });
    };
    $scope.calcularNota = function(){
        var nota = 0;
        $scope.documentos.forEach(function(item){
            nota += item.nota?item.nota:0;
        });
        $scope.alumno.nota = nota/$scope.documentos.length;
        $scope.alumno.nota = $scope.alumno.nota.toFixed(0);//solo se permite numero sin decimales
        
    };
    $scope.calificarTarea = function(){
        
        if(!$scope.alumno.nota || isNaN($scope.alumno.nota)){
            modal.mensaje("CONFIRMACION","La nota no es un valor valido");
            return;
        }
        
        modal.mensajeConfirmacion($scope,"Esta seguro que la calificacion es correcta",function(){
            
            var request = crud.crearRequest('tarea',1,'calificarTarea');
            request.setData({bandejaTareaID:$scope.alumno.bandejaTareaID,nota:$scope.alumno.nota});        
            crud.actualizar("/web",request,function(res){            
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.alumno.estado = res.data.estado;
                    $scope.alumno.nota = res.data.nota;
                    settingAlumno.dataset[$scope.alumno.i] = $scope.alumno;
                    $scope.tablaAlumno.reload();

                    $scope.active = 1;
                }            
            },function(data){
                console.info(data);
            });
        },"400");
    };
    
    $scope.finalizarCalificacion = function(){
        
        if(!$scope.tarea.tareaID || isNaN($scope.tarea.tareaID)){
            modal.mensaje("CONFIRMACION","Tarea no valida");
            $scope.active = 0;
            return;
        }
        
        modal.mensajeConfirmacion($scope,"Esta seguro que finalizara la calificacion de la tarea, y actualizar el registro auxilar",function(){
            
            var request = crud.crearRequest('tarea',1,'finalizarTarea');
            request.setData({tareaID:$scope.tarea.tareaID});        
            crud.actualizar("/web",request,function(res){
                modal.mensaje("CONFIRMACION",res.responseMsg);
                if(res.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.tarea.estado = res.data.estado;
                    settingTarea.dataset[$scope.tarea.i] = $scope.tarea;
                    $scope.tablaTarea.reload();

                    $scope.active = 0;
                }
            },function(data){
                console.info(data);
            });
        },"400");
    };
    
    $scope.calcularSecciones = function(grado){
        $scope.seccionSel = '';
        $scope.areaSel = '';
        if(grado)      
            $scope.tablaTarea.filter({ 'gradoID': grado.gradoID });
        else
            $scope.tablaTarea.filter({});
    };
    $scope.filtrarSecciones = function(){
        $scope.areaSel = '';
        if($scope.seccionSel)
            $scope.tablaTarea.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel.seccionID });
        else
            $scope.tablaTarea.filter({ 'gradoID': grado.gradoID });
    };
    $scope.filtrarAreas = function(){        
        if($scope.areaSel)
            $scope.tablaTarea.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel.seccionID, 'areaID': $scope.areaSel });
        else        
            $scope.tablaTarea.filter({'gradoID': $scope.gradoSel.gradoID, 'seccionID': $scope.seccionSel.seccionID });
    };
    $scope.buscarPlanEstudios = function(orgID,usuarioID){
        //preparamos un objeto request
        var request = crud.crearRequest('planEstudios',1,'buscarPlanEstudios');
        request.setData({organizacionID:orgID});
        crud.listar("/cuadroHoras",request,function(response){
            
            if(response.responseSta){
                $scope.plan = response.data;
                
                request = crud.crearRequest('cuadroHoras',1,'buscarDistribucionPorDocente');
                request.setData({planID:$scope.plan.planID,docenteID:usuarioID});
                crud.listar("/cuadroHoras",request,function(response){

                    if(response.responseSta){
                        $scope.grados = response.data;
                        $scope.grados.forEach(function (item){
                           var g = buscarObjeto($scope.plan.grados,"gradoID",item.gradoID);
                           item.grado = g.grado;
                           item.meta = g.meta;
                           var areas = buscarObjetos($scope.plan.areas,"gradoID",item.gradoID);                           
                           item.secciones.forEach(function (item2){
                                //si no tiene distribucion de areas, es docente de primaria se leasigna todas las areas segun el nivel
                                 if(item2.areas.length == 0)
                                     item2.areas = areas;
                                 else{
                                     item2.areas.forEach(function (item3){
                                         item3.area = buscarContenido(areas,"areaID","area",item3.areaID);
                                     });
                                 }
                            });
                        });
                    }
                },function(data){
                    console.info(data);
                });
                $scope.buscarTareas(usuarioID);
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.buscarTareas = function(usuarioID){
        var request = crud.crearRequest('tarea',1,'listarTareasPorDocente');
        request.setData({planID:$scope.plan.planID,docenteID:usuarioID});
        crud.listar("/web",request,function(response){

            if(response.responseSta){
                tareas = response.data;

                tareas.forEach(function (item){
                   item.grado = buscarContenido($scope.plan.grados,"gradoID","grado",item.gradoID);

                   item.area = buscarContenido($scope.plan.areas,"areaID","area",item.areaID);
                });

                settingTarea.dataset = tareas;

                $scope.tablaTarea.settings(settingTarea);
            }
        },function(data){
            console.info(data);
        });
        
    };
    
}]);
