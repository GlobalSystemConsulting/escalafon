app.controller("grupoCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    $scope.grupos = [];
    $scope.nuevoGrupo = {tipo:false,nombre:"",estado:'A'};
    $scope.grupoSel = {};
    
    $scope.usuarios = [];
    $scope.usuariosSel = [];
    
    $scope.usuariosEdi= [];
    $scope.usuariosSelEdi= [];
    
    //tabla de grupos
    var paramsGrupo = {count: 10};
    var settingGrupo = { counts: []};
    $scope.tablaGrupo = new NgTableParams(paramsGrupo, settingGrupo);
    
    //filtros
    $scope.estadosSelect = [{id:'A',title:"activo"},{id:'I',title:"inactivo"},{id:'E',title:"eliminado"}];
    $scope.tiposSelect = [{id:false,title:"interno"},{id:true,title:"externo"}];
    
    $scope.listarGrupos = function(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('grupo',1,'listarGrupos');
        request.setData({listar:true,organizacionID:orgID});
        crud.listar("/web",request,function(res){
            settingGrupo.dataset = res.data;
            $scope.tablaGrupo.settings(settingGrupo);
        },function(data){
            console.info(data);
        });
        
        listarUsuarios(orgID);
    };    
    $scope.agregarGrupo = function(orgID){
        
        var request = crud.crearRequest('grupo',1,'insertarGrupo');
        
        $scope.nuevoGrupo.usuarios = [];        
        for(var i=0;i<$scope.usuariosSel.length;i++ )
            $scope.nuevoGrupo.usuarios.push( $scope.usuariosSel[i] );
        
        $scope.nuevoGrupo.organizacionID = orgID;
        request.setData($scope.nuevoGrupo);
        
        crud.insertar("/web",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.nuevoGrupo.grupoID = response.data.grupoID;
                
                //insertamos el elemento a la lista            
                insertarElemento(settingGrupo.dataset,$scope.nuevoGrupo);
                //reiniciamos las variables
                $scope.nuevoGrupo = {tipo:false,nombre:"",estado:'A'};
                $scope.usuariosSel = [];
                $scope.usuarios.forEach(function(item){
                    item.ver = false;
                });
                $scope.tablaGrupo.reload();
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarGrupo = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el Grupo",function(){
            
            var request = crud.crearRequest('grupo',1,'eliminarGrupo');
            request.setData({grupoID:idDato});

            crud.eliminar("/web",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingGrupo.dataset,i);
                    $scope.tablaGrupo.reload();
                }

            },function(data){
                console.info(data);
            });
            
        },"400");
    };
    $scope.prepararAgregar = function(){
        $scope.usuarios.forEach(function(item){
            item.ver = false;
        });
        
        $('#modalNuevo').modal('show');
    };
    $scope.prepararEditar = function(t){
        $scope.grupoSel = JSON.parse(JSON.stringify(t));
        
        $scope.usuariosSelEdi= [];
        $scope.usuariosEdi = JSON.parse(JSON.stringify($scope.usuarios));
        $scope.usuariosEdi.forEach(function(item){
            item.ver = false;
        });
        
        for(var i=0; $scope.grupoSel.usuarios && i < $scope.grupoSel.usuarios.length;i++ ){            
            $scope.usuariosEdi.forEach(function(item){                
                if( item.sessionID === $scope.grupoSel.usuarios[i].sessionID){
                    item.ver = true;
                    $scope.usuariosSelEdi.push(item);
                }
            });
        }
        
        $('#modalEditar').modal('show');
    };
    $scope.editarGrupo = function(){
        
        var request = crud.crearRequest('grupo',1,'actualizarGrupo');
        
        $scope.grupoSel.usuarios = [];        
        for(var i=0;i<$scope.usuariosSelEdi.length;i++ )
            $scope.grupoSel.usuarios.push( $scope.usuariosSelEdi[i] );
        
        request.setData($scope.grupoSel);
                
        crud.actualizar("/web",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                
                //actualizando
                settingGrupo.dataset[$scope.grupoSel.i] = $scope.grupoSel;
                $scope.tablaGrupo.reload();
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    $scope.eliminarGrupos = function(){
            
        
    };
    
    $scope.seleccionarRol = function(i){
        $scope.usuariosSel.push( $scope.usuarios[i] );
        $scope.usuarios[i].ver = true;
    };
    $scope.desSeleccionarRol = function(i){
        $scope.usuariosSel[i].ver = false;
        $scope.usuariosSel.splice(i,1);
    };
    $scope.seleccionarRol2 = function(i){
        $scope.usuariosSelEdi.push( $scope.usuariosEdi[i] );
        $scope.usuariosEdi[i].ver = true;
    };
    $scope.desSeleccionarRol2 = function(i){
        $scope.usuariosSelEdi[i].ver = false;
        $scope.usuariosSelEdi.splice(i,1);
    };
    
    function listarUsuarios(orgID){
        //preparamos un objeto request
        var request = crud.crearRequest('usuarioSistema',1,'listarUsuarioSessionesPorOrganizacion');        
        request.setData({organizacionID:orgID});
        crud.listar("/configuracionInicial",request,function(data){
            $scope.usuarios = data.data;
        },function(data){
            console.info(data);
        });
    };
    
    
}]);