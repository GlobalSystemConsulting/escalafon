/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('app')
    .controller('registroAuxiliar',['$log','$location','modal','UtilAppServices','NgTableParams','$rootScope','crud',function($log,$location,modal,util,NgTableParams,$rootScope,crud){
        var self = this;
        self.abrirCalendar = false;
        self.openCalendar = function () {
            self.abrirCalendar = true;
        }
        self.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(),
            minDate: new Date(1900,1,1),
            startingDay: 1
        };

        self.tablaAsistenciaEst = new NgTableParams({count:8},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        self.patron = /^\d+$/;
        listarNiveles();
        function listarNiveles(){
            var request = crud.crearRequest('anecdotario',1,'listarGrados');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, doc:$rootScope.usuMaster.usuario.usuarioID});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.niveles = response.data.niveles;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        function listarPeriodos(nivel){
            var request = crud.crearRequest('unidades',1,'listarPeriodosPlanEstudios');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, niv:nivel.id});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.periodos = response.data;

                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarGrados =  function(nivel){
            self.grados = nivel.grados;
            listarPeriodos(nivel);
        }
        self.mostrarSecciones =  function(grado){
            self.secciones = grado.secciones;

        }
        self.mostrarAlumnos = function(){
            var request = crud.crearRequest('anecdotario',1,'listarEstudiantes');
            request.setData({org:$rootScope.usuMaster.organizacion.organizacionID, gra:self.grado.id,sec:self.seccion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.estudiantes = response.data;
                    angular.forEach(self.estudiantes,function(objest,keyest){
                        objest.notas = [];
                        angular.forEach(self.indicadores,function(objind,keyind){
                            objest.notas.push({ind:objind.id,not:""});
                        });
                    });
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
            });
        }
        self.mostrarAreas = function(seccion){
            var request = crud.crearRequest('acomp',1,'listarCursosDocente');
            request.setData({doc:$rootScope.usuMaster.usuario.usuarioID,org:$rootScope.usuMaster.organizacion.organizacionID,gra:self.grado.id,secc:seccion});
            crud.listar('/maestro',request,function(response){
                if(response.responseSta){
                    self.areas = response.data;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.mostrarCompetencias = function(area){
            var request = crud.crearRequest('notas_estudiante',1,'listarCompetenciasPeriodo');
            request.setData({
                per: self.periodo.id,
                usr:$rootScope.usuMaster.usuario.usuarioID,
                org:$rootScope.usuMaster.organizacion.organizacionID,
                gra:self.grado.id,
                are:area.id
            });
            crud.listar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    self.competencias = response.data;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.mostrarFicha = function(competencia){
            var request = crud.crearRequest('notas_estudiante',1,'listarNotasRegistroAuxiliar');
            request.setData({
                per: self.periodo.id,
                usr:$rootScope.usuMaster.usuario.usuarioID,
                org:$rootScope.usuMaster.organizacion.organizacionID,
                gra:self.grado.id,
                are:self.area.id,
                secc: self.seccion,
                comp:competencia.id
            });
            crud.listar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    self.indicadores = response.data.indicadores;
                    self.estudiantes = response.data.estudiantes;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.registrarNotaEstudiante = function(estudiante){
            var est = [];
            est.push(angular.copy(estudiante));
            var resolve = {
                data : function () {
                    return {
                        indicadores: self.indicadores,
                        estudiantes: est
                    }
                }
            }
            var modalInstance = util.openModal('notas_alumno.html','registrarNotaEstudiante','lg','ctrl',resolve);
            modalInstance.result.then(function(estRes){
                angular.extend(estudiante,estRes);
            },function(cdata){});
        }

        self.registrarNotasCompetencia = function(){
            var request = crud.crearRequest('notas_estudiante',1,'registrarNotasCompetencia');
            request.setData({
                comp: self.competencia.id,
                per: self.periodo.id,
                are: self.area.id,
                usr: $rootScope.usuMaster.usuario.usuarioID,
                estudiantes: self.estudiantes
            });
            crud.insertar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                   modal.mensaje("EXITO",response.responseMsg);

                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.registrarNotasArea = function(){
            var request = crud.crearRequest('notas_estudiante',1,'registrarNotasArea');
            request.setData({
                per: self.periodo.id,
                are: self.area.id,
                usr: $rootScope.usuMaster.usuario.usuarioID,
                estudiantes: self.estudiantes_resumen
            });
            crud.insertar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    modal.mensaje("EXITO",response.responseMsg);

                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.mostrarResumenNotas = function(){
            if(self.grado == undefined || self.grado == null){
                modal.mensaje("ERROR","DEBE SELECCIONAR UN GRADO");
                return;
            }else if(self.seccion == undefined || self.seccion == null){
                modal.mensaje("ERROR","DEBE SELECCIONAR UNA SECCION");
                return;
            } else if(self.periodo == undefined || self.periodo == null){
                modal.mensaje("ERROR","DEBE SELECCIONAR UN PERIODO");
                return;
            } else if(self.area == undefined || self.area == null){
                modal.mensaje("ERROR","DEBE SELECCIONAR UN AREA");
                return;
            }
            self.showResumen = true;
            var request = crud.crearRequest('notas_estudiante',1,'listarNotasCompetencias');
            request.setData({
                per: self.periodo.id,
                usr:$rootScope.usuMaster.usuario.usuarioID,
                org:$rootScope.usuMaster.organizacion.organizacionID,
                gra:self.grado.id,
                are:self.area.id,
                secc: self.seccion
            });
            crud.listar('/submodulo_academico',request,function(response){
                if(response.responseSta){
                    self.competencias_resumen = response.data.indicadores;
                    self.estudiantes_resumen = response.data.estudiantes;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){});
        }
        self.registrarNotaEstudiantesArea = function(estudiante){
            var est = [];
            est.push(angular.copy(estudiante));
            var resolve = {
                data : function () {
                    return {
                        competencias: self.competencias_resumen,
                        estudiantes: est
                    }
                }
            }
            var modalInstance = util.openModal('notas_area_alumno.html','registrarNotaEstudianteArea','lg','ctrl',resolve);
            modalInstance.result.then(function(estRes){
                angular.extend(estudiante,estRes);
            },function(cdata){});
        }
        self.volverDeResumen = function(){
            self.showResumen = false;
        }
    }]).controller('registrarNotaEstudiante',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        var self = this;
        self.indicadores  = data.indicadores;
        self.estudiantes = data.estudiantes;
        self.cancelar = function () {
            /*angular.forEach(self.estudiantes[0].notas,function(obj){
                obj.not = '';
            });*/
            $uibModalInstance.dismiss('cancel');
        }
        self.guardar = function(){
            $uibModalInstance.close(self.estudiantes[0]);
        }
    }]).controller('registrarNotaEstudianteArea',['$log','NgTableParams','$uibModalInstance','$rootScope','modal','data','crud',function($log,NgTableParams,$uibModalInstance,$rootScope,modal,data,crud){
        var self = this;
        self.competencias  = data.competencias;
        $log.log('competencias',self.competencias);
        self.estudiantes = data.estudiantes;
        self.cancelar = function () {
            $uibModalInstance.dismiss('cancel');
        }
        self.guardar = function(){
            $uibModalInstance.close(self.estudiantes[0]);
        }
    }]);

