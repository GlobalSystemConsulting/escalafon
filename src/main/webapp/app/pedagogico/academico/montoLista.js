/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 function MontoListaService($rootScope,crud){
    this.listarMontos = function(succ,err){
        var request = crud.crearRequest('utiles',1,'listarMonto');
        crud.listar('/submodulo_academico',request,succ,err);
    };
    this.registrarMonto = function(articulo,succ,err){
        var request = crud.crearRequest('utiles',1,'registrarMonto');
        request.setData(articulo);
        crud.insertar('/submodulo_academico',request,succ,err);
    };
    this.editarMonto = function (monto,succ,err) {
        var request = crud.crearRequest('utiles',1,'actualizarMonto');
        request.setData(monto);
        crud.actualizar('/submodulo_academico',request,succ,err);
    };

    this.eliminarMontos = function (id,succ,err) {
        var request = crud.crearRequest('utiles',1,'eliminarMonto');
        request.setMetadataValue('id',''+id);
        crud.eliminar('/submodulo_academico',request,succ,err);
    };
    this.listarNiveles = function(succ,err){
        var request = crud.crearRequest('utiles',1,'listarNivel');
        crud.listar('/submodulo_academico',request,succ,err);
    };
}

angular.module('app')
        .service('MontoListaService',['$rootScope','crud',MontoListaService])
        .controller('montoLista',['$log','$location','modal','UtilAppServices','NgTableParams','MontoListaService',function($log,$location,modal,util,NgTableParams,montoListaService){
        var self = this;
       
        self.tablaMontos = new NgTableParams({count:8},{
            counts: [],
            paginationMaxBlocks: 13,
            paginationMinBlocks: 2,
            dataset:[]
        });
        try{
            self.showProgress = true;
            montoListaService.listarMontos(function(response){
                $log.log('reponse',response);
                self.showProgress = false;
                self.tablaMontos.settings().dataset = response.data;
                self.tablaMontos.reload();
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje("ERROR","No se pudieron obtener los datos");
            });
        }catch(err){
            modal.mensaje("ERROR","No se pudieron obtener los datos");
        }
  
        self.nuevoMonto = function(){
            montoListaService.listarNiveles(function(response){
                if(response.responseSta){
                    $log.log('reponse',response);
                    var niveles = {
                        data: function(){
                            return response.data;
                               
                        }
                    }
                    var modalInstance = util.openModal('nuevoMonto.html','registrarMontoCtrl','lg','ctrl',niveles);
                    modalInstance.result.then(function(monto){
                        self.tablaMontos.settings().dataset.push(monto);
                        self.tablaMontos.reload();
                        modal.mensaje("ver","se ingreso al modal")
                    },function(errModal){});
                }
            },function(errResponse){
                self.showProgress = false;
                modal.mensaje("ERROR","No se pudieron obtener los datos");
            });
            
        }
        self.eliminMonto = function($e,row){
            util.openDialog('Eliminar Monto','¿Seguro que desea eliminar\n el monto?',$e,
                function (response) {
                    montoListaService.eliminarMontos(row.id,function(response){
                        if(response.response === 'OK'){
                            _.remove(self.tablaMontos.settings().dataset,function(item){
                                return row === item;
                            });
                            self.tablaMontos.reload();
                        }else {
                            modal.mensaje("ERROR","No se puede eliminar el cargo por defecto");
                        }

                    },function(errResponse){
                        modal.mensaje("ERROR","El servidor no responde");
                    });
            },function () {
                    modal.mensaje("CANCELAR","Se cancelo la accion");
                });
        }
        self.editarMonto = function(row){
           
                    var resolve = {
                        data:function(){
                            return{
                                monto : angular.copy(row),
                                niveles:[]
                        }
                        }
                    };
                    
                    var modalInstance = util.openModal('nuevoMonto.html','editarMontoCtrl','lg','ctrl',resolve);
                    modalInstance.result.then(function (response) {
                        if(response.response === 'OK'){
                            angular.extend(row,response.data);
                        }else if(response.response === 'BAD'){
                            modal.mensaje("ERROR","Hubo un error en el servidor");
                        }
                    },function (errResponse) {
                        modal.mensaje("CANCELAR","Se cancelo la accion");
                    });
                    }

            
        
        
    }])
    .controller('registrarMontoCtrl',['$log','$uibModalInstance','MontoListaService','data',function($log,$uibModalInstance,montoListaService,data){
        var self = this;
        self.niveles = data;
        
        self.titulo = 'Registrar Articulo';
        self.anios = [{id: "1", anio: "2016"},{id: "2", anio: "2017"},{id: "3", anio: "2018"},{id: "4", anio: "2019"},{id: "5", anio: "2020"},{id: "6", anio: "2021"},{id: "7", anio: "2022"},
        {id: "8", anio: "2023"},{id: "9", anio: "2024"},{id: "10", anio: "2025"},{id: "11", anio: "2026"},{id: "12", anio: "2027"},{id: "13", anio: "2028"},{id: "14", anio: "2029"},{id: "15", anio: "2030"},
        {id: "16", anio: "2031"},{id: "17", anio: "2032"},{id: "18", anio: "2033"},{id: "19", anio: "2034"},{id: "20", anio: "2035"},{id: "21", anio: "2036"},{id: "22", anio: "2037"},{id: "23", anio: "2038"}];
        //self.monto.anio = self.anios[1];
        
        
        self.guardar = function (){
            montoListaService.registrarMonto(self.monto,function(r){
               
                if(r.response === 'BAD'){
                    $log.log(r.responseMsg);
                }
                else if(r.response === 'OK'){
                    $log.log(r.responseMsg);
                    $uibModalInstance.close(r.data);
                }
             },function (errResponse){
                 $log.log(errResponse);
                 $uibModalInstance.dismiss('cancel');
            });
        }
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }])

        .controller('editarMontoCtrl',['$log','$uibModalInstance','MontoListaService','data',function($log,$uibModalInstance,montoListaService,data){
        var self = this;
        self.monto = data.monto;
         self.anios = [{id: "1", anio: "2016"},{id: "2", anio: "2017"},{id: "3", anio: "2018"},{id: "4", anio: "2019"},{id: "5", anio: "2020"},{id: "6", anio: "2021"},{id: "7", anio: "2022"},
        {id: "8", anio: "2023"},{id: "9", anio: "2024"},{id: "10", anio: "2025"},{id: "11", anio: "2026"},{id: "12", anio: "2027"},{id: "13", anio: "2028"},{id: "14", anio: "2029"},{id: "15", anio: "2030"},
        {id: "16", anio: "2031"},{id: "17", anio: "2032"},{id: "18", anio: "2033"},{id: "19", anio: "2034"},{id: "20", anio: "2035"},{id: "21", anio: "2036"},{id: "22", anio: "2037"},{id: "23", anio: "2038"}];
               
        self.titulo = 'Editar Monto';
        //Tener primero a lista de niveles para hacer lo que esta en los comentarios, esta lista se la tienes que 
        //pasar antes de abrir el modal como tu metodo guardar
        
        /*self.niveles = data.niveles;
        self.monto.nivel = _.find(self.niveles,function(o){
            return o.id == data.monto.niv.id;
        });*/
         //self.monto.val = data.monto.val;
         self.monto.ani = _.find(self.anios,function(o){
             return o.anio === data.monto.ani;
         });
       /* self.monto = data.monto;
        var auxArt = _.find(self.anios,function(o){
            return o.nomt.toLowerCase() === self.monto.ani.toLowerCase();
        });
        self.monto.ani = auxArt;*/
        
        self.guardar = function(){
            montoListaService.editarMonto(self.monto,function (response) {
                response.data = self.monto;
                //response.data.niv = self.monto.ani.anio;
                $uibModalInstance.close(response);
            },function (errResponse) {
                $log.log(errResponse);
                $uibModalInstance.dismiss('cancel');
            });
        }
        self.cancelar = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }]);
