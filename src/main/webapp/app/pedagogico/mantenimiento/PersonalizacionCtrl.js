app.controller("PersonalizacionCtrl",["$rootScope","$scope","crud","modal", function ($rootScope,$scope,crud,modal) {
    $scope.ladoIzq=false;
    $scope.ladoDer=false;
    $scope.selecPlantilla=0;
    $scope.color=0;
    
    $scope.seleccionarImagen=function(ladoIzq,ladoDer){
        $scope.ladoIzq=ladoIzq;
        $scope.ladoDer=ladoDer;
        if(!ladoIzq && !ladoDer)
            $scope.selecPlantilla=1;
        else if(!ladoIzq && ladoDer)
            $scope.selecPlantilla=2;
        else if(ladoIzq && !ladoDer)
            $scope.selecPlantilla=3;
        else if(ladoIzq && ladoDer)
            $scope.selecPlantilla=4;
    };
    $scope.seleccionarColor=function(color){
        $scope.color=color;
    };
    $scope.guardarConfiguracion=function(){
        if($scope.selecPlantilla!=0 && $scope.color!=0){
            modal.mensajeConfirmacion($scope, "Esta seguro de guardar los cambios", function () {
                var request=crud.crearRequest('mantenimiento',1,'insertarPersonalizacion');
                request.setData({default: false, ladIzq: $scope.ladoIzq, ladDer:$scope.ladoDer, color:$scope.color, usuarioID:$scope.usuMaster.usuario.usuarioID});
                crud.insertar("/mantenimiento",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                },function(data){
                    console.info(data);
                }); 
            });                             
        }else{
            modal.mensaje("MENSAJE","Debe seleccionar el color y la plantilla");
        }
        
    };
    $scope.restaurarConfiguracion=function(){
        modal.mensajeConfirmacion($scope, "Esta seguro de restaurar la configuracion por defecto", function () {
            var request=crud.crearRequest('mantenimiento',1,'insertarPersonalizacion');
            request.setData({default: true, usuarioID:$scope.usuMaster.usuario.usuarioID});
            crud.insertar("/mantenimiento",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
            },function(data){
                console.info(data);
            }); 
        });
    };
}]);
