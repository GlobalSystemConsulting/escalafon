app.controller("planEstudiosCtrl", ["$scope", "$rootScope", "NgTableParams", "crud", "modal", function ($scope, $rootScope, NgTableParams, crud, modal) {

        $scope.myOrganizacion = $rootScope.usuMaster.organizacion.organizacionID;

        $scope.plan = {};
        $scope.nivel = {};

        $scope.buscarPlanEstudios = function (orgID) {
            //preparamos un objeto request
            var request = crud.crearRequest('planEstudios', 1, 'buscarVigente');
            request.setData({organizacionID: orgID});
            crud.listar("/cuadroHoras", request, function (response) {

                if (response.responseSta) {
                    $scope.plan = response.data;
                    $scope.plan.niveles.forEach(function (item) {
                        var j = buscarObjeto($scope.disenoActual.jornadas, "jornadaID", item.jornadaID);
                        item.jornada = j.nombre;
                        item.hLibre = j.hLibre;
                        item.hObligatoria = j.hObligatoria;
                        item.hTotal = j.hTotal;
                        item.hTutoria = j.hTutoria;
                        item.nivelID = j.nivelID;
                        item.nivel = buscarContenido($scope.disenoActual.niveles, "nivelID", "nombre", item.nivelID);
                        item.turno = buscarContenido($scope.turnos, "turnoID", "nombre", item.turnoID);
                        item.periodo = buscarContenido($scope.periodos, "periodoID", "nombre", item.periodoID);
                    });
                    $scope.calcularMatriz();
                }
            }, function (data) {
                console.info(data);
            });
        };
        $scope.registrarPlan = function (orgID) {
            $scope.plan.organizacionID = orgID;
            var request = crud.crearRequest('planEstudios', 1, 'insertarPlanEstudios');
            request.setData($scope.plan);

            modal.mensajeConfirmacion($scope, "seguro que desea quiere iniciar el plan de estudios", function () {
                crud.insertar("/cuadroHoras", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        $scope.plan.planID = response.data.planID;

                        $scope.plan.niveles.forEach(function (item, index) {
                            item.planNivelID = response.data.IDs[index];

                            var j = buscarObjeto($scope.disenoActual.jornadas, "jornadaID", item.jornadaID);
                            item.hLibre = j.hLibre;
                            item.hObligatoria = j.hObligatoria;
                            item.hTotal = j.hTotal;
                            item.hTutoria = j.hTutoria;

                        });
                        $scope.calcularMatriz();
                    }
                }, function (data) {
                    console.info(data);
                });
            }, '400');
        };

        $scope.buscarJornada = function () {
            $scope.nivel.jornadas = [];
            $scope.disenoActual.jornadas.forEach(function (item) {
                if (item.nivelID == $scope.nivel.nivelID)
                    $scope.nivel.jornadas.push(item);
            });
        };
        $scope.agregarNivel = function () {

            for (var i in $scope.plan.niveles)
                if ($scope.plan.niveles[i].jornadaID == $scope.nivel.jornadaID && $scope.plan.niveles[i].turnoID == $scope.nivel.turnoID) {
                    modal.mensaje("CONFIRMACION", "No puede existir una jornada educativa con el mismo turno");
                    return;
                }

            if (!$scope.plan.planID) {
                if (!$scope.plan.niveles)
                    $scope.plan.niveles = [];

                $scope.nivel.nivel = buscarContenido($scope.disenoActual.niveles, "nivelID", "nombre", $scope.nivel.nivelID);
                $scope.nivel.jornada = buscarContenido($scope.disenoActual.jornadas, "jornadaID", "nombre", $scope.nivel.jornadaID);
                $scope.nivel.turno = buscarContenido($scope.turnos, "turnoID", "nombre", $scope.nivel.turnoID);
                $scope.nivel.periodo = buscarContenido($scope.periodos, "periodoID", "nombre", $scope.nivel.periodoID);

                $scope.nivel.descripcion = $scope.nivel.nivel + " turno " + $scope.nivel.turno;

                $scope.plan.niveles.push($scope.nivel);
                $scope.nivel = {};
            }
            else {
                var request = crud.crearRequest('planEstudios', 1, 'persistenciaPlanNivel');
                var nivelT = buscarContenido($scope.disenoActual.niveles, "nivelID", "nombre", $scope.nivel.nivelID);
                var turnoT = buscarContenido($scope.turnos, "turnoID", "nombre", $scope.nivel.turnoID);

                $scope.nivel.planID = $scope.plan.planID;
                $scope.nivel.descripcion = nivelT + " turno " + turnoT;
                request.setData($scope.nivel);
                modal.mensajeConfirmacion($scope, "seguro que desea insertar el nuevo nivel al plan de estudios", function () {
                    crud.insertar("/cuadroHoras", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {
                            $scope.nivel.planNivelID = response.data.planNivelID;
                            $scope.nivel.nivel = nivelT;
                            $scope.nivel.jornada = buscarObjeto($scope.disenoActual.jornadas, "jornadaID", $scope.nivel.jornadaID);
                            var j = buscarObjeto($scope.disenoActual.jornadas, "jornadaID", $scope.nivel.jornadaID);
                            $scope.nivel.jornada = j.nombre;
                            $scope.nivel.hLibre = j.hLibre;
                            $scope.nivel.hObligatoria = j.hObligatoria;
                            $scope.nivel.htutoria = j.hTutoria;
                            $scope.nivel.hTotal = j.hTotal;

                            $scope.nivel.turno = turnoT;
                            $scope.nivel.periodo = buscarContenido($scope.periodos, "periodoID", "nombre", $scope.nivel.periodoID);

                            $scope.plan.niveles.push($scope.nivel);
                            $scope.nivel = {};

                            $scope.calcularMatriz();
                        }
                    }, function (data) {
                        console.info(data);
                    });
                }, '400');
            }

        };
        $scope.editarNivel = function (i, n) {
            //si estamso editando
            if (n.edi) {
                for (var j in $scope.plan.niveles)
                    if (j != i && $scope.plan.niveles[j].jornadaID == n.copia.jornadaID && $scope.plan.niveles[j].turnoID == n.copia.turnoID) {
                        modal.mensaje("CONFIRMACION", "No puede existir una jornada educativa con el mismo turno");
                        return;
                    }
                if (!$scope.plan.planID) {
                    n.copia.nivel = buscarContenido($scope.disenoActual.niveles, "nivelID", "nombre", n.copia.nivelID);
                    var j = buscarObjeto($scope.disenoActual.jornadas, "jornadaID", n.copia.jornadaID);
                    n.copia.jornada = j.nombre;
                    n.copia.hLibre = j.hLibre;
                    n.copia.hObligatoria = j.hObligatoria;
                    n.copia.hTotal = j.hTotal;
                    n.copia.hTutoria = j.hTutoria;
                    n.copia.nivelID = j.nivelID;

                    n.copia.turno = buscarContenido($scope.turnos, "turnoID", "nombre", n.copia.turnoID);
                    n.copia.periodo = buscarContenido($scope.periodos, "periodoID", "nombre", n.copia.periodoID);
                    $scope.plan.niveles[i] = n.copia;
                }
                else {
                    var request = crud.crearRequest('planEstudios', 1, 'persistenciaPlanNivel');
                    n.copia.planID = $scope.plan.planID;
                    request.setData(n.copia);
                    modal.mensajeConfirmacion($scope, "seguro que desea editar el nivel del plan de estudios", function () {
                        crud.insertar("/cuadroHoras", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                            if (response.responseSta) {
                                n.copia.nivel = buscarContenido($scope.disenoActual.niveles, "nivelID", "nombre", n.copia.nivelID);
                                var j = buscarObjeto($scope.disenoActual.jornadas, "jornadaID", n.copia.jornadaID);
                                n.copia.jornada = j.nombre;
                                n.copia.hLibre = j.hLibre;
                                n.copia.hObligatoria = j.hObligatoria;
                                n.copia.hTotal = j.hTotal;
                                n.copia.hTutoria = j.hTutoria;
                                n.copia.nivelID = j.nivelID;

                                n.copia.turno = buscarContenido($scope.turnos, "turnoID", "nombre", n.copia.turnoID);
                                n.copia.periodo = buscarContenido($scope.periodos, "periodoID", "nombre", n.copia.periodoID);
                                $scope.plan.niveles[i] = n.copia;
                                $scope.calcularMatriz();
                            }
                        }, function (data) {
                            console.info(data);
                        });
                    }, '400');
                }
            }
            //si queremos editar
            else {
                n.copia = JSON.parse(JSON.stringify(n));
                n.edi = true;
            }
        };
        $scope.eliminarNivel = function (i, r) {
            //si estamso cancelando la edicion
            if (r.edi) {
                r.edi = false;
                delete r.copia;
            }
            //si queremos eliminar el elemento
            /*else{
             $scope.usuarioSel.sessiones.splice(i,1);
             }*/
        };
        $scope.editarSecciones = function (m, i) {
            //si estamso editando
            if (m.seccionesEdi != null) {
                var horas = $scope.matriz[m.n].turnos[m.t].hTotal * m.seccionesEdi;
                var meta = JSON.parse(JSON.stringify(m));

                meta.secciones = m.seccionesEdi;
                meta.horas = horas;
                meta.carga = m.alumnos / m.seccionesEdi;
                meta.carga = meta.carga.toFixed(2);

                var request = crud.crearRequest('planEstudios', 1, 'persistenciaMetaAtencion');
                request.setData(meta);
                modal.mensajeConfirmacion($scope, "seguro que desea editar el numero de secciones", function () {
                    crud.insertar("/cuadroHoras", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {

                            var horasAnt = horas - $scope.matriz[m.n].turnos[m.t].hTotal * m.secciones;

                            $scope.matriz[m.n].secciones = $scope.matriz[m.n].secciones - m.secciones + m.seccionesEdi;
                            $scope.matriz[m.n].horas = $scope.matriz[m.n].horas + horasAnt;
                            $scope.matriz[m.n].turnos[m.t].secciones = $scope.matriz[m.n].turnos[m.t].secciones - m.secciones + m.seccionesEdi;
                            $scope.matriz[m.n].turnos[m.t].horas = $scope.matriz[m.n].turnos[m.t].horas + horasAnt;
                            $scope.matriz[m.n].turnos[m.t].grados[i].secciones = m.seccionesEdi;
                            $scope.matriz[m.n].turnos[m.t].grados[i].horas = horas;
                            $scope.matriz[m.n].turnos[m.t].grados[i].carga = m.alumnos / m.seccionesEdi;
                            $scope.matriz[m.n].turnos[m.t].grados[i].carga = $scope.matriz[m.n].turnos[m.t].grados[i].carga.toFixed(2);

                            $scope.matriz[m.n].turnos[m.t].grados[i].metaID = response.data.metaID;

                            delete $scope.matriz[m.n].turnos[m.t].grados[i].seccionesEdi;
                        }
                    }, function (data) {
                        console.info(data);
                    });
                }, '400');
                //delete $scope.matriz[m.n].turnos[m.t].grados[i].seccionesEdi; 
            }
            //si queremos editar
            else {
                m.seccionesEdi = m.secciones;
            }
        };
        $scope.editarAlumnos = function (m, i) {
            //si estamso editando
            if (m.alumnosEdi != null) {

                var meta = JSON.parse(JSON.stringify(m));

                meta.alumnos = m.alumnosEdi;
                meta.carga = meta.alumnos / meta.secciones;
                if (isFinite(meta.carga)) {
                    meta.carga = meta.carga.toFixed(2);
                }
                else
                    meta.carga = 0.0;

                var request = crud.crearRequest('planEstudios', 1, 'persistenciaMetaAtencion');
                request.setData(meta);
                modal.mensajeConfirmacion($scope, "seguro que desea editar el numero de alumnos", function () {
                    crud.insertar("/cuadroHoras", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {

                            $scope.matriz[m.n].alumnos = $scope.matriz[m.n].alumnos - m.alumnos + m.alumnosEdi;
                            $scope.matriz[m.n].turnos[m.t].alumnos = $scope.matriz[m.n].turnos[m.t].alumnos - m.alumnos + m.alumnosEdi;
                            $scope.matriz[m.n].turnos[m.t].grados[i].alumnos = m.alumnosEdi;
                            $scope.matriz[m.n].turnos[m.t].grados[i].carga = meta.carga;

                            $scope.matriz[m.n].turnos[m.t].grados[i].metaID = response.data.metaID;

                            delete $scope.matriz[m.n].turnos[m.t].grados[i].alumnosEdi;
                        }
                    }, function (data) {
                        console.info(data);
                    });
                }, '400');
            }
            //si queremos editar
            else {
                m.alumnosEdi = m.alumnos;
            }
        };

        $scope.editarAreaHora = function (o) {
            if (!o)
                return;

            if (o.edi) {
                o.edi = false;
                delete o.copia;
            }
            //si queremos editar
            else {
                o.copia = JSON.parse(JSON.stringify(o));
                o.edi = true;
            }
        };
        $scope.agregarHoraDisponible = function (o, jor, i) {
            if (o.areaID == 0) {
                modal.mensaje("ALERTA", "el area asignada no tiene asignada horas obligatorias");
                return;
            }
            if (($scope.matris2.grados[i].totalL - o.horaL) + o.copia.horaL > jor.hLibre) {
                modal.mensaje("ALERTA", "la hora sobrepasa lo establecido por la jornada");
                return;
            }
            var request = crud.crearRequest('planEstudios', 1, 'persistenciaHoraDisponibilidad');
            request.setData(o.copia);
            modal.mensajeConfirmacion($scope, "seguro que desea editar la hora de libre disponibilidad para el grado y area curricular seleccionado", function () {
                crud.insertar("/cuadroHoras", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {

                        $scope.matris2.grados[i].totalL = ($scope.matris2.grados[i].totalL - o.horaL) + o.copia.horaL;
                        o.horaDisponibleID = response.data.horaDisponibleID;
                        o.horaL = o.copia.horaL;
                        o.edi = null;
                        delete o.copia;
                    }
                }, function (data) {
                    console.info(data);
                });
            }, '400');
        };

        $scope.agregarGradoTaller = function (jor) {
            var posIni = $scope.matris2.grados[0].i;
            if (!$scope.gradoTaller.grado || !$scope.gradoTaller.area) {
                modal.mensaje("ALERTA", "seleccione el taller para el grado");
                return;
            }
            if ($scope.gradoTaller.horaL <= 0) {
                modal.mensaje("ALERTA", "la hora no puede ser 0 o menor");
                return;
            }
            if ($scope.matris2.gradoTalleres[$scope.gradoTaller.area.i] && $scope.matris2.gradoTalleres[$scope.gradoTaller.area.i][$scope.gradoTaller.grado.i - posIni]) {
                modal.mensaje("ALERTA", "ya se registro el taller seleccionado");
                $scope.gradoTaller.area = {};
                return;
            }
            if ($scope.gradoTaller.grado.totalL + $scope.gradoTaller.horaL > jor.hLibre) {
                modal.mensaje("ALERTA", "la hora sobrepasa lo establecido por la jornada");
                return;
            }

            var request = crud.crearRequest('planEstudios', 1, 'persistenciaHoraDisponibilidad');
            var nuevoGradoHora = {horaDisponibleID: 0, planNivelID: jor.planNivelID, areaID: $scope.gradoTaller.area.areaID, gradoID: $scope.gradoTaller.grado.gradoID, horaL: $scope.gradoTaller.horaL};
            request.setData(nuevoGradoHora);
            modal.mensajeConfirmacion($scope, "seguro que desea agregar el taller al grado", function () {
                crud.insertar("/cuadroHoras", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {


                        nuevoGradoHora.horaDisponibleID = response.data.horaDisponibleID;

                        if (!$scope.matris2.gradoTalleres[ $scope.gradoTaller.area.i ]) {
                            $scope.matris2.gradoTalleres[ $scope.gradoTaller.area.i ] = [];
                            $scope.matris2.f2++;
                        }

                        $scope.matris2.gradoTalleres[$scope.gradoTaller.area.i][$scope.gradoTaller.grado.i - posIni] = nuevoGradoHora;
                        $scope.gradoTaller.grado.totalL += $scope.gradoTaller.horaL;

                        for (var h in $scope.matris2.gradoTalleres) {
                            $scope.matris2.ini2 = Number(h);
                            break;
                        }
                        $scope.gradoTaller = {horaL: 0};
                        jor.horasDisponibles.push(nuevoGradoHora);
                    }
                }, function (data) {
                    console.info(data);
                });
            }, '400');
        };
        listarDatos();

        function listarDatos() {
            var request = crud.crearRequest('planEstudios', 1, 'listarTurnos');

            crud.listar("/cuadroHoras", request, function (response) {
                $scope.turnos = response.data;
            }, function (data) {
                console.info(data);
            });

            request = crud.crearRequest('planEstudios', 1, 'listarPeriodos');
            crud.listar("/cuadroHoras", request, function (response) {
                $scope.periodos = response.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        $scope.diseoCurricularVigente = function (orgID) {
            var request = crud.crearRequest('diseñoCurricular', 1, 'buscarVigentePorOrganizacion');
            request.setData({organizacionID: orgID});
            crud.listar("/cuadroHoras", request, function (response) {
                if (response.responseSta) {
                    $scope.disenoActual = response.data;
                    $scope.disenoActual.talleres = [];
                    $scope.disenoActual.areas.forEach(function (item) {
                        if (item.tipo)
                            $scope.disenoActual.talleres.push(item);
                    });
                    $scope.buscarPlanEstudios(orgID);
                }
            }, function (data) {
                console.info(data);
            });

            request = crud.crearRequest('diseñoCurricular', 1, 'listarJornadaEscolar');
            crud.listar("/cuadroHoras", request, function (response) {
                $scope.jornadas = response.data;
            }, function (data) {
                console.info(data);
            });
        };

        $scope.calcularMatriz = function () {
            var plan = $scope.plan;
            if (plan.niveles) {
                var niveles = [];
                var turnos = [];
                var nivelAnt = 0;

                plan.niveles.forEach(function (item) {

                    if (item.nivelID != nivelAnt) {
                        turnos = [];
                        niveles.push({nivel: item.nivel, turnos: turnos, secciones: 0, alumnos: 0, horas: 0, carga: 0});
                        nivelAnt = item.nivelID;
                    }

                    //obteniendo los grados por nivel
                    var grados = [];
                    var contador = {s: 0, a: 0, h: 0};
                    $scope.disenoActual.grados.forEach(function (item2) {
                        if (item2.nivelID == item.nivelID) {
                            var meta = buscarObjeto(item.metas, 'gradoID', item2.gradoID);
                            var grado = {};
                            if (meta) {
                                grado = meta;
                                grado.grado = item2.nombre;
                                grado.n = niveles.length - 1;
                                grado.t = turnos.length;
                            }
                            else
                                grado = {planNivelID: item.planNivelID, gradoID: item2.gradoID, grado: item2.nombre, secciones: 0, alumnos: 0, horas: 0, carga: 0, n: niveles.length - 1, t: turnos.length};

                            contador.s += grado.secciones;
                            contador.a += grado.alumnos;
                            contador.h += grado.horas;
                            grados.push(grado);
                        }
                    });
                    turnos.push({turno: item.turno, jornada: item.jornada, r: grados.length + 1, grados: grados, secciones: contador.s, alumnos: contador.a, horas: contador.h, carga: 0, hTotal: item.hTotal});

                    niveles[niveles.length - 1].secciones += contador.s;
                    niveles[niveles.length - 1].alumnos += contador.a;
                    niveles[niveles.length - 1].horas += contador.h;


                });
                $scope.matriz = niveles;
            }
        };

        $scope.calcularMatriz2 = function (jornada) {

            $scope.matris2 = {};


            var disenoActual = $scope.disenoActual;
            if (jornada && disenoActual.grados) {

                var nivelID = jornada.nivelID;


                var grados = [];
                var ciclos = [];
                var cAnt = {id: 0, c: 1, nom: ""};

                disenoActual.grados.forEach(function (item) {

                    if (nivelID == item.nivelID) {
                        item.total = 0;
                        item.totalL = 0;
                        grados.push(item);
                        if (cAnt.id == item.cicloID)
                            cAnt.c++;
                        else {
                            cAnt = {id: item.cicloID, c: 1, nom: item.ciclo};
                            ciclos.push(cAnt);
                        }
                    }
                });

                $scope.matris2.grados = grados;
                $scope.matris2.ciclos = ciclos;
                $scope.matris2.c = grados.length;
                $scope.matris2.f = 0;

                $scope.matris2.gradoAreas = [];

                var jorSel = buscarObjeto($scope.jornadas, 'jornadaID', jornada.jornadaID);

                var posIni = grados[0].i;
                disenoActual.gradoAreas.forEach(function (item) {
                    if (item.nivelID == jorSel.nivelID) {

                        if (!$scope.matris2.gradoAreas[ item.areaPos ]) {
                            $scope.matris2.gradoAreas[ item.areaPos ] = [];
                            $scope.matris2.f++;
                        }
                        $scope.matris2.gradoAreas[ item.areaPos ][item.gradoPos - posIni] = {horaDisponibleID: 0, planNivelID: jornada.planNivelID, areaID: 0, gradoID: 0, hora: 0, horaL: 0};
                        for (var h in jorSel.horas) {
                            var obj = jorSel.horas[h];
                            if (obj.gradoID == item.gradoID && obj.areaID == item.areaID) {
                                $scope.matris2.gradoAreas[ item.areaPos ][item.gradoPos - posIni] = obj;
                                $scope.matris2.gradoAreas[ item.areaPos ][item.gradoPos - posIni].horaDisponibleID = 0;
                                $scope.matris2.gradoAreas[ item.areaPos ][item.gradoPos - posIni].planNivelID = jornada.planNivelID;
                                $scope.matris2.gradoAreas[ item.areaPos ][item.gradoPos - posIni].horaL = 0;
                                break;
                            }
                        }
                        //buscando las horas de libre disponibilidad
                        for (var h in jornada.horasDisponibles) {
                            var obj = jornada.horasDisponibles[h];
                            if (obj.gradoID == item.gradoID && obj.areaID == item.areaID) {
                                $scope.matris2.gradoAreas[ item.areaPos ][item.gradoPos - posIni].horaDisponibleID = obj.horaDisponibleID;
                                $scope.matris2.gradoAreas[ item.areaPos ][item.gradoPos - posIni].horaL = obj.horaL;
                                break;
                            }
                        }
                        $scope.matris2.grados[item.gradoPos - posIni].total += $scope.matris2.gradoAreas[ item.areaPos ][item.gradoPos - posIni].hora;
                        $scope.matris2.grados[item.gradoPos - posIni].totalL += $scope.matris2.gradoAreas[ item.areaPos ][item.gradoPos - posIni].horaL;
                    }
                });
                //obteniendo el label del primer elemento de los areas curriculares
                for (var h in $scope.matris2.gradoAreas) {
                    $scope.matris2.ini = Number(h);
                    break;
                }
                $scope.matris2.gradoTalleres = [];
                $scope.gradoTaller = {horaL: 0};
                $scope.matris2.f2 = 0;
                //buscando talleres
                if (jornada.horasDisponibles)
                    jornada.horasDisponibles.forEach(function (item) {
                        for (var h in $scope.disenoActual.talleres) {
                            var obj = $scope.disenoActual.talleres[h];
                            if (obj.tipo && item.areaID == obj.areaID) {
                                if (!$scope.matris2.gradoTalleres[ obj.i ]) {
                                    $scope.matris2.gradoTalleres[ obj.i ] = [];
                                    $scope.matris2.f2++;
                                }
                                //buscando el grado correspondiente
                                for (var g in $scope.matris2.grados) {
                                    if ($scope.matris2.grados[g].gradoID == item.gradoID) {
                                        $scope.matris2.gradoTalleres[ obj.i ][g] = item;
                                        $scope.matris2.grados[g].totalL += item.horaL;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    });
                //obteniendo el label del primer elemento de los talleres
                for (var h in $scope.matris2.gradoTalleres) {
                    $scope.matris2.ini2 = Number(h);
                    break;
                }
            }
        };

        $scope.iniciarProcesoMatriculas = function () {
            modal.mensajeConfirmacion($scope, "¿Esta seguro de Iniciar el Proceso de Matriculas?, se inhabilitaran las matriculas correspondientes al plan de estudios actual.", function () {
                var itmRequest = {
                    orgIdUser: $scope.myOrganizacion
                };
                var request = crud.crearRequest('matriculaIndividual', 1, 'generarGradosySeccion');
                request.setData(itmRequest);
                crud.listar("/matriculaInstitucional", request, function (data) {
                    if (data.responseSta) {
                        modal.mensaje("Operacion Exitosa", "Se generaro las Aulas en base al plan de estudios vigente");
                    } else {
                        modal.mensaje("Error", "Error al Generar las Aulas");
                    }
                }, function (data) {
                    console.info(data);
                });
            });
        };
    }]);