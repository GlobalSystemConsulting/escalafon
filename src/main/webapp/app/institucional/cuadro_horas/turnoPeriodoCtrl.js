app.controller("turnoPeriodoCtrl",["$scope","crud","modal", function ($scope,crud,modal){
     
    $scope.objeto = {nombre:"",descripcion:"",estado:'A'};
    $scope.objetoSel = {};
    $scope.turnos = [];
    $scope.periodos = [];
    $scope.tipoObj = true;
    
    $scope.listarTurnos = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('planEstudios',1,'listarTurnos');
        crud.listar("/cuadroHoras",request,function(data){
            $scope.turnos = data.data;
        },function(data){
            console.info(data);
        });
    };
    $scope.listarPeriodos = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('planEstudios',1,'listarPeriodos');
        crud.listar("/cuadroHoras",request,function(data){
            $scope.periodos = data.data;
        },function(data){
            console.info(data);
        });
    };
    $scope.prepararAgregar = function(tipoObj){
        $scope.tipoObj = tipoObj;
        $('#modalNuevo').modal('show');        
    };
    $scope.agregarObjeto = function(){
        
        if($scope.tipoObj){
            var request = crud.crearRequest('planEstudios',1,'persistenciaTurno');
            request.setData($scope.objeto);
            crud.insertar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.objeto.turnoID = response.data.turnoID;
                    $scope.turnos.push($scope.objeto);
                    $scope.objeto = {nombre:"",descripcion:"",estado:'A'};
                    $('#modalNuevo').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
        }
        else{
            var request = crud.crearRequest('planEstudios',1,'persistenciaPeriodo');
            request.setData($scope.objeto);
            crud.insertar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.objeto.periodoID = response.data.periodoID;
                    $scope.periodos.push($scope.objeto);
                    $scope.objeto = {nombre:"",descripcion:"",estado:'A'};
                    $('#modalNuevo').modal('hide');
                }
            },function(data){
                console.info(data);
            });
        }
    };
    $scope.eliminarTurno = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el turno seleccionado",function(){
            var request = crud.crearRequest('planEstudios',1,'eliminarTurnoPeriodo');
            request.setData({turnoID:idDato});
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    $scope.turnos.splice(i,1);
            },function(data){
                console.info(data);
            });
        },'400');
    };
    $scope.eliminarPeriodo = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el periodo seleccionado",function(){
            var request = crud.crearRequest('planEstudios',1,'eliminarTurnoPeriodo');
            request.setData({periodoID:idDato});
            crud.eliminar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    $scope.periodos.splice(i,1);
            },function(data){
                console.info(data);
            });
        },'400');
    };
    $scope.prepararEditar = function(i,t,tipObj){
        $scope.tipoObj = tipObj;
        $scope.objetoSel = JSON.parse(JSON.stringify(t));
        $scope.objetoSel.i = i;
        $('#modalEditar').modal('show');
    };
    $scope.editarObjeto = function(){
        
        if($scope.tipoObj){
        
            var request = crud.crearRequest('planEstudios',1,'persistenciaTurno');
            request.setData($scope.objetoSel);

            crud.actualizar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.turnos[$scope.objetoSel.i] = $scope.objetoSel;
                    $('#modalEditar').modal('hide');
                }
            },function(data){
                console.info(data);
            });
        }
        else{
            var request = crud.crearRequest('planEstudios',1,'persistenciaPeriodo');
            request.setData($scope.objetoSel);

            crud.actualizar("/cuadroHoras",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.periodos[$scope.objetoSel.i] = $scope.objetoSel;
                    $('#modalEditar').modal('hide');
                }
            },function(data){
                console.info(data);
            });
        }
    };
    
}]);