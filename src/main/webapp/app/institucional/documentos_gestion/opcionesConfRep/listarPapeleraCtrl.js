app.controller("listarPapeleraCtrl", ["$scope", "crud", "modal", function infoController($scope, crud, modal) {
        $scope.tipDoc = {nom: "", abr: "", des: "", fec: new Date()};
        $scope.documentos = [];

        //Listar todos los tipos de documentos
        $scope.listarPapelera = function () {
            var request = crud.crearRequest('entornoInicio', 1, 'listarPapelera');
            crud.listar("/documentosGestion", request, function (data) {
                //colocar la data de la base a la variable del ambito
                console.info(data.data);
                $scope.documentos = data.data[0];
            }, function (data) {
                //console.info(data);
            });
        };

        $scope.restaurarDoc = function (i,docSelected) {
            modal.mensajeConfirmacion($scope, "seguro que desea restaurar el documento", function () {
                var request = crud.crearRequest('entornoInicio', 1, 'restaurarDoc');
                request.setData({idDoc: docSelected.iteIde});
                crud.actualizar("/documentosGestion", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        console.info("Se restauro el(los) documento(s) exitosamente");
                    }
                }, function (data) {
                    console.info(data);
                });
            });
        };

        $scope.suprimirDoc = function (i, docId) {
            modal.mensajeConfirmacion($scope, "seguro que desea eliminar completamente el registro", function () {
                var request = crud.crearRequest('entornoInicio', 1, 'eliminarTotalDoc');
                request.setData({idDoc: docId});
                crud.eliminar("/documentosGestion", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                    if (response.responseSta) {
                        console.info("Se elimino Completamente el Archivo");
                    }
                }, function (data) {
                    console.info(data);
                });
            });
        };

        $scope.suprimirAllDocs = function () {
            //Establecer registro de un nuevo tipo de documento
            console.info("Iniciando Configuracion");
            var request = crud.crearRequest('entornoInicio', 1, 'insertarTipoDoc');
            //insertando resultando del contenido del archivo
            request.setData($scope.tipDoc);

            if ($scope.tipDoc.nom !== "" && $scope.tipDoc.des !== "" && $scope.tipDoc.abr !== "") {
                crud.insertar("/documentosGestion", request, function (response) {
                    modal.mensaje("CONFIRMACION HECHA", response.responseMsg);
                    console.info("Insercion Hecha Tipo Documento");
                    if (response.responseSta) {
                        //reiniciamos las variables
                        $scope.tipDoc = {nom: "", abr: "", des: "", fec: new Date()};
                        $('#modalNuevoTipDoc').modal('hide');
                    }
                }, function (data) {

                });
            } else {
                modal.mensaje("CONFIRMACIÓN", "Porfavor Rellene todos los campos");
            }
        };

    }]);



