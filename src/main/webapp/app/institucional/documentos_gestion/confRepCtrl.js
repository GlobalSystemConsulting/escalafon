app.controller("confRepCtrl", ["$scope", "crud", "modal", function infoController($scope, crud, modal) {
        
    }]);


app.config(['$routeProvider',function($routeProvider){
        $routeProvider.when('/listarTiposDoc',{
            templateUrl:'institucional/documentos_gestion/opcionesConfRep/listaTiposDoc.html',
            controller:'listaTiposDocCtrl'
        });
        $routeProvider.when('/listarPapelera',{
            templateUrl:'institucional/documentos_gestion/opcionesConfRep/listarPapelera.html',
            controller:'listarPapeleraCtrl'
        });
}]);
