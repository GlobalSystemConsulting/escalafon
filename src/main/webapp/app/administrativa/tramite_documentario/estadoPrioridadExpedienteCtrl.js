app.controller("estadoPrioridadExpedienteCtrl",["$scope","crud","modal", function ($scope,crud,modal){
     
    $scope.objeto = {nombre:"",descripcion:"",estado:'A'};
    $scope.objetoSel = {};
    $scope.estados = [];
    $scope.prioridades = [];
    $scope.tipoObj = true;
    
    $scope.listarEstadoExpedientes = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('tramite_datos',1,'listarEstadoExpedientes');
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.estados = data.data;
        },function(data){
            console.info(data);
        });
    };
    $scope.listarPrioridadExpedientes = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('tramite_datos',1,'listarPrioridadExpedientes');
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.prioridades = data.data;
        },function(data){
            console.info(data);
        });
    };
    $scope.prepararAgregar = function(tipoObj){
        $scope.tipoObj = tipoObj;
        $('#modalNuevo').modal('show');        
    };
    $scope.agregarObjeto = function(){
        
        if($scope.tipoObj){
            var request = crud.crearRequest('tramite_datos',1,'insertarEstadoExpediente');
            request.setData($scope.objeto);
            crud.insertar("/tramiteDocumentario",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.objeto.estadoExpedienteID = response.data.estadoExpedienteID;
                    //insertamos el elemento a la lista
                    $scope.estados.push($scope.objeto);
                    //reiniciamos las variables
                    $scope.objeto = {nombre:"",descripcion:"",estado:'A'};
                    //cerramos la ventana modal
                    $('#modalNuevo').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
        }
        else{
            var request = crud.crearRequest('tramite_datos',1,'insertarPrioridadExpediente');
            request.setData($scope.objeto);
            crud.insertar("/tramiteDocumentario",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.objeto.prioridadExpedienteID = response.data.prioridadExpedienteID;
                    //insertamos el elemento a la lista
                    $scope.prioridades.push($scope.objeto);
                    //reiniciamos las variables
                    $scope.objeto = {nombre:"",descripcion:"",estado:'A'};
                    //cerramos la ventana modal
                    $('#modalNuevo').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
        }
    };
    $scope.eliminarEstadoExpediente = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el estado del expediente",function(){
            var request = crud.crearRequest('tramite_datos',1,'eliminarEstadoExpediente');
            request.setData({estadoExpedienteID:idDato});
            crud.eliminar("/tramiteDocumentario",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    $scope.estados.splice(i,1);
            },function(data){
                console.info(data);
            });
        });
    };
    $scope.eliminarPrioridadExpediente = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar la prioridad del expediente",function(){
            var request = crud.crearRequest('tramite_datos',1,'eliminarPrioridadExpediente');
            request.setData({prioridadExpedienteID:idDato});
            crud.eliminar("/tramiteDocumentario",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    $scope.prioridades.splice(i,1);
            },function(data){
                console.info(data);
            });
        });
    };
    $scope.prepararEditar = function(i,t,tipObj){
        $scope.tipoObj = tipObj;
        $scope.objetoSel = JSON.parse(JSON.stringify(t));
        $scope.objetoSel.i = i;
        $('#modalEditar').modal('show');
    };
    $scope.editarObjeto = function(){
        
        if($scope.tipoObj){
        
            var request = crud.crearRequest('tramite_datos',1,'actualizarEstadoExpediente');
            request.setData($scope.objetoSel);

            crud.actualizar("/tramiteDocumentario",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.estados[$scope.objetoSel.i] = $scope.objetoSel;
                    $('#modalEditar').modal('hide');
                }
            },function(data){
                console.info(data);
            });
        }
        else{
            var request = crud.crearRequest('tramite_datos',1,'actualizarPrioridadExpediente');
            request.setData($scope.objetoSel);

            crud.actualizar("/tramiteDocumentario",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.prioridades[$scope.objetoSel.i] = $scope.objetoSel;
                    $('#modalEditar').modal('hide');
                }
            },function(data){
                console.info(data);
            });
        }
    };
    
}]);