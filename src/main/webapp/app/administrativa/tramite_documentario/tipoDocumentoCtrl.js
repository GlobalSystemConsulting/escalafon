app.controller("tipoDocumentoCtrl",["$scope","crud","modal", function ($scope,crud,modal){
     
    $scope.tipoDocumento = {nombre:"",descripcion:"",estado:'A'};
    $scope.tipoDocumentoSel = {};
    
    $scope.documentos = [];
    
    $scope.listarTipoDocumentos = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('tramite_datos',1,'listarTipoDocumentos');
        crud.listar("/tramiteDocumentario",request,function(data){
            $scope.documentos = data.data;
        },function(data){
            console.info(data);
        });
    };
    $scope.agregarTipoDocumento = function(){
        
        var request = crud.crearRequest('tramite_datos',1,'insertarTipoDocumento');
        request.setData($scope.tipoDocumento);
        
        crud.insertar("/tramiteDocumentario",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.tipoDocumento.tipoDocumentoID = response.data.tipoDocumentoID;
                //insertamos el elemento a la lista
                $scope.documentos.push($scope.tipoDocumento);
                //reiniciamos las variables
                $scope.tipoDocumento = {nombre:"",descripcion:"",estado:'A'};
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    $scope.eliminarTipoDocumento = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el Tipo de documento",function(){
            
            var request = crud.crearRequest('tramite_datos',1,'eliminarTipoDocumento');
            request.setData({tipoDocumentoID:idDato});

            crud.eliminar("/tramiteDocumentario",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    $scope.documentos.splice(i,1);
            },function(data){
                console.info(data);
            });
        });
    };
    $scope.prepararEditar = function(i,t){
        
        $scope.tipoDocumentoSel = JSON.parse(JSON.stringify(t));
        $scope.tipoDocumentoSel.i = i;
        $('#modalEditar').modal('show');
    };
    $scope.editarTipoDocumento = function(){
        
        var request = crud.crearRequest('tramite_datos',1,'actualizarTipoDocumento');
        request.setData($scope.tipoDocumentoSel);
                
        crud.actualizar("/tramiteDocumentario",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                $scope.documentos[$scope.tipoDocumentoSel.i] = $scope.tipoDocumentoSel;
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
    };
    
}]);