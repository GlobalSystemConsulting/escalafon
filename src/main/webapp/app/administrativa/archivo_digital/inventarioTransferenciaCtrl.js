app.controller("inventarioTransferenciaCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
        $scope.inventarios_transferencia=[];
        $scope.prestamos_serie =[];
        $scope.areas = [];
        $scope.unidades_organicas = [];
        $scope.series_documentales = [];
        $scope.archivos = [];
        $scope.inventario_transferencia = {};
        
        
        //Datos de la tabla Iventario de Series Documentales
        var params = {count: 20};
        var setting = { counts: []};
        $scope.tabla_inventarios = new NgTableParams(params, setting);
        
        var params2 = {count: 20};
        var setting2 = { counts: []}; 
        $scope.tabla_archivos = new NgTableParams(params2,setting2);
        
        var editar_archivo = false;

        //INICIALIZAMOS NUESTRO INVENTARIO DETALLE DE TRANSFERENCIA ACTUAL
        /*
        $scope.inventario_transferencia.asu = 0;
        $scope.inventario_transferencia.met_lin=0;
        $scope.inventario_transferencia.res_con="";
        $scope.inventario_transferencia.loc=0;
        $scope.inventario_transferencia.alm=0;
        $scope.inventario_transferencia.est=0;
        $scope.inventario_transferencia.bal=0;
        $scope.inventario_transferencia.fil=0;
        $scope.inventario_transferencia.cue=0;
        $scope.inventario_transferencia.caj=0;
        */
        
        
   $scope.listarInventarios = function(){
      
       var request = crud.crearRequest('inventario_transferencia',1,'listar_inventario_transferencia');
       request.setData({serie_id:1});
       crud.listar("/archivoDigital",request,function(data){
       $scope.inventarios_transferencia = data.data;
       $scope.setting.dataset = data.data;
       $scope.tabla_inventarios.settings(setting);
       
        //Listamos los Prestamos de serie documental
        var request2 = crud.crearRequest('inventario_transferencia',1,'listar_prestamos_serie');
       
       crud.listar("/archivoDigital",request2,function(data){
           
       $scope.prestamos_serie = data.data;
     //  $scope.CurrentDate = new Date();
       //Verificamos si por cada inventario existe un prestamo
       for(var i=0 ; i< $scope.inventarios_transferencia.length;i++){
           for(var j = 0;j<$scope.prestamos_serie.length;j++){
               if($scope.inventarios_transferencia[i].inv_tra_id == $scope.prestamos_serie[j].inv_tra_id){
                   $scope.inventarios_transferencia[i].prestamo = true;
                   $scope.inventarios_transferencia[i].fec_dev  = $scope.prestamos_serie[j].fec_dev;
                   $scope.inventarios_transferencia[i].fec_pre =  $scope.prestamos_serie[j].fec_pre;
                   $scope.inventarios_transferencia[i].etiqueta = "Inventario Prestado!" ;
                   /*
                   if($scope.prestamos_serie[j].fec_dev > $scope.CurrentDate){
                       $scope.inventarios_transferencia[i].fecha = 'A';
                   }
                   else{
                       $scope.inventarios_transferencia[i].fecha = 'I';
                   }
                   */
                   
               }
           }  
       }
       
       },function(data){
           console.info(data);
       });
     
       
       },function(data){
           console.info(data);
       });
     
     
   }
   
   
   $scope.listarAreas = function(){
       
       var request = crud.crearRequest('serie_documental',1,'listarArea');
       crud.listar("/archivoDigital",request,function(data){
       $scope.areas = data.data;
       },function(data){
           console.info(data);
       });
       
   };
   $scope.elegirUnidadOrganica = function(AreaID){
       
       var tipoArea = null;
        for(var i=0;i<$scope.areas.length;i++ )
            if($scope.areas[i].areaID == AreaID){
                tipoArea = $scope.areas[i];
                break;
            }
        if(tipoArea){
            var request = crud.crearRequest('gestion_area_archivo_central',1,'listar_unidad_organica');
            request.setData({codigo_area:tipoArea.areaID});
            crud.listar("/archivoDigital",request,function(data){
                 $scope.unidades_organicas = data.data;
                },function(data){
                    console.info(data);
                });
            
        }
        else{
            $scope.requisitos = [];
            $scope.rutas = [];
        }
      
   }
   $scope.listarSeries = function(){
       var request = crud.crearRequest('gestion_series_documentales',1,'listar_series_documentales');
       var uniID =  $scope.inventario_transferencia.unID;
            request.setData({uni_org_id:uniID});
            crud.listar("/archivoDigital",request,function(data){
               // setting.dataset = data.data;
                $scope.series_documentales = data.data;
                },function(data){
                    console.info(data);
                });
       
   }
   $scope.elegir_codigo_serie = function(SerieID){
       
        for(var i=0;i<$scope.series_documentales.length;i++ )
            if($scope.series_documentales[i].serie_id == SerieID){
                    $scope.inventario_transferencia.cod_serie = $scope.series_documentales[i].cod_serie;
                break;
            }
   }
   
   
   $scope.registrar_inventario = function(){
       
       $scope.inventario_transferencia.fec = convertirFecha($scope.inventario_transferencia.fec);
       var request = crud.crearRequest('inventario_transferencia',1,'registrar_inventario_transferencia');
       request.setData($scope.inventario_transferencia);
       crud.insertar("/archivoDigital",request,function(response){
          modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
              
              $scope.buscar_serie($scope.inventario_transferencia);
              $scope.buscar_unidad($scope.inventario_transferencia);
              $scope.buscar_area($scope.inventario_transferencia);
              $scope.inventario_transferencia.inv_tra_id = response.data.det_inv_trans;
           
              insertarElemento(setting.dataset,$scope.inventario_transferencia);
              $('#modalNuevoInventario').modal('hide');
              //Ademas insertarmos a nuestro arreglo de inventarios de transferencia
              insertarElemento($scope.inventarios_transferencia.dataset,$scope.inventario_transferencia);
              
              $scope.tabla_inventarios.reload();
               
             
            }
           
       });
       
   }
    $scope.buscar_area = function(area){
        for(var i=0;i<$scope.areas.length;i++){
            if($scope.areas[i].areaID == area.areaID ){
                $scope.inventario_transferencia.area = $scope.areas[i].nombre_area;
                 break;  
            }
        }
   }
   
   $scope.buscar_serie = function(serie){
        for(var i=0;i<$scope.series_documentales.length;i++){
            if($scope.series_documentales[i].serie_id == serie.serie_id ){
                $scope.inventario_transferencia.serie = $scope.series_documentales[i].nom_serie;
                 break;  
            }
        }
   }
   
   $scope.buscar_unidad = function(unidad){
        
        for(var i=0;i<$scope.unidades_organicas.length;i++){
            if($scope.unidades_organicas[i].uni_org_id == unidad.unID ){
                $scope.inventario_transferencia.un_org = $scope.unidades_organicas[i].nom_uni;
                break;  
            }
        }
   }
   
   $scope.PrepararPrestamo = function(Inventario){
       
       $scope.inventario_transferencia.inv_tra_id = Inventario.inv_tra_id;
       $scope.inventario_transferencia.un_org   = Inventario.un_org;
       $scope.inventario_transferencia.serie   = Inventario.serie;
       $scope.inventario_transferencia.ser_doc_id = Inventario.ser_doc_id;
       
        $('#modalNuevoPrestamo').modal('show');
   }
   
   $scope.registrar_prestamo = function(){
       
       $scope.inventario_transferencia.fec_prestamo = convertirFecha($scope.inventario_transferencia.fec_prestamo);
       $scope.inventario_transferencia.fec_dev = convertirFecha($scope.inventario_transferencia.fec_dev);
       
       var request = crud.crearRequest('inventario_transferencia',1,'registrar_prestamo_inventario');
       request.setData($scope.inventario_transferencia);
       crud.insertar("/archivoDigital",request,function(response){
          modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
              
              
            for(var i=0;i<$scope.inventarios_transferencia.length;i++){
                if($scope.inventarios_transferencia[i].inv_tra_id == $scope.inventario_transferencia.inv_tra_id){
                    $scope.inventarios_transferencia[i].prestamo = true;
                    $scope.inventarios_transferencia[i].fec_dev = $scope.inventario_transferencia.fec_dev;
                    $scope.inventarios_transferencia[i].fec_pre = $scope.inventario_transferencia.fec_pre;
                    $scope.inventarios_transferencia[i].etiqueta = "Inventario Prestado!";
                    break;
                }
            }
            $scope.tabla_inventarios.reload();
             $('#modalNuevoPrestamo').modal('hide');
            }
           
       });
   }
   
   $scope.Registrar_Archivo = function(){
      
      
      $scope.inventario_transferencia.fecha = convertirFecha($scope.inventario_transferencia.fecha);
      
      if(editar_archivo == false){
            var request = crud.crearRequest('inventario_transferencia',1,'registrar_archivo');
            request.setData($scope.inventario_transferencia);  
             crud.insertar("/archivoDigital",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                            
                 $('#modalNuevoArchivo').modal('hide');
                 
                 $scope.nuevo_archivo={}; 
                 $scope.nuevo_archivo.arc_inv_tra_id = response.data.arc_inv_tra_id;
                 $scope.nuevo_archivo.det_inv_trans = response.data.det_inv_trans;
                 $scope.nuevo_archivo.num_fol = response.data.num_fol;
                 $scope.nuevo_archivo.titulo = response.data.titulo;
                 $scope.nuevo_archivo.fecha = response.data.fecha;
                 $scope.nuevo_archivo.cod_expediente = response.data.cod_expediente;
                 $scope.nuevo_archivo.nombre_archivo = response.data.nombre_archivo;
                           
                 insertarElemento(setting2.dataset,$scope.nuevo_archivo);
                 $scope.tabla_archivos.reload();          
            }
            else{
                var request = crud.crearRequest('inventario_transferencia',1,'editar_archivo');
                request.setData($scope.inventario_transferencia);
                crud.actualizar("/archivoDigital",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                  if(response.responseSta){  
                       $('#modalNuevoArchivo').modal('hide');
                  }
            });
                
            }
             });
      }
      else{
           
            var request = crud.crearRequest('inventario_transferencia',1,'editar_archivo');
            request.setData($scope.inventario_transferencia);
            crud.actualizar("/archivoDigital",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                  if(response.responseSta){
                      editar_archivo = false;
                  }
            });

      }
      
      
   }

   
   $scope.Listar_Archivos = function(Inventario){
       $scope.inventario_transferencia = Inventario;
        var request = crud.crearRequest('inventario_transferencia',1,'listar_archivos');
            request.setData(Inventario);
            crud.listar("/archivoDigital",request,function(data){
                setting2.dataset = data.data;
                $scope.archivos = data.data;
                },function(data){
                    console.info(data);
            });
            $('#modalListarArchivo').modal('show');
    
   }
   
   $scope.nuevo_archivo = function(){
       $scope.inventario_transferencia.titulo = "";
       $scope.inventario_transferencia.cod_expediente ="";
       $scope.inventario_transferencia.num_folio ="";
       
        $('#modalNuevoArchivo').modal('show');
   }
   
   $scope.EliminarInventario = function(Inventario){
       
       
           modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este Inventario ?",function(){
           var request = crud.crearRequest('inventario_transferencia',1,'eliminar_inventario_transferencia');
           request.setData(Inventario);
           
           crud.eliminar("/archivoDigital",request,
            function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                   // eliminarElemento(setting.dataset,i);
                   // $scope.tabla_series.reload();
                }

            },function(data){
            });
       
           });
       
    }
    $scope.EditarInventario = function(inventario){
        
        /*Buscamos el Inventario de Transferencia*/
        for(var i=0;i<$scope.inventarios_transferencia.length;i++){
            if($scope.inventarios_transferencia[i].inv_tra_id == inventario.inv_tra_id){
                $scope.inventario_transferencia = $scope.inventarios_transferencia[i];
                break;
            }
        }
        $('#modalEditarInventario').modal('show');
    }
    
    
    $scope.actualizar_inventario = function(){
 
        $scope.inventario_transferencia.fec = convertirFecha($scope.inventario_transferencia.fec);
        var request = crud.crearRequest('inventario_transferencia',1,'editar_inventario_transferencia');
            request.setData($scope.inventario_transferencia);
            crud.actualizar("/archivoDigital",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                  if(response.responseSta){
                      $scope.inventario_transferencia = null;
                       $('#modalEditarInventario').modal('hide');
                  }
            });
    
    }
    
    $scope.limpiar_inventario = function(){
        
        
        $('#modalNuevoInventario').modal('show');
    }
   
   
    $scope.cancelar_archivo = function(){
        
        $('#modalNuevoArchivo').modal('hide');
        
    }
    $scope.cancelar_inventario = function(){
        
        $scope.inventario_transferencia = null;
        $('#modalNuevoInventario').modal('hide');
    }
   
    $scope.EditarArchivo = function(archivo){
       
       for(var i = 0 ; i< $scope.archivos.length ; i++){
           if(archivo.arc_inv_tra_id == $scope.archivos[i].arc_inv_tra_id){
               
               $scope.inventario_transferencia.titulo = archivo.titulo;
               $scope.inventario_transferencia.cod_expediente = archivo.cod_expediente;
               $scope.inventario_transferencia.fecha = archivo.fecha;
               $scope.inventario_transferencia.num_folio = archivo.num_fol;
               $scope.inventario_transferencia.archivo = archivo.archivo;
               editar_archivo = true;
               $('#modalNuevoArchivo').modal('show');  
               break;
           }
       }
   }
   
   $scope.EliminarArchivo = function(archivo){
       
       var pos ;
       for(var i = 0;i<$scope.archivos.length;i++){
           if($scope.archivos[i].arc_inv_tra_id == archivo.arc_inv_tra_id){
               pos=i;
               break;
           }
       }
       
        modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este Archivo ?",function(){
           var request = crud.crearRequest('inventario_transferencia',1,'eliminar_archivo');
           request.setData(archivo);
           
           crud.eliminar("/archivoDigital",request,
            function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(setting2.dataset,pos);
                    $scope.tabla_archivos.reload();
                }

            },function(data){
            });
       
           });
     
    
     
   }
   
   
 }]);