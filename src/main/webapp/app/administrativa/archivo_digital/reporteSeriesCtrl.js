
 app.controller("reporteSeriesCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
      
      $scope.areas = [];
      $scope.area = {};

      $scope.verReporte = function(){
            var request = crud.crearRequest('gestion_series_documentales',1,'reporte_series');
            request.setData({area_id:$scope.area});        
            crud.listar("/archivoDigital",request,function(data){            
                 $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                    },function(data){
                    console.info(data);
            });   
      }  
      
      
    $scope.listarAreas = function(){
       
       var request = crud.crearRequest('serie_documental',1,'listarArea');
       crud.listar("/archivoDigital",request,function(data){
       $scope.areas = data.data;
       },function(data){
           console.info(data);
       });    
    };
    
 }]);