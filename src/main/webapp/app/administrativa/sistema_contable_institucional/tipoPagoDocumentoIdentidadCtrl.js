app.controller("tipoPagoDocumentoIdentidadCtrl",["$scope","crud","modal", function ($scope,crud,modal){
     
    $scope.objeto = {nombre:"",estado:'A'};
    $scope.objetoSel = {};
    $scope.tiposPagos = [];
    $scope.tiposDocumentos = [];
    $scope.tipoObj = true;
    
    $scope.listarTipoPago = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('contable_datos',1,'listarTipoPago');
        crud.listar("/sistemaContable",request,function(data){
            $scope.tiposPagos = data.data;
        },function(data){
            console.info(data);
        });
    };
    $scope.listarTipoDocumento = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('contable_datos',1,'listarTipoDocumentoIdentidad');
        crud.listar("/sistemaContable",request,function(data){
            $scope.tiposDocumentos = data.data;
        },function(data){
            console.info(data);
        });
    };
    $scope.prepararAgregar = function(tipoObj){
        $scope.tipoObj = tipoObj;
        $('#modalNuevo').modal('show');        
    };
    $scope.agregarObjeto = function(){
        
        if($scope.tipoObj){
            var request = crud.crearRequest('contable_datos',1,'insertarTipoPago');
            request.setData($scope.objeto);
            crud.insertar("/sistemaContable",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.objeto.tipoPagoID = response.data.tipoPagoID;
                    //insertamos el elemento a la lista
                    $scope.tiposPagos.push($scope.objeto);
                    //reiniciamos las variables
                    $scope.objeto = {nombre:"",estado:'A'};
                    //cerramos la ventana modal
                    $('#modalNuevo').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
        }
        else{
            var request = crud.crearRequest('contable_datos',1,'insertarTipoDocumentoIdentidad');
            request.setData($scope.objeto);
            crud.insertar("/sistemaContable",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    //recuperamos las variables que nos envio el servidor
                    $scope.objeto.tipoDocumentoID = response.data.tipoDocumentoID;
                    //insertamos el elemento a la lista
                    $scope.tiposDocumentos.push($scope.objeto);
                    //reiniciamos las variables
                    $scope.objeto = {nombre:"",estado:'A'};
                    //cerramos la ventana modal
                    $('#modalNuevo').modal('hide');
                }            
            },function(data){
                console.info(data);
            });
        }
    };
    $scope.eliminarTipoPago = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el tipo de pago",function(){
            var request = crud.crearRequest('contable_datos',1,'eliminarTipoPago');
            request.setData({tipoPagoID:idDato});
            crud.eliminar("/sistemaContable",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    $scope.tiposPagos.splice(i,1);
            },function(data){
                console.info(data);
            });
        });
    };
    $scope.eliminarTipoDocumento = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar el tipo de documento",function(){
            var request = crud.crearRequest('contable_datos',1,'eliminarTipoDocumentoIdentidad');
            request.setData({tipoDocumentoID:idDato});
            crud.eliminar("/sistemaContable",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta)
                    $scope.tiposDocumentos.splice(i,1);
            },function(data){
                console.info(data);
            });
        });
    };
    $scope.prepararEditar = function(i,t,tipObj){
        $scope.tipoObj = tipObj;
        $scope.objetoSel = JSON.parse(JSON.stringify(t));
        $scope.objetoSel.i = i;
        $('#modalEditar').modal('show');
    };
    $scope.editarObjeto = function(){
        
        if($scope.tipoObj){
        
            var request = crud.crearRequest('contable_datos',1,'actualizarTipoPago');
            request.setData($scope.objetoSel);

            crud.actualizar("/sistemaContable",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.tiposPagos[$scope.objetoSel.i] = $scope.objetoSel;
                    $('#modalEditar').modal('hide');
                }
            },function(data){
                console.info(data);
            });
        }
        else{
            var request = crud.crearRequest('contable_datos',1,'actualizarTipoDocumentoIdentidad');
            request.setData($scope.objetoSel);

            crud.actualizar("/sistemaContable",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    $scope.tiposDocumentos[$scope.objetoSel.i] = $scope.objetoSel;
                    $('#modalEditar').modal('hide');
                }
            },function(data){
                console.info(data);
            });
        }
    };
    
}]);