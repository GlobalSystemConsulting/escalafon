/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */





app.requires.push('angularModalService');
app.requires.push('ngAnimate');
app.requires.push('ui.bootstrap');
      

app.controller("reporteLibroCajaCtrl",["$scope","$rootScope","NgTableParams","crud","modal","ModalService", function ($scope,$rootScope,NgTableParams,crud,modal,ModalService){
    $scope.isCollapsed = false;
   
   $scope.seleccionar ={flagReporteL:false,flagReporteI:false,flagReporteE:false,flagReporteB:false};                       
 
    //variables de consulta BD
    $scope.consulta ={desde:"",hasta:"",organizacionID:$rootScope.usuMaster.organizacion.organizacionID,personaID:$rootScope.usuMaster.usuario.usuarioID};
    $scope.datosOrganizacion={};
    //dato del libro  
    $scope.libroEstado= {debe:0.00,haber:0.00,saldoMesAnterior:0.00,saldoMes:0.00,estado:"",mes:"",mesSel:"",libro:"",enunciado:""};
    
        $scope.libro={libroID:0,nombre:"",observacion:"",fechaApertura:"",fechaCierre:"",saldoApertura:0.00,saldoActual:0.00,organizacionID:0,personaID:0,estado:''};             
        $scope.libroEstado.mes=new Date();
  // $scope.libro.mesSel= $scope.libro.mes.getMonth().toString();
    
     
    //Variables para manejo de la Operacion del Libro Caja
    var paramsLibroCaja= {count: 10};
    var settingLibroCaja = { counts: []};
    $scope.tablaLibroCaja = new NgTableParams(paramsLibroCaja, settingLibroCaja);
    // $scope.tablaLibroCaja.filter().fecha=$scope.libro.mesSel;

    //Variables para manejo de la Operacion del Libro Caja
    var paramsCuentasIngreso= {count: 10 };
    var settingCuentasIngreso= { counts: []};
    $scope.tablaCuentasIngreso = new NgTableParams(paramsCuentasIngreso, settingCuentasIngreso);
     
    //Variables para manejo de la Operacion del Libro Caja
    var paramsCuentasEgreso= {count: 10 };
    var settingCuentasEgreso= { counts: []};
    $scope.tablaCuentasEgreso = new NgTableParams(paramsCuentasEgreso, settingCuentasEgreso);
     
    $scope.resultados=[] ;
    var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
   
  // verificaSaldo();
  // listarLibroCaja();               
        
  $scope.verReporte =function (){
     
        //preparamos un objeto request               
      var request = crud.crearRequest('libroCaja',1,'reporteLibro');        
        $scope.consulta.fechaD=$scope.consulta.desde.toString();
        $scope.consulta.fechaH=$scope.consulta.hasta.toString();
        request.setData($scope.consulta); 
        //llamamos al servicio          
        crud.listar("/sistemaContable",request,function(response){
            //  modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                console.log(response.data);
             if (response.data.estado==='I'|| response.data.estado==='C' || response.data.estado==='N'){
                  $scope.libroEstado.estado=response.data.estado;
                 modal.mensaje("CONFIRMACION",response.responseMsg);
             }  
             else{
                modal.mensaje("CONFIRMACION",response.responseMsg);  
               //   $scope.libroEstado=response.data;
              //    $scope.libroEstado.mes= new Date();
             //     $scope.libroEstado.mesSel=$scope.libroEstado.mes.getMonth().toString();                  
            //      if($scope.libroEstado.mes.getMonth()<9)
           //           $scope.tablaLibroCaja.filter().fecha="/0"+ (parseFloat($scope.libroEstado.mesSel)+ 1) +"/";
          //        else
         //             $scope.tablaLibroCaja.filter().fecha="/"+ (parseFloat($scope.libroEstado.mesSel)+ 1) +"/";


                  $scope.datosOrganizacion=response.data.datosOrganizacion;
                  settingLibroCaja.dataset =response.data.asientos;
                  $scope.tablaLibroCaja.settings(settingLibroCaja);
                  settingCuentasIngreso.dataset =response.data.cuentasIngreso;
                  $scope.tablaCuentasIngreso.settings(settingCuentasIngreso);
                  $scope.totalIngreso = response.data.totalIngreso;
                  settingCuentasEgreso.dataset =response.data.cuentasEgreso;
                  $scope.tablaCuentasEgreso.settings(settingCuentasEgreso);
                  $scope.totalEgreso = response.data.totalEgreso;
               
                  $scope.libro = response.data.libro;
                  $scope.resultados = response.data.resultados;
                  $scope.saldoDesde= response.data.saldoDesde;
                  $scope.saldoInicial = saldoInicial($scope.libro.saldoApertura,$scope.resultados[0],$scope.saldoDesde);
                  $scope.totalI= suma( $scope.saldoInicial,$scope.totalIngreso.totalIngresos);
                  $scope.saldoA= resta( $scope.totalI,$scope.totalEgreso.totalEgresos);
        //          $scope.cuentasEfectivo = response.data.cuentasEfectivo;
        //          $scope.hechosLibroCaja = response.data.hechosLibroCaja;
                  
                  
        //          var fechaApertura = new Date($scope.libroEstado.libro.fechaApertura.toString());

       //           if(fechaApertura.getMonth()=== $scope.libroEstado.mes.getMonth()){
         //             var saldoApertura =0.00;
         //             for(var i=0;i<$scope.cuentasEfectivo.length;i++)
         //                 saldoApertura += $scope.cuentasEfectivo[i].saldoApertura;                      
                      
         //           $scope.libroEstado.saldoMesAnterior=saldoApertura.toFixed(2);
         //           $scope.libroEstado.enunciado = "SALDO DE APERTURA INICIAL";
         //         }
         //         else{
         //           $scope.libroEstado.enunciado = "SALDO DE MES ANTERIOR";
         //         }
         //         console.log(response.data);             
             }
               
            }
        
        },function(data){
            console.info(data);
        });
     
        
    }; 
      
      function saldoInicial (a,re,sd){
          var saldoInicial,mesAnterior, saldoDesde =0.00;
        
           if(re.saldo && re.mes!==$scope.consulta.desde.getMonth()) {mesAnterior = re.saldo;}
           else { mesAnterior=a;}
           
           if(sd.debe) {saldoDesde=sd.saldo;}
           
           saldoInicial = mesAnterior + saldoDesde;
           saldoInicial = saldoInicial.toFixed(2);
           return saldoInicial;
      };
      
      function suma (a,b){
          var result=0.00;
                               
           result = parseFloat(a) + parseFloat(b); 
           result = result.toFixed(2);
           return result;
      };
       function resta (a,b){
          var result=0.00;
                               
           result = parseFloat(a) - parseFloat(b); 
           result = result.toFixed(2);
           return result;
      };
    
  $scope.cambiarMes=function (mesSel)  {
      
      var hechos=[]; var debeM=0.00 , haberM=0.00 , debeMA=0.00 , haberMA=0.00, saldoMes=0.00 ,saldoMesA=0.00; 
      hechos=$scope.hechosLibroCaja;
      
      var mes= parseInt(mesSel), mesA=mes-1; 
      
      for(var i=0;i<hechos.length;i++){
          
         
           if(hechos[i].fechaMes === mes){                           
                debeM+= hechos[i].importeD ; 
                haberM+= hechos[i].importeH ; 
            }
            else if(hechos[i].fechaMes === mesA){
                debeMA+= hechos[i].importeD ; 
                haberMA+= hechos[i].importeH ; 
            }                                                                                                                                                             
      }
      saldoMes= debeM - haberM;
      saldoMesA = debeMA - haberMA;
      
      $scope.libroEstado.debe= debeM.toFixed(2);
      $scope.libroEstado.haber= haberM.toFixed(2);
      $scope.libroEstado.saldoMesAnterior= saldoMesA.toFixed(2);
      $scope.libroEstado.saldoMes=saldoMes.toFixed(2);
      
       if(mesSel<9){
           $scope.tablaLibroCaja.filter().fecha="/0"+ (parseFloat($scope.libroEstado.mesSel)+ 1) +"/";
           $scope.libroEstado.mesSel=mesSel;
           $scope.libroEstado.mes.setMonth(mesSel);
       }
       else{
           $scope.tablaLibroCaja.filter().fecha="/"+ (parseFloat($scope.libroEstado.mesSel)+ 1) +"/";
           $scope.libroEstado.mesSel=mesSel;
           $scope.libroEstado.mes.setMonth(mesSel);
       }
       
         var fechaApertura = new Date($scope.libroEstado.libro.fechaApertura.toString());

                  if(fechaApertura.getMonth()=== $scope.libroEstado.mes.getMonth()){
                      var saldoApertura =0.00;
                      for(var i=0;i<$scope.cuentasEfectivo.length;i++)
                          saldoApertura += $scope.cuentasEfectivo[i].saldoApertura;                      
                      
                    $scope.libroEstado.saldoMesAnterior=saldoApertura.toFixed(2);
                    $scope.libroEstado.enunciado = "SALDO DE APERTURA INICIAL";
                  }
                  else{
                    $scope.libroEstado.enunciado = "SALDO DE MES ANTERIOR";
                  }
       
      


    };


        
        //Visualizar de archivos en formato PDF
    $scope.reporte = function () {        
        var request = crud.crearRequest('libroCaja',1,'reporteLibroCaja');       
     
        request.setData({tablaLibroCaja:$scope.tablaLibroCaja.settings().dataset,datos:$scope.datosOrganizacion});        
        crud.insertar("/sistemaContable",request,function(data){            
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        },function(data){
            console.info(data);
        });            
    };
    
      $scope.reporteIngresos = function () {        
        var request = crud.crearRequest('libroCaja',1,'reporteIngresos');       
        var cabeceraReporte={saldoInicial:$scope.saldoInicial,totalIngreso:$scope.totalIngreso.totalIngresos,totalI:$scope.totalI,totalEgreso:$scope.totalEgreso.totalEgresos,saldoA:$scope.saldoA};
        request.setData({cabecera:cabeceraReporte,tablaIngresos:$scope.tablaCuentasIngreso.settings().dataset,totales:$scope.totalIngreso,datos:$scope.datosOrganizacion});        
        crud.insertar("/sistemaContable",request,function(data){            
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        },function(data){
            console.info(data);
        });            
    };
    
     $scope.reporteEgresos = function () {        
        var request = crud.crearRequest('libroCaja',1,'reporteEgresos');       
        request.setData({tablaEgresos:$scope.tablaCuentasEgreso.settings().dataset,totales:$scope.totalEgreso,datos:$scope.datosOrganizacion});        
        crud.insertar("/sistemaContable",request,function(data){            
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        },function(data){
            console.info(data);
        });            
    };
    
     $scope.reporteBalanceGeneral = function () {        
       var request = crud.crearRequest('libroCaja',1,'reporteBalanceGeneral');       
        var cabeceraReporte={saldoInicial:$scope.saldoInicial,totalIngreso:$scope.totalIngreso.totalIngresos,totalI:$scope.totalI,totalEgreso:$scope.totalEgreso.totalEgresos,saldoA:$scope.saldoA};
        request.setData({cabecera:cabeceraReporte,tablaIngresos:$scope.tablaCuentasIngreso.settings().dataset,totalesIngreso:$scope.totalIngreso,tablaEgresos:$scope.tablaCuentasEgreso.settings().dataset,totalesEgreso:$scope.totalEgreso,datos:$scope.datosOrganizacion});        
        crud.insertar("/sistemaContable",request,function(data){            
            $scope.dataBase64 = data.data[0].datareporte;
            window.open($scope.dataBase64);
        },function(data){
            console.info(data);
        });     
    };
        
   
       
    
 $scope.seleccionar = function (v) {        
        if(v ==="1"){
           
           $scope.seleccionar.flagReporteL=true;         
           $scope.seleccionar.flagReporteI = $scope.seleccionar.flagReporteE= $scope.seleccionar.flagReporteB =false;
        }
        else if(v==="2"){
            
           $scope.seleccionar.flagReporteI=true;           
           $scope.seleccionar.flagReporteL = $scope.seleccionar.flagReporteE= $scope.seleccionar.flagReporteB =false;
        }
         else if(v==="3"){
            $scope.seleccionar.flagReporteE=true;           
           $scope.seleccionar.flagReporteL = $scope.seleccionar.flagReporteI= $scope.seleccionar.flagReporteB =false;
        }
         else if(v==="4"){
            $scope.seleccionar.flagReporteB=true;           
           $scope.seleccionar.flagReporteL = $scope.seleccionar.flagReporteI= $scope.seleccionar.flagReporteE =false;
        }
        else{                    
          $scope.seleccionar.flagReporteL= $scope.seleccionar.flagReporteI = $scope.seleccionar.flagReporteE= $scope.seleccionar.flagReporteB =false;
        }
        
    };

}]);


