app.controller("consulta_inventarioCtrl",["$scope","NgTableParams","$location","crud","modal", function ($scope,NgTableParams,$location,crud,modal){


  /*Tabla de Bienes Inmuebles*/
    
    var params = {count: 20};
    var setting = { counts: []};
    $scope.tabla_inventarios = new NgTableParams(params, setting); 


    $scope.listar_inventarios = function(orgId){
        var request = crud.crearRequest('ingresos',1,'listar_inventarios');
         request.setData({org_id:orgId});
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_inventarios.settings(setting);
                $scope.tabla_inventarios.reload();

            }
            },function(data){
                
            console.info(data);
        });  
    }
    
    $scope.verificar_inventario = function(inventario,orgId){
        
         $scope.verificar_inventario = {
            id_inv:0,
            org_id:0,
            tipo_inv:"",
            interfaz:""
          };
          $scope.verificar_inventario.id_inv = inventario.inv_ini_id;
          $scope.verificar_inventario.org_id = orgId;
          $scope.verificar_inventario.tipo_inv = inventario.tipo_inv;
          $scope.verificar_inventario.interfaz = "consulta_inventario";

          localStorage.setItem('update_inventario', window.btoa(JSON.stringify($scope.verificar_inventario)) );
          $location.path('inventario_inicial');
    
        
    }
    
    $scope.EliminarInventario = function(Inv){
        
        if(Inv.tipo_inv=="Inventario Inicial"){
            modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este Inventario Inicial ?",function(){
            var request = crud.crearRequest('ingresos',1,'eliminar_inventario_inicial');
            request.setData({inv_ini_id:Inv.inv_ini_id});
            crud.eliminar("/controlPatrimonial",request,function(response){  
            modal.mensaje("CONFIRMACION",response.responseMsg);        
            if(response.responseSta){
                eliminarElemento(setting.dataset,Inv.i);
                iniciarPosiciones(setting.dataset);  
                $scope.tabla_inventarios.settings(setting);
                $scope.tabla_inventarios.reload();
            }
            },function(data){
                console.info(data);
            });
            });
        }
        if(Inv.tipo_inv=="Inventario Fisico"){
            modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este Inventario Fisico  ?",function(){
            var request = crud.crearRequest('ingresos',1,'eliminar_inventario_fisico');
            request.setData({inv_ini_id:Inv.inv_ini_id});
            crud.eliminar("/controlPatrimonial",request,function(response){   
            modal.mensaje("CONFIRMACION",response.responseMsg);     
                if(response.responseSta){
                    eliminarElemento(setting.dataset,Inv.i);
                    iniciarPosiciones(setting.dataset);  
                    $scope.tabla_inventarios.settings(setting);
                    $scope.tabla_inventarios.reload();
                }
            },function(data){
                console.info(data);
            });
      });
            
            
            
            
        }
    }

}]);