app.controller("reporte_bienesCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){

    $scope.reporte ={
        org_id:"",
        fec_ini:new Date(),
        fec_fin:new Date(),
        tip_ane:0
    };
    
    $scope.anexos={};/*Lista de Anexos*/ 

    $scope.generar_reporte = function(orgId){
        
    
        $scope.reporte.fec_ini = convertirFecha( $scope.reporte.fec_ini);   
        $scope.reporte.fec_fin = convertirFecha( $scope.reporte.fec_fin); 
        $scope.reporte.org_id  = orgId;
        
        /* Verificamos que todos los campos esten llenados */
        var accept = true;
        for(var atrib in $scope.reporte){
            if($scope.reporte[atrib]==""){
                accept = false;
                break;
            }
        }
        if(accept==true){ /*Todos los campos estan llenados correctamente*/
            
            var request = crud.crearRequest('ingresos',1,'reporte_bienes');
            request.setData($scope.reporte);        
            crud.listar("/controlPatrimonial",request,function(data){            
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                    },function(data){
                    console.info(data);
            });  
        }
        else{
               modal.mensaje("ERROR","Verifique que todos los campos esten llenados");
        }
         
     }
     
     $scope.listar_anexos = function(){
        var request = crud.crearRequest('configuracion_patrimonial',1,'listar_anexos');
            crud.listar("/controlPatrimonial",request,function(data){
            $scope.anexos = data.data;
            },function(data){
            console.info(data);
            });
     }

     
}]);

