app.controller("catalogo_sbnCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){

    /*Datos Catalogo*/
    $scope.item_catalogo= {
        gru_gen_id:0,
        cla_gen_id:0,
        fam_gen_id:0,
        uni_med_id:0,
        cod:"",
        den_bie:""
    };
    /*Lista Grupos GenericmostrarCatalogoos*/
    $scope.grupos_genericos={};
    
    /*Lista Clases Genericas*/
    $scope.clases_genericas={};
    
    /*Lista Familia Genericas*/
    $scope.familias_genericas={};
    
    /*Lista de Unidades de Medida*/
    $scope.unidades_medida={};
    
    /*Tabla del Catalogo de Bienes*/
    var params = {count: 20};
    var setting = { counts: []};
    $scope.tabla_catalogo = new NgTableParams(params, setting);
    
    $scope.tabla_catalogo_reporte; 
    
    /*Catalogo de Bienes Importados*/
    $scope.catalogo_bienes = { nombre:"",archivo:""};
    

   $scope.listarGruposGenericos = function(){
       
       var request = crud.crearRequest('catalogo_bienes',1,'listar_grupos');
       crud.listar("/controlPatrimonial",request,function(data){
       $scope.grupos_genericos = data.data;
       },function(data){
           console.info(data);
       });
   }
   
   $scope.listarClasesGenericas = function(GrupoId){
       
       var request = crud.crearRequest('catalogo_bienes',1,'listar_clases');
       request.setData({gru_gen_id:GrupoId});
       crud.listar("/controlPatrimonial",request,function(data){
           $scope.clases_genericas = data.data;
       },function(data){
           console.info(data);
       });
       
   }
   
   $scope.listarFamiliasGenericas = function(ClaseId){
       
       var request = crud.crearRequest('catalogo_bienes',1,'listar_familias');
       request.setData({fam_gen_id:ClaseId});
       crud.listar("/controlPatrimonial",request,function(data){
           $scope.familias_genericas = data.data;
       },function(data){
           console.info(data);
       });
       
   }
   
   
   $scope.listarUnidadesMedida = function(){
       var request = crud.crearRequest('catalogo_bienes',1,'listar_unidades_medida');
       crud.listar("/controlPatrimonial",request,function(data){
       $scope.unidades_medida = data.data;
       },function(data){
           console.info(data);
       });
       
   }
   
   
   
   
   $scope.agregarItemCatalogo = function(ItemCatalogo,usu_mod){
       
        $scope.nuevo_item = ItemCatalogo;
        //VERIFICAMOS SI TODOS LOS CAMPOS SE LLENARON CORRECTAMENTE
        var accept = true;
        for(var atrib in ItemCatalogo){
            if(ItemCatalogo[atrib]==""){
                accept = false;
            }
        }
        var codigo_sbn = $scope.nuevo_item.cod;
        if(!(codigo_sbn.length==6)){
            aceept=false;
        }
        
        if(accept==true){ /*Todos los campos estan llenados correctamente*/
            
            //BUSCAR UNIDAD DE MEDIDA
            for(var i = 0;i<$scope.unidades_medida.length;i++){
                if($scope.unidades_medida[i].uni_med_id == $scope.nuevo_item.uni_med_id){
                   $scope.nuevo_item.uni_med_nom = $scope.unidades_medida[i].nom;
                   break;
                }
            }   
                $scope.nuevo_item.num_res = "";
                $scope.nuevo_item.usu_mod = usu_mod.ID;
                $scope.registrarItemsCatalogo($scope.nuevo_item);
              
               
               insertarElemento(setting.dataset,$scope.nuevo_item);
               $scope.tabla_catalogo.reload(); 
               /*Finalmente cerramos la ventana modal*/
                $('#modalnuevoitem').modal('hide');
        }else{
                
            modal.mensaje("ERROR AL REGISTRAR","Verifique que todos los campos esten llenados");
        }
        
        
   }
   
 
   $scope.registrarItemsCatalogo = function(Item){
        $scope.bienes = Item;
        modal.mensajeConfirmacion($scope,"Seguro que desea registrar Items al Catalogo de Bienes ?",function(){
               
               var request = crud.crearRequest('catalogo_bienes',1,'registrar_catalogo_bienes');
               request.setData($scope.bienes);        
               crud.insertar("/controlPatrimonial",request,function(response){
                   modal.mensaje("CONFIRMACION",response.responseMsg);
                   if(response.responseSta){
                       $scope.area = response.data;
                   }
               },function(data){
                   console.info(data);
               });
        });
   }
   
   $scope.mostrarCatalogo = function(){
        var request = crud.crearRequest('catalogo_bienes',1,'listar_catalogo_bienes');
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                $scope.tabla_catalogo_reporte = data.data;
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_catalogo.settings(setting);
                $scope.tabla_catalogo.reload();
            }
        },function(data){
            console.info(data);
        });
        
   }
   
   $scope.importar_catalogo = function(){
       
   }
   
   $scope.EditarItemCatalogo = function(Item){
       
       /*Actualizamos el Item Actual*/
       $scope.item_catalogo = Item;
       
       
        var request = crud.crearRequest('catalogo_bienes',1,'editar_item_catalogo');
            request.setData($scope.item_catalogo);
            crud.actualizar("/controlPatrimonial",request,function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                  if(response.responseSta){  
                     
                  }
            });

   }
   
   $scope.EliminarItemCatalogo = function(Item){
       
       modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este Catalogo  ?",function(){
           var request = crud.crearRequest('inventario_transferencia',1,'eliminar_inventario_transferencia');
           request.setData(Item);
           
           crud.eliminar("/controlPatrimonial",request,
            function(response){
                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                   // eliminarElemento(setting.dataset,i);
                   // $scope.tabla_series.reload();
                }

            },function(data){
            });
       
           });      
   }
   
   $scope.reporte_catalogo = function(){

       var request = crud.crearRequest('catalogo_bienes',1,'reporte_catalogo');
            request.setData($scope.tabla_catalogo_reporte);        
            crud.listar("/controlPatrimonial",request,function(data){            
                    $scope.dataBase64 = data.data[0].datareporte;
                    window.open($scope.dataBase64);
                    },function(data){
                    console.info(data);
            });
       
       
       
   }

     $scope.cargar_codigo = function(){
        
       var grupo;var clase; var familia;  
         
       var size_gr = $scope.grupos_genericos.length;
       for(var i=0;i<size_gr;i++){
           if($scope.grupos_genericos[i].gru_gen_id==$scope.item_catalogo.gru_gen_id){
               grupo = $scope.grupos_genericos[i].cod_gru;
               break;
           }
       }
       var size_cl = $scope.clases_genericas.length;
       for(var j=0;j<size_cl;j++){
           if($scope.clases_genericas[j].cla_gen_id==$scope.item_catalogo.cla_gen_id){
               clase = $scope.clases_genericas[j].cod_cla;
               break;
           }
       }
       var size_fam = $scope.familias_genericas.length;
       for(var k=0;k<size_fam;k++){
           if($scope.familias_genericas[k].fam_gen_id==$scope.item_catalogo.fam_gen_id){
               familia = $scope.familias_genericas[k].cod_fa;
               break;
           }
       }
       
   
       var codigo_sbn = (grupo.concat(clase)).concat(familia);
       if(codigo_sbn.length==8){
           $scope.item_catalogo.cod = codigo_sbn;
       }
       else{
           $scope.item_catalogo.cod = "";
       }
       
        
   }
        
   
        
}]);


