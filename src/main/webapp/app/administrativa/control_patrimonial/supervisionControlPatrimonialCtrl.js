
app.requires.push('angularModalService');
app.requires.push('ngAnimate');
app.requires.push('ui.bootstrap');
      

app.controller("supervisionControlPatrimonialCtrl",["$scope","$filter","$rootScope","NgTableParams","crud","modal","ModalService", function ($scope,$filter,$rootScope,NgTableParams,crud,modal,ModalService){
 
 
  //variable para agregar un nuevo control
  $scope.control= {fecha:"",controlID:0,descripcion:"",fechaPreCierre:'',fechaCierre:'',organizacionID:""};
  
  //variable para manejo de la fecha del control
  $scope.control.fecha = new Date();   
 
  //Variable almacena organizacion y fecha de control para consulta de sus libros a sus cargo
  $scope.organizacionControl={organizacionID:$rootScope.usuMaster.organizacion.organizacionID,fechaControl:""};
  
  //Variables para manejo de la tabla del los Libros Caja
  var paramsLibrosCaja= {count: 10};
  var settingLibrosCaja = { counts: [],filterOptions: { filterComparator:_.startsWith }};
  $scope.tablaLibrosCaja = new NgTableParams(paramsLibrosCaja, settingLibrosCaja);
       
  
  //Variable para listar registros de controles del libro
  $scope.controles=[];
  
  listarControl();
  
  $scope.listarLibros = function ()   {
     
       $scope.organizacionControl.fechaControl=  $scope.control.fecha.getFullYear();
       //preparamos un objeto request
        var request = crud.crearRequest('libroCaja',1,'listarLibrosCajaPorUGEL');        
        request.setData( $scope.organizacionControl);
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request             
        crud.listar("/sistemaContable",request,function(response){
           
           if(response.data===null){ $scope.tablaLibrosCaja=[];}
           else{
            settingLibrosCaja.dataset =response.data;
        //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingLibrosCaja.dataset);
            $scope.tablaLibrosCaja.settings(settingLibrosCaja);
            
            console.log($scope.tablaLibrosCaja);}
         
        },function(data){
            console.info(data);
        });
       
  };
  
  $scope.agregarControlLibro = function(){
     
       var request = crud.crearRequest('controlLibro',1,'insertarControl');                            
       

        if($scope.control.descripcion==="" ){
            modal.mensaje("CONFIRMACION","Ingrese la descripción del control");
            return;
        }
        if( ($scope.control.fechaPreCierre==="" )){
            modal.mensaje("CONFIRMACION","La fecha Pre-cierre no es correcta");
            return;
        }
        if(($scope.control.fechaCierre==="" )){
            modal.mensaje("CONFIRMACION","La fecha Cierre no es correcta");
            return;
        }
        
       $scope.control.fechaPreCierre=$scope.control.fechaPreCierre.toString();
       $scope.control.fechaCierre=$scope.control.fechaCierre.toString();
       $scope.control.organizacionID=$rootScope.usuMaster.organizacion.organizacionID;
       request.setData($scope.control);
       
       crud.insertar("/sistemaContable",request,function(response){
           
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
               $scope.controles.push(response.data);
               $scope.control={controlID:0,descripcion:"",fechaPreCierre:'',fechaCierre:''};                                                        
            }  
            
        },function(data){
            console.info(data);
        });               
             
    }; 
    
    function listarControl(){
      var request= crud.crearRequest('controlLibro',1,'listarControl') ;
      request.setData({year:$scope.control.fecha.getFullYear(),organizacionID:$rootScope.usuMaster.organizacion.organizacionID});
      
      crud.listar("/sistemaContable",request,function (response){
          modal.mensaje("CONFIRMACION",response.responseMsg);
          if(response.responseSta){
              $scope.controles = response.data;
          }
      
          },function(data){
            console.info(data);
        });  
      
      
    };
    $scope.editarControl = function(i,r,modo){
        //si estamso editando
        if(r.edi){
            if(modo)
                $scope.tipoTramiteSel.requisitos[i] = r.copia;                
            else
                $scope.requisitos[i] = r.copia;
        }
        //si queremos editar
        else{
            
            var dd =  r.fechaPreCierre.toString().substr(0,2);
            var mm = r.fechaPreCierre.toString().substr(3,2);
            var yy =  r.fechaPreCierre.toString().substr(6,4);
            
            var d =  r.fechaCierre.toString().substr(0,2);
            var m = r.fechaCierre.toString().substr(3,2);
            var y =  r.fechaCierre.toString().substr(6,4);
            r.copia = JSON.parse(JSON.stringify(r));
            r.copia.fechaPreCierre= new Date(yy,mm-1,dd);
            r.copia.fechaCierre= new Date(y,m-1,d);

            r.edi =true;            
        }
    };
  
  $scope.dateOptions = {      
    datepickerMode: "year",
        formatYear: "yyyy" ,
        minMode: "year",
        minDate:new Date("2010,1,1"),
        maxDate:new Date("2100/10/10")
      
  };

     
  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };
  $scope.popup1 = {
    opened: false
  };

var f=new Date();
$scope.dateOptions1 = {  
    
    maxDate: new Date(f.getFullYear(),11,31),
    minDate: new Date(f.getFullYear(),0,1),
    startingDay: 1
        
  };

     
  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };
  $scope.popup2 = {
    opened: false
  };
  $scope.open3 = function() {
    $scope.popup3.opened = true;
  };
  $scope.popup3 = {
    opened: false
  };
    $scope.open4 = function() {
    $scope.popup4.opened = true;
  };
  $scope.popup4 = {
    opened: false
  };$scope.open5 = function() {
    $scope.popup5.opened = true;
  };
  $scope.popup5 = {
    opened: false
  };    
        
   $scope.applyGlobalSearch = function(term) {
    $scope.tablaLibrosCaja.filter({ $: term });
};
    


 $scope.showNuevoInventario = function() {
   
   
    ModalService.showModal({
      templateUrl: "administrativa/control_patrimonial/importarInventario.html",
      controller: "importarInventarioCtrl",
      inputs: {
        title: "Inventario Inicial"
        
      }
    }).then(function(modal) {
      modal.element.modal();
      modal.close.then(function(result) {
          if(result.flag){
                        
             
            }  
          
                              
      });
    });
    
    
   
  };       
  }]);

  app.controller('importarInventarioCtrl', [
  '$scope', '$element', 'title','NgTableParams', 'close','crud','modal',
  function($scope, $element, title,NgTableParams, close,crud,modal) {
  
  $scope.title=title;
  $scope.doc= {archivo:[],edi:false};
  $scope.inventario=["CODIGO","TABLA","SECUENCIA"];
  $scope.verificar={COD:"numero",SECUENCIA:"texto",TABLA:"fecha"}
  
  //Variables para manejo de la tabla del los Libros Caja
  var paramsArchivoJSON= {count: 10};
  var settingArchivoJSON = { counts: []};
  $scope.tablaArchivoJSON = new NgTableParams(paramsArchivoJSON, settingArchivoJSON);
  //Variable Header del tablaJSON
  $scope.tablaHeaderArchivoJSON={};
 
    
  $scope.verRegistros = function (file){                  
 
        console.log(file);
         $scope.tablaHeaderArchivoJSON= file.header;
         settingArchivoJSON.dataset = file.result;                         
         $scope.tablaArchivoJSON.settings(settingArchivoJSON);                                         
    
};
 $scope.upload = function (file){                  
     if(file.success){
         
         
         
     }
     else{              
        modal.mensaje("CONFIRMACION", "ERROR! VERIFICAR DATOS");
        $scope.observacion=(file.errorHeader == null || file.errorHeader == 0)? file.errorResult : file.errorHeader;
     }
    
    
};
 

  
  //  This close function doesn't need to use jQuery or bootstrap, because
  //  the button has the 'data-dismiss' attribute.
  $scope.close = function() {
         $element.modal('hide');
 	  close({
    
      flag:false
    }, 500); // close, but give 500ms for bootstrap to animate
  };

  //  This cancel function must use the bootstrap, 'modal' function because
  //  the doesn't have the 'data-dismiss' attribute.
  $scope.cancel = function() {      

    //  Manually hide the modal.
    $element.modal('hide');
    
    //  Now call close, returning control to the caller.
    close({
    
      flag:false
    }, 500); // close, but give 500ms for bootstrap to animate
  };
  
 
 

}]);

