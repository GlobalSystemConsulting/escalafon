app.controller("verificacion_registrosCtrl",["$scope","NgTableParams","$location","crud","modal", function ($scope,NgTableParams,$location,crud,modal){


$scope.cant_bienes ={
    bienes_alta:0,
    bienes_baja:5,
    bienes_inventario:0
};


/*Control Patrimonial Actual*/
$scope.cp_actual;

/*TABLA BIENES (GENERALES,ALTA,BAJA)*/
var params = {count: 20};
var setting = { counts: []};
$scope.tabla_bienes = new NgTableParams(params, setting); 


/*TABLA DE BIENES DE ALTA*/
var params_a = {count:10};
var setting_a = {count : []};
$scope.tabla_altas = new NgTableParams(params_a,setting_a);


/*TABLA DE BIENES DE BAJA*/
var params_b = {count:10};
var setting_b = {count : []};
$scope.tabla_bajas = new NgTableParams(params_b,setting_b);


/*Mostrar todos los bienes*/
$scope.mostrar_bienes_generales = function(orgId){
      var request = crud.crearRequest('ingresos',1,'listar_bienes_muebles');
      request.setData({org_id:orgId});
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_bienes.settings(setting);
                $scope.tabla_bienes.reload();
            }
        },function(data){
            console.info(data);
        });
};

$scope.mostrar_bienes_alta = function(orgId){
    var request = crud.crearRequest('ingresos',1,'listar_altas');
    request.setData({org_id:orgId});
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                $scope.cant_bienes.bienes_alta = data.data.length;
                setting_a.dataset = data.data;
                iniciarPosiciones(setting_a.dataset);
                $scope.tabla_altas.settings(setting_a);
                $scope.tabla_altas.reload();
            }
        },function(data){
            console.info(data);
        });

};

$scope.mostrar_bienes_baja = function(orgId){
    var request = crud.crearRequest('salidas',1,'listar_bajas');
    request.setData({org_id:orgId});
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                $scope.cant_bienes.bienes_baja = data.data.length;
                setting_b.dataset = data.data;
                iniciarPosiciones(setting_b.dataset);
                $scope.tabla_bajas.settings(setting_b);
                $scope.tabla_bajas.reload();
            }
        },function(data){
            console.info(data);
        });
        
};
/*Edicion y Verificacion de Bienes*/
$scope.verificar_registro_bien = function(bien,user){
    
    
    $scope.verificar_bien = {
      id_bien:0,
      org_id:0,
      interfaz:""
    };
    $scope.verificar_bien.id_bien = bien.cod_bie;
    $scope.verificar_bien.org_id = bien.org_id;
    $scope.verificar_bien.interfaz = "verificacion_registros";
    
    localStorage.setItem('update_bien_mueble', window.btoa(JSON.stringify($scope.verificar_bien)) );
    $location.path('registro_bienes');
    
    
};

$scope.obtener_cp_actual = function(orgId){
    
};

$scope.estado_bien = function(){
    
}

$scope.ElimimarBienMueble = function(Bien){
    
      modal.mensajeConfirmacion($scope,"Seguro que desea eliminar este Bien Mueble  ?",function(){
      var request = crud.crearRequest('ingresos',1,'eliminar_bien_mueble');
      request.setData({cod_bie:Bien.cod_bie});
      crud.eliminar("/controlPatrimonial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                eliminarElemento(setting.dataset,Bien.i);
                iniciarPosiciones(setting.dataset);  
                $scope.tabla_bienes.settings(setting);
                $scope.tabla_bienes.reload();
            }
            },function(data){
                console.info(data);
            });
      });
}

}]);
