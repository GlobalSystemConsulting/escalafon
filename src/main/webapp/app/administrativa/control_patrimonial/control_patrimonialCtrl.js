app.controller("control_patrimonialCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){



     /*Tabla Control Patrimonial para una IE*/
    var params = {count: 20};
    var setting = { counts: []};
    $scope.tabla_control_patrimonial = new NgTableParams(params, setting);


    $scope.nueva_configuracion = {
        id_cp:0,
        org_id:0,
        per_res:"",
        fec_ini:"",
        fec_cie:"",
        obs:"",
        usu_mod:0
    };
    $scope.mostrar_control_patrimonial = function(orgId){ 
        var request = crud.crearRequest('configuracion_patrimonial',1,'listar_control_patrimonial');
        request.setData({org_id:orgId}); 
        crud.listar("/controlPatrimonial",request,function(data){
            if(data.data){
                setting.dataset = data.data;
                iniciarPosiciones(setting.dataset);
                $scope.tabla_control_patrimonial.settings(setting);
                $scope.tabla_control_patrimonial.reload();
            }
        },function(data){
            console.info(data);
        });
    }
      
    
    $scope.nuevo_control_patrimonial = function(orgId,user){
        $scope.nueva_configuracion.org_id = orgId;
        $scope.nueva_configuracion.usu_mod = user.ID;
        $scope.nueva_configuracion.fec_ini = convertirFecha($scope.nueva_configuracion.fec_ini);
        $scope.nueva_configuracion.fec_cie = convertirFecha($scope.nueva_configuracion.fec_cie);

        /*Verificamos los campos llenados correctamente*/
        var accept = true;
        for(var atrib in $scope.nueva_configuracion){
            if($scope.nueva_configuracion[atrib]==""){
                if(atrib=="cp_id"){continue;}
                accept = false;
                break;
            }
        }
        if(accept==true){ /*Todos los campos estan llenados correctamente*/
            
            if($scope.nueva_configuracion.cp_id!=0){
                
                modal.mensajeConfirmacion($scope,"Seguro que desea Actualizar el Control Patrimonial  ?",function(){
                    
                    var request = crud.crearRequest('configuracion_patrimonial',1,'editar_control_patrimonial');
                    request.setData($scope.nueva_configuracion);
                    crud.actualizar("/controlPatrimonial",request,function(response){
                    modal.mensaje("CONFIRMACION",response.responseMsg);
                    if(response.responseSta){
                        $scope.limpiar_campos_control();
                        $('#modalnuevocontrol').modal('hide');
                    }
                    }

                    );
                });   
            }
            else{
                modal.mensajeConfirmacion($scope,"Seguro que desea Registrar el Control Patrimonial  ?",function(){

                    var request = crud.crearRequest('configuracion_patrimonial',1,'registrar_control_patrimonial');
                    request.setData($scope.nueva_configuracion);        
                    crud.insertar("/controlPatrimonial",request,function(response){
                        modal.mensaje("CONFIRMACION",response.responseMsg);
                        if(response.responseSta){
                           var nuevo_control = $scope.nueva_configuracion ;
                           insertarElemento(setting.dataset,nuevo_control);
                           $scope.tabla_control_patrimonial.reload(); 
                           $scope.limpiar_campos_control();
                           $('#modalnuevocontrol').modal('hide');
                        }
                    },function(data){
                       console.info(data);
                    });
                });
            
            } 
        }
        else{
             modal.mensaje("ERROR AL REGISTRAR","Verifique que todos los campos esten llenados");
        }
             
    }
    
    $scope.editar_control_patrimonial = function(control){
        $scope.nueva_configuracion.per_res = control.per_res;
        fec_ini = new Date(control.fec_ini);
        fec_cie = new Date(control.fec_cie);
        $scope.nueva_configuracion.fec_ini = fec_ini;
        $scope.nueva_configuracion.fec_cie = fec_cie;
        $scope.nueva_configuracion.obs = control.obs;
        $scope.nueva_configuracion.id_cp = control.cp_id;
        
        $('#modalnuevocontrol').modal('show');
     
    };
    
    $scope.limpiar_campos_control = function(){
        $scope.nueva_configuracion = {
            cp_id:0,
            org_id:0,
            per_res:"",
            fec_ini:"",
            fec_cie:"",
            obs:"",
            usu_mod:0
        };   
    }
    
        
    

}]);
