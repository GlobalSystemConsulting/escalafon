app.controller("gestorConsultasCtrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {
        $rootScope.paramsConsultas = {count: 10};
        $rootScope.settingConsultas = {counts: []};
        $rootScope.tablaGestorConsultas = new NgTableParams($rootScope.paramsConsultas, $rootScope.settingConsultas);
        var JSONcon_cer_id;
        $scope.pass="";
        //////////////////////////LISTAR CONSULTAS VERIFICADAS Y NO VERIFICADAS//////////////////////
        $scope.listarConsultasPendientesCertificacion = function () {
            var request = crud.crearRequest('reportes', 1, 'listarSolicitudesCertificacion');
            request.setData();    
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingConsultas.dataset = data.data;
                iniciarPosiciones($rootScope.settingConsultas.dataset);
                $rootScope.tablaGestorConsultas.settings($rootScope.settingConsultas);
            }, function (data) {
                console.info(data);
            });
        };
        //////////////////////////LISTAR CONSULTAS VERIFICADAS Y NO VERIFICADAS//////////////////////
        var procesarConsulta = function(d,passwordCert){
            //var trabajadorSel = JSON.parse(JSON.stringify(t));
            var cab = JSON.parse(d.atr_usa);
            var request = crud.crearRequest('reportes',1,'reporteConsultaGeneral');
            ///////////////CARGAMOS LAS CABECERAS DEL REPORTE///////////////////
            var cabeceras = [];
            cabeceras.push({item_cabecera:"N°"});///AGREGAMOS LA NUMERACION A LAS FILAS DEL REPORTE
            for(x in cab){
                    for(y in cab[x].atributos){
                        if(cab[x].atributos[y].show){//solo si esta marcado para mostrar con el switch lo coloca en cabeceras sino no lo coloca en el query
                            cabeceras.push({item_cabecera:cab[x].atributos[y].alias_name});
                        }
                    }
                }
            ////////////////////////////////////////////////////////////////////
            request.setData({
                    id:d.con_cer_id,
                    titulo:d.tit,
                    observaciones_reporte:d.obs,
                    consulta:d.sql_que,
                    cabeceras:cabeceras,//envia las cabeceras generadas aqui en la variable cabeceras no usa la de d
                    idCertificacion:passwordCert
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    verDocumentoPestaña(response.data.file);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            });
        };
        //////////////////////////LISTAR CONSULTAS VERIFICADAS Y NO VERIFICADAS//////////////////////
        $scope.setConCerId = function(d){
            JSONcon_cer_id = d.con_cer_id;
        };
        $scope.certificarConsulta = function(){
            /////////////////////////ALMACENAR LA CONSULTA GENERAL EN UN JSON QUE SE GUARDARA EN LA BD////////////////
            console.info("111: ",$scope.usuMaster.usuario.usuarioID);
            //console.info("222: ",$scope.pass);
                //////certificamos y procesamos//////
                //JSONcon_cer_id = d.con_cer_id;
                var request = crud.crearRequest('reportes',1,'actualizarFechaCertificacionConsulta');
                request.setData({
                        con_cer_id:JSONcon_cer_id,
                        usu_id:$scope.usuMaster.usuario.usuarioID,
                        pas:$scope.pass
                });
                crud.actualizar('/sistema_escalafon',request,function(response){
                    if(response.responseSta){
                        //tiene k reposnder la fecha de actualizacion la transaccion yluego a apartir del dataset lo actualizamso manualemnte no es necesario recargar toda la pagina
                        $scope.listarConsultasPendientesCertificacion();
                        $rootScope.tablaGestorConsultas.reload();
                        if(response.data.isVerified){
                            modal.mensaje("CONFIRMACION", "Estado Actualizado");
                        }else{
                            modal.mensaje("ERROR", "Contraseña incorrecta");
                        }
                        
                        
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(error){
                    modal.mensaje('ERROR','El servidor no responde No se pudo almacenar la fecha de certififcacion de la consulta');
                });
           $('#certificadoDigital').modal('hide');
            
            $scope.pass="";
        };
        $scope.verConsulta = function(d){
            procesarConsulta(d,"");
        };
        
    }]);