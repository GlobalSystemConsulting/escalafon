var seApp = angular.module('app');
seApp.requires.push('angularModalService');
seApp.requires.push('ngAnimate');
seApp.requires.push('ngSanitize');


seApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/pasosConsultaDiversav2/:dataConsulta',{
        templateUrl:'administrativa/sistema_escalafon/pasosConsultaDiversav2.html',
        controller:'pasosConsultaDiversav2Ctrl',
        controllerAs:'pasosConsultaDiversav2Ctrl'
    });
}]);
//SATU
    
seApp.controller("consultaDiversav2Ctrl", ["$rootScope", "$scope","NgTableParams", "crud", "modal", "$location", '$uibModal', function ($rootScope, $scope, NgTableParams, crud, modal, $location, $uibModal) {
    
    $scope.idConsulta = "";
    $scope.gestionarConsultas = function() {
        $uibModal.open({
            animation: true,
            keyboard: true,
            backdrop: true,
            templateUrl: 'abrirConsulta.html',
            controller: 'abrirConsultaCtrl',
            controllerAs: 'abrirConsulta'
        });
    };
    
    $scope.gestionarPasos = function(){
        var dataConsulta = {
            idConsulta: $scope.idConsulta,
            opcion:1,
            listaConsultasFrecuentes: "",
            save:false
        };
        $location.url('/pasosConsultaDiversav2/'+btoa(JSON.stringify(dataConsulta)));
    };
}]);

seApp.controller('abrirConsultaCtrl',['$uibModalInstance', 'crud', 'modal', '$window','$rootScope', '$scope','$location', function($uibModalInstance, crud, modal, window, $rootScope, $scope, $location) {
    //////////////////////////////////////LISTAR CONSULTAS FRECUENTES///////////////////////////
    $scope.idConsulta = "";
    $scope.listaConsultasFrecuentes="";
    $scope.listarConsultasFrecuentes = function () {
        var request = crud.crearRequest('reportes', 1, 'listarConsultasFrecuentesxUsuario');
        request.setData({
            usuId:$rootScope.usuMaster.usuario.usuarioID
        });  
        crud.listar("/sistema_escalafon", request, function (data) {
            $scope.listaConsultasFrecuentes = data.data;
        }, function (data) {
            console.info(data);
        });
    };
    
    $scope.verDatosConsulta = function(idConsulta){
        var dataConsulta = {
            idConsulta: idConsulta,
            opcion:2,
            listaConsultasFrecuentes: $scope.listaConsultasFrecuentes,
            save:true
        };
        $location.url('/pasosConsultaDiversav2/'+btoa(JSON.stringify(dataConsulta)));
    };
}]);

seApp.controller('pasosConsultaDiversav2Ctrl', ['$routeParams', '$rootScope', '$scope','NgTableParams', 'crud','modal', 'ModalService', function ($routeParams, $rootScope, $scope,  NgTableParams, crud, modal, ModalService) {
    var datos = JSON.parse(atob($routeParams.dataConsulta));
    console.log(datos);
    var save=datos.save;
    $scope.idConsulta = datos.idConsulta;
    $scope.opcion = datos.opcion;
    $scope.listaConsultasFrecuentes = datos.listaConsultasFrecuentes;
    $scope.consulta_reporte = "";
    $scope.idCertificacion = ""; 
    $scope.datosReporte = {titulo_reporte:"",observaciones_reporte:""};
    
    //////////////////////////////////////LISTAR CATALOGO///////////////////////////
    $rootScope.settingTablas = {counts: []};
    $scope.listarTablas = function () {
        var request = crud.crearRequest('reportes', 1, 'listarCatalogoConsultaGeneral');
        var atrib_temp={};
        var table_temp=[];
        var Table_temp={};
        crud.listar("/sistema_escalafon", request, function (data) {
            $rootScope.settingTablas.dataset = data.data;
            $scope.listaAtributos = data.data;//LINEA LISTAR METADATA EN OPTGROUP
            
            ////LISTAR Y MOSTRAR EN OPTOGROUP
            $scope.listaTablas=[];
            for(var listaAtrib=0;listaAtrib<$scope.listaAtributos.length;listaAtrib++){
                var inserto=false;
                if($scope.listaTablas.length>0){//si no esta vacio
                    for (var listaTabl=0;listaTabl<$scope.listaTablas.length;listaTabl++) {
                        if($scope.listaTablas[listaTabl].entity_name===$scope.listaAtributos[listaAtrib].entity_name){//si encuentra un array de determinada tabla
                            atrib_temp={entity_name:$scope.listaAtributos[listaAtrib].entity_name,atrib_name:$scope.listaAtributos[listaAtrib].atrib_name,alias_name:$scope.listaAtributos[listaAtrib].alias_name,data_type:$scope.listaAtributos[listaAtrib].data_type,hide:false,indexTab:listaTabl,indexAtr:$scope.listaTablas[listaTabl].atributos.length,show:true,orden:""};
                            $scope.listaTablas[listaTabl].atributos.push(atrib_temp);
                            inserto=true;//si lo encontro lo inserta y no vuleve a crear nueva tabla
                            break;
                        }
                    }
                }
                if($scope.listaTablas.length==0||!inserto){//////esta vacio inserta primer elemento o si no inserto(la encontro)crear una nueva
                    atrib_temp={entity_name:$scope.listaAtributos[listaAtrib].entity_name,atrib_name:$scope.listaAtributos[listaAtrib].atrib_name,alias_name:$scope.listaAtributos[listaAtrib].alias_name,data_type:$scope.listaAtributos[listaAtrib].data_type,hide:false,indexTab:$scope.listaTablas.length,indexAtr:0,show:true,orden:""};
                    table_temp.push(atrib_temp);
                    Table_temp={entity_name:$scope.listaAtributos[listaAtrib].entity_name,atributos:table_temp,hide:false};
                    $scope.listaTablas.push(Table_temp);
                    table_temp=[];
                }    
            }
            ////LISTAR Y MOSTRAR EN OPTOGRUP
            
            if($scope.opcion===2){
                var index=0;
                for(var i=0;i<$scope.listaConsultasFrecuentes.length;i++){
                    if($scope.listaConsultasFrecuentes[i].con_cer_id===$scope.idConsulta){
                        index = i;
                        break;
                    }
                }
                //No se puede reemplazar variables de un solo ya que rompe el scope con la directiva de query builder
                //por ello se limpia la variable poniendo su length a cero
                ///***************cambiamos el length de las variables a 0 para poder cargarlas  despues

                $scope.listaTablasSel.length=0;
                $scope.listaTablas.length=0;
                $scope.filter.length=0;$scope.filter = [];
                $scope.listaFiltros.length=0;
                $scope.datosReporte.titulo_reporte.length=0;
                $scope.datosReporte.observaciones_reporte.length=0;
                //********************cargamos el resto de datos*******
                var listaTablasSelTemp=JSON.parse($scope.listaConsultasFrecuentes[i].atr_usa);//usamos una variable temporal que luego recorreremos para llevar $scope.listaTablasSel
                $scope.listaTablas=JSON.parse($scope.listaConsultasFrecuentes[i].cat);
                const dateFormat = /^\d{4}-\d{2}-\d{2}$/;
                function reviver(key, value) {

                    if (typeof value === "string" && dateFormat.test(value)) {
                        console.log(value);
                        const str = value.split('-');
                        console.log(str);
                        const year = Number(str[0]);
                        const month = Number(str[1]-1);
                        const date = Number(str[2]);

                        //return new Date(year, month, date);
                        var fecha = new Date(year, month, date);;
                        //return $filter('date')(fecha,'yyyy-MM-dd');
                        return fecha;
                    }
                    return value;
                }

                $scope.filter=JSON.parse($scope.listaConsultasFrecuentes[i].log,reviver);
                $scope.listaFiltros=JSON.parse($scope.listaConsultasFrecuentes[i].fil);
                $scope.datosReporte.titulo_reporte=JSON.parse($scope.listaConsultasFrecuentes[i].tit);
                $scope.datosReporte.observaciones_reporte=JSON.parse($scope.listaConsultasFrecuentes[i].obs);
                //*****************/CARGAR UNO POR UNO LOS TRIBUTOS DE LA LOGICA EBIDO A PROBLEMAS DE SINCRONIZACION DE SCOPES
                var tablaTemp;
                for(var i=0;i<listaTablasSelTemp.length;i++){
                    tablaTemp=listaTablasSelTemp[i];
                    $scope.listaTablasSel.push(tablaTemp);
                }
            }
        }, function (data) {
            console.info(data);
        });
    };


    ////////////////////////////////////////
    //////FUNCION PARA SELECCIONAR ATRIBUTOS
    $scope.listaTablasSel=[];
    $scope.seleccionar = function(entity_name,atrib_name,alias_name,indexTab,indexAtr){
        
        $scope.listaTablas[indexTab].atributos[indexAtr].hide=true;
        var atrib_temp_sel={};
        var table_temp_sel=[];
        var Table_temp_sel={};
        var inserto=false;
        var tabla_vacia = true;
        
        //BEGIN: buscar seleccion de tablas existentes y reemplazar sus atributos 
        if($scope.listaTablasSel.length>0){//si no esta vacio
            for (var listaTabl=0;listaTabl<$scope.listaTablasSel.length;listaTabl++) {
                if($scope.listaTablasSel[listaTabl].entity_name===$scope.listaTablas[indexTab].entity_name){
                    if(alias_name=="Apellido Materno"||alias_name=="Apellido Paterno"){
                        for(var atributo=0;atributo<$scope.listaTablas[indexTab].atributos.length;atributo++){
                            if($scope.listaTablas[indexTab].atributos[atributo].alias_name=="Apellido Materno"||$scope.listaTablas[indexTab].atributos[atributo].alias_name=="Apellido Paterno"){
                                $scope.listaTablas[indexTab].atributos[atributo].hide=true;
                                atrib_temp_sel= $scope.listaTablas[indexTab].atributos[atributo];
                                $scope.listaTablasSel[listaTabl].atributos.push(atrib_temp_sel);
                            }
                        }
                    }
                    else{
                        atrib_temp_sel= $scope.listaTablas[indexTab].atributos[indexAtr];
                        $scope.listaTablasSel[listaTabl].atributos.push(atrib_temp_sel);}
                    inserto=true;
                    break;
                }
            }
        }
        if($scope.listaTablasSel.length==0||!inserto){
            if(alias_name=="Apellido Materno"||alias_name=="Apellido Paterno"){
                        for(var atributo=0;atributo<$scope.listaTablas[indexTab].atributos.length;atributo++){
                            if($scope.listaTablas[indexTab].atributos[atributo].alias_name=="Apellido Materno"||$scope.listaTablas[indexTab].atributos[atributo].alias_name=="Apellido Paterno"){
                                $scope.listaTablas[indexTab].atributos[atributo].hide=true;    
                                atrib_temp_sel= $scope.listaTablas[indexTab].atributos[atributo];
                                table_temp_sel.push(atrib_temp_sel);
                            }
                        }
                    }
            else{        
                atrib_temp_sel= $scope.listaTablas[indexTab].atributos[indexAtr];
                table_temp_sel.push(atrib_temp_sel);
            }
            Table_temp_sel={entity_name:$scope.listaTablas[indexTab].entity_name,atributos:table_temp_sel};
            $scope.listaTablasSel.push(Table_temp_sel);
            table_temp_sel=[];
        }
        //OCULTAMOS LA TABLA SI YA ESTA VACIA
        for (var listaAtr=0;listaAtr<$scope.listaTablas[indexTab].atributos.length;listaAtr++) {
                tabla_vacia=tabla_vacia&&$scope.listaTablas[indexTab].atributos[listaAtr].hide;
        }
        $scope.listaTablas[indexTab].hide=tabla_vacia;
    };


    $scope.deseleccionar = function(entity_name,atrib_name,alias_name,indexTab,indexAtr){
       //DECLARAMOS VARIABLES
       var indexTabSel;
       var indexAtrSel;
       //BUSCAMOS EL INDICE DE LA TABLA
       for (var listaTabl=0;listaTabl<$scope.listaTablasSel.length;listaTabl++) {
           if($scope.listaTablasSel[listaTabl].entity_name===$scope.listaTablas[indexTab].entity_name){
               indexTabSel=listaTabl;
               break;
           }else{
               indexTabSel= -1;
           }
       }
       //BUSCAMOS EL INDICE DEl ATRIBUTO
       for (var listaAtri=0;listaAtri<$scope.listaTablasSel[indexTabSel].atributos.length;listaAtri++) {
           if($scope.listaTablasSel[listaTabl].atributos[listaAtri].atrib_name===$scope.listaTablas[indexTab].atributos[indexAtr].atrib_name){
               indexAtrSel=listaAtri;
               break;
           }else{
               indexAtrSel= -1;
           }
       }
       //QUITAMOS DICHO ATRIBUTO
       $scope.listaTablas[indexTab].atributos[indexAtr].hide=false;
       $scope.listaTablasSel[indexTabSel].atributos.splice(indexAtrSel, 1);
       //SI ESTA VACIA LA TABLA QUITAMOS LA TABLA
       if($scope.listaTablasSel[indexTabSel].atributos.length===0){
            $scope.listaTablasSel.splice(indexTabSel,1);
       }

       //FALTA QUITAR LA TABLA DE LA PRIMERA COLUMNA DE SELECCIONAR
       ////MOSTRAMOS LA TABLA SI DESELECCIONAMOS UN ATRIBUTO
       $scope.listaTablas[indexTab].hide=false; 
    };
    
    
    ///////////////////////////////////////querybuilder/////////////////////////////////////////////////
    var dataLogic = '{"group": {"operator": "AND","rules": []}}';

    function htmlEntities(str) {
        return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }

    function computed(group,save) {
        if(!save){
            if (!group) return "";
            for (var str = "(", i = 0; i < group.rules.length; i++) {
                i > 0 && (str += " <strong>" + group.operator + "</strong> ");
                if(group.rules[i].group){
                    str += computed(group.rules[i].group);
                }else{
                    if(group.rules[i].condition=='LIKE'){
                        str += group.rules[i].alias + " " + "CONTENGA" + " " + group.rules[i].dataLogic;
                    }
                    else{
                        str += group.rules[i].alias + " " + htmlEntities(group.rules[i].condition) + " " + group.rules[i].dataLogic;
                    }
                }
            }

            return str + ")";
        }
        else {
            if (!group) return "";
            for (var str = "(", i = 0; i < group.rules.length; i++) {
                i > 0 && (str += " <strong>" + group.operator + "</strong> ");
                if(group.rules[i].group){
                    str += computed(group.rules[i].group);
                }else{
                    if(group.rules[i].condition=='LIKE'){
                        str += group.rules[i].alias + " " + "CONTENGA" + " " + group.rules[i].dataLogic;
                    }
                    else{
                        console.info("Es fecha");
                        if(group.rules[i].data_type=="date"){
                            console.info("ENTRO EN EL TIPO DE DATO");
                            var date = new Date(group.rules[i].dataLogic);
                            var year=date.getFullYear();
                            var month=date.getMonth()+1 //getMonth is zero based;
                            var day=date.getDate();
                            var formatted=year+"-"+month+"-"+day;

                            str += group.rules[i].alias + " " + htmlEntities(group.rules[i].condition) + " " + formatted;
                            //str += group.rules[i].field + " " + group.rules[i].condition + " ('" + formatted+ "') ";
                        }
                        else{
                            str += group.rules[i].alias + " " + htmlEntities(group.rules[i].condition) + " " + group.rules[i].dataLogic;
                        }
                    }
                }
            }

            return str + ")";
        }
        
    }
    function createLogic(group,save) {
        if(!save){
            if (!group) return "";
            for (var str = "(", i = 0; i < group.rules.length; i++) {
                i > 0 && (str += " "+group.operator+" " );
                if(group.rules[i].group){
                    str += createLogic(group.rules[i].group);
                }else{
                    if(group.rules[i].condition=='LIKE'){
                        str += "UPPER("+group.rules[i].field + ") " + group.rules[i].condition + " UPPER('%" + group.rules[i].dataLogic+ "%') ";
                    }
                    else{
                        str += group.rules[i].field + " " + group.rules[i].condition + " ('" + group.rules[i].dataLogic+ "') ";
                    }
                }  
            }
            return str + ")";
        }
        else{
            if (!group) return "";
            for (var str = "(", i = 0; i < group.rules.length; i++) {
                i > 0 && (str += " "+group.operator+" " );
                if(group.rules[i].group){
                    str += createLogic(group.rules[i].group);
                }else{
                    if(group.rules[i].condition=='LIKE'){
                        str += "UPPER("+group.rules[i].field + ") " + group.rules[i].condition + " UPPER('%" + group.rules[i].dataLogic+ "%') ";
                    }
                    else{
                        console.info("Es fecha");
                        if(group.rules[i].data_type=="date"){
                            console.info("ENTRO EN EL TIPO DE DATO");
                            var date = new Date(group.rules[i].dataLogic);
                            var year=date.getFullYear();
                            var month=date.getMonth()+1 //getMonth is zero based;
                            var day=date.getDate();
                            var formatted=year+"-"+month+"-"+day;

                            str += group.rules[i].field + " " + group.rules[i].condition + " ('" + formatted+ "') ";
                        }
                        else{
                            str += group.rules[i].field + " " + group.rules[i].condition + " ('" + group.rules[i].dataLogic+ "') ";
                        }
                    }
                }  
            }
            return str + ")";
        }
        
    }
    
    $scope.json = null;
    $scope.filter = JSON.parse(dataLogic);    
    $scope.$watch('filter', function (newValue) {
        $scope.json = JSON.stringify(newValue, null, 2);
        $scope.output = computed(newValue.group,save);
        $scope.output2 = createLogic(newValue.group,save);///contiene las operaciones logicas las crea a partir de la recursividad
    }, true);
    
    
    
    ////////////////////////////////////////////////////////////////////////LISTAR METADATA 2//////////////////////////////////////////////
    $scope.verReporteConsulta = function(d){
        //////////////////////////////////////SELECCION DE CAMPOS//////////////////////////////////////////
        var select="select row_number() over() ";
        for (var listaTabl=0;listaTabl<$scope.listaTablasSel.length;listaTabl++) {
            for (var listaTablAtrib=0;listaTablAtrib<$scope.listaTablasSel[listaTabl].atributos.length;listaTablAtrib++) {
                if($scope.listaTablasSel[listaTabl].atributos[listaTablAtrib].show){
                    select=select.concat(',');
                    select=select.concat($scope.listaTablasSel[listaTabl].entity_name);
                    select = select.concat(".");
                    select=select.concat($scope.listaTablasSel[listaTabl].atributos[listaTablAtrib].atrib_name);
                    select=select.concat(' ');
                }
            }
        }
        select = select.concat(" FROM ");
        
        /////////////////////////////CREACION DE GRAFO DE ESQUEMA ACTUAL NO CAMBIA SIEMPRESE EJECUTA////////////////////////////////////////////////////
        var request = crud.crearRequest('reportes', 1, 'listarConsultaGeneral');
        crud.listar("/sistema_escalafon",request,function(data){
            $scope.metadata2=data.data;
            var nodos = [];
            var schemas = [];

            /////////////////////////CREAMOS LOS NODOS//////////////////////////////////////
            for(var i=0;i<$scope.metadata2.length;i++){
                if(nodos.indexOf($scope.metadata2[i].table_name)==-1){
                    nodos.push($scope.metadata2[i].table_name);
                    schemas.push($scope.metadata2[i].table_schema);
                    console.info("NODO INSERTADO: (",nodos.length-1,"): ",$scope.metadata2[i].table_schema,".",$scope.metadata2[i].table_name);
                }
            }
            
            //////////////////////////////CREAMOS MATRIZ DE ADYACENCIA DE N x N y llenamos con ZEROS //////////
            var matriz_adyacencia = [];
            for(var i=0;i<nodos.length;i++){
                var temp_matriz_adyacencia = [];
                for(var j=0;j<nodos.length;j++){
                    var item_matrix = {key:"-",value:0};
                    temp_matriz_adyacencia.push(item_matrix);
                }
                matriz_adyacencia.push(temp_matriz_adyacencia);
            }
            
            /////////////////////////LLENAMOS MATRIZ DE ADYACENCIA///////////////////////////
            for(var i=0;i<$scope.metadata2.length;i++){
                for(var j=i+1;j<$scope.metadata2.length;j++){///////encuentra todas lasrelaciones entre tablas generando en ciertos casos grafo ciclico
                    if($scope.metadata2[i].column_name==$scope.metadata2[j].column_name){
                        matriz_adyacencia
                            [nodos.indexOf($scope.metadata2[i].table_name)]
                            [nodos.indexOf($scope.metadata2[j].table_name)].value = 1;
                        matriz_adyacencia
                            [nodos.indexOf($scope.metadata2[i].table_name)]
                            [nodos.indexOf($scope.metadata2[j].table_name)].key = $scope.metadata2[i].column_name;
                        matriz_adyacencia
                            [nodos.indexOf($scope.metadata2[j].table_name)]
                            [nodos.indexOf($scope.metadata2[i].table_name)].value = 1;
                        matriz_adyacencia
                            [nodos.indexOf($scope.metadata2[j].table_name)]
                            [nodos.indexOf($scope.metadata2[i].table_name)].key = $scope.metadata2[i].column_name;}
                }
            }
                        ///////////////////////////MOSTRAMOS CAMINO CON EL BFS///////////////////////////////

            var nodos_por_usar = []///////01243//1,0,5,2,3///////////TABLAS QUE SE ELIGIERON PARA EL REPORTES
            for(var h=0;h<$scope.listaTablasSel.length;h++){
                nodos_por_usar.push(nodos.indexOf($scope.listaTablasSel[h].entity_name));
            }
            
            ////////////////////////////////////////////RECORREMOS GRAFO CON NODOS A USAR SI SON MAS DE 2 CONSTRUYE ARBOL, SINO EJECUTA SOLO 1/////////////////
            var keys_order = [];
            var shortest_path = [];
            if(nodos_por_usar.length>1){
                var temporal = [];
                for(var i = 0;i < nodos_por_usar.length-1;i++){
                    temporal = bfs(matriz_adyacencia,nodos_por_usar[i],nodos_por_usar[i+1]);///temporal siempre va a retornar 2 elementos al menos
                    console.info("INI: ",nodos_por_usar[i],"FIN: ",nodos_por_usar[i+1],"TEMPORAL: ",temporal);
                    for(var j=0; j<temporal.length;j++){
                        if(shortest_path.indexOf(temporal[j])=== -1){///SI NO ENCUENTRA ELEMENTOS EXISTENTES IGUALES NO DENTRA
                            shortest_path.push(temporal[j]); 
                            if(j>0){//mayor que 0 xk debe comenzar en j-1  y no ser un out of index 
                              keys_order.push(matriz_adyacencia[temporal[j-1]][temporal[j]].key);
                            }
                        }else{console.info("REPETIDO: ",temporal[j])}
                    }
                }
                
             }else{
                 if(nodos_por_usar.length>0){
                     shortest_path.push(nodos_por_usar[0]);
                 }
                 else{
                     console.info("no hay tablas seleccionadas");
                 }
             }       
      ////////////////////////////////////////////RECORREMOS GRAFO CON NODOS A USAR SI SON MAS DE 2 CONSTRUYE ARBOL, SINO EJECUTA SOLO 1/////////////////
            var cabeceras = [];
            cabeceras.push({item_cabecera:"N°"});///AGREGAMOS LA NUMERACION A LAS FILAS DEL REPORTE
            for(x in $scope.listaTablasSel){
                for(y in $scope.listaTablasSel[x].atributos){
                    if($scope.listaTablasSel[x].atributos[y].show){//solo si esta marcado para mostrar con el switch lo coloca en cabeceras sino no lo coloca en el query
                        cabeceras.push({item_cabecera:$scope.listaTablasSel[x].atributos[y].alias_name});
                    }
                }
            }

            ////////////////////////////envio de datos al reporte general//////////////////////////////
            if($scope.listaTablasSel.length>0){/////////si hay tablas seleccionadas ejecuta el script para generar REPORTE PDF
                var where = $scope.output2;
//                console.log($scope.datosReporte.titulo_reporte);
//                console.log($scope.datosReporte.observaciones_reporte);
                var resultado_consulta = query_creator(select, shortest_path, schemas ,nodos, keys_order, where);
                console.info("Query Creator:: ",resultado_consulta);
                console.info("select:",select);
                console.info("shortest_path:",shortest_path);
                console.info("schemas:",schemas);
                console.info("nodos:",nodos);
                console.info("key orders:",keys_order);
                console.info("Where:"+ where);
                
                if(d===0){
                    var request = crud.crearRequest('reportes',1,'reporteConsultaGeneral');
                    request.setData({
                            titulo:$scope.datosReporte.titulo_reporte,
                            observaciones_reporte:$scope.datosReporte.observaciones_reporte,
                            consulta:resultado_consulta,
                            cabeceras:cabeceras,
                            idCertificacion:$scope.idCertificacion
                    });
                    crud.insertar('/sistema_escalafon',request,function(response){
                        if(response.responseSta){
                            verDocumentoPestaña(response.data.file);
                        }else{
                            modal.mensaje('ERROR',response.responseMsg);
                        }
                    },function(errResponse){
                        modal.mensaje('ERROR','El servidor no responde');
                    });
                }else if(d===1){
                    var request = crud.crearRequest('reportes',1,'reporteConsultaGeneral');
                    request.setData({
                            titulo:$scope.datosReporte.titulo_reporte,
                            observaciones_reporte:$scope.datosReporte.observaciones_reporte,
                            consulta:resultado_consulta,
                            cabeceras:cabeceras,
                            idCertificacion:$scope.idCertificacion
                    });
                    crud.insertar('/sistema_escalafon',request,function(response){
                        if(response.responseSta){
                            var titulo=$scope.datosReporte.titulo_reporte;
                            descargarArchivo(response.data.file,"ReporteGeneral_"+titulo,"pdf");
                        }else{
                            modal.mensaje('ERROR',response.responseMsg);
                        }
                    },function(errResponse){
                        modal.mensaje('ERROR','El servidor no responde');
                    });
                }else if(d===2){
                    var request = crud.crearRequest('reportes',1,'reporteConsultaGeneral');
                    request.setData({
                            titulo:$scope.datosReporte.titulo_reporte,
                            observaciones_reporte:$scope.datosReporte.observaciones_reporte,
                            consulta:resultado_consulta,
                            cabeceras:cabeceras,
                            idCertificacion:$scope.idCertificacion
                    });
                    crud.insertar('/sistema_escalafon',request,function(response){
                        if(response.responseSta){
                            var titulo=$scope.datosReporte.titulo_reporte;
                            descargarArchivo(response.data.reporteXls,"ReporteGeneral_"+titulo,"xls");
                        }else{
                            modal.mensaje('ERROR',response.responseMsg);
                        }
                    },function(errResponse){
                        modal.mensaje('ERROR','El servidor no responde');
                    });
                }else if(d===3){
                    modal.mensajeConfirmacion($scope, "¿Desea Solictar certificacion de la consulta?", function () {
                    $scope.almacenarConsultaCertificada(resultado_consulta);

                    }, '400');
                }else if(d===4){
                    modal.mensajeConfirmacion($scope, "¿Desea almacenar la consulta frecuente?", function () {
                        $scope.almacenarConsultaFrecuente(resultado_consulta);

                    }, '400');
                }

            }else{"No hay tablas Seleccionadas!!!!"}
            /////////////////////////////envio de datos la reporte general////////////////////////////////////
        }, function (data) {
            console.info(data);
        });
    };
    
    //////////////////////////////////////FILTROS PARA REPORTE/////////////////////////////////
    $scope.listaFiltros=[];
    $scope.addFiltro = function(){
        var filtro={valor:"",tipo:"",index:$scope.listaFiltros.length};
        $scope.listaFiltros.push(filtro);
    };
    $scope.removeFilter = function(index){
        $scope.listaFiltros.splice(index,1);
        for(var i=index;i<$scope.listaFiltros.length;i++){
            $scope.listaFiltros[i].index=$scope.listaFiltros[i].index-1;
        }
    };
    ////////////////////////////////////////BFS porque necesita usar el camino mas corto//////////////////////
    var bfs = (function () {
        function buildPath(parents, targetNode) {
          var result = [targetNode];
          while (parents[targetNode] !== null) {
            targetNode = parents[targetNode];
            result.push(targetNode);
          }
          return result.reverse();
        }

        return function (graph, startNode, targetNode) {
          var parents = [];
          var queue = [];
          var visited = [];
          var current;
          queue.push(startNode);
          parents[startNode] = null;
          visited[startNode] = true;
          while (queue.length) {
            current = queue.shift();
            if (current === targetNode) {
              return buildPath(parents, targetNode);
            }
            for (var i = 0; i < graph.length; i += 1) {
              if (i !== current && graph[current][i].value && !visited[i]) {///accede al vale directo
                parents[i] = current;
                visited[i] = true;
                queue.push(i);
              }
            }
          }
          return null;
        };
    }());
    
    ///////////////////////////////////////QUERYCREATOR/////////////////////////////////////////////////
    var query_creator = (function (select, shortest_path, schemas , nodos, keys_order, where) {
            var salida_string = select;

            for(var i=0; i < shortest_path.length;i++){
                if(i==0){
                    salida_string = salida_string.concat(schemas[shortest_path[i]]);
                    salida_string = salida_string.concat(".");
                    salida_string = salida_string.concat(nodos[shortest_path[i]]);

                }else{
                    salida_string = salida_string.concat(" JOIN ");
                    salida_string = salida_string.concat(schemas[shortest_path[i]]);
                    salida_string = salida_string.concat(".");
                    salida_string = salida_string.concat(nodos[shortest_path[i]]);
                    salida_string = salida_string.concat(" USING (");
                    salida_string = salida_string.concat(keys_order[i-1]);
                    salida_string = salida_string.concat(") ");
                }

            }
            if(where!="()"){////si es k no hay condiciones no agrega el WHERE
                var condiciones = " WHERE ";condiciones = condiciones.concat(where);
                salida_string = salida_string.concat(condiciones);
            }
            /////////////////AGREGAMOS LOS ORDER BY////////////////////////////
            var orderBy="";
            for(var i=0;i<$scope.listaFiltros.length;i++){
                if(i==0){
                    orderBy=orderBy.concat(" ORDER BY ");
                    orderBy=orderBy.concat($scope.listaFiltros[i].valor);
                    orderBy=orderBy.concat(" ");
                    orderBy=orderBy.concat($scope.listaFiltros[i].tipo);
                }else{
                    orderBy=orderBy.concat(", ");
                    orderBy=orderBy.concat($scope.listaFiltros[i].valor);
                    orderBy=orderBy.concat(" ");
                    orderBy=orderBy.concat($scope.listaFiltros[i].tipo);
                }
            }
            salida_string=salida_string.concat(orderBy);
            return salida_string;
    });
    
    //////////////////////////////////////ALMACENAR CONSULTAS ///////////////////////////
    $scope.almacenarConsultaCertificada = function (consulSql) {
        var JSONatributosUsados = "";
        var JSONcatalogo = "";
        var JSONlogica = "";
        var JSONfiltros = "";
        var JSONtitulo = "";
        var JSONobservaciones = "";
        JSONatributosUsados = JSON.stringify($scope.listaTablasSel);
        JSONcatalogo = JSON.stringify($scope.listaTablas);
        JSONlogica = JSON.stringify($scope.filter);
        JSONfiltros = JSON.stringify($scope.listaFiltros);
        JSONtitulo = JSON.stringify($scope.datosReporte.titulo_reporte);
        JSONobservaciones = JSON.stringify($scope.datosReporte.observaciones_reporte);
        var request = crud.crearRequest('reportes',1,'agregarConsultaCertificada');
        request.setData({
                atributosUsados:JSONatributosUsados,
                catalogo:JSONcatalogo,
                logica:JSONlogica,
                filtros:JSONfiltros,
                titulo:JSONtitulo,
                observaciones:JSONobservaciones,
                consultaSql:consulSql,
                idUsuarioEmisor:$rootScope.usuMaster.usuario.usuarioID,
                usuarioEmisor:$rootScope.usuMaster.usuario.nombre
        });
        crud.insertar('/sistema_escalafon',request,function(response){
            if(response.responseSta){
                modal.mensaje("CONFIRMACION", "Se Solicito certificacion de consulta");
                //$scope.listarConsultasFrecuentes();
            }else{
                modal.mensaje('ERROR',response.responseMsg);
            }
        },function(errResponse){
            modal.mensaje('ERROR','El servidor no responde No se pudo almacenar consulta Frecuente');
        }); 
    };
    
    $scope.almacenarConsultaFrecuente = function (consulSql) {
        var JSONatributosUsados = "";
        var JSONcatalogo = "";
        var JSONlogica = "";
        var JSONfiltros = "";
        var JSONtitulo = "";
        var JSONobservaciones = "";
        JSONatributosUsados = JSON.stringify($scope.listaTablasSel);
        JSONcatalogo = JSON.stringify($scope.listaTablas);
        JSONlogica = JSON.stringify($scope.filter);
        JSONfiltros = JSON.stringify($scope.listaFiltros);
        JSONtitulo = JSON.stringify($scope.datosReporte.titulo_reporte);
        JSONobservaciones = JSON.stringify($scope.datosReporte.observaciones_reporte);
        var request = crud.crearRequest('reportes',1,'agregarConsultaFrecuente');
        request.setData({
                atributosUsados:JSONatributosUsados,
                catalogo:JSONcatalogo,
                logica:JSONlogica,
                filtros:JSONfiltros,
                titulo:JSONtitulo,
                observaciones:JSONobservaciones,
                consultaSql:consulSql,
                idUsuarioEmisor:$rootScope.usuMaster.usuario.usuarioID,
                usuarioEmisor:$rootScope.usuMaster.usuario.nombre
        });
        crud.insertar('/sistema_escalafon',request,function(response){
            if(response.responseSta){
                modal.mensaje("CONFIRMACION", "Se agrego la consulta frecuente");
                $scope.listarConsultasFrecuentes();
            }else{
                modal.mensaje('ERROR',response.responseMsg);
            }
        },function(errResponse){
            modal.mensaje('ERROR','El servidor no responde No se pudo almacenar consulta Frecuente');
        }); 
    };
    
}]);
