app.controller("estadisticasSECtrl", ["$rootScope", "$scope", "NgTableParams", "crud", "modal", function ($rootScope, $scope, NgTableParams, crud, modal) {

        $scope.desplazamientos = {
            desde: "",
            hasta: ""
        };
        
        $scope.colorsDesplazamientos = ['#000099', '#000099', '#000099', '#000099', '#000099', '#000099', '#000099', '#000099', '#000099'];
        
        $scope.labelsDesplazamientos = [
            "Designación",
            "Rotación", 
            "Reasignación", 
            "Destaque", 
            "Permuta",  
            "Encargo", 
            "Comisión de servicio", 
            "Transferencia", 
            "Otros"
        ];
        
        $scope.optionsDesplazamientos =  {
            title: {
                display: true,
                text: 'Desplazamientos del personal',
                position: 'top',
                fontSize: 16,
                fontColor: '#000000',
                padding: 20
            },
            scales: {
                yAxes:  [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Nº de trabajadores',
                        fontSize: 14,
                        fontColor: '#000000'
                    }
                }],
                xAxes:  [{
                    barThickness: 60
                }]
            }
        };
        
        var desplazamientos = [];
        listarDesplazamientos();
        function listarDesplazamientos() {
            
            var request = crud.crearRequest('desplazamiento', 1, 'listarPorOrganizacion');
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID});
                   
            crud.listar("/sistema_escalafon", request, function (data) {
                desplazamientos = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        
        $scope.dataDesplazamientos = [];
        $scope.generarEstadisticaDesplazamientos = function () {
            
            var aux = [];
            
            aux = buscarObjetos(desplazamientos, 'tip', '1');
            $scope.dataDesplazamientos.push(aux.length);
            
            aux = buscarObjetos(desplazamientos, 'tip', '2');
            $scope.dataDesplazamientos.push(aux.length);
                              
            aux = buscarObjetos(desplazamientos, 'tip', '3');
            $scope.dataDesplazamientos.push(aux.length);
            
            aux = buscarObjetos(desplazamientos, 'tip', '4');
            $scope.dataDesplazamientos.push(aux.length);
            
            aux = buscarObjetos(desplazamientos, 'tip', '5');
            $scope.dataDesplazamientos.push(aux.length);
            
            aux = buscarObjetos(desplazamientos, 'tip', '6');
            $scope.dataDesplazamientos.push(aux.length);
            
            aux = buscarObjetos(desplazamientos, 'tip', '7');
            $scope.dataDesplazamientos.push(aux.length);
            
            aux = buscarObjetos(desplazamientos, 'tip', '8');
            $scope.dataDesplazamientos.push(aux.length);
            
            aux = buscarObjetos(desplazamientos, 'tip', '9');
            $scope.dataDesplazamientos.push(aux.length);
        };
        
        $scope.numeroHijos = {
            desde: "",
            hasta: ""
        };
        
        $scope.colorsNumeroHijos = [];
        $scope.labelsNumeroHijos = [];
        $scope.dataNumeroHijos = [];
        $scope.optionsNumeroHijos =  {
            title: {
                display: true,
                text: 'Nº de trabajadores con 1 o mas Hijos',
                position: 'top',
                fontSize: 16,
                fontColor: '#000000',
                padding: 20
            },
            scales: {
                yAxes:  [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Cantidad de trabajadores',
                        fontSize: 14,
                        fontColor: '#000000'
                    }
                }],
                xAxes:  [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Nº de hijos',
                        fontSize: 14,
                        fontColor: '#000000'
                    },
                    barThickness: 60
                }]
            }
        };
        
        var numeroHijosPorTrabajador = [];
        listarnumeroHijosPorTrabajador();
        function listarnumeroHijosPorTrabajador() {
            
            var request = crud.crearRequest('reportes', 1, 'getChildrenGroupByWorker');
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID});
                   
            crud.listar("/sistema_escalafon", request, function (data) {
                numeroHijosPorTrabajador = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        $scope.generarEstadisticaNumeroHijos = function () {
            var numHijos = [];
            numeroHijosPorTrabajador.forEach( function (item) {
               numHijos.push(item.numHijos); 
            });
            
            var min = Math.min.apply(null, numHijos);
            var max = Math.max.apply(null, numHijos);
            
            console.log(min);
            console.log(max);
            
            for (var i=min; i< max; i++){
                $scope.labelsNumeroHijos.push(i);
                var aux = buscarObjetos(numeroHijosPorTrabajador, 'numHijos', i);
                $scope.dataNumeroHijos.push(aux.length);
                $scope.colorsNumeroHijos.push('#000099');
            }
        };
        
        $scope.meritosDemeritos = {
            desde: "",
            hasta: ""
        };
        
        $scope.optionsMeritosdemeritos =  {
            legend: {
                display: true,
                labels: {
                    fontColor: '#000000',
                    fontSize: 14
                },
                position: 'bottom'
            },
            title: {
                display: true,
                text: 'Cantidad de meritos y demeritos',
                position: 'top',
                fontSize: 16,
                fontColor: '#000000'
            }
        };
        $scope.colorsMeritosDemeritos = ['#0099ff', '#cc0000'];
        $scope.labelsMeritosDemeritos = ['Meritos', 'Demeritos'];
        $scope.dataMeritosDemeritos = [];
        
        $scope.generarEstadisticaMeritosDemeritos = function () {
            var request = crud.crearRequest('reportes', 1, 'contarMeritosPorOrganizacion');
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID});
                   
            crud.listar("/sistema_escalafon", request, function (data) {
                console.log(data.data);
                $scope.dataMeritosDemeritos.push(data.data.totalMeritos);
            }, function (data) {
                console.info(data);
            });
            
            var request = crud.crearRequest('reportes', 1, 'contarDemeritosPorOrganizacion');
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID});
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.dataMeritosDemeritos.push(data.data.totalDemeritos);
            }, function (data) {
                console.info(data);
            });
            
            console.log($scope.dataMeritosDemeritos);
        };
        
        $scope.imprimirGraficoDesplazamientos = function(){
            var grafico = new MyFile("Reporte Desplazamientos de Personal");
            grafico.parseDataURL( document.getElementById("desplazamientosPorNivelEducativo").toDataURL("image/png") );

            var request = crud.crearRequest('reportes',1,'reporteEstadistico');
            request.setData({grafico: grafico, orgNom: $rootScope.usuMaster.organizacion.nombre});
            crud.insertar("/sistema_escalafon",request,function(response){
                if(response.responseSta){                
                    verDocumento(response.data.reporte);
                }            
            },function(data){
                console.info(data);
            });       
        };
        
        $scope.imprimirGraficoNumeroHijos = function(){
            var grafico = new MyFile("Reporte Cantidad de Hijos");
            grafico.parseDataURL( document.getElementById("numeroHijos").toDataURL("image/png") );

            var request = crud.crearRequest('reportes',1,'reporteEstadistico');
            request.setData({grafico: grafico, orgNom: $rootScope.usuMaster.organizacion.nombre});
            crud.insertar("/sistema_escalafon",request,function(response){
                if(response.responseSta){                
                    verDocumento(response.data.reporte);
                }            
            },function(data){
                console.info(data);
            });       
        };
        
        $scope.imprimirGraficoMeritosyDemeritos = function(){
            var grafico = new MyFile("Reporte de Meritos y Demeritos");
            grafico.parseDataURL( document.getElementById("meritosyDemeritos").toDataURL("image/png") );

            var request = crud.crearRequest('reportes',1,'reporteEstadistico');
            request.setData({grafico: grafico, orgNom: $rootScope.usuMaster.organizacion.nombre});
            crud.insertar("/sistema_escalafon",request,function(response){
                if(response.responseSta){                
                    verDocumento(response.data.reporte);
                }            
            },function(data){
                console.info(data);
            });       
        };

    }]);


