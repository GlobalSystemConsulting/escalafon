var seApp = angular.module('app');
seApp.requires.push('angularModalService');
seApp.requires.push('ngAnimate');

seApp.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/legajo_personal_ridea/:data', {
            templateUrl: 'administrativa/sistema_escalafon/verLegajoPersonalRidea.html',
            controller: 'verLegajoPersonalRideaCtrl',
            controllerAs: 'verLegajoRideaCtrl'
        }).when('/actualizar_datos_trabajador/:data', {
            templateUrl: 'administrativa/sistema_escalafon/ficha_escalafonaria/actualizarDatosTrabajador.html',
            controller: 'actualizarDatosTrabajadorCtrl',
            controllerAs: 'actualizarDatosTrabajadorCtrl'
        }).when('/visualizar_ficha_escalafonaria/:data', {
            templateUrl: 'administrativa/sistema_escalafon/ficha_escalafonaria/visualizarFichaEscalafonaria.html',
            controller: 'visualizarFichaEscalafonariaCtrl',
            controllerAs: 'visualizarFichaEscalafonariaCtrl'
        }).when('/registroFichaEscalafonaria/', {
            templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/registrarFichaEscalafonaria.html",
            controller: "registrarFichaEscalafonariaCtrl"
        });
    }]);


seApp.controller("listaTrabajadoresCtrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {

        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.pages = [];
        $scope.canTotTra = 0;
        $scope.opcionesFiltro = false;
        $scope.f = {nombre: "", apePat: "", apeMat: "", DNI: ""};
        $scope.metaDataFiltro = {campo: "", data: ""};

        $scope.confFiltros = [
            {nomFil: "apePat", est: false, data: ""},
            {nomFil: "apeMat", est: false, data: ""},
            {nomFil: "nom", est: false, data: ""},
            {nomFil: "dni", est: false, data: ""}
        ];

        $scope.filtrar = function () {
            var cont = 0;
            $scope.confFiltros.forEach(function (item) {
                if (item.data.length > 0) {
                    item.est = true;
                    cont++;
                } else {
                    item.est = false;
                }
            });

            if (cont === 0) {
                $scope.opcionesFiltro = false;
                $scope.listarTrabajadores(10, ($scope.currentPage - 1) * 10);
            } else {
                $scope.opcionesFiltro = true;
                $scope.listarTraxFiltros(10, (($scope.currentPage - 1) * 10), $scope.confFiltros);
            }
        };
        $scope.configPages = function () {
            $scope.pages.length = 0;
            var ini = $scope.currentPage - 4;
            var fin = $scope.currentPage + 5;
            if (ini < 1) {
                ini = 1;
                if (Math.ceil($scope.canTotTra / $scope.pageSize) > 10)
                    fin = 10;
                else
                    fin = Math.ceil($scope.canTotTra / $scope.pageSize);
            } else {
                if (ini >= Math.ceil($scope.canTotTra / $scope.pageSize) - 10) {
                    ini = Math.ceil($scope.canTotTra / $scope.pageSize) - 10;
                    fin = Math.ceil($scope.canTotTra / $scope.pageSize);
                }
            }
            if (ini < 1)
                ini = 1;
            for (var i = ini; i <= fin; i++) {
                $scope.pages.push({no: i});
            }

            if ($scope.currentPage >= $scope.pages.length)
                $scope.currentPage = $scope.pages.length - 1;
        };

        $scope.setPage = function (index) {
            $scope.currentPage = index - 1;
            if (!$scope.opcionesFiltro) {
                $scope.listarTrabajadores(10, (index - 1) * 10);
            } else {
                //$scope.listarTrabajadoresxFiltro(10,(index-1)*10,$scope.metaDataFiltro.campo,$scope.metaDataFiltro.data);
                $scope.listarTraxFiltros(10, ((index - 1) * 10), $scope.confFiltros);
            }
        };



        $scope.filter_by = function (field, data) {
            $scope.metaDataFiltro.campo = field;
            $scope.metaDataFiltro.data = data;
            if (data.length === 0) {
                $scope.opcionesFiltro = false;
                $scope.listarTrabajadores(10, ($scope.currentPage - 1) * 10);
            } else {
                $scope.opcionesFiltro = true;
                $scope.listarTrabajadoresxFiltro(10, (($scope.currentPage - 1) * 10), field, data);
            }
        };

        $scope.mostrarDataPagAnt = function () {
            $scope.currentPage = $scope.currentPage - 1;

            var cont = 0;
            $scope.confFiltros.forEach(function (item) {
                if (item.data.length > 0) {
                    item.est = true;
                    cont++;
                } else {
                    item.est = false;
                }
            });

            if (cont === 0) {
                $scope.opcionesFiltro = false;
                $scope.listarTrabajadores(10, ($scope.currentPage - 1) * 10);
            } else {
                $scope.opcionesFiltro = true;
                $scope.listarTraxFiltros(10, (($scope.currentPage) * 10), $scope.confFiltros);
            }
        };

        $scope.mostrarDataPagSig = function () {
            $scope.currentPage = $scope.currentPage + 1;
            var cont = 0;
            $scope.confFiltros.forEach(function (item) {
                if (item.data.length > 0) {
                    item.est = true;
                    cont++;
                } else {
                    item.est = false;
                }
            });

            if (cont === 0) {
                $scope.opcionesFiltro = false;
                $scope.listarTrabajadores(10, ($scope.currentPage - 1) * 10);
            } else {
                $scope.opcionesFiltro = true;
                $scope.listarTraxFiltros(10, (($scope.currentPage) * 10), $scope.confFiltros);
            }
        };

        //Cadena por la cual se listaran los trabajadores
        $scope.cadenaBusqueda = "";
        console.log($scope.cadenaBusqueda);
        //var nuevaB;
        //var myObj = JSON.parse($scope.cadenaBusqueda);
        //console.log(">>>"+myObj);
        //if ($scope.cadenaBusqueda==="")
        //    nuevaB='A';
        //else
        //    nuevaB=$scope.cadenaBusqueda.replace(/\s/g,"_");
        //arreglo donde estan todas los trabajadores
        $scope.trabajadores = [];
        //variable que servira para contener los datos de un trabajador seleccionado
        $scope.trabajadorSel = {};
        //Variables para manejo de la tabla
        $rootScope.paramsTrabajadores = {count: 10};
        $rootScope.settingTrabajadores = {counts: []};
        $rootScope.tablaTrabajadores = new NgTableParams($rootScope.paramsTrabajadores, $rootScope.settingTrabajadores);
        $scope.listarTrabajadores = function (limitI, offsetI) {
            //preparamos un objeto request
            $rootScope.showLoading();
            var request = crud.crearRequest('datos_personales', 1, 'listarPorOrganizacion');
            request.setData({
                orgId: $rootScope.usuMaster.organizacion.organizacionID,
                limit: limitI,
                offset: offsetI,
                opcion: 0,
                nomCampo: "",
                dataCampo: ""});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log("LISTANDO TRABAJADORES sin filtros");
                    console.log(response);
                    var aux = [];
                    response.data.dataEscalafon.forEach(function (item) {
                        if (!buscarObjeto(aux, 'perId', item.perId))
                            aux.push(item);
                    });

                    $rootScope.settingTrabajadores.dataset = aux;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingTrabajadores.dataset);
                    $rootScope.tablaTrabajadores.settings($rootScope.settingTrabajadores);

                    $scope.tablaTrabajadores.reload();
                    $scope.canTotTra = response.data.canTotReg;
                    $scope.configPages();
                    $rootScope.hideLoading();

                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.listarTraxFiltros = function (limitI, offsetI, confFiltros) {
            //preparamos un objeto request
            var request = crud.crearRequest('datos_personales', 1, 'listarxFiltros');
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID, limit: limitI, offset: offsetI, opcion: 1, filtros: confFiltros});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log("LISTANDO TRABAJADORES con filtros");
                    console.log(response);
                    var aux = [];
                    response.data.dataEscalafon.forEach(function (item) {
                        if (!buscarObjeto(aux, 'perId', item.perId))
                            aux.push(item);
                    });

                    $rootScope.settingTrabajadores.dataset = aux;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingTrabajadores.dataset);
                    $rootScope.tablaTrabajadores.settings($rootScope.settingTrabajadores);

                    $scope.tablaTrabajadores.reload();
                    $scope.canTotTra = response.data.canTotReg;
                    $scope.configPages();

                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.verListadoFiltradoTrabajadores = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('datos_personales', 1, 'listarPorOrgyCadIni');
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID, cadenaInicio: $scope.cadenaBusqueda});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    var aux = [];
                    response.data.forEach(function (item) {
                        if (!buscarObjeto(aux, 'perId', item.perId))
                            aux.push(item);
                    });

                    $rootScope.settingTrabajadores.dataset = aux;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingTrabajadores.dataset);
                    $rootScope.tablaTrabajadores.settings($rootScope.settingTrabajadores);

                    $scope.tablaTrabajadores.reload();
                    $scope.canTotUsu = response.data.canTotReg;
                    $scope.configPages();

                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.changePage = function (numberPage) {
            $scope.currentPage = numberPage;
        };

        $scope.actualizarDatosTrabajador = function (t) {
            console.log(t);
            var trabajadorSel = JSON.parse(JSON.stringify(t));
            trabajadorSel.opcion = 5;
            var request = crud.crearRequest('datos_personales', 1, 'buscarFichaPorDNI');
            request.setData(trabajadorSel);
            crud.listar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    //console.log("going to do shit with: "+JSON.stringify(response.data));
                    var finalData = {};
                    crud.listar('/sistema_escalafon', request, function (response) {
                        if (response.responseSta) {
                            for (var key in response.data)
                                finalData[key] = response.data[key];
                            // Once the esc file is retrieved, you must get the personal legacy files from RIDEA
                            var request = crud.crearRequest('legajo_personal_ridea', 1, 'listarLegajosRidea');
                            request.setData(trabajadorSel);
                            crud.listar('/sistema_escalafon', request, function (response) {
                                if (response.responseSta) {
                                    for (var key in response.data)
                                        finalData[key] = response.data[key];
                                    $location.url('/actualizar_datos_trabajador/' + btoa(JSON.stringify(finalData)));
                                    //console.log("docs: "+finalData.Documentos);
                                } else {
                                    $location.url('/actualizar_datos_trabajador/' + btoa(JSON.stringify(finalData)));
                                    modal.mensaje("ERROR", "Se cargó la ficha pero el legajo no ha sido digitalizado");
                                }
                            }, function (error) {
                                modal.mensaje("ERROR", "El legajo no ha sido digitalizado");
                            });
                        } else {
                            modal.mensaje('ERROR', response.responseMsg);
                        }
                    }, function (data) {
                        console.info(data);
                    });
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.visualizarFichaEscalafonaria = function (t) {
            var trabajadorSel = JSON.parse(JSON.stringify(t));
            console.log("datos visualizarFichaEscalafonaria");
            console.log(trabajadorSel);
            trabajadorSel.opcion = 1;
            var request = crud.crearRequest('datos_personales', 1, 'buscarFichaPorDNI');
            request.setData(trabajadorSel);
            console.log("datos visualizarFichaEscalafonaria2");
            console.log(trabajadorSel);
            console.log(request.data);

            crud.listar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    console.log("datos visualizarFichaEscalafonaria3");
                    console.log(response);
                    console.log("going with: " + JSON.stringify(response.data));
                    var finalData = {};
                    crud.listar('/sistema_escalafon', request, function (response) {
                        if (response.responseSta) {
                            for (var key in response.data)
                                finalData[key] = response.data[key];
                            // Once the esc file is retrieved, you must get the personal legacy files from RIDEA
                            var request = crud.crearRequest('legajo_personal_ridea', 1, 'listarLegajosRidea');
                            request.setData(trabajadorSel);
                            crud.listar('/sistema_escalafon', request, function (response) {
                                if (response.responseSta) {
                                    for (var key in response.data)
                                        finalData[key] = response.data[key];
                                    $location.url('/visualizar_ficha_escalafonaria/' + btoa(JSON.stringify(finalData)));
                                    //console.log("docs: "+finalData.Documentos);
                                } else {
                                    $location.url('/visualizar_ficha_escalafonaria/' + btoa(JSON.stringify(finalData)));
                                    modal.mensaje("ERROR", "Se cargó la ficha pero el legajo no ha sido digitalizado");
                                }
                            }, function (error) {
                                modal.mensaje("ERROR", "El legajo no ha sido digitalizado");
                            });
                        } else {
                            modal.mensaje('ERROR', response.responseMsg);
                        }
                    }, function (data) {
                        console.info(data);
                    });
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (data) {
                console.info(data);
            });
        };

        // prepararVerLegajoPersonalRidea
        $scope.prepararVerLegajoPersonalRidea = function (t) {

            var trabajadorSel = JSON.parse(JSON.stringify(t));
            trabajadorSel.opcion = 200;
            var request = crud.crearRequest('legajo_personal_ridea', 1, 'listarLegajosRidea');
            request.setData(trabajadorSel);

            console.info(trabajadorSel);
            crud.listar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    console.info(response.data);
                    $location.url('/legajo_personal_ridea/' + btoa(JSON.stringify(response.data)));
                } else {
                    modal.mensaje("ERROR", "El legajo no ha sido digitalizado");
                }
            }, function (error) {
                modal.mensaje("ERROR", "El legajo no ha sido digitalizado");
            });
        };

        //Registrar Nuevo Trabajador
        $scope.nuevoTrabajador = function () {
            $location.url('/registroFichaEscalafonaria/');
        };

    }]);
seApp.controller('verLegajoPersonalRideaCtrl', ['$routeParams', '$rootScope', '$scope', 'crud', 'NgTableParams', 'ModalService', 'modal', function ($routeParams, $rootScope, $scope, crud, NgTableParams, ModalService, modal) {
        var datos = JSON.parse(atob($routeParams.data));
        $scope.dataRidea = datos;
        var url;
        var serR = "/rideaoffice";
        var serP = "/getPdf?"
        $scope.uri;
        $scope.uriv;
        $scope.service = function () {

            var request = crud.crearRequest('servicioRidea', 1, 'verServicioRIDEA');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                if (data.data) {
                    url = data.data.servicio;
                    $scope.uri = url + serR + serP;
                    $scope.uriv = url + serR;
                    console.info(url);

                } else {
                    modal.mensaje("ERROR", "No se pudo leer el servicio");
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.pdf = true;
        $scope.mostrar = function () {
            $scope.pdf = !$scope.pdf;
        };
        $scope.mostrar2 = function () {
            $scope.pdf = !$scope.pdf;
        };

        $scope.OpenBrowserInNewTab = function (nroPagAgrp, pagIni, uriv) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/visorPDF.html",
                controller: "visorCtrl",
                inputs: {
                    numPaginas: nroPagAgrp,
                    idPagDigital: pagIni,
                    uri: uriv
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararVerAgrupamientos = function (documento) {
            $scope.doc = documento;
            if (!doc.estagrupamiento)
                modal.mensaje("ERROR", "el documento no tiene agruapmientos");


        };

    }]);
seApp.controller('visorCtrl', ['$rootScope', '$scope', '$element', 'close', 'crud', 'modal', 'numPaginas', 'idPagDigital', 'uri', function ($rootScope, $scope, $element, close, crud, modal, numPaginas, idPagDigital, uri) {

        $scope.uri = uri;
        $scope.numPaginas = numPaginas;
        $scope.pagInicial = idPagDigital;
        $scope.pagActual = idPagDigital;
        $scope.pagFinal = numPaginas + idPagDigital - 1;
        $scope.pagInput = "";
        $scope.mostrarPagina = function (t) {
            // when a specific page number is given
            if (t === undefined) {
                if ($scope.pagInput !== "" && !isNaN(parseInt($scope.pagInput)) && $scope.pagInput > 0 && $scope.pagInput <= $scope.numPaginas) {
                    next = $scope.pagInput + $scope.pagInicial - 1;
                    if ($scope.pagActual !== next) {
                        $scope.pagActual = next;
                        $("#idImagenReal").src = $scope.uri + "/cargarPagina?idPagina=" + next;
                    }
                } else
                    alert("Ingrese un número de página válido");
            } else {
                $scope.pagActual = t;
                document.getElementById("idImagenReal").src = $scope.uri + "/cargarPagina?idPagina=" + t;
                document.getElementById("idPagSeleccionada").value = t - $scope.pagInicial + 1;
            }
        };
    }]);
seApp.controller('visualizarFichaEscalafonariaCtrl', ['$routeParams', '$scope', '$rootScope', '$http', 'NgTableParams', 'crud', 'modal', 'ModalService', function ($routeParams, $scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {

        var datosPersonales = JSON.parse(atob($routeParams.data));
        console.log("datos personales: " + JSON.stringify(datosPersonales));
        //
        $scope.fichaEscalafonariaSel = {
            ficEscId: datosPersonales.ficha.ficEscId
        };

        $scope.dataTrabajador = {
            orgiId: datosPersonales.trabajador.orgiId,
            carId: datosPersonales.trabajador.carId,
            catId: datosPersonales.trabajador.catId
        };

        //Variables y funciones para controlar la vista
        $scope.optionsDatosPersonales = [true, false, false, false, false];
        $scope.optionsDatosAcademicos = [true, false, false, false, false, false];
        $scope.optionsExperienciaLaboral = [true, false, false];

        var url;
        var serR = "/rideaoffice";
        var serP = "/getPdf?";
        $scope.uri;
        $scope.uriv;

        $scope.mostrarButtonOpen = true;
        $scope.mostrarButtonClose = false;

        $scope.mostrarAgrupamiento = false;
        $scope.ocultarAgrupamiento = true;

        $scope.showAgrupamiento = function () {
            $scope.mostrarAgrupamiento = true;
            $scope.ocultarAgrupamiento = false;

            $scope.mostrarButtonOpen = false;
            $scope.mostrarButtonClose = true;
        };

        $scope.hideAgrupamiento = function () {
            $scope.mostrarAgrupamiento = false;
            $scope.ocultarAgrupamiento = true;

            $scope.mostrarButtonOpen = true;
            $scope.mostrarButtonClose = false;
        };

        $scope.estadoBttnInfoOrgi = true;
        $scope.estadoInfoOrgi = false;

        $scope.actualizarEstadoOrganismos = function (opcion) {
            switch (opcion) {
                case 0:
                    $scope.estadoBttnInfoOrgi = true;
                    $scope.estadoInfoOrgi = false;

                    break;
                case 1:
                    $scope.estadoBttnInfoOrgi = false;
                    $scope.estadoInfoOrgi = true;
                    break;
            }
        }

        $scope.prepararVerAgrups = function (documento) {
            $scope.doc = documento;
            //ver servicio
            var request = crud.crearRequest('servicioRidea', 1, 'verServicioRIDEA');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                if (data.data) {
                    url = data.data.servicio;
                    $scope.uri = url + serR + serP;
                    $scope.uriv = url + serR;
                } else {
                    modal.mensaje("ERROR", "No se pudo leer el servicio");
                }
            }, function (data) {
                console.info(data);
            });
            //console.log("preparing doc:" +doc.agrupamientos);
            if (!$scope.doc.estagrupamiento)
                modal.mensaje("ERROR", "el documento no tiene agruapmientos");
        };

        $scope.updateOptions = function (numOption, optionsSet) {
            for (var i = 0; i < optionsSet.length; i += 1) {
                optionsSet[i] = false;
            }
            optionsSet[numOption] = true;
        };

        $scope.status_1 = {close: true, statusGD: false, statusLD: false};
        $scope.changeDiscapacidadSi = function () {
            $scope.fichaEscalafonariaSel.perDis = true;
        };
        $scope.changeDiscapacidadNo = function () {
            $scope.fichaEscalafonariaSel.perDis = false;
        };

        $scope.statusDA = false;
        $scope.changeDASi = function () {
            $scope.statusDA = false;
        };
        $scope.changeDANo = function () {
            $scope.statusDA = true;
        };

        function changeAseguradoState(codAutEss) {
            if (codAutEss !== "               ")
                $scope.asegurado = "SI";
            else
                $scope.asegurado = "NO";
        }
        ;

        function changeSistemaPensionesState(sisPen) {
            if (sisPen === "ONP")
                $scope.optionsSP = "NO";
            else
                $scope.optionsSP = "SI";
        }
        ;

        function changeDishabilityState(perDis) {
            if (perDis === true)
                $scope.discapacidad = "SI";
            else
                $scope.discapacidad = "NO";
        }
        ;

        $scope.parsearBooleano = function (i) {
            var o = "";
            if (i) {
                o = "Si";
            } else {
                o = "No";
            }
            return o;
        };

        $scope.parsearBooleano2 = function (i) {
            var o = "";
            if (i) {
                o = "Habilitado";
            } else {
                o = "No Habilitado";
            }
            return o;
        };

        $rootScope.paramsDatCenLab = {count: 10};
        $rootScope.settingDatCenLab = {counts: []};
        $rootScope.tablaDatCenLab = new NgTableParams($rootScope.paramsDatCenLab, $rootScope.settingDatCenLab);

        $rootScope.paramsModLicencias = {count: 10};
        $rootScope.settingModLicencias = {counts: []};
        $rootScope.tablaModLicencias = new NgTableParams($rootScope.paramModLicencias, $rootScope.settingModLicencias);
        $rootScope.tablaModLicencias.sorting({licId: 'asc'});

        $rootScope.paramsModVacaciones = {count: 10};
        $rootScope.settingModVacaciones = {counts: []};
        $rootScope.tablaModVacaciones = new NgTableParams($rootScope.paramModsVacaciones, $rootScope.settingModVacaciones);
        $rootScope.tablaModVacaciones.sorting({vacId: 'asc'});
        
        $rootScope.paramsModAscensos = {count: 10};
        $rootScope.settingModAscensos = {counts: []};
        $rootScope.tablaModAscensos = new NgTableParams($rootScope.paramModsAscensos, $rootScope.settingModAscensos);
        $rootScope.tablaModAscensos.sorting({ascId: 'asc'});
        
        $rootScope.paramsModDesplazamientos = {count: 10};
        $rootScope.settingModDesplazamientos = {counts: []};
        $rootScope.tablaModDesplazamientos = new NgTableParams($rootScope.paramsModDesplazamientos, $rootScope.settingModDesplazamientos);
        $rootScope.tablaModDesplazamientos.sorting({desId: 'asc'});

        $rootScope.paramsModCeses = {count: 10};
        $rootScope.settingModCeses = {counts: []};
        $rootScope.tablaModCeses = new NgTableParams($rootScope.paramsModCeses, $rootScope.settingModCeses);
        $rootScope.tablaModDesplazamientos.sorting({cesId: 'asc'});

        $scope.totalDatosxTabla = {
            tablaDesplazamientos: 0,
            tablaCeses: 0,
            tablaLicencias: 0,
            tablaVacaciones: 0
        };

        var datosPersonales = JSON.parse(atob($routeParams.data));
        console.log("primera datos personales");
        console.log(datosPersonales);
        // setting ridea data
        $scope.dataRidea = datosPersonales.Documentos;
        //"http://190.119.213.87:8091/rideaoffice/getPdf?"
        $scope.uriRidea = $scope.uri;
        $scope.perfil = datosPersonales.persona.foto;
        $scope.trabajadorData = datosPersonales.persona.nom + ' ' + datosPersonales.persona.apePat + ' ' + datosPersonales.persona.apeMat;
        $scope.trabajadorDNI = datosPersonales.persona.dni;
        $scope.trabajadorCargo = "";
        datosPersonales.sessiones.forEach(function (element) {
            $scope.trabajadorCargo += element.rol + " - ";
        });
        $scope.trabajadorCargo = $scope.trabajadorCargo !== "" ? $scope.trabajadorCargo.substr(0, $scope.trabajadorCargo.length - 3) : "";
        $scope.estLabTraSel = "-1";

        var estadosLaborales = [
            {estLabId: '1', estLabDes: "Ingreso"},
            {estLabId: '2', estLabDes: "Rotación"},
            {estLabId: '3', estLabDes: "Proyección"},
            {estLabId: '4', estLabDes: "Retiro"},
            {estLabId: '9', estLabDes: "Reingreso"}
        ];


        $scope.tiposDocumentos = [];
        $scope.jornadasLaborales = [];
        $scope.estadosProceso = [];
        $scope.tiposLicencia = [];

        listarTiposDoc();
        listarJornadasLab();
        listarEstadosPro();
        listarTiposLic();

        function listarTiposDoc() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposDoc');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.tiposDocumentos = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        
        function listarJornadasLab() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarJornadas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.jornadasLaborales = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }

        function listarEstadosPro() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarEstadosPro');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.estadosProceso = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }

        function listarTiposLic() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposLic');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.tiposLicencia = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }

        $scope.OpenBrowserInNewTab = function (coddocumento, nroPagAgrp, pagIni, uriv) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/visorPDF.html",
                controller: "visorCtrl",
                inputs: {
                    numPaginas: nroPagAgrp,
                    idPagDigital: pagIni,
                    uri: uriv
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.mostrarEstLabFormato = function (row) {
            //console.log("estlab of the fucker: "+row.estLab);
            if (row.estLab === undefined || row.estLab === null) {
                row.estLab = '1';
            }
            ;
            switch (row.estLab) {
                case '1':
                    $("#btn-est-lab" + row.traId).removeClass("btn-warning").addClass("btn-success");
                    return estadosLaborales[0].estLabDes;
                case '2':
                    $("#btn-est-lab" + row.traId).removeClass("btn-success").addClass("btn-warning");
                    return estadosLaborales[1].estLabDes;
                case '3':
                    $("#btn-est-lab" + row.traId).removeClass("btn-danger").addClass("btn-info");
                    return estadosLaborales[2].estLabDes;
                case '4':
                    $("#btn-est-lab" + row.traId).removeClass("btn-info").addClass("btn-danger");
                    return estadosLaborales[3].estLabDes;
                case '9':
                    $("#btn-est-lab" + row.traId).removeClass("btn-warning").addClass("btn-success");
                    return estadosLaborales[4].estLabDes;
            }
        };
        $scope.mostrarOpcionDesplazamiento = function (row) {
            switch (row.estLab) {
                case '1':
                    $("#btn-dplz" + row.i).removeClass("btn-success").addClass("btn-danger");
                    return 'Retiro';
                case '2':
                    $("#btn-dplz" + row.i).removeClass("btn-success").addClass("btn-danger");
                    return 'Retiro';
                case '3':
                    $("#btn-dplz" + row.i).removeClass("btn-danger").addClass("btn-success");
                    return 'Re-ingreso';
            }
        };

        //Funciones para listas los datos de tablas
        $rootScope.listarDatCenLab = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('datos_trabajador', 1, 'listarDatCenLabTra');
            request.setData({perId: datosPersonales.persona.perId, traId: datosPersonales.trabajador.traId});

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    response.data.forEach(function (item) {
                        item.estLabDes = buscarContenido(estadosLaborales, 'estLabId', 'estLabDes', item.estLab);
                        datosPersonales.trabajador.estLab = item.estLabDes;
                        $scope.estLabTraSel = item.estLab;
                    });
                    $rootScope.settingDatCenLab.dataset = response.data;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingDatCenLab.dataset);
                    $rootScope.tablaDatCenLab.settings($rootScope.settingDatCenLab);
                } else {
                    response.responseMsg;
                }
            }, function (error) {
                console.info(error);
            });
        };

        $scope.prepararEditarProyeccion = function (d) {
            var last = $scope.totalDatosxTabla.tablaDesplazamientos;
            var fecSal = 0;
            var tipFecSal = 0;
            if ($rootScope.settingModDesplazamientos.dataset[last - 1].tip === "3") {
                fecSal = $rootScope.settingModDesplazamientos.dataset[last - 1].fecTer;
                tipFecSal = 0;
            } else {
                fecSal = "-- En Actividad --";
                tipFecSal = 1;
            }
            console.log(fecSal);

            var SalLic = 0;
            var SalVac = 0;
            var SalInt = 0;

            console.log($scope.totalDatosxTabla);
            var data = {
                title: "Proyección",
                //Desplazamiento
                fecSal: fecSal,
                tipFecSal: tipFecSal,
                totDes: $scope.totalDatosxTabla.tablaDesplazamientos,
                //Vacacion
                fecSalVac: SalVac,
                totVac: $scope.totalDatosxTabla.tablaVacaciones,
                //Licencia
                fecSalLic: SalLic,
                totLic: $scope.totalDatosxTabla.tablaLicencias,
                //Interrupcion
                fecSalInt: SalInt,
                totInt: $scope.totalDatosxTabla.tablaCeses,

                trabajadorInfo: d,
                fichaEscalafonaria: datosPersonales.ficha.ficEscId
            };
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstadoProyeccion.html",
                controller: "editarEstadoProyeccionCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararReportePersonalizado = function (t) {
            var trabajadorSel = JSON.parse(JSON.stringify(t));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/modalSeleccionadosPersonalizado.html",
                controller: "modalSeleccionadosPersonalizadoCtrl",
                inputs: {
                    title: "Modal seleccionados Personalizado",
                    trabajadorInfo: trabajadorSel
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarRotacion = function (d) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstadoRotacion.html",
                controller: "editarEstadoRotacionCtrl",
                inputs: {
                    title: "Rotación",
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    trabajadorInfo: d,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarRetiro = function (d) {
            var data = {
                title: (d.estLab == '1' ? "Retiro" : "Reingreso"),
                trabajadorInfo: d,
                fichaEscalafonaria: datosPersonales.ficha.ficEscId,
                tiposDocumentos: $scope.tiposDocumentos,
                jorLabs: $scope.jornadasLaborales
            };
            ModalService.showModal({
                templateUrl: (d.estLab != '4' ? "administrativa/sistema_escalafon/editarEstadoRetiro.html" : "administrativa/sistema_escalafon/agregarReingreso.html"),
                controller: (d.estLab != '4' ? "editarEstadoRetiroCtrl" : "agregarReingresoCtrl"),
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.listarLicencias = function () {
            var request = crud.crearRequest('licencia', 1, 'listarLicencias');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    response.data.forEach(function (item) {
                        item.estGocDes = item.estGoc ? "Con Goce" : "Sin Goce";
                        
                        var datosFechaTer = item.fecTer.split("-");
                        var datosFechaIni = item.fecIni.split("-");
                                        
                        var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                        var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    
                        item.fecIni = fecIniFor;
                        item.fecTer = fecTerFor;
                    
                    
                        if(item.fecDocSec!==""){
                            var datosFechaInf = item.fecDocSec.split("-");
                            var fecInfFor = new Date(parseInt(datosFechaInf[2]), parseInt(datosFechaInf[1]) - 1, parseInt(datosFechaInf[0]));
                            item.fecDocSec = fecInfFor;
                        }
                        if(item.fecRecInfMain!==""){
                            var datosFechaRecInf = item.fecRecInfMain.split("-");
                            var fecRecInfFor = new Date(parseInt(datosFechaRecInf[2]), parseInt(datosFechaRecInf[1]) - 1, parseInt(datosFechaRecInf[0]));
                            item.fecRecInfMain = fecRecInfFor;

                        }
                        if(item.fecInfMain!==""){
                            var datosFechaDoc = item.fecInfMain.split("-");
                            var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));
                            item.fecInfMain = fecDocFor;

                        }
                        if(item.fecInfMemo!==""){
                            var datosFechaMemo=item.fecInfMemo.split("-");
                            var fecMemFor=new Date(parseInt(datosFechaMemo[2]), parseInt(datosFechaMemo[1]) - 1, parseInt(datosFechaMemo[0]));
                            item.fecInfMemo = fecMemFor;

                        }
                        if(item.fecRecInMemo!==""){                        
                            var datosFechaReiMemo=item.fecRecInMemo.split("-");
                            var fecMemReiFor=new Date(parseInt(datosFechaReiMemo[2]), parseInt(datosFechaReiMemo[1]) - 1, parseInt(datosFechaReiMemo[0]));
                            item.fecRecInMemo = fecMemReiFor;
                        }                         
  
                        item.dias = (fecTerFor - fecIniFor) / (1000 * 60 * 60 * 24) + 1;
                        item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocIdMain);
                        item.estProDes = buscarContenido($scope.estadosProceso, 'id', 'nom', item.estProId);
                        item.tipLicDes = buscarContenido($scope.tiposLicencia, 'id', 'nom', item.tipLicId);
                    });
                    $rootScope.settingModLicencias.dataset = response.data;
                    //
                    $scope.totalDatosxTabla.tablaLicencias = response.data.length;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingModLicencias.dataset);
                    $rootScope.tablaModLicencias.settings($rootScope.settingModLicencias);
                }
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarVacaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('vacacion', 1, 'listarVacaciones');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    response.data.forEach(function (item) {
                        item.estGocDes = item.estGoc ? "Con Goce" : "Sin Goce";
                        console.log(item.fecIni);
                        console.log(item.fecIni.split("-"));
                        var datosFechaTerV = item.fecTer.split("-");
                        var datosFechaIniV = item.fecIni.split("-");
                        var datosFechaInfV = item.fecInf.split("-");
                        var datosFechaRecInfV = item.fecRecInf.split("-");
                        var datosFechaDocV = item.fecDoc.split("-");

                        var fecTerForV = new Date(parseInt(datosFechaTerV[2]), parseInt(datosFechaTerV[1]) - 1, parseInt(datosFechaTerV[0]));
                        var fecIniForV = new Date(parseInt(datosFechaIniV[2]), parseInt(datosFechaIniV[1]) - 1, parseInt(datosFechaIniV[0]));
                        var fecInfForV = new Date(parseInt(datosFechaInfV[2]), parseInt(datosFechaInfV[1]) - 1, parseInt(datosFechaInfV[0]));
                        var fecRecInfForV = new Date(parseInt(datosFechaRecInfV[2]), parseInt(datosFechaRecInfV[1]) - 1, parseInt(datosFechaRecInfV[0]));
                        var fecDocForV = new Date(parseInt(datosFechaDocV[2]), parseInt(datosFechaDocV[1]) - 1, parseInt(datosFechaDocV[0]));
                        console.log(fecIniForV);
                        item.fecIni = fecIniForV;
                        item.fecTer = fecTerForV;
                        item.fecInf = fecInfForV;
                        item.fecRecInf = fecRecInfForV;
                        item.fecDoc = fecDocForV;

                        item.dias = (fecTerForV - fecIniForV) / (1000 * 60 * 60 * 24) + 1;
                        item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                        item.estProDes = buscarContenido($scope.estadosProceso, 'id', 'nom', item.estProId);
                    });

                    $rootScope.settingModVacaciones.dataset = response.data;
                    //
                    $scope.totalDatosxTabla.tablaVacaciones = response.data.length;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingModVacaciones.dataset);
                    $rootScope.tablaModVacaciones.settings($rootScope.settingModVacaciones);
                }
            }, function (error) {
                console.info(error);
            });
        };

        var tiposAscensos = [{id: 1, nom: "Rotación"}, {id: 2, nom: "Cambio de Regimen"}, {id: 3, nom: "Nombramiento"}];
        $scope.listarAscensos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('ascenso', 1, 'listarAscensos');
            request.setData($scope.fichaEscalafonariaSel);
            crud.listar("/sistema_escalafon", request, function (response) {
                console.log("Ascensos:");
                console.log(response);
                response.data.forEach(function (item) {
                    item.tipDes = buscarContenido(tiposAscensos, 'id', 'nom', parseInt(item.tip));
                    item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                });
                console.log(response);
                $rootScope.settingModAscensos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModAscensos.dataset);
                $rootScope.tablaModAscensos.settings($rootScope.settingModAscensos);
            }, function (error) {
                console.info(error);
            });

        };

        var tiposDesplazamientos = [{id: "1", title: "Ingreso"}, {id: "2", title: "Re-ingreso"}, {id: "3", title: "Retiro"}];

        $scope.listarDesplazamientos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                console.log(response);
                response.data.forEach(function (item) {
                    console.log(item.fecIni);
                    console.log(item.fecIni.split("-"));
                    var datosFechaTer = item.fecTer.split("-");
                    var datosFechaIni = item.fecIni.split("-");
                    //var datosFechaInf = item.fecInf.split("-");
                    //var datosFechaRecInf = item.fecRecInf.split("-");
                    var datosFechaDoc = item.fecDoc.split("-");
                    var datosFechaDocTer = item.fecDoc.split("-");

                    var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                    var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    //var fecInfFor = new Date(parseInt(datosFechaInf[2]), parseInt(datosFechaInf[1]) - 1, parseInt(datosFechaInf[0]));
                    //var fecRecInfFor = new Date(parseInt(datosFechaRecInf[2]), parseInt(datosFechaRecInf[1]) - 1, parseInt(datosFechaRecInf[0]));
                    var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));
                    var fecDocTerFor = new Date(parseInt(datosFechaDocTer[2]), parseInt(datosFechaDocTer[1]) - 1, parseInt(datosFechaDocTer[0]));
                    console.log(fecIniFor);
                    item.fecIni = fecIniFor;
                    item.fecTer = fecTerFor;
                    //item.fecInf = fecInfFor;
                    //item.fecRecInf = fecRecInfFor;
                    item.fecDoc = fecDocFor;
                    item.fecDocTer = fecDocTerFor;

                    item.tipDes = buscarContenido(tiposDesplazamientos, 'id', 'title', item.tip);
                    item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                    item.jorLabDes = buscarContenido($scope.jornadasLaborales, 'id', 'nom', item.jorLabId);
                });
                $rootScope.settingModDesplazamientos.dataset = response.data;
                //
                $scope.totalDatosxTabla.tablaDesplazamientos = response.data.length;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModDesplazamientos.dataset);
                $rootScope.tablaModDesplazamientos.settings($rootScope.settingModDesplazamientos);
            }, function (error) {
                console.info(error);
            });
        };
        
        var tiposCeses = [{id: "0", nom: "Otras Interrupciones"}, {id: "1", nom: "Régimen Laboral"}];

        $scope.listarCeses = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('cese', 1, 'listarCeses');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                console.log(response);
                response.data.forEach(function (item) {
                    console.log(item.fecIni);
                    console.log(item.fecIni.split("-"));
                    var datosFechaTer = item.fecTer.split("-");
                    var datosFechaIni = item.fecIni.split("-");
                    //var datosFechaInf = item.fecInf.split("-");
                    //var datosFechaRecInf = item.fecRecInf.split("-");
                    var datosFechaDoc = item.fecDoc.split("-");

                    var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                    var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    //var fecInfFor = new Date(parseInt(datosFechaInf[2]), parseInt(datosFechaInf[1]) - 1, parseInt(datosFechaInf[0]));
                    //var fecRecInfFor = new Date(parseInt(datosFechaRecInf[2]), parseInt(datosFechaRecInf[1]) - 1, parseInt(datosFechaRecInf[0]));
                    var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));
                    console.log(fecIniFor);
                    item.fecIni = fecIniFor;
                    item.fecTer = fecTerFor;
                    //item.fecInf = fecInfFor;
                    //item.fecRecInf = fecRecInfFor;
                    item.fecDoc = fecDocFor;

                    item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                    item.jorLabDes = buscarContenido($scope.jornadasLaborales, 'id', 'nom', item.jorLabId);
                    item.tipCesDes = buscarContenido(tiposCeses, 'id', 'nom', item.tipCes);
                });
                $rootScope.settingModCeses.dataset = response.data;
                console.info(response);
                //
                $scope.totalDatosxTabla.tablaCeses = response.data.length;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModCeses.dataset);
                $rootScope.tablaModCeses.settings($rootScope.settingModCeses);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.showNuevaLicencia = function () {
            var data = {
                title: "Nueva Licencia",
                fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                tiposDocumentos: $scope.tiposDocumentos,
                estadosProceso: $scope.estadosProceso,
                tiposLicencias: $scope.tiposLicencia,
                orgiId: $scope.dataTrabajador.orgiId,
                catId: $scope.dataTrabajador.catId,
                carId: $scope.dataTrabajador.carId

            };
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarLicencia.html",
                controller: "agregarNuevaLicenciaCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevaVacacion = function () {
            var data = {
                title: "Nueva Vacación",
                fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                tiposDocumentos: $scope.tiposDocumentos,
                estadosProceso: $scope.estadosProceso,
                orgiId: $scope.dataTrabajador.orgiId,
                catId: $scope.dataTrabajador.catId,
                carId: $scope.dataTrabajador.carId

            };
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarVacacion.html",
                controller: "agregarNuevaVacacionCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevoCese = function () {
            var data = {
                title: "Nueva interrupción",
                fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                tiposDocumentos: $scope.tiposDocumentos,
                jornadasLaborales: $scope.jornadasLaborales,
                tiposCeses: tiposCeses,
                orgiId: $scope.dataTrabajador.orgiId,
                catId: $scope.dataTrabajador.catId,
                carId: $scope.dataTrabajador.carId
            };
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarInterrupcion.html",
                controller: "agregarNuevoCeseCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showOrganigramaRegLab = function (t) {
            var operacion = 0;
            if (t.orgiId === 0)
                operacion = 1;
            else
                operacion = 2;
            var data = {
                title: "Información jerárquica y de ubicación",
                operacion: operacion,
                trabajador: t
            };
            console.log(t);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/visualizarOrganigrama.html",
                controller: "visualizarOrganigramaRegLabCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarDesplazamiento = function (d) {
            var despSel = JSON.parse(JSON.stringify(d));
            var tipoTitle = "";
            var opcion = 0;
            switch (d.tip) {
                case "1":
                    tipoTitle = "Ingreso";
                    opcion = 1;
                    break;
                case "2":
                    tipoTitle = "Re-Ingreso";
                    opcion = 1;
                    break;
                case "3":
                    tipoTitle = "Retiro";
                    opcion = 2;
                    break;
            }
            ;
            var data = {
                title: "Editar datos de " + tipoTitle,
                desplazamiento: despSel,
                opcion: opcion,
                tiposDocumentos: $scope.tiposDocumentos,
                jornadasLaborales: $scope.jornadasLaborales,
                tiposDesplazamientos: tiposDesplazamientos
            };

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarDesplazamiento.html",
                controller: "editarDesplazamientoCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarCese = function (ceseSel) {
            var ceseSelec = JSON.parse(JSON.stringify(ceseSel));
            var data = {
                title: "Editar interrupción",
                ceseSel: ceseSelec,
                tiposDocumentos: $scope.tiposDocumentos,
                jornadasLaborales: $scope.jornadasLaborales,
                tiposCeses: tiposCeses
            };
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarInterrupcion.html",
                controller: "editarCeseCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarAscenso = function (ascSel) {
            var ascSelec = JSON.parse(JSON.stringify(ascSel));
            var data = {
                title: "Editar datos de Rotación",
                ascenso: ascSelec,
                tiposDocumentos: $scope.tiposDocumentos
            };
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarAscenso.html",
                controller: "editarAscensoCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarLicencia = function (c) {
            var licSel = JSON.parse(JSON.stringify(c));
            var data = {
                title: "Editar datos de Licencia",
                licenciaSel: licSel,
                fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                tiposDocumentos: $scope.tiposDocumentos,
                estadosProceso: $scope.estadosProceso,
                tiposLicencias: $scope.tiposLicencia
            };

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarLicencia.html",
                controller: "editarLicenciaCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarVacacion = function (v) {
            var vacSel = JSON.parse(JSON.stringify(v));
            var data = {
                title: "Editar datos de Vacación",
                vacacionSel: vacSel,
                fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                tiposDocumentos: $scope.tiposDocumentos,
                estadosProceso: $scope.estadosProceso
            };

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarVacacion.html",
                controller: "editarVacacionCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.eliminarCese = function (c) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el cese?", function () {
                var request = crud.crearRequest('cese', 1, 'eliminarCese');
                request.setData({cesId: c.cesId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModCeses.settings().dataset, function (item) {
                            return c === item;
                        });
                        $rootScope.tablaModCeses.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarLicencia = function (l) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la licencia?", function () {
                var request = crud.crearRequest('licencia', 1, 'eliminarLicencia');
                request.setData({licId: l.licId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModLicencias.settings().dataset, function (item) {
                            return l === item;
                        });
                        $rootScope.tablaModLicencias.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarVacacion = function (v) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la vacación?", function () {
                var request = crud.crearRequest('vacacion', 1, 'eliminarVacacion');
                request.setData({vacId: v.vacId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModVacaciones.settings().dataset, function (item) {
                            return v === item;
                        });
                        $rootScope.tablaModVacaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.showRealizarRotacion = function (t) {
            var data = {
                title: "Desplazamiento",
                trabajador: t,
                tiposAscensos: tiposAscensos,
                tiposDocumentos: $scope.tiposDocumentos,
                fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                orgiAntId: $scope.dataTrabajador.orgiId,
                catAntId: $scope.dataTrabajador.catId,
                carAntId: $scope.dataTrabajador.carId
            };
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/realizarRotacion.html",
                controller: "realizarRotacionCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.cambiarEstadoLaboral = function (row) {
            console.log(row);
            var newEstLab = '';
            switch (row.estLab) {
                case '1':
                    newEstLab = '3';
                    break;
                case '2':
                    newEstLab = '3';
                    break;
                case '3':
                    newEstLab = '2';
                    break;
            }/*
             switch(row.estLab){
             case '2': $("#btn-asist" + row.i).removeClass("btn-danger").addClass("btn-success"); break;
             case '3': $("#btn-asist" + row.i).removeClass("btn-success").addClass("btn-danger"); break;
             }*/

            var data = {
                title: (row.estLab === '2' ? "Retiro" : "Reingreso"),
                trabajadorInfo: row,
                newEstLab: newEstLab,
                fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                tiposDocumentos: $scope.tiposDocumentos,
                jornadasLaborales: $scope.jornadasLaborales
            };
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarEstadoLaboral.html",
                controller: "editarEstadoLaboralCtrl",
                inputs: {
                    data: data
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        //$scope.seleccionados = [false, false,false, false, false,false, false, false,false, ---, false,false, false,-----,------];
        $scope.verFichaEscalafonaria = function (t) {

            $scope.selecc = [];//[false, false,false, false, false,false, false, false,false, false, false,false, false, false, false];
            var trabajadorSel = JSON.parse(JSON.stringify(t));

            var request = crud.crearRequest('reportes', 1, 'selecReporteFicha');
            request.setData({traId: trabajadorSel.traId, seleccionados: $scope.selecc});
            crud.insertar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    for (i = 0; i < response.data.seleccionados.length; i++)
                    {
                        $scope.selecc[i] = response.data.seleccionados[i];
                    }
                }
            }, function (error) {
                console.info(error);
            });

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/modalSeleccionados.html",
                controller: "modalSeleccionadosCtrl",
                inputs: {
                    title: "Modal seleccionados",
                    ficEscId: trabajadorSel.ficEscId,
                    traId: trabajadorSel.traId,
                    perDni: trabajadorSel.perDni,
                    selecc: $scope.selecc
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {

                    }
                });
            });

        };


    }]);
seApp.controller('editarEstadoProyeccionCtrl', ['$rootScope', '$scope', '$element', 'data', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, data, close, crud, modal) {

        var dataInput = data;
        $scope.title = dataInput.title;
        
        $scope.fichaEscalafonariaSel = {
            ficEscId: dataInput.fichaEscalafonaria
        };
        console.info(dataInput.fichaEscalafonaria);
        $scope.trabajadorSel = {};
        var trabajadorInfo = dataInput.trabajadorInfo;
        
        $scope.estadosProceso=[];
        function listarEstadosPro() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarEstadosPro');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.estadosProceso = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarEstadosPro(); 
        $scope.tiposLicencia=[];
        function listarTiposLic() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposLic');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.tiposLicencia = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarTiposLic();

        
        $scope.jornadasLaborales=[];
        function listarJornadasLab() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarJornadas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.jornadasLaborales = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarJornadasLab();
        
        $scope.tiposDocumentos = [];
        function listarTiposDoc() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposDoc');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.tiposDocumentos = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarTiposDoc();

        function listarCeses() {
            //preparamos un objeto request
            var request = crud.crearRequest('cese', 1, 'listarCeses');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                console.log(response);
                response.data.forEach(function (item) {
                    console.log(item.fecIni);
                    console.log(item.fecIni.split("-"));
                    var datosFechaTer = item.fecTer.split("-");
                    var datosFechaIni = item.fecIni.split("-");
                    //var datosFechaInf = item.fecInf.split("-");
                    //var datosFechaRecInf = item.fecRecInf.split("-");
                    var datosFechaDoc = item.fecDoc.split("-");

                    var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                    var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    //var fecInfFor = new Date(parseInt(datosFechaInf[2]), parseInt(datosFechaInf[1]) - 1, parseInt(datosFechaInf[0]));
                    //var fecRecInfFor = new Date(parseInt(datosFechaRecInf[2]), parseInt(datosFechaRecInf[1]) - 1, parseInt(datosFechaRecInf[0]));
                    var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));
                    console.log(fecIniFor);
                    item.fecIni = fecIniFor;
                    item.fecTer = fecTerFor;
                    //item.fecInf = fecInfFor;
                    //item.fecRecInf = fecRecInfFor;
                    item.fecDoc = fecDocFor;

                    item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                    item.jorLabDes = buscarContenido($scope.jornadasLaborales, 'id', 'nom', item.jorLabId);
                });
                $rootScope.settingModCeses.dataset = response.data;
                console.info(response);
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModCeses.dataset);
                $rootScope.tablaModCeses.settings($rootScope.settingModCeses);
            }, function (error) {
                console.info(error);
            });
        }
        listarCeses();
        
        function listarVacaciones()  {
            //preparamos un objeto request
            var request = crud.crearRequest('vacacion', 1, 'listarVacaciones');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    response.data.forEach(function (item) {
                        item.estGocDes = item.estGoc ? "Con Goce" : "Sin Goce";
                        console.log(item.fecIni);
                        console.log(item.fecIni.split("-"));
                        var datosFechaTerV = item.fecTer.split("-");
                        var datosFechaIniV = item.fecIni.split("-");
                        var datosFechaInfV = item.fecInf.split("-");
                        var datosFechaRecInfV = item.fecRecInf.split("-");
                        var datosFechaDocV = item.fecDoc.split("-");

                        var fecTerForV = new Date(parseInt(datosFechaTerV[2]), parseInt(datosFechaTerV[1]) - 1, parseInt(datosFechaTerV[0]));
                        var fecIniForV = new Date(parseInt(datosFechaIniV[2]), parseInt(datosFechaIniV[1]) - 1, parseInt(datosFechaIniV[0]));
                        var fecInfForV = new Date(parseInt(datosFechaInfV[2]), parseInt(datosFechaInfV[1]) - 1, parseInt(datosFechaInfV[0]));
                        var fecRecInfForV = new Date(parseInt(datosFechaRecInfV[2]), parseInt(datosFechaRecInfV[1]) - 1, parseInt(datosFechaRecInfV[0]));
                        var fecDocForV = new Date(parseInt(datosFechaDocV[2]), parseInt(datosFechaDocV[1]) - 1, parseInt(datosFechaDocV[0]));
                        console.log(fecIniForV);
                        item.fecIni = fecIniForV;
                        item.fecTer = fecTerForV;
                        item.fecInf = fecInfForV;
                        item.fecRecInf = fecRecInfForV;
                        item.fecDoc = fecDocForV;

                        item.dias = (fecTerForV - fecIniForV) / (1000 * 60 * 60 * 24) + 1;
                        item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                        item.estProDes = buscarContenido($scope.estadosProceso, 'id', 'nom', item.estProId);
                    });

                    $rootScope.settingModVacaciones.dataset = response.data;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingModVacaciones.dataset);
                    $rootScope.tablaModVacaciones.settings($rootScope.settingModVacaciones);
                }
            }, function (error) {
                console.info(error);
            });
        };
        listarVacaciones();
        
        function listarLicencias() {
            var request = crud.crearRequest('licencia', 1, 'listarLicencias');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    
                    response.data.forEach(function (item) {
                        item.estGocDes = item.estGoc ? "Con Goce" : "Sin Goce";
                        var datosFechaTer = item.fecTer.split("-");
                        var datosFechaIni = item.fecIni.split("-");
                                        
                        var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                        var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    
                        item.fecIni = fecIniFor;
                        item.fecTer = fecTerFor;
                    
                    
                        if(item.fecDocSec!==""){
                            var datosFechaInf = item.fecDocSec.split("-");
                            var fecInfFor = new Date(parseInt(datosFechaInf[2]), parseInt(datosFechaInf[1]) - 1, parseInt(datosFechaInf[0]));
                            item.fecDocSec = fecInfFor;
                        }
                        if(item.fecRecInfMain!==""){
                            var datosFechaRecInf = item.fecRecInfMain.split("-");
                            var fecRecInfFor = new Date(parseInt(datosFechaRecInf[2]), parseInt(datosFechaRecInf[1]) - 1, parseInt(datosFechaRecInf[0]));
                            item.fecRecInfMain = fecRecInfFor;

                        }
                        if(item.fecInfMain!==""){
                            var datosFechaDoc = item.fecInfMain.split("-");
                            var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));
                            item.fecInfMain = fecDocFor;

                        }
                        if(item.fecInfMemo!==""){
                            var datosFechaMemo=item.fecInfMemo.split("-");
                            var fecMemFor=new Date(parseInt(datosFechaMemo[2]), parseInt(datosFechaMemo[1]) - 1, parseInt(datosFechaMemo[0]));
                            item.fecInfMemo = fecMemFor;

                        }
                        if(item.fecRecInMemo!==""){                        
                            var datosFechaReiMemo=item.fecRecInMemo.split("-");
                            var fecMemReiFor=new Date(parseInt(datosFechaReiMemo[2]), parseInt(datosFechaReiMemo[1]) - 1, parseInt(datosFechaReiMemo[0]));
                            item.fecRecInMemo = fecMemReiFor;
                        }                         
  
                        item.dias = (fecTerFor - fecIniFor) / (1000 * 60 * 60 * 24) + 1;
                        item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocIdMain);
                        item.estProDes = buscarContenido($scope.estadosProceso, 'id', 'nom', item.estProId);
                        item.tipLicDes = buscarContenido($scope.tiposLicencia, 'id', 'nom', item.tipLicId);
                    });
                    $rootScope.settingModLicencias.dataset = response.data;
                    console.log($rootScope.settingModLicencias.dataset);
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingModLicencias.dataset);
                    $rootScope.tablaModLicencias.settings($rootScope.settingModLicencias);
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarLicencias();
        
        var tiposDesplazamientos = [{id: "1", title: "Ingreso"}, {id: "2", title: "Re-ingreso"}, {id: "3", title: "Retiro"}];
        function listarDesplazamientos() {
            //preparamos un objeto request
            var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                console.log(response);
                response.data.forEach(function (item) {
                    console.log(item.fecIni);
                    console.log(item.fecIni.split("-"));
                    var datosFechaTer = item.fecTer.split("-");
                    var datosFechaIni = item.fecIni.split("-");
                    var datosFechaDoc = item.fecDoc.split("-");

                    var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                    var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));
                    console.log(fecIniFor);
                    item.fecIni = fecIniFor;
                    item.fecTer = fecTerFor;
                    item.fecDoc = fecDocFor;

                    item.tipDes = buscarContenido(tiposDesplazamientos, 'id', 'title', item.tip);
                    item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                    item.jorLabDes = buscarContenido($scope.jornadasLaborales, 'id', 'nom', item.jorLabId);
                });
                $rootScope.settingModDesplazamientos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModDesplazamientos.dataset);
                $rootScope.tablaModDesplazamientos.settings($rootScope.settingModDesplazamientos);
            }, function (error) {
                console.info(error);
            });
        }
        listarDesplazamientos();
        
        var tiposAscensos = [{id: 1, nom: "Rotación"}, {id: 2, nom: "Cambio de Regimen"}, {id: 3, nom: "Nombramiento"}];
        function listarAscensos() {
            //preparamos un objeto request
            var request = crud.crearRequest('ascenso', 1, 'listarAscensos');
            request.setData($scope.fichaEscalafonariaSel);
            crud.listar("/sistema_escalafon", request, function (response) {
                console.log("Ascensos:");
                console.log(response);
                response.data.forEach(function (item) {
                    item.tipDes = buscarContenido(tiposAscensos, 'id', 'nom', parseInt(item.tip));
                    item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                });
                console.log(response);
                $rootScope.settingModAscensos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModAscensos.dataset);
                $rootScope.tablaModAscensos.settings($rootScope.settingModAscensos);
            }, function (error) {
                console.info(error);
            });

        }
        listarAscensos();
        
        var tamDes=$rootScope.settingModDesplazamientos.dataset.length;
        $scope.fechaIngresoParse = JSON.parse(JSON.stringify($rootScope.settingModDesplazamientos.dataset[0].fecIni));
        console.log("Fecha de Ingreso");
        console.info($scope.fechaIngresoParse);
        $scope.subfecha=$scope.fechaIngresoParse.substring(0, 10);
        console.log("Fecha de inicio");
        console.log($scope.subfecha);
        
        $scope.datosProyeccion = {
            fechaIngreso: $scope.subfecha,
            fechaSalida: dataInput.fecSal,
            tiempoAcu: 0,
            tiempoLic: 0,
            tiempoVac: 0,
            tiempoInt: 0,
            tiempoUNSA: 0,
            tiempoTOTAL: 0,
            tiempo25Anios: 0,
            tiempo30Anios: 0,
            regLabAnio:0,
            regLabMes:0,
            regLabDia:0,
            licAnio:0,
            licMes:0,
            licDia:0,
            intAnio:0,
            intMes:0,
            intDia:0,
            
            tiempoLicInt:0
            
        };

        $scope.aniosOut = {
            an: 0,
            me: 0,
            di: 0,
            an1: trabajadorInfo.an1,
            me1: trabajadorInfo.me1,
            di1: trabajadorInfo.di1,
            an2: trabajadorInfo.an2,
            me2: trabajadorInfo.me2,
            di2: trabajadorInfo.di2
        };
        
            var aniosToDias = $scope.aniosOut.an1 * 365 + $scope.aniosOut.an2 * 365;
            var mesesToDias = $scope.aniosOut.me1 * 30 + $scope.aniosOut.me2 * 30;
            var diasToDias = $scope.aniosOut.di1 + $scope.aniosOut.di2;

            $scope.datosProyeccion.tiempoTOTAL = ($scope.datosProyeccion.tiempoAcu + aniosToDias + mesesToDias + diasToDias) - ($scope.datosProyeccion.tiempoInt + $scope.datosProyeccion.tiempoLicVac);
            $scope.datosProyeccion.tiempoTOTALDes = calcularFechaToDias($scope.datosProyeccion.tiempoTOTAL);
            aniosServicio25();
            aniosServicio30();
        
        
        $scope.proyeccion={
            tiempoServ: $scope.aniosOut,
            trabajador: trabajadorInfo
        };
        $scope.actualizarProyeccion = function () {
            var request = crud.crearRequest('datos_trabajador', 1, 'actualizarProyeccion');
            request.setData($scope.proyeccion);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    $scope.aniosOut.an1=response.data.an1;
                    $scope.aniosOut.me1=response.data.me1;
                    $scope.aniosOut.di1=response.data.di1;
                    $scope.aniosOut.an2= response.data.an2;
                    $scope.aniosOut.me2=response.data.me2;
                    $scope.aniosOut.di2= response.data.di2;
                    console.log("ACTUALIZO PROYECCION",response.data);
                }
            }, function (data) {
                console.info(data);
            });
        };
        

        calcularTiempoAcumulado();
        function calcularTiempoAcumulado() {

                var tiempoAcu = 0;
                
                var fechaIngresoDate = JSON.parse(JSON.stringify($rootScope.settingModDesplazamientos.dataset[0].fecIni));
                console.info("fechaIngresoDate");
                console.info(fechaIngresoDate);
                var fechaIngresoParse = JSON.parse(JSON.stringify(fechaIngresoDate));
               
                console.info(fechaIngresoParse);
                var fecIngDis = fechaIngresoParse.split("-");
                console.info(parseInt(fecIngDis[2]));
                console.info(parseInt(fecIngDis[1]) - 1);
                console.info(parseInt(fecIngDis[0]));
                var fecIngFor = new Date(parseInt(fecIngDis[0]), parseInt(fecIngDis[1]) - 1, parseInt(fecIngDis[2]));
                console.info(fecIngFor);
                var fecSalFor = new Date();
                console.info(fecSalFor);
                var acumulacion = getDiffDatesv2(fecIngFor, fecSalFor);
                
                tiempoAcu = tiempoAcu + acumulacion.totalDias;
                console.info(acumulacion);
                console.info(tiempoAcu);
                var tiAcu = calcularFechaToDias(tiempoAcu);
            $scope.datosProyeccion.tiempoAcu = tiempoAcu;
            $scope.datosProyeccion.tiempoAcuDes = tiAcu.des;
            $scope.datosProyeccion.regLabAnio=tiAcu.anios;
            $scope.datosProyeccion.regLabMes=tiAcu.meses;
            $scope.datosProyeccion.regLabDia=tiAcu.dias;
            console.log("TIEMPO ACUMULADO");
            console.log($scope.datosProyeccion.tiempoAcuDes);
            console.log($scope.datosProyeccion.regLabAnio);
            console.log($scope.datosProyeccion.regLabMes);
            console.log($scope.datosProyeccion.regLabDia);
        }
        ;

        calcularTiempoVacacionLicencia();
        function calcularTiempoVacacionLicencia() {
            var tiempoDia = 0;
            var tiempoMes = 0;
            var tiempoAnio = 0;

            if($rootScope.settingModLicencias.dataset.length!==undefined){
                for (var i = 0; i < $rootScope.settingModLicencias.dataset.length; i = i + 1) {
                    var ingSalL = $rootScope.settingModLicencias.dataset[i];

                    if (!ingSalL.estGoc) {
                        tiempoDia = tiempoDia+ ingSalL.diaPro;
                        tiempoMes = tiempoMes+ ingSalL.mesPro;
                        tiempoAnio = tiempoAnio+ ingSalL.anioPro;
                    }
                }
            }
            //tiempoVacLic = tiempoVac + tiempoLic;  //Solo licencias
            var tiLic=calcularSumaFechaToDiasMesesAnios(tiempoAnio,tiempoMes,tiempoDia);
            $scope.datosProyeccion.licAnio=tiLic.anios;
            $scope.datosProyeccion.licMes=tiLic.meses;
            $scope.datosProyeccion.licDia=tiLic.dias;
            $scope.datosProyeccion.tiempoLicVacDes = tiLic.des;
            console.log("LICENCIA");
            console.log($scope.datosProyeccion.tiempoLicVacDes);
            console.log($scope.datosProyeccion.licAnio);
            console.log($scope.datosProyeccion.licMes);
            console.log($scope.datosProyeccion.licDia);
        }

        calcularTiempoInterrupcion();
        function calcularTiempoInterrupcion() {
            var tiempoDia = 0;
            var tiempoMes = 0;
            var tiempoAnio = 0;
            if($rootScope.settingModCeses.dataset.length!==undefined){
                for (var i = 0; i < $rootScope.settingModCeses.dataset.length; i = i + 1) {
                    console.log($rootScope.settingModCeses.dataset[i]);
                    var ingSalInt =$rootScope.settingModCeses.dataset[i];
                    console.log(ingSalInt);
                    
                    
                    tiempoDia = parseInt(tiempoDia+ ingSalInt.diasPro);
                    tiempoMes = parseInt(tiempoMes+ ingSalInt.mesesPro);
                    tiempoAnio = parseInt(tiempoAnio+ ingSalInt.aniosPro);
                    
                }
            }
            var tiInt=calcularSumaFechaToDiasMesesAnios(tiempoAnio,tiempoMes,tiempoDia);
            console.log("tiempo interrupcion");
            $scope.datosProyeccion.intAnio=tiInt.anios;
            $scope.datosProyeccion.intMes=tiInt.meses;
            $scope.datosProyeccion.intDia=tiInt.dias;
            $scope.datosProyeccion.tiempoIntDes = tiInt.des;
            console.log("INTERUUPCION");
            console.log($scope.datosProyeccion.tiempoIntDes);
            console.log($scope.datosProyeccion.intAnio);
            console.log($scope.datosProyeccion.intMes);
            console.log($scope.datosProyeccion.intDia);
        }
        ;


        calcularTiempoUnsa();
        function calcularTiempoUnsa() {
            var aniosLicInt=0;
            var mesesLicInt=0;
            var diasLicInt=0;
            aniosLicInt = $scope.datosProyeccion.intAnio + $scope.datosProyeccion.licAnio;
            mesesLicInt = $scope.datosProyeccion.intMes + $scope.datosProyeccion.licMes;
            diasLicInt = $scope.datosProyeccion.intDia + $scope.datosProyeccion.licDia;
            console.log("LICENCIA INTERRUPCION felipe");
            console.log(aniosLicInt);
            console.log(mesesLicInt);
            console.log(diasLicInt);
 
            var tiLicInt=calcularSumaFechaToDiasMesesAnios(aniosLicInt,mesesLicInt,diasLicInt);
            console.log("tiempo licencia interrupcion WTF");
            console.log(tiLicInt.des);
            
            aniosLicInt=tiLicInt.anios;
            mesesLicInt=tiLicInt.meses;
            diasLicInt=tiLicInt.dias;
            console.log("LICENCIA INTERRUPCION");
            console.log(aniosLicInt);
            console.log(mesesLicInt);
            console.log(diasLicInt);
            $scope.datosProyeccion.tiempoLicInt =tiLicInt.des;
            console.log($scope.datosProyeccion.tiempoLicInt);
            
            var tiemUnsaAnio=$scope.datosProyeccion.regLabAnio-(aniosLicInt);
            console.log("AÑO");
            console.log($scope.datosProyeccion.regLabAnio);
            console.log($scope.datosProyeccion.intAnio);
            console.log($scope.datosProyeccion.licAnio);
            var tiemUnsaMes=$scope.datosProyeccion.regLabMes-(mesesLicInt);
            console.log("MES");
            console.log($scope.datosProyeccion.regLabMes);
            console.log($scope.datosProyeccion.intMes);
            console.log($scope.datosProyeccion.licMes);
            var tiemUnsaDia=$scope.datosProyeccion.regLabDia-(diasLicInt);
            console.log("DIA");
            console.log($scope.datosProyeccion.regLabDia);
            console.log($scope.datosProyeccion.intDia);
            console.log($scope.datosProyeccion.licDia);
            
            console.log("CALCULO INICIAL");
            console.log($scope.datosProyeccion.regLabAnio-(aniosLicInt));
            console.log($scope.datosProyeccion.regLabMes-(mesesLicInt));
            console.log($scope.datosProyeccion.regLabDia-(diasLicInt));
            
            var tiUnsa=calcularSumaFechaToDiasMesesAnios(tiemUnsaAnio,tiemUnsaMes,tiemUnsaDia);
            var tiTotUnsa=calcularSumaFechaToDiasMesesAnios(tiemUnsaAnio,tiemUnsaMes,tiemUnsaDia);
            $scope.datosProyeccion.tiempoUNSADes = tiUnsa.des;
            $scope.datosProyeccion.tiempoUNSAAnio = tiUnsa.anios;
            $scope.datosProyeccion.tiempoUNSAMes = tiUnsa.meses;
            $scope.datosProyeccion.tiempoUNSADia = tiUnsa.dias;
            
            $scope.datosProyeccion.tiempoTOTALDes = tiTotUnsa.des;
            $scope.datosProyeccion.tiempoTOTALAnio = tiTotUnsa.anios;
            $scope.datosProyeccion.tiempoTOTALMes = tiTotUnsa.meses;
            $scope.datosProyeccion.tiempoTOTALDia = tiTotUnsa.dias;
            console.log("CALCULO TIEMPO UNSA");
            console.log(tiUnsa.anios);
            console.log(tiUnsa.meses);
            console.log(tiUnsa.dias);
            console.log($scope.datosProyeccion.tiempoUNSADes);
            
            console.log("EJEMPLO");
            console.log(calcularSumaFechaToDiasMesesAnios(4,5,-8).des);
            
            //25 años
            var totalEstado = (tiTotUnsa.anios*360)+(tiTotUnsa.meses*12)+tiTotUnsa.dias;
            var dias25AniosTotal = 25 * 360; //NUmero de dias en 25 años
            var calculo25Anios = 0;
            if (totalEstado <= dias25AniosTotal) {
                var calculo25Anios = dias25AniosTotal - totalEstado;// Número de días a agregar
                var fecha1 = new Date();
                fecha1.setDate(fecha1.getDate() + calculo25Anios);
                $scope.datosProyeccion.tiempo25Anios = fecha1.toLocaleDateString();
                $scope.datosProyeccion.tiempo25AniosDes = $scope.datosProyeccion.tiempo25Anios;
            } else {
                $scope.datosProyeccion.tiempo25AniosDes = "Cumplido";
            }
            //30 años
            var dias30AniosTotal = 30 * 360; //Numero de dias 30 años
            var calculo30Anios = 0;
            if (totalEstado <= dias30AniosTotal) {
                var calculo30Anios = dias30AniosTotal - totalEstado;// Número de días a agregar
                var fecha2 = new Date();
                fecha2.setDate(fecha2.getDate() + calculo30Anios);
                $scope.datosProyeccion.tiempo30Anios = fecha2.toLocaleDateString();
                $scope.datosProyeccion.tiempo30AniosDes = $scope.datosProyeccion.tiempo30Anios;
            } else {
                $scope.datosProyeccion.tiempo30AniosDes = "Cumplido";
            }

        }

        //var salida = formatearFechas();
        //console.log(salida);
        function formatearFechas() {
            var fecIngDis = $scope.datosProyeccion.fechaIngreso.split("/");
            var fecIngFor = new Date(parseInt(fecIngDis[2]), parseInt(fecIngDis[1]) - 1, parseInt(fecIngDis[0]));
            var fecSalDis = [];
            var fecSalFor = 0;

            if (dataInput.tipFecSal === 1) {
                fecSalFor = $scope.datosProyeccion.fechaSalida;
            } else {
                fecSalDis = $scope.datosProyeccion.fechaSalida.split("/");
                fecSalFor = new Date(parseInt(fecSalDis[2]), parseInt(fecSalDis[1]) - 1, parseInt(fecSalDis[0]));
            }

            var acumulacion = getDiffDates(fecIngFor, fecSalFor);
            return acumulacion;

        }
        ;
        function getDiffDates(fechaInicio, fechaFin) {
            fechaFin = fechaFin !== "" ? fechaFin : new Date();
            // Fecha inicio
            var diaInicio = fechaInicio.getDate();
            var mesInicio = fechaInicio.getMonth() + 1; // 0 Enero, 11 Diciembre
            var anioInicio = fechaInicio.getFullYear();

            // Fecha fin
            var diaFin = fechaFin.getDate();
            var mesFin = fechaFin.getMonth() + 1; // 0 Enero, 11 Diciembre
            var anioFin = fechaFin.getFullYear();

            var anios = 0;
            var mesesPorAnio = 0;
            var diasPorMes = 0;
            var diasTipoMes = 0;
            var tiempoExt = {};
            //
            // Calculo de días del mes
            //
            if (mesInicio == 2) {
                // Febrero
                if ((anioFin % 4 == 0) && ((anioFin % 100 != 0) || (anioFin % 400 == 0))) {
                    // Bisiesto
                    diasTipoMes = 29;
                } else {
                    // No bisiesto
                    diasTipoMes = 28;
                }
            } else if (mesInicio <= 7) {
                // De Enero a Julio los meses pares tienen 30 y los impares 31
                if (mesInicio % 2 == 0) {
                    diasTipoMes = 30;
                } else {
                    diasTipoMes = 31;
                }
            } else if (mesInicio > 7) {
                // De Julio a Diciembre los meses pares tienen 31 y los impares 30
                if (mesInicio % 2 == 0) {
                    diasTipoMes = 31;
                } else {
                    diasTipoMes = 30;
                }
            }

            //
            // Calculo de diferencia de año, mes y dia
            //
            if ((anioInicio > anioFin) || (anioInicio == anioFin && mesInicio > mesFin)
                    || (anioInicio == anioFin && mesInicio == mesFin && diaInicio > diaFin)) {
                // La fecha de inicio es posterior a la fecha fin
                // System.out.println("La fecha de inicio ha de ser anterior a la fecha fin");
                alert("La fecha de inicio ha de ser anterior a la fecha fin");
            } else {
                //tiempoExt.setEstado(true);
                if (mesInicio <= mesFin) {
                    anios = anioFin - anioInicio;
                    if (diaInicio <= diaFin) {
                        mesesPorAnio = mesFin - mesInicio;
                        diasPorMes = diaFin - diaInicio;
                    } else {
                        if (mesFin == mesInicio) {
                            anios = anios - 1;
                        }
                        mesesPorAnio = (mesFin - mesInicio - 1 + 12) % 12;
                        diasPorMes = diasTipoMes - (diaInicio - diaFin);
                    }
                } else {
                    anios = anioFin - anioInicio - 1;
                    if (diaInicio > diaFin) {
                        mesesPorAnio = mesFin - mesInicio - 1 + 12;
                        diasPorMes = diasTipoMes - (diaInicio - diaFin);
                    } else {
                        mesesPorAnio = mesFin - mesInicio + 12;
                        diasPorMes = diaFin - diaInicio;
                    }
                }
            }

            /*--------------------------------------------------------------------*/
            /**- Totales                                                         -*/
            /*--------------------------------------------------------------------*/

            // Total Años
            tiempoExt.anios = Math.floor(anios);

            // Total Meses
            tiempoExt.meses = Math.floor(mesesPorAnio);


            // Dias del mes
            tiempoExt.dias = Math.floor(diasPorMes);

            //total dias
            var millsecsPerDay = 86400000; // Milisegundos al día
            tiempoExt.totalDias = Math.floor((fechaFin.getTime() - fechaInicio.getTime()) / millsecsPerDay);
            return tiempoExt;
        }
        ;

        //TIEMPO TOTAL tiempoTOTALDes
        $scope.calcularDiasDatosExtra = function () {
            
            var aniosToDias = $scope.aniosOut.an1+ $scope.aniosOut.an2+$scope.datosProyeccion.tiempoUNSAAnio;
            var mesesToDias = $scope.aniosOut.me1+ $scope.aniosOut.me2+$scope.datosProyeccion.tiempoUNSAMes;
            var diasToDias = $scope.aniosOut.di1+ $scope.aniosOut.di2+$scope.datosProyeccion.tiempoUNSADia;
            

            $scope.datosProyeccion.tiempoTOTALAnio = aniosToDias;
            $scope.datosProyeccion.tiempoTOTALMes = mesesToDias;
            $scope.datosProyeccion.tiempoTOTALDia = diasToDias;

            var tiTotUnsa=calcularSumaFechaToDiasMesesAnios(aniosToDias,mesesToDias,diasToDias);
            $scope.datosProyeccion.tiempoTOTALDes = tiTotUnsa.des;
            aniosServicio25();
            aniosServicio30();
        };
        

        function aniosServicio25() {
            var totalEstado = ($scope.datosProyeccion.tiempoTOTALAnio*360)+($scope.datosProyeccion.tiempoTOTALMes*12)+$scope.datosProyeccion.tiempoTOTALDia;
            var dias25AniosTotal = 25 * 360; //NUmero de dias en 25años
            var calculo25Anios = 0;
            if (totalEstado <= dias25AniosTotal) {
                var calculo25Anios = dias25AniosTotal - totalEstado;// Número de días a agregar
                var fecha = new Date();
                fecha.setDate(fecha.getDate() + calculo25Anios);
                $scope.datosProyeccion.tiempo25Anios = fecha.toLocaleDateString();
                $scope.datosProyeccion.tiempo25AniosDes = $scope.datosProyeccion.tiempo25Anios;
            } else {
                $scope.datosProyeccion.tiempo25AniosDes = "Cumplido";
            }

        }

        function aniosServicio30() {
            var totalEstado = ($scope.datosProyeccion.tiempoTOTALAnio*360)+($scope.datosProyeccion.tiempoTOTALMes*12)+$scope.datosProyeccion.tiempoTOTALDia;
            var dias30AniosTotal = 30 * 360;
            var calculo30Anios = 0;
            if (totalEstado <= dias30AniosTotal) {
                var calculo30Anios = dias30AniosTotal - totalEstado;// Número de días a agregar
                var fecha = new Date();
                fecha.setDate(fecha.getDate() + calculo30Anios);
                $scope.datosProyeccion.tiempo30Anios = fecha.toLocaleDateString();
                $scope.datosProyeccion.tiempo30AniosDes = $scope.datosProyeccion.tiempo30Anios;
            } else {
                $scope.datosProyeccion.tiempo30AniosDes = "Cumplido";
            }

        }

        function calcularFechaToDias(d) {
            var calculoD ={anios:0,meses:0, dias:0, des:""};
            var diasTot = d;
            var anio = 0;
            var mes = 0;
            var dia = 0;
            if (d >= 0) {
                anio = Math.floor(Math.floor(diasTot/30)/12);
                mes = mod(Math.floor(diasTot/30),12);
                dia = mod(diasTot,30);

                calculoD.dias = dia;
                calculoD.meses = mes;
                calculoD.anios = anio;
                calculoD.des = anio + " años " + mes + " meses " + dia + " días";
                return calculoD;
            } else {
                calculoD.dias = dia;
                calculoD.meses = mes;
                calculoD.anios = anio;
                calculoD.des = anio + " años " + mes + " meses " + dia + " días";
                return calculoD;
            }
        };
        
        function calcularSumaFechaToDiasMesesAnios(a,m,d){
            var calculoAMD ={anios:0,meses:0, dias:0, des:""};
            var anio = 0;
            var mes = 0;
            var dia = 0;
            console.log("calculoAMD");
            anio = parseInt(a+Math.floor((mod(m,12)+Math.floor(d/30))/12)+Math.floor(m/12));
            console.log(a);
            
            console.log(Math.floor(mod(m,12)));
            console.log(Math.floor(d/12));
            
            console.log(Math.floor((mod(m,12)+Math.floor(d/30))/12));
            
            console.log(Math.floor(m/12));
            console.log("fin");
            mes = parseInt(mod((mod(m,12)+Math.floor(d/30)),12));
            console.log(mes);
            dia = parseInt(mod(d,30));
            console.log(dia);
            calculoAMD.dias = dia;
            calculoAMD.meses = mes;
            calculoAMD.anios = anio;
            calculoAMD.des = anio + " años " + mes + " meses " + dia + " días";
            return calculoAMD;
        }

        function mod(m,n){
            if(m<0)
                return ((m%n)+n)%n;
            if(m>=0)
                return m%n;
        };
        
        function getDiffDatesv2(fecIni, fecFin){
            var salida ={anios:0,meses:0, dias:0,totalDias:0};
            if(fecIni !== "" && fecFin !== ""){
                var diaIni = fecIni.getDate();
                var mesIni = fecIni.getMonth()+1;
                var anioIni = fecIni.getFullYear();

                var diaTer = fecFin.getDate();
                var mesTer = fecFin.getMonth()+1;
                var anioTer = fecFin.getFullYear();

                var numDias360=0;

                if(anioTer===anioIni){

                    if(mesTer===mesIni){

                        numDias360=diaTer-diaIni;

                    }else{
                        if(diaTer===diaIni){
                            numDias360=(mesTer-mesIni)*30;
                        }
                        if(diaTer>diaIni){
                            numDias360=(mesTer-mesIni)*30+(diaTer-diaIni);
                        }
                        if(diaTer<diaIni){
                            numDias360=(mesTer-mesIni)*30-(diaIni-diaTer);
                        }
                    }

                }

                if(anioTer>anioIni){
                    if(mesTer===mesIni){
                        if(diaTer===diaIni){
                            numDias360=(anioTer-anioIni)*360;
                        }
                        if(diaTer>diaIni){
                            numDias360=(anioTer-anioIni)*360+(diaTer-diaIni);
                        }
                    }

                    if(mesTer>mesIni){
                        numDias360=(anioTer-anioIni)*360;
                        if(diaTer===diaIni){
                            numDias360=numDias360+(mesTer-mesIni)*30;
                        }
                        if(diaTer>diaIni){
                            numDias360=numDias360+(mesTer-mesIni)*30+(diaTer-diaIni);
                        }
                        if(diaTer<diaIni){
                            numDias360=numDias360+(mesTer-mesIni)*30-(diaIni-diaTer);
                        }
                    }

                    if(mesTer<mesIni){
                        numDias360=(anioTer-anioIni)*360;
                        if(diaTer===diaIni){
                            numDias360=numDias360-(mesIni-mesTer)*30;
                        }
                        if(diaTer>diaIni){
                            numDias360=numDias360-(mesIni-mesTer)*30+(diaTer-diaIni);
                        }
                        if(diaTer<diaIni){
                            numDias360=numDias360-(mesIni-mesTer)*30-(diaIni-diaTer);
                        }
                    }

                }
                
                var diaTran=numDias360%30;
                var mesTran=Math.trunc(numDias360/30)%12;
                var anioTran=Math.trunc(Math.trunc(numDias360/30)/12);

            }
            salida.dias = diaTran;
            salida.meses = mesTran;
            salida.anios = anioTran;
            salida.totalDias = numDias360;
            return salida;
        };

    }]);
seApp.controller('modalSeleccionadosPersonalizadoCtrl', ['$scope', '$rootScope', '$element', 'title', 'trabajadorInfo', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, trabajadorInfo, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente
        //console.log(trabajadorInfo);
        //$scope.tipoInforme = 1;
        $scope.title = title;
        $scope.seleccionados = [false, false, false, false, false, false];
        $scope.formatos = ["PDF", "WORD"];
        $scope.selecFormat = $scope.formatos[0];//Establecer la selección
        var checked = false;
        $scope.marcarTodos = function () {
            if (checked === false) {
                $('.dinamico').prop('checked', true);
                $scope.seleccionados = [true, true, true, true, true, true];
                checked = true;
            } else {
                $('.dinamico').prop('checked', false);
                $scope.seleccionados = [false, false, false, false, false, false];
                console.log($scope.seleccionados);
                checked = false;
            }
        };

        $scope.reportePersonalizado = {
            numInf: "",
            numRef: "",
            lugDes: "",
            lugPro: "SUBDIRECCIÓN DE RECURSOS HUMANOS - OFICINA DE ESCALAFON",
            asu: ""
        };
        $scope.genererarReportePersonalizado = function () {
            //console.log($scope.tipoInforme);

            if ($scope.reportePersonalizado.numInf.length == '' ||
                    $scope.reportePersonalizado.numRef.length == '' ||
                    $scope.reportePersonalizado.lugDes.length == '' ||
                    $scope.reportePersonalizado.asu.length == '')
            {
                modal.mensaje("ALERTA", "Debe completar el encabezado de forma correcta.");
                return;
            }
            console.log($scope.selecFormat);
            var request = crud.crearRequest('reportes', 1, 'reportePersonalizado');
            request.setData({
                traId: trabajadorInfo.traId,
                perDni: trabajadorInfo.perDni,
                tipoInforme: $scope.tipoInforme,
                seleccionados: $scope.seleccionados,
                //enviara al backend
                reporteData: $scope.reportePersonalizado
            });
            crud.insertar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    console.log($scope.seleccionados);
                    verDocumentoPestaña(response.data.file);
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (errResponse) {
                modal.mensaje('ERROR', 'El servidor no responde');
            });
        };
        $scope.descargarReportePersonalizado = function () {
            //console.log($scope.tipoInforme);
            if ($scope.reportePersonalizado.numInf.length == '' ||
                    $scope.reportePersonalizado.numRef.length == '' ||
                    $scope.reportePersonalizado.lugDes.length == '' ||
                    $scope.reportePersonalizado.asu.length == '')
            {
                modal.mensaje("ALERTA", "Debe completar el encabezado de forma correcta.");
                return;
            }
            console.log($scope.selecFormat);
            var request = crud.crearRequest('reportes', 1, 'reportePersonalizado');
            request.setData({
                traId: trabajadorInfo.traId,
                perDni: trabajadorInfo.perDni,
                tipoInforme: $scope.tipoInforme,
                seleccionados: $scope.seleccionados,
                //enviara al backend
                reporteData: $scope.reportePersonalizado
            });
            crud.insertar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    console.log($scope.seleccionados);
                    if ($scope.selecFormat.toString() === "PDF") {
                        descargarArchivo(response.data.file, "ReportePersonalizado_" + request.data.perDni, "pdf");
                    }
                    if ($scope.selecFormat.toString() === "WORD")
                    {
                        descargarArchivo(response.data.reporteDoc, "ReportePersonalizado_" + request.data.perDni, "doc");
                    }
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (errResponse) {
                modal.mensaje('ERROR', 'El servidor no responde');
            });
        };
    }]);
seApp.controller('editarEstadoRotacionCtrl', ['$rootScope', '$scope', '$element', 'title', 'trabajadorInfo', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'tipDocs', function ($rootScope, $scope, $element, title, trabajadorInfo, fichaEscalafonariaId, close, crud, modal, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        $scope.trabajadorSel = {
            traId: trabajadorInfo.traId,

            orgId: trabajadorInfo.orgId,
            orgNom: trabajadorInfo.orgNom,

            rolId: trabajadorInfo.rolId,
            rolNom: trabajadorInfo.rolNom,

            plaId: trabajadorInfo.plaId,
            plaNom: trabajadorInfo.plaNom,

            catId: trabajadorInfo.catId,
            catNom: trabajadorInfo.catNom,

            orgaId: trabajadorInfo.orgaId,
            orgaNom: trabajadorInfo.orgaNom,

            facId: trabajadorInfo.facId,
            facNom: trabajadorInfo.facNom,

            depId: trabajadorInfo.depId,
            depNom: trabajadorInfo.depNom,

            zonId: trabajadorInfo.zonId,
            zonNom: trabajadorInfo.zonNom,

            carId: trabajadorInfo.carId,
            carNom: trabajadorInfo.carNom,

            fecIng: trabajadorInfo.fecIngEst ? (new Date(trabajadorInfo.fecIng.split("-")[2], trabajadorInfo.fecIng.split("-")[1] - 1, trabajadorInfo.fecIng.split("-")[0])).toISOString() : "",

            fecIngEst: trabajadorInfo.fecIngEst,

            fecSal: trabajadorInfo.fecSal,

            sal: trabajadorInfo.sal,

            tieServ: trabajadorInfo.tieServ,
            aniosOut: trabajadorInfo.aniosOut,

            estLab: '2',
            estLabId: '2',
            estLabNom: "Rotación"
        };

        $scope.planillas = [];
        listarPlanillas();
        function listarPlanillas() {
            var request = crud.crearRequest('planillaConfiguracion', 1, 'listarPlanillas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.planillas = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.categorias = [];
        listarCategorias();
        function listarCategorias() {
            var request = crud.crearRequest('categoriaConfiguracion', 1, 'listarCategorias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.categorias = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.zonas = [];
        listarZonas();
        function listarZonas() {
            var request = crud.crearRequest('zonaConfiguracion', 1, 'listarZonas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.zonas = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.cargos = [];
        listarCargos();
        function listarCargos() {
            var request = crud.crearRequest('cargoConfiguracion', 1, 'listarCargos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.cargos = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.organismos = [];
        listarOrganismos();
        function listarOrganismos() {
            var request = crud.crearRequest('organismoConfiguracion', 1, 'listarOrganismos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.organismos = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        var facultades = [];
        $scope.facultadesSel = [];
        listarFacultades();
        function listarFacultades() {
            var request = crud.crearRequest('facultadConfiguracion', 1, 'listarFacultades');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    facultades = response.data;
                    $scope.listarFacultadesSel();
                }
            }, function (data) {
                console.info(data);
            });
        }

        var dependencias = [];
        $scope.dependenciasSel = [];
        listarDependencias();
        function listarDependencias() {
            var request = crud.crearRequest('dependenciaConfiguracion', 1, 'listarDependencias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    dependencias = response.data;
                    $scope.listarDependenciasSel();
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.listarFacultadesSel = function () {
            $scope.facultadesSel = [];
            facultades.forEach(function (item) {
                if (item.orgaId === $scope.trabajadorSel.orgaId)
                    $scope.facultadesSel.push(item);
            });
            $scope.dependenciasSel = [];
        };

        $scope.listarDependenciasSel = function () {
            $scope.dependenciasSel = [];
            dependencias.forEach(function (item) {
                if (item.facId === $scope.trabajadorSel.facId)
                    $scope.dependenciasSel.push(item);
            });
        };

        var ft = new Date();
        ft = ft.toISOString();
        $scope.nuevoAscensoRot = {ficEscId: fichaEscalafonariaId, numDoc: "", fecDoc: "", tipDoc: "", fecEfe: ft, esc: "", tip: "2", catId: "", plaId: "", orgId: "", depId: ""};
        $scope.actualizarDatCenLab = function () {
            //save the old data on the ascenso table
            var request = crud.crearRequest('ascenso', 1, 'agregarAscenso');
            $scope.nuevoAscensoRot.catId = parseInt($scope.trabajadorSel.catId);
            $scope.nuevoAscensoRot.orgId = parseInt($scope.trabajadorSel.orgId);
            $scope.nuevoAscensoRot.plaId = parseInt($scope.trabajadorSel.plaId);
            $scope.nuevoAscensoRot.depId = parseInt($scope.trabajadorSel.depId);
            request.setData($scope.nuevoAscensoRot);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingModAscensos.dataset, response.data);
                    $rootScope.tablaModAscensos.reload();
                }
            }, function (data) {
                console.info(data);
            });
            //new data
            var request = crud.crearRequest('datos_trabajador', 1, 'actualizarDatCenLabTra');
            request.setData($scope.trabajadorSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    /*response.data.i = trabajadorInfo.i;
                     $rootScope.settingDatCenLab.dataset[trabajadorInfo.i] = response.data;*/
                    response.data.fecIng = new Date(response.data.fecIng);
                    $rootScope.listarDatCenLab();
                    $rootScope.tablaDatCenLab.reload();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else {
                    modal.mensaje("ERROR", response.responseMsg);
                }
            }, function (error) {
                console.info(error);
            });

        };
    }]);
seApp.controller('editarEstadoRetiroCtrl', ['$rootScope', '$scope', '$element', 'data', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, data, close, crud, modal) {
        var dataInput = data;
        var trabajadorInfo = dataInput.trabajadorInfo;
        $scope.title = dataInput.title;
        $scope.tiposDocumentos = dataInput.tiposDocumentos;
        console.log($scope.tiposDocumentos);
        $scope.jorLabs = dataInput.jornadasLaborales;
        var ft = new Date();
        ft = ft.toISOString();
        $scope.nuevoDesplazamiento = {ficEscId: dataInput.fichaEscalafonariaId, tip: "1", numDoc: "", fecDoc: "", tipDoc: 0, insEdu: (dataInput.trabajadorInfo.orgNom != null ? dataInput.trabajadorInfo.orgNom : "UNSA"), car: dataInput.trabajadorInfo.carNom, jorLab: "", fecIni: (dataInput.trabajadorInfo.fecIng != null ? dataInput.trabajadorInfo.fecIng : "2018-01-01T00:00:00.000Z"), fecTer: ft, motRet: ""};

        // Method to update every last displacement
        $scope.fichaEscalafonariaSel = {
            ficEscId: dataInput.fichaEscalafonariaId,
        };
        $scope.actualizarDesplXRetiro = function () {
            //Set up the list of displacements and get the last of them
            var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            var lastDisplacement = "";
            crud.listar("/sistema_escalafon", request, function (response) {
                var counter = 1;
                response.data.forEach(function (item) {
                    // look for the item that has the types 1 or 2 becuase another type indicates an interruption
                    (item.tip != "3") && (item.numDocTer === undefined || item.numDocTer === null || item.numDocTer === "") ? lastDisplacement = item : counter++;
                });
                lastDisplacement = {
                    i: lastDisplacement.i,
                    desId: lastDisplacement.desId,
                    tip: lastDisplacement.tip,
                    numDoc: lastDisplacement.numDoc,
                    fecDoc: lastDisplacement.fecDoc,
                    tipDoc: lastDisplacement.tipDoc,
                    insEdu: lastDisplacement.insEdu,
                    car: lastDisplacement.car,
                    jorLab: lastDisplacement.jorLab,
                    fecIni: new Date(lastDisplacement.fecIni),
                    fecTer: $scope.nuevoDesplazamiento.fecTer,
                    fecDocTer: $scope.nuevoDesplazamiento.fecDoc,
                    numDocTer: $scope.nuevoDesplazamiento.numDoc,
                    tipDocTer: $scope.nuevoDesplazamiento.tipDoc,
                    motRet: $scope.nuevoDesplazamiento.motRet
                };
                //console.log("upd retirement: "+JSON.stringify(lastDisplacement));
                // Once you set up the last displacement, update it
                var request = crud.crearRequest('desplazamiento', 1, 'actualizarDesplazamiento');
                request.setData(lastDisplacement);
                crud.actualizar("/sistema_escalafon", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                }, function (data) {
                    console.info(data);
                });
            }, function (error) {
                console.info(error);
            });
        };
        $scope.trabajadorSel = {
            traId: trabajadorInfo.traId,

            orgId: trabajadorInfo.orgId,
            orgNom: trabajadorInfo.orgNom,

            carId: trabajadorInfo.carId,
            carNom: trabajadorInfo.carNom,

            fecIng: trabajadorInfo.fecIngEst ? trabajadorInfo.fecIng : "",

            fecIngEst: trabajadorInfo.fecIngEst,
            fecSal: trabajadorInfo.fecSal,

            sal: trabajadorInfo.sal,

            tieServ: trabajadorInfo.tieServ,
            aniosOut: trabajadorInfo.aniosOut,
            estLab: '4',
            estLabId: '4',
            estLabNom: "Retiro"
        };

        $scope.actualizarDatCenLab = function () {
            var request = crud.crearRequest('datos_trabajador', 1, 'actualizarDatCenLabTra');
            request.setData($scope.trabajadorSel);
            //console.log("trasel: "+JSON.stringify($scope.trabajadorSel));
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    /*response.data.i = trabajadorInfo.i;
                     $rootScope.settingDatCenLab.dataset[trabajadorInfo.i] = response.data;*/
                    response.data.fecIng = new Date(response.data.fecIng);
                    $rootScope.listarDatCenLab();
                    $rootScope.tablaDatCenLab.reload();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else {
                    modal.mensaje("ERROR", response.responseMsg);
                }
            }, function (error) {
                console.info(error);
            });

        };


    }]);
seApp.controller('agregarReingresoCtrl', ['$rootScope', '$scope', '$element', 'title', 'trabajadorInfo', 'close', 'crud', 'modal', 'fichaEscalafonaria', 'tipDocs', 'jorLabs', function ($rootScope, $scope, $element, title, trabajadorInfo, close, crud, modal, fichaEscalafonaria, tipDocs, jorLabs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        $scope.jorLabs = jorLabs;
        var ft = new Date();
        ft = ft.toISOString();
        $scope.nuevoDesplazamiento = {ficEscId: fichaEscalafonaria.ficEscId, tip: "2", numDoc: "", fecDoc: "", tipDoc: "", insEdu: trabajadorInfo.orgNom, car: trabajadorInfo.rolNom, jorLab: "", fecIni: (trabajadorInfo.fecIng != null ? trabajadorInfo.fecIng : "2018-01-01T00:00:00.000Z"), fecTer: "", fecDocTer: "", numDocTer: "", motRet: ""};

        $scope.agregarDesplXReingreso = function () {
            var request = crud.crearRequest('desplazamiento', 1, 'agregarDesplazamiento');
            request.setData($scope.nuevoDesplazamiento);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.trabajadorSel = {
            traId: trabajadorInfo.traId,

            orgId: trabajadorInfo.orgId,
            orgNom: trabajadorInfo.orgNom,

            carId: trabajadorInfo.carId,
            carNom: trabajadorInfo.carNom,

            fecIng: trabajadorInfo.fecIngEst ? trabajadorInfo.fecIng : "",

            fecIngEst: trabajadorInfo.fecIngEst,
            fecSal: trabajadorInfo.fecSal,

            sal: trabajadorInfo.sal,

            tieServ: trabajadorInfo.tieServ,
            aniosOut: trabajadorInfo.aniosOut,

            estLab: '9',
            estLabId: '9',
            estLabNom: "Reingreso"
        };
        $scope.actualizarDatCenLabReingreso = function () {
            var request = crud.crearRequest('datos_trabajador', 1, 'actualizarDatCenLabTra');
            request.setData($scope.trabajadorSel);
            //console.log("update lab cen: "+JSON.stringify($scope.trabajadorSel));
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    /*response.data.i = trabajadorInfo.i;
                     $rootScope.settingDatCenLab.dataset[trabajadorInfo.i] = response.data;*/
                    response.data.fecIng = new Date(response.data.fecIng);
                    $rootScope.listarDatCenLab();
                    $rootScope.tablaDatCenLab.reload();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else {
                    modal.mensaje("ERROR", response.responseMsg);
                }
            }, function (error) {
                console.info(error);
            });

        };
    }]);
seApp.controller('modalSeleccionadosCtrl', ['$scope', '$rootScope', '$element', 'title', 'ficEscId', 'traId', 'perDni', 'close', 'crud', 'modal', 'selecc', 'ModalService', function ($scope, $rootScope, $element, title, ficEscId, traId, perDni, close, crud, modal, selecc, ModalService) {
        //variable que servira para registrar un nuevo pariente

        $scope.title = title;
        $scope.selecc = selecc;
        $scope.seleccionados = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
        console.log($scope.selecc);
        $scope.formatos = ["PDF"];
        $scope.selecFormat = $scope.formatos[0];//Establecer la selección
        var checked = false;
        $scope.marcarTodos = function () {
            if (checked === false) {
                $('.dinamico').prop('checked', true);
                for (i = 0; i < $scope.selecc.length; i++)
                {
                    if ($scope.selecc[i] === true)
                        $scope.seleccionados[i] = true;
                    else
                        $scope.seleccionados[i] = false;
                }

                //$scope.seleccionados = [true, true, true, true, true, true, true, true, true, true, true, true, true, true, true];
                //console.log($scope.seleccionados);
                checked = true;
            } else {
                $('.dinamico').prop('checked', false);
                for (i = 0; i < $scope.selecc.length; i++)
                {
                    if ($scope.selecc[i] === true)
                        $scope.seleccionados[i] = false;
                    else
                        $scope.seleccionados[i] = false;

                }
                //$scope.seleccionados = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
                //console.log($scope.seleccionados);
                checked = false;
            }
        };
        $scope.verFichaEscalafonaria = function () {
            var request = crud.crearRequest('reportes', 1, 'reporteFichaEscalafonaria');
            request.setData({traId: traId, ficEscId: ficEscId, perDni: perDni, seleccionados: $scope.seleccionados});
            crud.insertar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    verDocumentoPestaña(response.data.file);
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (errResponse) {
                modal.mensaje('ERROR', 'El servidor no responde');
            });
        };
        $scope.descargarFichaEscalafonaria = function () {
            var request = crud.crearRequest('reportes', 1, 'reporteFichaEscalafonaria');
            request.setData({traId: traId, ficEscId: ficEscId, perDni: perDni, seleccionados: $scope.seleccionados});
            crud.insertar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    if ($scope.selecFormat.toString() === "PDF") {
                        descargarArchivo(response.data.file, "FichaEscalafonaria_" + perDni, "pdf");
                    }
                    if ($scope.selecFormat.toString() === "EXCEL")
                    {
                        descargarArchivo(response.data.reporteXls, "FichaEscalafonaria_" + perDni, "xls");
                    }
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (errResponse) {
                modal.mensaje('ERROR', 'El servidor no responde');
            });
        };

    }]);
/*ESTO DEBERIA IR EN CONTROLADOR DE ACTUALIZARDATOS*/
seApp.controller('actualizarDatosTrabajadorCtrl', ['$routeParams', '$scope', '$rootScope', '$http', 'NgTableParams', 'crud', 'modal', 'ModalService', function ($routeParams, $scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {

        //var datos = JSON.parse(atob($routeParams.data));
        //$scope.dataRidea = datos;
        //Variables y funciones para controlar la vista
        $scope.optionsDatosPersonales = [true, false, false, false, false];
        $scope.optionsDatosAcademicos = [true, false, false, false, false, false];

        var url;
        var serR = "/rideaoffice";
        var serP = "/getPdf?"
        $scope.uri;
        $scope.uriv;

        $scope.mostrarButtonOpen = true;
        $scope.mostrarButtonClose = false;

        $scope.mostrarAgrupamiento = false;
        $scope.ocultarAgrupamiento = true;

        $scope.showAgrupamiento = function () {
            $scope.mostrarAgrupamiento = true;
            $scope.ocultarAgrupamiento = false;

            $scope.mostrarButtonOpen = false;
            $scope.mostrarButtonClose = true;
        };

        $scope.hideAgrupamiento = function () {
            $scope.mostrarAgrupamiento = false;
            $scope.ocultarAgrupamiento = true;

            $scope.mostrarButtonOpen = true;
            $scope.mostrarButtonClose = false;
        };

        $scope.prepararVerAgrups = function (documento) {
            $scope.doc = documento;
            //ver servicio
            var request = crud.crearRequest('servicioRidea', 1, 'verServicioRIDEA');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                if (data.data) {
                    url = data.data.servicio;
                    $scope.uri = url + serR + serP;
                    $scope.uriv = url + serR;
                } else {
                    modal.mensaje("ERROR", "No se pudo leer el servicio");
                }
            }, function (data) {
                console.info(data);
            });
            //console.log("preparing doc:" +doc.agrupamientos);
            if (!$scope.doc.estagrupamiento)
                modal.mensaje("ERROR", "el documento no tiene agruapmientos");
        };



        $scope.updateOptions = function (numOption, optionsSet) {
            for (var i = 0; i < optionsSet.length; i += 1) {
                optionsSet[i] = false;
            }
            optionsSet[numOption] = true;
        };
        
        $scope.changeDiscapacidadSi = function () {
            $scope.personaSel.perDis = true;
        };
        $scope.changeDiscapacidadNo = function () {
            $scope.personaSel.perDis = false;
        };

        $scope.status_1 = {close: true, statusGD: false, statusLD: false};
        
        $scope.statusDA = false;
        $scope.changeDASi = function () {
            $scope.statusDA = false;
        };
        $scope.changeDANo = function () {
            $scope.statusDA = true;
        };

        function changeAseguradoState(codAutEss) {
            if (codAutEss !== "")
                $scope.asegurado = "SI";
            else
                $scope.asegurado = "NO";
        }
        ;

        function changeSistemaPensionesState(sisPen) {
            if (sisPen === "ONP")
                $scope.optionsSP = "NO";
            else
                $scope.optionsSP = "SI";
        }
        ;

        function changeDishabilityState(perDis) {
            if (perDis === true)
                $scope.discapacidad = "SI";
            else
                $scope.discapacidad = "NO";
        }
        ;

        $scope.parsearBooleano = function (i) {
            var o = "";
            if (i) {
                o = "Si";
            } else {
                o = "No";
            }
            return o;
        };

        $scope.parsearBooleano2 = function (i) {
            var o = "";
            if (i) {
                o = "Habilitado";
            } else {
                o = "No Habilitado";
            }
            return o;
        };

        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.estadoCivil = [{id: "S", title: "Soltero(a)"}, {id: "C", title: "Casado(a)"}];
        $scope.tipoTrabajador = [{id: "N", title: "Nombrado"}, {id: "V", title: "Contratado"}];
        $scope.grupoOcupacional = [{id: "A", title: "Administrativo"}, {id: "D", title: "Docente"}];
        $scope.afps = ["20530", "25897"];

        $rootScope.paramsDatCenLab = {count: 10};
        $rootScope.settingDatCenLab = {counts: []};
        $rootScope.tablaDatCenLab = new NgTableParams($rootScope.paramsDatCenLab, $rootScope.settingDatCenLab);

        $rootScope.paramsModParientes = {count: 10};
        $rootScope.settingModParientes = {counts: []};
        $rootScope.tablaModParientes = new NgTableParams($rootScope.paramsModParientes, $rootScope.settingModParientes);

        $rootScope.paramsModForEdu = {count: 10};
        $rootScope.settingModForEdu = {counts: []};
        $rootScope.tablaModForEdu = new NgTableParams($rootScope.paramsModForEdu, $rootScope.settingModForEdu);

        $rootScope.paramsModEstCom = {count: 10};
        $rootScope.settingModEstCom = {counts: []};
        $rootScope.tablaModEstCom = new NgTableParams($rootScope.paramsModEstCom, $rootScope.settingModEstCom);

        $rootScope.paramsModExpPon = {count: 10};
        $rootScope.settingModExpPon = {counts: []};
        $rootScope.tablaModExpPon = new NgTableParams($rootScope.paramsModExpPon, $rootScope.settingModExpPon);

        $rootScope.paramsModPublicaciones = {count: 10};
        $rootScope.settingModPublicaciones = {counts: []};
        $rootScope.tablaModPublicaciones = new NgTableParams($rootScope.paramsModPublicaciones, $rootScope.settingModPublicaciones);

        $rootScope.paramsModDesplazamientos = {count: 10};
        $rootScope.settingModDesplazamientos = {counts: []};
        $rootScope.tablaModDesplazamientos = new NgTableParams($rootScope.paramsModDesplazamientos, $rootScope.settingModDesplazamientos);

        $rootScope.paramsModColegiaturas = {count: 10};
        $rootScope.settingModColegiaturas = {counts: []};
        $rootScope.tablaModColegiaturas = new NgTableParams($rootScope.paramsModModColegiaturas, $rootScope.settingModColegiaturas);

        $rootScope.paramsModAscensos = {count: 10};
        $rootScope.settingModAscensos = {counts: []};
        $rootScope.tablaModAscensos = new NgTableParams($rootScope.paramModsAscensos, $rootScope.settingModAscensos);

        $rootScope.paramsModCapacitaciones = {count: 10};
        $rootScope.settingModCapacitaciones = {counts: []};
        $rootScope.tablaModCapacitaciones = new NgTableParams($rootScope.paramsModCapacitaciones, $rootScope.settingModCapacitaciones);

        $rootScope.paramsModLicencias = {count: 10};
        $rootScope.settingModLicencias = {counts: []};
        $rootScope.tablaModLicencias = new NgTableParams($rootScope.paramModLicencias, $rootScope.settingModLicencias);

        $rootScope.paramsModVacaciones = {count: 10};
        $rootScope.settingModVacaciones = {counts: []};
        $rootScope.tablaModVacaciones = new NgTableParams($rootScope.paramModsVacaciones, $rootScope.settingModVacaciones);

        $rootScope.paramsModReconocimientos = {count: 10};
        $rootScope.settingModReconocimientos = {counts: []};
        $rootScope.tablaModReconocimientos = new NgTableParams($rootScope.paramsModReconocimientos, $rootScope.settingModReconocimientos);

        $rootScope.paramsModEstPos = {count: 10};
        $rootScope.settingModEstPos = {counts: []};
        $rootScope.tablaModEstPos = new NgTableParams($rootScope.paramsModEstPos, $rootScope.settingModEstPos);

        $rootScope.paramsModDemeritos = {count: 10};
        $rootScope.settingModDemeritos = {counts: []};
        $rootScope.tablaModDemeritos = new NgTableParams($rootScope.paramsModDemeritos, $rootScope.settingModDemeritos);

        var datosPersonales = JSON.parse(atob($routeParams.data));
        console.log(datosPersonales);
        // setting ridea data
        $scope.dataRidea = datosPersonales.Documentos;
        //"http://190.119.213.87:8091/rideaoffice/getPdf?"
        $scope.uriRidea = $scope.uri;
        $scope.perfil = datosPersonales.persona.foto;
        $scope.trabajadorData = datosPersonales.persona.nom + ' ' + datosPersonales.persona.apePat + ' ' + datosPersonales.persona.apeMat;
        $scope.trabajadorDNI = datosPersonales.persona.dni;
        $scope.trabajadorCargo = "";
        datosPersonales.sessiones.forEach(function (element) {
            $scope.trabajadorCargo += element.rol + " - ";
        });
        $scope.trabajadorCargo = $scope.trabajadorCargo !== "" ? $scope.trabajadorCargo.substr(0, $scope.trabajadorCargo.length - 3) : "";
        $scope.estLabTraSel = "-1";


        //Carga de Catalogo EstadoCivil
        $scope.estadoCivil = [];
        function listarEstadoCivil() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarEstadoCivil');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.estadoCivil = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        listarEstadoCivil();
        console.log($scope.estadoCivil);

        //Carga de Catalogo Nacionalidad/Pais
        $scope.nacionalidad = [];

        function listarNacionalidad() {
            var request = crud.crearRequest('datos_personales', 1, 'listarNacionalidad');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.nacionalidad = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        listarNacionalidad();
        //Carga de Catalogo Idioma
        $scope.idiomas = [];

        function listarIdioma() {
            var request = crud.crearRequest('datos_personales', 1, 'listarIdioma');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.idiomas = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        listarIdioma();
        $scope.jornadasLaborales=[];
        function listarJornadasLab() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarJornadas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.jornadasLaborales = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarJornadasLab();
        
        $scope.tiposDocumentos = [];
        function listarTiposDoc() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposDoc');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.tiposDocumentos = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarTiposDoc();

        $scope.tiposFormacion2 = [];
        function listarTiposFormacion() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposFor');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.tiposFormacion2 = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarTiposFormacion();

        $scope.nivelesAcademicos = [];
        function listarNivelesAcademicos() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarNivAca');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.nivelesAcademicos = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarNivelesAcademicos();

        $scope.tiposEstudioComplementario = [];
        function listarTiposEstudiosComplementarios() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposEstCom');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.tiposEstudioComplementario = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarTiposEstudiosComplementarios();

        $scope.nivelesEstudioComplementario = [];
        function listarNivelesEstudiosComplementarios() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarNivEstCom');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.nivelesEstudioComplementario = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarNivelesEstudiosComplementarios();

        $scope.personaSel = {
            perId: datosPersonales.persona.perId,
            perCod: datosPersonales.persona.perCod,
            apePat: datosPersonales.persona.apePat,
            apeMat: datosPersonales.persona.apeMat,
            nom: datosPersonales.persona.nom,
            sex: datosPersonales.persona.sex,
            estCivId: datosPersonales.persona.estCivId,
            eda: calcularEdad(datosPersonales.persona.fecNac),
            dni: datosPersonales.persona.dni,
            pas: datosPersonales.persona.pas,
            estAseEss: datosPersonales.persona.estAseEss === "" ? false : datosPersonales.persona.estAseEss,
            autEss: datosPersonales.persona.autEss,
            fij: datosPersonales.persona.fij,
            num1: datosPersonales.persona.num1,
            num2: datosPersonales.persona.num2,
            email: datosPersonales.persona.email,
            nacId: datosPersonales.persona.nacId, //
            depNac: datosPersonales.persona.depNac,
            proNac: datosPersonales.persona.proNac,
            disNac: datosPersonales.persona.disNac,
            fecNac: datosPersonales.persona.fecNac===""?"":convertirFechaStr(datosPersonales.persona.fecNac),
            //2 Direcciones?Abajo
            sisPen: datosPersonales.persona.sisPen === "" ? "NUL" : datosPersonales.persona.sisPen,
            tipAfp: datosPersonales.persona.tipAfp,
            codCuspp: datosPersonales.persona.codCuspp,
            fecIngAfp: datosPersonales.persona.fecIngAfp===""?"":convertirFechaStr(datosPersonales.persona.fecIngAfp),
            fecTraAfp: datosPersonales.persona.fecTraAfp===""?"":convertirFechaStr(datosPersonales.persona.fecTraAfp),
            perDis: datosPersonales.persona.perDis === "" ? false : datosPersonales.persona.perDis,
            regCon: datosPersonales.persona.regCon,
            idiomId: datosPersonales.persona.idiomId, //
            licCond: datosPersonales.persona.licCond,
            bonCaf: datosPersonales.persona.bonCaf,
            estado: 'A'
        };

        $scope.direccionDNISel = {
            dirId: 0,
            tipDir: "R",
            nomDir: "",
            depDir: "",
            proDir: "",
            disDir: "",
            nomZon: "",
            desRef: ""
        };
        $scope.direccionDASel = {
            dirId: 0,
            tipDir: "A",
            nomDir: "",
            depDir: "",
            proDir: "",
            disDir: "",
            nomZon: "",
            desRef: ""
        };

        $scope.boolSalud = {
            valS: ($scope.personaSel.estAseEss) === true ? 'SI' : 'NO'
        };

        $scope.boolDis = {
            val: ($scope.personaSel.perDis) === true ? 'SI' : 'NO'
        };

        $scope.direcciones = [];
        function listarDirecciones() {
            var request = crud.crearRequest('datos_personales', 1, 'listarDirecciones');
            request.setData({perId: datosPersonales.persona.perId});
            crud.listar('/sistema_escalafon', request, function (response) {
                $scope.direcciones = response.data;
                if($scope.direcciones.length!==0)
                {
                    nuevaDireccionSel=[];
                    nuevaDireccionAct=[];
                    for (var dir in $scope.direcciones)
                    {
                        if ($scope.direcciones[dir].tipDir === "R")
                        {
                            nuevaDireccionSel.dirId = $scope.direcciones[dir].dirId;
                            nuevaDireccionSel.tipDir = $scope.direcciones[dir].tipDir;
                            nuevaDireccionSel.nomDir = $scope.direcciones[dir].nomDir;
                            nuevaDireccionSel.depDir = $scope.direcciones[dir].depDir;
                            nuevaDireccionSel.proDir = $scope.direcciones[dir].proDir;
                            nuevaDireccionSel.disDir = $scope.direcciones[dir].disDir;
                            nuevaDireccionSel.nomZon = $scope.direcciones[dir].nomZon;
                            nuevaDireccionSel.desRef = $scope.direcciones[dir].desRef;
                        }
                        if ($scope.direcciones[dir].tipDir === "A")
                        {
                            nuevaDireccionAct.dirId = $scope.direcciones[dir].dirId;
                            nuevaDireccionAct.tipDir = $scope.direcciones[dir].tipDir;
                            nuevaDireccionAct.nomDir = $scope.direcciones[dir].nomDir;
                            nuevaDireccionAct.depDir = $scope.direcciones[dir].depDir;
                            nuevaDireccionAct.proDir = $scope.direcciones[dir].proDir;
                            nuevaDireccionAct.disDir = $scope.direcciones[dir].disDir;
                            nuevaDireccionAct.nomZon = $scope.direcciones[dir].nomZon;
                            nuevaDireccionAct.desRef = $scope.direcciones[dir].desRef;
                        }
                    }
                    $scope.direcciones = [];
                    $scope.direcciones.push(nuevaDireccionSel);//pos0
                    $scope.direcciones.push(nuevaDireccionAct);//pos1
                    console.log("dir", $scope.direcciones);
                }
                changeOptionsDirectionsState($scope.direcciones);
                
            }, function (data) {
                console.info(data);
            });
        }   
        
        function changeOptionsDirectionsState(direcciones) {
            var caso=1;
            if(direcciones.length===0)
                caso=0;
            if(direcciones.length===2)
            {
                if(direcciones[1].nomDir==='' && direcciones[1].depDir==='  '  && direcciones[1].proDir==='  '  &&
                        direcciones[1].disDir==='  '  && direcciones[1].nomZon===''  && direcciones[1].desRef===''  )
                {    
                    caso=3;
                }else
                    caso=2;
            }
            switch (caso) {
                case 1:
                    $scope.optionsDirections = "SI";
                    $scope.status_1.statusGD = true;
                    $scope.statusDA = false;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDir = direcciones[0].depDir;
                    $scope.direccionDNISel.proDir = direcciones[0].proDir;
                    $scope.direccionDNISel.disDir = direcciones[0].disDir;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;
                    break;

                case 2:
                    $scope.optionsDirections = "NO";
                    $scope.status_1.statusGD = true;
                    $scope.status_1.statusLD = true;
                    $scope.statusDA = true;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDir = direcciones[0].depDir;
                    $scope.direccionDNISel.proDir = direcciones[0].proDir;
                    $scope.direccionDNISel.disDir = direcciones[0].disDir;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;

                    $scope.direccionDASel.dirId = direcciones[1].dirId;
                    $scope.direccionDASel.tipDir = direcciones[1].tipDir;
                    $scope.direccionDASel.nomDir = direcciones[1].nomDir;
                    $scope.direccionDASel.depDir = direcciones[1].depDir;
                    $scope.direccionDASel.proDir = direcciones[1].proDir;
                    $scope.direccionDASel.disDir = direcciones[1].disDir;
                    $scope.direccionDASel.nomZon = direcciones[1].nomZon;
                    $scope.direccionDASel.desRef = direcciones[1].desRef;
                    break;
                    
                case 3:
                    $scope.optionsDirections = "SI";
                    $scope.status_1.statusGD = true;
                    $scope.status_1.statusLD = false;
                    $scope.statusDA = false;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDir = direcciones[0].depDir;
                    $scope.direccionDNISel.proDir = direcciones[0].proDir;
                    $scope.direccionDNISel.disDir = direcciones[0].disDir;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;

                    $scope.direccionDASel.dirId = direcciones[1].dirId;
                    $scope.direccionDASel.tipDir = direcciones[1].tipDir;
                    $scope.direccionDASel.nomDir = direcciones[1].nomDir;
                    $scope.direccionDASel.depDir = direcciones[1].depDir;
                    $scope.direccionDASel.proDir = direcciones[1].proDir;
                    $scope.direccionDASel.disDir = direcciones[1].disDir;
                    $scope.direccionDASel.nomZon = direcciones[1].nomZon;
                    $scope.direccionDASel.desRef = direcciones[1].desRef;
                    break;
            }
        }
        listarDirecciones();

        $scope.trabajadorSel = {
            traId: datosPersonales.trabajador.traId,
            traCon: datosPersonales.trabajador.traCon
        };
        $scope.fichaEscalafonariaSel = {
            ficEscId: datosPersonales.ficha.ficEscId
        };

        changeAseguradoState($scope.fichaEscalafonariaSel.autEss);
        changeDishabilityState($scope.fichaEscalafonariaSel.perDis);

        $scope.tipDocs = [
            "Resolución decanal",
            "Resolución rectoral",
            "Resolución subdirectoral",
            "Resolución de consejo universitario",
            "Resolución directoral (DIGA)",
            "Memorándum",
            "Oficio",
            "Constancia",
            "Otros"
        ];

        $scope.jorLabs = ["Completa", "Parcial", "Otros"];

        var tiposEstPos = [
            {id: "1", title: "Maestria"},
            {id: "2", title: "Doctorado"},
            {id: "3", title: "Licenciatura"},
            {id: "4", title: "Otros"}
        ];

        var tiposDesplazamientos = [
            {id: "1", title: "Ingreso"},
            {id: "2", title: "Reingreso"},
            {id: "3", title: "Interrupción"}
        ];

        var motivos = [
            {id: "1", title: "Mérito"},
            {id: "2", title: "Felicitación"},
            {id: "3", title: "25 años de servicio"},
            {id: "4", title: "30 años de servicio"},
            {id: "5", title: "Luto y sepelio"},
            {id: "6", title: "Otros"}
        ];

        $http.get('../recursos/json/departamentos.json').success(function (data) {
            $scope.departamentosLugNac = data;
            $scope.departamentosDirDNI = data;
            $scope.departamentosDirDA = data;
        });

        $scope.loadProLugNac = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.personaSel.depNac;
                })[0];
                $scope.provinciasLugNac = data[dep.id_ubigeo];
                $scope.distritosLugNac = [];
            });
        };

        $scope.loadDisLugNac = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.personaSel.proNac;
                })[0];
                $scope.distritosLugNac = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDNI = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDNISel.depDir;
                })[0];
                $scope.provinciasDirDNI = data[dep.id_ubigeo];
                $scope.distritosDirDNI = [];
            });
        };

        $scope.loadDisDirDNI = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDNISel.proDir;
                })[0];
                $scope.distritosDirDNI = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDA = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDASel.depDir;
                })[0];
                $scope.provinciasDirDA = data[dep.id_ubigeo];
                $scope.distritosDirDA = [];
            });
        };

        $scope.loadDisDirDA = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDASel.proDir;
                })[0];
                $scope.distritosDirDA = data[pro.id_ubigeo];
            });
        };

        $scope.nuevaFichaEscalafonaria = {ficEscId: 0, autEss: "", sisPen: "NUL", nomAfp: "", codCuspp: "", fecIngAfp: "", perDis: false, regCon: "", gruOcu: ""};
        $scope.legajoDatosPersonales = [
            {ficEscId: "", nom: "", archivo: "", url: "", des: "DNI", catLeg: "1", subCat: "1", codAspOri: 0},
            {ficEscId: "", nom: "", archivo: "", url: "", des: "Partida de Nacimiento", catLeg: "1", subCat: "2", codAspOri: 0},
            {ficEscId: "", nom: "", archivo: "", url: "", des: "Registro CONADIS", catLeg: "1", subCat: "4", codAspOri: 0}
        ];

        var estadosLaborales = [
            {estLabId: '1', estLabDes: "Ingreso"},
            {estLabId: '2', estLabDes: "Rotación"},
            {estLabId: '3', estLabDes: "Proyección"},
            {estLabId: '4', estLabDes: "Retiro"},
            {estLabId: '9', estLabDes: "Reingreso"}
        ];

        $scope.OpenBrowserInNewTab = function (coddocumento, nroPagAgrp, pagIni, uriv) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/visorPDF.html",
                controller: "visorCtrl",
                inputs: {
                    numPaginas: nroPagAgrp,
                    idPagDigital: pagIni,
                    uri: uriv
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        }

        $scope.mostrarEstLabFormato = function (row) {
            //console.log("estlab of the fucker: "+row.estLab);
            if (row.estLab === undefined || row.estLab === null) {
                row.estLab = '1';
            }
            ;
            switch (row.estLab) {
                case '1':
                    $("#btn-est-lab" + row.traId).removeClass("btn-warning").addClass("btn-success");
                    return estadosLaborales[0].estLabDes;
                case '2':
                    $("#btn-est-lab" + row.traId).removeClass("btn-success").addClass("btn-warning");
                    return estadosLaborales[1].estLabDes;
                case '3':
                    $("#btn-est-lab" + row.traId).removeClass("btn-danger").addClass("btn-info");
                    return estadosLaborales[2].estLabDes;
                case '4':
                    $("#btn-est-lab" + row.traId).removeClass("btn-info").addClass("btn-danger");
                    return estadosLaborales[3].estLabDes;
                case '9':
                    $("#btn-est-lab" + row.traId).removeClass("btn-warning").addClass("btn-success");
                    return estadosLaborales[4].estLabDes;
            }
        };
        //Funciones para listas los datos de tablas
        $rootScope.listarDatCenLab = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('datos_trabajador', 1, 'listarDatCenLabTra');
            request.setData({perId: datosPersonales.persona.perId});

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.forEach(function (item) {
                        item.estLabDes = buscarContenido(estadosLaborales, 'estLabId', 'estLabDes', item.estLab);
                        datosPersonales.trabajador.estLab = item.estLabDes;
                        $scope.estLabTraSel = item.estLab;

                    });
                    $rootScope.settingDatCenLab.dataset = response.data;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingDatCenLab.dataset);
                    $rootScope.tablaDatCenLab.settings($rootScope.settingDatCenLab);
                } else {
                    response.responseMsg;
                }
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarParientes = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarParientes');
            request.setData($scope.trabajadorSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.parEda = calcularEdad(item.parFecNac);
                });
                $rootScope.settingModParientes.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModParientes.dataset);
                $rootScope.tablaModParientes.settings($rootScope.settingModParientes);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarFormacionesEducativas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('formacion_educativa', 1, 'listarFormacionesEducativas');
            request.setData($scope.personaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.estConDes = item.estCon === "" ? "No registrado" : (item.estCon ? "Si" : "No");
                    item.tipFor = buscarContenido($scope.tiposFormacion2, 'id', 'nom', item.tipForId);
                    item.nivAca = buscarContenido($scope.nivelesAcademicos, 'id', 'nom', item.nivAcaId);
                    item.pais = item.paisId === 0 ? "No registrado" : buscarContenido($scope.nacionalidad, 'nacId', 'nacNom', item.paisId);
                });
                $rootScope.settingModForEdu.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModForEdu.dataset);
                $rootScope.tablaModForEdu.settings($rootScope.settingModForEdu);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarColegiaturas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('colegiatura', 1, 'listarColegiaturas');
            request.setData($scope.personaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.conRegDes = item.conReg ? "Habilitado" : "No habilitado";
                });
                $rootScope.settingModColegiaturas.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModColegiaturas.dataset);
                $rootScope.tablaModColegiaturas.settings($rootScope.settingModColegiaturas);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarEstudiosComplementarios = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_complementario', 1, 'listarEstudiosComplementarios');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {

                    item.tip = item.tipId === 0 ? "No registrado" : buscarContenido($scope.tiposEstudioComplementario, 'id', 'nom', item.tipId);
                    item.niv = item.nivId === 0 ? "No registrado" : buscarContenido($scope.nivelesEstudioComplementario, 'id', 'nom', item.nivId);
                });
                $rootScope.settingModEstCom.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModEstCom.dataset);
                $rootScope.tablaModEstCom.settings($rootScope.settingModEstCom);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarEstudiosPostgrado = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_postgrado', 1, 'listarEstudiosPostgrado');
            request.setData($scope.personaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    console.log(item);
                    item.tipDoc = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                    item.tipDes = buscarContenido(tiposEstPos, 'id', 'title', item.tip);
                });
                $rootScope.settingModEstPos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModEstPos.dataset);
                $rootScope.tablaModEstPos.settings($rootScope.settingModEstPos);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarExposiciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('exposicion_ponencia', 1, 'listarExposiciones');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                $rootScope.settingModExpPon.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModExpPon.dataset);
                $rootScope.tablaModExpPon.settings($rootScope.settingModExpPon);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarPublicaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('publicacion', 1, 'listarPublicaciones');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                $rootScope.settingModPublicaciones.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModPublicaciones.dataset);
                $rootScope.tablaModPublicaciones.settings($rootScope.settingModPublicaciones);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarDesplazamientos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    console.log(response);
                    console.log(item.fecIni.split("-"));
                    var datosFechaTer = item.fecTer.split("-");
                    var datosFechaIni = item.fecIni.split("-");
                    //var datosFechaInf = item.fecInf.split("-");
                    //var datosFechaRecInf = item.fecRecInf.split("-");
                    var datosFechaDoc = item.fecDoc.split("-");

                    var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                    var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    //var fecInfFor = new Date(parseInt(datosFechaInf[2]), parseInt(datosFechaInf[1]) - 1, parseInt(datosFechaInf[0]));
                    //var fecRecInfFor = new Date(parseInt(datosFechaRecInf[2]), parseInt(datosFechaRecInf[1]) - 1, parseInt(datosFechaRecInf[0]));
                    var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));
                    console.log(fecIniFor);
                    item.fecIni = fecIniFor;
                    item.fecTer = fecTerFor;
                    //item.fecInf = fecInfFor;
                    //item.fecRecInf = fecRecInfFor;
                    item.fecDoc = fecDocFor;

                    
                    item.tipDes = buscarContenido(tiposDesplazamientos, 'id', 'title', item.tip);
                    item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                    item.jorLabDes = buscarContenido($scope.jornadasLaborales, 'id', 'nom', item.jorLabId);
                });
                $rootScope.settingModDesplazamientos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModDesplazamientos.dataset);
                $rootScope.tablaModDesplazamientos.settings($rootScope.settingModDesplazamientos);
                $rootScope.tablaModDesplazamientos.reload();
                
            }, function (error) {
                console.info(error);
            });
        };



        $scope.planillas = [];
        listarPlanillas();
        function listarPlanillas() {
            var request = crud.crearRequest('planillaConfiguracion', 1, 'listarPlanillas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.forEach(function (item) {
                        $scope.planillas[item.plaId] = item.nomPla;
                    });
                    //$scope.planillas[response.data.plaId] = response.data.nomPla;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.categorias = [];
        listarCategorias();
        function listarCategorias() {
            var request = crud.crearRequest('categoriaConfiguracion', 1, 'listarCategorias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.forEach(function (item) {
                        $scope.categorias[item.catId] = item.nomCat;
                    });
                    //$scope.categorias[response.data.catId]=response.data.nomCat;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.organismos = [];
        listarOrganismos();
        function listarOrganismos() {
            var request = crud.crearRequest('organismoConfiguracion', 1, 'listarOrganismos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.forEach(function (item) {
                        $scope.organismos[item.orgaId] = item.nomOrga;
                    });
                    //$scope.organismos[response.data.orgaId] = response.data.nomOrga;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.dependencias = [];
        listarDependencias();
        function listarDependencias() {
            var request = crud.crearRequest('dependenciaConfiguracion', 1, 'listarDependencias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.forEach(function (item) {
                        $scope.dependencias[item.depId] = item.nomDep;
                    });
                    //console.log("deps: "+JSON.stringify($scope.dependencias));
                    //$scope.dependencias[response.data.depId]= response.data.nomDep;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.listarAscensos = function () {

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            var rols = [], tiposAscn = ["Ascenso", "Rotación", "Nombramiento"];
            var request = crud.crearRequest('rol', 1, 'listarRoles');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/configuracionInicial", request, function (data) {
                //$scope.roles = data.data;
                data.data.forEach(function (item) {
                    rols[item.rolID] = item.nombre;
                });
                //preparamos un objeto request
                var request = crud.crearRequest('ascenso', 1, 'listarAscensos');
                request.setData($scope.fichaEscalafonariaSel);
                crud.listar("/sistema_escalafon", request, function (response) {
                    console.info(response);
                    response.data.forEach(function (item) {
                        item.tipNom = tiposAscn[item.tip - 1];
                        item.catNom = $scope.categorias[item.catId];
                        item.depNom = $scope.dependencias[item.depId];
                        item.orgNom = $scope.organismos[item.orgId];
                        item.plaNom = $scope.planillas[item.plaId];
                        item.escNom = rols[parseInt(item.esc)];
                        //console.log("ascnso: "+JSON.stringify(item));
                    });
                    $rootScope.settingModAscensos.dataset = response.data;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingModAscensos.dataset);
                    $rootScope.tablaModAscensos.settings($rootScope.settingModAscensos);
                }, function (error) {
                    console.info(error);
                });
                //console.log("rols2: "+JSON.stringify($scope.rols));
            }, function (data) {
                console.info(data);
            });

        };

        $scope.listarCapacitaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('capacitacion', 1, 'listarCapacitaciones');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                $rootScope.settingModCapacitaciones.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModCapacitaciones.dataset);
                $rootScope.tablaModCapacitaciones.settings($rootScope.settingModCapacitaciones);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarReconocimientos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('reconocimientos', 1, 'listarReconocimientos');
            //request.setData($scope.fichaEscalafonariaSel);
            request.setData($scope.personaSel);
            console.log(">>>");
            console.log($scope.personaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon/", request, function (response) {
                response.data.forEach(function (item) {
                    item.tipDoc = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                    item["motDes"] = buscarContenido(motivos, 'id', 'title', item.mot);//Agregar la descripcion del motivo
                });
                $rootScope.settingModReconocimientos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModReconocimientos.dataset);
                $rootScope.tablaModReconocimientos.settings($rootScope.settingModReconocimientos);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarDemeritos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('demeritos', 1, 'listarDemeritos');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.tipDoc = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                    item.sepDes = item.sep ? "Si" : "No";
                });
                $rootScope.settingModDemeritos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModDemeritos.dataset);
                $rootScope.tablaModDemeritos.settings($rootScope.settingModDemeritos);
            }, function (error) {
                console.info(error);
            });
        };

        //Agregar Nueva formación educativa
        $scope.showNuevaFormacionEducativa = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarFormacionEducativa.html",
                controller: "agregarNuevaFormacionEducativaCtrl",
                inputs: {
                    title: "Nueva Formacion Educativa",
                    personaId: datosPersonales.persona.perId,
                    tipFors: $scope.tiposFormacion2,
                    niveles: $scope.nivelesAcademicos,
                    tipDocs: $scope.tiposDocumentos,
                    paises: $scope.nacionalidad
                            //fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };


        //Agregar nuevo pariente
        $scope.showNuevoPariente = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarPariente.html",
                controller: "agregarNuevoParienteCtrl",
                inputs: {
                    title: "Nuevo Pariente",
                    personaId: datosPersonales.persona.perId,
                    sexo: $scope.sexo,
                    actDni: datosPersonales.persona.dni
                            //fichaEscalafonariaId: datosPersonales.ficha.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevaColegiatura = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarColegiatura.html",
                controller: "agregarNuevaColegiaturaCtrl",
                inputs: {
                    title: "Nueva Colegiatura",
                    personaId: datosPersonales.persona.perId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevoEstudioComplementario = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarEstudioComplementario.html",
                controller: "agregarNuevoEstudioComplementarioCtrl",
                inputs: {
                    title: "Nuevo Estudio Complementario",
                    personaId: datosPersonales.persona.perId,
                    tiposEstCom: $scope.tiposEstudioComplementario,
                    nivelesEstCom: $scope.nivelesEstudioComplementario
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevoEstudioPostgrado = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarEstudioPostgrado.html",
                controller: "agregarNuevoEstudioPostgradoCtrl",
                inputs: {
                    title: "Nueva Estudio Postgrado",
                    personaId: datosPersonales.persona.perId,
                    tipDocs: $scope.tiposDocumentos
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevaExposicion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarExposicion.html",
                controller: "agregarNuevaExposicionCtrl",
                inputs: {
                    title: "Nueva Exposicion y/o Ponencia",
                    personaId: datosPersonales.persona.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevaPublicacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarPublicacion.html",
                controller: "agregarNuevaPublicacionCtrl",
                inputs: {
                    title: "Nueva Publicacion",
                    personaId: datosPersonales.persona.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevoDesplazamiento = function () {
            //console.log("asd: "+JSON.stringify(datosPersonales));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarDesplazamiento.html",
                controller: "agregarNuevoDesplazamientoCtrl",
                inputs: {
                    title: "Nueva Interrupción",
                    fichaEscalafonaria: datosPersonales.ficha,
                    trabajadorInfo: datosPersonales,
                    tipDocs: $scope.tipDocs,
                    jorLabs: $scope.jorLabs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevoAscenso = function (val) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarAscenso.html",
                controller: "agregarNuevoAscensoCtrl",
                inputs: {
                    title: val !== undefined ? "Nuevo Nombramiento" : "Nuevo Ascenso",
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    orgas: $scope.organismos,
                    deps: $scope.dependencias,
                    cats: $scope.categorias,
                    plans: $scope.planillas,
                    nombOasc: val !== undefined,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevaCapacitacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarCapacitacion.html",
                controller: "agregarNuevaCapacitacionCtrl",
                inputs: {
                    title: "Nueva capacitación",
                    personaId: datosPersonales.persona.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevaLicencia = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarLicencia.html",
                controller: "agregarNuevaLicenciaCtrl",
                inputs: {
                    title: "Nueva licencia",
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevaVacacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarVacacion.html",
                controller: "agregarNuevaVacacionCtrl",
                inputs: {
                    title: "Nueva vacación",
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevoReconocimiento = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarReconocimiento.html",
                controller: "agregarNuevoReconocimientoCtrl",
                inputs: {
                    title: "Nuevo Reconocimiento",
                    //fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    personaId: datosPersonales.persona.perId,
                    tipDocs: $scope.tiposDocumentos
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevoDemerito = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/agregarDemerito.html",
                controller: "agregarNuevoDemeritoCtrl",
                inputs: {
                    title: "Nueva Demerito",
                    //fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    personaId: datosPersonales.persona.perId,
                    tipDocs: $scope.tiposDocumentos
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.actualizarDatosGenerales = function () {

            //Direcciones DNI
            $scope.direccionesToSend = [];
            $scope.direccionesToSend.push($scope.direccionDNISel);
            if($scope.statusDA){
                $scope.direccionesToSend.push($scope.direccionDASel);
            }
            else{
                $scope.direccionDASel.nomDir="";//Por si el domicilio actual coincide con el DNI(con datos)-> Limpiar caso "No"(DNISel).
                $scope.direccionDASel.depDir="";
                $scope.direccionDASel.proDir="";
                $scope.direccionDASel.disDir="";
                $scope.direccionDASel.nomZon="";
                $scope.direccionDASel.desRef="";
                $scope.direccionesToSend.push($scope.direccionDASel);
            }
            console.log("direcciones",$scope.direccionesToSend);

            if ($scope.boolSalud.valS === 'SI' && $scope.personaSel.autEss.length < 15) {
                modal.mensaje("ALERTA", "El código autogenerado de ESSALUD debe tener 15 caracteres");
                return;
            }
            if ($scope.personaSel.sisPen === 'AFP' && $scope.personaSel.codCuspp.length < 12) {
                modal.mensaje("ALERTA", "El código COD. CUSPP debe tener 12 caracteres");
                return;
            }

            var request = crud.crearRequest('datos_personales', 1, 'actualizarDatosPersonales');
            request.setData({persona: $scope.personaSel, direcciones: $scope.direccionesToSend});
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                
                listarDirecciones();
                /*$scope.personaSel.apePat = response.data.persona.apePat;
                 $scope.personaSel.apeMat = response.data.persona.apeMat;
                 $scope.personaSel.nom = response.data.persona.nom;
                 $scope.personaSel.sex = response.data.persona.sex;
                 $scope.personaSel.estCiv = response.data.persona.estCiv;
                 $scope.personaSel.dni = response.data.persona.dni;
                 $scope.personaSel.pas = response.data.persona.pas;
                 
                 $scope.personaSel.fecNac = new Date(response.data.persona.fecNac);
                 $scope.personaSel.num1 = response.data.persona.num1;
                 $scope.personaSel.num2 = response.data.persona.num2;
                 $scope.personaSel.fij = response.data.persona.fij;
                 $scope.personaSel.email = response.data.persona.email;
                 
                 
                 $scope.personaSel.depNac = response.data.persona.depNac;
                 $scope.personaSel.proNac = response.data.persona.proNac;
                 $scope.personaSel.disNac = response.data.persona.disNac;
                 
                 $scope.trabajadorSel.traCon = response.data.trabajador.traCon;
                 
                 $scope.fichaEscalafonariaSel.autEss = response.data.ficha.autEss;
                 $scope.fichaEscalafonariaSel.sisPen = response.data.ficha.sisPen;
                 $scope.fichaEscalafonariaSel.nomAfp = response.data.ficha.nomAfp;
                 $scope.fichaEscalafonariaSel.codCuspp = response.data.ficha.codCuspp;
                 $scope.fichaEscalafonariaSel.fecIngAfp = new Date(response.data.ficha.fecIngAfp);
                 $scope.fichaEscalafonariaSel.perDis = response.data.ficha.perDis;
                 $scope.fichaEscalafonariaSel.regCon = response.data.ficha.autEss;
                 $scope.fichaEscalafonariaSel.gruOcu = response.data.ficha.gruOcu;
                 */
                /*
                switch (response.data.direcciones.lenght) {
                    case 1:
                        $scope.direccionDNISel.tipDir = response.data.direcciones[0].tipDir;
                        $scope.direccionDNISel.nomDir = response.data.direcciones[0].nomDir;
                        $scope.direccionDNISel.depDir = response.data.direcciones[0].depDir;
                        $scope.direccionDNISel.proDir = response.data.direcciones[0].proDir;
                        $scope.direccionDNISel.disDir = response.data.direcciones[0].disDir;
                        $scope.direccionDNISel.nomZon = response.data.direcciones[0].nomZon;
                        $scope.direccionDNISel.desRef = response.data.direcciones[0].desRef;

                        break;
                    case 2:
                        $scope.direccionDNISel.tipDir = response.data.direcciones[0].tipDir;
                        $scope.direccionDNISel.nomDir = response.data.direcciones[0].nomDir;
                        $scope.direccionDNISel.depDir = response.data.direcciones[0].depDir;
                        $scope.direccionDNISel.proDir = response.data.direcciones[0].proDir;
                        $scope.direccionDNISel.disDir = response.data.direcciones[0].disDir;
                        $scope.direccionDNISel.nomZon = response.data.direcciones[0].nomZon;
                        $scope.direccionDNISel.desRef = response.data.direcciones[0].desRef;

                        $scope.direccionDASel.tipDir = response.data.direcciones[1].tipDir;
                        $scope.direccionDASel.nomDir = response.data.direcciones[1].nomDir;
                        $scope.direccionDASel.depDir = response.data.direcciones[1].depDir;
                        $scope.direccionDASel.proDir = response.data.direcciones[1].proDir;
                        $scope.direccionDASel.disDir = response.data.direcciones[1].disDir;
                        $scope.direccionDASel.nomZon = response.data.direcciones[1].nomZon;
                        $scope.direccionDASel.desRef = response.data.direcciones[1].desRef;
                        break;
                }*/

            }, function (data) {
                console.info(data);
            });
        };

        $scope.prepararEditarRotacion = function (d) {
            var trabInfo = JSON.parse(JSON.stringify(d));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstadoRotacion.html",
                controller: "editarEstadoRotacionCtrl",
                inputs: {
                    title: "Rotación",
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    trabajadorInfo: trabInfo,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarProyeccion = function (d) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstadoProyeccion.html",
                controller: "editarEstadoProyeccionCtrl",
                inputs: {
                    title: "Proyección",
                    trabajadorInfo: d,
                    fichaEscalafonaria: datosPersonales.ficha.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarRetiro = function (d) {
            ModalService.showModal({
                templateUrl: (d.estLab != '4' ? "administrativa/sistema_escalafon/editarEstadoRetiro.html" : "administrativa/sistema_escalafon/agregarReingreso.html"),
                controller: (d.estLab != '4' ? "editarEstadoRetiroCtrl" : "agregarReingresoCtrl"),
                inputs: {
                    title: (d.estLab == '1' ? "Retiro" : "Reingreso"),
                    trabajadorInfo: d,
                    fichaEscalafonaria: datosPersonales.ficha,
                    tipDocs: $scope.tipDocs,
                    jorLabs: $scope.jorLabs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarDatCenLab = function (d) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDatCenLab.html",
                controller: "editarDatCenLabCtrl",
                inputs: {
                    title: "Edición de datos de centro laboral",
                    trabajadorInfo: d
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };


        $scope.prepararReportePersonalizado = function (t) {
            var trabajadorSel = JSON.parse(JSON.stringify(t));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/modalSeleccionadosPersonalizado.html",
                controller: "modalSeleccionadosPersonalizadoCtrl",
                inputs: {
                    title: "Modal seleccionados Personalizado",
                    trabajadorInfo: trabajadorSel
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarPariente = function (p) {
            var parienteSel = JSON.parse(JSON.stringify(p));
            console.log(parienteSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarPariente.html",
                controller: "editarParienteCtrl",
                inputs: {
                    title: "Editar datos del pariente",
                    pariente: parienteSel,
                    personaId: $scope.personaSel.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarFormacionEducativa = function (fe) {
            var formacionEducativaSel = JSON.parse(JSON.stringify(fe));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarFormacionEducativa.html",
                controller: "editarFormacionEducativaCtrl",
                inputs: {
                    title: "Editar datos de Formacion Educativa",
                    formacionEducativa: formacionEducativaSel,
                    personaId: $scope.personaSel.perId,
                    tipFors: $scope.tiposFormacion2,
                    niveles: $scope.nivelesAcademicos,
                    tipDocs: $scope.tiposDocumentos,
                    paises: $scope.nacionalidad
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };

        $scope.prepararEditarColegiatura = function (c) {
            var colegiaturaSel = JSON.parse(JSON.stringify(c));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarColegiatura.html",
                controller: "editarColegiaturaCtrl",
                inputs: {
                    title: "Editar datos de Colegiatura",
                    colegiatura: colegiaturaSel,
                    personaId: $scope.personaSel.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };

        $scope.prepararEditarEstudioComplementario = function (e) {
            var estComSel = JSON.parse(JSON.stringify(e));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarEstudioComplementario.html",
                controller: "editarEstudioComplementarioCtrl",
                inputs: {
                    title: "Editar datos de Estudio Complementario",
                    personaId: $scope.personaSel.perId,
                    estudioComplementario: estComSel,
                    tiposEstCom: $scope.tiposEstudioComplementario,
                    nivelesEstCom: $scope.nivelesEstudioComplementario
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };

        $scope.prepararEditarEstudioPostgrado = function (e) {
            var estPosSel = JSON.parse(JSON.stringify(e));
            console.log(estPosSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarEstudioPostgrado.html",
                controller: "editarEstudioPostgradoCtrl",
                inputs: {
                    title: "Editar datos de Estudio de Postgrado",
                    estudioPostgrado: estPosSel,
                    personaId: $scope.personaSel.perId,
                    tipDocs: $scope.tiposDocumentos
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarExposicion = function (e) {
            var expSel = JSON.parse(JSON.stringify(e));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarExposicion.html",
                controller: "editarExposicionCtrl",
                inputs: {
                    title: "Editar datos de Exposicion",
                    exposicion: expSel,
                    personaId: $scope.personaSel.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarPublicacion = function (p) {
            var pubSel = JSON.parse(JSON.stringify(p));
            console.log(pubSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarPublicacion.html",
                controller: "editarPublicacionCtrl",
                inputs: {
                    title: "Editar datos de Publicacion",
                    publicacion: pubSel,
                    personaId: $scope.personaSel.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarDesplazamiento = function (d) {
            var desSel = JSON.parse(JSON.stringify(d));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDesplazamiento.html",
                controller: "editarDesplazamientoCtrl",
                inputs: {
                    title: "Editar datos de Desplazamiento",
                    desplazamiento: desSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    trabajadorInfo: datosPersonales,
                    tipDocs: $scope.tipDocs,
                    jorLabs: $scope.jorLabs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarAscenso = function (a) {
            var ascSel = JSON.parse(JSON.stringify(a));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarAscenso.html",
                controller: "editarAscensoCtrl",
                inputs: {
                    title: "Editar datos de Ascenso",
                    ascenso: ascSel,
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    orgas: $scope.organismos,
                    deps: $scope.dependencias,
                    cats: $scope.categorias,
                    plans: $scope.planillas,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarCapacitacion = function (c) {
            var capSel = JSON.parse(JSON.stringify(c));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarCapacitacion.html",
                controller: "editarCapacitacionCtrl",
                inputs: {
                    title: "Editar datos de Capacitacion",
                    capacitacion: capSel,
                    personaId: $scope.personaSel.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarLicencia = function (c) {
            var licSel = JSON.parse(JSON.stringify(c));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarLicencia.html",
                controller: "editarLicenciaCtrl",
                inputs: {
                    title: "Editar datos de Licencia",
                    licencia: licSel,
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarVacacion = function (v) {
            var vacSel = JSON.parse(JSON.stringify(v));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarVacacion.html",
                controller: "editarVacacionCtrl",
                inputs: {
                    title: "Editar datos de Vacación",
                    vacacion: vacSel,
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarReconocimiento = function (r) {
            var recSel = JSON.parse(JSON.stringify(r));
            //console.log("recuperar");
            //console.log(recSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarReconocimiento.html",
                controller: "editarReconocimientoCtrl",
                inputs: {
                    title: "Editar datos de Reconocimiento",
                    reconocimiento: recSel,
                    personaId: $scope.personaSel.perId,
                    //fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    tipDocs: $scope.tiposDocumentos
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarDemerito = function (d) {
            var demSel = JSON.parse(JSON.stringify(d));
            console.log(demSel);
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/ficha_escalafonaria/editarDemerito.html",
                controller: "editarDemeritoCtrl",
                inputs: {
                    title: "Editar datos de Demerito",
                    demerito: demSel,
                    personaId: $scope.personaSel.perId,
                    //fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    tipDocs: $scope.tiposDocumentos
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.eliminarPariente = function (p) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el pariente?", function () {
                var request = crud.crearRequest('familiares', 1, 'eliminarFamiliar');
                request.setData({parId: p.parId, perId: $scope.personaSel.perId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModParientes.settings().dataset, function (item) {
                            return p === item;
                        });
                        $rootScope.tablaModParientes.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarFormacionEducativa = function (fe) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la formación educativa?", function () {
                var request = crud.crearRequest('formacion_educativa', 1, 'eliminarFormacionEducativa');
                request.setData({forEduId: fe.forEduId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModForEdu.settings().dataset, function (item) {
                            return fe === item;
                        });
                        $rootScope.tablaModForEdu.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarColegiatura = function (c) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la colegiatura?", function () {
                var request = crud.crearRequest('colegiatura', 1, 'eliminarColegiatura');
                request.setData({colId: c.colId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModColegiaturas.settings().dataset, function (item) {
                            return c === item;
                        });
                        $rootScope.tablaModColegiaturas.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarEstudioComplementario = function (ec) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el estudio complementario?", function () {
                var request = crud.crearRequest('estudio_complementario', 1, 'eliminarEstudioComplementario');
                request.setData({estComId: ec.estComId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModEstCom.settings().dataset, function (item) {
                            return ec === item;
                        });
                        $rootScope.tablaModEstCom.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarEstudioPostgrado = function (ep) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el estudio de postgrado?", function () {
                var request = crud.crearRequest('estudio_postgrado', 1, 'eliminarEstudioPostgrado');
                request.setData({estPosId: ep.estPosId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModEstPos.settings().dataset, function (item) {
                            return ep === item;
                        });
                        $rootScope.tablaModEstPos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarExposicion = function (ep) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la exposicion y/o ponencia?", function () {
                var request = crud.crearRequest('exposicion_ponencia', 1, 'eliminarExposicion');
                request.setData({expId: ep.expId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModExpPon.settings().dataset, function (item) {
                            return ep === item;
                        });
                        $rootScope.tablaModExpPon.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarPublicacion = function (p) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la publicacion?", function () {
                var request = crud.crearRequest('publicacion', 1, 'eliminarPublicacion');
                request.setData({pubId: p.pubId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModPublicaciones.settings().dataset, function (item) {
                            return p === item;
                        });
                        $rootScope.tablaModPublicaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarDesplazamiento = function (d) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el desplazamiento?", function () {
                var request = crud.crearRequest('desplazamiento', 1, 'eliminarDesplazamiento');
                request.setData({desId: d.desId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModDesplazamientos.settings().dataset, function (item) {
                            return d === item;
                        });
                        $rootScope.tablaModDesplazamientos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarAscenso = function (a) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el ascenso?", function () {
                var request = crud.crearRequest('ascenso', 1, 'eliminarAscenso');
                request.setData({ascId: a.ascId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModAscensos.settings().dataset, function (item) {
                            return a === item;
                        });
                        $rootScope.tablaModAscensos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarCapacitacion = function (c) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la capacitacion?", function () {
                var request = crud.crearRequest('capacitacion', 1, 'eliminarCapacitacion');
                request.setData({capId: c.capId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModCapacitaciones.settings().dataset, function (item) {
                            return c === item;
                        });
                        $rootScope.tablaModCapacitaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarLicencia = function (l) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la licencia?", function () {
                var request = crud.crearRequest('licencia', 1, 'eliminarLicencia');
                request.setData({licId: l.licId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModLicencias.settings().dataset, function (item) {
                            return l === item;
                        });
                        $rootScope.tablaModLicencias.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarVacacion = function (v) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la vacación?", function () {
                var request = crud.crearRequest('vacacion', 1, 'eliminarVacacion');
                request.setData({vacId: v.vacId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModVacaciones.settings().dataset, function (item) {
                            return v === item;
                        });
                        $rootScope.tablaModVacaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarVacacion = function (v) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la vacación?", function () {
                var request = crud.crearRequest('vacacion', 1, 'eliminarVacacion');
                request.setData({vacId: v.vacId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModVacaciones.settings().dataset, function (item) {
                            return v === item;
                        });
                        $rootScope.tablaModVacaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };


        $scope.eliminarReconocimiento = function (r) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la reconocimiento?", function () {
                var request = crud.crearRequest('reconocimientos', 1, 'eliminarReconocimiento');
                console.log(r);
                request.setData({recId: r.recId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModReconocimientos.settings().dataset, function (item) {
                            return r === item;
                        });
                        $rootScope.tablaModReconocimientos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarDemerito = function (d) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la demerito?", function () {
                var request = crud.crearRequest('demeritos', 1, 'eliminarDemerito');
                request.setData({demId: d.demId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModDemeritos.settings().dataset, function (item) {
                            return d === item;
                        });
                        $rootScope.tablaModDemeritos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };
    }]);
seApp.controller('editarDatCenLabCtrl', ['$rootScope', '$scope', '$element', 'title', 'trabajadorInfo', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, trabajadorInfo, close, crud, modal) {
        $scope.title = title;

        $scope.trabajadorSel = {
            traId: trabajadorInfo.traId,

            orgId: trabajadorInfo.orgId,
            orgNom: trabajadorInfo.orgNom,

            rolId: trabajadorInfo.rolId,
            rolNom: trabajadorInfo.rolNom,

            plaId: trabajadorInfo.plaId,
            plaNom: trabajadorInfo.plaNom,

            catId: trabajadorInfo.catId,
            catNom: trabajadorInfo.catNom,

            orgaId: trabajadorInfo.orgaId,
            orgaNom: trabajadorInfo.orgaNom,

            facId: trabajadorInfo.facId,
            facNom: trabajadorInfo.facNom,

            depId: trabajadorInfo.depId,
            depNom: trabajadorInfo.depNom,

            zonId: trabajadorInfo.zonId,
            zonNom: trabajadorInfo.zonNom,

            carId: trabajadorInfo.carId,
            carNom: trabajadorInfo.carNom,

            estLab: trabajadorInfo.estLab,

            fecIng: trabajadorInfo.fecIngEst ? new Date(trabajadorInfo.fecIng.split("-")[2], trabajadorInfo.fecIng.split("-")[1] - 1, trabajadorInfo.fecIng.split("-")[0]) : "",

            fecIngEst: trabajadorInfo.fecIngEst,

            fecSal: trabajadorInfo.fecSal,

            sal: trabajadorInfo.sal,

            tieServ: trabajadorInfo.tieServ,

            aniosOut: trabajadorInfo.aniosOut
        };


        $scope.estadosLaborales = [
            {estLabId: '1', estLabNom: "Ingreso"},
            {estLabId: '2', estLabNom: "Rotación"},
            {estLabId: '3', estLabNom: "Proyección"},
            {estLabId: '4', estLabNom: "Retiro"}
        ];

        $scope.planillas = [];
        listarPlanillas();
        function listarPlanillas() {
            var request = crud.crearRequest('planillaConfiguracion', 1, 'listarPlanillas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.planillas = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.categorias = [];
        listarCategorias();
        function listarCategorias() {
            var request = crud.crearRequest('categoriaConfiguracion', 1, 'listarCategorias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.categorias = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.zonas = [];
        listarZonas();
        function listarZonas() {
            var request = crud.crearRequest('zonaConfiguracion', 1, 'listarZonas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.zonas = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.cargos = [];
        listarCargos();
        function listarCargos() {
            var request = crud.crearRequest('cargoConfiguracion', 1, 'listarCargos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.cargos = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.organismos = [];
        listarOrganismos();
        function listarOrganismos() {
            var request = crud.crearRequest('organismoConfiguracion', 1, 'listarOrganismos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.organismos = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        var facultades = [];
        $scope.facultadesSel = [];
        listarFacultades();
        function listarFacultades() {
            var request = crud.crearRequest('facultadConfiguracion', 1, 'listarFacultades');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    facultades = response.data;
                    $scope.listarFacultadesSel();
                }
            }, function (data) {
                console.info(data);
            });
        }

        var dependencias = [];
        $scope.dependenciasSel = [];
        listarDependencias();
        function listarDependencias() {
            var request = crud.crearRequest('dependenciaConfiguracion', 1, 'listarDependencias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    dependencias = response.data;
                    $scope.listarDependenciasSel();
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.listarFacultadesSel = function () {
            $scope.facultadesSel = [];
            facultades.forEach(function (item) {
                if (item.orgaId === $scope.trabajadorSel.orgaId)
                    $scope.facultadesSel.push(item);
            });
            $scope.dependenciasSel = [];
        };

        $scope.listarDependenciasSel = function () {
            $scope.dependenciasSel = [];
            dependencias.forEach(function (item) {
                if (item.facId === $scope.trabajadorSel.facId)
                    $scope.dependenciasSel.push(item);
            });
        };

        $scope.actualizarDatCenLab = function () {
            $scope.trabajadorSel.fecIngEst = $scope.trabajadorSel.fecIng !== "" ? true : false;
            var request = crud.crearRequest('datos_trabajador', 1, 'actualizarDatCenLabTra');

            request.setData($scope.trabajadorSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.i = trabajadorInfo.i;
                    //response.data.fecIng = new Date(response.data.fecIng);
                    $rootScope.settingDatCenLab.dataset[trabajadorInfo.i] = response.data;

                    $rootScope.tablaDatCenLab.reload();
                    $rootScope.listarDatCenLab();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else {
                    modal.mensaje("ERROR", response.responseMsg);
                }
            }, function (error) {
                console.info(error);
            });

        };


    }]);
seApp.controller('editarParienteCtrl', ['$rootScope', '$scope', '$element', 'title', 'pariente', 'personaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, pariente, personaId, close, crud, modal) {
        $scope.title = title;

        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.tipoPariente = [];

        function listarTipoParentesco() {
            var request = crud.crearRequest('familiares', 1, 'listarTipoParentesco');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.tipoPariente = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        listarTipoParentesco();

        $scope.parienteSel = {
            parId: pariente.parId,
            perId: personaId,
            tipoParienteId: pariente.tipoParienteId,
            parentesco: "",
            parApePat: pariente.parApePat,
            parApeMat: pariente.parApeMat,
            parNom: pariente.parNom,
            parDni: pariente.parDni,
            parFecNac: convertirFechaStr(pariente.parFecNac),
            parSex: pariente.parSex,
            parNum1: pariente.parNum1,
            parNum2: pariente.parNum2,
            parFij: pariente.parFij,
            parEmail: pariente.parEmail,
            retJud: pariente.retJud,
            parEda: pariente.parEda
        };

        console.log($scope.parienteSel);

        $scope.actualizarPariente = function () {
            var request = crud.crearRequest('familiares', 1, 'actualizarFamiliar');
            request.setData($scope.parienteSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.parEda = calcularEdad(response.data.parFecNac);
                    response.data.parentesco = buscarContenido($scope.tipoPariente, 'tpaId', 'tpaDes', response.data.tipoParienteId);
                    response.data.i = pariente.i;
                    //response.data.fecNac = new Date(response.data.fecNac);
                    $rootScope.settingModParientes.dataset[pariente.i] = response.data;
                    $rootScope.tablaModParientes.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
seApp.controller('editarFormacionEducativaCtrl', ['$rootScope', '$scope', '$element', 'title', 'formacionEducativa', 'personaId', 'close', 'crud', 'tipFors', 'niveles', 'tipDocs', 'paises', 'modal', function ($rootScope, $scope, $element, title, formacionEducativa, personaId, close, crud, tipFors, niveles, tipDocs, paises, modal) {
        $scope.title = title;
        $scope.tipFors = tipFors;
        $scope.tipDocs = tipDocs;
        $scope.paises = paises;
        $scope.niveles = niveles;
        //variable que servira para registrar una nueva formacion academic
        var datos = [];
        for (var i in niveles)
        {
            if (niveles[i]["pad"] == formacionEducativa.tipForId) {
                datos.push(niveles[i]);
            }
        }
        $scope.comNiveles = datos;

        $scope.forEduSel = {
            i: formacionEducativa.i,
            forEduId: formacionEducativa.forEduId,
            tipForId: formacionEducativa.tipForId,
            tipFor: "",
            nivAcaId: formacionEducativa.nivAcaId,
            nivAca: "",
            tipDocId: formacionEducativa.tipDocId,
            tipDoc: "",
            numDoc: formacionEducativa.numDoc,
            fecDoc: convertirFechaStr(formacionEducativa.fecDoc),
            esp: formacionEducativa.esp,
            estCon: formacionEducativa.estCon == "" ? false : formacionEducativa.estCon,
            fecIni: convertirFechaStr(formacionEducativa.fecIni),
            fecTer: convertirFechaStr(formacionEducativa.fecTer),
            cenEst: formacionEducativa.cenEst,
            paisId: formacionEducativa.paisId,
            pais: formacionEducativa.pais
        };

        console.log($scope.forEduSel);

        $scope.loadTipoFormacion = function () {
            var form = $scope.forEduSel.tipForId;
            var datos = [];
            $scope.comNiveles = {};
            for (var i in niveles)
            {
                if (niveles[i]["pad"] == form) {
                    datos.push(niveles[i]);
                }
            }
            $scope.comNiveles = datos;
        }

        $scope.actualizarFormacionEducativa = function () {
            var request = crud.crearRequest('formacion_educativa', 1, 'actualizarFormacionEducativa');
            request.setData($scope.forEduSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.estConDes = response.data.estCon ? "Si" : "No";
                    response.data.nivAca = buscarContenido($scope.niveles, 'id', 'nom', response.data.nivAcaId);
                    response.data.tipFor = buscarContenido($scope.tipFors, 'id', 'nom', response.data.tipForId);
                    response.data.pais = response.data.paisId === 0 ? "No registrado" : buscarContenido($scope.paises, 'nacId', 'nacNom', response.data.paisId);
                    response.data.i = formacionEducativa.i;
                    $rootScope.settingModForEdu.dataset[formacionEducativa.i] = response.data;
                    $rootScope.tablaModForEdu.reload();

                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarColegiaturaCtrl', ['$rootScope', '$scope', '$element', 'title', 'colegiatura', 'personaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, colegiatura, personaId, close, crud, modal) {
        $scope.title = title;

        //variable que servira para registrar una nueva formacion academica
        $scope.colegiaturaSel = {
            i: colegiatura.i,
            colId: colegiatura.colId,
            nomColPro: colegiatura.nomColPro,
            numRegCol: colegiatura.numRegCol,
            conReg: colegiatura.conReg
        };

        $scope.actualizarColegiatura = function () {
            var request = crud.crearRequest('colegiatura', 1, 'actualizarColegiatura');
            request.setData($scope.colegiaturaSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.conRegDes = response.data.conReg ? "Habilitado" : "No Habilitado";
                    response.data.i = colegiatura.i;
                    $rootScope.settingModColegiaturas.dataset[ colegiatura.i] = response.data;
                    $rootScope.tablaModColegiaturas.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarEstudioComplementarioCtrl', ['$rootScope', '$scope', '$element', 'title', 'estudioComplementario', 'personaId', 'close', 'crud', 'tiposEstCom', 'nivelesEstCom', 'modal', function ($rootScope, $scope, $element, title, estudioComplementario, personaId, close, crud, tiposEstCom, nivelesEstCom, modal) {
        $scope.title = title;

        //variable que servira para registrar una nueva formacion academica
        $scope.estComSel = {
            i: estudioComplementario.i,
            estComId: estudioComplementario.estComId,
            tipId: estudioComplementario.tipId,
            des: estudioComplementario.des,
            nivId: estudioComplementario.nivId,
            insCer: estudioComplementario.insCer,
            tipPar: estudioComplementario.tipPar,
            fecIni: convertirFechaStr(estudioComplementario.fecIni),
            fecTer: convertirFechaStr(estudioComplementario.fecTer),
            horLec: estudioComplementario.horLec,
            pais: estudioComplementario.pais
        };

        $scope.tiposEstCom = tiposEstCom;
        $scope.nivelesEstCom = nivelesEstCom;

        $scope.actualizarEstudioComplementario = function () {
            var request = crud.crearRequest('estudio_complementario', 1, 'actualizarEstudioComplementario');
            request.setData($scope.estComSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tip = response.data.tipId === 0 ? "No registrado" : buscarContenido($scope.tiposEstCom, 'id', 'nom', response.data.tipId);
                    response.data.niv = response.data.nivId === 0 ? "No registrado" : buscarContenido($scope.nivelesEstCom, 'id', 'nom', response.data.nivId);
                    response.data.i = estudioComplementario.i;
                    $rootScope.settingModEstCom.dataset[ estudioComplementario.i] = response.data;
                    $rootScope.tablaModEstCom.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarEstudioPostgradoCtrl', ['$rootScope', '$scope', '$element', 'title', 'estudioPostgrado', 'personaId', 'close', 'crud', 'modal', 'tipDocs', function ($rootScope, $scope, $element, title, estudioPostgrado, personaId, close, crud, modal, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.estPosSel = {
            i: estudioPostgrado.i,
            estPosId: estudioPostgrado.estPosId,
            perId: personaId,
            tip: estudioPostgrado.tip,
            numDoc: estudioPostgrado.numDoc,
            fecDoc: new Date(estudioPostgrado.fecDoc),
            tipDocId: estudioPostgrado.tipDocId,
            tipDoc: "",
            fecIniEst: new Date(estudioPostgrado.fecIniEst),
            fecTerEst: new Date(estudioPostgrado.fecTerEst),
            ins: estudioPostgrado.ins,
            pais: estudioPostgrado.pais
        };

        $scope.tiposEstPos = [
            {id: "1", title: "Maestria"},
            {id: "2", title: "Doctorado"},
            {id: "3", title: "Licenciatura"},
            {id: "4", title: "Otros"}
        ];

        $scope.actualizarEstudioPostgrado = function () {
            var request = crud.crearRequest('estudio_postgrado', 1, 'actualizarEstudioPostgrado');
            request.setData($scope.estPosSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDoc = buscarContenido($scope.tipDocs, 'id', 'nom', response.data.tipDocId);
                    response.data.tipDes = buscarContenido($scope.tiposEstPos, 'id', 'title', response.data.tip);
                    response.data.i = estudioPostgrado.i;
                    $rootScope.settingModEstPos.dataset[estudioPostgrado.i] = response.data;
                    $rootScope.tablaModEstPos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarExposicionCtrl', ['$rootScope', '$scope', '$element', 'title', 'exposicion', 'personaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, exposicion, personaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.expPonSel = {
            i: exposicion.i,
            expId: exposicion.expId,
            perId: personaId,
            des: exposicion.des,
            insOrg: exposicion.insOrg,
            tipPar: exposicion.tipPar,
            fecIni: convertirFechaStr(exposicion.fecIni),
            fecTer: convertirFechaStr(exposicion.fecTer),
            horLec: exposicion.horLec
        };

        $scope.actualizarExposicion = function () {
            var request = crud.crearRequest('exposicion_ponencia', 1, 'actualizarExposicion');
            request.setData($scope.expPonSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = exposicion.i;
                    $rootScope.settingModExpPon.dataset[exposicion.i] = response.data;
                    $rootScope.tablaModExpPon.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarPublicacionCtrl', ['$rootScope', '$scope', '$element', 'title', 'publicacion', 'personaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, publicacion, personaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        console.log(convertirFechaStr(publicacion.fecPub));
        $scope.publicacionSel = {
            i: publicacion.i,
            pubId: publicacion.pubId,
            nomEdi: publicacion.nomEdi,
            tipPub: publicacion.tipPub,
            titPub: publicacion.titPub,
            graPar: publicacion.graPar,
            lug: publicacion.lug,
            fecPub: convertirFechaStr(publicacion.fecPub),
            numReg: publicacion.numReg
        };

        $scope.actualizarPublicacion = function () {
            var request = crud.crearRequest('publicacion', 1, 'actualizarPublicacion');
            request.setData($scope.publicacionSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = publicacion.i;
                    $rootScope.settingModPublicaciones.dataset[publicacion.i] = response.data;
                    $rootScope.tablaModPublicaciones.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarCapacitacionCtrl', ['$rootScope', '$scope', '$element', 'title', 'capacitacion', 'personaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, capacitacion, personaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.capacitacionSel = {
            i: capacitacion.i,
            capId: capacitacion.capId,
            nom: capacitacion.nom,
            tip: capacitacion.tip,
            fec: convertirFechaStr(capacitacion.fec),
            cal: capacitacion.cal,
            lug: capacitacion.lug
        };

        $scope.actualizarCapacitacion = function () {
            var request = crud.crearRequest('capacitacion', 1, 'actualizarCapacitacion');
            request.setData($scope.capacitacionSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = capacitacion.i;
                    $rootScope.settingModCapacitaciones.dataset[capacitacion.i] = response.data;
                    $rootScope.tablaModCapacitaciones.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarReconocimientoCtrl', ['$rootScope', '$scope', '$element', 'title', 'reconocimiento', 'personaId', 'close', 'crud', 'modal', 'tipDocs', function ($rootScope, $scope, $element, title, reconocimiento, personaId, close, crud, modal, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.reconocimientoSel = {
            i: reconocimiento.i,
            recId: reconocimiento.recId,
            mot: reconocimiento.mot,
            desMot: reconocimiento.desMot,
            numDoc: reconocimiento.numDoc,
            fecDoc: convertirFechaStr(reconocimiento.fecDoc),
            tipDocId: reconocimiento.tipDocId,
            entEmi: reconocimiento.entEmi
        };
        $scope.motivos = [
            {id: "1", title: "Mérito"},
            {id: "2", title: "Felicitación"},
            {id: "3", title: "25 años de servicio"},
            {id: "4", title: "30 años de servicio"},
            {id: "5", title: "Luto y sepelio"},
            {id: "6", title: "Otros"}
        ];

        $scope.actualizarReconocimiento = function () {
            var request = crud.crearRequest('reconocimientos', 1, 'actualizarReconocimiento');
            request.setData($scope.reconocimientoSel);
            crud.actualizar("/sistema_escalafon/", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDoc = buscarContenido($scope.tipDocs, 'id', 'nom', response.data.tipDocId);
                    response.data.motDes = buscarContenido($scope.motivos, 'id', 'title', response.data.mot);
                    response.data.i = reconocimiento.i;
                    $rootScope.settingModReconocimientos.dataset[reconocimiento.i] = response.data;
                    $rootScope.tablaModReconocimientos.reload();
                }
            }, function (data) {
                console.info(data);
            });

        };

    }]);
seApp.controller('editarDemeritoCtrl', ['$rootScope', '$scope', '$element', 'title', 'demerito', 'personaId', 'close', 'crud', 'modal', 'tipDocs', function ($rootScope, $scope, $element, title, demerito, personaId, close, crud, modal, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.demeritoSel = {
            i: demerito.i,
            demId: demerito.demId,
            entEmi: demerito.entEmi,
            numDoc: demerito.numDoc,
            fecDoc: convertirFechaStr(demerito.fecDoc),
            tipDocId: demerito.tipDocId,
            sep: demerito.sep,
            fecIni: convertirFechaStr(demerito.fecIni),
            fecFin: convertirFechaStr(demerito.fecFin),
            mot: demerito.mot
        };


        $scope.actualizarDemerito = function () {
            var request = crud.crearRequest('demeritos', 1, 'actualizarDemerito');
            request.setData($scope.demeritoSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDoc = buscarContenido($scope.tipDocs, 'id', 'nom', response.data.tipDocId);
                    response.data.sepDes = response.data.sep ? "Si" : "No";
                    response.data.i = demerito.i;
                    $rootScope.settingModDemeritos.dataset[demerito.i] = response.data;
                    $rootScope.tablaModDemeritos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
//Para ver el legajo segun pestañas del trabajador????
seApp.controller('verLegajoPersonalCtrl', ['$routeParams', '$rootScope', '$scope', 'crud', 'NgTableParams', 'ModalService', 'modal', function ($routeParams, $rootScope, $scope, crud, NgTableParams, ModalService, modal) {
        var datos = JSON.parse(atob($routeParams.ficha));

        var categorias = [
            {idCat: "1", idSubCat: "1", subCatDes: "Datos personales"},
            {idCat: "1", idSubCat: "2", subCatDes: "Nacimiento"},
            {idCat: "1", idSubCat: "3", subCatDes: "Salud"},
            {idCat: "1", idSubCat: "4", subCatDes: "Direccion"},
            {idCat: "2", idSubCat: "1", subCatDes: "Datos familiares"},
            {idCat: "3", idSubCat: "1", subCatDes: "Formacion educativa"},
            {idCat: "3", idSubCat: "2", subCatDes: "Colegiatura"},
            {idCat: "3", idSubCat: "3", subCatDes: "Estudios complementarios"},
            {idCat: "3", idSubCat: "4", subCatDes: "Estudios postgrado"},
            {idCat: "3", idSubCat: "5", subCatDes: "Exposicion y/o Ponencias"},
            {idCat: "3", idSubCat: "6", subCatDes: "Publicaciones"},
            {idCat: "4", idSubCat: "1", subCatDes: "Desplazamientos"},
            {idCat: "4", idSubCat: "2", subCatDes: "Ascensos"},
            {idCat: "4", idSubCat: "3", subCatDes: "Capacitaciones"},
            {idCat: "5", idSubCat: "1", subCatDes: "Reconocimientos"},
            {idCat: "6", idSubCat: "1", subCatDes: "Demeritos"},
            {idCat: "7", idSubCat: "1", subCatDes: "Otros"}
        ];

        $rootScope.paramsLegDatosPersonales = {count: 10};
        $rootScope.settingLegDatosPersonales = {counts: []};
        $rootScope.tablaLegDatosPersonales = new NgTableParams($rootScope.paramsLegDatosPersonales, $rootScope.settingLegDatosPersonales);

        $rootScope.paramsLegDatosFamiliares = {count: 10};
        $rootScope.settingLegDatosFamiliares = {counts: []};
        $rootScope.tablaLegDatosFamiliares = new NgTableParams($rootScope.paramsLegDatosFamiliares, $rootScope.settingLegDatosFamiliares);

        $rootScope.paramsLegDatosAcademicos = {count: 10};
        $rootScope.settingLegDatosAcademicos = {counts: []};
        $rootScope.tablaLegDatosAcademicos = new NgTableParams($rootScope.paramsLegDatosAcademicos, $rootScope.settingLegDatosAcademicos);

        $rootScope.paramsLegExperienciaLaboral = {count: 10};
        $rootScope.settingLegExperienciaLaboral = {counts: []};
        $rootScope.tablaLegExperienciaLaboral = new NgTableParams($rootScope.paramsLegExperienciaLaboral, $rootScope.settingLegExperienciaLaboral);

        $rootScope.paramsLegReconocimientos = {count: 10};
        $rootScope.settingLegReconocimientos = {counts: []};
        $rootScope.tablaLegReconocimientos = new NgTableParams($rootScope.paramsLegReconocimientos, $rootScope.settingLegReconocimientos);

        $rootScope.paramsLegDemeritos = {count: 10};
        $rootScope.settingLegDemeritos = {counts: []};
        $rootScope.tablaLegDemeritos = new NgTableParams($rootScope.paramsLegDemeritos, $rootScope.settingLegDemeritos);

        $rootScope.paramsLegOtros = {count: 10};
        $rootScope.settingLegOtros = {counts: []};
        $rootScope.tablaLegOtros = new NgTableParams($rootScope.paramsLegOtros, $rootScope.settingLegOtros);

        var legDatosPersonales = [];
        var legDatosFamiliares = [];
        var legDatosAcademicos = [];
        var legExperienciaLaboral = [];
        var legReconocimientos = [];
        var legDemeritos = [];
        var legOtros = [];

        listarLegajos();
        function listarLegajos() {
            //preparamos un objeto request
            var request = crud.crearRequest('legajo_personal', 1, 'listarLegajos');
            request.setData(datos);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    var subCategorias = buscarObjetos(categorias, 'idCat', item.catLeg);
                    item.subCatDes = buscarContenido(subCategorias, 'idSubCat', 'subCatDes', item.subCat);

                    switch (item.catLeg) {
                        case '1':
                            legDatosPersonales.push(item);
                            break;
                        case '2':
                            legDatosFamiliares.push(item);
                            break;
                        case '3':
                            legDatosAcademicos.push(item);
                            break;
                        case '4':
                            legExperienciaLaboral.push(item);
                            break;
                        case '5':
                            legReconocimientos.push(item);
                            break;
                        case '6':
                            legDemeritos.push(item);
                            break;
                        default:
                            legOtros.push(item);
                    }
                });

                $rootScope.settingLegDatosPersonales.dataset = legDatosPersonales;
                iniciarPosiciones($rootScope.settingLegDatosPersonales.dataset);
                $rootScope.tablaLegDatosPersonales.settings($rootScope.settingLegDatosPersonales);

                $rootScope.settingLegDatosFamiliares.dataset = legDatosFamiliares;
                iniciarPosiciones($rootScope.settingLegDatosFamiliares.dataset);
                $rootScope.tablaLegDatosFamiliares.settings($rootScope.settingLegDatosFamiliares);

                $rootScope.settingLegDatosAcademicos.dataset = legDatosAcademicos;
                iniciarPosiciones($scope.settingLegDatosAcademicos.dataset);
                $rootScope.tablaLegDatosAcademicos.settings($rootScope.settingLegDatosAcademicos);

                $scope.settingLegExperienciaLaboral.dataset = legExperienciaLaboral;
                iniciarPosiciones($rootScope.settingLegExperienciaLaboral.dataset);
                $rootScope.tablaLegExperienciaLaboral.settings($rootScope.settingLegExperienciaLaboral);

                $rootScope.settingLegReconocimientos.dataset = legReconocimientos;
                iniciarPosiciones($rootScope.settingLegReconocimientos.dataset);
                $rootScope.tablaLegReconocimientos.settings($rootScope.settingLegReconocimientos);

                $rootScope.settingLegDemeritos.dataset = legDemeritos;
                iniciarPosiciones($rootScope.settingLegDatosPersonales.dataset);
                $rootScope.tablaLegDemeritos.settings($rootScope.settingLegDemeritos);

                $rootScope.settingLegOtros.dataset = legOtros;
                iniciarPosiciones($rootScope.settingLegOtros.dataset);
                $rootScope.tablaLegOtros.settings($rootScope.settingLegOtros);


            }, function (data) {
                console.info(data);
            });

        }
        ;

        $scope.showNuevoLegajo = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarLegajoPersonal.html",
                controller: "agregarNuevoLegajoCtrl",
                inputs: {
                    title: "Nuevo Legajo",
                    fichaEscalafonariaId: datos.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.prepararEditarLegajo = function (lp) {
            var legajoSel = JSON.parse(JSON.stringify(lp));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarLegajo.html",
                controller: "editarLegajoCtrl",
                inputs: {
                    title: "Editar datos del legajo",
                    legajo: legajoSel
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.eliminarLegajo = function (l) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el legajo?", function () {
                var request = crud.crearRequest('legajo_personal', 1, 'eliminarLegajo');
                request.setData({legId: l.legId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaLegOtros.settings().dataset, function (item) {
                            return l === item;
                        });
                        $rootScope.tablaLegOtros.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

    }]);
seApp.controller('agregarNuevoLegajoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente
        $scope.nuevoLegajo = {
            ficEscId: fichaEscalafonariaId,
            nomDoc: "",
            archivo: "",
            des: "",
            url: "",
            catLeg: "7",
            subCat: "1",
            codAspOri: "0"
        };
        $scope.title = title;


        $scope.agregarLegajo = function () {
            var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
            request.setData($scope.nuevoLegajo);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingLegOtros.dataset, response.data);
                    $rootScope.tablaLegOtros.reload();

                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
seApp.controller('editarLegajoCtrl', ['$rootScope', '$scope', '$element', 'title', 'legajo', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, legajo, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.legajoSel = {
            i: legajo.i,
            ficEscId: legajo.ficEscId,
            legId: legajo.legId,
            nomDoc: legajo.nomDoc,
            archivo: legajo.archivo,
            des: legajo.des,
            url: legajo.url,
            catLeg: legajo.catleg,
            subCat: legajo.subCat,
            codAspOri: legajo.codAspOri
        };


        $scope.actualizarLegajo = function () {
            var request = crud.crearRequest('legajo_personal', 1, 'actualizarLegajo');
            request.setData($scope.legajoSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = legajo.i;
                    $rootScope.settingLegOtros.dataset[legajo.i] = response.data;
                    $rootScope.tablaLegOtros.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };


    }]);
seApp.controller('agregarNuevoParienteCtrl', ['$scope', '$rootScope', '$element', 'title', 'personaId', 'sexo', 'actDni', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, personaId, sexo, actDni, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente
        $scope.nuevoPariente = {perId: personaId, existe: true, tipoPariente: "", parApePat: "", parApeMat: "", parNom: "", parDni: "", parFecNac: "", parSex: "", parNum1: "", parNum2: "", parFij: "", parEmail: "", retJud: 0};
        //$scope.nuevoLegajoPar = {ficEscId:fichaEscalafonariaId ,nom:"", archivo:"", des:"DNI-Pariente", url:"", catLeg:"2", subCat:"1", codAspOri:""};
        $scope.title = title;
        $scope.sexoPariente = sexo;
        $scope.tipoParentesco = [];

        listarTipoParentesco();
        function listarTipoParentesco() {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarTipoParentesco');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.tipoParentesco = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;

        $scope.boolCampos = true;
        $scope.estadoPar = true;
        $scope.mostrar = true;

        $scope.agregarPariente = function () {
            var request = crud.crearRequest('familiares', 1, 'agregarPariente');
            request.setData($scope.nuevoPariente);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.parEda = calcularEdad(response.data.parFecNac);
                    response.data.parentesco = buscarContenido($scope.tipoParentesco, 'tpaId', 'tpaDes', response.data.tipoParienteId);
                    insertarElemento($rootScope.settingModParientes.dataset, response.data);
                    $rootScope.tablaModParientes.reload();
                }
            }, function (response) {
                console.info(response);
            });
        };

        $scope.reiniciarDatosP = function () {
            $scope.mostrar = true;
            $scope.boolCampos = true;
            $scope.estadoPar = true;
        };

        //BuscarPersona
        $scope.buscarPersona = function () {
            if ($scope.nuevoPariente.parDni === '') {
                modal.mensaje("ALERTA", "Ingrese DNI");
                return;
            }
            if ($scope.nuevoPariente.parDni === actDni) {
                modal.mensaje("ALERTA", "Usted no puede agregarse como pariente");
                return;
            }
            var request = crud.crearRequest('familiares', 1, 'buscarPersonaPariente');
            request.setData({
                dni: $scope.nuevoPariente.parDni,
                perId: personaId
            });
            crud.listar("/sistema_escalafon", request, function (data) {
                modal.mensaje("CONFIRMACION", data.responseMsg);
                console.log(data);
                $scope.mostrar = false;
                if (!data.responseSta) {//No existe la Persona 
                    $scope.boolCampos = false;
                    $scope.nuevoPariente.parDni = request.data.dni;
                    $scope.nuevoPariente.perId = request.data.perId;
                    $scope.nuevoPariente.tipoPariente = "";
                    $scope.nuevoPariente.parApePat = "";
                    $scope.nuevoPariente.parApeMat = "";
                    $scope.nuevoPariente.parNom = "";
                    $scope.nuevoPariente.parSex = "";
                    $scope.nuevoPariente.parFij = "";
                    $scope.nuevoPariente.parNum1 = "";
                    $scope.nuevoPariente.parNum2 = "";
                    $scope.nuevoPariente.parEmail = "";
                    $scope.nuevoPariente.parFecNac = "";
                    $scope.nuevoPariente.retJud = "";
                    $scope.nuevoPariente.estado = 'A';
                    $scope.nuevoPariente.caso = 0;
                    $scope.estadoPar = false;
                } else {
                    if (data.data.persona.caso === 1)//Solo agregar Parentesco
                    {
                        $scope.boolCampos = true;
                        $scope.nuevoPariente = {
                            parId: data.data.persona.parId,
                            tipoPariente: "",
                            parApePat: data.data.persona.apePat,
                            parApeMat: data.data.persona.apeMat,
                            parNom: data.data.persona.nom,
                            parSex: data.data.persona.sex,
                            parFij: data.data.persona.fij,
                            parNum1: data.data.persona.num1,
                            parNum2: data.data.persona.num2,
                            parEmail: data.data.persona.email,
                            parFecNac: data.data.persona.fecNac === "" ? "" : new Date(data.data.persona.fecNac),
                            retJud: "",
                            caso: data.data.persona.caso
                        };
                        $scope.nuevoPariente.parDni = request.data.dni;//?????
                        $scope.nuevoPariente.perId = request.data.perId;
                        $scope.estadoPar = false;
                    }
                    if (data.data.persona.caso === 2)//Ya tiene parentesco con el DNI ingresado
                    {
                        $scope.boolCampos = true;
                        $scope.nuevoPariente = {
                            tipoPariente: data.data.persona.tipoPariente, //Recibir de backend
                            parApePat: data.data.persona.apePat,
                            parApeMat: data.data.persona.apeMat,
                            parNom: data.data.persona.nom,
                            parSex: data.data.persona.sex,
                            parFij: data.data.persona.fij,
                            parNum1: data.data.persona.num1,
                            parNum2: data.data.persona.num2,
                            parEmail: data.data.persona.email,
                            parFecNac: data.data.persona.fecNac === "" ? "" : new Date(data.data.persona.fecNac),
                            retJud: data.data.persona.retJud,
                            caso: data.data.persona.caso
                        };
                        $scope.nuevoPariente.parDni = request.data.dni;
                        $scope.estadoPar = true;
                    }
                }
            }, function (data) {
                console.info(data);
            });

        };

    }]);
seApp.controller('agregarNuevaFormacionEducativaCtrl', ['$scope', '$rootScope', '$element', 'title', 'personaId', 'close', 'crud', 'modal', 'tipFors', 'niveles', 'tipDocs', 'paises', 'ModalService', function ($scope, $rootScope, $element, title, personaId, close, crud, modal, tipFors, niveles, tipDocs, paises, ModalService) {
        $scope.title = title;
        $scope.tipFors = tipFors;
        $scope.tipDocs = tipDocs;
        $scope.paises = paises;
        $scope.niveles = niveles;

        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaForEdu = {perId: personaId, tipForId: 0, nivAcaId: 0, tipDocId: 0, numDoc: "", fecDoc: "", esp: "", estCon: false, fecIni: "", fecTer: "", cenEst: "", paisId: 0};
        //$scope.nuevoLegajoForEdu = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:" Titulo y/o Certificado", url:"", catLeg:"3", subCat:"1", codAspOri:""};

        $scope.loadTipoFormacion = function () {
            var form = $scope.nuevaForEdu.tipForId;
            var datos = [];
            $scope.comNiveles = {};
            for (var i in niveles)
            {
                if (niveles[i]["pad"] == form) {
                    datos.push(niveles[i]);
                }
            }
            $scope.comNiveles = datos;
        };

        $scope.agregarFormacionEducativa = function () {
            var request = crud.crearRequest('formacion_educativa', 1, 'agregarFormacionEducativa');
            request.setData($scope.nuevaForEdu);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipFor = buscarContenido($scope.tipFors, 'id', 'nom', response.data.tipForId);
                    response.data.nivAca = buscarContenido($scope.niveles, 'id', 'nom', response.data.nivAcaId);
                    response.data.estConDes = response.data.estCon ? "Si" : "No";
                    response.data.pais = response.data.paisId === 0 ? "No registrado" : buscarContenido($scope.paises, 'nacId', 'nacNom', response.data.paisId);
                    insertarElemento($rootScope.settingModForEdu.dataset, response.data);
                    $rootScope.tablaModForEdu.reload();

                    /*if($scope.nuevoLegajoForEdu.archivo){
                     $scope.nuevoLegajoForEdu.codAspOri = response.data.forEduId;
                     var request = crud.crearRequest('sistema_escalafon', 1, 'agregarLegajoPersonal');
                     request.setData($scope.nuevoLegajoForEdu);
                     crud.insertar("/sistema_escalafon", request, function (response) {
                     $scope.listarFormacionesEducativas();
                     modal.mensaje("CONFIRMACION", response.responseMsg);
                     }, function (data) {
                     console.info(data);
                     });
                     }*/
                }
            }, function (data) {
                console.info(data);
            });
        };


    }]);
seApp.controller('agregarNuevaColegiaturaCtrl', ['$scope', '$rootScope', '$element', 'title', 'personaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, personaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaColegiatura = {perId: personaId, nomColPro: "", numRegCol: "", conReg: false};
        //$scope.nuevoLegajoCol = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", url:"", des:"Constancia", catLeg:"3", subCat:"2", codAspOri:""};

        $scope.agregarColegiatura = function () {
            var request = crud.crearRequest('colegiatura', 1, 'agregarColegiatura');
            request.setData($scope.nuevaColegiatura);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.conRegDes = response.data.conReg ? "Hablitado" : "No Habilitado";
                    insertarElemento($rootScope.settingModColegiaturas.dataset, response.data);
                    $rootScope.tablaModColegiaturas.reload();
                    /*
                     if($scope.nuevoLegajoCol.archivo){
                     $scope.nuevoLegajoCol.codAspOri = response.data.colId;
                     
                     var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                     request.setData($scope.nuevoLegajoCol);
                     crud.insertar("/sistema_escalafon", request, function (response) {
                     
                     modal.mensaje("CONFIRMACION", response.responseMsg);
                     }, function (data) {
                     console.info(data);
                     });
                     }*/

                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevoEstudioComplementarioCtrl', ['$scope', '$rootScope', '$element', 'title', 'personaId', 'close', 'crud', 'modal', 'tiposEstCom', 'nivelesEstCom', 'ModalService', function ($scope, $rootScope, $element, title, personaId, close, crud, modal, tiposEstCom, nivelesEstCom, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoEstCom = {perId: personaId, tipId: 0, des: "", nivId: 0, insCer: "", tipPar: "", fecIni: "", fecTer: "", horLec: 0, pais: ""};
        //$scope.nuevoLegajoEC = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Certificado y/o Titulo", url:"", catLeg:"3", subCat:"3", codAspOri:""};
        $scope.tiposEstCom = tiposEstCom;
        $scope.nivelesEstCom = nivelesEstCom;

        $scope.agregarEstudioComplementario = function () {
            var request = crud.crearRequest('estudio_complementario', 1, 'agregarEstudioComplementario');
            request.setData($scope.nuevoEstCom);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {

                    response.data.tip = response.data.tipId === 0 ? "No registrado" : buscarContenido($scope.tiposEstCom, 'id', 'nom', response.data.tipId);
                    response.data.niv = response.data.nivId === 0 ? "No registrado" : buscarContenido($scope.nivelesEstCom, 'id', 'nom', response.data.nivId);

                    insertarElemento($rootScope.settingModEstCom.dataset, response.data);
                    $rootScope.tablaModEstCom.reload();

                    /*if ($scope.nuevoLegajoEC.archivo){
                     $scope.nuevoLegajoEC.codAspOri = response.data.estComId;
                     var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                     request.setData($scope.nuevoLegajoEC);
                     crud.insertar("/sistema_escalafon", request, function (response) {
                     $scope.listarEstudiosComplementarios();
                     modal.mensaje("CONFIRMACION", response.responseMsg);
                     }, function (data) {
                     console.info(data);
                     });
                     }*/
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevoEstudioPostgradoCtrl', ['$scope', '$rootScope', '$element', 'title', 'personaId', 'close', 'crud', 'modal', 'ModalService', 'tipDocs', function ($scope, $rootScope, $element, title, personaId, close, crud, modal, ModalService, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoEstPos = {perId: personaId, tip: "", numDoc: "", fecDoc: "", tipDoc: "", des: "", niv: "", insCer: "", tipPar: "", fecIniEst: "", fecTerEst: "", horLec: "", pais: ""};
        //$scope.nuevoLegajoEP = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Titulo", url:"", catLeg:"3", subCat:"4", codAspOri: ""};
        $scope.tiposEstPos = [
            {id: "1", title: "Maestria"},
            {id: "2", title: "Doctorado"},
            {id: "3", title: "Licenciatura"},
            {id: "4", title: "Otros"}
        ];

        $scope.agregarEstudioPostgrado = function () {
            var request = crud.crearRequest('estudio_postgrado', 1, 'agregarEstudioPostgrado');
            request.setData($scope.nuevoEstPos);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //response.data.tipDoc = buscarContenido($scope.tipDocs, 'id', 'nom', response.data.tipDoc);
                    //console.log(response.data.tipDoc);
                    console.log(response.data);
                    response.data.tipDoc = buscarContenido($scope.tipDocs, 'id', 'nom', response.data.tipDocId);

                    var auxTip = buscarObjeto($scope.tiposEstPos, "id", response.data.tip);
                    response.data.tipDes = auxTip.title;
                    insertarElemento($rootScope.settingModEstPos.dataset, response.data);
                    $rootScope.tablaModEstPos.reload();
                    /*
                     if ($scope.nuevoLegajoEP.archivo){
                     $scope.nuevoLegajoEP.codAspOri = response.data.estPosId;
                     var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                     request.setData($scope.nuevoLegajoEP);
                     crud.insertar("/sistema_escalafon", request, function (response) {
                     $scope.listarEstudiosPostgrado();
                     modal.mensaje("CONFIRMACION", response.responseMsg);
                     }, function (data) {
                     console.info(data);
                     });
                     }*/
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevaExposicionCtrl', ['$scope', '$rootScope', '$element', 'title', 'personaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, personaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaExpPon = {perId: personaId, des: "", insOrg: "", tipPar: "", fecIni: "", fecTer: "", horLec: 0};
        //$scope.nuevoLegajoExp = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Certificado de participación", url:"", catLeg:"3", subCat:"5", codAspOri:""};

        $scope.agregarExposicion = function () {
            var request = crud.crearRequest('exposicion_ponencia', 1, 'agregarExposicion');
            request.setData($scope.nuevaExpPon);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingModExpPon.dataset, response.data);
                    $rootScope.tablaModExpPon.reload();
                    /*
                     if ($scope.nuevoLegajoExp.archivo){
                     $scope.nuevoLegajoExp.codAspOri = response.data.expId;
                     var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                     request.setData($scope.nuevoLegajoExp);
                     crud.insertar("/sistema_escalafon", request, function (response) {
                     $scope.listarExposiciones();
                     modal.mensaje("CONFIRMACION", response.responseMsg);
                     }, function (data) {
                     console.info(data);
                     });
                     }*/
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevaPublicacionCtrl', ['$scope', '$rootScope', '$element', 'title', 'personaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, personaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaPublicacion = {perId: personaId, nomEdi: "", tipPub: "", titPub: "", graPar: "", lug: "", fecPub: "", numReg: ""};
        //$scope.nuevoLegajoPub = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", url:"", des:"Constancia", catLeg:"3", subCat:"6", codAspOri:""};
        console.log($scope.nuevaPublicacion);
        $scope.agregarPublicacion = function () {
            var request = crud.crearRequest('publicacion', 1, 'agregarPublicacion');
            request.setData($scope.nuevaPublicacion);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingModPublicaciones.dataset, response.data);
                    $rootScope.tablaModPublicaciones.reload();
                    /*
                     if ($scope.nuevoLegajoPub.archivo){
                     $scope.nuevoLegajoPub.codAspOri = response.data.pubId;
                     
                     var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                     request.setData($scope.nuevoLegajoPub);
                     crud.insertar("/sistema_escalafon", request, function (response) {
                     $scope.listarPublicaciones();
                     modal.mensaje("CONFIRMACION", response.responseMsg);
                     }, function (data) {
                     console.info(data);
                     });
                     }*/
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevaCapacitacionCtrl', ['$scope', '$rootScope', '$element', 'title', 'personaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, personaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaCapacitacion = {perId: personaId, nom: "", tip: "", fec: "", cal: 0, lug: ""};
        //$scope.nuevoLegajoCap = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Certificado", url:"", catLeg:"4", subCat:"3", codAspOri:""};


        $scope.agregarCapacitacion = function () {
            var request = crud.crearRequest('capacitacion', 1, 'agregarCapacitacion');
            request.setData($scope.nuevaCapacitacion);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {

                    insertarElemento($rootScope.settingModCapacitaciones.dataset, response.data);
                    $rootScope.tablaModCapacitaciones.reload();

                    /*if ($scope.nuevoLegajoCap.archivo) {
                     $scope.nuevoLegajoCap.codAspOri = response.data.capId;
                     
                     var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                     request.setData($scope.nuevoLegajoCap);
                     crud.insertar("/sistema_escalafon", request, function (response) {
                     $scope.listarCapacitaciones();
                     modal.mensaje("CONFIRMACION", response.responseMsg);
                     }, function (data) {
                     console.info(data);
                     });
                     }*/
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevaLicenciaCtrl', ['$scope', '$rootScope', '$element', 'data', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, data, close, crud, modal, ModalService) {
        var dataInput = data;
        $scope.title = dataInput.title;
        $scope.fichaId = dataInput.fichaEscalafonariaId;
        $scope.tiposDocumentos = dataInput.tiposDocumentos;
        $scope.estadosProceso = dataInput.estadosProceso;
        $scope.tiposLicencias = dataInput.tiposLicencias;

        $scope.nuevaLicencia = {
            ficEscId: $scope.fichaId,
            entEmi: "",
            numInfMain: "",
            fecInfMain: "",
            tipDocIdMain: 0,
            car: "",
            estGocNum: 0,
            estGoc: false,
            fecIni: new Date(),
            fecTer: new Date(),
            mot: "",
            numDocSec: "",
            fecDocSec: "",
            fecRecInfMain: "",
            tipLicId: 0,
            tipRes: "",
            estProId: 0,
            orgiId: dataInput.orgiId,
            carId: dataInput.carId,
            catId: dataInput.catId,
            tipDocIdSec:0,
            tipDocIdMemo:0,
            numInfMemo:"",
            fecInfMemo:"",
            fecRecInMemo:"",
            diaPro:0,
            mesPro:0,
            anioPro:0
        };

        $scope.opcionesFechaIni = {
            maxDate: $scope.nuevaLicencia.fecTer,
            initDate: new Date()
        };
        $scope.opcionesFechaTer = {
            minDate: $scope.nuevaLicencia.fecIni,
            initDate: new Date()
        };
        
        $scope.calcularAnioProNuevoLicencia = function () {
        if($scope.nuevaLicencia.fecIni !== "" && $scope.nuevaLicencia.fecFin !== ""){
            var diaIni =$scope.nuevaLicencia.fecIni.getDate();
            var mesIni =$scope.nuevaLicencia.fecIni.getMonth()+1;
            var anioIni =$scope.nuevaLicencia.fecIni.getFullYear();
            
            var diaTer =$scope.nuevaLicencia.fecTer.getDate();
            var mesTer =$scope.nuevaLicencia.fecTer.getMonth()+1;
            var anioTer =$scope.nuevaLicencia.fecTer.getFullYear();
            var numDias360=0;
            
            if(anioTer===anioIni){

                if(mesTer===mesIni){

                    numDias360=diaTer-diaIni;

                }else{
                    if(diaTer===diaIni){
                        numDias360=(mesTer-mesIni)*30;
                    }
                    if(diaTer>diaIni){
                        numDias360=(mesTer-mesIni)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360+(mesTer-mesIni)*30-(diaIni-diaTer);
                    }
                }
                    
            }
            
            if(anioTer>anioIni){
                if(mesTer===mesIni){
                    if(diaTer===diaIni){
                        numDias360=(anioTer-anioIni)*360;
                    }
                    if(diaTer>diaIni){
                        numDias360=(anioTer-anioIni)*360+(diaTer-diaIni);
                    }
                    if(diaIni>diaTer){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=(anioTer-anioIni)*360-(diaTer-diaIni);
                    }
                }
                
                if(mesTer>mesIni){
                    numDias360=(anioTer-anioIni)*360;
                    if(diaTer===diaIni){
                        numDias360=numDias360+(mesTer-mesIni)*30;
                    }
                    if(diaTer>diaIni){
                        numDias360=numDias360+(mesTer-mesIni)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        var numD=0;
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            for(var i=mesIni; i<=mesTer;i++){
                                if(mesTer-mesIni===1 && diaIni-diaTer>0){
                                    if(mesIni===2){
                                        numD=numD+(29-(diaIni-diaTer));
                                    }else{
                                        numD=numD+(30-(diaIni-diaTer));
                                    }    
                                }else{
                                    numD=numD+30;
                                }
                                
                            }
                        }else{
                            for(var i=mesIni; i<=mesTer;i++){
                                if(mesTer-mesIni===1 && diaIni-diaTer>0){
                                    if(mesIni===2){
                                        numD=numD+(28-(diaIni-diaTer));
                                    }else{
                                        numD=numD+(30-(diaIni-diaTer));
                                    }    
                                }else{
                                    numD=numD+30;
                                }
                                
                            }
                        }
                        
                        numDias360=numDias360+numD;
                    }
                    
                    
                }
                
                if(mesTer<mesIni){
                    numDias360=(anioTer-anioIni)*360;
                    if(diaTer===diaIni){
                        numDias360=numDias360-(mesIni-mesTer)*30;
                    }
                    if(diaTer>diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360-(mesIni-mesTer)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360-(mesIni-mesTer)*30-(diaIni-diaTer);
                    }
                }
                    
            }
            
            var diaTran=numDias360%30;
            var mesTran=Math.trunc(numDias360/30)%12;
            var anioTran=Math.trunc(Math.trunc(numDias360/30)/12);
            
        }
            $scope.nuevaLicencia.diaPro= diaTran;
            $scope.nuevaLicencia.mesPro= mesTran;
            $scope.nuevaLicencia.anioPro= anioTran;
            
        };

        $scope.agregarLicencia = function () {
            var request = crud.crearRequest('licencia', 1, 'agregarLicencia');
            console.log("newlic: " + JSON.stringify($scope.nuevaLicencia));
            request.setData($scope.nuevaLicencia);
            crud.insertar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    response.data.estGocDes = response.data.estGoc ? "Con Goce" : "Sin Goce";
                    var datosFechaTer = response.data.fecTer.split("-");
                    var datosFechaIni = response.data.fecIni.split("-");
                                        
                    var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                    var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    
                    response.data.fecIni = fecIniFor;
                    response.data.fecTer = fecTerFor;
                    
                    
                    if(response.data.fecDocSec!==""){
                        var datosFechaInf = response.data.fecDocSec.split("-");
                        var fecInfFor = new Date(parseInt(datosFechaInf[2]), parseInt(datosFechaInf[1]) - 1, parseInt(datosFechaInf[0]));
                        response.data.fecDocSec = fecInfFor;
                    }
                    if(response.data.fecRecInfMain!==""){
                        var datosFechaRecInf = response.data.fecRecInfMain.split("-");
                        var fecRecInfFor = new Date(parseInt(datosFechaRecInf[2]), parseInt(datosFechaRecInf[1]) - 1, parseInt(datosFechaRecInf[0]));
                        response.data.fecRecInfMain = fecRecInfFor;
                    
                    }
                    if(response.data.fecInfMain!==""){
                        var datosFechaDoc = response.data.fecInfMain.split("-");
                        var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));
                        response.data.fecInfMain = fecDocFor;
                    
                    }
                    if(response.data.fecInfMemo!==""){
                        var datosFechaMemo=response.data.fecInfMemo.split("-");
                        var fecMemFor=new Date(parseInt(datosFechaMemo[2]), parseInt(datosFechaMemo[1]) - 1, parseInt(datosFechaMemo[0]));
                        response.data.fecInfMemo = fecMemFor;
                                            
                    }
                    if(response.data.fecRecInMemo!==""){                        
                        var datosFechaReiMemo=response.data.fecRecInMemo.split("-");
                        var fecMemReiFor=new Date(parseInt(datosFechaReiMemo[2]), parseInt(datosFechaReiMemo[1]) - 1, parseInt(datosFechaReiMemo[0]));
                        response.data.fecRecInMemo = fecMemReiFor;
                    }                         
  
                    response.data.dias = (fecTerFor - fecIniFor) / (1000 * 60 * 60 * 24) + 1;
                        
                    response.data.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', response.data.tipDocIdMain);
                    response.data.estProDes = buscarContenido($scope.estadosProceso, 'id', 'nom', response.data.estProId);
                    response.data.tipLicDes = buscarContenido($scope.tiposLicencias, 'id', 'nom', response.data.tipLicId);
                    response.data.i = $rootScope.settingModLicencias.dataset.length===undefined?0:$rootScope.settingModLicencias.dataset.length;
                    
                    $rootScope.settingModLicencias.dataset.push(response.data);
                    $rootScope.tablaModLicencias.sorting({licId:'asc'});
                    $rootScope.tablaModLicencias.page(1);
                    $rootScope.tablaModLicencias.reload();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                }
            }, function (error) {
                console.info(error);
            });
        };

    }]);
seApp.controller('agregarNuevaVacacionCtrl', ['$scope', '$rootScope', '$element', 'data', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, data, close, crud, modal, ModalService) {
        var dataInput = data;
        console.log("DATA INPUT");
        console.log(dataInput);
        $scope.title = dataInput.title;
        $scope.fichaId = dataInput.fichaEscalafonariaId;
        $scope.tiposDocumentos = dataInput.tiposDocumentos;
        $scope.estadosProceso = dataInput.estadosProceso;

        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaVacacion = {
            ficEscId: $scope.fichaId,
            tipDocId: 0,
            numDoc: "",
            fecDoc: "",
            car: "",
            estGocNum: 0,
            estGoc: false,
            fecIni: new Date(),
            fecTer: new Date(),
            mot: "",
            numInf: "",
            fecInf: "",
            fecRecInf: "",
            tip: "",
            estProId: 0,
            orgiId: dataInput.orgiId,
            carId: dataInput.carId,
            catId: dataInput.catId
        };
        
        $scope.opcionesFechaIni = {
            maxDate: $scope.nuevaVacacion.fecTer,
            initDate: new Date()
        };
        $scope.opcionesFechaTer = {
            minDate: $scope.nuevaVacacion.fecIni,
            initDate: new Date()
        };

        $scope.agregarVacacion = function () {
            var request = crud.crearRequest('vacacion', 1, 'agregarVacacion');
            request.setData($scope.nuevaVacacion);
            console.log($scope.nuevaVacacion);
            crud.insertar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.estGocDes = response.data.estGoc ? "Con Goce" : "Sin Goce";
                    var datosFechaTer = response.data.fecTer.split("-");
                    var datosFechaIni = response.data.fecIni.split("-");
                    var datosFechaInf = response.data.fecInf.split("-");
                    var datosFechaRecInf = response.data.fecRecInf.split("-");
                    var datosFechaDoc = response.data.fecDoc.split("-");

                    var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                    var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    var fecInfFor = new Date(parseInt(datosFechaInf[2]), parseInt(datosFechaInf[1]) - 1, parseInt(datosFechaInf[0]));
                    var fecRecInfFor = new Date(parseInt(datosFechaRecInf[2]), parseInt(datosFechaRecInf[1]) - 1, parseInt(datosFechaRecInf[0]));
                    var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));

                    response.data.fecIni = fecIniFor;
                    response.data.fecTer = fecTerFor;
                    response.data.fecInf = fecInfFor;
                    response.data.fecRecInf = fecRecInfFor;
                    response.data.fecDoc = fecDocFor;

                    response.data.dias = (fecTerFor - fecIniFor) / (1000 * 60 * 60 * 24) + 1;
                    response.data.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', response.data.tipDocId);
                    response.data.estProDes = buscarContenido($scope.estadosProceso, 'id', 'nom', response.data.estProId);
                    response.data.i = $rootScope.settingModVacaciones.dataset.length===undefined?0:$rootScope.settingModVacaciones.dataset.length;

                    $rootScope.settingModVacaciones.dataset.push(response.data);
                    $rootScope.tablaModVacaciones.sorting({vacId:'asc'});      
                    $rootScope.tablaModVacaciones.page(1);
                    $rootScope.tablaModVacaciones.reload();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                }
            }, function (error) {
                console.info(error);
            });
        };

    }]);
seApp.controller('agregarNuevoReconocimientoCtrl', ['$scope', '$rootScope', '$element', 'title', 'personaId', 'close', 'crud', 'modal', 'ModalService', 'tipDocs', function ($scope, $rootScope, $element, title, personaId, close, crud, modal, ModalService, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoReconocimiento = {perId: personaId, mot: "", desMot: "", numDoc: "", fecDoc: "", tipDoc: 0, entEmi: ""};
        //$scope.nuevoLegajoRec = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", des:"Resolucion", url:"", catLeg:"5", subCat:"1", codAspOri:""};

        $scope.motivos = [
            {id: "1", title: "Mérito"},
            {id: "2", title: "Felicitación"},
            {id: "3", title: "25 años de servicio"},
            {id: "4", title: "30 años de servicio"},
            {id: "5", title: "Luto y sepelio"},
            {id: "6", title: "Otros"}
        ];
        $scope.agregarReconocimiento = function () {
            var request = crud.crearRequest('reconocimientos', 1, 'agregarReconocimiento');
            request.setData($scope.nuevoReconocimiento);
            crud.insertar("/sistema_escalafon/", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDoc = buscarContenido($scope.tipDocs, 'id', 'nom', response.data.tipDocId);
                    var auxMot = buscarObjeto($scope.motivos, "id", response.data.mot);
                    response.data.motDes = auxMot.title;
                    insertarElemento($rootScope.settingModReconocimientos.dataset, response.data);
                    $rootScope.tablaModReconocimientos.reload();                    /*if ($scope.nuevoLegajoRec.archivo){
                     $scope.nuevoLegajoRec.codAspOri = response.data.recId;
                     
                     var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                     request.setData($scope.nuevoLegajoRec);
                     crud.insertar("/sistema_escalafon", request, function (response) {
                     modal.mensaje("CONFIRMACION", response.responseMsg);
                     }, function (data) {
                     console.info(data);
                     });
                     }
                     var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                     request.setData($scope.nuevoLegajoRec);
                     crud.insertar("/sistema_escalafon", request, function (response) {
                     $scope.listarReconocimientos();
                     modal.mensaje("CONFIRMACION", response.responseMsg);
                     }, function (data) {
                     console.info(data);
                     });*/
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevoDemeritoCtrl', ['$scope', '$rootScope', '$element', 'title', 'personaId', 'close', 'crud', 'modal', 'ModalService', 'tipDocs', function ($scope, $rootScope, $element, title, personaId, close, crud, modal, ModalService, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoDemerito = {perId: personaId, entEmi: "", numDoc: "", fecDoc: "", tipDoc: 0, sep: false, fecIni: "", fecFin: "", mot: ""};
        //$scope.nuevoLegajoDem = {ficEscId:fichaEscalafonariaId, nom:"", archivo:"", url:"", des:"Resolución", catLeg:"6", subCat:"1", codAspOri:""};

        $scope.agregarDemerito = function () {
            var request = crud.crearRequest('demeritos', 1, 'agregarDemerito');
            request.setData($scope.nuevoDemerito);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    //console.log(response.data);
                    response.data.tipDoc = buscarContenido($scope.tipDocs, 'id', 'nom', response.data.tipDocId);
                    response.data.sepDes = response.data.sep ? "Si" : "No";
                    insertarElemento($rootScope.settingModDemeritos.dataset, response.data);
                    $rootScope.tablaModDemeritos.reload();

                    /*
                     if ($scope.nuevoLegajoDem.archivo){
                     $scope.nuevoLegajoDem.codAspOri = response.data.demId;
                     
                     var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                     request.setData($scope.nuevoLegajoDem);
                     crud.insertar("/sistema_escalafon", request, function (response) {
                     $scope.listarDemeritos();
                     modal.mensaje("CONFIRMACION", response.responseMsg);
                     }, function (data) {
                     console.info(data);
                     });
                     }*/
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
seApp.controller('visualizarOrganigramaRegLabCtrl', ['$scope', '$rootScope', 'data', 'close', 'NgTableParams', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, data, close, NgTableParams, crud, modal, ModalService) {
        var dataInput = data;
        console.log("dataInput ");
        console.log(dataInput);
        $scope.title = dataInput.title;
        $scope.organigramas = [];
        $scope.planillas = [];
        $scope.categorias = [];
        $scope.cargos = [];
        $scope.organigrama = {id: data.trabajador.datOrgId = 0 ? 0 : data.trabajador.datOrgId};
        $rootScope.paramsOrganismos = {count: 10};
        $rootScope.settingOrganismos = {counts: []};
        $rootScope.tablaOrganismos = new NgTableParams($rootScope.paramsOrganismos, $rootScope.settingOrganismos);

        $scope.arregloSelects = [];
        $scope.organismosArray = [];
        $scope.newTraOrgDetalle = {
            traId: dataInput.trabajador.traId,
            orgiId: dataInput.trabajador.orgiId = 0 ? 0 : dataInput.trabajador.orgiId,
            ubiId: dataInput.trabajador.ubiId = 0 ? 0 : dataInput.trabajador.ubiId,
            plaId: dataInput.trabajador.plaId = 0 ? 0 : dataInput.trabajador.plaId,
            catId: dataInput.trabajador.catId = 0 ? 0 : dataInput.trabajador.catId,
            carId: dataInput.trabajador.carId = 0 ? 0 : dataInput.trabajador.carId
        };
        $scope.mostrarMensaje = false;

        $scope.operacion = dataInput.operacion;
        ;
        $scope.actualizarDescendiente = function (index, orgiPadId, isLast) {
            var orgFil = buscarObjetos($scope.organismosArray[index + 1], 'orgIdPad', orgiPadId);
            if (!isLast) {
                $rootScope.settingOrganismos.dataset[index + 1].organismosFil = orgFil;
                $rootScope.tablaOrganismos.reload();
            } else {
                $scope.newTraOrgDetalle.orgiId = $scope.arregloSelects[index].orgId;
                $scope.newTraOrgDetalle.ubiId = $scope.arregloSelects[index].ubiId;
            }
        };
        listarDatosOrganigramas();
        function listarDatosOrganigramas() {
            var request = crud.crearRequest('datosOrgaConfiguracion', 1, 'listarDatosOrg');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.organigramas = response.data;
                    listarTO();
                } else {
                    modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
                }
            }, function (data) {
                console.info(data);
            });
        }
        ;

        $scope.listarDatosOrg = function () {
            var request = crud.crearRequest('datosOrgaConfiguracion', 1, 'listarDatosOrg');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.organigramas = response.data;
                    listarTO();
                } else {
                    modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
                }
            }, function (data) {
                console.info(data);
            });
        };

        //listarTO();
        function listarTO() {
            if ($scope.organigrama.id !== 0) {
                console.log("listando tipos de organismos 1");
                var request = crud.crearRequest('tiposOrganigramaConfiguracion', 1, 'listarTiposOrganismosxOrgi');
                request.setData({organigramaId: $scope.organigrama.id});

                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
                crud.listar("/sistema_escalafon", request, function (response) {
                    if (response.responseSta) {
                        console.log(response);
                        if (response.data.length === 0) {
                            $scope.mensajeOrganigrama = "No existen niveles y entidades definidos para el organigrama";
                            $scope.mostrarMensaje = true;
                        } else {
                            $scope.organismosArray = [];
                            $scope.arregloSelects = [];
                            response.data.forEach(function (item) {
                                $scope.organismosArray.push(item.organismos);
                            });
                            $scope.listarDetalleOrganigrama();
                            $rootScope.settingOrganismos.dataset = response.data;
                            //asignando la posicion en el arreglo a cada objeto
                            iniciarPosiciones($rootScope.settingOrganismos.dataset);
                            $rootScope.tablaOrganismos.settings($rootScope.settingOrganismos);
                        }

                    } else {
                        modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
                    }
                }, function (data) {
                    console.info(data);
                });
            }
        }
        ;
        $scope.listarTiposOrganismos = function () {
            console.log($scope.organigrama.id);
            if ($scope.organigrama.id !== null) {
                console.log("listando tipos de organismos 2");
                var request = crud.crearRequest('tiposOrganigramaConfiguracion', 1, 'listarTiposOrganismosxOrgi');
                request.setData({organigramaId: $scope.organigrama.id});

                //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
                crud.listar("/sistema_escalafon", request, function (response) {
                    if (response.responseSta) {
                        console.log(response);
                        if (response.data.length === 0) {
                            $scope.mensajeOrganigrama = "No existen niveles y entidades definidos para el organigrama";
                            $scope.mostrarMensaje = true;
                        } else {
                            $scope.organismosArray = [];
                            $scope.arregloSelects = [];
                            response.data.forEach(function (item) {
                                $scope.organismosArray.push(item.organismos);
                                //$scope.arregloSelects.push({orgId: 0, nomOrg:""});
                            });
                            $scope.listarDetalleOrganigrama();
                            $rootScope.settingOrganismos.dataset = response.data;
                            //asignando la posicion en el arreglo a cada objeto
                            iniciarPosiciones($rootScope.settingOrganismos.dataset);
                            $rootScope.tablaOrganismos.settings($rootScope.settingOrganismos);
                        }
                    } else {
                        modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
                    }
                }, function (data) {
                    console.info(data);
                });
            }
        };

        listarPlanillas();
        function listarPlanillas() {
            var request = crud.crearRequest('planillaConfiguracion', 1, 'listarPlanillas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.planillas = response.data;
                    $scope.listarCategorias();
                } else {
                    modal.mensaje("ERROR", "No se pudo listar las planillas");
                }
            }, function (data) {
                console.info(data);
            });
        }
        ;

        $scope.listarCategorias = function () {
            console.log($scope.newTraOrgDetalle);
            var request = crud.crearRequest('categoriaConfiguracion', 1, 'listarxPlanilla');
            request.setData({plaId: $scope.newTraOrgDetalle.plaId});

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.categorias = response.data;
                } else {
                    modal.mensaje("ERROR", "No se pudo listar las categorías");
                }
            }, function (data) {
                console.info(data);
            });
        };

        listarCargos();
        function listarCargos() {
            var request = crud.crearRequest('cargoConfiguracion', 1, 'listarCargos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.cargos = response.data;
                } else {
                    modal.mensaje("ERROR", "No se pudo listar los cargos");
                }
            }, function (data) {
                console.info(data);
            });
        }
        ;

        $scope.listarDetalleOrganigrama = function () {
            if ($scope.newTraOrgDetalle.orgiId > 0) {
                $scope.arregloSelects = new Array($scope.organismosArray.length);
                for (var i = $scope.organismosArray.length - 1; i > -1; i--) {
                    $scope.arregloSelects[i] = {orgId: 0, nomOrg: ""};
                    if (i === $scope.organismosArray.length - 1) {
                        $scope.arregloSelects[i] = buscarObjeto($scope.organismosArray[i], 'orgId', $scope.newTraOrgDetalle.orgiId);
                    } else {
                        $scope.arregloSelects[i] = buscarObjeto($scope.organismosArray[i], 'orgId', $scope.arregloSelects[i + 1].orgIdPad);
                    }
                }
            }
        };

        $scope.actualizarTraOrgDet = function () {
            console.log($scope.newTraOrgDetalle);
            if (data.trabajador.tipOrgId === 0) {
                var request = crud.crearRequest('organigrama_trabajador', 1, 'agregarTraOrgDet');
                request.setData($scope.newTraOrgDetalle);
                crud.insertar("/sistema_escalafon", request, function (response) {
                    if (response.responseSta) {
                        console.log(response.data);
                        $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].orgiId = response.data.orgiId;
                        $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].plaId = response.data.plaId;
                        $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].catId = response.data.catId;
                        $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].carId = response.data.carId;
                        $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].datOrgId = $scope.organigrama.id;
                        
                        if(response.data.plaId>0){
                            var item = buscarObjeto($scope.planillas,'plaId',response.data.plaId);
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomPla = item.nomPla;
                        }else{
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomPla = "";
                        }
                        
                        if(response.data.catId>0){
                            var item = buscarObjeto($scope.categorias,'catId',response.data.catId);
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomCat = item.nomCat;
                        }else{
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomCat = "";
                        }
                        
                        if(response.data.carId>0){
                            var item = buscarObjeto($scope.cargos,'carId',response.data.carId);
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomCar = item.nomCar;
                        }else{
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomCar = "";
                        }// $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomUbi = buscarObjeto($scope.cargos,'carId',response.data.carId);
                        $rootScope.tablaDatCenLab.reload();
                        modal.mensaje("CONFIRMACION", response.responseMsg);

                    }
                }, function (data) {
                    console.info(data);
                });
            } else {
                var request = crud.crearRequest('organigrama_trabajador', 1, 'actualizarTraOrgDet');
                request.setData($scope.newTraOrgDetalle);
                crud.insertar("/sistema_escalafon", request, function (response) {
                    if (response.responseSta) {
                        console.log(response.data);
                        $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].orgiId = response.data.orgiId;
                        $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].plaId = response.data.plaId;
                        $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].catId = response.data.catId;
                        $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].catId = response.data.catId;
                        $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].datOrgId = $scope.organigrama.id;
                        
                        /*$scope.newTraOrgDetalle.plaId = response.data.plaId;
                        $scope.newTraOrgDetalle.catId = response.data.catId;
                        $scope.newTraOrgDetalle.carId = response.data.catId;*/
                        
                        if(response.data.plaId>0){
                            var item = buscarObjeto($scope.planillas,'plaId',response.data.plaId);
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomPla = item.nomPla;
                        }else{
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomPla = "";
                        }
                        
                        if(response.data.catId>0){
                            var item = buscarObjeto($scope.categorias,'catId',response.data.catId);
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomCat = item.nomCat;
                        }else{
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomCat = "";
                        }
                        
                        if(response.data.carId>0){
                            var item = buscarObjeto($scope.cargos,'carId',response.data.carId);
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomCar = item.nomCar;
                        }else{
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomCar = "";
                        }
                        
                        if($scope.organigrama.id !== null && $scope.organigrama.id >0){
                            var item = buscarObjeto($scope.organigramas,'datId', $scope.organigrama.id);
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomDatOrgi = item.nomDat;
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].anioDatOrgi = item.anioDat;
                        }else{
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomDatOrgi = "";
                        }
                        
                        if(response.data.orgiId>0){
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomUbi = $scope.arregloSelects[2].ubiNom;
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomOrgiN3 = $scope.arregloSelects[0].nomOrg;
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomOrgiN2 = $scope.arregloSelects[1].nomOrg;
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomOrgiN1 = $scope.arregloSelects[2].nomOrg;
                        }else{
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomUbi = "";
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomOrgiN3 = "";
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomOrgiN2 = "";
                            $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].nomOrgiN1 = "";
                        }
                        $rootScope.tablaDatCenLab.reload();
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                    }
                }, function (data) {
                    console.info(data);
                });

            }

        };
    }]);
seApp.controller('agregarNuevoCeseCtrl', ['$scope', '$rootScope', '$element', 'data', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, data, close, crud, modal, ModalService) {
        var dataInput = data;
        $scope.title = dataInput.title;
        $scope.fichaId = dataInput.fichaEscalafonariaId;
        $scope.tiposDocumentos = dataInput.tiposDocumentos;
        $scope.jornadasLaborales = dataInput.jornadasLaborales;
        $scope.tiposCeses = dataInput.tiposCeses;
        
        
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoCese = {entEmi: "", tipDocId: 0, numDoc: "", fecDoc: new Date(), jorLabId: 0, fecIni: new Date, fecFin: new Date(), ficEscId: $scope.fichaId, motCes: "",
            orgiId: dataInput.orgiId,
            carId: dataInput.carId,
            catId: dataInput.catId,
            diasPro:0,
            mesesPro:0,
            aniosPro:0,
            tipCes:'0'
        };
        
        $scope.calcularAnioProNuevoCese = function () {
        if($scope.nuevoCese.fecIni !== "" && $scope.nuevoCese.fecFin !== ""){
            var diaIni =$scope.nuevoCese.fecIni.getDate();
            var mesIni =$scope.nuevoCese.fecIni.getMonth()+1;
            var anioIni =$scope.nuevoCese.fecIni.getFullYear();
            
            var diaTer =$scope.nuevoCese.fecFin.getDate();
            var mesTer =$scope.nuevoCese.fecFin.getMonth()+1;
            var anioTer =$scope.nuevoCese.fecFin.getFullYear();
            var numDias360=0;
            
            if(anioTer===anioIni){

                if(mesTer===mesIni){

                    numDias360=diaTer-diaIni;

                }else{
                    if(diaTer===diaIni){
                        numDias360=(mesTer-mesIni)*30;
                    }
                    if(diaTer>diaIni){
                        numDias360=(mesTer-mesIni)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360+(mesTer-mesIni)*30-(diaIni-diaTer);
                    }
                }
                    
            }
            
            if(anioTer>anioIni){
                if(mesTer===mesIni){
                    if(diaTer===diaIni){
                        numDias360=(anioTer-anioIni)*360;
                    }
                    if(diaTer>diaIni){
                        numDias360=(anioTer-anioIni)*360+(diaTer-diaIni);
                    }
                    if(diaIni>diaTer){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=(anioTer-anioIni)*360-(diaTer-diaIni);
                    }
                }
                
                if(mesTer>mesIni){
                    numDias360=(anioTer-anioIni)*360;
                    if(diaTer===diaIni){
                        numDias360=numDias360+(mesTer-mesIni)*30;
                    }
                    if(diaTer>diaIni){
                        numDias360=numDias360+(mesTer-mesIni)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        var numD=0;
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            for(var i=mesIni; i<=mesTer;i++){
                                if(mesTer-mesIni===1 && diaIni-diaTer>0){
                                    if(mesIni===2){
                                        numD=numD+(29-(diaIni-diaTer));
                                    }else{
                                        numD=numD+(30-(diaIni-diaTer));
                                    }    
                                }else{
                                    numD=numD+30;
                                }
                                
                            }
                        }else{
                            for(var i=mesIni; i<=mesTer;i++){
                                if(mesTer-mesIni===1 && diaIni-diaTer>0){
                                    if(mesIni===2){
                                        numD=numD+(28-(diaIni-diaTer));
                                    }else{
                                        numD=numD+(30-(diaIni-diaTer));
                                    }    
                                }else{
                                    numD=numD+30;
                                }
                                
                            }
                        }
                        
                        numDias360=numDias360+numD;
                    }
                    
                    
                }
                
                if(mesTer<mesIni){
                    numDias360=(anioTer-anioIni)*360;
                    if(diaTer===diaIni){
                        numDias360=numDias360-(mesIni-mesTer)*30;
                    }
                    if(diaTer>diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360-(mesIni-mesTer)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360-(mesIni-mesTer)*30-(diaIni-diaTer);
                    }
                }
                    
            }

            var diaTran=numDias360%30;
            var mesTran=Math.trunc(numDias360/30)%12;
            var anioTran=Math.trunc(Math.trunc(numDias360/30)/12);
            
        }
            $scope.nuevoCese.diasPro= diaTran;
            $scope.nuevoCese.mesesPro= mesTran;
            $scope.nuevoCese.aniosPro= anioTran;
            
        };

        $scope.agregarCese = function () {
            var request = crud.crearRequest('cese', 1, 'agregarCese');
            request.setData($scope.nuevoCese);
            crud.insertar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    var ceseAgre = JSON.parse(JSON.stringify(response));
                    
                    var datosFechaTer = ceseAgre.data.fecTer.split("-");
                    var datosFechaIni = ceseAgre.data.fecIni.split("-");
                    var datosFechaDoc = ceseAgre.data.fecDoc.split("-");

                    var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                    var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));

                    response.data.fecIni = fecIniFor;
                    response.data.fecTer = fecTerFor;
                    response.data.fecDoc = fecDocFor;

                    response.data.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', ceseAgre.data.tipDocId);
                    response.data.jorLabDes = buscarContenido($scope.jornadasLaborales, 'id', 'nom', ceseAgre.data.jorLabId);
                    response.data.tipCesDes = buscarContenido($scope.tiposCeses, 'id', 'nom', response.data.tipCes);
                    response.data.i = $rootScope.settingModCeses.dataset.length===undefined?0:$rootScope.settingModCeses.dataset.length;
                    $rootScope.settingModCeses.dataset.push(response.data);                    
                    $rootScope.tablaModCeses.sorting({cesId:'asc'});
                    $rootScope.tablaModCeses.page(1);
                    $rootScope.tablaModCeses.reload();
                    

                    modal.mensaje("CONFIRMACION", response.responseMsg);

                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
seApp.controller('editarCeseCtrl', ['$rootScope', '$scope', '$element', 'data', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, data, close, crud, modal) {
        var dataInput = data;
        $scope.title = dataInput.title;
        var cese = dataInput.ceseSel;
        $scope.tiposDocumentos = dataInput.tiposDocumentos;
        $scope.jornadasLaborales = dataInput.jornadasLaborales;
        $scope.tiposCeses = dataInput.tiposCeses;
        
        $scope.ceseSel = {
            cesId: cese.cesId,
            entEmi: cese.entEmiRes,
            tipDocId: cese.tipDocId,
            numDoc: cese.numDoc,
            fecDoc: cese.fecDoc !== "" ?
                    new Date(cese.fecDoc.substring(0, 10).split("-")[0],
                            cese.fecDoc.substring(0, 10).split("-")[1] - 1,
                            cese.fecDoc.substring(0, 10).split("-")[2]) : "",
            jorLabId: cese.jorLabId,
            fecIni: cese.fecIni !== "" ?
                    new Date(cese.fecIni.substring(0, 10).split("-")[0],
                            cese.fecIni.substring(0, 10).split("-")[1] - 1,
                            cese.fecIni.substring(0, 10).split("-")[2]) : "",
            fecFin: cese.fecTer !== "" ?
                    new Date(cese.fecTer.substring(0, 10).split("-")[0],
                            cese.fecTer.substring(0, 10).split("-")[1] - 1,
                            cese.fecTer.substring(0, 10).split("-")[2]) : "",      
            motCes: cese.motCes,
            
            diasPro:cese.diasPro,
            mesesPro:cese.mesesPro,
            aniosPro:cese.aniosPro,
            tipCes: cese.tipCes
            
        };      
        $scope.calcularAnioPro = function () {
        if($scope.ceseSel.fecIni !== "" && $scope.ceseSel.fecFin !== ""){
            var diaIni =$scope.ceseSel.fecIni.getDate();
            var mesIni =$scope.ceseSel.fecIni.getMonth()+1;
            var anioIni =$scope.ceseSel.fecIni.getFullYear();
            
            var diaTer =$scope.ceseSel.fecFin.getDate();
            var mesTer =$scope.ceseSel.fecFin.getMonth()+1;
            var anioTer =$scope.ceseSel.fecFin.getFullYear();

            var numDias360=0;
            
            if(anioTer===anioIni){

                if(mesTer===mesIni){

                    numDias360=diaTer-diaIni;

                }else{
                    if(diaTer===diaIni){
                        numDias360=(mesTer-mesIni)*30;
                    }
                    if(diaTer>diaIni){
                        numDias360=(mesTer-mesIni)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360+(mesTer-mesIni)*30-(diaIni-diaTer);
                    }
                }
                    
            }
            
            if(anioTer>anioIni){
                if(mesTer===mesIni){
                    if(diaTer===diaIni){
                        numDias360=(anioTer-anioIni)*360;
                    }
                    if(diaTer>diaIni){
                        numDias360=(anioTer-anioIni)*360+(diaTer-diaIni);
                    }
                    if(diaIni>diaTer){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=(anioTer-anioIni)*360-(diaTer-diaIni);
                    }
                }
                
                if(mesTer>mesIni){
                    numDias360=(anioTer-anioIni)*360;
                    if(diaTer===diaIni){
                        numDias360=numDias360+(mesTer-mesIni)*30;
                    }
                    if(diaTer>diaIni){
                        numDias360=numDias360+(mesTer-mesIni)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        var numD=0;
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            for(var i=mesIni; i<=mesTer;i++){
                                if(mesTer-mesIni===1 && diaIni-diaTer>0){
                                    if(mesIni===2){
                                        numD=numD+(29-(diaIni-diaTer));
                                    }else{
                                        numD=numD+(30-(diaIni-diaTer));
                                    }    
                                }else{
                                    numD=numD+30;
                                }
                                
                            }
                        }else{
                            for(var i=mesIni; i<=mesTer;i++){
                                if(mesTer-mesIni===1 && diaIni-diaTer>0){
                                    if(mesIni===2){
                                        numD=numD+(28-(diaIni-diaTer));
                                    }else{
                                        numD=numD+(30-(diaIni-diaTer));
                                    }    
                                }else{
                                    numD=numD+30;
                                }
                                
                            }
                        }
                        
                        numDias360=numDias360+numD;
                    }
                    
                    
                }
                
                if(mesTer<mesIni){
                    numDias360=(anioTer-anioIni)*360;
                    if(diaTer===diaIni){
                        numDias360=numDias360-(mesIni-mesTer)*30;
                    }
                    if(diaTer>diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360-(mesIni-mesTer)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360-(mesIni-mesTer)*30-(diaIni-diaTer);
                    }
                }
                    
            }
            numDias360=numDias360-1;
            var diaTran=numDias360%30;
            var mesTran=Math.trunc(numDias360/30)%12;
            var anioTran=Math.trunc(Math.trunc(numDias360/30)/12);
            
        }
            $scope.ceseSel.diasPro= diaTran;
            $scope.ceseSel.mesesPro= mesTran;
            $scope.ceseSel.aniosPro= anioTran;
            
        };
        
        $scope.actualizarCese = function () {
            var request = crud.crearRequest('cese', 1, 'actualizarCese');
            request.setData($scope.ceseSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    var ceseAgre = JSON.parse(JSON.stringify(response));                    
                    var datosFechaTer = ceseAgre.data.fecTer.split("-");
                    var datosFechaIni = ceseAgre.data.fecIni.split("-");
                    var datosFechaDoc = ceseAgre.data.fecDoc.split("-");

                    var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                    var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));

                    response.data.fecIni = fecIniFor;
                    response.data.fecTer = fecTerFor;
                    response.data.fecDoc = fecDocFor;

                    response.data.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', ceseAgre.data.tipDocId);
                    response.data.jorLabDes = buscarContenido($scope.jornadasLaborales, 'id', 'nom', ceseAgre.data.jorLabId);
                    response.data.tipCesDes = buscarContenido($scope.tiposCeses, 'id', 'nom', response.data.tipCes);
                    response.data.i = cese.i;
                    
                    $rootScope.settingModCeses.dataset[cese.i] = response.data;
                    $rootScope.tablaModCeses.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
seApp.controller('editarLicenciaCtrl', ['$rootScope', '$scope', '$element', 'data', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, data, close, crud, modal) {
        var dataInput = data;
        $scope.title = dataInput.title;
        var licencia = dataInput.licenciaSel;
        $scope.fichaId = dataInput.fichaEscalafonariaId;
        $scope.tiposDocumentos = dataInput.tiposDocumentos;
        $scope.estadosProceso = dataInput.estadosProceso;
        $scope.tiposLicencias = dataInput.tiposLicencias;

        $scope.licenciaSel = {
            i: licencia.i,
            licId: licencia.licId,
            entEmi: licencia.entEmi,
            tipDocIdMain: licencia.tipDocIdMain,
            numInfMain: licencia.numInfMain,
            fecInfMain: licencia.fecInfMain !== "" ?
                    new Date(licencia.fecInfMain.substring(0, 10).split("-")[0],
                            licencia.fecInfMain.substring(0, 10).split("-")[1] - 1,
                            licencia.fecInfMain.substring(0, 10).split("-")[2]) : "",
            estGoc: licencia.estGoc,
            fecIni: licencia.fecIni !== "" ?
                    new Date(licencia.fecIni.substring(0, 10).split("-")[0],
                            licencia.fecIni.substring(0, 10).split("-")[1] - 1,
                            licencia.fecIni.substring(0, 10).split("-")[2]) : "",
            fecTer: licencia.fecTer !== "" ?
                    new Date(licencia.fecTer.substring(0, 10).split("-")[0],
                            licencia.fecTer.substring(0, 10).split("-")[1] - 1,
                            licencia.fecTer.substring(0, 10).split("-")[2]) : "",
            mot: licencia.mot,
            numDocSec: licencia.numDocSec,
            fecDocSec: licencia.fecDocSec !== "" ?
                    new Date(licencia.fecDocSec.substring(0, 10).split("-")[0],
                            licencia.fecDocSec.substring(0, 10).split("-")[1] - 1,
                            licencia.fecDocSec.substring(0, 10).split("-")[2]) : "",
            fecRecInfMain: licencia.fecRecInfMain !== "" ?
                    new Date(licencia.fecRecInfMain.substring(0, 10).split("-")[0],
                            licencia.fecRecInfMain.substring(0, 10).split("-")[1] - 1,
                            licencia.fecRecInfMain.substring(0, 10).split("-")[2]) : "",
            tipLicId: licencia.tipLicId,
            estProId: licencia.estProId,
            tipDocIdSec:licencia.tipDocIdSec,
            tipDocIdMemo:licencia.tipDocIdMemo,
            numInfMemo:licencia.numInfMemo,
            fecInfMemo:licencia.fecInfMemo !== "" ?
                new Date(licencia.fecInfMemo.substring(0, 10).split("-")[0],
                                licencia.fecInfMemo.substring(0, 10).split("-")[1] - 1,
                                licencia.fecInfMemo.substring(0, 10).split("-")[2]) : "",
            fecRecInMemo:licencia.fecRecInMemo !== "" ?
                new Date(licencia.fecRecInMemo.substring(0, 10).split("-")[0],
                            licencia.fecRecInMemo.substring(0, 10).split("-")[1] - 1,
                            licencia.fecRecInMemo.substring(0, 10).split("-")[2]) : "",
            diaPro:licencia.diaPro,
            mesPro:licencia.mesPro,
            anioPro:licencia.anioPro
        };
        
        $scope.calcularAnioProEditarLic = function () {
        if($scope.licenciaSel.fecIni !== "" && $scope.licenciaSel.fecTer !== ""){
            var diaIni =$scope.licenciaSel.fecIni.getDate();
            var mesIni =$scope.licenciaSel.fecIni.getMonth()+1;
            var anioIni =$scope.licenciaSel.fecIni.getFullYear();
            
            var diaTer =$scope.licenciaSel.fecTer.getDate();
            var mesTer =$scope.licenciaSel.fecTer.getMonth()+1;
            var anioTer =$scope.licenciaSel.fecTer.getFullYear();

            var numDias360=0;
            
            if(anioTer===anioIni){

                if(mesTer===mesIni){

                    numDias360=diaTer-diaIni;

                }else{
                    if(diaTer===diaIni){
                        numDias360=(mesTer-mesIni)*30;
                    }
                    if(diaTer>diaIni){
                        numDias360=(mesTer-mesIni)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360+(mesTer-mesIni)*30-(diaIni-diaTer);
                    }
                }
                    
            }
            
            if(anioTer>anioIni){
                if(mesTer===mesIni){
                    if(diaTer===diaIni){
                        numDias360=(anioTer-anioIni)*360;
                    }
                    if(diaTer>diaIni){
                        numDias360=(anioTer-anioIni)*360+(diaTer-diaIni);
                    }
                    if(diaIni>diaTer){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=(anioTer-anioIni)*360-(diaTer-diaIni);
                    }
                }
                
                if(mesTer>mesIni){
                    numDias360=(anioTer-anioIni)*360;
                    if(diaTer===diaIni){
                        numDias360=numDias360+(mesTer-mesIni)*30;
                    }
                    if(diaTer>diaIni){
                        numDias360=numDias360+(mesTer-mesIni)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        var numD=0;
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            for(var i=mesIni; i<=mesTer;i++){
                                if(mesTer-mesIni===1 && diaIni-diaTer>0){
                                    if(mesIni===2){
                                        numD=numD+(29-(diaIni-diaTer));
                                    }else{
                                        numD=numD+(30-(diaIni-diaTer));
                                    }    
                                }else{
                                    numD=numD+30;
                                }
                                
                            }
                        }else{
                            for(var i=mesIni; i<=mesTer;i++){
                                if(mesTer-mesIni===1 && diaIni-diaTer>0){
                                    if(mesIni===2){
                                        numD=numD+(28-(diaIni-diaTer));
                                    }else{
                                        numD=numD+(30-(diaIni-diaTer));
                                    }    
                                }else{
                                    numD=numD+30;
                                }
                                
                            }
                        }
                        
                        numDias360=numDias360+numD;
                    }
                    
                    
                }
                
                if(mesTer<mesIni){
                    numDias360=(anioTer-anioIni)*360;
                    if(diaTer===diaIni){
                        numDias360=numDias360-(mesIni-mesTer)*30;
                    }
                    if(diaTer>diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360-(mesIni-mesTer)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360-(mesIni-mesTer)*30-(diaIni-diaTer);
                    }
                }
                    
            }

            var diaTran=numDias360%30;
            var mesTran=Math.trunc(numDias360/30)%12;
            var anioTran=Math.trunc(Math.trunc(numDias360/30)/12);
            
        }
            $scope.licenciaSel.diaPro= diaTran;
            $scope.licenciaSel.mesPro= mesTran;
            $scope.licenciaSel.anioPro= anioTran;
            
        };
        
        $scope.opcionesFechaIni = {
            maxDate: $scope.licenciaSel.fecTer,
            initDate: $scope.licenciaSel.fecIni
        };
        $scope.opcionesFechaTer = {
            minDate: $scope.licenciaSel.fecIni,
            initDate: $scope.licenciaSel.fecTer
        };

        $scope.actualizarLicencia = function () {
            var request = crud.crearRequest('licencia', 1, 'actualizarLicencia');
            request.setData($scope.licenciaSel);
            console.log("licAct: " + JSON.stringify($scope.licenciaSel));
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    response.data.i = licencia.i;
                    response.data.estGocDes = response.data.estGoc ? "Con Goce" : "Sin Goce";
                    var datosFechaTer = response.data.fecTer.split("-");
                    var datosFechaIni = response.data.fecIni.split("-");
                                        
                    var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                    var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    
                    response.data.fecIni = fecIniFor;
                    response.data.fecTer = fecTerFor;
                                        
                    if(response.data.fecDocSec!==""){
                        var datosFechaInf = response.data.fecDocSec.split("-");
                        var fecInfFor = new Date(parseInt(datosFechaInf[2]), parseInt(datosFechaInf[1]) - 1, parseInt(datosFechaInf[0]));
                        response.data.fecDocSec = fecInfFor;
                    }
                    if(response.data.fecRecInfMain!==""){
                        var datosFechaRecInf = response.data.fecRecInfMain.split("-");
                        var fecRecInfFor = new Date(parseInt(datosFechaRecInf[2]), parseInt(datosFechaRecInf[1]) - 1, parseInt(datosFechaRecInf[0]));
                        response.data.fecRecInfMain = fecRecInfFor;
                    
                    }
                    if(response.data.fecInfMain!==""){
                        var datosFechaDoc = response.data.fecInfMain.split("-");
                        var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));
                        response.data.fecInfMain = fecDocFor;
                    
                    }
                    if(response.data.fecInfMemo!==""){
                        var datosFechaMemo=response.data.fecInfMemo.split("-");
                        var fecMemFor=new Date(parseInt(datosFechaMemo[2]), parseInt(datosFechaMemo[1]) - 1, parseInt(datosFechaMemo[0]));
                        response.data.fecInfMemo = fecMemFor;
                                            
                    }
                    if(response.data.fecRecInMemo!==""){                        
                        var datosFechaReiMemo=response.data.fecRecInMemo.split("-");
                        var fecMemReiFor=new Date(parseInt(datosFechaReiMemo[2]), parseInt(datosFechaReiMemo[1]) - 1, parseInt(datosFechaReiMemo[0]));
                        response.data.fecRecInMemo = fecMemReiFor;
                    }                         
  
                    response.data.dias = (fecTerFor - fecIniFor) / (1000 * 60 * 60 * 24) + 1;
                    response.data.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', response.data.tipDocIdMain);
                    response.data.estProDes = buscarContenido($scope.estadosProceso, 'id', 'nom', response.data.estProId);
                    response.data.tipLicDes = buscarContenido($scope.tiposLicencias, 'id', 'nom', response.data.tipLicId);
                    $rootScope.settingModLicencias.dataset[licencia.i] = response.data;
                    $rootScope.tablaModLicencias.reload();
                    
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
seApp.controller('editarVacacionCtrl', ['$rootScope', '$scope', '$element', 'data', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, data, close, crud, modal) {
        var dataInput = data;
        $scope.title = dataInput.title;
        var vacacion = dataInput.vacacionSel;
        $scope.fichaId = dataInput.fichaEscalafonariaId;
        $scope.tiposDocumentos = dataInput.tiposDocumentos;
        $scope.estadosProceso = dataInput.estadosProceso;

        $scope.vacacionSel = {
            i: vacacion.i,
            vacId: vacacion.vacId,
            entEmi: vacacion.entEmi,
            tipDocId: vacacion.tipDocId,
            numDoc: vacacion.numDoc,
            fecDoc: vacacion.fecDoc != "" ?
                    new Date(vacacion.fecDoc.substring(0, 10).split("-")[0],
                            vacacion.fecDoc.substring(0, 10).split("-")[1] - 1,
                            vacacion.fecDoc.substring(0, 10).split("-")[2]) : "",
            estGoc: vacacion.estGoc,
            fecIni: vacacion.fecIni != "" ?
                    new Date(vacacion.fecIni.substring(0, 10).split("-")[0],
                            vacacion.fecIni.substring(0, 10).split("-")[1] - 1,
                            vacacion.fecIni.substring(0, 10).split("-")[2]) : "",
            fecTer: vacacion.fecTer != "" ?
                    new Date(vacacion.fecTer.substring(0, 10).split("-")[0],
                            vacacion.fecTer.substring(0, 10).split("-")[1] - 1,
                            vacacion.fecTer.substring(0, 10).split("-")[2]) : "",
            mot: vacacion.mot,
            numInf: vacacion.numInf,
            fecInf: vacacion.fecInf != "" ?
                    new Date(vacacion.fecInf.substring(0, 10).split("-")[0],
                            vacacion.fecInf.substring(0, 10).split("-")[1] - 1,
                            vacacion.fecInf.substring(0, 10).split("-")[2]) : "",
            fecRecInf: vacacion.fecRecInf != "" ?
                    new Date(vacacion.fecRecInf.substring(0, 10).split("-")[0],
                            vacacion.fecRecInf.substring(0, 10).split("-")[1] - 1,
                            vacacion.fecRecInf.substring(0, 10).split("-")[2]) : "",
            estProId: vacacion.estProId
        };
        
        $scope.opcionesFechaIni = {
            maxDate: $scope.vacacionSel.fecTer,
            initDate: $scope.vacacionSel.fecIni
        };
        $scope.opcionesFechaTer = {
            minDate: $scope.vacacionSel.fecIni,
            initDate: $scope.vacacionSel.fecTer
        };
        
        $scope.actualizarVacacion = function () {
            var request = crud.crearRequest('vacacion', 1, 'actualizarVacacion');
            request.setData($scope.vacacionSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.i = vacacion.i;
                    response.data.estGocDes = response.data.estGoc ? "Con Goce" : "Sin Goce";
                    var datosFechaTer = response.data.fecTer.split("-");
                    var datosFechaIni = response.data.fecIni.split("-");
                    var datosFechaInf = response.data.fecInf.split("-");
                    var datosFechaRecInf = response.data.fecRecInf.split("-");
                    var datosFechaDoc = response.data.fecDoc.split("-");

                    var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                    var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                    var fecInfFor = new Date(parseInt(datosFechaInf[2]), parseInt(datosFechaInf[1]) - 1, parseInt(datosFechaInf[0]));
                    var fecRecInfFor = new Date(parseInt(datosFechaRecInf[2]), parseInt(datosFechaRecInf[1]) - 1, parseInt(datosFechaRecInf[0]));
                    var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));

                    response.data.fecIni = fecIniFor;
                    response.data.fecTer = fecTerFor;
                    response.data.fecInf = fecInfFor;
                    response.data.fecRecInf = fecRecInfFor;
                    response.data.fecDoc = fecDocFor;

                    response.data.dias = (fecTerFor - fecIniFor) / (1000 * 60 * 60 * 24) + 1;
                    response.data.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', response.data.tipDocId);
                    response.data.estProDes = buscarContenido($scope.estadosProceso, 'id', 'nom', response.data.estProId);
                    $rootScope.settingModVacaciones.dataset[vacacion.i] = response.data;
                    $rootScope.tablaModVacaciones.reload();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('realizarRotacionCtrl', ['$scope', '$rootScope', 'data', 'close', 'NgTableParams', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, data, close, NgTableParams, crud, modal, ModalService) {
        var dataInput = data;
        console.log(dataInput);
        $scope.title = dataInput.title;
        $scope.organigramas = [];
        $scope.planillas = [];
        $scope.categorias = [];
        $scope.cargos = [];
        $scope.organigrama = {id: data.trabajador.datOrgId = 0 ? 0 : data.trabajador.datOrgId};
        $rootScope.paramsOrganismosMod = {count: 10};
        $rootScope.settingOrganismosMod = {counts: []};
        $rootScope.tablaOrganismosMod = new NgTableParams($rootScope.paramsOrganismosMod, $rootScope.settingOrganismosMod);
        $scope.tiposAscensos = dataInput.tiposAscensos;
        $scope.tiposDocumentos = dataInput.tiposDocumentos;
        $scope.opcion = 0;

        $scope.arregloSelects = [];
        $scope.organismosArray = [];
        $scope.traOrgDetalleSel = {
            traId: dataInput.trabajador.traId,
            orgiIdOld: dataInput.trabajador.orgiId,
            orgiIdNew: dataInput.trabajador.orgiId,
            ubiId: 0,
            plaIdOld: dataInput.trabajador.plaId,
            catIdOld: dataInput.trabajador.catId,
            carIdOld: dataInput.trabajador.carId,
            plaId: dataInput.trabajador.plaId,
            catId: dataInput.trabajador.catId,
            carId: dataInput.trabajador.carId
        };
        $scope.nuevoAscenso = {
            ficEscId: dataInput.fichaEscalafonariaId,
            tip: "",
            tipDocId: 0,
            numDoc: "",
            fecDoc: "",
            fecEfe: "",
            mot: "",
            orgiAntId: dataInput.trabajador.orgiId,
            catAntId: dataInput.trabajador.catId,
            carAntId: dataInput.trabajador.carId,
            orgiPostId: dataInput.trabajador.orgiId,
            catPostId: dataInput.trabajador.catId,
            carPostId: dataInput.trabajador.carId
        };
        $scope.mensajeOrganigrama = "miaua";
        $scope.mostrarMensaje = false;

        $scope.actualizarDescendiente = function (index, orgiPadId, isLast) {
            var orgFil = buscarObjetos($scope.organismosArray[index + 1], 'orgIdPad', orgiPadId);
            console.log($scope.arregloSelects[index]);
            if (!isLast) {
                $rootScope.settingOrganismosMod.dataset[index + 1].organismosFil = orgFil;
                $rootScope.tablaOrganismosMod.reload();
            } else {
                $scope.traOrgDetalleSel.orgiIdNew = $scope.arregloSelects[index].orgId;
                $scope.traOrgDetalleSel.ubiId = $scope.arregloSelects[index].ubiId;
            }
        };
        listarDatosOrganigramas();
        function listarDatosOrganigramas() {
            var request = crud.crearRequest('datosOrgaConfiguracion', 1, 'listarDatosOrg');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.organigramas = response.data;
                    listarTO();
                } else {
                    modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
                }
            }, function (data) {
                console.info(data);
            });
        }
        ;

        $scope.listarDatosOrg = function () {
            var request = crud.crearRequest('datosOrgaConfiguracion', 1, 'listarDatosOrg');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.organigramas = response.data;
                    listarTO();
                } else {
                    modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
                }
            }, function (data) {
                console.info(data);
            });
        };

        listarTO();
        function listarTO() {
            console.log("listando tipos de organismos");
            var request = crud.crearRequest('tiposOrganigramaConfiguracion', 1, 'listarTiposOrganismosxOrgi');
            request.setData({organigramaId: $scope.organigrama.id});

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    if (response.data.length === 0) {
                        $scope.mensajeOrganigrama = "No existen niveles y entidades definidos para el organigrama";
                        $scope.mostrarMensaje = true;
                    } else {
                        $scope.organismosArray = [];
                        $scope.arregloSelects = [];
                        response.data.forEach(function (item) {
                            $scope.organismosArray.push(item.organismos);
                        });
                        $scope.listarDetalleOrganigrama();
                        $rootScope.settingOrganismosMod.dataset = response.data;
                        //asignando la posicion en el arreglo a cada objeto
                        iniciarPosiciones($rootScope.settingOrganismosMod.dataset);
                        $rootScope.tablaOrganismosMod.settings($rootScope.settingOrganismosMod);
                    }
                } else {
                    modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
                }
            }, function (data) {
                console.info(data);
            });
        }
        ;
        $scope.listarTiposOrganismos = function () {
            var request = crud.crearRequest('tiposOrganigramaConfiguracion', 1, 'listarTiposOrganismosxOrgi');
            request.setData({organigramaId: $scope.organigrama.id});

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    if (response.data.length === 0) {
                        $scope.mensajeOrganigrama = "No existen niveles y entidades definidos para el organigrama";
                        $scope.mostrarMensaje = true;
                    } else {
                        $scope.organismosArray = [];
                        $scope.arregloSelects = [];
                        response.data.forEach(function (item) {
                            $scope.organismosArray.push(item.organismos);
                        });
                        $scope.listarDetalleOrganigrama();
                        $rootScope.settingOrganismosMod.dataset = response.data;
                        //asignando la posicion en el arreglo a cada objeto
                        iniciarPosiciones($rootScope.settingOrganismosMod.dataset);
                        $rootScope.tablaOrganismosMod.settings($rootScope.settingOrganismosMod);
                    }
                } else {
                    modal.mensaje("ERROR", "No se pudo listar los datos de organigrama");
                }
            }, function (data) {
                console.info(data);
            });
        };

        listarPlanillas();
        function listarPlanillas() {
            var request = crud.crearRequest('planillaConfiguracion', 1, 'listarPlanillas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.planillas = response.data;
                    $scope.listarCategorias();
                    console.log(response);
                } else {
                    modal.mensaje("ERROR", "No se pudo listar las planillas");
                }
            }, function (data) {
                console.info(data);
            });
        }
        ;

        $scope.listarCategorias = function () {
            console.log($scope.newTraOrgDetalle);
            var request = crud.crearRequest('categoriaConfiguracion', 1, 'listarxPlanilla');
            request.setData({plaId: $scope.traOrgDetalleSel.plaId});

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.categorias = response.data;
                } else {
                    modal.mensaje("ERROR", "No se pudo listar las categorías");
                }
            }, function (data) {
                console.info(data);
            });
        };

        listarCargos();
        function listarCargos() {
            var request = crud.crearRequest('cargoConfiguracion', 1, 'listarCargos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.cargos = response.data;
                } else {
                    modal.mensaje("ERROR", "No se pudo listar los cargos");
                }
            }, function (data) {
                console.info(data);
            });
        }
        ;

        $scope.listarDetalleOrganigrama = function () {
            if ($scope.traOrgDetalleSel.orgiIdNew > 0) {
                $scope.arregloSelects = new Array($scope.organismosArray.length);
                for (var i = $scope.organismosArray.length - 1; i > -1; i--) {
                    $scope.arregloSelects[i] = {orgId: 0, nomOrg: ""};
                    if (i === $scope.organismosArray.length - 1) {
                        $scope.arregloSelects[i] = buscarObjeto($scope.organismosArray[i], 'orgId', $scope.traOrgDetalleSel.orgiIdNew);
                    } else {
                        $scope.arregloSelects[i] = buscarObjeto($scope.organismosArray[i], 'orgId', $scope.arregloSelects[i + 1].orgIdPad);
                    }
                }
            }
        };
        
        $scope.listarAscensos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('ascenso', 1, 'listarAscensos');
            request.setData($scope.fichaEscalafonariaSel);
            crud.listar("/sistema_escalafon", request, function (response) {
                console.log("Ascensos:");
                console.log(response);
                response.data.forEach(function (item) {
                    item.tipDes = buscarContenido(tiposAscensos, 'id', 'nom', parseInt(item.tip));
                    item.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', item.tipDocId);
                });
                console.log(response);
                $rootScope.settingModAscensos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModAscensos.dataset);
                $rootScope.tablaModAscensos.settings($rootScope.settingModAscensos);
            }, function (error) {
                console.info(error);
            });

        };

        $scope.realizarRotacion = function () {
            var request = crud.crearRequest('organigrama_trabajador', 1, 'actTraOrgDetByCase');
            request.setData({opcion: $scope.opcion, ascenso: $scope.traOrgDetalleSel});
            crud.actualizar("/sistema_escalafon", request, function (response1) {
                if (response1.responseSta) {
                    switch ($scope.opcion) {
                        case 1:
                            $scope.nuevoAscenso.tip = '1';
                            $scope.nuevoAscenso.orgiAntId = $scope.traOrgDetalleSel.orgiIdOld;
                            $scope.nuevoAscenso.orgiPostId = $scope.traOrgDetalleSel.orgiIdNew;
                            break;
                        case 2:
                            $scope.nuevoAscenso.tip = '2';
                            $scope.nuevoAscenso.catAntId = $scope.traOrgDetalleSel.catIdOld;
                            $scope.nuevoAscenso.carAntId = $scope.traOrgDetalleSel.carIdOld;
                            $scope.nuevoAscenso.catPostId = $scope.traOrgDetalleSel.catId;
                            $scope.nuevoAscenso.carPostId = $scope.traOrgDetalleSel.carId;
                            break;
                        case 3:
                            $scope.nuevoAscenso.tip = '3';
                            $scope.nuevoAscenso.catAntId = $scope.traOrgDetalleSel.catIdOld;
                            $scope.nuevoAscenso.catAntId = $scope.traOrgDetalleSel.carIdOld;
                            $scope.nuevoAscenso.catPostId = $scope.traOrgDetalleSel.catId;
                            $scope.nuevoAscenso.carPostId = $scope.traOrgDetalleSel.carId;
                            break;
                    }

                    var request = crud.crearRequest('ascenso', 1, 'agregarAscxTipo');
                    request.setData({opcion: $scope.opcion, ascenso: $scope.nuevoAscenso});
                    crud.insertar("/sistema_escalafon", request, function (response2) {
                        if (response2.responseSta) {
                            response2.data.tipDes = buscarContenido($scope.tiposAscensos, 'id', 'nom', response2.data.tip);
                            response2.data.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', response2.data.tipDocId);
                            switch ($scope.opcion) {
                                case 1:
                                    $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].orgiId = response1.data.orgiId;
                                    break;
                                case 2:
                                    $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].plaId = response1.data.plaId;
                                    $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].catId = response1.data.catId;
                                    $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].carId = response1.data.carId;
                                    break;
                                case 3:
                                    $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].plaId = response1.data.plaId;
                                    $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].catId = response1.data.catId;
                                    $rootScope.settingDatCenLab.dataset[dataInput.trabajador.i].carId = response1.data.carId;
                                    break;
                            }

                            $rootScope.tablaDatCenLab.reload();

                            $rootScope.settingModAscensos.dataset.push(response2.data);
                            $rootScope.tablaModAscensos.sorting({ascId:'asc'});
                            $rootScope.tablaModAscensos.page(1);
                            $scope.listarAscensos();
                            $rootScope.tablaModAscensos.reload();

                            modal.mensaje("CONFIRMACION", response2.responseMsg);

                        }
                    }, function (data) {
                        console.info(data);
                    });
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
seApp.controller('editarDesplazamientoCtrl', ['$rootScope', '$scope', '$element', 'data', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, data, close, crud, modal) {
        var dataInput = data;
        console.log(dataInput);
        $scope.title = dataInput.title;
        $scope.opcion = dataInput.opcion;
        $scope.tiposDocumentos = dataInput.tiposDocumentos;
        $scope.tiposDocumentosRe = dataInput.tiposDocumentos;
        $scope.jornadasLaborales = dataInput.jornadasLaborales;
        $scope.tiposDesplazamientos = dataInput.tiposDesplazamientos;
        var desplazamiento = dataInput.desplazamiento;
        if(desplazamiento.tip==="1" || desplazamiento.tip==="2"){
            desplazamiento.fecTer="";
        }
        if(desplazamiento.tip==="3"){
            desplazamiento.fecIni="";
        }
        console.log(desplazamiento);
        $scope.desplazamientoSel = {
            i: desplazamiento.i,
            desId: desplazamiento.desId,
            tip: desplazamiento.tip,
            tipDocId: desplazamiento.tipDocId,
            tipDocIdIngRe: desplazamiento.tipDocId,
            tipDocIdRe: desplazamiento.tipDocId,
            numDoc: desplazamiento.numDoc,
            fecDoc: new Date(desplazamiento.fecDoc),

                    
            jorLabId: desplazamiento.jorLabId,
            jorLabIdIngRe: desplazamiento.jorLabId,
            jorLabRe: desplazamiento.tipLabId,
            fecIni: new Date(desplazamiento.fecIni),
            numDocTer: desplazamiento.numDocTer,
            fecDocTer: new Date(desplazamiento.fecDocTer),            
            fecTer: new Date(desplazamiento.fecTer),
            motRet: desplazamiento.motRet
        };
        
        $scope.actualizarDesplazamiento = function () {
            var request = crud.crearRequest('desplazamiento', 1, 'actualizarDesplxTipo');
            switch ($scope.desplazamientoSel.tip) {
                case '1':                     
                    $scope.desplazamientoSel.tipDocId = $scope.desplazamientoSel.tipDocIdIngRe;
                    $scope.desplazamientoSel.jorLabId = $scope.desplazamientoSel.jorLabIdIngRe;
                    break;
                case '2':
                    $scope.desplazamientoSel.tipDocId = $scope.desplazamientoSel.tipDocIdIngRe;
                    $scope.desplazamientoSel.jorLabId = $scope.desplazamientoSel.jorLabIdIngRe;
                    break;
                case '3':
                    $scope.desplazamientoSel.tipDocId = $scope.desplazamientoSel.tipDocIdRe;                    
                    break;
            }
            ;
            console.log($scope.desplazamientoSel);
            request.setData({desplazamiento: $scope.desplazamientoSel, opcion: $scope.desplazamientoSel.tip});
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {                    
                    var vacSel = JSON.parse(JSON.stringify(response.data));
                    console.info(response.data);
                     console.info(vacSel);
                    var datosFechaIni;
                    var datosFechaTer;
                    var datosFechaDoc;
                    
                    var fecDocFor;
                    var fecIniFor;
                    var fecTerFor;
                    
                    response.data.tipDes = buscarContenido($scope.tiposDesplazamientos, 'id', 'title', vacSel.tip);
                    response.data.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', vacSel.tipDocId);
                    response.data.jorLabDes = buscarContenido($scope.jornadasLaborales, 'id', 'nom', vacSel.jorLabId);
                    response.data.i = $scope.desplazamientoSel.i;
                    
                    
                    if(vacSel.tip==="1"||vacSel.tip==="2"){
                        console.info("entro1");
                        datosFechaIni = vacSel.fecIni.split("-");                    
                        datosFechaDoc = vacSel.fecDoc.split("-");
                        
                        fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                        fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));
                        response.data.fecIni = fecIniFor;
                        response.data.fechaDes=fecIniFor;
                        response.data.fecDoc = fecDocFor;
                        response.data.fechaDoc=fecDocFor;
                    }
                    if(vacSel.tip==="3"){
                        console.info("entro2");
                        datosFechaTer = vacSel.fecTer.split("-");                    
                        datosFechaDoc = vacSel.fecDocTer.split("-");
                        
                        fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                        fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));
                        response.data.fecTer = fecTerFor;
                        response.data.fechaDes=fecTerFor;
                        response.data.fecDocTer = fecDocFor;
                        response.data.fechaDoc=fecDocFor;
                       
                    }
                    
                    $rootScope.settingModDesplazamientos.dataset[$scope.desplazamientoSel.i] = response.data;
                    console.info(response.data);
                    //$scope.listarDesplazamientos();
                    $rootScope.tablaModDesplazamientos.reload();
                    
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarAscensoCtrl', ['$rootScope', '$scope', '$element', 'data', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, data, close, crud, modal) {
        var dataInput = data;
        console.log("miau");
        console.log(dataInput);
        $scope.title = dataInput.title;
        $scope.tiposDocumentos = dataInput.tiposDocumentos;
        //variable que servira para registrar una nueva formacion academica
        $scope.ascensoSel = {
            ascId: data.ascenso.ascId,
            tipDocId: data.ascenso.tipDocId,
            numDoc: data.ascenso.numDoc,
            fecDoc: data.ascenso.fecDoc !== "" ?
                    new Date(data.ascenso.fecDoc.substring(0, 10).split("-")[0],
                            data.ascenso.fecDoc.substring(0, 10).split("-")[1] - 1,
                            data.ascenso.fecDoc.substring(0, 10).split("-")[2]) : "",
            fecEfe: data.ascenso.fecEfe !== "" ?
                    new Date(data.ascenso.fecEfe.substring(0, 10).split("-")[0],
                            data.ascenso.fecEfe.substring(0, 10).split("-")[1] - 1,
                            data.ascenso.fecEfe.substring(0, 10).split("-")[2]) : "",
            mot: data.ascenso.mot

        };

        $scope.actualizarAscenso = function () {
            var request = crud.crearRequest('ascenso', 1, 'actualizarAscenso');
            request.setData($scope.ascensoSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $rootScope.settingModAscensos.dataset[dataInput.ascenso.i].tipDocId = response.data.tipDocId;
                    $rootScope.settingModAscensos.dataset[dataInput.ascenso.i].numDoc = response.data.numDoc;
                    $rootScope.settingModAscensos.dataset[dataInput.ascenso.i].fecDoc = response.data.fecDoc;
                    $rootScope.settingModAscensos.dataset[dataInput.ascenso.i].fecEfe = response.data.fecEfe;
                    $rootScope.settingModAscensos.dataset[dataInput.ascenso.i].mot = response.data.mot;
                    $rootScope.settingModAscensos.dataset[dataInput.ascenso.i].tipDocDes= buscarContenido($scope.tiposDocumentos, 'id', 'nom', response.data.tipDocId);
                    $rootScope.tablaModAscensos.reload();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarEstadoLaboralCtrl', ['$rootScope', '$scope', '$element', 'data', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, data, close, crud, modal) {
        var dataInput = data;
        console.log("dataInput");
        console.log(dataInput);
        $scope.opcion = dataInput.newEstLab;
        $scope.title = dataInput.title;
        $scope.tiposDocumentos = dataInput.tiposDocumentos;
        $scope.jornadasLaborales = dataInput.jornadasLaborales;
        var ft = new Date();
        ft = ft.toISOString();
        $scope.nuevoDesplazamientoRet = {
            ficEscId: dataInput.fichaEscalafonariaId,
            tip: dataInput.newEstLab,
            tipDocId: 0,
            numDocTer: "",
            fecDocTer: "",
            fecTer: ft,
            motRet: ""
        };
        $scope.nuevoDesplazamientoReIng = {
            ficEscId: dataInput.fichaEscalafonariaId,
            tip: dataInput.newEstLab,
            tipDocId: 0,
            numDoc: "",
            fecDoc: "",
            jorLabId: 0,
            fecIni: (dataInput.trabajadorInfo.fecIng !== null ? dataInput.trabajadorInfo.fecIng : "2018-01-01T00:00:00.000Z")
        };
        $scope.nuevoCese = {
            entEmi: "", 
            tipDocId: 0, 
            numDoc: "", 
            fecDoc: new Date(), 
            jorLabId: 0, 
            fecIni: new Date, 
            fecFin: new Date(), 
            ficEscId: dataInput.fichaEscalafonariaId,
            motCes: "",
            orgiId: dataInput.trabajadorInfo.orgiId,
            carId: dataInput.trabajadorInfo.carId,
            catId: dataInput.trabajadorInfo.catId,
            diasPro:0,
            mesesePro:0,
            aniosPro:0,
            tipCes:''
        };
        
        $scope.tiposDesplazamientos = [{id: "1", title: "Ingreso"}, {id: "2", title: "Re-ingreso"}, {id: "3", title: "Retiro"}];
        
        function calcularAnioProNuevoCese() {
        if($scope.nuevoCese.fecIni !== "" && $scope.nuevoCese.fecFin !== ""){
            var diaIni =$scope.nuevoCese.fecIni.getDate();
            var mesIni =$scope.nuevoCese.fecIni.getMonth()+1;
            var anioIni =$scope.nuevoCese.fecIni.getFullYear();
            
            var diaTer =$scope.nuevoCese.fecFin.getDate();
            var mesTer =$scope.nuevoCese.fecFin.getMonth()+1;
            var anioTer =$scope.nuevoCese.fecFin.getFullYear();
            var numDias360=0;
            
            if(anioTer===anioIni){

                if(mesTer===mesIni){

                    numDias360=diaTer-diaIni;

                }else{
                    if(diaTer===diaIni){
                        numDias360=(mesTer-mesIni)*30;
                    }
                    if(diaTer>diaIni){
                        numDias360=(mesTer-mesIni)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360+(mesTer-mesIni)*30-(diaIni-diaTer);
                    }
                }
                    
            }
            
            if(anioTer>anioIni){
                if(mesTer===mesIni){
                    if(diaTer===diaIni){
                        numDias360=(anioTer-anioIni)*360;
                    }
                    if(diaTer>diaIni){
                        numDias360=(anioTer-anioIni)*360+(diaTer-diaIni);
                    }
                    if(diaIni>diaTer){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=(anioTer-anioIni)*360-(diaTer-diaIni);
                    }
                }
                
                if(mesTer>mesIni){
                    numDias360=(anioTer-anioIni)*360;
                    if(diaTer===diaIni){
                        numDias360=numDias360+(mesTer-mesIni)*30;
                    }
                    if(diaTer>diaIni){
                        numDias360=numDias360+(mesTer-mesIni)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        var numD=0;
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            for(var i=mesIni; i<=mesTer;i++){
                                if(mesTer-mesIni===1 && diaIni-diaTer>0){
                                    if(mesIni===2){
                                        numD=numD+(29-(diaIni-diaTer));
                                    }else{
                                        numD=numD+(30-(diaIni-diaTer));
                                    }    
                                }else{
                                    numD=numD+30;
                                }
                                
                            }
                        }else{
                            for(var i=mesIni; i<=mesTer;i++){
                                if(mesTer-mesIni===1 && diaIni-diaTer>0){
                                    if(mesIni===2){
                                        numD=numD+(28-(diaIni-diaTer));
                                    }else{
                                        numD=numD+(30-(diaIni-diaTer));
                                    }    
                                }else{
                                    numD=numD+30;
                                }
                                
                            }
                        }
                        
                        numDias360=numDias360+numD;
                    }
                    
                    
                }
                
                if(mesTer<mesIni){
                    numDias360=(anioTer-anioIni)*360;
                    if(diaTer===diaIni){
                        numDias360=numDias360-(mesIni-mesTer)*30;
                    }
                    if(diaTer>diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360-(mesIni-mesTer)*30+(diaTer-diaIni);
                    }
                    if(diaTer<diaIni){
                        if (((anioTer % 4 === 0)&&(anioTer % 100 !== 0 ))||(anioTer % 400 === 0)){
                            if(mesIni===2 && diaIni===29){
                                numDias360=numDias360-1;
                            }
                        }else{
                            if(mesIni===2 && diaIni===28){
                                numDias360=numDias360-2;
                            }
                        }
                        if((mesIni===1||mesIni===3||mesIni===5||mesIni===7||mesIni===8||mesIni===10||mesIni===12) && diaIni===31){
                                numDias360=numDias360+1;
                        }
                        numDias360=numDias360-(mesIni-mesTer)*30-(diaIni-diaTer);
                    }
                }
                    
            }
            numDias360=numDias360-1;
            
            var diaTran=numDias360%30;
            var mesTran=Math.trunc(numDias360/30)%12;
            var anioTran=Math.trunc(Math.trunc(numDias360/30)/12);
            
        }
            $scope.nuevoCese.diasPro= diaTran;
            $scope.nuevoCese.mesesPro= mesTran;
            $scope.nuevoCese.aniosPro= anioTran;
            
        };
        $scope.agregarDesplazamiento = function () {
            var request = crud.crearRequest('desplazamiento', 1, 'agregarDesplxTipo');
            if ($scope.opcion === '2'){
                request.setData({opcion: 2, desplazamiento: $scope.nuevoDesplazamientoReIng, traId: dataInput.trabajadorInfo.traId, orgiId:dataInput.trabajadorInfo.orgiId, carId: dataInput.trabajadorInfo.carId,catId: dataInput.trabajadorInfo.catId});
                $scope.nuevoCese.tipDocId = $scope.nuevoDesplazamientoReIng.tipDocId;
                $scope.nuevoCese.numDoc = $scope.nuevoDesplazamientoReIng.numDoc;
                $scope.nuevoCese.jorLabId = $scope.nuevoDesplazamientoReIng.jorLabId;
                console.log("Retiro");
                console.log($rootScope.settingModDesplazamientos.dataset[$rootScope.settingModDesplazamientos.dataset.length-1]);
                $scope.nuevoCese.fecIni = new Date($rootScope.settingModDesplazamientos.dataset[$rootScope.settingModDesplazamientos.dataset.length-1].fecTer);
                $scope.nuevoCese.fecFin =  $scope.nuevoDesplazamientoReIng.fecIni;
                $scope.nuevoCese.tipCes = '1';
                calcularAnioProNuevoCese();
                
                console.log($scope.nuevoDesplazamientoReIng);
            }else{
                request.setData({opcion: 3, desplazamiento: $scope.nuevoDesplazamientoRet, traId: dataInput.trabajadorInfo.traId, orgiId:dataInput.trabajadorInfo.orgiId, carId: dataInput.trabajadorInfo.carId,catId: dataInput.trabajadorInfo.catId});
                console.log($scope.nuevoDesplazamientoRet);
            }
            crud.insertar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    response.data.tipDes = buscarContenido($scope.tiposDesplazamientos, 'id', 'title', response.data.tip);
                    response.data.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', response.data.tipDocId);
                    response.data.jorLabDes = buscarContenido($scope.jornadasLaborales, 'id', 'nom', response.data.jorLabId);
                    response.data.i = $rootScope.settingModDesplazamientos.dataset.length;
                    $rootScope.settingDatCenLab.dataset[dataInput.trabajadorInfo.i].estLab = response.data.tip;
                    $rootScope.tablaDatCenLab.reload();

                    $rootScope.settingModDesplazamientos.dataset.push(response.data);
                    $rootScope.tablaModDesplazamientos.sorting({desId:'asc'});
                    $rootScope.tablaModDesplazamientos.page(1);
                    $rootScope.tablaModDesplazamientos.reload();
                    if ($scope.opcion === '2') {
                        modal.mensaje("CONFIRMACION", "El reingreso fue registrado con éxito");
                        var request = crud.crearRequest('cese', 1, 'agregarCese');
                        request.setData($scope.nuevoCese);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            if (response.responseSta) {
                                var ceseAgre = JSON.parse(JSON.stringify(response));

                                var datosFechaTer = ceseAgre.data.fecTer.split("-");
                                var datosFechaIni = ceseAgre.data.fecIni.split("-");
                                var datosFechaDoc = ceseAgre.data.fecDoc.split("-");

                                var fecTerFor = new Date(parseInt(datosFechaTer[2]), parseInt(datosFechaTer[1]) - 1, parseInt(datosFechaTer[0]));
                                var fecIniFor = new Date(parseInt(datosFechaIni[2]), parseInt(datosFechaIni[1]) - 1, parseInt(datosFechaIni[0]));
                                var fecDocFor = new Date(parseInt(datosFechaDoc[2]), parseInt(datosFechaDoc[1]) - 1, parseInt(datosFechaDoc[0]));

                                response.data.fecIni = fecIniFor;
                                response.data.fecTer = fecTerFor;
                                response.data.fecDoc = fecDocFor;

                                response.data.tipDocDes = buscarContenido($scope.tiposDocumentos, 'id', 'nom', ceseAgre.data.tipDocId);
                                response.data.jorLabDes = buscarContenido($scope.jornadasLaborales, 'id', 'nom', ceseAgre.data.jorLabId);
                                response.data.i = $rootScope.settingModCeses.dataset.length===undefined?0:$rootScope.settingModCeses.dataset.length;
                                $rootScope.settingModCeses.dataset.push(response.data);                    
                                $rootScope.tablaModCeses.sorting({cesId:'asc'});
                                $rootScope.tablaModCeses.page(1);
                                $rootScope.tablaModCeses.reload();

                                modal.mensaje("CONFIRMACION", response.responseMsg);
                            }
                        }, function (data) {
                            console.info(data);
                        });
                        
                    } else {
                        modal.mensaje("CONFIRMACION", "El retiro fue registrado con éxito");
                    }
                } else {
                    if ($scope.opcion === 2) {
                        modal.mensaje("ERROR", "El reingreso no fue registrado");
                    } else {
                        modal.mensaje("ERROR", "El retiro no fue registrado");
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);

