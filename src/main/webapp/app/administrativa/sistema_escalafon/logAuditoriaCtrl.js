app.controller("logAuditoriaCtrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {
        $rootScope.paramsCatalogo = {count: 9};
        $rootScope.settingCatalogo = {counts: []};
        $rootScope.tablaCatalogo = new NgTableParams($rootScope.paramsCatalogo, $rootScope.settingCatalogo);
        $scope.audit_user = [];
        $scope.audit_user.nom = "";
        $scope.audit_user.ape_pat = "";
        $scope.audit_user.ape_mat = "";
        $scope.audit_user.dni = "";
        $scope.dni = "";
        $scope.fec_ini = "";
        $scope.fec_fin = "";
        //////////////////////////LISTAR LOGS DE AUDITORIA//////////////////////
        $scope.listarRegistros = function () {
            var request = crud.crearRequest('auditoria', 1, 'listarRegistros');
            request.setData();    
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingCatalogo.dataset = data.data;
                iniciarPosiciones($rootScope.settingCatalogo.dataset);
                $rootScope.tablaCatalogo.settings($rootScope.settingCatalogo);
            }, function (data) {
                console.info(data);
            });
        };
        //////////////////////////LISTAR LOGS DE AUDITORIA//////////////////////
        $scope.getUsuarioInfo = function (userId) {
            var request = crud.crearRequest('auditoria',1,'getDatosAuditoriaUsuario');
            request.setData({
                    usu_id:userId
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.audit_user.nom = response.data.usuario.nom;
                    $scope.audit_user.ape_pat = response.data.usuario.ape_pat;
                    $scope.audit_user.ape_mat = response.data.usuario.ape_mat;
                    $scope.audit_user.dni = response.data.usuario.dni;
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo obtener datos auditoria usuario ejecutor');
            });
        };
        //////////////////////////DESCARGAR REPORTE DE AUDITORIA//////////////////////
        $scope.descargarReporte = function () {
            console.info( "DNI: ",$scope.dni);
            console.info( "FEC_INI: ",$scope.fec_ini);
            console.info( "FEC_FIN: ",$scope.fec_fin);
            var request = crud.crearRequest('auditoria',1,'reporteAuditoria');
                request.setData({
                        dni:$scope.dni,
                        fec_ini:$scope.fec_ini,
                        fec_fin:$scope.fec_fin
                });
                crud.insertar('/sistema_escalafon',request,function(response){
                    if(response.responseSta){
                        verDocumentoPestaña(response.data.file);
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(errResponse){
                    modal.mensaje('ERROR','El servidor no responde');
                }); 
            //////////////////////////////////////////////////////////
            
        };

    }]);