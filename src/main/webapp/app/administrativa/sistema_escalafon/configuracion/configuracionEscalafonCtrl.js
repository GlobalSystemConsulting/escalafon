app.requires.push('angularModalService');
app.requires.push('ngAnimate');
var seApp = angular.module('app');
seApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/editar_planillas/:data',{
            templateUrl:'administrativa/sistema_escalafon/configuracion/editarPlanillas.html',
            controller:'editarPlanillasConfiguracionCtrl',
            controllerAs:'editarPlanillasCtrl'
        });
}]);        
app.controller('configuracionEscalafonCtrl', ["$location", '$scope', '$rootScope', '$http', 'NgTableParams', 'crud', 'modal', 'ModalService',function ($location, $scope, $rootScope, $http, NgTableParams, crud, modal, ModalService){
//////PLANILLAS//////////////////////////////////////////////////////////////////////////////////////////////////
//////PLANILLAS//////////////////////////////////////////////////////////////////////////////////////////////////
//////PLANILLAS//////////////////////////////////////////////////////////////////////////////////////////////////
        $rootScope.paramsPlanillas = {count: 10};
        $rootScope.settingPlanillas = {counts: []};
        $rootScope.tablaPlanillas = new NgTableParams($rootScope.paramsPlanillas, $rootScope.settingPlanillas);

        $scope.item_planilla= {plaId:"",codPla:"",abrPla:"",nomPla:""};
        $scope.limpiarFormularioPlanilla = function(){$scope.item_planilla.plaId = "";$scope.item_planilla.codPla = "";$scope.item_planilla.abrPla = "";$scope.item_planilla.nomPla = "";};
        $scope.cargarFormularioPlanilla = function(a){$scope.item_planilla.plaId = a.plaId;$scope.item_planilla.codPla = a.codPla;$scope.item_planilla.abrPla = a.abrPla;$scope.item_planilla.nomPla = a.nomPla;};

        $scope.listarPlanillas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('planillaConfiguracion', 1, 'listarPlanillas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingPlanillas.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingPlanillas.dataset);
                $rootScope.tablaPlanillas.settings($rootScope.settingPlanillas);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.eliminarPlanilla = function(c){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la Planilla?", function () {
                var request = crud.crearRequest('planillaConfiguracion', 1, 'eliminarPlanillas');
                request.setData({plaId: c.plaId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaPlanillas.settings().dataset, function(item){
                            return c === item;
                        });
                        $rootScope.tablaPlanillas.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        $scope.procesarPlanilla = function(){
            if($scope.item_planilla.plaId===""){
                $scope.agregarPlanilla();
            }else{
                $scope.actualizarPlanilla();
            }
            $('#modalnuevoplanilla').modal('hide');
        };
        $scope.agregarPlanilla = function(){
            var request = crud.crearRequest('planillaConfiguracion',1,'agregarPlanillas');
            request.setData({
                    codPla:$scope.item_planilla.codPla,
                    abrPla:$scope.item_planilla.abrPla,
                    nomPla:$scope.item_planilla.nomPla
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarPlanillas();
                    $rootScope.tablaPlanillas.reload();
                    modal.mensaje("CONFIRMACION", "Se Agrego Planilla");
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo almacenar Planilla');
            }); 
             
        };
        $scope.actualizarPlanilla = function(){            
            var request = crud.crearRequest('planillaConfiguracion',1,'actualizarPlanillas');
            request.setData({
                    plaId:$scope.item_planilla.plaId,
                    codPla:$scope.item_planilla.codPla,
                    abrPla:$scope.item_planilla.abrPla,
                    nomPla:$scope.item_planilla.nomPla
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarPlanillas();
                    $rootScope.tablaPlanillas.reload();
                    modal.mensaje("CONFIRMACION", "Se Actualizo Planilla");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo actualizar planilla');
            }); 
             
        };
        
        
//////CARGO//////////////////////////////////////////////////////////////////////////////////////////////////
//////CARGO//////////////////////////////////////////////////////////////////////////////////////////////////
//////CARGO//////////////////////////////////////////////////////////////////////////////////////////////////
        $rootScope.paramsCargos = {count: 10};
        $rootScope.settingCargos = {counts: []};
        $rootScope.tablaCargos = new NgTableParams($rootScope.paramsCargos, $rootScope.settingCargos);
        
        $scope.item_cargo= {carId:"",codCar:"",nomCar:""};
        $scope.limpiarFormularioCargo = function(){$scope.item_cargo.carId = "";$scope.item_cargo.codCar = "";$scope.item_cargo.nomCar = "";};
        $scope.cargarFormularioCargo = function(a){$scope.item_cargo.carId = a.carId;$scope.item_cargo.codCar = a.codCar;$scope.item_cargo.nomCar = a.nomCar;};

        $scope.listarCargos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('cargoConfiguracion', 1, 'listarCargos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingCargos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingCargos.dataset);
                $rootScope.tablaCargos.settings($rootScope.settingCargos);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.eliminarCargo = function(c){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el cargo?", function () {
                var request = crud.crearRequest('cargoConfiguracion', 1, 'eliminarCargos');
                request.setData({carId: c.carId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaCargos.settings().dataset, function(item){
                            return c === item;
                        });
                        $rootScope.tablaCargos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        $scope.procesarCargo = function(){
            if($scope.item_cargo.carId===""){
                $scope.agregarCargo();
            }else{
                $scope.actualizarCargo();
            }
            $('#modalnuevocargo').modal('hide');
        };
        $scope.agregarCargo = function(){
            var request = crud.crearRequest('cargoConfiguracion',1,'agregarCargos');
            request.setData({
                    codCar:$scope.item_cargo.codCar,
                    nomCar:$scope.item_cargo.nomCar
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarCargos();
                    $rootScope.tablaCargos.reload();
                    modal.mensaje("CONFIRMACION", "Se Agrego Cargo");
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo almacenar Cargo');
            }); 
             
        };
        $scope.actualizarCargo = function(){            
            var request = crud.crearRequest('cargoConfiguracion',1,'actualizarCargos');
            request.setData({
                    carId:$scope.item_cargo.carId,
                    codCar:$scope.item_cargo.codCar,
                    nomCar:$scope.item_cargo.nomCar
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarCargos();
                    $rootScope.tablaCargo.reload();
                    modal.mensaje("CONFIRMACION", "Se Actualizo el cargo");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo actualizar cargo');
            }); 
             
        };
//////CATEGORIA//////////////////////////////////////////////////////////////////////////////////////////////////
//////CATEGORIA//////////////////////////////////////////////////////////////////////////////////////////////////
//////CATEGORIA//////////////////////////////////////////////////////////////////////////////////////////////////
        $rootScope.paramsCategorias = {count: 10};
        $rootScope.settingCategorias = {counts: []};
        $rootScope.tablaCategorias = new NgTableParams($rootScope.paramsCategorias, $rootScope.settingCategorias);
        
        $scope.item_categoria= {catId:"",abrCat:"",nomCat:"",plaCat:""};
        $scope.limpiarFormularioCategoria = function(){$scope.item_categoria.catId = "";$scope.item_categoria.abrCat = "";$scope.item_categoria.nomCat = "";$scope.item_categoria.plaCat = "";};
        $scope.cargarFormularioCategoria = function(a){$scope.item_categoria.catId = a.catId;$scope.item_categoria.abrCat = a.abrCat;$scope.item_categoria.nomCat = a.nomCat;$scope.item_categoria.plaNom = a.plaCat;};

        $scope.listarCategorias = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('categoriaConfiguracion', 1, 'listarCategorias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingCategorias.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingCategorias.dataset);
                $rootScope.tablaCategorias.settings($rootScope.settingCategorias);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.eliminarCategoria = function(c){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la Categoria?", function () {
                var request = crud.crearRequest('categoriaConfiguracion', 1, 'eliminarCategorias');
                request.setData({catId: c.catId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaCategorias.settings().dataset, function(item){
                            return c === item;
                        });
                        $rootScope.tablaCategorias.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        $scope.procesarCategoria = function(){
            if($scope.item_categoria.catId===""){
                $scope.agregarCategoria();
            }else{
                $scope.actualizarCategoria();
            }
            $('#modalnuevocategoria').modal('hide');
        };
        $scope.agregarCategoria = function(){
            var request = crud.crearRequest('categoriaConfiguracion',1,'agregarCategorias');
            if($scope.item_categoria.plaCat===-1 || $scope.item_categoria.plaCat===null||$scope.item_categoria.plaCat==="")
                    $scope.item_categoria.plaCat=0;
            console.info($scope.item_categoria.plaCat);    
            request.setData({
                    abrCat:$scope.item_categoria.abrCat,
                    nomCat:$scope.item_categoria.nomCat,
                    plaCat:$scope.item_categoria.plaCat
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarCategorias();
                    $rootScope.tablaCategorias.reload();
                    modal.mensaje("CONFIRMACION", "Se Agrego Catergoria");
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo almacenar Categoria');
            }); 
             
        };
        $scope.actualizarCategoria = function(){            
            var request = crud.crearRequest('categoriaConfiguracion',1,'actualizarCategorias');
             if($scope.item_categoria.plaCat===-1 || $scope.item_categoria.plaCat===null||$scope.item_categoria.plaCat==="")
                    $scope.item_categoria.plaCat=0;
            console.info($scope.item_categoria.plaCat); 
            request.setData({
                    catId:$scope.item_categoria.catId,
                    abrCat:$scope.item_categoria.abrCat,
                    nomCat:$scope.item_categoria.nomCat,
                    plaCat:$scope.item_categoria.plaCat
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarCategorias();
                    $rootScope.tablaCategorias.reload();
                    modal.mensaje("CONFIRMACION", "Se Actualizo Categoria");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo actualizar categoria');
            }); 
             
        };
//////ORGANIZACION//////////////////////////////////////////////////////////////////////////////////////////////////
//////ORGANIZACION//////////////////////////////////////////////////////////////////////////////////////////////////
//////ORGANIZACION//////////////////////////////////////////////////////////////////////////////////////////////////
        $rootScope.paramsOrganizaciones = {count: 10};
        $rootScope.settingOrganizaciones = {counts: []};
        $rootScope.tablaOrganizaciones = new NgTableParams($rootScope.paramsOrganizaciones, $rootScope.settingOrganizaciones);

        $scope.listarOrganizaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('organizacionConfiguracion', 1, 'listarOrganizaciones');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingOrganizaciones.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingOrganizaciones.dataset);
                $rootScope.tablaOrganizaciones.settings($rootScope.settingOrganizaciones);
            }, function (data) {
                console.info(data);
            });
        };
//////ZONA//////////////////////////////////////////////////////////////////////////////////////////////////
//////ZONA//////////////////////////////////////////////////////////////////////////////////////////////////
//////ZONA//////////////////////////////////////////////////////////////////////////////////////////////////

        $rootScope.paramsZonas = {count: 10};
        $rootScope.settingZonas = {counts: []};
        $rootScope.tablaZonas = new NgTableParams($rootScope.paramsZonas, $rootScope.settingZonas);
        
        $scope.item_zona= {zonaId:"",codZon:"",nomZon:""};
        $scope.limpiarFormularioZona = function(){$scope.item_zona.zonaId = "";$scope.item_zona.codZon = "";$scope.item_zona.nomZon = "";};
        $scope.cargarFormularioZona = function(a){$scope.item_zona.zonaId = a.zonaId;$scope.item_zona.codZon = a.codZon;$scope.item_zona.nomZon = a.nomZon;};

        $scope.listarZonas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('zonaConfiguracion', 1, 'listarZonas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingZonas.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingZonas.dataset);
                $rootScope.tablaZonas.settings($rootScope.settingZonas);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.eliminarZona = function(c){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la Zona?", function () {
                var request = crud.crearRequest('zonaConfiguracion', 1, 'eliminarZonas');
                request.setData({zonaId: c.zonaId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaZonas.settings().dataset, function(item){
                            return c === item;
                        });
                        $rootScope.tablaZonas.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        $scope.procesarZona = function(){
            if($scope.item_zona.zonaId===""){
                $scope.agregarZona();
            }else{
                $scope.actualizarZona();
            }
            $('#modalnuevozona').modal('hide');
        };
        $scope.agregarZona = function(){
            var request = crud.crearRequest('zonaConfiguracion',1,'agregarZonas');
            request.setData({
                    codZon:$scope.item_zona.codZon,
                    nomZon:$scope.item_zona.nomZon
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarZonas();
                    $rootScope.tablaZonas.reload();
                    modal.mensaje("CONFIRMACION", "Se Agrego Zona");
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo almacenar Zona');
            }); 
             
        };
        $scope.actualizarZona = function(){            
            var request = crud.crearRequest('zonaConfiguracion',1,'actualizarZonas');
            request.setData({
                    zonaId:$scope.item_zona.zonaId,
                    codZon:$scope.item_zona.codZon,
                    nomZon:$scope.item_zona.nomZon
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarZonas();
                    $rootScope.tablaZonas.reload();
                    modal.mensaje("CONFIRMACION", "Se Actualizo Zona");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo actualizar zona');
            }); 
             
        };
        
//////ORGANISMO//////////////////////////////////////////////////////////////////////////////////////////////////
//////ORGANISMO//////////////////////////////////////////////////////////////////////////////////////////////////
//////ORGANISMO//////////////////////////////////////////////////////////////////////////////////////////////////
        $rootScope.paramsOrganismos = {count: 10};
        $rootScope.settingOrganismos = {counts: []};
        $rootScope.tablaOrganismos = new NgTableParams($rootScope.paramsOrganismos, $rootScope.settingOrganismos);

        $scope.item_organismo= {orgaId:"",codOrga:"",nomOrga:""};
        $scope.limpiarFormularioOrganismo = function(){console.info("A");$scope.item_organismo.orgaId = "";$scope.item_organismo.codOrga = "";$scope.item_organismo.nomOrga = "";};
        $scope.cargarFormularioOrganismo = function(a){console.info(a);console.info("B");$scope.item_organismo.orgaId = a.orgaId;$scope.item_organismo.codOrga = a.codOrga;$scope.item_organismo.nomOrga = a.nomOrga;};
        
        $scope.listarOrganismos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('organismoConfiguracion', 1, 'listarOrganismos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingOrganismos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingOrganismos.dataset);
                $rootScope.tablaOrganismos.settings($rootScope.settingOrganismos);
                //console.info("Length Organismos: ",$rootScope.settingOrganismos.dataset.length);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.eliminarOrganismo = function(c){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el Organismo?", function () {
                var request = crud.crearRequest('organismoConfiguracion', 1, 'eliminarOrganismos');
                request.setData({orgaId: c.orgaId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaOrganismos.settings().dataset, function(item){
                            return c === item;
                        });
                        $rootScope.tablaOrganismos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        $scope.procesarOrganismo = function(){
            if($scope.item_organismo.orgaId===""){
                $scope.agregarOrganismo();console.info("C");
            }else{
                $scope.actualizarOrganismo();console.info("D");
            }
            $('#modalnuevoorganismo').modal('hide');
        };
        $scope.agregarOrganismo = function(){
            var request = crud.crearRequest('organismoConfiguracion',1,'agregarOrganismos');
            request.setData({
                    codOrga:$scope.item_organismo.codOrga,
                    nomOrga:$scope.item_organismo.nomOrga
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarOrganismos();
                    $rootScope.tablaOrganismos.reload();
                    modal.mensaje("CONFIRMACION", "Se Agrego Organismo");
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo almacenar Organismo');
            }); 
             
        };
        $scope.actualizarOrganismo = function(){            
            var request = crud.crearRequest('organismoConfiguracion',1,'actualizarOrganismos');
            request.setData({
                    orgaId:$scope.item_organismo.orgaId,
                    codOrga:$scope.item_organismo.codOrga,
                    nomOrga:$scope.item_organismo.nomOrga
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarOrganismos();
                    $rootScope.tablaOrganismos.reload();
                    modal.mensaje("CONFIRMACION", "Se Actualizo Organismo");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo actualizar organismo');
            }); 
             
        };
//////FACULTAD//////////////////////////////////////////////////////////////////////////////////////////////////
//////FACULTAD//////////////////////////////////////////////////////////////////////////////////////////////////
//////FACULTAD//////////////////////////////////////////////////////////////////////////////////////////////////
        $rootScope.paramsFacultades = {count: 10};
        $rootScope.settingFacultades = {counts: []};
        $rootScope.tablaFacultades = new NgTableParams($rootScope.paramsFacultades, $rootScope.settingFacultades);

        $scope.item_facultad= {facId:"",codFac:"",nomFac:"",orgaId:""};
        $scope.limpiarFormularioFacultad = function(){$scope.item_facultad.facId = "";$scope.item_facultad.codFac = "";$scope.item_facultad.nomFac = "";$scope.item_facultad.orgaId = "";};
        $scope.cargarFormularioFacultad = function(a){$scope.item_facultad.facId = a.facId;$scope.item_facultad.codFac = a.codFac;$scope.item_facultad.nomFac = a.nomFac;$scope.item_facultad.orgaId = a.orgaId;};

        $scope.listarFacultades = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('facultadConfiguracion', 1, 'listarFacultades');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingFacultades.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingFacultades.dataset);
                $rootScope.tablaFacultades.settings($rootScope.settingFacultades);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.eliminarFacultad = function(c){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la Facultad?", function () {
                var request = crud.crearRequest('facultadConfiguracion', 1, 'eliminarFacultades');
                request.setData({facId: c.facId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaFacultades.settings().dataset, function(item){
                            return c === item;
                        });
                        $rootScope.tablaFacultades.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        $scope.procesarFacultad = function(){
            if($scope.item_facultad.facId===""){
                $scope.agregarFacultad();
            }else{
                $scope.actualizarFacultad();
            }
            $('#modalnuevofacultad').modal('hide');
        };
        $scope.agregarFacultad = function(){
            var request = crud.crearRequest('facultadConfiguracion',1,'agregarFacultades');
            request.setData({
                    codFac:$scope.item_facultad.codFac,
                    nomFac:$scope.item_facultad.nomFac,
                    orgaId:$scope.item_facultad.orgaId
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarFacultades();
                    $rootScope.tablaFacultades.reload();
                    modal.mensaje("CONFIRMACION", "Se Agrego Facultad");
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo almacenar Facultad');
            }); 
             
        };
        $scope.actualizarFacultad = function(){            
            var request = crud.crearRequest('facultadConfiguracion',1,'actualizarFacultades');
            request.setData({
                    facId:$scope.item_facultad.facId,
                    codFac:$scope.item_facultad.codFac,
                    nomFac:$scope.item_facultad.nomFac,
                    orgaId:$scope.item_facultad.orgaId
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarFacultades();
                    $rootScope.tablaFacultades.reload();
                    modal.mensaje("CONFIRMACION", "Se Actualizo Facultad");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo actualizar facultad');
            }); 
             
        };
//////DEPENDENCIA//////////////////////////////////////////////////////////////////////////////////////////////////
//////DEPENDENCIA//////////////////////////////////////////////////////////////////////////////////////////////////
//////DEPENDENCIA//////////////////////////////////////////////////////////////////////////////////////////////////
        $rootScope.paramsDependencias = {count: 10};
        $rootScope.settingDependencias = {counts: []};
        $rootScope.tablaDependencias = new NgTableParams($rootScope.paramsDependencias, $rootScope.settingDependencias);

        $scope.item_dependencia= {depId:"",codDep:"",nomDep:""};
        $scope.limpiarFormularioDependencia = function(){$scope.item_dependencia.depId = "";$scope.item_dependencia.codDep = "";$scope.item_dependencia.nomDep = "";$scope.item_dependencia.facId = "";};
        $scope.cargarFormularioDependencia = function(a){$scope.item_dependencia.depId = a.depId;$scope.item_dependencia.codDep = a.codDep;$scope.item_dependencia.nomDep = a.nomDep;$scope.item_dependencia.facId = a.facId;};

        $scope.listarDependencias = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('dependenciaConfiguracion', 1, 'listarDependencias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingDependencias.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingDependencias.dataset);
                $rootScope.tablaDependencias.settings($rootScope.settingDependencias);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.eliminarDependencia = function(c){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la Zona?", function () {
                var request = crud.crearRequest('dependenciaConfiguracion', 1, 'eliminarDependencias');
                request.setData({depId: c.depId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaDependencias.settings().dataset, function(item){
                            return c === item;
                        });
                        $rootScope.tablaDependencias.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        $scope.procesarDependencia = function(){
            if($scope.item_dependencia.depId===""){
                $scope.agregarDependencia();
            }else{
                $scope.actualizarDependencia();
            }
            $('#modalnuevodependencia').modal('hide');
        };
        $scope.agregarDependencia = function(){
            var request = crud.crearRequest('dependenciaConfiguracion',1,'agregarDependencias');
            request.setData({
                    codDep:$scope.item_dependencia.codDep,
                    nomDep:$scope.item_dependencia.nomDep,
                    facId:$scope.item_dependencia.facId
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarDependencias();
                    $rootScope.tablaDependencias.reload();
                    modal.mensaje("CONFIRMACION", "Se Agrego Dependencia");
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo almacenar Dependencia');
            }); 
             
        };
        $scope.actualizarDependencia = function(){            
            var request = crud.crearRequest('dependenciaConfiguracion',1,'actualizarDependencias');
            request.setData({
                    depId:$scope.item_dependencia.depId,
                    codDep:$scope.item_dependencia.codDep,
                    nomDep:$scope.item_dependencia.nomDep,
                    facId:$scope.item_dependencia.facId
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarDependencias();
                    $rootScope.tablaDependencias.reload();
                    modal.mensaje("CONFIRMACION", "Se Actualizo Dependencia");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo actualizar dependencia');
            }); 
             
        };
//////Servicio URL//////////////////////////////////////////////////////////////////////////////////////////////////
//////Servicio URL//////////////////////////////////////////////////////////////////////////////////////////////////
//////Servicio URL//////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        $scope.editar=true;
        $scope.verDireccion=function(){
            $rootScope.showLoading();
            var request = crud.crearRequest('servicioRidea', 1, 'verServicioRIDEA');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
                crud.listar("/sistema_escalafon", request, function (data) {
                    if(data.data){
                        $scope.servicio=data.data.servicio;
                        $scope.conec=data.data.com;
                        $rootScope.hideLoading();
                    }
                    else{
                         modal.mensaje("ERROR", "No se pudo actualizar la dirección");
                    }
                }, function (data) {
                console.info(data);
            });
        };
        $scope.editarServicio=function(){         
            $scope.editar=false;
            
        };
        $scope.guardarServicio=function(a){
            $rootScope.showLoading();
             $scope.editar=true;
             console.info(a);
             var request = crud.crearRequest('servicioRidea',1,'actualizarServicioRIDEA');
            request.setData({
                    servicio:a                    
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $rootScope.hideLoading();
                    $scope.verDireccion();
                    modal.mensaje("CONFIRMACION", "Se Actualizo Dirección de Servicio");
                }
                else{
                    $rootScope.hideLoading();
                    modal.mensaje("ERROR", "No se pudo actualizar la dirección");
                }
            }, function (data) {
                console.info(data);
                    
            });    
        };
        
    }]);

//////------//////////////////////////////////////////////////////////////////////////////////////////////////
//////------//////////////////////////////////////////////////////////////////////////////////////////////////
//////------//////////////////////////////////////////////////////////////////////////////////////////////////
/*app.controller('editarPlanillasConfiguracionCtrl', ['$routeParams', '$scope', '$rootScope', '$http', 'NgTableParams', 'crud','modal', 'ModalService', function ($routeParams, $scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {
     
}]);*/
