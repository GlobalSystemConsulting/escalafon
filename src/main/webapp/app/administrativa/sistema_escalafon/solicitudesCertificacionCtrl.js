app.controller("solicitudesCertificacionCtrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {
        $rootScope.paramsMisConsultas = {count: 10};
        $rootScope.settingMisConsultas = {counts: []};
        $rootScope.tablaMisConsultasCertificadas = new NgTableParams($rootScope.paramsMisConsultas, $rootScope.settingConsultas);
        //////////////////////////LISTAR CONSULTAS VERIFICADAS Y NO VERIFICADAS//////////////////////
        $scope.listarMisSolicitudesCertificacion = function () {
            var request = crud.crearRequest('reportes', 1, 'listarSolicitudesCertificacionxUsuario');
            request.setData({
                    usuId:$rootScope.usuMaster.usuario.usuarioID
            });    
            crud.listar("/sistema_escalafon", request, function (data) {
                if(data.responseSta){
                    $rootScope.settingMisConsultas.dataset = data.data;
                    iniciarPosiciones($rootScope.settingMisConsultas.dataset);
                    $rootScope.tablaMisConsultasCertificadas.settings($rootScope.settingMisConsultas);
                }else{
                    modal.mensaje('ERROR',data.responseMsg);
                }
                
            }, function (data) {
                console.info(data);
            });
        };
        //////////////////////////LISTAR CONSULTAS VERIFICADAS Y NO VERIFICADAS//////////////////////
        var procesarConsulta = function(d,passwordCert){
            //var trabajadorSel = JSON.parse(JSON.stringify(t));
            var cab = JSON.parse(d.atr_usa);
            var request = crud.crearRequest('reportes',1,'reporteConsultaGeneral');
            ///////////////CARGAMOS LAS CABECERAS DEL REPORTE///////////////////
            var cabeceras = [];
            cabeceras.push({item_cabecera:"N°"});///AGREGAMOS LA NUMERACION A LAS FILAS DEL REPORTE
            for(x in cab){
                    for(y in cab[x].atributos){
                        if(cab[x].atributos[y].show){//solo si esta marcado para mostrar con el switch lo coloca en cabeceras sino no lo coloca en el query
                            cabeceras.push({item_cabecera:cab[x].atributos[y].alias_name});
                        }
                    }
                }
            ////////////////////////////////////////////////////////////////////
            request.setData({
                    id:d.con_cer_id,
                    titulo:d.tit,
                    observaciones_reporte:d.obs,
                    consulta:d.sql_que,
                    cabeceras:cabeceras,//envia las cabeceras generadas aqui en la variable cabeceras no usa la de d
                    idCertificacion:passwordCert
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    descargarArchivo(response.data.file, "ConsultaCertificada_"+d.tit, "pdf");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            });
        };
        //////////////////////////LISTAR CONSULTAS VERIFICADAS Y NO VERIFICADAS//////////////////////
        $scope.descagarConsultaCertificada = function(d){
            modal.mensajeConfirmacion($scope, "¿Desea descargar la consulta certificada?, Desactive el bloqueo de pop-up, solo se puede descargar la consulta certificada una vez.", function () {
                procesarConsulta(d,"555125");
                var JSONcon_cer_id = d.con_cer_id;
                var request = crud.crearRequest('reportes',1,'actualizarEstadoConsulta');////CREARPROC
                request.setData({
                        con_cer_id:JSONcon_cer_id
                });
                crud.actualizar('/sistema_escalafon',request,function(response){
                    if(response.responseSta){
                        $scope.listarMisSolicitudesCertificacion();
                        $rootScope.tablaMisConsultasCertificadas.reload();
                        modal.mensaje("CONFIRMACION", "Se proceso la consulta con exito");
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(error){
                    modal.mensaje('ERROR','El servidor no responde No se pudo procesar la consulta');
                });
                
                
            }, '400'); 
        };
        //////////////////////////SOLICITAR NUEVAMENTE CERTIFICACION//////////////////////
        $scope.solicitarDeNuevo = function(d){
            modal.mensajeConfirmacion($scope, "¿Desea solicitar certificacion nuevamente?.", function () {
                //procesarConsulta(d,"555125");
                var JSONcon_cer_id = d.con_cer_id;
                var request = crud.crearRequest('reportes',1,'actualizarEstadoConsultaCertificarNuevamente');////CREARPROC
                request.setData({
                        con_cer_id:JSONcon_cer_id
                });
                crud.actualizar('/sistema_escalafon',request,function(response){
                    if(response.responseSta){
                        $scope.listarMisSolicitudesCertificacion();
                        $rootScope.tablaMisConsultasCertificadas.reload();
                        modal.mensaje("CONFIRMACION", "Se solicito certificacion con exito");
                    }else{
                        modal.mensaje('ERROR',response.responseMsg);
                    }
                },function(error){
                    modal.mensaje('ERROR','El servidor no responde No se pudo solicitar certificacion');
                });
                
                
            }, '400'); 
        };
        
    }]);