app.controller("gestorCatalogosCtrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {
        /*
         * Item catalogo
         */
        $scope.item_catalogo= {
            id_met:"",
            entity_name:"",
            atrib_name:"",
            alias_name:"",
            data_type:"",
            mod:""
        };
        
        $rootScope.paramsCatalogo = {count: 10};
        $rootScope.settingCatalogo = {counts: []};
        $scope.tablas = [];
        $scope.atributos = [];
        $scope.metadata2;
        $rootScope.tablaCatalogo = new NgTableParams($rootScope.paramsCatalogo, $rootScope.settingCatalogo);
        //////////////////////////LISTAR CONSULTAS VERIFICADAS Y NO VERIFICADAS//////////////////////
        $scope.listarCatalogos = function () {
            var request = crud.crearRequest('reportes', 1, 'listarCatalogoConsultaGeneral2');
            request.setData();    
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingCatalogo.dataset = data.data;
                iniciarPosiciones($rootScope.settingCatalogo.dataset);
                $rootScope.tablaCatalogo.settings($rootScope.settingCatalogo);
                $scope.listarCatalogosAutomatizado();
            }, function (data) {
                console.info(data);
            });
        };
        $scope.eliminarAtributo = function(c){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el atributo?", function () {
                var request = crud.crearRequest('reportes', 1, 'eliminarAtributo');
                request.setData({id_met: c.id_met});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaCatalogo.settings().dataset, function(item){
                            return c === item;
                        });
                        $rootScope.tablaCatalogo.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        $scope.limpiarFormulario = function(){
            $scope.item_catalogo.id_met = "";
            $scope.item_catalogo.entity_name = "";
            $scope.item_catalogo.atrib_name = "";
            $scope.item_catalogo.alias_name = "";
            $scope.item_catalogo.data_type = "";
            $scope.item_catalogo.mod = "Escalafon";
        };
        $scope.cargarFormulario = function(a){
            $scope.item_catalogo.id_met = a.id_met;
            $scope.item_catalogo.entity_name = a.entity_name;
            $scope.item_catalogo.atrib_name = a.atrib_name;
            $scope.item_catalogo.alias_name = a.alias_name;
            $scope.item_catalogo.data_type = a.data_type;
            $scope.item_catalogo.mod = "Escalafon";
        };
        $scope.procesarAtributo = function(){
            if($scope.item_catalogo.id_met===""){
                $scope.agregarAtributo();
            }else{
                $scope.actualizarAtributo();
            }
            $('#modalnuevoitem').modal('hide');
        }
        $scope.agregarAtributo = function(){
            var request = crud.crearRequest('reportes',1,'agregarAtributoMetadata');
            request.setData({
                    entity_name:$scope.item_catalogo.entity_name,
                    atrib_name:$scope.item_catalogo.atrib_name,
                    alias_name:$scope.item_catalogo.alias_name,
                    data_type:$scope.item_catalogo.data_type,
                    mod:$scope.item_catalogo.mod
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarCatalogos();
                    $rootScope.tablaCatalogo.reload();
                    modal.mensaje("CONFIRMACION", "Se Agrego atributo de metadata");
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo almacenar atributo de metadata');
            }); 
             
        };
        $scope.actualizarAtributo = function(){            
            var request = crud.crearRequest('reportes',1,'actualizarAtributoMetadata');
            request.setData({
                    id_met:$scope.item_catalogo.id_met,
                    entity_name:$scope.item_catalogo.entity_name,
                    atrib_name:$scope.item_catalogo.atrib_name,
                    alias_name:$scope.item_catalogo.alias_name,
                    data_type:$scope.item_catalogo.data_type,
                    mod:$scope.item_catalogo.mod
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarCatalogos();
                    $rootScope.tablaCatalogo.reload();
                    modal.mensaje("CONFIRMACION", "Se Actualizo atributo de metadata");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo actualizar atributo de metadata');
            }); 
             
        };
///////////////////////////////////////////AUTOMATIZACION DE LLENADO DE TABLAS////////////////////////////////////////        
    $scope.listarCatalogosAutomatizado = function () {    
        var request = crud.crearRequest('reportes', 1, 'listarTodoEsquema');
            crud.listar("/sistema_escalafon",request,function(data){
                $scope.metadata2=data.data;
                $scope.tablas = [];
                $scope.atributos = [];

                /////////////////////////CREAMOS LOS NODOS//////////////////////////////////////
                for(var i=0;i<$scope.metadata2.length;i++){
                    if($scope.tablas.indexOf($scope.metadata2[i].table_name)==-1){
                        $scope.tablas.push($scope.metadata2[i].table_name);
                    }
                }
            }, function (data) {
                console.info(data);
            });
            
    };
///////////////////////////////////////////////////LISTAR ATRIBUTOS///////////////////////////////////////
    $scope.listarAtributos = function (tb) { 
        $scope.atributos.length = 0;
        $scope.item_catalogo.atrib_name = "";
        for(var i=0;i<$scope.metadata2.length;i++){
            if($scope.metadata2[i].table_name === tb){
                $scope.atributos.push({description:$scope.metadata2[i].description,column_name:$scope.metadata2[i].column_name,data_type:$scope.metadata2[i].data_type});
            }
        }
    };
    $scope.setAtribName = function (tb) { 
        console.info("length atriBUtos", $scope.atributos.length);
        for(var i=0;i<$scope.atributos.length;i++){
            if($scope.atributos[i].description === tb){
                $scope.item_catalogo.atrib_name = $scope.atributos[i].column_name;
                if($scope.item_catalogo.atrib_name.includes("dni")){
                    $scope.item_catalogo.data_type = "number";
                }else{
                    $scope.item_catalogo.data_type = setDataType($scope.atributos[i].data_type);
                }
            }
        }
    };
    var setDataType = function (data_type){
        if(data_type.includes("date")){
            return "date";
        }else if(data_type.includes("bool")){
            return "checkbox";
        }else if(data_type.includes("numeric")||data_type.includes("int")){
            return "number";
        }else{
            return "text";
        }
        
    };
    
    
    
    
}]);