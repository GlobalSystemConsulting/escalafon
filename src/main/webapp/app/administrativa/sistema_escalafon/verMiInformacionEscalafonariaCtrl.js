app.requires.push('angularModalService');
var seApp = angular.module('app');
seApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/mi_legajo_personal/:ficha',{
            templateUrl:'administrativa/sistema_escalafon/verMiLegajoPersonal.html',
            controller:'verMiLegajoPersonalCtrl',
            controllerAs:'verMiLegajoCtrl'
        });
}]);
app.controller('verMiInformacionEscalafonariaCtrl', ["$location", '$scope', '$rootScope', '$http', 'NgTableParams', 'crud', 'modal', 'ModalService', function ($location, $scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {
        //Variables y funciones para controlar la vista
        $scope.optionsDatosPersonales = [true, false, false, false, false];
        $scope.optionsDatosAcademicos = [true, false, false, false, false, false];

        $scope.updateOptions = function (numOption, optionsSet) {
            for (var i = 0; i < optionsSet.length; i += 1) {
                optionsSet[i] = false;
            }
            optionsSet[numOption] = true;
        };

        $scope.status_1 = {close: true, statusGD: false, statusLD: false};
        $scope.statusDA = false;
        
        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.afps = ["20530","25897"];
        
        $rootScope.paramsMisParientes = {count: 10};
        $rootScope.settingMisParientes = {counts: []};
        $rootScope.tablaMisParientes = new NgTableParams($rootScope.paramsMisParientes, $rootScope.settingMisParientes);

        $rootScope.paramsMisForEdu = {count: 10};
        $rootScope.settingMisForEdu = {counts: []};
        $rootScope.tablaMisForEdu = new NgTableParams($rootScope.paramsMisForEdu, $rootScope.settingMisForEdu);

        $rootScope.paramsMisEstCom = {count: 10};
        $rootScope.settingMisEstCom = {counts: []};
        $rootScope.tablaMisEstCom = new NgTableParams($rootScope.paramsMisEstCom, $rootScope.settingMisEstCom);

        $rootScope.paramsMisExpPon = {count: 10};
        $rootScope.settingMisExpPon = {counts: []};
        $rootScope.tablaMisExpPon = new NgTableParams($rootScope.paramsMisExpPon, $rootScope.settingMisExpPon);

        $rootScope.paramsMisPublicaciones = {count: 10};
        $rootScope.settingMisPublicaciones = {counts: []};
        $rootScope.tablaMisPublicaciones = new NgTableParams($rootScope.paramsMisPublicaciones, $rootScope.settingMisPublicaciones);

        $rootScope.paramsMisDesplazamientos = {count: 10};
        $rootScope.settingMisDesplazamientos = {counts: []};
        $rootScope.tablaMisDesplazamientos = new NgTableParams($rootScope.paramsMisDesplazamientos, $rootScope.settingMisDesplazamientos);

        $rootScope.paramsMisColegiaturas = {count: 10};
        $rootScope.settingMisColegiaturas = {counts: []};
        $rootScope.tablaMisColegiaturas = new NgTableParams($rootScope.paramsMisColegiaturas, $rootScope.settingMisColegiaturas);

        $rootScope.paramsMisAscensos = {count: 10};
        $rootScope.settingMisAscensos = {counts: []};
        $rootScope.tablaMisAscensos = new NgTableParams($rootScope.paramMisAscensos, $rootScope.settingMisAscensos);

        $rootScope.paramsMisCapacitaciones = {count: 10};
        $rootScope.settingMisCapacitaciones = {counts: []};
        $rootScope.tablaMisCapacitaciones = new NgTableParams($rootScope.paramsMisCapacitaciones, $rootScope.settingMisCapacitaciones);

        $rootScope.paramsMisReconocimientos = {count: 10};
        $rootScope.settingMisReconocimientos = {counts: []};
        $rootScope.tablaMisReconocimientos = new NgTableParams($rootScope.paramsMisReconocimientos, $rootScope.settingMisReconocimientos);

        $rootScope.paramsMisEstPos = {count: 10};
        $rootScope.settingMisEstPos = {counts: []};
        $rootScope.tablaMisEstPos = new NgTableParams($rootScope.paramsMisEstPos, $rootScope.settingMisEstPos);

        $rootScope.paramsMisDemeritos = {count: 10};
        $rootScope.settingMisDemeritos = {counts: []};
        $rootScope.tablaMisDemeritos = new NgTableParams($rootScope.paramsMisDemeritos, $rootScope.settingMisDemeritos);
        
        //Carga de Catalogo EstadoCivil
        $scope.estadoCivil = [];
        function listarEstadoCivil() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarEstadoCivil');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.estadoCivil = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        listarEstadoCivil();
        
        //Carga de Catalogo Nacionalidad/Pais
        $scope.nacionalidad = [];

        function listarNacionalidad() {
            var request = crud.crearRequest('datos_personales', 1, 'listarNacionalidad');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.nacionalidad = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        listarNacionalidad();
        //Carga de Catalogo Idioma
        $scope.idiomas = [];

        function listarIdioma() {
            var request = crud.crearRequest('datos_personales', 1, 'listarIdioma');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.idiomas = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        listarIdioma();
        
        $scope.tiposFormacion2 = [];
        function listarTiposFormacion() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposFor');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response);
                    $scope.tiposFormacion2 = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarTiposFormacion();

        $scope.nivelesAcademicos = [];
        function listarNivelesAcademicos() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarNivAca');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.nivelesAcademicos = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarNivelesAcademicos();
        
        $scope.tiposEstudioComplementario = [];
        function listarTiposEstudiosComplementarios() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarTiposEstCom');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.tiposEstudioComplementario = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarTiposEstudiosComplementarios();

        $scope.nivelesEstudioComplementario = [];
        function listarNivelesEstudiosComplementarios() {
            var request = crud.crearRequest('catalogo_escalafon', 1, 'listarNivEstCom');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.nivelesEstudioComplementario = response.data;
                }
            }, function (error) {
                console.info(error);
            });
        }
        listarNivelesEstudiosComplementarios();
        
        var motivos = [
            {id: "1", title: "Mérito"},
            {id: "2", title: "Felicitación"},
            {id: "3", title: "25 años de servicio"},
            {id: "4", title: "30 años de servicio"},
            {id: "5", title: "Luto y sepelio"},
            {id: "6", title: "Otros"}
        ];
        
        $scope.direcciones = [];
        function listarDirecciones(perId) {
            var request = crud.crearRequest('datos_personales', 1, 'listarDirecciones');
            request.setData({perId: perId});
            crud.listar('/sistema_escalafon', request, function (response) {
                $scope.direcciones = response.data;
                if($scope.direcciones.length!==0)
                {
                    nuevaDireccionSel=[];
                    nuevaDireccionAct=[];
                    for (var dir in $scope.direcciones)
                    {
                        if ($scope.direcciones[dir].tipDir === "R")
                        {
                            nuevaDireccionSel.dirId = $scope.direcciones[dir].dirId;
                            nuevaDireccionSel.tipDir = $scope.direcciones[dir].tipDir;
                            nuevaDireccionSel.nomDir = $scope.direcciones[dir].nomDir;
                            nuevaDireccionSel.depDir = $scope.direcciones[dir].depDir;
                            nuevaDireccionSel.proDir = $scope.direcciones[dir].proDir;
                            nuevaDireccionSel.disDir = $scope.direcciones[dir].disDir;
                            nuevaDireccionSel.nomZon = $scope.direcciones[dir].nomZon;
                            nuevaDireccionSel.desRef = $scope.direcciones[dir].desRef;
                        }
                        if ($scope.direcciones[dir].tipDir === "A")
                        {
                            nuevaDireccionAct.dirId = $scope.direcciones[dir].dirId;
                            nuevaDireccionAct.tipDir = $scope.direcciones[dir].tipDir;
                            nuevaDireccionAct.nomDir = $scope.direcciones[dir].nomDir;
                            nuevaDireccionAct.depDir = $scope.direcciones[dir].depDir;
                            nuevaDireccionAct.proDir = $scope.direcciones[dir].proDir;
                            nuevaDireccionAct.disDir = $scope.direcciones[dir].disDir;
                            nuevaDireccionAct.nomZon = $scope.direcciones[dir].nomZon;
                            nuevaDireccionAct.desRef = $scope.direcciones[dir].desRef;
                        }
                    }
                    $scope.direcciones = [];
                    $scope.direcciones.push(nuevaDireccionSel);//pos0
                    $scope.direcciones.push(nuevaDireccionAct);//pos1
                    //console.log("dir", $scope.direcciones);
                }
                changeOptionsDirectionsState($scope.direcciones);
                
            }, function (data) {
                console.info(data);
            });
        }
        
        function changeOptionsDirectionsState(direcciones) {
            var caso=1;
            if(direcciones.length===0)
                caso=0;
            if(direcciones.length===2)
            {
                if(direcciones[1].nomDir==='' && direcciones[1].depDir==='  '  && direcciones[1].proDir==='  '  &&
                        direcciones[1].disDir==='  '  && direcciones[1].nomZon===''  && direcciones[1].desRef===''  )
                {    
                    caso=3;
                }else
                    caso=2;
            }
            switch (caso) {
                case 1:
                    $scope.optionsDirections = "SI";
                    $scope.status_1.statusGD = true;
                    $scope.statusDA = false;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDir = direcciones[0].depDir;
                    $scope.direccionDNISel.proDir = direcciones[0].proDir;
                    $scope.direccionDNISel.disDir = direcciones[0].disDir;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;
                    break;

                case 2:
                    $scope.optionsDirections = "NO";
                    $scope.status_1.statusGD = true;
                    $scope.status_1.statusLD = true;
                    $scope.statusDA = true;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDir = direcciones[0].depDir;
                    $scope.direccionDNISel.proDir = direcciones[0].proDir;
                    $scope.direccionDNISel.disDir = direcciones[0].disDir;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;

                    $scope.direccionDASel.dirId = direcciones[1].dirId;
                    $scope.direccionDASel.tipDir = direcciones[1].tipDir;
                    $scope.direccionDASel.nomDir = direcciones[1].nomDir;
                    $scope.direccionDASel.depDir = direcciones[1].depDir;
                    $scope.direccionDASel.proDir = direcciones[1].proDir;
                    $scope.direccionDASel.disDir = direcciones[1].disDir;
                    $scope.direccionDASel.nomZon = direcciones[1].nomZon;
                    $scope.direccionDASel.desRef = direcciones[1].desRef;
                    break;
                    
                case 3:
                    $scope.optionsDirections = "SI";
                    $scope.status_1.statusGD = true;
                    $scope.status_1.statusLD = false;
                    $scope.statusDA = false;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDir = direcciones[0].depDir;
                    $scope.direccionDNISel.proDir = direcciones[0].proDir;
                    $scope.direccionDNISel.disDir = direcciones[0].disDir;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;

                    $scope.direccionDASel.dirId = direcciones[1].dirId;
                    $scope.direccionDASel.tipDir = direcciones[1].tipDir;
                    $scope.direccionDASel.nomDir = direcciones[1].nomDir;
                    $scope.direccionDASel.depDir = direcciones[1].depDir;
                    $scope.direccionDASel.proDir = direcciones[1].proDir;
                    $scope.direccionDASel.disDir = direcciones[1].disDir;
                    $scope.direccionDASel.nomZon = direcciones[1].nomZon;
                    $scope.direccionDASel.desRef = direcciones[1].desRef;
                    break;
            }
        }

        $scope.personaSel = {
            perId: 0,
            perCod: "",
            apePat: "",
            apeMat: "",
            nom: "",
            sex:"",
            estCivId:0,
            eda: 0,
            dni: "",
            pas:"",
            estAseEss:false,
            autEss:"",
            fij: "",
            num1: "",
            num2: "",
            email: "",
            nacId:"",
            depNac: "",
            proNac: "",
            disNac: "",
            fecNac: "",
            sisPen: "NUL",
            tipAfp: "",
            codCuspp: "",
            fecIngAfp: "",
            fecTraAfp: "",       
            perDis: false,
            regCon: "",
            idiomId:"",
            licCond:"",
            bonCaf:"",
            estado: 'A'
        };
        
        $scope.direccionDNISel = {
            tipDir: "R",
            nomDir: "",
            depDir: "",
            proDir: "",
            disDir: "",
            nomZon: "",
            desRef: ""
        };
        $scope.direccionDASel = {
            tipDir: "A",
            nomDir: "",
            depDir: "",
            proDir: "",
            disDir: "",
            nomZon: "",
            desRef: ""
        };
        
        $scope.trabajadorSel = {
            traId: 0,
            traCon: ""
        };
        
        $scope.fichaEscalafonariaSel = {
            ficEscId: 0
        };
        
        function obtenerInformacionEscalafonaria() {
            var request = crud.crearRequest('datos_personales', 1, 'buscarFichaPorUsuId');
            request.setData({usuId: $scope.usuMaster.usuario.usuarioID, opcion: 1});
            crud.listar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    var datosPersonales = response.data;
                    $scope.personaSel.perId = datosPersonales.persona.perId;
                    $scope.personaSel.apePat = datosPersonales.persona.apePat;
                    $scope.personaSel.apeMat = datosPersonales.persona.apeMat;
                    $scope.personaSel.nom = datosPersonales.persona.nom;
                    $scope.personaSel.sex = datosPersonales.persona.sex;
                    $scope.personaSel.estCivId = datosPersonales.persona.estCivId;
                    $scope.personaSel.eda = calcularEdad(datosPersonales.persona.fecNac);
                    $scope.personaSel.dni = datosPersonales.persona.dni;
                    $scope.personaSel.pas = datosPersonales.persona.pas;
                    $scope.personaSel.estAseEss = datosPersonales.persona.estAseEss === "" ? false : datosPersonales.persona.estAseEss;
                    $scope.personaSel.autEss = datosPersonales.persona.autEss;
                    $scope.personaSel.fij = datosPersonales.persona.fij;
                    $scope.personaSel.num1 = datosPersonales.persona.num1;
                    $scope.personaSel.num2 = datosPersonales.persona.num2;
                    $scope.personaSel.email = datosPersonales.persona.email;
                    $scope.personaSel.nacId = datosPersonales.persona.nacId;
                    $scope.personaSel.depNacDes = datosPersonales.persona.depNacDes;
                    $scope.personaSel.proNacDes = datosPersonales.persona.proNacDes; 
                    $scope.personaSel.disNacDes = datosPersonales.persona.disNacDes;
                    $scope.personaSel.fecNac = datosPersonales.persona.fecNac===""?"":convertirFechaStr(datosPersonales.persona.fecNac);
                    $scope.personaSel.sisPen = datosPersonales.persona.sisPen === "" ? "NUL" : datosPersonales.persona.sisPen;
                    $scope.personaSel.tipAfp = datosPersonales.persona.tipAfp;
                    $scope.personaSel.codCuspp = datosPersonales.persona.codCuspp;
                    $scope.personaSel.fecIngAfp = datosPersonales.persona.fecIngAfp===""?"":convertirFechaStr(datosPersonales.persona.fecIngAfp);
                    $scope.personaSel.fecTraAfp = datosPersonales.persona.fecTraAfp===""?"":convertirFechaStr(datosPersonales.persona.fecTraAfp);
                    $scope.personaSel.perDis = datosPersonales.persona.perDis === "" ? false : datosPersonales.persona.perDis;
                    $scope.personaSel.regCon = datosPersonales.persona.regCon;
                    $scope.personaSel.idiomId = datosPersonales.persona.idiomId;
                    $scope.personaSel.licCond = datosPersonales.persona.licCond;
                    $scope.personaSel.bonCaf = datosPersonales.persona.bonCaf;
                    $scope.personaSel.estado = 'A';
                    
                    
                    //Direccion
                    listarDirecciones(datosPersonales.persona.perId);
                    
                    $http.get('../recursos/json/departamentos.json').success(function (data) {
                        $scope.departamentosDir = data;
                    });
                    $http.get('../recursos/json/provincias.json').success(function (data) {
                        var dep = $scope.departamentosDir.filter(function (obj) {
                            return obj.codigo_ubigeo === $scope.direccionDNISel.depDir;
                        })[0];
                        $scope.provinciasDirDNI = data[dep.id_ubigeo];
                        $scope.distritosDirDNI = [];
                    });
                    $http.get('../recursos/json/distritos.json').success(function (data) {
                        var pro = $scope.provinciasDirDNI.filter(function (obj) {
                            return obj.codigo_ubigeo === $scope.direccionDNISel.proDir;
                        })[0];
                        $scope.distritosDirDNI = data[pro.id_ubigeo];
                    });
                    $http.get('../recursos/json/provincias.json').success(function (data) {
                        var dep = $scope.departamentosDir.filter(function (obj) {
                            return obj.codigo_ubigeo === $scope.direccionDASel.depDir;
                        })[0];
                        $scope.provinciasDirDA = data[dep.id_ubigeo];
                        $scope.distritosDirDA = [];
                    });
                    $http.get('../recursos/json/distritos.json').success(function (data) {
                        var pro = $scope.provinciasDirDA.filter(function (obj) {
                            return obj.codigo_ubigeo === $scope.direccionDASel.proDir;
                        })[0];
                        $scope.distritosDirDA = data[pro.id_ubigeo];
                    });
                    
                    $scope.boolSalud = {
                        valS: ($scope.personaSel.estAseEss) === true ? 'SI' : 'NO'
                    };

                    $scope.boolDis = {
                        val: ($scope.personaSel.perDis) === true ? 'SI' : 'NO'
                    };
                    
                    $scope.trabajadorSel.traId = datosPersonales.trabajador.traId;
                    $scope.trabajadorSel.traCon = datosPersonales.trabajador.traCon;
                    
                    $scope.fichaEscalafonariaSel.ficEscId = datosPersonales.ficha.ficEscId;
                    
                    listarParientes();
                    listarFormacionesEducativas();
                    listarEstudiosComplementarios();
                    listarCapacitaciones();
                    listarColegiaturas();
                    listarPublicaciones();
                    listarExposiciones();
                    listarReconocimientos();
                    listarDemeritos();
                    
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (data) {
                modal.mensaje('ERROR','Usted no tiene una Ficha Escalafonaria');
            });
        };
        obtenerInformacionEscalafonaria();
        //Funciones para listas los datos de tablas
        function listarParientes() {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarParientes');
            request.setData($scope.trabajadorSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.parEda = calcularEdad(item.parFecNac);
                });
                $rootScope.settingMisParientes.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisParientes.dataset);
                $rootScope.tablaMisParientes.settings($rootScope.settingMisParientes);
            }, function (data) {
                console.info(data);
            });
        };

        function listarFormacionesEducativas() {
            //preparamos un objeto request
            var request = crud.crearRequest('formacion_educativa', 1, 'listarFormacionesEducativas');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.estConDes = item.estCon === "" ? "No registrado" : (item.estCon ? "Si" : "No");
                    item.tipFor = buscarContenido($scope.tiposFormacion2, 'id', 'nom', item.tipForId);
                    item.nivAca = buscarContenido($scope.nivelesAcademicos, 'id', 'nom', item.nivAcaId);
                });
                $rootScope.settingMisForEdu.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisForEdu.dataset);
                $rootScope.tablaMisForEdu.settings($rootScope.settingMisForEdu);
            }, function (data) {
                console.info(data);
            });
        };

        function listarColegiaturas() {
            //preparamos un objeto request
            var request = crud.crearRequest('colegiatura', 1, 'listarColegiaturas');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.conRegDes = item.conReg ? "Habilitado" : "No habilitado";
                });
                $rootScope.settingMisColegiaturas.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisColegiaturas.dataset);
                $rootScope.tablaMisColegiaturas.settings($rootScope.settingMisColegiaturas);
            }, function (data) {
                console.info(data);
            });
        };

        function listarEstudiosComplementarios () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_complementario', 1, 'listarEstudiosComplementarios');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.tip = item.tipId === 0 ? "No registrado" : buscarContenido($scope.tiposEstudioComplementario, 'id', 'nom', item.tipId);
                    item.niv = item.nivId === 0 ? "No registrado" : buscarContenido($scope.nivelesEstudioComplementario, 'id', 'nom', item.nivId);
                });
                $rootScope.settingMisEstCom.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisEstCom.dataset);
                $rootScope.tablaMisEstCom.settings($rootScope.settingMisEstCom);
            }, function (data) {
                console.info(data);
            });
        };

        /*function listarEstudiosPostgrado() {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_postgrado', 1, 'listarEstudiosPostgrado');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.tipDes = buscarContenido(tiposEstPos, 'id', 'title', item.tip);
                });
                $rootScope.settingMisEstPos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisEstPos.dataset);
                $rootScope.tablaMisEstPos.settings($rootScope.settingMisEstPos);
            }, function (data) {
                console.info(data);
            });
        };*/

        function listarExposiciones() {
            //preparamos un objeto request
            var request = crud.crearRequest('exposicion_ponencia', 1, 'listarExposiciones');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingMisExpPon.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisExpPon.dataset);
                $rootScope.tablaMisExpPon.settings($rootScope.settingMisExpPon);
            }, function (data) {
                console.info(data);
            });
        };

        function listarPublicaciones() {
            //preparamos un objeto request
            var request = crud.crearRequest('publicacion', 1, 'listarPublicaciones');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingMisPublicaciones.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisPublicaciones.dataset);
                $rootScope.tablaMisPublicaciones.settings($rootScope.settingMisPublicaciones);
            }, function (data) {
                console.info(data);
            });
        };

        function listarCapacitaciones() {
            //preparamos un objeto request
            var request = crud.crearRequest('capacitacion', 1, 'listarCapacitaciones');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingMisCapacitaciones.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisCapacitaciones.dataset);
                $rootScope.tablaMisCapacitaciones.settings($rootScope.settingMisCapacitaciones);
            }, function (data) {
                console.info(data);
            });
        };

        function listarReconocimientos() {
            //preparamos un objeto request
            var request = crud.crearRequest('reconocimientos', 1, 'listarReconocimientos');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.motDes = buscarContenido(motivos, 'id', 'title', item.mot);
                });
                $rootScope.settingMisReconocimientos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisReconocimientos.dataset);
                $rootScope.tablaMisReconocimientos.settings($rootScope.settingMisReconocimientos);
            }, function (data) {
                console.info(data);
            });
        };

        function listarDemeritos() {
            //preparamos un objeto request
            var request = crud.crearRequest('demeritos', 1, 'listarDemeritos');
            request.setData($scope.personaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    item.sepDes = item.sep ? "Si" : "No";
                });
                $rootScope.settingMisDemeritos.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingMisDemeritos.dataset);
                $rootScope.tablaMisDemeritos.settings($rootScope.settingMisDemeritos);
            }, function (data) {
                console.info(data);
            });
        };
        
        $scope.verFichaEscalafonaria = function(){
            
            $scope.selecc = [];

            var request = crud.crearRequest('reportes', 1, 'selecReporteFicha');
            request.setData({traId: $scope.trabajadorSel.traId, seleccionados: $scope.selecc});
            crud.insertar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    for (i = 0; i < response.data.seleccionados.length; i++)
                    {
                        $scope.selecc[i] = response.data.seleccionados[i];
                    }
                }
            }, function (error) {
                console.info(error);
            });
            
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/modalSeleccionados.html",
                controller: "verModalSeleccionadosCtrl",
                inputs: {
                    title: "Modal seleccionados",
                    ficEscId:$scope.fichaEscalafonariaSel.ficEscId, 
                    traId:$scope.trabajadorSel.traId, 
                    perDni:$scope.personaSel.dni,
                    selecc: $scope.selecc
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                        
                    }
                });
            });
        };
        
        $scope.prepararVerMiLegajoPersonal = function () {

            var request = crud.crearRequest('datos_personales',1,'buscarFichaPorDNI');
            request.setData({perDni:$scope.personaSel.dni, opcion:200});
            crud.listar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $location.url('/mi_legajo_personal/'+btoa(JSON.stringify(response.data)));
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function (data) {
                console.info(data);
            });
        };
    }]);
app.controller('verMiLegajoPersonalCtrl', ['$routeParams', '$rootScope', '$scope', 'crud', 'NgTableParams', 'modal', function ($routeParams, $rootScope, $scope, crud, NgTableParams, modal) {
        var datos = JSON.parse(atob($routeParams.ficha));
        

        var categorias = [
            {idCat: "1", idSubCat: "1", subCatDes: "Datos personales"},
            {idCat: "1", idSubCat: "2", subCatDes: "Nacimiento"},
            {idCat: "1", idSubCat: "3", subCatDes: "Salud"},
            {idCat: "1", idSubCat: "4", subCatDes: "Direccion"},
            {idCat: "2", idSubCat: "1", subCatDes: "Datos familiares"},
            {idCat: "3", idSubCat: "1", subCatDes: "Formacion educativa"},
            {idCat: "3", idSubCat: "2", subCatDes: "Colegiatura"},
            {idCat: "3", idSubCat: "3", subCatDes: "Estudios complementarios"},
            {idCat: "3", idSubCat: "4", subCatDes: "Estudios postgrado"},
            {idCat: "3", idSubCat: "5", subCatDes: "Exposicion y/o Ponencias"},
            {idCat: "3", idSubCat: "6", subCatDes: "Publicaciones"},
            {idCat: "4", idSubCat: "1", subCatDes: "Desplazamientos"},
            {idCat: "4", idSubCat: "2", subCatDes: "Ascensos"},
            {idCat: "4", idSubCat: "3", subCatDes: "Capacitaciones"},
            {idCat: "5", idSubCat: "1", subCatDes: "Reconocimientos"},
            {idCat: "6", idSubCat: "1", subCatDes: "Demeritos"},
            {idCat: "7", idSubCat: "1", subCatDes: "Otros"}
        ];

        $rootScope.paramsMiLegDatosPersonales = {count: 10};
        $rootScope.settingMiLegDatosPersonales = {counts: []};
        $rootScope.tablaMiLegDatosPersonales = new NgTableParams($rootScope.paramsMiLegDatosPersonales, $rootScope.settingMiLegDatosPersonales);

        $rootScope.paramsMiLegDatosFamiliares = {count: 10};
        $rootScope.settingMiLegDatosFamiliares = {counts: []};
        $rootScope.tablaMiLegDatosFamiliares = new NgTableParams($rootScope.paramsMiLegDatosFamiliares, $rootScope.settingMiLegDatosFamiliares);

        $rootScope.paramsMiLegDatosAcademicos = {count: 10};
        $rootScope.settingMiLegDatosAcademicos = {counts: []};
        $rootScope.tablaMiLegDatosAcademicos = new NgTableParams($rootScope.paramsMiLegDatosAcademicos, $rootScope.settingMiLegDatosAcademicos);

        $rootScope.paramsMiLegExperienciaLaboral = {count: 10};
        $rootScope.settingMiLegExperienciaLaboral = {counts: []};
        $rootScope.tablaMiLegExperienciaLaboral = new NgTableParams($rootScope.paramsMiLegExperienciaLaboral, $rootScope.settingMiLegExperienciaLaboral);

        $rootScope.paramsMiLegReconocimientos = {count: 10};
        $rootScope.settingMiLegReconocimientos = {counts: []};
        $rootScope.tablaMiLegReconocimientos = new NgTableParams($rootScope.paramsMiLegReconocimientos, $rootScope.settingMiLegReconocimientos);

        $rootScope.paramsMiLegDemeritos = {count: 10};
        $rootScope.settingMiLegDemeritos = {counts: []};
        $rootScope.tablaMiLegDemeritos = new NgTableParams($rootScope.paramsMiLegDemeritos, $rootScope.settingMiLegDemeritos);

        $rootScope.paramsMiLegOtros = {count: 10};
        $rootScope.settingMiLegOtros = {counts: []};
        $rootScope.tablaMiLegOtros = new NgTableParams($rootScope.paramsMiLegOtros, $rootScope.settingMiLegOtros);

        var legDatosPersonales = [];
        var legDatosFamiliares = [];
        var legDatosAcademicos = [];
        var legExperienciaLaboral = [];
        var legReconocimientos = [];
        var legDemeritos = [];
        var legOtros = [];

        listarLegajos();
        function listarLegajos() {
            //preparamos un objeto request
            var request = crud.crearRequest('legajo_personal', 1, 'listarLegajos');
            request.setData(datos);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    var subCategorias = buscarObjetos(categorias, 'idCat', item.catLeg);
                    item.subCatDes = buscarContenido(subCategorias, 'idSubCat', 'subCatDes', item.subCat);

                    switch (item.catLeg) {
                        case '1':
                            legDatosPersonales.push(item);
                            break;
                        case '2':
                            legDatosFamiliares.push(item);
                            break;
                        case '3':
                            legDatosAcademicos.push(item);
                            break;
                        case '4':
                            legExperienciaLaboral.push(item);
                            break;
                        case '5':
                            legReconocimientos.push(item);
                            break;
                        case '6':
                            legDemeritos.push(item);
                            break;
                        default:
                            legOtros.push(item);
                    }
                });

                $rootScope.settingMiLegDatosPersonales.dataset = legDatosPersonales;
                iniciarPosiciones($rootScope.settingMiLegDatosPersonales.dataset);
                $rootScope.tablaMiLegDatosPersonales.settings($rootScope.settingMiLegDatosPersonales);

                $rootScope.settingMiLegDatosFamiliares.dataset = legDatosFamiliares;
                iniciarPosiciones($rootScope.settingMiLegDatosFamiliares.dataset);
                $rootScope.tablaMiLegDatosFamiliares.settings($rootScope.settingMiLegDatosFamiliares);

                $rootScope.settingMiLegDatosAcademicos.dataset = legDatosAcademicos;
                iniciarPosiciones($scope.settingMiLegDatosAcademicos.dataset);
                $rootScope.tablaMiLegDatosAcademicos.settings($rootScope.settingMiLegDatosAcademicos);

                $scope.settingMiLegExperienciaLaboral.dataset = legExperienciaLaboral;
                iniciarPosiciones($rootScope.settingMiLegExperienciaLaboral.dataset);
                $rootScope.tablaMiLegExperienciaLaboral.settings($rootScope.settingMiLegExperienciaLaboral);

                $rootScope.settingMiLegReconocimientos.dataset = legReconocimientos;
                iniciarPosiciones($rootScope.settingMiLegReconocimientos.dataset);
                $rootScope.tablaMiLegReconocimientos.settings($rootScope.settingMiLegReconocimientos);

                $rootScope.settingMiLegDemeritos.dataset = legDemeritos;
                iniciarPosiciones($rootScope.settingMiLegDemeritos.dataset);
                $rootScope.tablaMiLegDemeritos.settings($rootScope.settingMiLegDemeritos);

                $rootScope.settingMiLegOtros.dataset = legOtros;
                iniciarPosiciones($rootScope.settingMiLegOtros.dataset);
                $rootScope.tablaMiLegOtros.settings($rootScope.settingMiLegOtros);


            }, function (data) {
                console.info(data);
            });

        };
    }]);

app.controller('verModalSeleccionadosCtrl', ['$scope', '$rootScope', '$element','title', 'ficEscId', 'traId', 'perDni','close', 'crud', 'modal', 'selecc','ModalService', function ($scope, $rootScope, $element, title, ficEscId, traId, perDni, close, crud, modal, selecc, ModalService) {
        //variable que servira para registrar un nuevo pariente
        
        $scope.title = title;
        $scope.selecc = selecc;
        $scope.seleccionados = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
        console.log($scope.selecc);
        $scope.formatos = ["PDF"];
        $scope.selecFormat = $scope.formatos[0];//Establecer la selección
        var checked = false;
        
        $scope.marcarTodos = function () {
        if (checked === false) {
            $('.dinamico').prop('checked', true);
            $scope.seleccionados = [true, true, true, true, true, true, true, true, true, true, true, true, true, true, true];
            checked = true;
        } else {
            $('.dinamico').prop('checked', false);
            $scope.seleccionados = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
            console.log($scope.seleccionados);
            checked = false;
        }
        };
        
        $scope.verFichaEscalafonaria = function () {
            var request = crud.crearRequest('reportes',1,'reporteFichaEscalafonaria');
            request.setData({
                traId:traId, 
                ficEscId:ficEscId, 
                perDni:perDni, 
                seleccionados: $scope.seleccionados });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    verDocumentoPestaña(response.data.file);
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            }); 
        };
        $scope.descargarFichaEscalafonaria = function () {
            var request = crud.crearRequest('reportes',1,'reporteFichaEscalafonaria');
            request.setData({
                traId:traId, 
                ficEscId:ficEscId, 
                perDni:perDni, 
                seleccionados: $scope.seleccionados });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    if ($scope.selecFormat.toString() === "PDF") {
                        descargarArchivo(response.data.file, "FichaEscalafonaria_" + perDni, "pdf");
                    }
                    if ($scope.selecFormat.toString() === "EXCEL")
                    {
                        descargarArchivo(response.data.reporteXls, "FichaEscalafonaria_" + perDni, "xls");
                    }
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde');
            }); 
        };
    }]);