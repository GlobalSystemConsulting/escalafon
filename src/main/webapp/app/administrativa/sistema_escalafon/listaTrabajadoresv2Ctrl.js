var seApp = angular.module('app');
seApp.requires.push('angularModalService');
seApp.requires.push('ngAnimate');

seApp.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/actualizar_ficha_escalafonariav2/:data', {
            templateUrl: 'administrativa/sistema_escalafon/actualizarFichaEscalafonariav2.html',
            controller: 'actualizarFichaEscalafonariav2Ctrl',
            controllerAs: 'actualizarFichav2Ctrl'
        }).when('/legajo_personal/:ficha', {
            templateUrl: 'administrativa/sistema_escalafon/verLegajoPersonal.html',
            controller: 'verLegajoPersonalCtrl',
            controllerAs: 'verLegajoCtrl'
        }).when('/legajo_personal_ridea/:data', {
            templateUrl: 'administrativa/sistema_escalafon/verLegajoPersonalRidea.html',
            controller: 'verLegajoPersonalRideaCtrl',
            controllerAs: 'verLegajoRideaCtrl'
        });
    }]);
seApp.controller("listaTrabajadoresv2Ctrl", ["$location", "$rootScope", "$scope", "NgTableParams", "crud", "modal", "ModalService", function ($location, $rootScope, $scope, NgTableParams, crud, modal, ModalService) {

        //Cadena por la cual se listaran los trabajadores
        $scope.cadenaBusqueda = "";
        console.log($scope.cadenaBusqueda);
        //var nuevaB;
        //var myObj = JSON.parse($scope.cadenaBusqueda);
        //console.log(">>>"+myObj);
        //if ($scope.cadenaBusqueda==="")
        //    nuevaB='A';
        //else
        //    nuevaB=$scope.cadenaBusqueda.replace(/\s/g,"_");
        //arreglo donde estan todas los trabajadores
        $scope.trabajadores = [];
        //variable que servira para contener los datos de un trabajador seleccionado
        $scope.trabajadorSel = {};
        //Variables para manejo de la tabla
        $rootScope.paramsTrabajadores = {count: 10};
        $rootScope.settingTrabajadores = {counts: []};
        $rootScope.tablaTrabajadores = new NgTableParams($rootScope.paramsTrabajadores, $rootScope.settingTrabajadores);
        $scope.listarTrabajadores = function () {
            //preparamos un objeto request
            //preparamos un objeto request
            var request = crud.crearRequest('datos_personales', 1, 'listarPorOrgyCadIni');
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID, cadenaInicio: $scope.cadenaBusqueda});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    var aux = [];
                    response.data.forEach(function (item) {
                        if (!buscarObjeto(aux, 'perId', item.perId))
                            aux.push(item);
                    });

                    $rootScope.settingTrabajadores.dataset = aux;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingTrabajadores.dataset);
                    $rootScope.tablaTrabajadores.settings($rootScope.settingTrabajadores);

                }
            }, function (data) {
                console.info(data);
            });
        };


        $scope.verListadoFiltradoTrabajadores = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('datos_personales', 1, 'listarPorOrgyCadIni');
            request.setData({orgId: $rootScope.usuMaster.organizacion.organizacionID, cadenaInicio: $scope.cadenaBusqueda});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    var aux = [];
                    response.data.forEach(function (item) {
                        if (!buscarObjeto(aux, 'perId', item.perId))
                            aux.push(item);
                    });

                    $rootScope.settingTrabajadores.dataset = aux;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingTrabajadores.dataset);
                    $rootScope.tablaTrabajadores.settings($rootScope.settingTrabajadores);

                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.prepararModificarFichaEscalafonaria = function (t) {
            var trabajadorSel = JSON.parse(JSON.stringify(t));
            trabajadorSel.opcion = 1;
            var request = crud.crearRequest('datos_personales', 1, 'buscarFichaPorDNI');
            request.setData(trabajadorSel);
            crud.listar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    //console.log("going to do shit with: "+JSON.stringify(response.data));
                    var finalData = {};
                    crud.listar('/sistema_escalafon', request, function (response) {
                        if (response.responseSta) {
                            for (var key in response.data)
                                finalData[key] = response.data[key];
                            // Once the esc file is retrieved, you must get the personal legacy files from RIDEA
                            var request = crud.crearRequest('legajo_personal_ridea', 1, 'listarLegajosRidea');
                            request.setData(trabajadorSel);
                            crud.listar('/sistema_escalafon', request, function (response) {
                                if (response.responseSta) {
                                    for (var key in response.data)
                                        finalData[key] = response.data[key];
                                    $location.url('/actualizar_ficha_escalafonariav2/' + btoa(JSON.stringify(finalData)));
                                    //console.log("docs: "+finalData.Documentos);
                                } else {
                                    $location.url('/actualizar_ficha_escalafonariav2/' + btoa(JSON.stringify(finalData)));
                                    modal.mensaje("ERROR", "Se cargó la ficha pero el legajo no ha sido digitalizado ENTRO JOJO");
                                }
                            }, function (error) {
                                modal.mensaje("ERROR", "El legajo no ha sido digitalizado");
                            });
                        } else {
                            modal.mensaje('ERROR', response.responseMsg);
                        }
                    }, function (data) {
                        console.info(data);
                    });
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.prepararVerLegajoPersonal = function (t) {

            var trabajadorSel = JSON.parse(JSON.stringify(t));
            trabajadorSel.opcion = 200;
            var request = crud.crearRequest('datos_personales', 1, 'buscarFichaPorDNI');
            request.setData(trabajadorSel);

            crud.listar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    $location.url('/legajo_personal/' + btoa(JSON.stringify(response.data)));
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (data) {
                console.info(data);
            });
        };

        // prepararVerLegajoPersonalRidea
        $scope.prepararVerLegajoPersonalRidea = function (t) {

            var trabajadorSel = JSON.parse(JSON.stringify(t));
            trabajadorSel.opcion = 200;
            var request = crud.crearRequest('legajo_personal_ridea', 1, 'listarLegajosRidea');
            request.setData(trabajadorSel);

            console.info(trabajadorSel);
            crud.listar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    console.info(response.data);
                    $location.url('/legajo_personal_ridea/' + btoa(JSON.stringify(response.data)));
                } else {
                    modal.mensaje("ERROR", "El legajo no ha sido digitalizado");
                }
            }, function (error) {
                modal.mensaje("ERROR", "El legajo no ha sido digitalizado");
            });
        };

        $scope.verFichaEscalafonaria = function (t) {
            var trabajadorSel = JSON.parse(JSON.stringify(t));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/modalSeleccionados.html",
                controller: "modalSeleccionadosCtrl",
                inputs: {
                    title: "Modal seleccionados",
                    ficEscId: trabajadorSel.ficEscId,
                    traId: trabajadorSel.traId,
                    perDni: trabajadorSel.perDni
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {

                    }
                });
            });

        };

        $scope.generarModeloReporteEscalafonario = function (t) {
            var trabajadorSel = JSON.parse(JSON.stringify(t));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/modalModelosReporte.html",
                controller: "modalModelosReporteCtrl",
                inputs: {
                    title: "Modal seleccionados",
                    ficEscId: trabajadorSel.ficEscId,
                    traId: trabajadorSel.traId,
                    perDni: trabajadorSel.perDni
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {

                    }
                });
            });

        };

        $scope.eliminarTrabajador = function (t) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el trabajador?", function () {
                var request = crud.crearRequest('datos_personales', 1, 'eliminarTrabajador');
                request.setData({traId: t.traId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaTrabajadores.settings().dataset, function (item) {
                            return t === item;
                        });
                        $rootScope.tablaTrabajadores.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };
    }]);
seApp.controller('verLegajoPersonalRideaCtrl', ['$routeParams', '$rootScope', '$scope', 'crud', 'NgTableParams', 'ModalService', 'modal', function ($routeParams, $rootScope, $scope, crud, NgTableParams, ModalService, modal) {
        var datos = JSON.parse(atob($routeParams.data));
        $scope.dataRidea = datos;
        var url;
        var serR = "/rideaoffice";
        var serP = "/getPdf?"
        $scope.uri;
        $scope.uriv;
        $scope.service = function () {

            var request = crud.crearRequest('servicioRidea', 1, 'verServicioRIDEA');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                if (data.data) {
                    url = data.data.servicio;
                    $scope.uri = url + serR + serP;
                    $scope.uriv = url + serR;
                    console.info(url);

                } else {
                    modal.mensaje("ERROR", "No se pudo leer el servicio");
                }
            }, function (data) {
                console.info(data);
            });
        };

        $scope.pdf = true;
        $scope.mostrar = function () {
            $scope.pdf = !$scope.pdf;
        };
        $scope.mostrar2 = function () {
            $scope.pdf = !$scope.pdf;
        };

        $scope.OpenBrowserInNewTab = function (nroPagAgrp, pagIni, uriv) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/visorPDF.html",
                controller: "visorCtrl",
                inputs: {
                    numPaginas: nroPagAgrp,
                    idPagDigital: pagIni,
                    uri: uriv
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararVerAgrupamientos = function (documento) {
            $scope.doc = documento;
            if (!doc.estagrupamiento)
                modal.mensaje("ERROR", "el documento no tiene agruapmientos");


        };
    }]);
seApp.controller('actualizarFichaEscalafonariav2Ctrl', ['$routeParams', '$scope', '$rootScope', '$http', 'NgTableParams', 'crud', 'modal', 'ModalService', function ($routeParams, $scope, $rootScope, $http, NgTableParams, crud, modal, ModalService) {

        //var datos = JSON.parse(atob($routeParams.data));
        //$scope.dataRidea = datos;
        //Variables y funciones para controlar la vista
        $scope.optionsDatosPersonales = [true, false, false, false, false];
        $scope.optionsDatosAcademicos = [true, false, false, false, false, false];
        $scope.optionsExperienciaLaboral = [true, false, false];
        
        $scope.optionsCargos = [true, false, false];

        var url;
        var serR = "/rideaoffice";
        var serP = "/getPdf?"
        $scope.uri;
        $scope.uriv;

        $scope.prepararVerAgrups = function (documento) {
            $scope.doc = documento;
            //ver servicio
            var request = crud.crearRequest('servicioRidea', 1, 'verServicioRIDEA');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                if (data.data) {
                    url = data.data.servicio;
                    $scope.uri = url + serR + serP;
                    $scope.uriv = url + serR;
                } else {
                    modal.mensaje("ERROR", "No se pudo leer el servicio");
                }
            }, function (data) {
                console.info(data);
            });
            //console.log("preparing doc:" +doc.agrupamientos);
            if (!$scope.doc.estagrupamiento)
                modal.mensaje("ERROR", "el documento no tiene agruapmientos");
        };



        $scope.updateOptions = function (numOption, optionsSet) {
            for (var i = 0; i < optionsSet.length; i += 1) {
                optionsSet[i] = false;
            }
            optionsSet[numOption] = true;
        };

        $scope.status_1 = {close: true, statusGD: false, statusLD: false};
        $scope.changeDiscapacidadSi = function () {
            $scope.fichaEscalafonariaSel.perDis = true;
        };
        $scope.changeDiscapacidadNo = function () {
            $scope.fichaEscalafonariaSel.perDis = false;
        };

        $scope.statusDA = false;
        $scope.changeDASi = function () {
            $scope.statusDA = false;
        };
        $scope.changeDANo = function () {
            $scope.statusDA = true;
        };

        function changeAseguradoState(codAutEss) {
            if (codAutEss !== "               ")
                $scope.asegurado = "SI";
            else
                $scope.asegurado = "NO";
        }
        ;

        function changeSistemaPensionesState(sisPen) {
            if (sisPen === "ONP")
                $scope.optionsSP = "NO";
            else
                $scope.optionsSP = "SI";
        }
        ;

        function changeDishabilityState(perDis) {
            if (perDis === true)
                $scope.discapacidad = "SI";
            else
                $scope.discapacidad = "NO";
        }
        ;

        $scope.parsearBooleano = function (i) {
            var o = "";
            if (i) {
                o = "Si";
            } else {
                o = "No";
            }
            return o;
        };

        $scope.parsearBooleano2 = function (i) {
            var o = "";
            if (i) {
                o = "Habilitado";
            } else {
                o = "No Habilitado";
            }
            return o;
        };

        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.estadoCivil = [{id: "S", title: "Soltero(a)"}, {id: "C", title: "Casado(a)"}];
        $scope.tipoTrabajador = [{id: "N", title: "Nombrado"}, {id: "V", title: "Contratado"}];
        $scope.grupoOcupacional = [{id: "A", title: "Administrativo"}, {id: "D", title: "Docente"}];
        $scope.afps = ["20530", "25897"]

        $rootScope.paramsDatCenLab = {count: 10};
        $rootScope.settingDatCenLab = {counts: []};
        $rootScope.tablaDatCenLab = new NgTableParams($rootScope.paramsDatCenLab, $rootScope.settingDatCenLab);

        $rootScope.paramsModParientes = {count: 10};
        $rootScope.settingModParientes = {counts: []};
        $rootScope.tablaModParientes = new NgTableParams($rootScope.paramsModParientes, $rootScope.settingModParientes);

        $rootScope.paramsModForEdu = {count: 10};
        $rootScope.settingModForEdu = {counts: []};
        $rootScope.tablaModForEdu = new NgTableParams($rootScope.paramsModForEdu, $rootScope.settingModForEdu);

        $rootScope.paramsModEstCom = {count: 10};
        $rootScope.settingModEstCom = {counts: []};
        $rootScope.tablaModEstCom = new NgTableParams($rootScope.paramsModEstCom, $rootScope.settingModEstCom);

        $rootScope.paramsModExpPon = {count: 10};
        $rootScope.settingModExpPon = {counts: []};
        $rootScope.tablaModExpPon = new NgTableParams($rootScope.paramsModExpPon, $rootScope.settingModExpPon);

        $rootScope.paramsModPublicaciones = {count: 10};
        $rootScope.settingModPublicaciones = {counts: []};
        $rootScope.tablaModPublicaciones = new NgTableParams($rootScope.paramsModPublicaciones, $rootScope.settingModPublicaciones);

        $rootScope.paramsModDesplazamientos = {count: 10};
        $rootScope.settingModDesplazamientos = {counts: []};
        $rootScope.tablaModDesplazamientos = new NgTableParams($rootScope.paramsModDesplazamientos, $rootScope.settingModDesplazamientos);

        $rootScope.paramsModColegiaturas = {count: 10};
        $rootScope.settingModColegiaturas = {counts: []};
        $rootScope.tablaModColegiaturas = new NgTableParams($rootScope.paramsModModColegiaturas, $rootScope.settingcColegiaturas);

        $rootScope.paramsModAscensos = {count: 10};
        $rootScope.settingModAscensos = {counts: []};
        $rootScope.tablaModAscensos = new NgTableParams($rootScope.paramModsAscensos, $rootScope.settingModAscensos);

        $rootScope.paramsModCapacitaciones = {count: 10};
        $rootScope.settingModCapacitaciones = {counts: []};
        $rootScope.tablaModCapacitaciones = new NgTableParams($rootScope.paramsModCapacitaciones, $rootScope.settingModCapacitaciones);

        $rootScope.paramsModLicencias = {count: 10};
        $rootScope.settingModLicencias = {counts: []};
        $rootScope.tablaModLicencias = new NgTableParams($rootScope.paramModLicencias, $rootScope.settingModLicencias);

        $rootScope.paramsModVacaciones = {count: 10};
        $rootScope.settingModVacaciones = {counts: []};
        $rootScope.tablaModVacaciones = new NgTableParams($rootScope.paramModsVacaciones, $rootScope.settingModVacaciones);

        $rootScope.paramsModReconocimientos = {count: 10};
        $rootScope.settingModReconocimientos = {counts: []};
        $rootScope.tablaModReconocimientos = new NgTableParams($rootScope.paramsModReconocimientos, $rootScope.settingModReconocimientos);

        $rootScope.paramsModEstPos = {count: 10};
        $rootScope.settingModEstPos = {counts: []};
        $rootScope.tablaModEstPos = new NgTableParams($rootScope.paramsModEstPos, $rootScope.settingModEstPos);

        $rootScope.paramsModDemeritos = {count: 10};
        $rootScope.settingModDemeritos = {counts: []};
        $rootScope.tablaModDemeritos = new NgTableParams($rootScope.paramsModDemeritos, $rootScope.settingModDemeritos);

        var datosPersonales = JSON.parse(atob($routeParams.data));

        // setting ridea data
        $scope.dataRidea = datosPersonales.Documentos;
        //"http://190.119.213.87:8091/rideaoffice/getPdf?"
        $scope.uriRidea = $scope.uri;

        $scope.trabajadorData = datosPersonales.persona.nom + ' ' + datosPersonales.persona.apePat + ' ' + datosPersonales.persona.apeMat;
        $scope.trabajadorDNI = datosPersonales.persona.dni;
        $scope.trabajadorCargo = "";
        datosPersonales.sessiones.forEach(function (element) {
            $scope.trabajadorCargo += element.rol + " - ";
        });
        $scope.trabajadorCargo = $scope.trabajadorCargo !== "" ? $scope.trabajadorCargo.substr(0, $scope.trabajadorCargo.length - 3) : "";
        $scope.estLabTraSel = "-1";

        $scope.personaSel = {
            perId: datosPersonales.persona.perId,
            perCod: datosPersonales.persona.perCod,
            apePat: datosPersonales.persona.apePat,
            apeMat: datosPersonales.persona.apeMat,
            nom: datosPersonales.persona.nom,
            dni: datosPersonales.persona.dni,
            fecNac: datosPersonales.persona.fecNac,
            eda: datosPersonales.persona.fecNac != "" ? Date().substr(11, 4) - datosPersonales.persona.fecNac.split("-")[0] : "",
            num1: datosPersonales.persona.num1,
            num2: datosPersonales.persona.num2,
            fij: datosPersonales.persona.fij,
            email: datosPersonales.persona.email,
            sex: datosPersonales.persona.sex,
            estCiv: datosPersonales.persona.estCiv,
            nac: datosPersonales.persona.nac,
            depNac: datosPersonales.persona.depNac,
            proNac: datosPersonales.persona.proNac,
            disNac: datosPersonales.persona.disNac,
            licCond: datosPersonales.persona.licCond,
            bonCaf: datosPersonales.persona.bonCaf,
            idiom: datosPersonales.persona.idiom,
            estado: 'A'
        };

        $scope.direccionDNISel = {
            dirId: 0,
            tipDir: "R",
            nomDir: "",
            depDir: "",
            proDir: "",
            disDir: "",
            nomZon: "",
            desRef: ""
        };
        $scope.direccionDASel = {
            dirId: 0,
            tipDir: "A",
            nomDir: "",
            depDir: "",
            proDir: "",
            disDir: "",
            nomZon: "",
            desRef: ""
        };

        var direcciones = [];
        function listarDirecciones() {
            var request = crud.crearRequest('datos_personales', 1, 'listarDirecciones');
            request.setData({perId: datosPersonales.persona.perId});
            crud.listar('/sistema_escalafon', request, function (response) {
                direcciones = response.data;
                changeOptionsDirectionsState();
            }, function (data) {
                console.info(data);
            });
        }
        function changeOptionsDirectionsState() {
            switch (direcciones.length) {
                case 1:
                    $scope.optionsDirections = "SI";
                    $scope.status_1.statusGD = true;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDir = direcciones[0].depDir;
                    $scope.direccionDNISel.proDir = direcciones[0].proDir;
                    $scope.direccionDNISel.disDir = direcciones[0].disDir;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;
                    break;

                case 2:
                    $scope.optionsDirections = "NO";
                    $scope.status_1.statusGD = true;
                    $scope.status_1.statusLD = true;
                    $scope.direccionDNISel.dirId = direcciones[0].dirId;
                    $scope.direccionDNISel.tipDir = direcciones[0].tipDir;
                    $scope.direccionDNISel.nomDir = direcciones[0].nomDir;
                    $scope.direccionDNISel.depDir = direcciones[0].depDir;
                    $scope.direccionDNISel.proDir = direcciones[0].proDir;
                    $scope.direccionDNISel.disDir = direcciones[0].disDir;
                    $scope.direccionDNISel.nomZon = direcciones[0].nomZon;
                    $scope.direccionDNISel.desRef = direcciones[0].desRef;

                    $scope.direccionDASel.dirId = direcciones[1].dirId;
                    $scope.direccionDASel.tipDir = direcciones[1].tipDir;
                    $scope.direccionDASel.nomDir = direcciones[1].nomDir;
                    $scope.direccionDASel.depDir = direcciones[1].depDir;
                    $scope.direccionDASel.proDir = direcciones[1].proDir;
                    $scope.direccionDASel.disDir = direcciones[1].disDir;
                    $scope.direccionDASel.nomZon = direcciones[1].nomZon;
                    $scope.direccionDASel.desRef = direcciones[1].desRef;
                    break;
            }
        }
        listarDirecciones();

        $scope.trabajadorSel = {
            traId: datosPersonales.trabajador.traId,
            traCon: datosPersonales.trabajador.traCon
        };
        $scope.fichaEscalafonariaSel = {
            ficEscId: datosPersonales.ficha.ficEscId,
            autEss: datosPersonales.ficha.autEss,
            sisPen: datosPersonales.ficha.sisPen,
            nomAfp: datosPersonales.ficha.nomAfp,
            codCuspp: datosPersonales.ficha.codCuspp,
            fecIngAfp: datosPersonales.ficha.fecIngAfp == "" ? "" : new Date(datosPersonales.ficha.fecIngAfp),
            perDis: datosPersonales.ficha.perDis == "" ? false : datosPersonales.ficha.perDis,
            regCon: datosPersonales.ficha.regCon
        };

        changeAseguradoState($scope.fichaEscalafonariaSel.autEss);
        changeSistemaPensionesState($scope.fichaEscalafonariaSel.sisPen);
        changeDishabilityState($scope.fichaEscalafonariaSel.perDis);

        var tiposFormacion = [
            {id: "1", title: "Estudios Basicos Regulares"},
            {id: "2", title: "Estudios Tecnicos"},
            {id: "3", title: "Bachiller"},
            {id: "4", title: "Universitario"},
            {id: "5", title: "Licenciatura"},
            {id: "6", title: "Maestria"},
            {id: "7", title: "Doctorado"},
            {id: "8", title: "Otros"}
        ];

        var tiposEstCom = [
            {id: "1", title: "Informatica"},
            {id: "2", title: "Idiomas"},
            {id: "3", title: "Certificación"},
            {id: "4", title: "Diplomado"},
            {id: "5", title: "Especialización"},
            {id: "6", title: "Otros"}
        ];

        var niveles = [
            {id: "N", title: "Ninguno"},
            {id: "B", title: "Básico"},
            {id: "I", title: "Intermedio"},
            {id: "A", title: "Avanzado"}
        ];

        $scope.tipDocs = ["Resolución decanal", "Rsolución rectoral", "Resolución subdirectoral", "Resolución de consejo universitario",
            "Resolución directoral (DIGA)", "Memorándum", "Oficio", "Constancia", "Otros"];

        $scope.jorLabs = ["Completa", "Parcial", "Otros"];

        var tiposEstPos = [{id: "1", title: "Maestria"}, {id: "2", title: "Doctorado"}];

        var tiposDesplazamientos = [
            {id: "1", title: "Ingreso"},
            {id: "2", title: "Reingreso"},
            {id: "3", title: "Interrupción"}
        ];

        var motivos = [
            {id: "1", title: "Mérito"},
            {id: "2", title: "Felicitación"},
            {id: "3", title: "25 años de servicio"},
            {id: "4", title: "30 años de servicio"},
            {id: "5", title: "Luto y sepelio"},
            {id: "6", title: "Otros"}
        ];

        $http.get('../recursos/json/departamentos.json').success(function (data) {
            $scope.departamentosLugNac = data;
            $scope.departamentosDirDNI = data;
            $scope.departamentosDirDA = data;
        });

        $scope.loadProLugNac = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.personaSel.depNac;
                })[0];
                $scope.provinciasLugNac = data[dep.id_ubigeo];
                $scope.distritosLugNac = [];
            });
        };

        $scope.loadDisLugNac = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasLugNac.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.personaSel.proNac;
                })[0];
                $scope.distritosLugNac = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDNI = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDNISel.depDir;
                })[0];
                $scope.provinciasDirDNI = data[dep.id_ubigeo];
                $scope.distritosDirDNI = [];
            });
        };

        $scope.loadDisDirDNI = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDNI.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDNISel.proDir;
                })[0];
                $scope.distritosDirDNI = data[pro.id_ubigeo];
            });
        };

        $scope.loadProDirDA = function () {
            $http.get('../recursos/json/provincias.json').success(function (data) {
                var dep = $scope.departamentosDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDASel.depDir;
                })[0];
                $scope.provinciasDirDA = data[dep.id_ubigeo];
                $scope.distritosDirDA = [];
            });
        };

        $scope.loadDisDirDA = function () {
            $http.get('../recursos/json/distritos.json').success(function (data) {
                var pro = $scope.provinciasDirDA.filter(function (obj) {
                    return obj.codigo_ubigeo === $scope.direccionDASel.proDir;
                })[0];
                $scope.distritosDirDA = data[pro.id_ubigeo];
            });
        };

        $scope.nuevaFichaEscalafonaria = {ficEscId: 0, autEss: "", sisPen: "", nomAfp: "", codCuspp: "", fecIngAfp: "", perDis: false, regCon: "", gruOcu: ""};
        $scope.legajoDatosPersonales = [
            {ficEscId: "", nom: "", archivo: "", url: "", des: "DNI", catLeg: "1", subCat: "1", codAspOri: 0},
            {ficEscId: "", nom: "", archivo: "", url: "", des: "Partida de Nacimiento", catLeg: "1", subCat: "2", codAspOri: 0},
            {ficEscId: "", nom: "", archivo: "", url: "", des: "Registro CONADIS", catLeg: "1", subCat: "4", codAspOri: 0}
        ];

        var estadosLaborales = [
            {estLabId: '1', estLabDes: "Ingreso"},
            {estLabId: '2', estLabDes: "Rotación"},
            {estLabId: '3', estLabDes: "Proyección"},
            {estLabId: '4', estLabDes: "Retiro"},
            {estLabId: '9', estLabDes: "Reingreso"}
        ];

        $scope.OpenBrowserInNewTab = function (coddocumento, nroPagAgrp, pagIni, uriv) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/visorPDF.html",
                controller: "visorCtrl",
                inputs: {
                    numPaginas: nroPagAgrp,
                    idPagDigital: pagIni,
                    uri: uriv
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        }

        $scope.mostrarEstLabFormato = function (row) {
            //console.log("estlab of the fucker: "+row.estLab);
            if (row.estLab === undefined || row.estLab === null) {
                row.estLab = '1';
            }
            ;
            switch (row.estLab) {
                case '1':
                    $("#btn-est-lab" + row.traId).removeClass("btn-warning").addClass("btn-success");
                    return estadosLaborales[0].estLabDes;
                case '2':
                    $("#btn-est-lab" + row.traId).removeClass("btn-success").addClass("btn-warning");
                    return estadosLaborales[1].estLabDes;
                case '3':
                    $("#btn-est-lab" + row.traId).removeClass("btn-danger").addClass("btn-info");
                    return estadosLaborales[2].estLabDes;
                case '4':
                    $("#btn-est-lab" + row.traId).removeClass("btn-info").addClass("btn-danger");
                    return estadosLaborales[3].estLabDes;
                case '9':
                    $("#btn-est-lab" + row.traId).removeClass("btn-warning").addClass("btn-success");
                    return estadosLaborales[4].estLabDes;
            }
        };
        //Funciones para listas los datos de tablas
        $rootScope.listarDatCenLabv2 = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('datos_trabajador', 1, 'listarDatCenLabTra');
            request.setData({perId: datosPersonales.persona.perId});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response.data);
                    //$scope.data=response.data;
                    
                } else {
                    response.responseMsg;
                }
            }, function (error) {
                console.info(error);
            });
        };
        //Funciones para listas los datos de tablas
        $rootScope.listarDatCenLab = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('datos_trabajador', 1, 'listarDatCenLabTra');
            request.setData({perId: datosPersonales.persona.perId});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    console.log(response.data);
                    response.data.forEach(function (item) {
                        item.estLabDes = buscarContenido(estadosLaborales, 'estLabId', 'estLabDes', item.estLab);
                        datosPersonales.trabajador.estLab = item.estLabDes;
                        $scope.estLabTraSel = item.estLab;

                    });
                    $rootScope.settingDatCenLab.dataset = response.data;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingDatCenLab.dataset);
                    $rootScope.tablaDatCenLab.settings($rootScope.settingDatCenLab);
                } else {
                    response.responseMsg;
                }
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarParientes = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarParientes');
            request.setData($scope.trabajadorSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.parEda = Date().substr(11, 4) - item.parFecNac.split("-")[0];
                });
                $rootScope.settingModParientes.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModParientes.dataset);
                $rootScope.tablaModParientes.settings($rootScope.settingModParientes);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarFormacionesEducativas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('formacion_educativa', 1, 'listarFormacionesEducativas');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.estConDes = item.estCon ? "Si" : "No";
                    item.tipDes = buscarContenido(tiposFormacion, 'id', 'title', item.tip);
                });
                $rootScope.settingModForEdu.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModForEdu.dataset);
                $rootScope.tablaModForEdu.settings($rootScope.settingModForEdu);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarColegiaturas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('colegiatura', 1, 'listarColegiaturas');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.conRegDes = item.conReg ? "Habilitado" : "No habilitado";
                });
                $rootScope.settingModColegiaturas.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModColegiaturas.dataset);
                $rootScope.tablaModColegiaturas.settings($rootScope.settingModColegiaturas);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarEstudiosComplementarios = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_complementario', 1, 'listarEstudiosComplementarios');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.tipDes = buscarContenido(tiposEstCom, 'id', 'title', item.tip);
                    item.nivDes = buscarContenido(niveles, 'id', 'title', item.niv);
                });
                $rootScope.settingModEstCom.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModEstCom.dataset);
                $rootScope.tablaModEstCom.settings($rootScope.settingModEstCom);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarEstudiosPostgrado = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('estudio_postgrado', 1, 'listarEstudiosPostgrado');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.tipDes = buscarContenido(tiposEstPos, 'id', 'title', item.tip);
                });
                $rootScope.settingModEstPos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModEstPos.dataset);
                $rootScope.tablaModEstPos.settings($rootScope.settingModEstPos);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarExposiciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('exposicion_ponencia', 1, 'listarExposiciones');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                $rootScope.settingModExpPon.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModExpPon.dataset);
                $rootScope.tablaModExpPon.settings($rootScope.settingModExpPon);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarPublicaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('publicacion', 1, 'listarPublicaciones');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                $rootScope.settingModPublicaciones.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModPublicaciones.dataset);
                $rootScope.tablaModPublicaciones.settings($rootScope.settingModPublicaciones);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarDesplazamientos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.tipDes = buscarContenido(tiposDesplazamientos, 'id', 'title', item.tip);
                });
                $rootScope.settingModDesplazamientos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModDesplazamientos.dataset);
                $rootScope.tablaModDesplazamientos.settings($rootScope.settingModDesplazamientos);
            }, function (error) {
                console.info(error);
            });
        };



        $scope.planillas = [];
        listarPlanillas();
        function listarPlanillas() {
            var request = crud.crearRequest('planillaConfiguracion', 1, 'listarPlanillas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.forEach(function (item) {
                        $scope.planillas[item.plaId] = item.nomPla;
                    });
                    //$scope.planillas[response.data.plaId] = response.data.nomPla;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.categorias = [];
        listarCategorias();
        function listarCategorias() {
            var request = crud.crearRequest('categoriaConfiguracion', 1, 'listarCategorias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.forEach(function (item) {
                        $scope.categorias[item.catId] = item.nomCat;
                    });
                    //$scope.categorias[response.data.catId]=response.data.nomCat;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.organismos = [];
        listarOrganismos();
        function listarOrganismos() {
            var request = crud.crearRequest('organismoConfiguracion', 1, 'listarOrganismos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.forEach(function (item) {
                        $scope.organismos[item.orgaId] = item.nomOrga;
                    });
                    //$scope.organismos[response.data.orgaId] = response.data.nomOrga;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.dependencias = [];
        listarDependencias();
        function listarDependencias() {
            var request = crud.crearRequest('dependenciaConfiguracion', 1, 'listarDependencias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.forEach(function (item) {
                        $scope.dependencias[item.depId] = item.nomDep;
                    });
                    //console.log("deps: "+JSON.stringify($scope.dependencias));
                    //$scope.dependencias[response.data.depId]= response.data.nomDep;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.listarAscensos = function () {

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            var rols = [], tiposAscn = ["Ascenso", "Rotación", "Nombramiento"];
            var request = crud.crearRequest('rol', 1, 'listarRoles');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/configuracionInicial", request, function (data) {
                //$scope.roles = data.data;
                data.data.forEach(function (item) {
                    rols[item.rolID] = item.nombre;
                });
                //preparamos un objeto request
                var request = crud.crearRequest('ascenso', 1, 'listarAscensos');
                request.setData($scope.fichaEscalafonariaSel);
                crud.listar("/sistema_escalafon", request, function (response) {
                    response.data.forEach(function (item) {
                        item.tipNom = tiposAscn[item.tip - 1];
                        item.catNom = $scope.categorias[item.catId];
                        item.depNom = $scope.dependencias[item.depId];
                        item.orgNom = $scope.organismos[item.orgId];
                        item.plaNom = $scope.planillas[item.plaId];
                        item.escNom = rols[parseInt(item.esc)];
                        //console.log("ascnso: "+JSON.stringify(item));
                    });
                    $rootScope.settingModAscensos.dataset = response.data;
                    //asignando la posicion en el arreglo a cada objeto
                    iniciarPosiciones($rootScope.settingModAscensos.dataset);
                    $rootScope.tablaModAscensos.settings($rootScope.settingModAscensos);
                }, function (error) {
                    console.info(error);
                });
                //console.log("rols2: "+JSON.stringify($scope.rols));
            }, function (data) {
                console.info(data);
            });

        };

        $scope.listarCapacitaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('capacitacion', 1, 'listarCapacitaciones');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                $rootScope.settingModCapacitaciones.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModCapacitaciones.dataset);
                $rootScope.tablaModCapacitaciones.settings($rootScope.settingModCapacitaciones);
            }, function (error) {
                console.info(error);
            });
        };

        var tiposLicencias = [{id: "1", title: "Enfermedad"}, {id: "2", title: "Maternidad"}, {id: "3", title: "Capacitación"}, {id: "4", title: "Otros"}];
        var tiposResolucion = [{id: "1", title: "Directoral"}, {id: "2", title: "Rectoral"}, {id: "3", title: "Otros"}];
        $scope.listarLicencias = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('licencia', 1, 'listarLicencias');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.estGocDes = item.estGocDes ? "Con Goce" : "Sin Goce";
                    fecTe = new Date(item.fecTer);
                    fecIn = new Date(item.fecIni);
                    item.dias = (fecTe - fecIn) / (1000 * 60 * 60 * 24) + 1;
                    item.tipDes = tiposLicencias[parseInt(item.tip) - 1].title;
                });
                $rootScope.settingModLicencias.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModLicencias.dataset);
                $rootScope.tablaModLicencias.settings($rootScope.settingModLicencias);
            }, function (error) {
                console.info(error);
            });
        };

        var tiposResolucion = [{id: "1", title: "Directoral"}, {id: "2", title: "Rectoral"}, {id: "3", title: "Otros"}];
        $scope.listarVacaciones = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('vacacion', 1, 'listarVacaciones');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.estGocDes = item.estGoc ? "Con Goce" : "Sin Goce";
                    fecTe = new Date(item.fecTer);
                    fecIn = new Date(item.fecIni);
                    item.dias = (fecTe - fecIn) / (1000 * 60 * 60 * 24) + 1;
                    item.tip = tiposLicencias[parseInt(item.tip) - 1];
                    //item.tipRes = tiposResolucion[parseInt(item.tipRes)-1].title;
                });
                $rootScope.settingModVacaciones.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModVacaciones.dataset);
                $rootScope.tablaModVacaciones.settings($rootScope.settingModVacaciones);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarReconocimientos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('reconocimientos', 1, 'listarReconocimientos');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.motDes = buscarContenido(motivos, 'id', 'title', item.mot);
                });
                $rootScope.settingModReconocimientos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModReconocimientos.dataset);
                $rootScope.tablaModReconocimientos.settings($rootScope.settingModReconocimientos);
            }, function (error) {
                console.info(error);
            });
        };

        $scope.listarDemeritos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('demeritos', 1, 'listarDemeritos');
            request.setData($scope.fichaEscalafonariaSel);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    item.sepDes = item.sep ? "Si" : "No";
                });
                $rootScope.settingModDemeritos.dataset = response.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingModDemeritos.dataset);
                $rootScope.tablaModDemeritos.settings($rootScope.settingModDemeritos);
            }, function (error) {
                console.info(error);
            });
        };

        //Agregar Nueva formación educativa
        $scope.showNuevaFormacionEducativa = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarFormacionEducativa.html",
                controller: "agregarNuevaFormacionEducativaCtrl",
                inputs: {
                    title: "Nueva Formacion Educativa",
                    fichaEscalafonariaId: $scope.nuevaFichaEscalafonaria.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };


        //Agregar nuevo pariente
        $scope.showNuevoPariente = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarPariente.html",
                controller: "agregarNuevoParienteCtrl",
                inputs: {
                    title: "Nuevo Pariente",
                    personaId: datosPersonales.persona.perId,
                    sexo: $scope.sexo,
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevaColegiatura = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarColegiatura.html",
                controller: "agregarNuevaColegiaturaCtrl",
                inputs: {
                    title: "Nueva Colegiatura",
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevoEstudioComplementario = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarEstudioComplementario.html",
                controller: "agregarNuevoEstudioComplementarioCtrl",
                inputs: {
                    title: "Nuevo Estudio Complementario",
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevoEstudioPostgrado = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarEstudioPostgrado.html",
                controller: "agregarNuevoEstudioPostgradoCtrl",
                inputs: {
                    title: "Nueva Estudio Postgrado",
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevaExposicion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarExposicion.html",
                controller: "agregarNuevaExposicionCtrl",
                inputs: {
                    title: "Nueva Exposicion y/o Ponencia",
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevaPublicacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarPublicacion.html",
                controller: "agregarNuevaPublicacionCtrl",
                inputs: {
                    title: "Nueva Publicacion",
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevoDesplazamiento = function () {
            //console.log("asd: "+JSON.stringify(datosPersonales));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarDesplazamiento.html",
                controller: "agregarNuevoDesplazamientoCtrl",
                inputs: {
                    title: "Nueva Interrupción",
                    fichaEscalafonaria: datosPersonales.ficha,
                    trabajadorInfo: datosPersonales,
                    tipDocs: $scope.tipDocs,
                    jorLabs: $scope.jorLabs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevoAscenso = function (val) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarAscenso.html",
                controller: "agregarNuevoAscensoCtrl",
                inputs: {
                    title: val !== undefined ? "Nuevo Nombramiento" : "Nuevo Ascenso",
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    orgas: $scope.organismos,
                    deps: $scope.dependencias,
                    cats: $scope.categorias,
                    plans: $scope.planillas,
                    nombOasc: val !== undefined,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevaCapacitacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarCapacitacion.html",
                controller: "agregarNuevaCapacitacionCtrl",
                inputs: {
                    title: "Nueva capacitación",
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevaLicencia = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarLicencia.html",
                controller: "agregarNuevaLicenciaCtrl",
                inputs: {
                    title: "Nueva licencia",
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevaVacacion = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarVacacion.html",
                controller: "agregarNuevaVacacionCtrl",
                inputs: {
                    title: "Nueva vacación",
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNuevoReconocimiento = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarReconocimiento.html",
                controller: "agregarNuevoReconocimientoCtrl",
                inputs: {
                    title: "Nuevo Reconocimiento",
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.showNuevoDemerito = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarDemerito.html",
                controller: "agregarNuevoDemeritoCtrl",
                inputs: {
                    title: "Nueva Demerito",
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };
        $scope.direccionesToSend = [];
        $scope.actualizarDatosGenerales = function () {

            if ($scope.direccionDASel.dirId !== 0) {
                $scope.direccionesToSend.push($scope.direccionDASel);
            }

            if ($scope.direccionDNISel.dirId !== 0) {
                $scope.direccionesToSend.push($scope.direccionDNISel);
            }
            var request = crud.crearRequest('datos_personales', 1, 'actualizarDatosPersonales');
            request.setData({persona: $scope.personaSel, trabajador: $scope.trabajadorSel, fichaEscalafonaria: $scope.fichaEscalafonariaSel, direcciones: $scope.direccionesToSend, orgId: $rootScope.usuMaster.organizacion.organizacionID});
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);

                $scope.personaSel.apePat = response.data.persona.apePat;
                $scope.personaSel.apeMat = response.data.persona.apeMat;
                $scope.personaSel.nom = response.data.persona.nom;
                $scope.personaSel.dni = response.data.persona.dni;
                $scope.personaSel.fecNac = new Date(response.data.persona.fecNac);
                $scope.personaSel.num1 = response.data.persona.num1;
                $scope.personaSel.num2 = response.data.persona.num2;
                $scope.personaSel.fij = response.data.persona.fij;
                $scope.personaSel.email = response.data.persona.email;
                $scope.personaSel.sex = response.data.persona.sex;
                $scope.personaSel.estCiv = response.data.persona.estCiv;
                $scope.personaSel.depNac = response.data.persona.depNac;
                $scope.personaSel.proNac = response.data.persona.proNac;
                $scope.personaSel.disNac = response.data.persona.disNac;

                $scope.trabajadorSel.traCon = response.data.trabajador.traCon;

                $scope.fichaEscalafonariaSel.autEss = response.data.ficha.autEss;
                $scope.fichaEscalafonariaSel.sisPen = response.data.ficha.sisPen;
                $scope.fichaEscalafonariaSel.nomAfp = response.data.ficha.nomAfp;
                $scope.fichaEscalafonariaSel.codCuspp = response.data.ficha.codCuspp;
                $scope.fichaEscalafonariaSel.fecIngAfp = new Date(response.data.ficha.fecIngAfp);
                $scope.fichaEscalafonariaSel.perDis = response.data.ficha.perDis;
                $scope.fichaEscalafonariaSel.regCon = response.data.ficha.autEss;
                $scope.fichaEscalafonariaSel.gruOcu = response.data.ficha.gruOcu;

                switch (response.data.direcciones.lenght) {
                    case 1:
                        $scope.direccionDNISel.tipDir = response.data.direcciones[0].tipDir;
                        $scope.direccionDNISel.nomDir = response.data.direcciones[0].nomDir;
                        $scope.direccionDNISel.depDir = response.data.direcciones[0].depDir;
                        $scope.direccionDNISel.proDir = response.data.direcciones[0].proDir;
                        $scope.direccionDNISel.disDir = response.data.direcciones[0].disDir;
                        $scope.direccionDNISel.nomZon = response.data.direcciones[0].nomZon;
                        $scope.direccionDNISel.desRef = response.data.direcciones[0].desRef;

                        break;
                    case 2:
                        $scope.direccionDNISel.tipDir = response.data.direcciones[0].tipDir;
                        $scope.direccionDNISel.nomDir = response.data.direcciones[0].nomDir;
                        $scope.direccionDNISel.depDir = response.data.direcciones[0].depDir;
                        $scope.direccionDNISel.proDir = response.data.direcciones[0].proDir;
                        $scope.direccionDNISel.disDir = response.data.direcciones[0].disDir;
                        $scope.direccionDNISel.nomZon = response.data.direcciones[0].nomZon;
                        $scope.direccionDNISel.desRef = response.data.direcciones[0].desRef;

                        $scope.direccionDASel.tipDir = response.data.direcciones[1].tipDir;
                        $scope.direccionDASel.nomDir = response.data.direcciones[1].nomDir;
                        $scope.direccionDASel.depDir = response.data.direcciones[1].depDir;
                        $scope.direccionDASel.proDir = response.data.direcciones[1].proDir;
                        $scope.direccionDASel.disDir = response.data.direcciones[1].disDir;
                        $scope.direccionDASel.nomZon = response.data.direcciones[1].nomZon;
                        $scope.direccionDASel.desRef = response.data.direcciones[1].desRef;
                        break;
                }

            }, function (data) {
                console.info(data);
            });
        };

        $scope.prepararEditarRotacion = function (d) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstadoRotacion.html",
                controller: "editarEstadoRotacionCtrl",
                inputs: {
                    title: "Rotación",
                    fichaEscalafonariaId: datosPersonales.ficha.ficEscId,
                    trabajadorInfo: d,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarProyeccion = function (d) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstadoProyeccion.html",
                controller: "editarEstadoProyeccionCtrl",
                inputs: {
                    title: "Proyección",
                    trabajadorInfo: d,
                    fichaEscalafonaria: datosPersonales.ficha
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarRetiro = function (d) {
            ModalService.showModal({
                templateUrl: (d.estLab != '4' ? "administrativa/sistema_escalafon/editarEstadoRetiro.html" : "administrativa/sistema_escalafon/agregarReingreso.html"),
                controller: (d.estLab != '4' ? "editarEstadoRetiroCtrl" : "agregarReingresoCtrl"),
                inputs: {
                    title: (d.estLab == '1' ? "Retiro" : "Reingreso"),
                    trabajadorInfo: d,
                    fichaEscalafonaria: datosPersonales.ficha,
                    tipDocs: $scope.tipDocs,
                    jorLabs: $scope.jorLabs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarDatCenLab = function (d) {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDatCenLab.html",
                controller: "editarDatCenLabCtrl",
                inputs: {
                    title: "Edición de datos de centro laboral",
                    trabajadorInfo: d
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };


        $scope.prepararReportePersonalizado = function (t) {
            var trabajadorSel = JSON.parse(JSON.stringify(t));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/modalSeleccionadosPersonalizado.html",
                controller: "modalSeleccionadosPersonalizadoCtrl",
                inputs: {
                    title: "Modal seleccionados Personalizado",
                    trabajadorInfo: trabajadorSel
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarPariente = function (p) {
            var parienteSel = JSON.parse(JSON.stringify(p));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarPariente.html",
                controller: "editarParienteCtrl",
                inputs: {
                    title: "Editar datos del pariente",
                    pariente: parienteSel,
                    personaId: $scope.personaSel.perId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarFormacionEducativa = function (fe) {
            var formacionEducativaSel = JSON.parse(JSON.stringify(fe));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarFormacionEducativa.html",
                controller: "editarFormacionEducativaCtrl",
                inputs: {
                    title: "Editar datos de Formacion Educativa",
                    formacionEducativa: formacionEducativaSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };

        $scope.prepararEditarColegiatura = function (c) {
            var colegiaturaSel = JSON.parse(JSON.stringify(c));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarColegiatura.html",
                controller: "editarColegiaturaCtrl",
                inputs: {
                    title: "Editar datos de Colegiatura",
                    colegiatura: colegiaturaSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };

        $scope.prepararEditarEstudioComplementario = function (e) {
            var estComSel = JSON.parse(JSON.stringify(e));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstudioComplementario.html",
                controller: "editarEstudioComplementarioCtrl",
                inputs: {
                    title: "Editar datos de Estudio Complementario",
                    estudioComplementario: estComSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });

        };

        $scope.prepararEditarEstudioPostgrado = function (e) {
            var estPosSel = JSON.parse(JSON.stringify(e));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarEstudioPostgrado.html",
                controller: "editarEstudioPostgradoCtrl",
                inputs: {
                    title: "Editar datos de Estudio de Postgrado",
                    estudioPostgrado: estPosSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarExposicion = function (e) {
            var expSel = JSON.parse(JSON.stringify(e));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarExposicion.html",
                controller: "editarExposicionCtrl",
                inputs: {
                    title: "Editar datos de Exposicion",
                    exposicion: expSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarPublicacion = function (p) {
            var pubSel = JSON.parse(JSON.stringify(p));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarPublicacion.html",
                controller: "editarPublicacionCtrl",
                inputs: {
                    title: "Editar datos de Publicacion",
                    publicacion: pubSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarDesplazamiento = function (d) {
            var desSel = JSON.parse(JSON.stringify(d));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDesplazamiento.html",
                controller: "editarDesplazamientoCtrl",
                inputs: {
                    title: "Editar datos de Desplazamiento",
                    desplazamiento: desSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    tipDocs: $scope.tipDocs,
                    jorLabs: $scope.jorLabs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarAscenso = function (a) {
            var ascSel = JSON.parse(JSON.stringify(a));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarAscenso.html",
                controller: "editarAscensoCtrl",
                inputs: {
                    title: "Editar datos de Ascenso",
                    ascenso: ascSel,
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    orgas: $scope.organismos,
                    deps: $scope.dependencias,
                    cats: $scope.categorias,
                    plans: $scope.planillas,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarCapacitacion = function (c) {
            var capSel = JSON.parse(JSON.stringify(c));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarCapacitacion.html",
                controller: "editarCapacitacionCtrl",
                inputs: {
                    title: "Editar datos de Capacitacion",
                    capacitacion: capSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarLicencia = function (c) {
            var licSel = JSON.parse(JSON.stringify(c));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarLicencia.html",
                controller: "editarLicenciaCtrl",
                inputs: {
                    title: "Editar datos de Licencia",
                    licencia: licSel,
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarVacacion = function (v) {
            var vacSel = JSON.parse(JSON.stringify(v));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarVacacion.html",
                controller: "editarVacacionCtrl",
                inputs: {
                    title: "Editar datos de Vacación",
                    vacacion: vacSel,
                    trabajadorInfo: datosPersonales,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarReconocimiento = function (r) {
            var recSel = JSON.parse(JSON.stringify(r));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarReconocimiento.html",
                controller: "editarReconocimientoCtrl",
                inputs: {
                    title: "Editar datos de Reconocimiento",
                    reconocimiento: recSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.prepararEditarDemerito = function (d) {
            var demSel = JSON.parse(JSON.stringify(d));

            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarDemerito.html",
                controller: "editarDemeritoCtrl",
                inputs: {
                    title: "Editar datos de Demerito",
                    demerito: demSel,
                    fichaEscalafonariaId: $scope.fichaEscalafonariaSel.ficEscId,
                    tipDocs: $scope.tipDocs
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.eliminarPariente = function (p) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el pariente?", function () {
                var request = crud.crearRequest('familiares', 1, 'eliminarFamiliar');
                request.setData({parId: p.parId, perId: $scope.personaSel.perId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModParientes.settings().dataset, function (item) {
                            return p === item;
                        });
                        $rootScope.tablaModParientes.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarFormacionEducativa = function (fe) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la formación educativa?", function () {
                var request = crud.crearRequest('formacion_educativa', 1, 'eliminarFormacionEducativa');
                request.setData({forEduId: fe.forEduId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModForEdu.settings().dataset, function (item) {
                            return fe === item;
                        });
                        $rootScope.tablaModForEdu.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarColegiatura = function (c) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la colegiatura?", function () {
                var request = crud.crearRequest('colegiatura', 1, 'eliminarColegiatura');
                request.setData({colId: c.colId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModColegiaturas.settings().dataset, function (item) {
                            return c === item;
                        });
                        $rootScope.tablaModColegiaturas.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarEstudioComplementario = function (ec) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el estudio complementario?", function () {
                var request = crud.crearRequest('estudio_complementario', 1, 'eliminarEstudioComplementario');
                request.setData({colId: ec.colId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModEstCom.settings().dataset, function (item) {
                            return ec === item;
                        });
                        $rootScope.tablaModEstCom.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarEstudioPostgrado = function (ep) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el estudio de postgrado?", function () {
                var request = crud.crearRequest('estudio_postgrado', 1, 'eliminarEstudioPostgrado');
                request.setData({estPosId: ep.estPosId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModEstPos.settings().dataset, function (item) {
                            return ep === item;
                        });
                        $rootScope.tablaModEstPos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarExposicion = function (ep) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la exposicion y/o ponencia?", function () {
                var request = crud.crearRequest('exposicion_ponencia', 1, 'eliminarExposicion');
                request.setData({expId: ep.expId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModExpPon.settings().dataset, function (item) {
                            return ep === item;
                        });
                        $rootScope.tablaModExpPon.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarPublicacion = function (p) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la publicacion?", function () {
                var request = crud.crearRequest('publicacion', 1, 'eliminarPublicacion');
                request.setData({pubId: p.pubId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModPublicaciones.settings().dataset, function (item) {
                            return p === item;
                        });
                        $rootScope.tablaModPublicaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarDesplazamiento = function (d) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el desplazamiento?", function () {
                var request = crud.crearRequest('desplazamiento', 1, 'eliminarDesplazamiento');
                request.setData({desId: d.desId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModDesplazamientos.settings().dataset, function (item) {
                            return d === item;
                        });
                        $rootScope.tablaModDesplazamientos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarAscenso = function (a) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el ascenso?", function () {
                var request = crud.crearRequest('ascenso', 1, 'eliminarAscenso');
                request.setData({ascId: a.ascId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModAscensos.settings().dataset, function (item) {
                            return a === item;
                        });
                        $rootScope.tablaModAscensos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarCapacitacion = function (c) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la capacitacion?", function () {
                var request = crud.crearRequest('capacitacion', 1, 'eliminarCapacitacion');
                request.setData({capId: c.capId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModCapacitaciones.settings().dataset, function (item) {
                            return c === item;
                        });
                        $rootScope.tablaModCapacitaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarLicencia = function (l) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la licencia?", function () {
                var request = crud.crearRequest('licencia', 1, 'eliminarLicencia');
                request.setData({licId: l.licId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModLicencias.settings().dataset, function (item) {
                            return l === item;
                        });
                        $rootScope.tablaModLicencias.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarVacacion = function (v) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la vacación?", function () {
                var request = crud.crearRequest('vacacion', 1, 'eliminarVacacion');
                request.setData({vacId: v.vacId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModVacaciones.settings().dataset, function (item) {
                            return v === item;
                        });
                        $rootScope.tablaModVacaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarVacacion = function (v) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la vacación?", function () {
                var request = crud.crearRequest('vacacion', 1, 'eliminarVacacion');
                request.setData({vacId: v.vacId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModVacaciones.settings().dataset, function (item) {
                            return v === item;
                        });
                        $rootScope.tablaModVacaciones.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };


        $scope.eliminarReconocimiento = function (r) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la reconocimiento?", function () {
                var request = crud.crearRequest('reconocimiento', 1, 'eliminarReconocimiento');
                request.setData({recId: r.recId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModReconocimientos.settings().dataset, function (item) {
                            return r === item;
                        });
                        $rootScope.tablaModReconocimientos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

        $scope.eliminarDemerito = function (d) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar la demerito?", function () {
                var request = crud.crearRequest('demerito', 1, 'eliminarDemerito');
                request.setData({demId: d.demId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaModDemeritos.settings().dataset, function (item) {
                            return d === item;
                        });
                        $rootScope.tablaModDemeritos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };
    }]);

seApp.controller('visorCtrl', ['$rootScope', '$scope', '$element', 'close', 'crud', 'modal', 'numPaginas', 'idPagDigital', 'uri', function ($rootScope, $scope, $element, close, crud, modal, numPaginas, idPagDigital, uri) {

        $scope.uri = uri;
        $scope.numPaginas = numPaginas;
        $scope.pagInicial = idPagDigital;
        $scope.pagActual = idPagDigital;
        $scope.pagFinal = numPaginas + idPagDigital - 1;
        $scope.pagInput = "";
        $scope.mostrarPagina = function (t) {
            // when a specific page number is given
            if (t === undefined) {
                if ($scope.pagInput !== "" && !isNaN(parseInt($scope.pagInput)) && $scope.pagInput > 0 && $scope.pagInput <= $scope.numPaginas) {
                    next = $scope.pagInput + $scope.pagInicial - 1;
                    if ($scope.pagActual !== next) {
                        $scope.pagActual = next;
                        $("#idImagenReal").src = $scope.uri + "/cargarPagina?idPagina=" + next;
                    }
                } else
                    alert("Ingrese un número de página válido");
            } else {
                $scope.pagActual = t;
                document.getElementById("idImagenReal").src = $scope.uri + "/cargarPagina?idPagina=" + t;
                document.getElementById("idPagSeleccionada").value = t - $scope.pagInicial + 1;
            }
        };
    }]);

seApp.controller('editarDatCenLabCtrl', ['$rootScope', '$scope', '$element', 'title', 'trabajadorInfo', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, trabajadorInfo, close, crud, modal) {
        $scope.title = title;

        $scope.trabajadorSel = {
            traId: trabajadorInfo.traId,

            orgId: trabajadorInfo.orgId,
            orgNom: trabajadorInfo.orgNom,

            rolId: trabajadorInfo.rolId,
            rolNom: trabajadorInfo.rolNom,

            plaId: trabajadorInfo.plaId,
            plaNom: trabajadorInfo.plaNom,

            catId: trabajadorInfo.catId,
            catNom: trabajadorInfo.catNom,

            orgaId: trabajadorInfo.orgaId,
            orgaNom: trabajadorInfo.orgaNom,

            facId: trabajadorInfo.facId,
            facNom: trabajadorInfo.facNom,

            depId: trabajadorInfo.depId,
            depNom: trabajadorInfo.depNom,

            zonId: trabajadorInfo.zonId,
            zonNom: trabajadorInfo.zonNom,

            carId: trabajadorInfo.carId,
            carNom: trabajadorInfo.carNom,

            estLab: trabajadorInfo.estLab,

            fecIng: trabajadorInfo.fecIngEst ? new Date(trabajadorInfo.fecIng.split("-")[2], trabajadorInfo.fecIng.split("-")[1] - 1, trabajadorInfo.fecIng.split("-")[0]) : "",

            fecIngEst: trabajadorInfo.fecIngEst,

            fecSal: trabajadorInfo.fecSal,

            sal: trabajadorInfo.sal,

            tieServ: trabajadorInfo.tieServ,

            aniosOut: trabajadorInfo.aniosOut
        };


        $scope.estadosLaborales = [
            {estLabId: '1', estLabNom: "Ingreso"},
            {estLabId: '2', estLabNom: "Rotación"},
            {estLabId: '3', estLabNom: "Proyección"},
            {estLabId: '4', estLabNom: "Retiro"}
        ];

        $scope.planillas = [];
        listarPlanillas();
        function listarPlanillas() {
            var request = crud.crearRequest('planillaConfiguracion', 1, 'listarPlanillas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.planillas = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.categorias = [];
        listarCategorias();
        function listarCategorias() {
            var request = crud.crearRequest('categoriaConfiguracion', 1, 'listarCategorias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.categorias = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.zonas = [];
        listarZonas();
        function listarZonas() {
            var request = crud.crearRequest('zonaConfiguracion', 1, 'listarZonas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.zonas = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.cargos = [];
        listarCargos();
        function listarCargos() {
            var request = crud.crearRequest('cargoConfiguracion', 1, 'listarCargos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.cargos = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.organismos = [];
        listarOrganismos();
        function listarOrganismos() {
            var request = crud.crearRequest('organismoConfiguracion', 1, 'listarOrganismos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.organismos = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        var facultades = [];
        $scope.facultadesSel = [];
        listarFacultades();
        function listarFacultades() {
            var request = crud.crearRequest('facultadConfiguracion', 1, 'listarFacultades');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    facultades = response.data;
                    $scope.listarFacultadesSel();
                }
            }, function (data) {
                console.info(data);
            });
        }

        var dependencias = [];
        $scope.dependenciasSel = [];
        listarDependencias();
        function listarDependencias() {
            var request = crud.crearRequest('dependenciaConfiguracion', 1, 'listarDependencias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    dependencias = response.data;
                    $scope.listarDependenciasSel();
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.listarFacultadesSel = function () {
            $scope.facultadesSel = [];
            facultades.forEach(function (item) {
                if (item.orgaId === $scope.trabajadorSel.orgaId)
                    $scope.facultadesSel.push(item);
            });
            $scope.dependenciasSel = [];
        };

        $scope.listarDependenciasSel = function () {
            $scope.dependenciasSel = [];
            dependencias.forEach(function (item) {
                if (item.facId === $scope.trabajadorSel.facId)
                    $scope.dependenciasSel.push(item);
            });
        };

        $scope.actualizarDatCenLab = function () {
            $scope.trabajadorSel.fecIngEst = $scope.trabajadorSel.fecIng !== "" ? true : false;
            var request = crud.crearRequest('datos_trabajador', 1, 'actualizarDatCenLabTra');

            request.setData($scope.trabajadorSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    response.data.i = trabajadorInfo.i;
                    //response.data.fecIng = new Date(response.data.fecIng);
                    $rootScope.settingDatCenLab.dataset[trabajadorInfo.i] = response.data;

                    $rootScope.tablaDatCenLab.reload();
                    $rootScope.listarDatCenLab();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else {
                    modal.mensaje("ERROR", response.responseMsg);
                }
            }, function (error) {
                console.info(error);
            });

        };


    }]);
seApp.controller('editarEstadoRotacionCtrl', ['$rootScope', '$scope', '$element', 'title', 'trabajadorInfo', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'tipDocs', function ($rootScope, $scope, $element, title, trabajadorInfo, fichaEscalafonariaId, close, crud, modal, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        $scope.trabajadorSel = {
            traId: trabajadorInfo.traId,

            orgId: trabajadorInfo.orgId,
            orgNom: trabajadorInfo.orgNom,

            rolId: trabajadorInfo.rolId,
            rolNom: trabajadorInfo.rolNom,

            plaId: trabajadorInfo.plaId,
            plaNom: trabajadorInfo.plaNom,

            catId: trabajadorInfo.catId,
            catNom: trabajadorInfo.catNom,

            orgaId: trabajadorInfo.orgaId,
            orgaNom: trabajadorInfo.orgaNom,

            facId: trabajadorInfo.facId,
            facNom: trabajadorInfo.facNom,

            depId: trabajadorInfo.depId,
            depNom: trabajadorInfo.depNom,

            zonId: trabajadorInfo.zonId,
            zonNom: trabajadorInfo.zonNom,

            carId: trabajadorInfo.carId,
            carNom: trabajadorInfo.carNom,

            fecIng: trabajadorInfo.fecIngEst ? (new Date(trabajadorInfo.fecIng.split("-")[2], trabajadorInfo.fecIng.split("-")[1] - 1, trabajadorInfo.fecIng.split("-")[0])).toISOString() : "",

            fecIngEst: trabajadorInfo.fecIngEst,

            fecSal: trabajadorInfo.fecSal,

            sal: trabajadorInfo.sal,

            tieServ: trabajadorInfo.tieServ,
            aniosOut: trabajadorInfo.aniosOut,

            estLab: '2',
            estLabId: '2',
            estLabNom: "Rotación"
        };

        $scope.planillas = [];
        listarPlanillas();
        function listarPlanillas() {
            var request = crud.crearRequest('planillaConfiguracion', 1, 'listarPlanillas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.planillas = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.categorias = [];
        listarCategorias();
        function listarCategorias() {
            var request = crud.crearRequest('categoriaConfiguracion', 1, 'listarCategorias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.categorias = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.zonas = [];
        listarZonas();
        function listarZonas() {
            var request = crud.crearRequest('zonaConfiguracion', 1, 'listarZonas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.zonas = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.cargos = [];
        listarCargos();
        function listarCargos() {
            var request = crud.crearRequest('cargoConfiguracion', 1, 'listarCargos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.cargos = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.organismos = [];
        listarOrganismos();
        function listarOrganismos() {
            var request = crud.crearRequest('organismoConfiguracion', 1, 'listarOrganismos');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    $scope.organismos = response.data;
                }
            }, function (data) {
                console.info(data);
            });
        }

        var facultades = [];
        $scope.facultadesSel = [];
        listarFacultades();
        function listarFacultades() {
            var request = crud.crearRequest('facultadConfiguracion', 1, 'listarFacultades');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    facultades = response.data;
                    $scope.listarFacultadesSel();
                }
            }, function (data) {
                console.info(data);
            });
        }

        var dependencias = [];
        $scope.dependenciasSel = [];
        listarDependencias();
        function listarDependencias() {
            var request = crud.crearRequest('dependenciaConfiguracion', 1, 'listarDependencias');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    dependencias = response.data;
                    $scope.listarDependenciasSel();
                }
            }, function (data) {
                console.info(data);
            });
        }

        $scope.listarFacultadesSel = function () {
            $scope.facultadesSel = [];
            facultades.forEach(function (item) {
                if (item.orgaId === $scope.trabajadorSel.orgaId)
                    $scope.facultadesSel.push(item);
            });
            $scope.dependenciasSel = [];
        };

        $scope.listarDependenciasSel = function () {
            $scope.dependenciasSel = [];
            dependencias.forEach(function (item) {
                if (item.facId === $scope.trabajadorSel.facId)
                    $scope.dependenciasSel.push(item);
            });
        };

        var ft = new Date();
        ft = ft.toISOString();
        $scope.nuevoAscensoRot = {ficEscId: fichaEscalafonariaId, numDoc: "", fecDoc: "", tipDoc: "", fecEfe: ft, esc: "", tip: "2", catId: "", plaId: "", orgId: "", depId: ""};
        $scope.actualizarDatCenLab = function () {
            //save the old data on the ascenso table
            var request = crud.crearRequest('ascenso', 1, 'agregarAscenso');
            $scope.nuevoAscensoRot.catId = parseInt($scope.trabajadorSel.catId);
            $scope.nuevoAscensoRot.orgId = parseInt($scope.trabajadorSel.orgId);
            $scope.nuevoAscensoRot.plaId = parseInt($scope.trabajadorSel.plaId);
            $scope.nuevoAscensoRot.depId = parseInt($scope.trabajadorSel.depId);
            request.setData($scope.nuevoAscensoRot);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingModAscensos.dataset, response.data);
                    $rootScope.tablaModAscensos.reload();
                }
            }, function (data) {
                console.info(data);
            });
            //new data
            var request = crud.crearRequest('datos_trabajador', 1, 'actualizarDatCenLabTra');
            request.setData($scope.trabajadorSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    /*response.data.i = trabajadorInfo.i;
                     $rootScope.settingDatCenLab.dataset[trabajadorInfo.i] = response.data;*/
                    response.data.fecIng = new Date(response.data.fecIng);
                    $rootScope.listarDatCenLab();
                    $rootScope.tablaDatCenLab.reload();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else {
                    modal.mensaje("ERROR", response.responseMsg);
                }
            }, function (error) {
                console.info(error);
            });

        };


    }]);
seApp.controller('editarEstadoProyeccionCtrl', ['$rootScope', '$scope', '$element', 'title', 'trabajadorInfo', 'close', 'crud', 'modal', 'fichaEscalafonaria', function ($rootScope, $scope, $element, title, trabajadorInfo, close, crud, modal, fichaEscalafonaria) {
        function getDiffDates(fechaInicio, fechaFin) {
            fechaFin = fechaFin !== "" ? fechaFin : new Date();
            // Fecha inicio
            var diaInicio = fechaInicio.getDate();
            var mesInicio = fechaInicio.getMonth() + 1; // 0 Enero, 11 Diciembre
            var anioInicio = fechaInicio.getFullYear();

            // Fecha fin
            var diaFin = fechaFin.getDate();
            var mesFin = fechaFin.getMonth() + 1; // 0 Enero, 11 Diciembre
            var anioFin = fechaFin.getFullYear();

            var anios = 0;
            var mesesPorAnio = 0;
            var diasPorMes = 0;
            var diasTipoMes = 0;
            var tiempoExt = {};
            //
            // Calculo de días del mes
            //
            if (mesInicio == 2) {
                // Febrero
                if ((anioFin % 4 == 0) && ((anioFin % 100 != 0) || (anioFin % 400 == 0))) {
                    // Bisiesto
                    diasTipoMes = 29;
                } else {
                    // No bisiesto
                    diasTipoMes = 28;
                }
            } else if (mesInicio <= 7) {
                // De Enero a Julio los meses pares tienen 30 y los impares 31
                if (mesInicio % 2 == 0) {
                    diasTipoMes = 30;
                } else {
                    diasTipoMes = 31;
                }
            } else if (mesInicio > 7) {
                // De Julio a Diciembre los meses pares tienen 31 y los impares 30
                if (mesInicio % 2 == 0) {
                    diasTipoMes = 31;
                } else {
                    diasTipoMes = 30;
                }
            }

            //
            // Calculo de diferencia de año, mes y dia
            //
            if ((anioInicio > anioFin) || (anioInicio == anioFin && mesInicio > mesFin)
                    || (anioInicio == anioFin && mesInicio == mesFin && diaInicio > diaFin)) {
                // La fecha de inicio es posterior a la fecha fin
                // System.out.println("La fecha de inicio ha de ser anterior a la fecha fin");
                alert("La fecha de inicio ha de ser anterior a la fecha fin");
            } else {
                //tiempoExt.setEstado(true);
                if (mesInicio <= mesFin) {
                    anios = anioFin - anioInicio;
                    if (diaInicio <= diaFin) {
                        mesesPorAnio = mesFin - mesInicio;
                        diasPorMes = diaFin - diaInicio;
                    } else {
                        if (mesFin == mesInicio) {
                            anios = anios - 1;
                        }
                        mesesPorAnio = (mesFin - mesInicio - 1 + 12) % 12;
                        diasPorMes = diasTipoMes - (diaInicio - diaFin);
                    }
                } else {
                    anios = anioFin - anioInicio - 1;
                    if (diaInicio > diaFin) {
                        mesesPorAnio = mesFin - mesInicio - 1 + 12;
                        diasPorMes = diasTipoMes - (diaInicio - diaFin);
                    } else {
                        mesesPorAnio = mesFin - mesInicio + 12;
                        diasPorMes = diaFin - diaInicio;
                    }
                }
            }
            //System.out.println("Han transcurrido " + anios + " Años, " + mesesPorAnio + " Meses y " + diasPorMes + " Días.");		

            /*--------------------------------------------------------------------*/
            /**- Totales                                                         -*/
            /*--------------------------------------------------------------------*/

            // Total Años
            tiempoExt.anios = Math.floor(anios);

            // Total Meses
            tiempoExt.meses = Math.floor(mesesPorAnio);


            // Dias del mes
            tiempoExt.dias = Math.floor(diasPorMes);

            //total dias
            var millsecsPerDay = 86400000; // Milisegundos al día
            tiempoExt.totalDias = Math.floor((fechaFin.getTime() - fechaInicio.getTime()) / millsecsPerDay);
            return tiempoExt;
        }
        function checkDateCorrectness(fec) {
            if (fec.dias / 30 >= 1) {
                fec.meses += Math.floor(fec.dias / 30);
                fec.dias = fec.dias % 30;
            }

            if (fec.meses / 12 >= 1) {
                fec.anios += Math.floor(fec.meses / 12);
                fec.meses = fec.meses % 12;
            }
        }
        $scope.title = title;
        var tieAcuDes = "";
        var tieLicVacDes = "", tieGapsDes = "", tieInterrupsDes = "";
        var tieUNSADes = "";
        var fichaEscalafonariaSel = {
            ficEscId: fichaEscalafonaria.ficEscId,
            autEss: fichaEscalafonaria.autEss,
            sisPen: fichaEscalafonaria.sisPen,
            nomAfp: fichaEscalafonaria.nomAfp,
            codCuspp: fichaEscalafonaria.codCuspp,
            fecIngAfp: fichaEscalafonaria.fecIngAfp == "" ? "" : new Date(fichaEscalafonaria.fecIngAfp),
            perDis: fichaEscalafonaria.perDis == "" ? false : fichaEscalafonaria.perDis,
            regCon: fichaEscalafonaria.regCon
        };
        //Set up the list of displacements and get the last of them
        var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
        request.setData(fichaEscalafonariaSel);
        var f = new Date();
        //console.log(f.getTimezoneOffset());
        var firstDate = "", tmpIn, tmpOut;
        var lastDate = "";
        var lastDesId = "-1";
        var interrups = [], tiempoAcumulado = {}, licenciasSG = [], vacacionesSG = [], gaps = 0, count;
        $scope.trabajadorSel = {};
        // First part to obtain the accumulated and interruptions times
        crud.listar("/sistema_escalafon", request, function (response) {
            count = 0;
            cnt2 = 0;
            response.data.forEach(function (item) {
                // get the date of the ver first entry, but check that the role is the same that we are looking for
                if (item.tip == "1" && item.car === trabajadorInfo.rolNom) {
                    firstDate = new Date(item.fecIni);
                    item.fecTer !== undefined && item.fecTer !== "" && cnt2 == 0 ? lastDate = new Date(item.fecTer) : "";
                    // On this part the firstDate of a gap is calculated by setting it to the "fecTer" value of the current displ
                    // NO need to check nothing because this is a type "1" (entry) displacement 
                    item.fecTer !== undefined && item.fecTer !== "" ? tmpIn = new Date(item.fecTer) : "";
                    // Set the desId of the first entry and on the other items it will change to the last desID
                    lastDesId = item.desId;
                }
                // get the date of the very last entry (entry or re-entry), and if it doesn't exists just set it to the current date
                else if (item.tip == "2" && (item.car === trabajadorInfo.rolNom) && (item.desId !== undefined && item.desId > lastDesId)) {
                    lastDesId = item.desId;
                    item.fecTer !== undefined && item.fecTer !== "" ? lastDate = new Date(item.fecTer) : "";
                    // On this part the lastDate of a gap is set to the "fecTer" value of the current displ of type "2" but:
                    // First we check if the firstDate of the gap is set already, and if it's so, we set the lastDate of the gap and calculate the days within it
                    if (tmpIn !== undefined) {
                        tmpOut = item.fecIni;
                    }
                    // And if it's not, we set the firstDate of the gap with the fecTer of the current displ
                    // the lastDate of the gap will be set on the next iteration that finds a type2 displ,
                    // but temporally it will be the fecIni of the current displ because 
                    else {
                        tmpIn = (item.fecTer !== undefined && item.fecTer !== "" ? new Date(item.fecTer) : undefined);
                        tmpOut = (item.fecIni !== undefined && item.fecTer !== "" ? new Date(item.fecIni) : undefined);
                    }
                }
                // case of an interruption
                else if (item.tip == "3" && item.car === trabajadorInfo.rolNom) {
                    interrups[count++] = getDiffDates(new Date(item.fecIni), new Date(item.fecTer));
                }

                if (tmpIn !== undefined && tmpOut !== undefined && tmpOut.getTime() > tmpIn.getTime()) {
                    gaps += getDiffDates(tmpIn, tmpOut).totalDias;
                    tmpOut = undefined;
                    tmpIn = undefined;
                }
                cnt2++;
            });
            firstDate = new Date(firstDate.getTime() + Math.abs(firstDate.getTimezoneOffset() * 60000));
            // before you set the lastDate check if there's more than 1 displ. in order to set it correctly
            lastDate !== "" ? lastDate = new Date(lastDate.getTime() + Math.abs(lastDate.getTimezoneOffset() * 60000)) : lastDate = new Date();
            //lastDate = new Date(lastDate.getTime() + Math.abs(lastDate.getTimezoneOffset()*60000));
            // Second part to obtain the vacation times
            var request = crud.crearRequest('vacacion', 1, 'listarVacaciones'), count2 = 0;
            request.setData(fichaEscalafonariaSel);
            crud.listar("/sistema_escalafon", request, function (response) {
                response.data.forEach(function (item) {
                    !item.estGoc && item.car === trabajadorInfo.rolNom ? vacacionesSG[count2++] = getDiffDates(new Date(item.fecIni), new Date(item.fecTer)) : "";
                });
                // Third part to obtain the licences times
                var request = crud.crearRequest('licencia', 1, 'listarLicencias'), count3 = 0;
                request.setData(fichaEscalafonariaSel);
                crud.listar("/sistema_escalafon", request, function (response) {
                    response.data.forEach(function (item) {
                        !item.estGoc && item.car === trabajadorInfo.rolNom ? licenciasSG[count3++] = getDiffDates(new Date(item.fecIni), new Date(item.fecTer)) : "";
                    });
                    tiempoAcumulado = getDiffDates(firstDate, lastDate);
                    var tiempoUNSA = {anios: tiempoAcumulado.anios, meses: tiempoAcumulado.meses, dias: tiempoAcumulado.dias, totalDias: tiempoAcumulado.totalDias};
                    var tiempoLicVac = {anios: 0, meses: 0, dias: 0, totalDias: 0}, tiempoInterrups = {anios: 0, meses: 0, dias: 0, totalDias: 0}, tiempoGaps = {anios: 0, meses: 0, dias: 0, totalDias: 0};
                    licenciasSG.forEach(function (elm) {
                        tiempoLicVac.anios += elm.anios;
                        tiempoLicVac.meses += elm.meses;
                        tiempoLicVac.dias += elm.dias;
                        tiempoLicVac.totalDias += elm.totalDias;
                    });
                    vacacionesSG.forEach(function (elm) {
                        tiempoLicVac.anios += elm.anios;
                        tiempoLicVac.meses += elm.meses;
                        tiempoLicVac.dias += elm.dias;
                        tiempoLicVac.totalDias += elm.totalDias;
                    });
                    interrups.forEach(function (elm) {
                        tiempoInterrups.anios += elm.anios;
                        tiempoInterrups.meses += elm.meses;
                        tiempoInterrups.dias += elm.dias;
                        tiempoInterrups.totalDias += elm.totalDias;
                    });
                    tiempoGaps.dias += gaps;
                    tiempoGaps.totalDias += gaps;
                    //Then check the correctness of the time
                    checkDateCorrectness(tiempoLicVac);
                    checkDateCorrectness(tiempoGaps);
                    checkDateCorrectness(tiempoInterrups);
                    if (tiempoLicVac.totalDias > 0 || tiempoGaps.totalDias > 0 || tiempoInterrups.totalDias > 0) {
                        tiempoUNSA.anios = 0;
                        tiempoUNSA.meses = 0;
                        tiempoUNSA.totalDias -= (tiempoLicVac.totalDias + tiempoGaps.totalDias + tiempoInterrups.totalDias);
                        tiempoUNSA.dias = tiempoUNSA.totalDias;
                    }


                    //tiempoUNSA.dias = tiempoUNSA.totalDias;

                    checkDateCorrectness(tiempoUNSA);


                    tieAcuDes = tiempoAcumulado != "" ? tiempoAcumulado.anios + 'años  ' + tiempoAcumulado.meses + 'meses  ' +
                            tiempoAcumulado.dias + 'días' : "";
                    tieLicVacDes = tiempoLicVac != "" ? tiempoLicVac.anios + 'años  ' + tiempoLicVac.meses + 'meses  ' +
                            tiempoLicVac.dias + 'días' : "";
                    tieGapsDes = tiempoGaps != "" ? tiempoGaps.anios + 'años  ' + tiempoGaps.meses + 'meses  ' +
                            tiempoGaps.dias + 'días' : "";
                    tieInterrupsDes = tiempoInterrups != "" ? tiempoInterrups.anios + 'años  ' + tiempoInterrups.meses + 'meses  ' +
                            tiempoInterrups.dias + 'días' : "";
                    tieUNSADes = tiempoUNSA.anios + 'años  ' + tiempoUNSA.meses + 'meses  ' +
                            tiempoUNSA.dias + 'días';
                    //console.log("info about the fucker: "+tieAcuDes);
                    $scope.trabajadorSel = {
                        fecIng: firstDate.getDate() + "-" + (firstDate.getMonth() + 1) + "-" + firstDate.getFullYear(),
                        fecSal: lastDate.getDate() + "-" + (lastDate.getMonth() + 1) + "-" + lastDate.getFullYear(),
                        tieAcu: tieAcuDes,
                        tieLicVac: tieLicVacDes,
                        tieInterrups: tieInterrupsDes,
                        tieGaps: tieGapsDes,
                        tieUNSAparts: tiempoUNSA,
                        tieUNSA: tieUNSADes,
                        aniosOut: {an: Math.floor(trabajadorInfo.aniosOut / 360),
                            me: Math.floor(trabajadorInfo.aniosOut % 360 / 30),
                            di: Math.floor(trabajadorInfo.aniosOut % 360 % 30)}
                    };

                }, function (error) {
                    console.info(error);
                });
            }, function (error) {
                console.info(error);
            });
        }, function (error) {
            console.info(error);
        });
        //$scope.trabajadorSel = trabajadorInfo;
        $scope.actualizarDatCenLab = function () {
            $scope.trabajadorSel.fecIngEst = $scope.trabajadorSel.fecIng !== "" ? true : false;

            if ((!isNaN(parseInt($scope.trabajadorSel.aniosOut.an)) && !isNaN(parseInt($scope.trabajadorSel.aniosOut.me)) && !isNaN(parseInt($scope.trabajadorSel.aniosOut.di)))) {
                if (parseInt($scope.trabajadorSel.aniosOut.an) >= 0 && parseInt($scope.trabajadorSel.aniosOut.me) >= 0 && parseInt($scope.trabajadorSel.aniosOut.di) >= 0) {
                    var request = crud.crearRequest('datos_trabajador', 1, 'actualizarDatCenLabTra');
                    trabajadorInfo.aniosOut = parseInt($scope.trabajadorSel.aniosOut.an) * 360 + parseInt($scope.trabajadorSel.aniosOut.me) * 30 + parseInt($scope.trabajadorSel.aniosOut.di);

                    request.setData(trabajadorInfo);
                    crud.actualizar("/sistema_escalafon", request, function (response) {
                        if (response.responseSta) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        } else {
                            modal.mensaje("ERROR", response.responseMsg);
                        }
                    }, function (error) {
                        console.info(error);
                    });
                }
            } else {
                alert("Datos de tiempo fuera de la UNSA inválidos");
            }
        };
    }]);

seApp.controller('agregarReingresoCtrl', ['$rootScope', '$scope', '$element', 'title', 'trabajadorInfo', 'close', 'crud', 'modal', 'fichaEscalafonaria', 'tipDocs', 'jorLabs', function ($rootScope, $scope, $element, title, trabajadorInfo, close, crud, modal, fichaEscalafonaria, tipDocs, jorLabs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        $scope.jorLabs = jorLabs;
        var ft = new Date();
        ft = ft.toISOString();
        $scope.nuevoDesplazamiento = {ficEscId: fichaEscalafonaria.ficEscId, tip: "2", numDoc: "", fecDoc: "", tipDoc: "", insEdu: trabajadorInfo.orgNom, car: trabajadorInfo.rolNom, jorLab: "", fecIni: (trabajadorInfo.fecIng != null ? trabajadorInfo.fecIng : "2018-01-01T00:00:00.000Z"), fecTer: "", fecDocTer: "", numDocTer: "", motRet: ""};

        $scope.agregarDesplXReingreso = function () {
            var request = crud.crearRequest('desplazamiento', 1, 'agregarDesplazamiento');
            request.setData($scope.nuevoDesplazamiento);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.trabajadorSel = {
            traId: trabajadorInfo.traId,

            orgId: trabajadorInfo.orgId,
            orgNom: trabajadorInfo.orgNom,

            rolId: trabajadorInfo.rolId,
            rolNom: trabajadorInfo.rolNom,

            plaId: trabajadorInfo.plaId,
            plaNom: trabajadorInfo.plaNom,

            catId: trabajadorInfo.catId,
            catNom: trabajadorInfo.catNom,

            orgaId: trabajadorInfo.orgaId,
            orgaNom: trabajadorInfo.orgaNom,

            facId: trabajadorInfo.facId,
            facNom: trabajadorInfo.facNom,

            depId: trabajadorInfo.depId,
            depNom: trabajadorInfo.depNom,

            zonId: trabajadorInfo.zonId,
            zonNom: trabajadorInfo.zonNom,

            carId: trabajadorInfo.carId,
            carNom: trabajadorInfo.carNom,

            fecIng: trabajadorInfo.fecIngEst ? trabajadorInfo.fecIng : "",

            fecIngEst: trabajadorInfo.fecIngEst,
            fecSal: trabajadorInfo.fecSal,

            sal: trabajadorInfo.sal,

            tieServ: trabajadorInfo.tieServ,
            aniosOut: trabajadorInfo.aniosOut,

            estLab: '9',
            estLabId: '9',
            estLabNom: "Reingreso"
        };
        $scope.actualizarDatCenLabReingreso = function () {
            var request = crud.crearRequest('datos_trabajador', 1, 'actualizarDatCenLabTra');
            request.setData($scope.trabajadorSel);
            //console.log("update lab cen: "+JSON.stringify($scope.trabajadorSel));
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    /*response.data.i = trabajadorInfo.i;
                     $rootScope.settingDatCenLab.dataset[trabajadorInfo.i] = response.data;*/
                    response.data.fecIng = new Date(response.data.fecIng);
                    $rootScope.listarDatCenLab();
                    $rootScope.tablaDatCenLab.reload();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else {
                    modal.mensaje("ERROR", response.responseMsg);
                }
            }, function (error) {
                console.info(error);
            });

        };
    }]);
seApp.controller('editarEstadoRetiroCtrl', ['$rootScope', '$scope', '$element', 'title', 'trabajadorInfo', 'close', 'crud', 'modal', 'fichaEscalafonaria', 'tipDocs', 'jorLabs', function ($rootScope, $scope, $element, title, trabajadorInfo, close, crud, modal, fichaEscalafonaria, tipDocs, jorLabs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        $scope.jorLabs = jorLabs;
        var ft = new Date();
        ft = ft.toISOString();
        $scope.nuevoDesplazamiento = {ficEscId: fichaEscalafonaria.ficEscId, tip: "1", numDoc: "", fecDoc: "", tipDoc: "", insEdu: (trabajadorInfo.orgNom != null ? trabajadorInfo.orgNom : "UNSA"), car: trabajadorInfo.carNom, jorLab: "", fecIni: (trabajadorInfo.fecIng != null ? trabajadorInfo.fecIng : "2018-01-01T00:00:00.000Z"), fecTer: ft, motRet: ""};

        // Method to update every last displacement
        $scope.fichaEscalafonariaSel = {
            ficEscId: fichaEscalafonaria.ficEscId,
            autEss: fichaEscalafonaria.autEss,
            sisPen: fichaEscalafonaria.sisPen,
            nomAfp: fichaEscalafonaria.nomAfp,
            codCuspp: fichaEscalafonaria.codCuspp,
            fecIngAfp: fichaEscalafonaria.fecIngAfp == "" ? "" : new Date(datosPersonales.ficha.fecIngAfp),
            perDis: fichaEscalafonaria.perDis == "" ? false : datosPersonales.ficha.perDis,
            regCon: fichaEscalafonaria.regCon
        };
        $scope.actualizarDesplXRetiro = function () {
            //Set up the list of displacements and get the last of them
            var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            var lastDisplacement = "";
            crud.listar("/sistema_escalafon", request, function (response) {
                var counter = 1;
                response.data.forEach(function (item) {
                    // look for the item that has the types 1 or 2 becuase another type indicates an interruption
                    (item.tip != "3") && (item.numDocTer === undefined || item.numDocTer === null || item.numDocTer === "") ? lastDisplacement = item : counter++;
                });
                lastDisplacement = {
                    i: lastDisplacement.i,
                    desId: lastDisplacement.desId,
                    tip: lastDisplacement.tip,
                    numDoc: lastDisplacement.numDoc,
                    fecDoc: lastDisplacement.fecDoc,
                    tipDoc: lastDisplacement.tipDoc,
                    insEdu: lastDisplacement.insEdu,
                    car: lastDisplacement.car,
                    jorLab: lastDisplacement.jorLab,
                    fecIni: new Date(lastDisplacement.fecIni),
                    fecTer: $scope.nuevoDesplazamiento.fecTer,
                    fecDocTer: $scope.nuevoDesplazamiento.fecDoc,
                    numDocTer: $scope.nuevoDesplazamiento.numDoc,
                    tipDocTer: $scope.nuevoDesplazamiento.tipDoc,
                    motRet: $scope.nuevoDesplazamiento.motRet
                };
                //console.log("upd retirement: "+JSON.stringify(lastDisplacement));
                // Once you set up the last displacement, update it
                var request = crud.crearRequest('desplazamiento', 1, 'actualizarDesplazamiento');
                request.setData(lastDisplacement);
                crud.actualizar("/sistema_escalafon", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                }, function (data) {
                    console.info(data);
                });
            }, function (error) {
                console.info(error);
            });
        };
        $scope.trabajadorSel = {
            traId: trabajadorInfo.traId,

            orgId: trabajadorInfo.orgId,
            orgNom: trabajadorInfo.orgNom,

            rolId: trabajadorInfo.rolId,
            rolNom: trabajadorInfo.rolNom,

            plaId: trabajadorInfo.plaId,
            plaNom: trabajadorInfo.plaNom,

            catId: trabajadorInfo.catId,
            catNom: trabajadorInfo.catNom,

            orgaId: trabajadorInfo.orgaId,
            orgaNom: trabajadorInfo.orgaNom,

            facId: trabajadorInfo.facId,
            facNom: trabajadorInfo.facNom,

            depId: trabajadorInfo.depId,
            depNom: trabajadorInfo.depNom,

            zonId: trabajadorInfo.zonId,
            zonNom: trabajadorInfo.zonNom,

            carId: trabajadorInfo.carId,
            carNom: trabajadorInfo.carNom,

            fecIng: trabajadorInfo.fecIngEst ? trabajadorInfo.fecIng : "",

            fecIngEst: trabajadorInfo.fecIngEst,
            fecSal: trabajadorInfo.fecSal,

            sal: trabajadorInfo.sal,

            tieServ: trabajadorInfo.tieServ,
            aniosOut: trabajadorInfo.aniosOut,
            estLab: '4',
            estLabId: '4',
            estLabNom: "Retiro"
        };

        $scope.actualizarDatCenLab = function () {
            var request = crud.crearRequest('datos_trabajador', 1, 'actualizarDatCenLabTra');
            request.setData($scope.trabajadorSel);
            //console.log("trasel: "+JSON.stringify($scope.trabajadorSel));
            crud.actualizar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    /*response.data.i = trabajadorInfo.i;
                     $rootScope.settingDatCenLab.dataset[trabajadorInfo.i] = response.data;*/
                    response.data.fecIng = new Date(response.data.fecIng);
                    $rootScope.listarDatCenLab();
                    $rootScope.tablaDatCenLab.reload();
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                } else {
                    modal.mensaje("ERROR", response.responseMsg);
                }
            }, function (error) {
                console.info(error);
            });

        };


    }]);
seApp.controller('editarParienteCtrl', ['$rootScope', '$scope', '$element', 'title', 'pariente', 'personaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, pariente, personaId, close, crud, modal) {
        $scope.title = title;

        $scope.sexo = [{id: "F", title: "Femenino"}, {id: "M", title: "Masculino"}];
        $scope.tipoPariente = [];

        function listarTipoParentesco() {
            var request = crud.crearRequest('familiares', 1, 'listarTipoParentesco');
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.tipoPariente = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;
        listarTipoParentesco();

        $scope.parienteSel = {
            parId: pariente.parId,
            perId: personaId,
            tipoParienteId: pariente.tipoParienteId,
            parentesco: "",
            parApePat: pariente.parApePat,
            parApeMat: pariente.parApeMat,
            parNom: pariente.parNom,
            parDni: pariente.parDni,
            parFecNac: new Date(pariente.parFecNac),
            parSex: pariente.parSex,
            parNum1: pariente.parNum1,
            parNum2: pariente.parNum2,
            parFij: pariente.parFij,
            parEmail: pariente.parEmail,
            retJud: pariente.retJud,
            parEda: pariente.parEda
        };

        $scope.actualizarPariente = function () {
            var request = crud.crearRequest('familiares', 1, 'actualizarFamiliar');
            request.setData($scope.parienteSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.parentesco = buscarContenido($scope.tipoPariente, 'tpaId', 'tpaDes', response.data.tipoParienteId);
                    response.data.i = pariente.i;
                    response.data.fecNac = new Date(response.data.fecNac);
                    $rootScope.settingModParientes.dataset[pariente.i] = response.data;
                    $rootScope.tablaModParientes.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
seApp.controller('editarFormacionEducativaCtrl', ['$rootScope', '$scope', '$element', 'title', 'formacionEducativa', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, formacionEducativa, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;


        //variable que servira para registrar una nueva formacion academica
        $scope.forEduSel = {
            forEduId: formacionEducativa.forEduId,
            tip: formacionEducativa.tip,
            tipDes: "",
            niv: formacionEducativa.niv,
            numTit: formacionEducativa.numTit,
            esp: formacionEducativa.esp,
            estCon: formacionEducativa.estCon,
            fecExp: new Date(formacionEducativa.fecExp),
            cenEst: formacionEducativa.cenEst
        };
        $scope.tiposFormacion = [
            {id: "1", title: "Estudios Basicos Regulares"},
            {id: "2", title: "Estudios Tecnicos"},
            {id: "3", title: "Bachiller"},
            {id: "4", title: "Universitario"},
            {id: "5", title: "Licenciatura"},
            {id: "6", title: "Maestria"},
            {id: "7", title: "Doctorado"},
            {id: "8", title: "Otros"}
        ];


        $scope.actualizarFormacionEducativa = function () {
            var request = crud.crearRequest('formacion_educativa', 1, 'actualizarFormacionEducativa');
            request.setData($scope.forEduSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.estConDes = response.data.estCon ? "Si" : "No";
                    response.data.tipDes = buscarContenido($scope.tiposFormacion, 'id', 'title', response.data.tip);
                    response.data.i = formacionEducativa.i;
                    $rootScope.settingModForEdu.dataset[formacionEducativa.i] = response.data;
                    $rootScope.tablaModForEdu.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarColegiaturaCtrl', ['$rootScope', '$scope', '$element', 'title', 'colegiatura', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, colegiatura, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;

        //variable que servira para registrar una nueva formacion academica
        $scope.colegiaturaSel = {
            i: colegiatura.i,
            colId: colegiatura.colId,
            nomColPro: colegiatura.nomColPro,
            numRegCol: colegiatura.numRegCol,
            conReg: colegiatura.conReg
        };

        $scope.actualizarColegiatura = function () {
            var request = crud.crearRequest('colegiatura', 1, 'actualizarColegiatura');
            request.setData($scope.colegiaturaSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.conRegDes = response.data.conReg ? "Habilitado" : "No Habilitado";
                    response.data.i = colegiatura.i;
                    $rootScope.settingModColegiaturas.dataset[ colegiatura.i] = response.data;
                    $rootScope.tablaModColegiaturas.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarEstudioComplementarioCtrl', ['$rootScope', '$scope', '$element', 'title', 'estudioComplementario', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, estudioComplementario, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;

        //variable que servira para registrar una nueva formacion academica
        $scope.estComSel = {
            i: estudioComplementario.i,
            estComId: estudioComplementario.estComId,
            tip: estudioComplementario.tip,
            des: estudioComplementario.des,
            niv: estudioComplementario.niv,
            insCer: estudioComplementario.insCer,
            tipPar: estudioComplementario.tipPar,
            fecIni: new Date(estudioComplementario.fecIni),
            fecTer: new Date(estudioComplementario.fecTer),
            horLec: estudioComplementario.horLec,
            pais: estudioComplementario.pais
        };

        $scope.tiposEstCom = [
            {id: "1", title: "Informatica"},
            {id: "2", title: "Idiomas"},
            {id: "3", title: "Certificación"},
            {id: "4", title: "Diplomado"},
            {id: "5", title: "Especialización"},
            {id: "6", title: "Otros"}
        ];

        $scope.niveles = [
            {id: "N", title: "Ninguno"},
            {id: "B", title: "Básico"},
            {id: "I", title: "Intermedio"},
            {id: "A", title: "Avanzado"}
        ];

        $scope.actualizarEstudioComplementario = function () {
            var request = crud.crearRequest('estudio_complementario', 1, 'actualizarEstudioComplementario');
            request.setData($scope.estComSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDes = buscarContenido($scope.tiposEstCom, 'id', 'title', response.data.tip);
                    response.data.nivDes = buscarContenido($scope.niveles, 'id', 'title', response.data.niv);
                    response.data.i = estudioComplementario.i;
                    $rootScope.settingModEstCom.dataset[ estudioComplementario.i] = response.data;
                    $rootScope.tablaModEstCom.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarEstudioPostgradoCtrl', ['$rootScope', '$scope', '$element', 'title', 'estudioPostgrado', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'tipDocs', function ($rootScope, $scope, $element, title, estudioPostgrado, fichaEscalafonariaId, close, crud, modal, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.estPosSel = {
            i: estudioPostgrado.i,
            estPosId: estudioPostgrado.estPosId,
            ficEscId: fichaEscalafonariaId,
            tip: estudioPostgrado.tip,
            numDoc: estudioPostgrado.numDoc,
            fecDoc: new Date(estudioPostgrado.fecDoc),
            tipDoc: estudioPostgrado.tipDoc,
            fecIniEst: new Date(estudioPostgrado.fecIniEst),
            fecTerEst: new Date(estudioPostgrado.fecTerEst),
            ins: estudioPostgrado.ins,
            pais: estudioPostgrado.pais
        };

        $scope.tiposEstPos = [
            {id: "1", title: "Maestria"},
            {id: "2", title: "Doctorado"}
        ];

        $scope.actualizarEstudioPostgrado = function () {
            var request = crud.crearRequest('estudio_postgrado', 1, 'actualizarEstudioPostgrado');
            request.setData($scope.estPosSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDes = buscarContenido($scope.tiposEstPos, 'id', 'title', response.data.tip);
                    response.data.i = estudioPostgrado.i;
                    $rootScope.settingModEstPos.dataset[estudioPostgrado.i] = response.data;
                    $rootScope.tablaModEstPos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarExposicionCtrl', ['$rootScope', '$scope', '$element', 'title', 'exposicion', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, exposicion, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.expPonSel = {
            i: exposicion.i,
            expId: exposicion.expId,
            ficEscId: fichaEscalafonariaId,
            des: exposicion.des,
            insOrg: exposicion.insOrg,
            tipPar: exposicion.tipPar,
            fecIni: new Date(exposicion.fecIni),
            fecTer: new Date(exposicion.fecTer),
            horLec: exposicion.horLec
        };

        $scope.actualizarExposicion = function () {
            var request = crud.crearRequest('exposicion_ponencia', 1, 'actualizarExposicion');
            request.setData($scope.expPonSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = exposicion.i;
                    $rootScope.settingModExpPon.dataset[exposicion.i] = response.data;
                    $rootScope.tablaModExpPon.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarPublicacionCtrl', ['$rootScope', '$scope', '$element', 'title', 'publicacion', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, publicacion, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.publicacionSel = {
            i: publicacion.i,
            pubId: publicacion.pubId,
            nomEdi: publicacion.nomEdi,
            tipPub: publicacion.tipPub,
            titPub: publicacion.titPub,
            graPar: publicacion.graPar,
            lug: publicacion.lug,
            fecPub: new Date(publicacion.fecPub),
            numReg: publicacion.numReg
        };

        $scope.actualizarPublicacion = function () {
            var request = crud.crearRequest('publicacion', 1, 'actualizarPublicacion');
            request.setData($scope.publicacionSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = publicacion.i;
                    $rootScope.settingModPublicaciones.dataset[publicacion.i] = response.data;
                    $rootScope.tablaModPublicaciones.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarDesplazamientoCtrl', ['$rootScope', '$scope', '$element', 'title', 'desplazamiento', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'tipDocs', 'jorLabs', function ($rootScope, $scope, $element, title, desplazamiento, fichaEscalafonariaId, close, crud, modal, tipDocs, jorLabs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        $scope.jorLabs = jorLabs;
        //variable que servira para registrar una nueva formacion academica
        $scope.desplazamientoSel = {
            i: desplazamiento.i,
            desId: desplazamiento.desId,
            tip: desplazamiento.tip,
            numDoc: desplazamiento.numDoc,
            fecDoc: new Date(desplazamiento.fecDoc),
            tipDoc: desplazamiento.tipDoc,
            insEdu: desplazamiento.insEdu,
            car: desplazamiento.car,
            jorLab: desplazamiento.jorLab,
            fecIni: new Date(desplazamiento.fecIni),
            fecTer: new Date(desplazamiento.fecTer)
        };

        $scope.tiposDesplazamientos = [
            {id: "1", title: "Designación"},
            {id: "2", title: "Rotación"},
            {id: "3", title: "Reasignación"},
            {id: "4", title: "Destaque"},
            {id: "5", title: "Permuta"},
            {id: "6", title: "Encargo"},
            {id: "7", title: "Comisión de servicio"},
            {id: "8", title: "Transferencia"},
            {id: "9", title: "Otros"},
        ];

        $scope.actualizarDesplazamiento = function () {
            var request = crud.crearRequest('desplazamiento', 1, 'actualizarDesplazamiento');
            request.setData($scope.desplazamientoSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.tipDes = buscarContenido($scope.tiposDesplazamientos, 'id', 'title', response.data.tip);
                    response.data.i = desplazamiento.i;
                    $rootScope.settingModDesplazamientos.dataset[desplazamiento.i] = response.data;
                    $rootScope.tablaModDesplazamientos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarAscensoCtrl', ['$rootScope', '$scope', '$element', 'title', 'ascenso', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'trabajadorInfo', 'orgas', 'deps', 'plans', 'cats', 'tipDocs', function ($rootScope, $scope, $element, title, ascenso, fichaEscalafonariaId, close, crud, modal, trabajadorInfo, orgas, deps, plans, cats, tipDocs) {
        $scope.title = title;
        ag
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.ascensoSel = {
            i: ascenso.i,
            ascId: ascenso.ascId,
            numDoc: ascenso.numDoc,
            fecDoc: new Date(ascenso.fecDoc),
            tipDoc: ascenso.tipDoc,
            fecEfe: new Date(ascenso.fecEfe),
            esc: ascenso.esc,
            tip: 1,
            catId: ascenso.catId,
            plaId: ascenso.plaId,
            orgId: ascenso.orgId,
            depId: ascenso.depId
        };
        //console.log("sol: "+JSON.stringify($scope.ascensoSel));

        $scope.planillas = plans;

        $scope.categorias = cats;

        $scope.organismos = orgas;

        $scope.dependenciasSel = deps;

        $scope.rols = [];
        listarOrganizaciones();
        function listarOrganizaciones() {
            //preparamos un objeto request
            $scope.rols = [];
            var request = crud.crearRequest('organizacion', 1, 'listarOrganizacionesConRoles');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            //console.log("currOrg: "+JSON.stringify($scope.nuevoAscenso));
            crud.listar("/configuracionInicial", request, function (data) {
                //$scope.organizaciones.push({nombre:""});
                //once to get the org id
                data.data.forEach(function (item) {
                    item.roles.every(function (elm) {
                        if (elm.rolID == $scope.ascensoSel.esc) {
                            $scope.ascensoSel.currOrgId = item.organizacionID;
                            return;
                        }
                    });
                });
                data.data.forEach(function (item) {
                    if (item.organizacionID == $scope.ascensoSel.currOrgId) {
                        item.roles.forEach(function (elm) {
                            var r = {};
                            r.orgId = item.organizacionID;
                            r.rolID = elm.rolID;
                            r.rolNom = elm.nombre
                            $scope.rols.push(r);
                        });
                    }
                });
                //console.log("change rols: "+JSON.stringify($scope.rols));
            }, function (data) {
                console.info(data);
            });
        }
        ;
        var counter = 0;
        $scope.escalas = [];
        trabajadorInfo.sessiones.forEach(function (item) {
            $scope.escalas[counter++] = {id: item.rolID, title: item.rol};
        });

        $scope.actualizarAscenso = function () {
            var request = crud.crearRequest('ascenso', 1, 'actualizarAscenso');
            request.setData($scope.ascensoSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = ascenso.i;
                    $rootScope.settingModAscensos.dataset[ascenso.i] = response.data;
                    $rootScope.tablaModAscensos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarCapacitacionCtrl', ['$rootScope', '$scope', '$element', 'title', 'capacitacion', 'fichaEscalafonariaId', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, capacitacion, fichaEscalafonariaId, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.capacitacionSel = {
            i: capacitacion.i,
            capId: capacitacion.capId,
            nom: capacitacion.nom,
            tip: capacitacion.tip,
            fec: new Date(capacitacion.fec),
            cal: capacitacion.cal,
            lug: capacitacion.lug
        };

        $scope.actualizarCapacitacion = function () {
            var request = crud.crearRequest('capacitacion', 1, 'actualizarCapacitacion');
            request.setData($scope.capacitacionSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = capacitacion.i;
                    $rootScope.settingModCapacitaciones.dataset[capacitacion.i] = response.data;
                    $rootScope.tablaModCapacitaciones.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarLicenciaCtrl', ['$rootScope', '$scope', '$element', 'title', 'licencia', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'trabajadorInfo', 'tipDocs', function ($rootScope, $scope, $element, title, licencia, fichaEscalafonariaId, close, crud, modal, trabajadorInfo, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        $scope.estPros = ["Procedente", "Improcedente", "Pendiente"];
        $scope.tiposLicencias = [{id: "1", title: "Enfermedad"}, {id: "2", title: "Maternidad"}, {id: "3", title: "Capacitación"}, {id: "4", title: "Otros"}];
        //variable que servira para registrar una nueva formacion academica
        $scope.rolesTrabSel = [];
        var counter = 0;
        trabajadorInfo.sessiones.forEach(function (item) {
            $scope.rolesTrabSel[counter++] = item.rol;
        });
        $scope.licenciaSel = {
            i: licencia.i,
            licId: licencia.licId,
            entEmi: licencia.entEmi,
            tipDoc: licencia.tipDoc,
            numDoc: licencia.numDoc,
            fecDoc: licencia.fecDoc != "" ?
                    new Date(licencia.fecDoc.substring(0, 10).split("-")[0],
                            licencia.fecDoc.substring(0, 10).split("-")[1] - 1,
                            licencia.fecDoc.substring(0, 10).split("-")[2]) : "",
            car: licencia.car,
            estGoc: licencia.estGoc,
            fecIni: licencia.fecIni != "" ?
                    new Date(licencia.fecIni.substring(0, 10).split("-")[0],
                            licencia.fecIni.substring(0, 10).split("-")[1] - 1,
                            licencia.fecIni.substring(0, 10).split("-")[2]) : "",
            fecTer: licencia.fecTer != "" ?
                    new Date(licencia.fecTer.substring(0, 10).split("-")[0],
                            licencia.fecTer.substring(0, 10).split("-")[1] - 1,
                            licencia.fecTer.substring(0, 10).split("-")[2]) : "",
            mot: licencia.mot,
            numInf: licencia.numInf,
            fecInf: licencia.fecInf != "" ?
                    new Date(licencia.fecInf.substring(0, 10).split("-")[0],
                            licencia.fecInf.substring(0, 10).split("-")[1] - 1,
                            licencia.fecInf.substring(0, 10).split("-")[2]) : "",
            fecRecInf: licencia.fecRecInf != "" ?
                    new Date(licencia.fecRecInf.substring(0, 10).split("-")[0],
                            licencia.fecRecInf.substring(0, 10).split("-")[1] - 1,
                            licencia.fecRecInf.substring(0, 10).split("-")[2]) : "",
            tip: licencia.tip,
            estPro: licencia.estPro

        };
        changeGoceHaberState($scope.licenciaSel.estGoc);
        function changeGoceHaberState(estGoc) {
            if (estGoc !== "               ")
                $scope.goce_haber = "SI";
            else
                $scope.goce_haber = "NO";
        }
        ;

        $scope.actualizarLicencia = function () {
            var request = crud.crearRequest('licencia', 1, 'actualizarLicencia');
            request.setData($scope.licenciaSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = licencia.i;
                    response.data.estGocDes = response.data.estGoc ? "Con Goce" : "Sin Goce";
                    fecTe = new Date(response.data.fecTer.replace("COT ", ""));
                    fecIn = new Date(response.data.fecIni.replace("COT ", ""));
                    response.data.dias = (fecTe - fecIn) / (1000 * 60 * 60 * 24) + 1;
                    response.data.tipDes = $scope.tiposLicencias[parseInt(response.data.tip) - 1].title;
                    $rootScope.settingModLicencias.dataset[licencia.i] = response.data;
                    $rootScope.tablaModLicencias.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarVacacionCtrl', ['$rootScope', '$scope', '$element', 'title', 'vacacion', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'trabajadorInfo', 'tipDocs', function ($rootScope, $scope, $element, title, vacacion, fichaEscalafonariaId, close, crud, modal, trabajadorInfo, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        $scope.estPros = ["Procedente", "Improcedente", "Pendiente"];
        //variable que servira para registrar una nueva formacion academica
        $scope.rolesTrabSel = [];
        var counter = 0;
        trabajadorInfo.sessiones.forEach(function (item) {
            $scope.rolesTrabSel[counter++] = item.rol;
        });
        $scope.vacacionSel = {
            i: vacacion.i,
            vacId: vacacion.vacId,
            entEmi: vacacion.entEmi,
            tipDoc: vacacion.tipDoc,
            numDoc: vacacion.numDoc,
            fecDoc: vacacion.fecDoc != "" ?
                    new Date(vacacion.fecDoc.substring(0, 10).split("-")[0],
                            vacacion.fecDoc.substring(0, 10).split("-")[1] - 1,
                            vacacion.fecDoc.substring(0, 10).split("-")[2]) : "",
            car: vacacion.car,
            estGoc: vacacion.estGoc,
            fecIni: vacacion.fecIni != "" ?
                    new Date(vacacion.fecIni.substring(0, 10).split("-")[0],
                            vacacion.fecIni.substring(0, 10).split("-")[1] - 1,
                            vacacion.fecIni.substring(0, 10).split("-")[2]) : "",
            fecTer: vacacion.fecTer != "" ?
                    new Date(vacacion.fecTer.substring(0, 10).split("-")[0],
                            vacacion.fecTer.substring(0, 10).split("-")[1] - 1,
                            vacacion.fecTer.substring(0, 10).split("-")[2]) : "",
            mot: vacacion.mot,
            numInf: vacacion.numInf,
            fecInf: vacacion.fecInf != "" ?
                    new Date(vacacion.fecInf.substring(0, 10).split("-")[0],
                            vacacion.fecInf.substring(0, 10).split("-")[1] - 1,
                            vacacion.fecInf.substring(0, 10).split("-")[2]) : "",
            fecRecInf: vacacion.fecRecInf != "" ?
                    new Date(vacacion.fecRecInf.substring(0, 10).split("-")[0],
                            vacacion.fecRecInf.substring(0, 10).split("-")[1] - 1,
                            vacacion.fecRecInf.substring(0, 10).split("-")[2]) : "",
            estPro: vacacion.estPro
        };
        $scope.changeGoceHaberSi = function () {
            $scope.vacacionSel.estGoc = true;
        };
        $scope.changeGoceHaberNo = function () {
            $scope.vacacionSel.estGoc = false;
        };
        $scope.actualizarVacacion = function () {
            var request = crud.crearRequest('vacacion', 1, 'actualizarVacacion');
            request.setData($scope.vacacionSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = vacacion.i;
                    response.data.estGocDes = response.data.estGoc ? "Con Goce" : "Sin Goce";
                    fecTe = new Date(response.data.fecTer.replace("COT ", ""));
                    fecIn = new Date(response.data.fecIni.replace("COT ", ""));
                    response.data.dias = (fecTe - fecIn) / (1000 * 60 * 60 * 24) + 1;
                    $rootScope.settingModVacaciones.dataset[vacacion.i] = response.data;
                    $rootScope.tablaModVacaciones.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarReconocimientoCtrl', ['$rootScope', '$scope', '$element', 'title', 'reconocimiento', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'tipDocs', function ($rootScope, $scope, $element, title, reconocimiento, fichaEscalafonariaId, close, crud, modal, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.reconocimientoSel = {
            i: reconocimiento.i,
            recId: reconocimiento.recId,
            mot: reconocimiento.mot,
            numDoc: reconocimiento.numDoc,
            fecDoc: new Date(reconocimiento.fecDoc),
            tipDoc: reconocimiento.tipDoc,
            entEmi: reconocimiento.entEmi
        };

        $scope.motivos = [
            {id: "1", title: "Mérito"},
            {id: "2", title: "Felicitación"},
            {id: "3", title: "25 años de servicio"},
            {id: "4", title: "30 años de servicio"},
            {id: "5", title: "Luto y sepelio"},
            {id: "6", title: "Otros"}
        ];

        $scope.actualizarReconocimiento = function () {
            var request = crud.crearRequest('reconocimientos', 1, 'actualizarReconocimiento');
            request.setData($scope.reconocimientoSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.motDes = buscarContenido($scope.motivos, 'id', 'title', response.data.mot);
                    response.data.i = reconocimiento.i;
                    $rootScope.settingModReconocimientos.dataset[reconocimiento.i] = response.data;
                    $rootScope.tablaModReconocimientos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('editarDemeritoCtrl', ['$rootScope', '$scope', '$element', 'title', 'demerito', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'tipDocs', function ($rootScope, $scope, $element, title, demerito, fichaEscalafonariaId, close, crud, modal, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.demeritoSel = {
            i: demerito.i,
            demId: demerito.demId,
            entEmi: demerito.entEmi,
            numDoc: demerito.numDoc,
            fecDoc: new Date(demerito.fecDoc),
            tipDoc: demerito.tipDoc,
            sep: demerito.sep,
            fecIni: new Date(demerito.fecIni),
            fecFin: new Date(demerito.fecFin),
            mot: demerito.mot
        };


        $scope.actualizarDemerito = function () {
            var request = crud.crearRequest('demeritos', 1, 'actualizarDemerito');
            request.setData($scope.demeritoSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.sepDes = response.data.sep ? "Si" : "No";
                    response.data.i = demerito.i;
                    $rootScope.settingModDemeritos.dataset[demerito.i] = response.data;
                    $rootScope.tablaModDemeritos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('verLegajoPersonalCtrl', ['$routeParams', '$rootScope', '$scope', 'crud', 'NgTableParams', 'ModalService', 'modal', function ($routeParams, $rootScope, $scope, crud, NgTableParams, ModalService, modal) {
        var datos = JSON.parse(atob($routeParams.ficha));

        var categorias = [
            {idCat: "1", idSubCat: "1", subCatDes: "Datos personales"},
            {idCat: "1", idSubCat: "2", subCatDes: "Nacimiento"},
            {idCat: "1", idSubCat: "3", subCatDes: "Salud"},
            {idCat: "1", idSubCat: "4", subCatDes: "Direccion"},
            {idCat: "2", idSubCat: "1", subCatDes: "Datos familiares"},
            {idCat: "3", idSubCat: "1", subCatDes: "Formacion educativa"},
            {idCat: "3", idSubCat: "2", subCatDes: "Colegiatura"},
            {idCat: "3", idSubCat: "3", subCatDes: "Estudios complementarios"},
            {idCat: "3", idSubCat: "4", subCatDes: "Estudios postgrado"},
            {idCat: "3", idSubCat: "5", subCatDes: "Exposicion y/o Ponencias"},
            {idCat: "3", idSubCat: "6", subCatDes: "Publicaciones"},
            {idCat: "4", idSubCat: "1", subCatDes: "Desplazamientos"},
            {idCat: "4", idSubCat: "2", subCatDes: "Ascensos"},
            {idCat: "4", idSubCat: "3", subCatDes: "Capacitaciones"},
            {idCat: "5", idSubCat: "1", subCatDes: "Reconocimientos"},
            {idCat: "6", idSubCat: "1", subCatDes: "Demeritos"},
            {idCat: "7", idSubCat: "1", subCatDes: "Otros"}
        ];

        $rootScope.paramsLegDatosPersonales = {count: 10};
        $rootScope.settingLegDatosPersonales = {counts: []};
        $rootScope.tablaLegDatosPersonales = new NgTableParams($rootScope.paramsLegDatosPersonales, $rootScope.settingLegDatosPersonales);

        $rootScope.paramsLegDatosFamiliares = {count: 10};
        $rootScope.settingLegDatosFamiliares = {counts: []};
        $rootScope.tablaLegDatosFamiliares = new NgTableParams($rootScope.paramsLegDatosFamiliares, $rootScope.settingLegDatosFamiliares);

        $rootScope.paramsLegDatosAcademicos = {count: 10};
        $rootScope.settingLegDatosAcademicos = {counts: []};
        $rootScope.tablaLegDatosAcademicos = new NgTableParams($rootScope.paramsLegDatosAcademicos, $rootScope.settingLegDatosAcademicos);

        $rootScope.paramsLegExperienciaLaboral = {count: 10};
        $rootScope.settingLegExperienciaLaboral = {counts: []};
        $rootScope.tablaLegExperienciaLaboral = new NgTableParams($rootScope.paramsLegExperienciaLaboral, $rootScope.settingLegExperienciaLaboral);

        $rootScope.paramsLegReconocimientos = {count: 10};
        $rootScope.settingLegReconocimientos = {counts: []};
        $rootScope.tablaLegReconocimientos = new NgTableParams($rootScope.paramsLegReconocimientos, $rootScope.settingLegReconocimientos);

        $rootScope.paramsLegDemeritos = {count: 10};
        $rootScope.settingLegDemeritos = {counts: []};
        $rootScope.tablaLegDemeritos = new NgTableParams($rootScope.paramsLegDemeritos, $rootScope.settingLegDemeritos);

        $rootScope.paramsLegOtros = {count: 10};
        $rootScope.settingLegOtros = {counts: []};
        $rootScope.tablaLegOtros = new NgTableParams($rootScope.paramsLegOtros, $rootScope.settingLegOtros);

        var legDatosPersonales = [];
        var legDatosFamiliares = [];
        var legDatosAcademicos = [];
        var legExperienciaLaboral = [];
        var legReconocimientos = [];
        var legDemeritos = [];
        var legOtros = [];

        listarLegajos();
        function listarLegajos() {
            //preparamos un objeto request
            var request = crud.crearRequest('legajo_personal', 1, 'listarLegajos');
            request.setData(datos);

            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                data.data.forEach(function (item) {
                    var subCategorias = buscarObjetos(categorias, 'idCat', item.catLeg);
                    item.subCatDes = buscarContenido(subCategorias, 'idSubCat', 'subCatDes', item.subCat);

                    switch (item.catLeg) {
                        case '1':
                            legDatosPersonales.push(item);
                            break;
                        case '2':
                            legDatosFamiliares.push(item);
                            break;
                        case '3':
                            legDatosAcademicos.push(item);
                            break;
                        case '4':
                            legExperienciaLaboral.push(item);
                            break;
                        case '5':
                            legReconocimientos.push(item);
                            break;
                        case '6':
                            legDemeritos.push(item);
                            break;
                        default:
                            legOtros.push(item);
                    }
                });

                $rootScope.settingLegDatosPersonales.dataset = legDatosPersonales;
                iniciarPosiciones($rootScope.settingLegDatosPersonales.dataset);
                $rootScope.tablaLegDatosPersonales.settings($rootScope.settingLegDatosPersonales);

                $rootScope.settingLegDatosFamiliares.dataset = legDatosFamiliares;
                iniciarPosiciones($rootScope.settingLegDatosFamiliares.dataset);
                $rootScope.tablaLegDatosFamiliares.settings($rootScope.settingLegDatosFamiliares);

                $rootScope.settingLegDatosAcademicos.dataset = legDatosAcademicos;
                iniciarPosiciones($scope.settingLegDatosAcademicos.dataset);
                $rootScope.tablaLegDatosAcademicos.settings($rootScope.settingLegDatosAcademicos);

                $scope.settingLegExperienciaLaboral.dataset = legExperienciaLaboral;
                iniciarPosiciones($rootScope.settingLegExperienciaLaboral.dataset);
                $rootScope.tablaLegExperienciaLaboral.settings($rootScope.settingLegExperienciaLaboral);

                $rootScope.settingLegReconocimientos.dataset = legReconocimientos;
                iniciarPosiciones($rootScope.settingLegReconocimientos.dataset);
                $rootScope.tablaLegReconocimientos.settings($rootScope.settingLegReconocimientos);

                $rootScope.settingLegDemeritos.dataset = legDemeritos;
                iniciarPosiciones($rootScope.settingLegDatosPersonales.dataset);
                $rootScope.tablaLegDemeritos.settings($rootScope.settingLegDemeritos);

                $rootScope.settingLegOtros.dataset = legOtros;
                iniciarPosiciones($rootScope.settingLegOtros.dataset);
                $rootScope.tablaLegOtros.settings($rootScope.settingLegOtros);


            }, function (data) {
                console.info(data);
            });

        }
        ;

        $scope.showNuevoLegajo = function () {
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/agregarLegajoPersonal.html",
                controller: "agregarNuevoLegajoCtrl",
                inputs: {
                    title: "Nuevo Legajo",
                    fichaEscalafonariaId: datos.ficEscId
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result.flag) {
                    }
                });
            });
        };

        $scope.prepararEditarLegajo = function (lp) {
            var legajoSel = JSON.parse(JSON.stringify(lp));
            ModalService.showModal({
                templateUrl: "administrativa/sistema_escalafon/editarLegajo.html",
                controller: "editarLegajoCtrl",
                inputs: {
                    title: "Editar datos del legajo",
                    legajo: legajoSel
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.eliminarLegajo = function (l) {
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el legajo?", function () {
                var request = crud.crearRequest('legajo_personal', 1, 'eliminarLegajo');
                request.setData({legId: l.legId});

                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaLegOtros.settings().dataset, function (item) {
                            return l === item;
                        });
                        $rootScope.tablaLegOtros.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    } else {
                        modal.mensaje("ERROR", response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR", "El servidor no responde");
                });
            }, '400');
        };

    }]);
seApp.controller('agregarNuevoLegajoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente
        $scope.nuevoLegajo = {
            ficEscId: fichaEscalafonariaId,
            nomDoc: "",
            archivo: "",
            des: "",
            url: "",
            catLeg: "7",
            subCat: "1",
            codAspOri: "0"
        };
        $scope.title = title;


        $scope.agregarLegajo = function () {
            var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
            request.setData($scope.nuevoLegajo);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingLegOtros.dataset, response.data);
                    $rootScope.tablaLegOtros.reload();

                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
seApp.controller('editarLegajoCtrl', ['$rootScope', '$scope', '$element', 'title', 'legajo', 'close', 'crud', 'modal', function ($rootScope, $scope, $element, title, legajo, close, crud, modal) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.legajoSel = {
            i: legajo.i,
            ficEscId: legajo.ficEscId,
            legId: legajo.legId,
            nomDoc: legajo.nomDoc,
            archivo: legajo.archivo,
            des: legajo.des,
            url: legajo.url,
            catLeg: legajo.catleg,
            subCat: legajo.subCat,
            codAspOri: legajo.codAspOri
        };


        $scope.actualizarLegajo = function () {
            var request = crud.crearRequest('legajo_personal', 1, 'actualizarLegajo');
            request.setData($scope.legajoSel);
            crud.actualizar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.i = legajo.i;
                    $rootScope.settingLegOtros.dataset[legajo.i] = response.data;
                    $rootScope.tablaLegOtros.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };


    }]);
seApp.controller('modalSeleccionadosCtrl', ['$scope', '$rootScope', '$element', 'title', 'ficEscId', 'traId', 'perDni', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, ficEscId, traId, perDni, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente

        $scope.title = title;
        $scope.seleccionados = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
        $scope.formatos = ["PDF", "EXCEL"];
        $scope.selecFormat = $scope.formatos[0];//Establecer la selección
        var checked = false;
        $scope.marcarTodos = function () {
            if (checked === false) {
                $('.dinamico').prop('checked', true);
                $scope.seleccionados = [true, true, true, true, true, true, true, true, true, true, true, true, true, true, true];
                checked = true;
            } else {
                $('.dinamico').prop('checked', false);
                $scope.seleccionados = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
                console.log($scope.seleccionados);
                checked = false;
            }
        };
        $scope.verFichaEscalafonaria = function () {
            var request = crud.crearRequest('reportes', 1, 'reporteFichaEscalafonaria');
            request.setData({traId: traId, ficEscId: ficEscId, perDni: perDni, seleccionados: $scope.seleccionados});
            crud.insertar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    verDocumentoPestaña(response.data.file);
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (errResponse) {
                modal.mensaje('ERROR', 'El servidor no responde');
            });
        };
        $scope.descargarFichaEscalafonaria = function () {
            var request = crud.crearRequest('reportes', 1, 'reporteFichaEscalafonaria');
            request.setData({traId: traId, ficEscId: ficEscId, perDni: perDni, seleccionados: $scope.seleccionados});
            crud.insertar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    if ($scope.selecFormat.toString() === "PDF") {
                        downloadPDF(response.data.file);
                    }
                    if ($scope.selecFormat.toString() === "EXCEL")
                    {
                        var link = document.createElement("a");
                        link.href = response.data.reporteXls;//Aun no funciona
                        //set the visibility hidden so it will not effect on your web-layout
                        link.style = "visibility:hidden";
                        link.download = "fichaEscalafonaria.xls";
                        //this part will append the anchor tag and remove it after automatic click
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    }
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (errResponse) {
                modal.mensaje('ERROR', 'El servidor no responde');
            });
        };
    }]);
seApp.controller('modalSeleccionadosPersonalizadoCtrl', ['$scope', '$rootScope', '$element', 'title', 'trabajadorInfo', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, trabajadorInfo, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente
        //console.log(trabajadorInfo);
        //$scope.tipoInforme = 1;
        $scope.title = title;
        $scope.seleccionados = [false, false, false, false, false, false, false, false];
        $scope.formatos = ["PDF", "WORD"];
        $scope.selecFormat = $scope.formatos[0];//Establecer la selección
        var checked = false;
        $scope.marcarTodos = function () {
            if (checked === false) {
                $('.dinamico').prop('checked', true);
                $scope.seleccionados=[true, true, true, true, true, true, true, true];
                checked = true;
            } else {
                $('.dinamico').prop('checked', false);
                $scope.seleccionados = [false, false, false, false, false, false, false, false];
                console.log($scope.seleccionados);
                checked = false;
            }
        };

        $scope.reportePersonalizado = {
            numInf: "",
            lugDes: "",
            lugPro: "SUBDIRECCIÓN DE RECURSOS HUMANOS - OFICINA DE ESCALAFON",
            asu: ""
        };
        $scope.genererarReportePersonalizado = function () {
            //console.log($scope.tipoInforme);
            var request = crud.crearRequest('reportes', 1, 'reportePersonalizado');
            request.setData({
                traId: trabajadorInfo.traId,
                perDni: trabajadorInfo.perDni,
                tipoInforme: $scope.tipoInforme,
                seleccionados: $scope.seleccionados,
                //enviara al backend
                reporteData: $scope.reportePersonalizado
            });
            crud.insertar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    console.log($scope.seleccionados);
                    verDocumentoPestaña(response.data.file);
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (errResponse) {
                modal.mensaje('ERROR', 'El servidor no responde');
            });
        };
        $scope.descargarReportePersonalizado = function () {
            //console.log($scope.tipoInforme);
            console.log($scope.selecFormat);
            var request = crud.crearRequest('reportes', 1, 'reportePersonalizado');
            request.setData({
                traId: trabajadorInfo.traId,
                perDni: trabajadorInfo.perDni,
                tipoInforme: $scope.tipoInforme,
                seleccionados: $scope.seleccionados,
                //enviara al backend
                reporteData: $scope.reportePersonalizado
            });
            crud.insertar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    console.log($scope.seleccionados);
                    if ($scope.selecFormat.toString() === "PDF") {
                        downloadPDF(response.data.file);
                    }
                    if ($scope.selecFormat.toString() === "WORD")
                    {
                        var link2 = document.createElement("a");
                        link2.href = response.data.reporteDoc;
                        //set the visibility hidden so it will not effect on your web-layout
                        link2.style = "visibility:hidden";
                        link2.download = "reportePersonalizado.doc";
                        //this part will append the anchor tag and remove it after automatic click
                        document.body.appendChild(link2);
                        link2.click();
                        document.body.removeChild(link2);
                    }
                    /*
                     var link = document.createElement("a");
                     link.href = response.data.reporteXls;
                     //set the visibility hidden so it will not effect on your web-layout
                     link.style = "visibility:hidden";
                     link.download = "reportePersonalizado.xls";
                     //this part will append the anchor tag and remove it after automatic click
                     document.body.appendChild(link);
                     link.click();
                     document.body.removeChild(link);*/
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (errResponse) {
                modal.mensaje('ERROR', 'El servidor no responde');
            });
        };
    }]);
seApp.controller('modalModelosReporteCtrl', ['$scope', '$rootScope', '$element', 'title', 'ficEscId', 'traId', 'perDni', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, ficEscId, traId, perDni, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente

        $scope.title = title;
        $scope.selected = '1';


        $scope.verModeloReporte = function () {
            var request = crud.crearRequest('reportes', 1, 'modelos12ReporteFichaEscalafonaria');
            request.setData({traId: traId, ficEscId: ficEscId, perDni: perDni, selected: $scope.selected});
            crud.insertar('/sistema_escalafon', request, function (response) {
                if (response.responseSta) {
                    verDocumentoPestaña(response.data.file);
                } else {
                    modal.mensaje('ERROR', response.responseMsg);
                }
            }, function (errResponse) {
                modal.mensaje('ERROR', 'El servidor no responde');
            });
        };
    }]);
seApp.controller('agregarNuevoParienteCtrl', ['$scope', '$rootScope', '$element', 'title', 'personaId', 'sexo', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, personaId, sexo, fichaEscalafonariaId, close, crud, modal, ModalService) {
        //variable que servira para registrar un nuevo pariente
        $scope.nuevoPariente = {perId: personaId, existe: true, tipoPariente: "", parApePat: "", parApeMat: "", parNom: "", parDni: "", parFecNac: "", parSex: "", parNum1: "", parNum2: "", parFij: "", parEmail: "", retJud: ""};
        $scope.nuevoLegajoPar = {ficEscId: fichaEscalafonariaId, nom: "", archivo: "", des: "DNI-Pariente", url: "", catLeg: "2", subCat: "1", codAspOri: ""};
        $scope.title = title;
        $scope.sexoPariente = sexo;
        $scope.tipoParentesco = [];

        listarTipoParentesco();
        function listarTipoParentesco() {
            //preparamos un objeto request
            var request = crud.crearRequest('familiares', 1, 'listarTipoParentesco');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las funciones de exito y error
            crud.listar("/sistema_escalafon", request, function (data) {
                $scope.tipoParentesco = data.data;
            }, function (data) {
                console.info(data);
            });
        }
        ;

        $scope.agregarPariente = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('usuarioSistema', 1, 'buscarPersona');
            request.setData({dni: $scope.nuevoPariente.parDni});
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/configuracionInicial", request, function (data) {
                if (data.responseSta) {
                    $scope.nuevoPariente.dataPer = data.data.persona;
                } else {
                    $scope.nuevoPariente.existe = false;
                }
                if (!$scope.nuevoPariente.existe) {
                    var request = crud.crearRequest('familiares', 1, 'agregarPariente');
                    $scope.nuevoPariente.retJud = $scope.nuevoPariente.retJud.toString();
                    request.setData($scope.nuevoPariente);
                    crud.insertar("/sistema_escalafon", request, function (response) {
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                        if (response.responseSta) {
                            response.data.parentesco = buscarContenido($scope.tipoParentesco, 'tpaId', 'tpaDes', response.data.tipoParienteId);
                            insertarElemento($rootScope.settingModParientes.dataset, response.data);
                            $rootScope.tablaModParientes.reload();

                            if ($scope.nuevoLegajoPar.archivo) {
                                $scope.nuevoLegajoPar.codAspOri = response.data.parId;
                                var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                                request.setData($scope.nuevoLegajoPar);
                                crud.insertar("/sistema_escalafon", request, function (response) {
                                    $scope.listarParientes();
                                    modal.mensaje("CONFIRMACION", response.responseMsg);
                                }, function (data) {
                                    console.info(data);
                                });
                            }

                        }
                    }, function (data) {
                        console.info(data);
                    });
                } else {
                    alert("El DNI: " + $scope.nuevoPariente.parDni + " ya existe en el sistema.");
                }
            }, function (data) {
                console.info(data);
            });
        };
    }]);
seApp.controller('agregarNuevaFormacionEducativaCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaForEdu = {ficEscId: fichaEscalafonariaId, tip: "", niv: "", numTit: "", esp: "", estCon: false, fecExp: "", cenEst: ""};
        $scope.nuevoLegajoForEdu = {ficEscId: fichaEscalafonariaId, nom: "", archivo: "", des: " Titulo y/o Certificado", url: "", catLeg: "3", subCat: "1", codAspOri: ""};
        $scope.tiposFormacion = [{id: "1", title: "Estudios Basicos Regulares"}, {id: "2", title: "Estudios Tecnicos"}, {id: "3", title: "Bachiller"}, {id: "4", title: "Universitario"}, {id: "5", title: "Licenciatura"}, {id: "6", title: "Maestria"}, {id: "7", title: "Doctorado"}, {id: "8", title: "Otros"}];

        $scope.agregarFormacionEducativa = function () {
            var request = crud.crearRequest('formacion_educativa', 1, 'agregarFormacionEducativa');
            request.setData($scope.nuevaForEdu);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.estCon = $rootScope.parsearBooleano(response.data.estCon);
                    insertarElemento($rootScope.settingModForEdu.dataset, response.data);
                    $rootScope.tablaModForEdu.reload();

                    if ($scope.nuevoLegajoForEdu.archivo) {
                        $scope.nuevoLegajoForEdu.codAspOri = response.data.forEduId;
                        var request = crud.crearRequest('sistema_escalafon', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoForEdu);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            $scope.listarFormacionesEducativas();
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };


    }]);
seApp.controller('agregarNuevaColegiaturaCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaColegiatura = {ficEscId: fichaEscalafonariaId, nomColPro: "", numRegCol: "", conReg: false};
        $scope.nuevoLegajoCol = {ficEscId: fichaEscalafonariaId, nom: "", archivo: "", url: "", des: "Constancia", catLeg: "3", subCat: "2", codAspOri: ""};

        $scope.agregarColegiatura = function () {
            var request = crud.crearRequest('colegiatura', 1, 'agregarColegiatura');
            request.setData($scope.nuevaColegiatura);
            crud.insertar("/sistema_escalafon", request, function (response) {
                if (response.responseSta) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);

                    response.data.conReg = $rootScope.parsearBooleano2(response.data.conReg)
                    insertarElemento($rootScope.settingModColegiaturas.dataset, response.data);
                    $rootScope.tablaModColegiaturas.reload();

                    if ($scope.nuevoLegajoCol.archivo) {
                        $scope.nuevoLegajoCol.codAspOri = response.data.colId;

                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoCol);
                        crud.insertar("/sistema_escalafon", request, function (response) {

                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }

                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevoEstudioComplementarioCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoEstCom = {ficEscId: fichaEscalafonariaId, tip: "", des: "", niv: "", insCer: "", tipPar: "", fecIni: "", fecTer: "", horLec: "", pais: ""};
        $scope.nuevoLegajoEC = {ficEscId: fichaEscalafonariaId, nom: "", archivo: "", des: "Certificado y/o Titulo", url: "", catLeg: "3", subCat: "3", codAspOri: ""};
        $scope.tiposEstCom = [{id: "1", title: "Informatica"}, {id: "2", title: "Idiomas"}, {id: "3", title: "Certificación"}, {id: "4", title: "Diplomado"}, {id: "5", title: "Especialización"}, {id: "6", title: "Otros"}];
        $scope.niveles = [{id: "N", title: "Ninguno"}, {id: "B", title: "Básico"}, {id: "I", title: "Intermedio"}, {id: "A", title: "Avanzado"}];

        $scope.agregarEstudioComplementario = function () {
            var request = crud.crearRequest('estudio_complementario', 1, 'agregarEstudioComplementario');
            request.setData($scope.nuevoEstCom);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingModEstCom.dataset, response.data);
                    $rootScope.tablaModEstCom.reload();

                    if ($scope.nuevoLegajoEC.archivo) {
                        $scope.nuevoLegajoEC.codAspOri = response.data.estComId;
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoEC);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            $scope.listarEstudiosComplementarios();
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevoEstudioPostgradoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', 'tipDocs', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoEstPos = {ficEscId: fichaEscalafonariaId, tip: "", numDoc: "", fecDoc: "", tipDoc: "", des: "", niv: "", insCer: "", tipPar: "", fecIniEst: "", fecTerEst: "", horLec: "", pais: ""};
        $scope.nuevoLegajoEP = {ficEscId: fichaEscalafonariaId, nom: "", archivo: "", des: "Titulo", url: "", catLeg: "3", subCat: "4", codAspOri: ""};
        $scope.tiposEstPos = [{id: "1", title: "Maestria"}, {id: "2", title: "Doctorado"}];

        $scope.agregarEstudioPostgrado = function () {
            var request = crud.crearRequest('estudio_postgrado', 1, 'agregarEstudioPostgrado');
            request.setData($scope.nuevoEstPos);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    var auxTip = buscarObjeto($scope.tiposEstPos, "id", response.data.tip);
                    response.data.tip = auxTip.title;
                    insertarElemento($rootScope.settingModEstPos.dataset, response.data);
                    $rootScope.tablaModEstPos.reload();

                    if ($scope.nuevoLegajoEP.archivo) {
                        $scope.nuevoLegajoEP.codAspOri = response.data.estPosId;
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoEP);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            $scope.listarEstudiosPostgrado();
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevaExposicionCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaExpPon = {ficEscId: fichaEscalafonariaId, des: "", insOrg: "", tipPar: "", fecIni: "", fecTer: "", horLec: ""};
        $scope.nuevoLegajoExp = {ficEscId: fichaEscalafonariaId, nom: "", archivo: "", des: "Certificado de participación", url: "", catLeg: "3", subCat: "5", codAspOri: ""};

        $scope.agregarExposicion = function () {
            var request = crud.crearRequest('exposicion_ponencia', 1, 'agregarExposicion');
            request.setData($scope.nuevaExpPon);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingModExpPon.dataset, response.data);
                    $rootScope.tablaModExpPon.reload();

                    if ($scope.nuevoLegajoExp.archivo) {
                        $scope.nuevoLegajoExp.codAspOri = response.data.expId;
                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoExp);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            $scope.listarExposiciones();
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevaPublicacionCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaPublicacion = {ficEscId: fichaEscalafonariaId, nomEdi: "", tipPub: "", titPub: "", graPar: "", lug: "", fecPub: "", numReg: ""};
        $scope.nuevoLegajoPub = {ficEscId: fichaEscalafonariaId, nom: "", archivo: "", url: "", des: "Constancia", catLeg: "3", subCat: "6", codAspOri: ""};

        $scope.agregarPublicacion = function () {
            var request = crud.crearRequest('publicacion', 1, 'agregarPublicacion');
            request.setData($scope.nuevaPublicacion);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingModPublicaciones.dataset, response.data);
                    $rootScope.tablaModPublicaciones.reload();

                    if ($scope.nuevoLegajoPub.archivo) {
                        $scope.nuevoLegajoPub.codAspOri = response.data.pubId;

                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoPub);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            $scope.listarPublicaciones();
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
// Now "agregarNuevoDesplazamientoCtrl" adds a new interruption, displacements are no longer
// "displacements" as themeselves, they are only interruptions now
seApp.controller('agregarNuevoDesplazamientoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonaria', 'close', 'crud', 'modal', 'ModalService', 'trabajadorInfo', 'tipDocs', 'jorLabs', function ($scope, $rootScope, $element, title, fichaEscalafonaria, close, crud, modal, ModalService, trabajadorInfo, tipDocs, jorLabs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        $scope.jorLabs = jorLabs;
        //variable que servira para registrar una nueva formacionlist academica
        $scope.nuevoDesplazamiento = {ficEscId: fichaEscalafonaria.ficEscId, tip: "3", numDoc: "", fecDoc: "", tipDoc: "", insEdu: "", car: "", jorLab: "", fecIni: "", fecTer: "", numDocTer: "", fecDocTer: "", motRet: ""};
        $scope.nuevoLegajoDes = {ficEscId: fichaEscalafonaria.ficEscId, nom: "", archivo: "", des: "Resolución", url: "", catLeg: "4", subCat: "1", cosAspOri: ""};
        //console.log("daots: "+JSON.stringify(trabajadorInfo));
        $scope.tiposDesplazamientos = [{id: "1", title: "Designación"}, {id: "2", title: "Rotación"}, {id: "3", title: "Reasignación"}, {id: "4", title: "Destaque"}, {id: "5", title: "Permuta"}, {id: "6", title: "Encargo"}, {id: "7", title: "Comisión de servicio"}, {id: "8", title: "Transferencia"}, {id: "9", title: "Otros"}];
        $scope.rolesTrabSel = [];
        var counter = 0;
        trabajadorInfo.sessiones.forEach(function (item) {
            $scope.rolesTrabSel[counter++] = item.rol;
        });
        //console.log("le:"+$scope.rolesTrabSel.length);

        $scope.fichaEscalafonariaSel = {
            ficEscId: fichaEscalafonaria.ficEscId,
            autEss: fichaEscalafonaria.autEss,
            sisPen: fichaEscalafonaria.sisPen,
            nomAfp: fichaEscalafonaria.nomAfp,
            codCuspp: fichaEscalafonaria.codCuspp,
            fecIngAfp: fichaEscalafonaria.fecIngAfp == "" ? "" : new Date(datosPersonales.ficha.fecIngAfp),
            perDis: fichaEscalafonaria.perDis == "" ? false : datosPersonales.ficha.perDis,
            regCon: fichaEscalafonaria.regCon
        };
        $scope.agregarDesplazamiento = function () {
            //Set up the list of displacements and get the last of them
            var request = crud.crearRequest('desplazamiento', 1, 'listarDesplazamientos');
            request.setData($scope.fichaEscalafonariaSel);
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            var lastDisplacement = "";
            crud.listar("/sistema_escalafon", request, function (response) {
                var counter = 1;
                response.data.forEach(function (item) {
                    //console.log("numrester:"+JSON.stringify(item));
                    (item.tip != "3") && (item.numDocTer === undefined || item.numDocTer === null || item.numDocTer === "") ? lastDisplacement = item : counter++;
                });
                $scope.nuevoDesplazamiento.insEdu = lastDisplacement.insEdu;
                $scope.nuevoDesplazamiento.car = lastDisplacement.car;
                $scope.nuevoDesplazamiento.jorLab = lastDisplacement.jorLab;
                //console.log("getting the last displ: "+JSON.stringify(lastDisplacement));
                //console.log("gonna insert the int displ: "+JSON.stringify($scope.nuevoDesplazamiento));
                var request = crud.crearRequest('desplazamiento', 1, 'agregarDesplazamiento');
                request.setData($scope.nuevoDesplazamiento);
                //console.log("entra des1 with fic: "+JSON.stringify($scope.nuevoDesplazamiento));
                crud.insertar("/sistema_escalafon", request, function (response) {
                    modal.mensaje("CONFIRMACION", response.responseMsg);
                }, function (data) {
                    console.info(data);
                });
                /*var request = crud.crearRequest('desplazamiento', 1, 'agregarDesplazamiento');
                 request.setData($scope.nuevoDesplazamiento);
                 crud.insertar("/sistema_escalafon", request, function (response) {
                 modal.mensaje("CONFIRMACION", response.responseMsg);
                 if (response.responseSta) {
                 insertarElemento($rootScope.settingModDesplazamientos.dataset, response.data);
                 $rootScope.tablaModDesplazamientos.reload();
                 
                 if ($scope.nuevoLegajoDes.archivo){
                 $scope.nuevoLegajoDes.cosAspOri = response.data.desId;
                 
                 var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                 request.setData($scope.nuevoLegajoDes);
                 crud.insertar("/sistema_escalafon", request, function (response) {
                 $scope.listarDesplazamientos();
                 modal.mensaje("CONFIRMACION", response.responseMsg);
                 }, function (data) {
                 console.info(data);
                 });
                 }
                 }
                 }, function (data) {
                 console.info(data);
                 });*/
            });
        };

    }]);
seApp.controller('agregarNuevoAscensoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', 'trabajadorInfo', 'orgas', 'deps', 'plans', 'cats', 'nombOasc', 'tipDocs', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService, trabajadorInfo, orgas, deps, plans, cats, nombOasc, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.nombOasc = nombOasc;
        $scope.escOnom = nombOasc ? "Nuevo rol de nombramiento" : "Nueva escala";
        $scope.nuevoAscenso = {currOrgId: "", ficEscId: fichaEscalafonariaId, numDoc: "", fecDoc: "", tipDoc: "", fecEfe: "", esc: "", tip: nombOasc ? "3" : "1", catId: "", plaId: "", orgId: "", depId: ""};
        $scope.nuevoLegajoAsc = {ficEscId: fichaEscalafonariaId, nom: "", archivo: "", des: "Resolución", url: "", catLeg: "4", subCat: "2", codAspOri: ""};

        var counter = 0;

        $scope.planillas = plans;

        $scope.categorias = cats;

        $scope.organismos = orgas;

        $scope.dependenciasSel = deps;

        $scope.rols = [];
        $scope.rolsSel = [];
        listarOrganizaciones();
        function listarOrganizaciones() {
            //preparamos un objeto request
            var request = crud.crearRequest('organizacion', 1, 'listarOrganizacionesConRoles');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
            //y las usuarios de exito y error
            crud.listar("/configuracionInicial", request, function (data) {
                //$scope.organizaciones.push({nombre:""});
                data.data.forEach(function (item) {
                    item.roles.forEach(function (elm) {
                        var r = {};
                        r.orgId = item.organizacionID;
                        r.rolID = elm.rolID;
                        r.rolNom = elm.nombre
                        $scope.rols.push(r);
                    });
                });
                //console.log("change rols: "+JSON.stringify($scope.rols));
            }, function (data) {
                console.info(data);
            });
        }
        ;

        $scope.changeOrgnz = function (val) {
            $scope.rolsSel = [];
            $scope.rols.forEach(function (elm) {
                if (elm.orgId == $scope.nuevoAscenso.currOrgId) {
                    var r = {};
                    r.orgId = elm.orgId;
                    r.rolID = elm.rolID;
                    r.rolNom = elm.rolNom;
                    $scope.rolsSel.push(r);
                }
                ;
            });
        };

        $scope.escalas = [];
        trabajadorInfo.sessiones.forEach(function (item) {
            $scope.escalas[counter++] = {id: item.rolID, title: item.rol, orgid: item.organizacionID};
        });

        $scope.agregarAscenso = function () {
            var request = crud.crearRequest('ascenso', 1, 'agregarAscenso');
            $scope.nuevoAscenso.catId = $scope.nuevoAscenso.tip !== "1" ? -1 : parseInt($scope.nuevoAscenso.catId);
            $scope.nuevoAscenso.orgId = $scope.nuevoAscenso.tip !== "1" ? -1 : parseInt($scope.nuevoAscenso.orgId);
            $scope.nuevoAscenso.plaId = $scope.nuevoAscenso.tip !== "1" ? -1 : parseInt($scope.nuevoAscenso.plaId);
            $scope.nuevoAscenso.depId = $scope.nuevoAscenso.tip !== "1" ? -1 : parseInt($scope.nuevoAscenso.depId);
            request.setData($scope.nuevoAscenso);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    insertarElemento($rootScope.settingModAscensos.dataset, response.data);
                    $rootScope.tablaModAscensos.reload();
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevaCapacitacionCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService) {
        $scope.title = title;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaCapacitacion = {ficEscId: fichaEscalafonariaId, nom: "", tip: "", fec: "", cal: 0, lug: ""};
        $scope.nuevoLegajoCap = {ficEscId: fichaEscalafonariaId, nom: "", archivo: "", des: "Certificado", url: "", catLeg: "4", subCat: "3", codAspOri: ""};


        $scope.agregarCapacitacion = function () {
            var request = crud.crearRequest('capacitacion', 1, 'agregarCapacitacion');
            request.setData($scope.nuevaCapacitacion);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {

                    insertarElemento($rootScope.settingModCapacitaciones.dataset, response.data);
                    $rootScope.tablaModCapacitaciones.reload();

                    if ($scope.nuevoLegajoCap.archivo) {
                        $scope.nuevoLegajoCap.codAspOri = response.data.capId;

                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoCap);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            $scope.listarCapacitaciones();
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevaLicenciaCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', 'trabajadorInfo', 'tipDocs', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService, trabajadorInfo, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        $scope.estPros = ["Procedente", "Improcedente", "Pendiente"];
        //variable que servira para registrar una nueva formacion academica
        $scope.tiposLicencias = [{id: "1", title: "Enfermedad"}, {id: "2", title: "Maternidad"}, {id: "3", title: "Capacitación"}, {id: "4", title: "Otros"}];
        $scope.tiposResolucion = [{id: "1", title: "Directoral"}, {id: "2", title: "Rectoral"}, {id: "3", title: "Otros"}];
        $scope.rolesTrabSel = [];
        var counter = 0;
        trabajadorInfo.sessiones.forEach(function (item) {
            $scope.rolesTrabSel[counter++] = item.rol;
        });
        $scope.nuevaLicencia = {
            ficEscId: fichaEscalafonariaId,
            entEmi: "",
            numDoc: "",
            fecDoc: "",
            tipDoc: "",
            car: "",
            estGoc: false,
            fecIni: "",
            fecTer: "",
            mot: "",
            numInf: "",
            fecInf: "",
            fecRecInf: "",
            tip: "",
            tipRes: "",
            estPro: ""
        };

        $scope.changeGoceHaberSi = function () {
            $scope.nuevaLicencia.estGoc = true;
        };
        $scope.changeGoceHaberNo = function () {
            $scope.nuevaLicencia.estGoc = false;
        };
        $scope.agregarLicencia = function () {
            var request = crud.crearRequest('licencia', 1, 'agregarLicencia');
            //console.log("newlic: "+JSON.stringify($scope.nuevaLicencia));
            request.setData($scope.nuevaLicencia);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.estGocDes = response.data.estGoc ? "Con Goce" : "Sin Goce";
                    fecTe = new Date(response.data.fecTer.replace("COT ", ""));
                    fecIn = new Date(response.data.fecIni.replace("COT ", ""));
                    response.data.dias = (fecTe - fecIn) / (1000 * 60 * 60 * 24) + 1;
                    response.data.tipDes = $scope.tiposLicencias[parseInt(response.data.tip) - 1].title;
                    insertarElemento($rootScope.settingModLicencias.dataset, response.data);
                    $rootScope.tablaModLicencias.reload();
                }
            }, function (error) {
                console.info(error);
            });
        };

    }]);
seApp.controller('agregarNuevaVacacionCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', 'trabajadorInfo', 'tipDocs', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService, trabajadorInfo, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        $scope.tiposResolucion = [{id: "1", title: "Directoral"}, {id: "2", title: "Rectoral"}, {id: "3", title: "Otros"}];
        $scope.estPros = ["Procedente", "Improcedente", "Pendiente"];
        $scope.rolesTrabSel = [];
        var counter = 0;
        trabajadorInfo.sessiones.forEach(function (item) {
            $scope.rolesTrabSel[counter++] = item.rol;
        });
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevaVacacion = {
            ficEscId: fichaEscalafonariaId,
            tipDoc: "",
            numDoc: "",
            fecDoc: "",
            car: "",
            estGoc: false,
            fecIni: "",
            fecTer: "",
            mot: "",
            numInf: "",
            fecInf: "",
            fecRecInf: "",
            tip: "",
            estPro: ""
        };

        $scope.agregarVacacion = function () {
            var request = crud.crearRequest('vacacion', 1, 'agregarVacacion');
            request.setData($scope.nuevaVacacion);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.estGocDes = response.data.estGoc ? "Con Goce" : "Sin Goce";
                    fecTe = new Date(response.data.fecTer.replace("COT ", ""));
                    fecIn = new Date(response.data.fecIni.replace("COT ", ""));
                    response.data.dias = (fecTe - fecIn) / (1000 * 60 * 60 * 24) + 1;
                    insertarElemento($rootScope.settingModVacaciones.dataset, response.data);
                    $rootScope.tablaModVacaciones.reload();
                }
            }, function (error) {
                console.info(error);
            });
        };

    }]);
seApp.controller('agregarNuevoReconocimientoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', 'tipDocs', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoReconocimiento = {ficEscId: fichaEscalafonariaId, mot: "", numDoc: "", fecDoc: "", tipDoc: "", entEmi: ""};
        $scope.nuevoLegajoRec = {ficEscId: fichaEscalafonariaId, nom: "", archivo: "", des: "Resolucion", url: "", catLeg: "5", subCat: "1", codAspOri: ""};

        $scope.motivos = [{id: "1", title: "Mérito"}, {id: "2", title: "Felicitación"}, {id: "3", title: "25 años de servicio"}, {id: "4", title: "30 años de servicio"}, {id: "5", title: "Luto y sepelio"}, {id: "6", title: "Otros"}];

        $scope.agregarReconocimiento = function () {
            var request = crud.crearRequest('reconocimientos', 1, 'agregarReconocimiento');
            request.setData($scope.nuevoReconocimiento);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    var auxMot = buscarObjeto($scope.motivos, "id", response.data.mot);
                    response.data.mot = auxMot.title;
                    insertarElemento($rootScope.settingModReconocimientos.dataset, response.data);
                    $rootScope.tablaModReconocimientos.reload();

                    if ($scope.nuevoLegajoRec.archivo) {
                        $scope.nuevoLegajoRec.codAspOri = response.data.recId;

                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoRec);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                    var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                    request.setData($scope.nuevoLegajoRec);
                    crud.insertar("/sistema_escalafon", request, function (response) {
                        $scope.listarReconocimientos();
                        modal.mensaje("CONFIRMACION", response.responseMsg);
                    }, function (data) {
                        console.info(data);
                    });
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);
seApp.controller('agregarNuevoDemeritoCtrl', ['$scope', '$rootScope', '$element', 'title', 'fichaEscalafonariaId', 'close', 'crud', 'modal', 'ModalService', 'tipDocs', function ($scope, $rootScope, $element, title, fichaEscalafonariaId, close, crud, modal, ModalService, tipDocs) {
        $scope.title = title;
        $scope.tipDocs = tipDocs;
        //variable que servira para registrar una nueva formacion academica
        $scope.nuevoDemerito = {ficEscId: fichaEscalafonariaId, entEmi: "", numDoc: "", fecDoc: "", tipDoc: "", sep: false, fecIni: "", fecFin: "", mot: ""};
        $scope.nuevoLegajoDem = {ficEscId: fichaEscalafonariaId, nom: "", archivo: "", url: "", des: "Resolución", catLeg: "6", subCat: "1", codAspOri: ""};

        $scope.agregarDemerito = function () {
            var request = crud.crearRequest('demeritos', 1, 'agregarDemerito');
            request.setData($scope.nuevoDemerito);
            crud.insertar("/sistema_escalafon", request, function (response) {
                modal.mensaje("CONFIRMACION", response.responseMsg);
                if (response.responseSta) {
                    response.data.sep = $rootScope.parsearBooleano(response.data.sep)
                    insertarElemento($rootScope.settingModDemeritos.dataset, response.data);
                    $rootScope.tablaModDemeritos.reload();

                    if ($scope.nuevoLegajoDem.archivo) {
                        $scope.nuevoLegajoDem.codAspOri = response.data.demId;

                        var request = crud.crearRequest('legajo_personal', 1, 'agregarLegajoPersonal');
                        request.setData($scope.nuevoLegajoDem);
                        crud.insertar("/sistema_escalafon", request, function (response) {
                            $scope.listarDemeritos();
                            modal.mensaje("CONFIRMACION", response.responseMsg);
                        }, function (data) {
                            console.info(data);
                        });
                    }
                }
            }, function (data) {
                console.info(data);
            });
        };

    }]);


//var params = {count: 20};
//var setting = { counts: []};
// $scope.setting.data=datos;
// $scope.legajos_categoria = new NgTableParams(params, setting);
// $scope.legajos_categoria.settings(setting);
