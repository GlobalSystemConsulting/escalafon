app.requires.push('angularModalService');
app.requires.push('ngAnimate');
var seApp = angular.module('app');
app.controller('organigramaCtrl', ["$location", '$scope', '$rootScope', '$http', 'NgTableParams', 'crud', 'modal', 'ModalService',function ($location, $scope, $rootScope, $http, NgTableParams, crud, modal, ModalService){
//////DATOS//////////////////////////////////////////////////////////////////////////////////////////////////
//////Datos//////////////////////////////////////////////////////////////////////////////////////////////////
//////DATOS//////////////////////////////////////////////////////////////////////////////////////////////////
        $rootScope.paramsDatos = {count: 10};
        $rootScope.settingDatos = {counts: []};
        $rootScope.tablaDatos = new NgTableParams($rootScope.paramsDatos, $rootScope.settingDatos);

        $scope.item_datOganigrama= {datId:"",codDat:"",nomDat:"",desDat:"",anioDat:""};
        $scope.limpiarFormularioDato = function(){
            $scope.item_datOganigrama.datId = "";
            $scope.item_datOganigrama.codDat = "";
            $scope.item_datOganigrama.nomDat = "";
            $scope.item_datOganigrama.desDat = "";
            $scope.item_datOganigrama.anioDat = "";
        };
        $scope.cargarFormularioDato = function(a){
            $scope.item_datOganigrama.datId = a.datId;
            $scope.item_datOganigrama.codDat = a.codDat;
            $scope.item_datOganigrama.nomDat = a.nomDat;
            $scope.item_datOganigrama.desDat = a.desDat;
            $scope.item_datOganigrama.anioDat = a.anioDat;
        };

        $scope.listarDatos = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('datosOrgaConfiguracion', 1, 'listarDatosOrg');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingDatos.dataset = data.data;
                console.info(data);
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingDatos.dataset);
                $rootScope.tablaDatos.settings($rootScope.settingDatos);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.eliminarDatOrganigrama = function(c){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar los datos del organigrama? LOS NIVELES JERARQUICOS DEBERAN SER REASIGNADOS", function () {
                var request = crud.crearRequest('datosOrgaConfiguracion', 1, 'eliminarDatosOrg');
                request.setData({datId: c.datId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaDatos.settings().dataset, function(item){
                            return c === item;
                        });
                        $rootScope.tablaDatos.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        $scope.procesarDatOrg = function(){
            if($scope.item_datOganigrama.datId===""){
                $scope.agregarDatOrganigrama(); 
                
            }else{
                $scope.actualizarDatOrganigrama();
            }
            $('#modalnuevodatosOrgani').modal('hide');
        };
        $scope.agregarDatOrganigrama = function(){
            var request = crud.crearRequest('datosOrgaConfiguracion',1,'agregarDatosOrg');
            request.setData({
                    codDat:$scope.item_datOganigrama.codDat,
                    nomDat:$scope.item_datOganigrama.nomDat,
                    desDat:$scope.item_datOganigrama.desDat,
                    anioDat:$scope.item_datOganigrama.anioDat
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarDatos();
                    $rootScope.tablaDatos.reload();
                    modal.mensaje("CONFIRMACION", "Se Agrego Datos Organigrama");
                    
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo almacenar DatosOrganigrama');
            }); 
             
        };
        $scope.actualizarDatOrganigrama = function(){            
            var request = crud.crearRequest('datosOrgaConfiguracion',1,'actualizarDatosOrg');
            request.setData({
                    datId:$scope.item_datOganigrama.datId,
                    codDat:$scope.item_datOganigrama.codDat,
                    nomDat:$scope.item_datOganigrama.nomDat,
                    desDat:$scope.item_datOganigrama.desDat,
                    anioDat:$scope.item_datOganigrama.anioDat
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarDatos();
                    $rootScope.tablaDatos.reload();
                    modal.mensaje("CONFIRMACION", "Se Actualizo Datos Organigrama");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo actualizar datos organigrama');
            }); 
             
        };
        
        

//////CATEGORIA//////////////////////////////////////////////////////////////////////////////////////////////////
//////CATEGORIA//////////////////////////////////////////////////////////////////////////////////////////////////
//////CATEGORIA//////////////////////////////////////////////////////////////////////////////////////////////////
        $rootScope.paramsTiposOrganigrama = {count: 10};
        $rootScope.settingTiposOrganigrama = {counts: []};
        $rootScope.tablaTiposOrganigrama = new NgTableParams($rootScope.paramsTiposOrganigrama, $rootScope.settingTiposOrganigrama);
        
        $scope.item_tipoOrg= {tipId:"",datId:"",codTipOrga:"",nomTipOrga:"",desTipOrga:""};
        var org_Tip=[];
        $scope.limpiarFormularioTipoOrganigrama = function(){
            $scope.item_tipoOrg.tipId = "";
            $scope.item_tipoOrg.datId = "";
            $scope.item_tipoOrg.codTipOrga = "";
            $scope.item_tipoOrg.nomTipOrga = "";
            $scope.item_tipoOrg.desTipOrga = "";
            
        };
        $scope.cargarFormularioTipoOrganigrama = function(a){
            $scope.item_tipoOrg.tipId = a.tipId;
            $scope.item_tipoOrg.datId = a.datId;
            $scope.item_tipoOrg.codTipOrga = a.codTipOrga;
            $scope.item_tipoOrg.nomTipOrga = a.nomTipOrga;
            $scope.item_tipoOrg.desTipOrga = a.desTipOrga;
        };

        $scope.listarTipoOrganigrama = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('tiposOrganigramaConfiguracion', 1, 'listarTiposOrganigrama');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingTiposOrganigrama.dataset = data.data;
                org_Tip=data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingTiposOrganigrama.dataset);
                $rootScope.tablaTiposOrganigrama.settings($rootScope.settingTiposOrganigrama);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.eliminarTipoOrganigrama = function(c){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar este nivel jerarquico?, LAS ENTIDADES RELACIONADAS A ESTE NIVEL JERARQUICO DEBERAN SER REASIGNADAS A OTRO NIVEL JERARQUICO", function () {
                var request = crud.crearRequest('tiposOrganigramaConfiguracion', 1, 'eliminarTiposOrganigrama');
                request.setData({tipId: c.tipId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaTiposOrganigrama.settings().dataset, function(item){
                            return c === item;
                        });
                        $rootScope.tablaTiposOrganigrama.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        $scope.procesarTipoOrganismo = function(){
            if($scope.item_tipoOrg.tipId===""){
                $scope.agregarTipoOrganigrama();
            }else{
                $scope.actualizarTipoOrganigrama();
            }
            $('#modalnuevoTipoOrgani').modal('hide');
        };
        $scope.agregarTipoOrganigrama = function(){
            var request = crud.crearRequest('tiposOrganigramaConfiguracion',1,'agregarTiposOrganigrama');
            request.setData({
                    datId:$scope.item_tipoOrg.datId,
                    codTipOrga:$scope.item_tipoOrg.codTipOrga,
                    nomTipOrga:$scope.item_tipoOrg.nomTipOrga,
                    desTipOrga:$scope.item_tipoOrg.desTipOrga
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarTipoOrganigrama();
                    $rootScope.tablaTiposOrganigrama.reload();
                    modal.mensaje("CONFIRMACION", "Se Agrego Tipo Organigrama");
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo almacenar Tipo Organigrama');
            }); 
             
        };
        $scope.actualizarTipoOrganigrama= function(){            
            var request = crud.crearRequest('tiposOrganigramaConfiguracion',1,'actualizarTiposOrganigrama');
            request.setData({
                    tipId:$scope.item_tipoOrg.tipId,
                    datId:$scope.item_tipoOrg.datId,
                    codTipOrga:$scope.item_tipoOrg.codTipOrga,
                    nomTipOrga:$scope.item_tipoOrg.nomTipOrga,
                    desTipOrga:$scope.item_tipoOrg.desTipOrga

            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarTipoOrganigrama();
                    $rootScope.tablaTiposOrganigrama.reload();
                    modal.mensaje("CONFIRMACION", "Se Actualizo Tipo Organigrama");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo actualizar Tipo Organigrama');
            }); 
             
        };

        
//////ORGANISMO//////////////////////////////////////////////////////////////////////////////////////////////////
//////ORGANISMO//////////////////////////////////////////////////////////////////////////////////////////////////
//////ORGANISMO//////////////////////////////////////////////////////////////////////////////////////////////////
        $rootScope.paramsOrganigrama = {count: 10};
        $rootScope.settingOrganigrama = {counts: []};
        $rootScope.tablaOrganigrama = new NgTableParams($rootScope.paramsOrganigrama, $rootScope.settingOrganigrama);
        
        var flag=false;
        //$scope.item_organigrama= {orgaId:"",datId:"",tipId:"",orgIdPad:"",codOrg:"",abrOrg:"",nomOrg:"",datOrg:""};
        $scope.item_organigrama= {orgId:"",tipId:"",orgIdPad:"",codOrg:"",abrOrg:"",nomOrg:"",datOrg:"",ubiId:""};
        $scope.limpiarFormularioOrganigrama = function(){
            
            $scope.item_organigrama.orgId = "";
            $scope.item_organigrama.tipId = "";
           // $scope.item_organigrama.datId = "";
            $scope.item_organigrama.orgIdPad = "";
            $scope.item_organigrama.codOrg = "";
            $scope.item_organigrama.abrOrg = "";
            $scope.item_organigrama.nomOrg = "";
            $scope.item_organigrama.datOrg = "";
            $scope.item_organigrama.ubiId="";
            //$scope.listarOrgPadre();
            flag=true;
        };
        
        $scope.cargarFormularioOrganigrama = function(a){
            
            $scope.item_organigrama.orgId = a.orgId;
            $scope.item_organigrama.tipId = a.tipId;
            //$scope.item_organigrama.datId = a.datId;
            $scope.item_organigrama.orgIdPad =a.orgIdPad;
            $scope.item_organigrama.codOrg = a.codOrg;
            $scope.item_organigrama.abrOrg = a.abrOrg;
            $scope.item_organigrama.nomOrg = a.nomOrg;
            $scope.item_organigrama.datOrg = a.datOrg;
            $scope.item_organigrama.ubiId=a.ubiId;
            $scope.listarOrgPadre();
            //listarOrganigrama_Padre();
            flag=false;
            
        };
        
        //////////Cargar Tipo x Datos de Organigrama
//        var tipo_Organigramas = [];
//        $scope.tipoOrganigramaSel = [];
//        listarTipo_Organigramas();
//        
//        function listarTipo_Organigramas() {
//            //preparamos un objeto request
//            var request = crud.crearRequest('organigramaConfiguracion', 1, 'listarTiposOrganigrama');
//            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
//            crud.listar("/sistema_escalafon", request, function (data) {
//               if(data.responseSta){
//                   tipo_Organigramas=data.data;
//                   $scope.tipoOrganigramaSel();
//               }
//            }, function (data) {
//                console.info(data);
//            });
//        };
//        $scope.tipoOrganigramaSel = function(){
//            $scope.tipoOrganigramaSel = [];
//            tipo_Organigramas.forEach(function(item){
//                if(item.datId === $scope.item_organigrama.datId)
//                    $scope.tipoOrganigramaSel.push(item);
//            });
//            //$scope.dependenciasSel = [];
//        };
//        
        var org_Pad = [];
        var item_DatId;
        $scope.orgPasdSel = [];
        listarOrganigrama_Padre();
        
        $scope.listarOrgPadre=function(){
            itemDatId();
            listarOrganigrama_Padre();
        };
        
        function itemDatId(){
            org_Tip.forEach(function(item){
                if(item.tipId===$scope.item_organigrama.tipId)
                    item_DatId=item.datId;
            });
        };
        
        function listarOrganigrama_Padre() {
            //preparamos un objeto request
            var request = crud.crearRequest('organigramaConfiguracion', 1, 'listarOrganigrama');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
               if(data.responseSta){
                   org_Pad=data.data;
                   $scope.padrOrganiSel();
                   
                  // console.info(" especializada");
                  // console.info(data);
               }
            }, function (data) {
                console.info(data);
            });
        };
        $scope.padrOrganiSel = function(){
            $scope.orgPasdSel = [];
            org_Pad.forEach(function(item){
                //console.info("flag"+flag);
                if(flag){
                    if(item.datId === item_DatId )
                        $scope.orgPasdSel.push(item);
                }
                else{
                    if(item.datId === item_DatId)
                        if(item.codOrg!==$scope.item_organigrama.codOrg){
                            if(item.orgIdPad!==$scope.item_organigrama.orgId && $scope.item_organigrama.orgIdPad!=="")
                                $scope.orgPasdSel.push(item);
                            else{
                                if(item.orgIdPad==="")
                                    $scope.orgPasdSel.push(item);
                            }
                        }
                }
                
                
            });
            //$scope.dependenciasSel = [];
        };
        
        $scope.zonasOrg = [];
        ////Listar zonas
        $scope.listarZonas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('zonaConfiguracion', 1, 'listarZonas');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                    $scope.zonasOrg=data.data;
                   // console.info($scope.zonasOrg);
                //$rootScope.settingOrganigrama.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                //iniciarPosiciones($rootScope.settingOrganigrama.dataset);
                //$rootScope.tablaOrganigrama.settings($rootScope.settingOrganigrama);
                //console.info("Length Organismos: ",$rootScope.settingOrganismos.dataset.length);
            }, function (data) {
                console.info(data);
            });
        };
        
        ///Listar entidades
        $scope.listarOrganigramas = function () {
            //preparamos un objeto request
            var request = crud.crearRequest('organigramaConfiguracion', 1, 'listarOrganigrama');
            //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request        
            crud.listar("/sistema_escalafon", request, function (data) {
                $rootScope.settingOrganigrama.dataset = data.data;
                //asignando la posicion en el arreglo a cada objeto
                iniciarPosiciones($rootScope.settingOrganigrama.dataset);
                $rootScope.tablaOrganigrama.settings($rootScope.settingOrganigrama);
                //console.info("Length Organismos: ",$rootScope.settingOrganismos.dataset.length);
            }, function (data) {
                console.info(data);
            });
        };
        $scope.eliminarOrganismo = function(c){   
            modal.mensajeConfirmacion($scope, "¿Seguro que desea eliminar el Organigrama?", function () {
                var request = crud.crearRequest('organigramaConfiguracion', 1, 'eliminarOrganigrama');
                request.setData({orgId: c.orgId});
                
                crud.eliminar('/sistema_escalafon', request, function (response) {
                    if (response.responseSta) {
                        _.remove($rootScope.tablaOrganigrama.settings().dataset, function(item){
                            return c === item;
                        });
                        $rootScope.tablaOrganigrama.reload();
                        modal.mensaje("CONFIRMACION", "La eliminación se realizó correctamente");
                    }else {
                            modal.mensaje("ERROR",response.responseMsg);
                    }
                }, function (error) {
                    modal.mensaje("ERROR","El servidor no responde");
                });
            }, '400'); 
        };
        $scope.procesarOrganigrama = function(){
            if($scope.item_organigrama.orgId===""){
                $scope.agregarOrganismo();
                        
                                
            }else{
                $scope.actualizarOrganismo();                
            }
            $('#modalnuevoorganigrama').modal('hide');
        };
        $scope.agregarOrganismo = function(){
            var request = crud.crearRequest('organigramaConfiguracion',1,'agregarOrganigrama');
             if($scope.item_organigrama.orgIdPad===-1 || $scope.item_organigrama.orgIdPad===null||$scope.item_organigrama.orgIdPad==="")
                    $scope.item_organigrama.orgIdPad=0;
            //console.info("codigoPadre: "+$scope.item_organigrama.orgIdPad);
            if($scope.item_organigrama.ubiId===-1 || $scope.item_organigrama.ubiId===null||$scope.item_organigrama.ubiId==="")
                    $scope.item_organigrama.ubiId=0;
            //console.info("ubicacion: "+$scope.item_organigrama.ubiId);    
            request.setData({
                    //datId:$scope.item_organigrama.datId,
                    tipId:$scope.item_organigrama.tipId,                 
                    orgIdPad:$scope.item_organigrama.orgIdPad,
                    //orgIdPad:$scope.item_organigrama.orgIdPad,
                    codOrg:$scope.item_organigrama.codOrg,
                    abrOrg:$scope.item_organigrama.abrOrg,                    
                    nomOrg:$scope.item_organigrama.nomOrg,
                    datOrg:$scope.item_organigrama.datOrg,
                    ubiId:$scope.item_organigrama.ubiId
            });
            crud.insertar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarOrganigramas();
                    $rootScope.tablaOrganigrama.reload();             
                    modal.mensaje("CONFIRMACION", "Se Agrego la entidad");
                    
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo almacenar Organismo');
            }); 
             
        };
        $scope.actualizarOrganismo = function(){            
            var request = crud.crearRequest('organigramaConfiguracion',1,'actualizarOrganigrama');
            if($scope.item_organigrama.orgIdPad===-1 || $scope.item_organigrama.orgIdPad===null||$scope.item_organigrama.orgIdPad==="")
                    $scope.item_organigrama.orgIdPad=0;
            //console.info("codigoPadre: "+$scope.item_organigrama.orgIdPad);
            if($scope.item_organigrama.ubiId===-1 || $scope.item_organigrama.ubiId===null||$scope.item_organigrama.ubiId==="")
                    $scope.item_organigrama.ubiId=0;
            //console.info("ubicacion: "+$scope.item_organigrama.ubiId);
            request.setData({
                    orgId:$scope.item_organigrama.orgId,
                    //datId:$scope.item_organigrama.datId,
                    tipId:$scope.item_organigrama.tipId,
                    orgIdPad:$scope.item_organigrama.orgIdPad,
                    codOrg:$scope.item_organigrama.codOrg,
                    abrOrg:$scope.item_organigrama.abrOrg,                    
                    nomOrg:$scope.item_organigrama.nomOrg,
                    datOrg:$scope.item_organigrama.datOrg,
                    ubiId:$scope.item_organigrama.ubiId
        
            });
            crud.actualizar('/sistema_escalafon',request,function(response){
                if(response.responseSta){
                    $scope.listarOrganigramas();
                    $rootScope.tablaOrganigrama.reload();
                    modal.mensaje("CONFIRMACION", "Se Actualizo Entidad");
                }else{
                    modal.mensaje('ERROR',response.responseMsg);
                }
            },function(errResponse){
                modal.mensaje('ERROR','El servidor no responde No se pudo actualizar organigrama');
            }); 
             
        };

        
}]);
