/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.generar_documento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sdc.DocumentoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ContenidoDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Documento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ImagenPlantilla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Plantilla;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class nuevoDocumentoTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        Documento nuevoDocumento = null; 
        ContenidoDocumento contDoc = null;
        Integer imgID=-1;
        ImagenPlantilla img=null;
        try {

            JSONObject requestData = (JSONObject) wr.getData();

            JSONArray contenidos = (JSONArray) requestData.getJSONArray("contenidoPlantilla");

            Integer usuario = requestData.getInt("personaID");
            Integer plaId=requestData.getInt("planId");
            if(requestData.has("imgID"))
            {
                imgID=requestData.getInt("imgID");
                img=new ImagenPlantilla(imgID);
            }
            
            Plantilla plaDoc=new Plantilla(plaId);
          

            List<ContenidoDocumento> ctnDoc = new ArrayList<>();
            nuevoDocumento = new Documento(usuario, plaDoc, new Date());
            for (int i = 0; i < 3; i++) {

                JSONObject  ctn = (JSONObject) contenidos.get(i);
                String ctn_=ctn.getString("contenido");
                
  
                String tip = String.valueOf(i + 1);

                contDoc = new ContenidoDocumento(tip, ctn_, nuevoDocumento);
                ctnDoc.add(contDoc);

            }
            nuevoDocumento.setContenidoDocumento(ctnDoc);

            if(imgID>=0)
            {
                nuevoDocumento.setImagenAdjunta(img);
            }
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo obtener datos y/o contenido del Documento", e.getMessage());
        }
        //Fin        

        /*
         *  Parte para la operacion en la Base de Datos
         */
        DocumentoDao documentoDao = (DocumentoDao) FactoryDao.buildDao("sdc.DocumentoDao");
        try {
            documentoDao.insert(nuevoDocumento);

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se genero el Documento ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Se inserto correctamente");
        //Fin
    }

}
