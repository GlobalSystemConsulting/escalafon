/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.login.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FuncionSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.AjustePaginaWebDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.RolFuncion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.RolFuncionModel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.security.TokenHandler;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.AjustePaginaWeb;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.BuiltJSON;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class SignInUsuarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        String username = "";
        String password = "";
        int organizacionID = 0;
        int rolID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            username = requestData.getString("nombre");
            password = requestData.getString("password");
            organizacionID = requestData.getInt("organizacionID" );
            rolID = requestData.getInt("rolID");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("Los datos enviados son incorrectos", e.getMessage() );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        Persona persona =null;
        Usuario usuario = null;
        try{
            usuario = ((UsuarioDao)FactoryDao.buildDao("UsuarioDao")).buscarPorUsuarioYPassword(username,password);
        
        }catch(Exception e){
            System.out.println("No se pudo buscar por Nombre y Password\n"+e);
            return WebResponse.crearWebResponseError("No se pudo buscar por Nombre y Password", e.getMessage() );
        }
        
        //validar el usuario
        if(usuario != null){
            
            PersonaDao personaDao = (PersonaDao)FactoryDao.buildDao("PersonaDao");
            persona=personaDao.buscarPorID(usuario.getUsuId());
            
            System.out.println("persona encontrada"+persona.getDni());
            
            UsuarioSession session = null;
            
            for( UsuarioSession us: usuario.getSessiones() )
                if( us.getOrganizacion().getOrgId() == organizacionID && us.getRol().getRolId() == rolID ){
                    session = us;
                    break;
                }
            
            if(session==null)
                return WebResponse.crearWebResponseExito("error el rol y oganizacion no pertenecen al usuario");
            
            
            
            List<ModuloSistema> modulos = new ArrayList<ModuloSistema>();
            //List<FuncionSistema> funciones = ((FuncionSistemaDao)FactoryDao.buildDao("FuncionSistemaDao")).buscarPorRol( session.getRol().getRolId() );               
            List<RolFuncionModel> funciones = ((FuncionSistemaDao)FactoryDao.buildDao("FuncionSistemaDao")).buscarFuncionesPersonalizadasPorRol( session.getRol().getRolId() );
            
            for(RolFuncionModel f :funciones)
                buscarModulo(modulos,f);
            
            JSONArray aModulos = new JSONArray();
            for(ModuloSistema modulo:modulos ){
                JSONObject oModulo = new JSONObject();
                oModulo.put("moduloID",modulo.getModSisId() );
                oModulo.put("nombre",modulo.getNom());
                oModulo.put("codigo",modulo.getCod());
                if(modulo.getIco().contentEquals(""))
                    oModulo.put("icono","../recursos/img/module1.png");
                else
                    oModulo.put("icono",modulo.getIco());
                
                if( modulo.getSubModuloSistemas().size() > 0 ){
                    JSONArray aSubModulos = new JSONArray();
                    for(SubModuloSistema subModulo:modulo.getSubModuloSistemas()){
                        JSONObject oSubModulo = new JSONObject();
                        oSubModulo.put("subModuloID",subModulo.getSubModSisId() );
                        oSubModulo.put("nombre",subModulo.getNom());
                        oSubModulo.put("codigo",subModulo.getCod());
                        oSubModulo.put("icono",subModulo.getIco());
                        if(subModulo.getIco().contentEquals(""))
                            oSubModulo.put("icono","../recursos/img/module1.png");
                        else
                            oSubModulo.put("icono",subModulo.getIco());
                        
                        
                        if( subModulo.getFunciones().size() > 0 ){
                            JSONArray aFunciones = new JSONArray();
                            
                            for(RolFuncionModel funcion:subModulo.getFunciones()){
                                JSONObject oFuncion = new JSONObject();
                                oFuncion.put("funcionID",funcion.funcionID );
                                oFuncion.put("nombre",funcion.nombre);
                                oFuncion.put("url",funcion.url);
                                oFuncion.put("clave",funcion.clave);
                                oFuncion.put("controlador",funcion.controlador);
                                oFuncion.put("interfaz",funcion.interfaz);
                                 if(funcion.icono.contentEquals(""))
                                    oFuncion.put("icono","../recursos/img/module1.png");
                                else
                                    oFuncion.put("icono",funcion.icono);
                                oFuncion.put("tipo",funcion.tipo);
                                oFuncion.put("dependencias",funcion.dependencias);
                                aFunciones.put(oFuncion);                            
                            }
                            oSubModulo.put("funciones",aFunciones);
                        }
                        
                        
                        aSubModulos.put(oSubModulo);
                    }
                    oModulo.put("subModulos",aSubModulos);
                }
                
                aModulos.put(oModulo);
            }
            
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("jwt",TokenHandler.getInstance().createTokenForUser(usuario));
            oResponse.put("url","/app/");
            
            JSONObject usuarioReponse = new JSONObject();
            usuarioReponse.put("ID", session.getUsuSesId());
            usuarioReponse.put("usuarioID", session.getUsuario().getUsuId());
            usuarioReponse.put("nombre", usuario.getNom() );
            if(persona.getFoto()!=null)
                usuarioReponse.put("foto",persona.getFoto());
            else
                usuarioReponse.put("foto","../recursos/img/avatar1_small.jpg");
            oResponse.put("usuario", usuarioReponse);
            
            String[] atributosO = {"OrgId","Nom","Ali"};
            String[] etiquetasO = {"organizacionID","nombre","alias"};
            oResponse.put("organizacion", BuiltJSON.builtJSONObjectFromPojo(session.getOrganizacion(), atributosO , etiquetasO ) );
            
            if(session.getArea()!=null){
                String[] atributosA = {"AreId","Nom","TipAreID"};
                String[] etiquetasA = {"areaID","nombre","tipoID"};
                oResponse.put("area", BuiltJSON.builtJSONObjectFromPojo(session.getArea(), atributosA , etiquetasA ) );
            }
            
            String[] atributosR = {"RolId","Nom"};
            String[] etiquetasR = {"rolID","nombre"};
            oResponse.put("rol", BuiltJSON.builtJSONObjectFromPojo(session.getRol(), atributosR , etiquetasR ) );
            
            oResponse.put("modulos",aModulos);
            
            //Carga de personalizacion
            AjustePaginaWeb ajuste= ((AjustePaginaWebDao)FactoryDao.buildDao("mnt.AjustePaginaWebDao")).obtenerAjustePorIdUsuario(usuario);
            if(ajuste!=null){//Tiene personalizacion
                JSONObject prop=new JSONObject();
                prop.put("ladoIzq", ajuste.getAjusteIzq());
                prop.put("ladoDer",ajuste.getAjusteDer());
                prop.put("color",ajuste.getAjusteColor());
                oResponse.put("personalizacion",prop);
            }            
            
            return WebResponse.crearWebResponseExito("el usuario se encuentra en la BD", oResponse);
        }else{
            return WebResponse.crearWebResponseError("contraseña incorrecta");
        }
    }
    public void buscarModulo(List<ModuloSistema> modulos,RolFuncionModel funcion){
        
        SubModuloSistema subModulo = new SubModuloSistema(funcion.subModuloID);
        subModulo.setNom(funcion.subNombre);
        subModulo.setCod(funcion.subCodigo);
        subModulo.setIco(funcion.subIcono);
        ModuloSistema modulo  = new ModuloSistema(funcion.moduloID);
        modulo.setNom(funcion.modNombre);
        modulo.setCod(funcion.modCodigo);
        modulo.setIco(funcion.modIcono);
        //System.out.println(funcion.getNom() +"  " +subModulo.getNom()+"  "+modulo.getNom());
        for(ModuloSistema m : modulos){
            if(m.getModSisId() == modulo.getModSisId()){
                buscarSubModulo(m.getSubModuloSistemas(),subModulo,funcion);
                return;
            }
        }
        //añadimos un nuevo modulo
        modulo.setSubModuloSistemas( new ArrayList<SubModuloSistema>());
        subModulo.setFunciones( new ArrayList<RolFuncionModel>());
        subModulo.getFunciones().add(funcion);
        modulo.getSubModuloSistemas().add(subModulo);
        modulos.add(modulo);                
    }
    public void buscarSubModulo(List<SubModuloSistema> subModulos, SubModuloSistema subModulo,RolFuncionModel funcion){
        
        for(SubModuloSistema sm : subModulos){
            if(sm.getSubModSisId() == subModulo.getSubModSisId()){
                sm.getFunciones().add(funcion);
                return;
            }
        }
        //añadimos un nuevo subModulo
        subModulo.setFunciones( new ArrayList<RolFuncionModel>());
        subModulo.getFunciones().add(funcion);
        subModulos.add(subModulo);
    }
    /*
    public void buscarModulo(List<ModuloSistema> modulos,RolFuncion funcion){
        
        SubModuloSistema subModulo = funcion.getFuncionSistema().getSubModuloSistema();
        ModuloSistema modulo  = subModulo.getModuloSistema();
        //System.out.println(funcion.getNom() +"  " +subModulo.getNom()+"  "+modulo.getNom());
        for(ModuloSistema m : modulos){
            if(m.getModSisId() == modulo.getModSisId()){
                buscarSubModulo(m.getSubModuloSistemas(),subModulo,funcion);
                return;
            }
        }
        //añadimos un nuevo modulo
        modulo.setSubModuloSistemas( new ArrayList<SubModuloSistema>());
        subModulo.setFunciones( new ArrayList<RolFuncion>());
        subModulo.getFunciones().add(funcion);
        modulo.getSubModuloSistemas().add(subModulo);
        modulos.add(modulo);                
    }
    public void buscarSubModulo(List<SubModuloSistema> subModulos, SubModuloSistema subModulo,RolFuncion funcion){
        
        for(SubModuloSistema sm : subModulos){
            if(sm.getSubModSisId() == subModulo.getSubModSisId()){
                sm.getFunciones().add(funcion);
                return;
            }
        }
        //añadimos un nuevo subModulo
        subModulo.setFunciones( new ArrayList<RolFuncion>());
        subModulo.getFunciones().add(funcion);
        subModulos.add(subModulo);
    }
    */
}
