/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.area.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.AreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoArea;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class ActualizarAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Area areaAct = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int organizacionID = requestData.getInt("organizacionID");
            Integer areaPadreID = requestData.getInt("areaPadreID");
            int tipoAreaID = requestData.getInt("tipoAreaID");
            int areaID = requestData.getInt("areaID");
            String codigo = requestData.getString("codigo");
            String nombre = requestData.getString("nombre");
            String estado = requestData.getString("estado");
            
            if(areaPadreID == 0)
                areaAct = new Area(areaID,new Organizacion(organizacionID), new TipoArea(tipoAreaID), codigo, nombre,new Date(), wr.getIdUsuario(), estado.charAt(0));            
            else
                areaAct = new Area(areaID,new Organizacion(organizacionID),new Area(areaPadreID==null?0:areaPadreID),new TipoArea(tipoAreaID), codigo, nombre, new Date(), wr.getIdUsuario(), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        AreaDao areaDao = (AreaDao)FactoryDao.buildDao("AreaDao");
        try{
            areaDao.update(areaAct);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el Area\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el Area", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Area se actualizo correctamente");
        //Fin
    }
    
}

