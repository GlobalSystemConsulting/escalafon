package com.dremo.ucsm.gsc.sigesmed.logic.maestro.anecdotario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario.AnecdotarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario.AccionesCorrectivas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario.Anecdotario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 23/12/2016.
 */
public class EditarAnecdotaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EditarAnecdotaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();

        int idAnec = data.getInt("id");
        JSONArray acciones = data.optJSONArray("acciones");

        return editarAnecdota(idAnec, data, acciones);
    }

    private WebResponse editarAnecdota(int idAnec, JSONObject data, JSONArray acciones) {
        try{
            AnecdotarioDao anecDao = (AnecdotarioDao) FactoryDao.buildDao("maestro.anecdotario.AnecdotarioDao");
            Anecdotario anecdota = anecDao.buscarAnecdotaConAcciones(idAnec);
            anecdota.setFec(new Date(data.optLong("fec",anecdota.getFec().getTime())));
            anecdota.setDes(data.optString("des", anecdota.getDes()));
            anecdota.setDer(data.optString("der", anecdota.getDer()));
            if (acciones != null){
                anecDao.eliminarAccionesCorrectivas(idAnec);
                anecdota.getAccionesCorrectivas().clear();
                for(int i = 0 ; i < acciones.length(); i++){
                    JSONObject jsonAccion = acciones.getJSONObject(i);
                    String nomAccion = jsonAccion.getString("nom");
                    AccionesCorrectivas accion = new AccionesCorrectivas(nomAccion);
                    accion.setAnecdotario(anecdota);
                    anecdota.getAccionesCorrectivas().add(accion);
                }
            }
            anecDao.update(anecdota);
            return WebResponse.crearWebResponseExito("Se edito la anecdota correctamente");

        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarAnecdota",e);
            return WebResponse.crearWebResponseError("error al editar la anecdota");
        }
    }
}
