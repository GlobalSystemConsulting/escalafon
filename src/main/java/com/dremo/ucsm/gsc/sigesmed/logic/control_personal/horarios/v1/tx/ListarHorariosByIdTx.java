/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ListarHorariosByIdTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        //Integer org;
        Integer idCab;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
           // org = requestData.getInt("organizacionID");        
           idCab=requestData.getInt("horCabId");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }
     
        HorarioCab horarioCab=new HorarioCab(idCab);
        List<HorarioDet> horario = new ArrayList<>();
        HorarioDao horarioDao = (HorarioDao)FactoryDao.buildDao("cpe.HorarioDao");
        try{
            horario =horarioDao.listarHorarioById(horarioCab);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar el Horario", e.getMessage() );
        }

        JSONArray miArray = new JSONArray();
        SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm a");//dd/MM/yyyy
       

            for(HorarioDet hd:horario)
            {
                
                JSONObject oResponseDet = new JSONObject();
                switch(hd.getDiaSemId().getDiaSemId())
                {
                    case 'L':
                        oResponseDet.put("nom","L");
                        oResponseDet.put("id",hd.getHorDetId());
                        oResponseDet.put("lunDesde",sdfDate.format(hd.getFecIngreso()));
                        oResponseDet.put("lunHasta",sdfDate.format(hd.getFecSalida()));
                        miArray.put(oResponseDet);
                        break;
                    case 'M':
                        oResponseDet.put("nom","M");
                        oResponseDet.put("id",hd.getHorDetId());
                        oResponseDet.put("marDesde",sdfDate.format(hd.getFecIngreso()));
                        oResponseDet.put("marHasta",sdfDate.format(hd.getFecSalida()));
                        miArray.put(oResponseDet);
                        break;
                    case 'W':
                        oResponseDet.put("nom","W");
                        oResponseDet.put("id",hd.getHorDetId());
                        oResponseDet.put("mieDesde",sdfDate.format(hd.getFecIngreso()));
                        oResponseDet.put("mieHasta",sdfDate.format(hd.getFecSalida()));
                        miArray.put(oResponseDet);
                        break;
                    case 'J':
                        oResponseDet.put("nom","J");
                        oResponseDet.put("id",hd.getHorDetId());
                        oResponseDet.put("jueDesde",sdfDate.format(hd.getFecIngreso()));
                        oResponseDet.put("jueHasta",sdfDate.format(hd.getFecSalida()));
                        miArray.put(oResponseDet);
                        break;
                    case 'V':
                        oResponseDet.put("nom","V");
                        oResponseDet.put("id",hd.getHorDetId());
                        oResponseDet.put("vieDesde",sdfDate.format(hd.getFecIngreso()));
                        oResponseDet.put("vieHasta",sdfDate.format(hd.getFecSalida()));
                        miArray.put(oResponseDet);
                        break;
                    case 'S':
                        oResponseDet.put("nom","S");
                        oResponseDet.put("id",hd.getHorDetId());
                        oResponseDet.put("sabDesde",sdfDate.format(hd.getFecIngreso()));
                        oResponseDet.put("sabHasta",sdfDate.format(hd.getFecSalida()));
                        miArray.put(oResponseDet);
                        break;
                    case 'D':
                        oResponseDet.put("nom","D");
                        oResponseDet.put("id",hd.getHorDetId());
                        oResponseDet.put("domDesde",sdfDate.format(hd.getFecIngreso()));
                        oResponseDet.put("domHasta",sdfDate.format(hd.getFecSalida()));
                        miArray.put(oResponseDet);
                        break;
                }
              
            }

        return WebResponse.crearWebResponseExito("Se Listo correctamente", miArray);
        //Fin
    }

}
