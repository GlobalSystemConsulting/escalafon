/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ReconocimientoDaoHibernate extends GenericDaoHibernate<Reconocimiento> implements ReconocimientoDao{

    private static final Logger logger = Logger.getLogger(ReconocimientoDaoHibernate.class.getName());

    @Override
    public List<Reconocimiento> listarxFichaEscalafonaria(int perId) {
        List<Reconocimiento> reconocimientos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT r from Reconocimiento as r "
                    + "join fetch r.persona as fe "
                    + "WHERE fe.perId=" + perId + " AND r.estReg='A'";
            Query query = session.createQuery(hql);
            reconocimientos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println(">>DAO: No se pudo listar los reconocimientos \n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los reconocimientos \n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return reconocimientos;
    }

    @Override
    public Reconocimiento buscarPorId(Integer recId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Reconocimiento rec = (Reconocimiento)session.get(Reconocimiento.class, recId);
        session.close();
        return rec;
    }

    @Override
    public JSONObject contarxOrganizacion(int orgId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            
            String queryStr = "SELECT COUNT(rec_id)\n" +
                            "  FROM administrativo.reconocimiento AS rec,\n" +
                            "   administrativo.ficha_escalafonaria AS fic, \n" +
                            "   public.trabajador AS tra\n" +
                            "   WHERE rec.fic_esc_id = fic.fic_esc_id AND fic.tra_id = tra.tra_id \n" +
                            "   AND (rec.tip_mot = '1' OR rec.tip_mot = '2') AND tra.org_id = " + orgId + 
                            "   GROUP BY rec.tip_mot";

            List<BigInteger> respuesta = (List<BigInteger>)session.createSQLQuery(queryStr).list();
            System.out.println(respuesta.get(0).toString());
            int aux = Integer.parseInt(respuesta.get(0).toString()) + Integer.parseInt(respuesta.get(1).toString());
            
            JSONObject resultado = new JSONObject();
            resultado.put("totalMeritos", aux);
            System.out.println(resultado);
            return resultado;
            
        } catch (NumberFormatException | JSONException e){
            logger.log(Level.SEVERE,"totalMeritosPorOrganizacion",e);
            throw e;
        } finally {
            session.close();
        } 
    }
    
}
