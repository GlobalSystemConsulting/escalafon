package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaInsDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.util.ITextMatricula;
import com.dremo.ucsm.gsc.sigesmed.util.ITextUtil;
import com.dremo.ucsm.gsc.sigesmed.util.JSONUtil;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.Style;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
//        
//        String arial = "src\\main\\webapp\\archivos\\matricula_institucional\\templates\\fonts\\arial.ttf";
//        //String path = ServicioREST.PATH_SIGESMED + "\\archivos\\matricula_institucional\\constanciaMatricula\\";
//        String path = "src\\main\\webapp\\archivos\\matricula_institucional\\constanciaMatricula\\";
//        String name = "constanciamatricula.pdf";
//        ITextMatricula myPdf = new ITextMatricula(path, name, true, 0.5, 0.5, 0.5, 0.5);
//        //String urlImg = ServicioREST.PATH_SIGESMED + "\\archivos\\matricula_institucional\\templates\\min_edu_cab.png";
//        String minEdu = "src\\main\\webapp\\archivos\\matricula_institucional\\templates\\imgs\\min_edu_cab.png";
//        String tiger = "src\\main\\webapp\\archivos\\matricula_institucional\\templates\\imgs\\tiger_extend.png";
//
////        myPdf.addDocumentHeader(minEdu, tiger);
////        myPdf.addDocumentHeader(minEdu, "Fecha: 11/11/2017\nPagina 1 de 1", ITextUtil.getFont(arial, 12, Color.BLACK, true, false, false));
////        myPdf.addLineJump();
////        myPdf.addLineJump();
////        myPdf.addLineJump();
//        myPdf.addParagraph("Nomina de Matricula", ITextUtil.getFont(arial, 22, Color.BLACK, true, false, false), ITextUtil.ALIGN_CENTER);
//        myPdf.addLineJump();
//
//        PdfFont fontTimes;
//        try {
//            fontTimes = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
//            Style titulo1 = new Style().setFont(fontTimes).setFontSize(8);
//            titulo1.setBold();
//            Style titulo0 = new Style().setFont(fontTimes).setFontSize(22);
//            titulo1.setBold();
//            Style normal = new Style().setFont(fontTimes).setFontSize(8);
//            Style indice = new Style().setFont(fontTimes).setFontSize(6);
//
//            myPdf.addParagraph("Nomina de Matricula", titulo0, ITextUtil.ALIGN_CENTER);
//            myPdf.addLineJump();
//
//            /**
//             * myPdf.insertTableConstanciaMatricula(ITextUtil.getFont(arial, 11,
//             * Color.BLACK, true, false, false), ITextUtil.getFont(arial, 11,
//             * Color.BLACK, false, false, false), "999999999999999", "Mariano
//             * Melgar Feliciado", "999999999999999", "IE honorio Delgado
//             * Ezpinosa", "Primaria", "JEC", "Jornada Escolar Completa",
//             * "Tarde", "1ro", 'A', "74744444", "Pedrito Juarez Delgado");
//             */
//            MatriculaInsDaoHibernate dao = new MatriculaInsDaoHibernate();
//            //List<Object[]> alumnos = dao.getNominaEstudiantes(6, 1, 1, 'T', 9, 'A');
//
//            myPdf.insertTableNominaMatricula(
//                    titulo1,
//                    normal,
//                    "IE. Maria Candelaria", "PGD", "1234567", "PM", "-", "RD N° 0006", "Esc", "PRI", "3", "-", "D", "Mi mama me mima mucho", "M", "EBR", "Jornada Escolar Completa",
//                    "14/03/2016", "30/12/2016", "PUNO", "PUNO", "ACORA", "MOLINO", "210001", "UGEL Puno", null);
//
//            myPdf.addParagraph("", normal);
//
//            myPdf.addParagraph("", normal);
//
//            myPdf.insertTableIndiceNomina(indice);
//
//            myPdf.addParagraph("", normal);
//
//            myPdf.addParagraph("", normal);
//
//            myPdf.insertPiePaginaNomina(10, 11, titulo1, normal);
//
//            myPdf.closeDocument();
//
//        } catch (IOException ex) {
//        }
//
        System.out.println(JSONUtil.getUbigeoLocation("040401").toString());
    }
}
