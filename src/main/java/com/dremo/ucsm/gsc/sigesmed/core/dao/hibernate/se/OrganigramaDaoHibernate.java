/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author felipe
 */
public class OrganigramaDaoHibernate extends GenericDaoHibernate<Organigrama> implements OrganigramaDao{

    @Override
    public List<Organigrama> listarXOrganigrama() {
        List<Organigrama> organigramas = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT o from Organigrama as o "
                    + "WHERE o.estReg='A'";
            Query query = session.createQuery(hql);
            organigramas = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los organigramas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los organigramas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return organigramas;
    }

    @Override
    public Organigrama buscarXId(Integer orgiId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Organigrama item = (Organigrama)session.get(Organigrama.class, orgiId);
        session.close();
        return item;
    }

    @Override
    public List<Organigrama> listarxTipo(int tipoOrganismoId) {
        List<Organigrama> organismos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT o from Organigrama as o "
                    + "LEFT JOIN FETCH o.organigramaPadre op "
                    + "LEFT JOIN FETCH o.ubicacion u "
                    + "JOIN FETCH o.tipoOrganigrama to "
                    + "WHERE o.estReg='A' "
                    + "AND to.tipOrgiId='"+tipoOrganismoId+"'";
            Query query = session.createQuery(hql);
            organismos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los organigramas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los organigramas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return organismos;
    }

    @Override
    public List<Organigrama> listarXCampo(String campo) {
        List<Organigrama> organigramas = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT o from Organigrama as o "
                    + "WHERE o.camOrgi='"+campo
                    + "' AND  o.estReg='A'";
            Query query = session.createQuery(hql);
            organigramas = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los organigramas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los organigramas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return organigramas;
    }

    @Override
    public List<Organigrama> listarXCodigo(String codigo) {
        List<Organigrama> organigramas = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT o from Organigrama as o "
                    + "WHERE o.codOrgi='"+codigo
                    + "'  AND o.estReg='A'";
            Query query = session.createQuery(hql);
            organigramas = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los organigramas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los organigramas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return organigramas;
    }

    @Override
    public Organigrama buscarXCodigo(Integer codigo) {
        Organigrama fichaEscalafonaria = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();

        try {
            String hql = "SELECT o from Organigrama as o "
                    + "WHERE o.codOrgi=' "+codigo
                    + "' AND o.estReg='A'";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            fichaEscalafonaria = (Organigrama) query.uniqueResult();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo  \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo  \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fichaEscalafonaria;
    }

    @Override
    public Organigrama buscarXCodigo(String codigo) {
        Organigrama fichaEscalafonaria = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();

        try {
            String hql = "SELECT o from Organigrama as o "
                    + "WHERE o.codOrgi=' "+codigo
                    + "' AND o.estReg='A'";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            fichaEscalafonaria = (Organigrama) query.uniqueResult();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo  \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo  \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fichaEscalafonaria;
    }
    
    
    @Override
    public Organigrama obtenerDetalle(int orgiId) {
        Organigrama organigrama = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT o from Organigrama as o "
                    + "JOIN FETCH o.tipoOrganigrama tipOrg "
                    + "LEFT JOIN FETCH o.ubicacion ubi "
                    + "JOIN FETCH tipOrg.datosOrganigramas datOrg "
                    + "WHERE o.orgiId='"+orgiId+"' "
                    + "AND o.estReg='A'";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            organigrama = (Organigrama) query.uniqueResult();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los organigramas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las organigramas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return organigrama;
    }
    
}
