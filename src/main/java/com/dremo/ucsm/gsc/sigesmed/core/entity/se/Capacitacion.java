/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "capacitacion", schema = "administrativo")
public class Capacitacion implements Serializable {

    @Id
    @Column(name = "cap_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_capacitacion", sequenceName="administrativo.capacitacion_cap_id_seq" )
    @GeneratedValue(generator="secuencia_capacitacion")
    private Integer capId;

    @Column(name = "nom")
    private String nom;

    @Column(name = "tip")
    private String tip;
    
    @Column(name = "fec")
    @Temporal(TemporalType.DATE)
    private Date fec;
    
    @Column(name = "cal")
    private Integer cal;

    @Column(name = "lug")
    private String lug;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "per_id")
    private Persona persona;

    public Capacitacion() {
    }

    public Capacitacion(Integer capId) {
        this.capId = capId;
    }
    
    public Capacitacion(Persona persona, String nom, String tip, Date fec, Integer cal, String lug, Integer usuMod, Date fecMod, Character estReg) {
        this.persona = persona;
        this.nom = nom;
        this.tip = tip;
        this.fec = fec;
        this.cal = cal;
        this.lug = lug;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    
    public Integer getCapId() {
        return capId;
    }

    public void setCapId(Integer capId) {
        this.capId = capId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Date getFec() {
        return fec;
    }

    public void setFec(Date fec) {
        this.fec = fec;
    }

    public Integer getCal() {
        return cal;
    }

    public void setCal(Integer cal) {
        this.cal = cal;
    }

    public String getLug() {
        return lug;
    }

    public void setLug(String lug) {
        this.lug = lug;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (capId != null ? capId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Capacitacion)) {
            return false;
        }
        Capacitacion other = (Capacitacion) object;
        if ((this.capId == null && other.capId != null) || (this.capId != null && !this.capId.equals(other.capId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Capacitacion{" + "capId=" + capId + ", nom=" + nom + ", tip=" + tip + ", fec=" + fec + ", cal=" + cal + ", lug=" + lug + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + ", persona=" + persona + '}';
    }

}
