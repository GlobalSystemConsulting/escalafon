package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.FichaEvaPerslDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 22/08/2016.
 */
public class DeleteFichaEvaluacionTx extends MepGeneralTx implements ITransaction {
    private static Logger logger = Logger.getLogger(DeleteFichaEvaluacionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String id = wr.getMetadataValue("id");
        return deleteFicha(Integer.parseInt(id));
    }
    public WebResponse deleteFicha(int idFicha){
        try{
            FichaEvaPerslDaoHibernate fedh = new FichaEvaPerslDaoHibernate();
            boolean isCorrectDelete = fedh.deleteFichaEvaluacionById(idFicha);
            return formWebResponse(isCorrectDelete);
        }catch (JSONException e){
            logger.log(Level.INFO,logger.getName() + ":deleteFicha()",e);
            return formWebResponse(false);
        }

    }
}
