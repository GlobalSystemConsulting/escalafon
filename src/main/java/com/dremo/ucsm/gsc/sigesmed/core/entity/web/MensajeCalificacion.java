package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@IdClass(MensajeCalificacionId.class)
@Entity
@Table(name="mensaje_calificacion" ,schema="pedagogico")
public class MensajeCalificacion  implements java.io.Serializable {

    @Id 
    @Column(name="men_cal_id", unique=true, nullable=false)
    private int menCalId;
    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="eva_esc_id", nullable=false)
    private EvaluacionEscolar evaluacion;
    
    @Column(name="men", length=32)
    private String mensaje;
    
    @Column(name="pun")
    private int puntos;

    public MensajeCalificacion() {
    }

	
    public MensajeCalificacion(int menCalId, EvaluacionEscolar evaluacion) {
        this.menCalId = menCalId;
        this.evaluacion = evaluacion;
    }
    public MensajeCalificacion(int menCalId, EvaluacionEscolar evaluacion, String mensaje,int puntos) {
       this.menCalId = menCalId;
       this.evaluacion = evaluacion;
       this.mensaje = mensaje;
       this.puntos = puntos;
              
    }
   
     
    public int getMenCalId() {
        return this.menCalId;
    }
    public void setMenCalId(int menCalId) {
        this.menCalId = menCalId;
    }

    public EvaluacionEscolar getEvaluacion() {
        return this.evaluacion;
    }
    
    public void setEvaluacion(EvaluacionEscolar evaluacion) {
        this.evaluacion = evaluacion;
    }

    
    public String getMensaje() {
        return this.mensaje;
    }
    
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    public int getPuntos(){
        return this.puntos;        
    }
    public void setPuntos(int puntos){
        this.puntos = puntos;
    }
    
}


