/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import static com.dremo.ucsm.gsc.sigesmed.util.JSONUtil.getUbigeoLocation;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class BuscarFichaEscalafonariaPorUsuarioIdTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(BuscarFichaEscalafonariaPorUsuarioIdTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject)wr.getData();
        System.out.println(requestData);
        
        Integer usuId = requestData.getInt("usuId");
        Integer opcion = requestData.getInt("opcion");
                
        FichaEscalafonaria fichaEscalafonaria = new FichaEscalafonaria();
        FichaEscalafonariaDao fichaEscalafonariaDao = (FichaEscalafonariaDao)FactoryDao.buildDao("se.FichaEscalafonariaDao");
        
        try{
            fichaEscalafonaria = fichaEscalafonariaDao.buscarPorUsuId(usuId);

        }catch(Exception e){
            System.out.println("No se encontro la ficha escalafonaria \n"+e);
            logger.log(Level.SEVERE,"Buscar ficha escalafonaria",e);
            return WebResponse.crearWebResponseError("No se encontro la ficha escalafonaria ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONObject data = new JSONObject();        
        if (opcion == 1){
            
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            Persona miPersona = fichaEscalafonaria.getTrabajador().getPersona();
            
            JSONObject persona = new JSONObject();
            JSONObject trabajador = new JSONObject();
            JSONObject ficha = new JSONObject();
            
            String apePat = miPersona.getApePat();
            String apeMat = miPersona.getApeMat();
            String nom = miPersona.getNom();
            Character sex = miPersona.getSex();
            Integer estCivId = miPersona.getEstCivId()==null?0:miPersona.getEstCivId();
            String dni=miPersona.getDni();
            String pas=miPersona.getPas();
            Boolean estAseEss=miPersona.getBoolSalud();
            String autEss = miPersona.getAutEss();
            String fij = miPersona.getFij();
            String num1 = miPersona.getNum1();
            String num2 = miPersona.getNum2();
            String email = miPersona.getEmail();
            Integer nacId = miPersona.getNacionalidad()==null?0:miPersona.getNacionalidad().getNacId();
            String depNac = miPersona.getDepNac()==null?"":miPersona.getDepNac();
            String proNac = miPersona.getProNac()==null?"":miPersona.getProNac();
            String disNac = miPersona.getDisNac()==null?"":miPersona.getDisNac();
            Date fecNac = miPersona.getFecNac();
            
            String sisPen = miPersona.getSisPen();
            String tipAfp = miPersona.getTipAfp();
            String codCuspp = miPersona.getCodCuspp();
            Date fecIngAfp = miPersona.getFecIngAfp();
            Date fecTraAfp = miPersona.getFecTraAfp();
            Boolean perDis = miPersona.getPerDis();
            String regCon = miPersona.getRegCon();
            Integer idiomId = miPersona.getIdiomas()==null?0:miPersona.getIdiomas().getIdiId();
            String licCond = miPersona.getLicCond();
            String bonCaf = miPersona.getBonCaf();
            String foto=miPersona.getFoto();
            
            persona.put("perId", miPersona.getPerId());
            persona.put("perCod", miPersona.getPerCod());
            persona.put("apePat", apePat==null?"":apePat);
            persona.put("apeMat",  apeMat==null?"":apeMat);
            persona.put("nom", nom==null?"":nom);
            persona.put("sex", sex==null?"":sex);
            persona.put("estCivId", estCivId);
            
            persona.put("dni", dni);
            persona.put("pas", pas==null?"":pas);
            persona.put("estAseEss", estAseEss==null?"":estAseEss);
            persona.put("autEss", autEss==null?"":autEss);
            persona.put("fij", fij==null?"":fij);
            persona.put("num1", num1==null?"":num1);
            persona.put("num2", num2==null?"":num2);
            persona.put("email", email==null?"":email);
            persona.put("nacId", nacId);
            String [] locationNac = getUbigeoLocation(depNac+proNac+disNac);
            persona.put("depNacDes", locationNac[0]);
            persona.put("proNacDes", locationNac[1]);
            persona.put("disNacDes", locationNac[2]);
            
            persona.put("fecNac", fecNac==null?"":sdi.format(fecNac));
            persona.put("sisPen", sisPen==null?"NUL":sisPen);
            persona.put("tipAfp", tipAfp==null?"":tipAfp);
            persona.put("codCuspp", codCuspp==null?"":codCuspp);
            persona.put("fecIngAfp", fecIngAfp==null?"":sdi.format(fecIngAfp));
            persona.put("fecTraAfp", fecTraAfp==null?"":sdi.format(fecTraAfp));
            persona.put("perDis", perDis==null?"":perDis);
            persona.put("regCon", regCon==null?"":regCon);
            persona.put("idiomId", idiomId);
            persona.put("licCond", licCond==null?"":licCond);
            persona.put("bonCaf", bonCaf==null?"":bonCaf);
            persona.put("foto",foto==null?"":foto);
            
            trabajador.put("traId", fichaEscalafonaria.getTrabajador().getTraId());
            trabajador.put("traCon", fichaEscalafonaria.getTrabajador().getTraCon());
            
            ficha.put("ficEscId", fichaEscalafonaria.getFicEscId());

            data.put("persona", persona);
            data.put("trabajador", trabajador);
            data.put("ficha", ficha);
 
        }else{
            data.put("ficEscId", fichaEscalafonaria.getFicEscId());
        }
        
        return WebResponse.crearWebResponseExito("Se listo correctamente", data);
    }
    
}
