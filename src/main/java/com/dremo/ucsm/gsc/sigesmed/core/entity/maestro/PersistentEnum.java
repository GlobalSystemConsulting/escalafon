package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro;

/**
 * Created by Administrador on 13/10/2016.
 */
public interface PersistentEnum {
    public String getValue();
}