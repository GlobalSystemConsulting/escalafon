/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.scec.cargo_comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONArray;

/**
 *
 * @author ucsm
 */
public class ListarCargosTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarCargosTx.class.getName());
       
    @Override
    public WebResponse execute(WebRequest wr) {
        return listarCargo();
    }
    private WebResponse listarCargo(){
        CargoComisionDao cargoComisionDao = (CargoComisionDao) FactoryDao.buildDao("scec.CargoComisionDao");
        try{
            List<CargoComision> cargos = cargoComisionDao.buscarTodos(CargoComision.class);
            return WebResponse.crearWebResponseExito("Exito al listar los cargos",
                    new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"carComId","nomCar","desCar"},
                            new String[]{"cod","nom","des"},cargos)));
        }catch (Exception e){
            return WebResponse.crearWebResponseError("Error al listar los cargos",WebResponse.BAD_RESPONSE);
        }

    }
}
