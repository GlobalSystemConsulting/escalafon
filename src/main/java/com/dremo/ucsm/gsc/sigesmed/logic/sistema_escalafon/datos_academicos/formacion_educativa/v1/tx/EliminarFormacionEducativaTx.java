/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.formacion_educativa.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarFormacionEducativaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarFormacionEducativaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer forEduId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            forEduId = requestData.getInt("forEduId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarFormacionEducativa",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        FormacionEducativaDao forEduDao = (FormacionEducativaDao)FactoryDao.buildDao("se.FormacionEducativaDao");
        try{
            forEduDao.delete(new FormacionEducativa(forEduId));
             ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarBajaAuditoria("Se elimino formacion edu", "ID: "+forEduId , wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la formacion educativa\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la formacion educativa", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La formacion educativa se elimino correctamente");
    }
    
}
