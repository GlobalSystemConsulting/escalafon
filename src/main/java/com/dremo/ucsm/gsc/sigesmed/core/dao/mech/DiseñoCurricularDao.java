/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.mech;


import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaCurricularHora;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.CicloEducativo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DiseñoCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.JornadaEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.ModalidadEducacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import java.util.List;

/**
 *
 * @author abel
 */

public interface DiseñoCurricularDao extends GenericDao<DiseñoCurricular>{
    
    public DiseñoCurricular buscarVigentePorOrganizacion(int organizacionID);
    public List<DiseñoCurricular> buscarConOrganizacion();
    
    public List<Nivel> buscarNiveles(int diseñoCurricularID);
    public List<Grado> buscarGrados(int diseñoCurricularID);
    public List<CicloEducativo> buscarCiclos(int diseñoCurricularID);
    public List<ModalidadEducacion> buscarModalidades(int diseñoCurricularID);
    public List<JornadaEscolar> buscarJornadaEscolar(int diseñoCurricularID);
    public List<JornadaEscolar> listarJornadaEscolarConHoras();
    
    public void insertarNivel(Nivel nivel);
    public void insertarCiclo(CicloEducativo ciclo);
    public void insertarJornadaEscolar(JornadaEscolar jornada);
    public void insertarModalidad(ModalidadEducacion modalidad);
    public void insertarArea(AreaCurricular area);
    public void insertarGrado(Grado grado);
    public void insertarGradoArea(int gradoID, int areaID, int gradoPos, int areaPos);
    public List<Object[]> listarGradoAreas();
    
    public void eliminarGradoArea(int gradoID, int areaID);
    
    public void insertarAreaHora(AreaCurricularHora areaHora);
}