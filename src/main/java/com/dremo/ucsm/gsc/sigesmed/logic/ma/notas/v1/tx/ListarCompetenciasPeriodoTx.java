package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 25/01/2017.
 */
public class ListarCompetenciasPeriodoTx implements ITransaction {
    private Logger logger = Logger.getLogger(ListarCompetenciasPeriodoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idPeriodo = data.getInt("per");
        int idArea = data.getInt("are");
        int idOrg = data.getInt("org");
        int idUser = data.getInt("usr");
        int idGrado = data.getInt("gra");
        int idPlan = data.optInt("pla", -1);
        return listarCompetenciasPeriodo(idPeriodo,idArea,idOrg,idUser,idGrado,idPlan);
    }

    private WebResponse listarCompetenciasPeriodo(int idPeriodo, int idArea, int idOrg, int idUser, int idGrado, int idPlan) {
        try{
            PlanEstudiosDao planDao = (PlanEstudiosDao) FactoryDao.buildDao("mech.PlanEstudiosDao");
            RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");

            List<CompetenciaAprendizaje> competencias = regisDao.listarCompetenciasPeriodo(idPeriodo, idArea, idOrg, idUser, idGrado,
                    idPlan == -1 ? planDao.buscarVigentePorOrganizacion(idOrg).getPlaEstId() : idPlan);
            JSONArray jsonCompetencias = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"comId","nom"},new String[]{"id","nom"},competencias
            ));
            return WebResponse.crearWebResponseExito("Se listo correcto",jsonCompetencias);

        }catch (Exception e){
            logger.log(Level.SEVERE,"listarCompetencias",e);
            return WebResponse.crearWebResponseError("No se puede listar los datos");
        }
    }
}
