package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaCapacidadDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaCapacidad;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/10/2016.
 */
public class EliminarCompetenciaBancoTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(EliminarCompetenciaBancoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int cod = data.getInt("cod");
        return eliminarCompetencia(cod);
    }
    private WebResponse eliminarCompetencia(int cod){
        try{
            CompetenciaAprendizajeDao competenciaDao = (CompetenciaAprendizajeDao) FactoryDao.buildDao("maestro.plan.CompetenciaAprendizajeDao");
            CompetenciaCapacidadDao comCapDao = (CompetenciaCapacidadDao) FactoryDao.buildDao("maestro.plan.CompetenciaCapacidadDao");

            CompetenciaAprendizaje competencia = competenciaDao.buscarCompetenciPorCodigo(cod);
            for(CompetenciaCapacidad comCap : competencia.getCapacidades()){
                comCapDao.delete(comCap);
            }
            competenciaDao.delete(competencia);
            return WebResponse.crearWebResponseExito("Error al eliminar la comptencia", WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarCompetencia",e);
            return WebResponse.crearWebResponseError("Error al eliminar la comptencia",e.getMessage());
        }
    }
}
