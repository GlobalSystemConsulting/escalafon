package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.EvaluacionesUnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.EvaluacionesUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 03/01/2017.
 */
public class RegistrarEvaluacionesUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(RegistrarEvaluacionesUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idUnidad = data.getInt("uni");
        String tipUnidad = data.getString("tipUni");
        int tipEvaluacion = data.getInt("tip");
        String des = data.optString("des","DESCRIPCION DE LA EVALUACION");
        long fec = data.optLong("fec",new Date().getTime());

        return registrarEvaluacion(idUnidad,tipUnidad, tipEvaluacion, des, fec);
    }

    private WebResponse registrarEvaluacion(int idUnidad,String tipUnidad, int tipEvaluacion, String des, long fec) {
        try{
            EvaluacionesUnidadDidacticaDao evaDao = (EvaluacionesUnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.EvaluacionesUnidadDidacticaDao");
            UnidadDidacticaDao uniDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");

            EvaluacionesUnidadDidactica evaluacion = new EvaluacionesUnidadDidactica(des,new Date(fec),tipEvaluacion);
            evaluacion.setUnidad(uniDao.buscarUnidadDidactica(idUnidad,tipUnidad));
            evaDao.insert(evaluacion);
            return WebResponse.crearWebResponseExito("Se registro la evaluacion", new JSONObject()
                    .put("id",evaluacion.getEvaUniDidId()));
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarEvaluacion",e);
            return WebResponse.crearWebResponseError("no se puede regitrar la evaluacion");
        }
    }
}
