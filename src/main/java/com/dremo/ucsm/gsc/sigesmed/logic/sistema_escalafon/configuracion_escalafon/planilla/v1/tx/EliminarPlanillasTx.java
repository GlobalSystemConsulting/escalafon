/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.planilla.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PlanillaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Planilla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarPlanillasTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarPlanillasTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer plaId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            plaId = requestData.getInt("plaId");
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminar Planilla",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        PlanillaDao planillaDao = (PlanillaDao)FactoryDao.buildDao("se.PlanillaDao");
        try{
            planillaDao.delete(new Planilla(plaId));
        }catch(Exception e){
            System.out.println("No se pudo eliminar la Planilla\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la Planilla", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La Planilla se elimino correctamente");
    }
    
}
