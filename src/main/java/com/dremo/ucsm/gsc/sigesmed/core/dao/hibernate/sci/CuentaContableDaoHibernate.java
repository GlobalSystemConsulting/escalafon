/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci;


import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.CuentaContableDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author ucsm
 */
public class CuentaContableDaoHibernate  extends GenericDaoHibernate<CuentaContable> implements CuentaContableDao {
    @Override
    public CuentaContable buscarCuentaPorNumero(String numero){
       CuentaContable cuenta = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT c FROM CuentaContable c WHERE c.estReg !='E' AND c.numCue=:p1";
            Query query = session.createQuery(hql);
            query.setString("p1", numero);
                        
            cuenta = (CuentaContable)query.uniqueResult();
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar la cuenta\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se encontrar la cuenta\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return cuenta;
    }
}
