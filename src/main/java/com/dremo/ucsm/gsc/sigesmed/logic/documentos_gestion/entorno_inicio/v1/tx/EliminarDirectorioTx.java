/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class EliminarDirectorioTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Integer ideDirectorio= 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            ideDirectorio = requestData.getInt("idDir");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("Error de envio de datos", e.getMessage() );
        }
        //Fin
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ItemFileDao iteFilDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
        //Buscamos el directorio a eliminar
        ItemFile iteFilPadre = iteFilDao.buscarPorID(ideDirectorio);
   
        //Primero Eliminamos el padre
        try {
            iteFilDao.delete(iteFilPadre);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo eliminar el directorio Padre ", e.getMessage());
        }
        //Eliminamos los hijos si los tuviera
        iteFilDao.eliminarHijosByPadre(iteFilPadre);
    
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Directorio se envio a papelera correctamente");
        //Fin
    }
    
}
