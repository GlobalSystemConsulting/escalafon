/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ClaseGenericaDAO;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.FamiliaGenericaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ClaseGenerica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.FamiliaGenerica;


/**
 *
 * @author Administrador
 */
public class FamiliaGenericaDAOHibernate extends GenericDaoHibernate<FamiliaGenerica> implements FamiliaGenericaDAO{

    @Override
    public List<FamiliaGenerica> listarFamilias(int cla_ge_id) {
        
         List<FamiliaGenerica> familias = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT DISTINCT fg FROM FamiliaGenerica fg JOIN FETCH fg.clase_generica WHERE fg.clase_generica.cla_gen_id=:p1 and fg.est_reg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1",cla_ge_id );
            familias = query.list();
            
        }catch(Exception e){
             System.out.println("No se pudo Listar las Clases Genericas \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Listar las Clases Genericas \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return familias;  
        
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public FamiliaGenerica mostrarDetalleFamilia(int fam_gem_id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
