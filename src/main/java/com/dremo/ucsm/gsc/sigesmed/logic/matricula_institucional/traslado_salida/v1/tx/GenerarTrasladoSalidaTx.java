package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_salida.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_ingreso.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mech.PlanEstudiosDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GradoSeccionPlanNivelDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.TrasladoIngresoDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.GradoIeEstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.MatriculaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.OrganizacionMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.TrasladoIngreso;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.TrasladoSalida;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.VacanteAutomatica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class GenerarTrasladoSalidaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        GenericMMIDaoHibernate<TrasladoSalida> ghTraSal = new GenericMMIDaoHibernate<>();
        GenericMMIDaoHibernate<TrasladoIngreso> ghTraIng = new GenericMMIDaoHibernate<>();
        GenericMMIDaoHibernate<MatriculaMMI> ghMatricula = new GenericMMIDaoHibernate<>();
        GenericMMIDaoHibernate<GradoIeEstudianteMMI> ghGradoIe = new GenericMMIDaoHibernate<>();
        TrasladoIngresoDaoHibernate traIngDao = new TrasladoIngresoDaoHibernate();
        MatriculaMMIDaoHibernate matdao = new MatriculaMMIDaoHibernate();

        GradoSeccionPlanNivelDaoHibernate gradoSeccion = new GradoSeccionPlanNivelDaoHibernate();
        TrasladoSalida myTrasladoSalida;
        TrasladoIngreso myTrasladoIngreso;
        Object idTraslado;
        int flagParId;
        PersonaMMI parPer, regPer;
        OrganizacionMMI orgOri, orgDes;
        int traIngId, usuMod;
        Character traSalEst;
        String traSalObs;

        MatriculaMMI myMatricula, oldMatricula;
        Object idMatricula;

        int oldGraId, oldPlaNivId, oldMatId;
        Character oldSecId;

        GradoIeEstudianteMMI gradoIe;
        Object idie;
        try {
            JSONObject requestData = (JSONObject) wr.getData();

            traIngId = requestData.getInt("traIngId");
            usuMod = requestData.getInt("usuMod");
            traSalEst = requestData.getString("traSalEst").charAt(0);
            traSalObs = requestData.getString("traSalObs");

            myTrasladoIngreso = traIngDao.getTrasladoById(traIngId);

            switch (traSalEst) {
                case 'A': {
                    myMatricula = new MatriculaMMI();

                    idMatricula = ghMatricula.llave(MatriculaMMI.class, "matId");
                    if (idMatricula != null) {
                        myMatricula.setMatId((long) idMatricula + 1);
                    } else {
                        myMatricula.setMatId(1);
                    }

                    parPer = new PersonaMMI();
                    flagParId = gradoSeccion.lastPariente(myTrasladoIngreso.getEstudiante().getPerId());
                    parPer.setPerId(flagParId);

                    regPer = new PersonaMMI();
                    regPer.setPerId(usuMod);

                    orgOri = new OrganizacionMMI();
                    orgOri.setOrgId(myTrasladoIngreso.getOrgOriId());

                    orgDes = new OrganizacionMMI();
                    orgDes.setOrgId(myTrasladoIngreso.getOrgDesId());

                    if (myTrasladoIngreso.getOrgOriId() != -1) {
                        myMatricula.setOrganizacionByOrgOriId(orgOri);
                    }

                    myMatricula.setPersonaByApoId(parPer);

                    myMatricula.setOrganizacionByOrgDesId(orgDes);
                    myMatricula.setPersonaByRegId(regPer);

                    myMatricula.setEstudiante(myTrasladoIngreso.getEstudiante());
                    myMatricula.setPlaNivId(myTrasladoIngreso.getPlaNivId());
                    myMatricula.setGraId(myTrasladoIngreso.getGraId());
                    myMatricula.setSecId(myTrasladoIngreso.getSecId());

                    myMatricula.setConMat(5); //Traslado
                    myMatricula.setMatSit(6); //Otra Traslado
                    myMatricula.setMatEst(2); //ASEPTADA
                    myMatricula.setObs(myTrasladoIngreso.getObs());

                    myMatricula.setMatFec(new Date());

                    myMatricula.setEstReg('A');
                    myMatricula.setAct(true);

                    if (myTrasladoIngreso.getEstudiante().getEstMat()) {
                        oldMatricula = matdao.getMatriculaByEstId(myTrasladoIngreso.getEstudiante().getPerId());
                        oldSecId = oldMatricula.getSecId();
                        oldGraId = oldMatricula.getGraId();
                        oldPlaNivId = oldMatricula.getPlaNivId();
                        oldMatId = (int) oldMatricula.getMatId();

                        gradoSeccion.disableEstudianteMatriculado((int) myTrasladoIngreso.getEstudiante().getPerId(), oldSecId, oldGraId, oldPlaNivId, oldMatId);

                    }

                    ghMatricula.saveOrUpdate(myMatricula);

                    gradoSeccion.ActivateEstudianteMatriculado(myTrasladoIngreso.getEstudiante().getPerId(), myTrasladoIngreso.getOrgDesId());

                    gradoIe = new GradoIeEstudianteMMI();
                    idie = ghGradoIe.llave(GradoIeEstudianteMMI.class, "graIeEstId");
                    if (idie != null) {
                        gradoIe.setGraIeEstId((long) idie + 1);
                    } else {
                        gradoIe.setGraIeEstId((long) 1);
                    }
                    gradoIe.setGraId(myTrasladoIngreso.getGraId());
                    gradoIe.setSecId(myTrasladoIngreso.getSecId());
                    gradoIe.setMatricula(myMatricula);
                    gradoIe.setAct(true);
                    gradoIe.setEstReg('A');
                    gradoIe.setEstGra('P');

                    ghGradoIe.saveOrUpdate(gradoIe);

                    //seteando el estado del traslado del ingreso
                    myTrasladoIngreso.setTraIngEst(traSalEst);
                    ghTraIng.saveOrUpdate(myTrasladoIngreso);
                    break;

                }
                case 'R': {
                    gradoSeccion.restarVacantes(usuMod, 1,
                            myTrasladoIngreso.getPlaNivId(),
                            myTrasladoIngreso.getSecId(),
                            myTrasladoIngreso.getGraId());

                    //seteando el estado del traslado del ingreso
                    myTrasladoIngreso.setTraIngEst(traSalEst);
                    ghTraIng.saveOrUpdate(myTrasladoIngreso);
                    break;
                }
            }

            myTrasladoSalida = new TrasladoSalida();
            idTraslado = ghTraSal.llave(TrasladoSalida.class, "traSalId");
            if (idTraslado != null) {
                myTrasladoSalida.setTraSalId((int) idTraslado + 1);
            } else {
                myTrasladoSalida.setTraSalId(1);
            }
            myTrasladoSalida.setEstReg('A');
            myTrasladoSalida.setFecMod(new Date());
            myTrasladoSalida.setObs(traSalObs);
            myTrasladoSalida.setTraSalEst(traSalEst);
            myTrasladoSalida.setTrasladoIngreso(myTrasladoIngreso);
            myTrasladoSalida.setUsuMod(usuMod);

            ghTraSal.saveOrUpdate(myTrasladoSalida);
            return WebResponse.crearWebResponseExito("Traslado Exitoso! ");
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error en el Proceso de Traslado! ");

        }

    }

}
