/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaGeneralDao;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
/**
 *
 * @author root
 */
public class ListarCatalogoConsultaGeneralTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *  Parte para la operacion en la Base de Datos
        */        
        JSONObject requestData = (JSONObject)wr.getData();  
        JSONArray atributos = new JSONArray();
        ConsultaGeneralDao consultaGeneralDao = (ConsultaGeneralDao)FactoryDao.buildDao("ConsultaGeneralDao");
        try{
            atributos = consultaGeneralDao.listarxCatalogo();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar el catalogo de datos  \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar el catalogo de datos ", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("Se Listo correctamente el catalogo",atributos);
    }
}
