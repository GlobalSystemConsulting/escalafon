/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.reporte.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.ma.reporte.v1.tx.ReporteListaTx;

/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
     @Override

    public WebComponent createComponent() {
        WebComponent maComponent = new WebComponent(Sigesmed.SUBMODULO_ACADEMICO);
        maComponent.setName("reportes");
        maComponent.setVersion(1);
        maComponent.addTransactionPOST("reporteLista",ReporteListaTx.class);
        //scecComponent.addTransactionGET("listarOrganizaciones",ListarOrganizacionesTx.class);
        //scecComponent.addTransactionGET("estadisticaGlobal",ListarDataEstadisticaGlobal.class);

        return maComponent;
    }
}
