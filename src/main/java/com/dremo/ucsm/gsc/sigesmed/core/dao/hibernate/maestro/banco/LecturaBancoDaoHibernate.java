package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.banco;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.banco.LecturaBancoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.Lectura;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.util.List;

/**
 * Created by Administrador on 26/12/2016.
 */
public class LecturaBancoDaoHibernate extends GenericDaoHibernate<Lectura> implements LecturaBancoDao {

    @Override
    public Lectura buscarLecturaId(int idLec) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(Lectura.class)
                    .add(Restrictions.eq("lecId",idLec))  ;
            query.setMaxResults(1);
            return (Lectura)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Lectura> listarLecturasDocente(int idDoc, int idGrad, Character idSecc, int idArea, int idOrg, int year) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(Lectura.class)
                    .createAlias("banco", "b", JoinType.INNER_JOIN)
                    .add(Restrictions.eq("estReg", 'A'))
                    .add(Restrictions.eq("b.temp", year));
            query.createCriteria("b.pro",JoinType.INNER_JOIN).add(Restrictions.eq("usuId",idDoc));
            query.createCriteria("b.grado",JoinType.INNER_JOIN).add(Restrictions.eq("graId",idGrad));
            query.createCriteria("b.seccion",JoinType.INNER_JOIN).add(Restrictions.eq("sedId",idSecc));
            query.createCriteria("b.area",JoinType.INNER_JOIN).add(Restrictions.eq("areCurId",idArea));
            query.createCriteria("b.organizacion",JoinType.INNER_JOIN).add(Restrictions.eq("orgId",idOrg));
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
