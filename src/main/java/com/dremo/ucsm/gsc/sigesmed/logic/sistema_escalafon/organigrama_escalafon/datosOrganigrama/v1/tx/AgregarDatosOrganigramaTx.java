/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.datosOrganigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DatosOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.DatosOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class AgregarDatosOrganigramaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarDatosOrganigramaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        DatosOrganigrama datosOrganigrama=null;
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            String codDat = requestData.optString("codDat");
            String nomDat = requestData.optString("nomDat");
            String desDat = requestData.optString("desDat");
            int anioDat= requestData.optInt("anioDat");
            
            datosOrganigrama =new DatosOrganigrama();
            datosOrganigrama.setCodDatOrgi(codDat);
            datosOrganigrama.setNomDatOrgi(nomDat);
            datosOrganigrama.setDesDatOrgi(desDat);
            datosOrganigrama.setAnioDatOrgi(anioDat);
            datosOrganigrama.setEstReg('A');
            datosOrganigrama.setFecMod(new Date());
            datosOrganigrama.setUsuMod(wr.getIdUsuario());
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"nuevos datos organigrama",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        DatosOrganigramaDao datOrganigramaDao = (DatosOrganigramaDao) FactoryDao.buildDao("se.DatosOrganigramaDao");
        try {
            datOrganigramaDao.insert(datosOrganigrama);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva datos organigrama",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("datId", datosOrganigrama.getDatOrgiId());
        oResponse.put("codDat", datosOrganigrama.getCodDatOrgi());
        oResponse.put("nomDat", datosOrganigrama.getNomDatOrgi());
        oResponse.put("desDat", datosOrganigrama.getDesDatOrgi());
        oResponse.put("anioDat", datosOrganigrama.getAnioDatOrgi());
        
       
                
        return WebResponse.crearWebResponseExito("El registro datos Organigrama se realizo correctamente", oResponse);

    }
    
}
