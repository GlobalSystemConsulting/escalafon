/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.ascenso.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarAscensosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarAscensosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<Ascenso> ascensos = null;
        Organigrama organigrama = new Organigrama();
        Cargo cargo = new Cargo();
        Categoria categoria = new Categoria();
        Ubicacion ubicacion = new Ubicacion();
        
        AscensoDao ascensoDao = (AscensoDao)FactoryDao.buildDao("se.AscensoDao");
        OrganigramaDao organigramaDao =(OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
        CategoriaDao categoriaDao =(CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
        CargoDao cargoDao =(CargoDao)FactoryDao.buildDao("se.CargoDao");
        UbicacionDao ubicacionDao =(UbicacionDao)FactoryDao.buildDao("se.UbicacionDao");
        
        try{
            ascensos = ascensoDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar ascensos",e);
            System.out.println("No se pudo listar los ascensos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los ascensos", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Ascenso a: ascensos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("ascId", a.getAscId());
            oResponse.put("tip", a.getTip());
            oResponse.put("tipDocId", a.getTipDocId());
            oResponse.put("numDoc", a.getNumDoc()); 
            oResponse.put("fecDoc", a.getFecDoc());
            oResponse.put("fecEfe", a.getFecEfe());
            oResponse.put("mot", a.getMot());
            //Historico
            if(a.getOrgiAntId()!=null && a.getOrgiAntId()>0){
                oResponse.put("orgiAntId", a.getOrgiAntId());
                organigrama = organigramaDao.obtenerDetalle(a.getOrgiAntId());
                oResponse.put("datOrgAntId", organigrama.getTipoOrganigrama().getDatosOrganigramas().getDatOrgiId());
                oResponse.put("nomAntOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getNomDatOrgi());
                oResponse.put("nomAntAnioOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getAnioDatOrgi());

                oResponse.put("nomAntOrgiN3", organigrama.getNomOrgi());
                oResponse.put("nomAntOrgiN2", organigrama.getOrganigramaPadre().getNomOrgi());
                oResponse.put("nomAntOrgiN1", organigrama.getOrganigramaPadre().getOrganigramaPadre().getNomOrgi());
                
                if (organigrama.getUbiId() != null){
                    ubicacion = ubicacionDao.buscarPorId(organigrama.getUbiId());
                    oResponse.put("ubiAntId",ubicacion.getUbiId());
                    oResponse.put("nomAntUbi",ubicacion.getNom());
                }else{
                    oResponse.put("ubiAntId",0);
                    oResponse.put("nomAntUbi","");
                }
            }else{
                oResponse.put("orgiAntId", 0);
                oResponse.put("datOrgAntId", 0);
                oResponse.put("nomAntOrgi", "");
                oResponse.put("nomAntAnioOrgi", 0);
                oResponse.put("nomAntOrgiN3", "");
                oResponse.put("nomAntOrgiN2", "");
                oResponse.put("nomAntOrgiN1", "");
                oResponse.put("ubiAntId", 0);
                oResponse.put("nomAntUbi","");
            }
            
            if(a.getCatAntId()!=null && a.getCatAntId()>0){
                categoria=categoriaDao.obtenerDetalle(a.getCatAntId());
                oResponse.put("catAntId", a.getCatAntId());
                oResponse.put("nomAntCat", categoria.getNomCat());
                oResponse.put("nomAntPla", categoria.getPlanilla().getNomPla());
            }
            else{
                oResponse.put("catAntId", 0);
                oResponse.put("nomAntCat", "");
                oResponse.put("nomAntPla", "");
            }

            if(a.getCarAntId()!=null && a.getCarAntId()>0){
                cargo=cargoDao.buscarPorId(a.getCarAntId());
                oResponse.put("carAntId", a.getCarAntId());
                oResponse.put("nomAntCar", cargo.getNomCar());            
            }
            else{
                oResponse.put("carAntId", 0);
                oResponse.put("nomAntCar", "");
            }
            
            //post
            if(a.getOrgiPostId()!=null && a.getOrgiPostId()>0){
                oResponse.put("orgiPostId", a.getOrgiPostId());

                organigrama = organigramaDao.obtenerDetalle(a.getOrgiPostId());
                oResponse.put("datOrgPostId", organigrama.getTipoOrganigrama().getDatosOrganigramas().getDatOrgiId());
                oResponse.put("nomPostOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getNomDatOrgi());
                oResponse.put("nomPostAnioOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getAnioDatOrgi());

                oResponse.put("nomPostOrgiN3", organigrama.getNomOrgi());
                oResponse.put("nomPostOrgiN2", organigrama.getOrganigramaPadre().getNomOrgi());
                oResponse.put("nomPostOrgiN1", organigrama.getOrganigramaPadre().getOrganigramaPadre().getNomOrgi());
                
                if (organigrama.getUbiId() != null){
                    ubicacion = ubicacionDao.buscarPorId(organigrama.getUbiId());
                    oResponse.put("ubiPostId",ubicacion.getUbiId());
                    oResponse.put("nomPostUbi",ubicacion.getNom());
                }else{
                    oResponse.put("ubiPostId",0);
                    oResponse.put("nomPostUbi","");
                }
            }else{
                oResponse.put("orgiPostId", 0);
                oResponse.put("datPostOrgId", 0);
                oResponse.put("nomPostOrgi", "");
                oResponse.put("nomPostAnioOrgi", 0);
                oResponse.put("nomPostOrgiN3", "");
                oResponse.put("nomPostOrgiN2", "");
                oResponse.put("nomPostOrgiN1", "");
                oResponse.put("ubiPostId", 0);
                oResponse.put("nomPostUbi","");
            }
            
            if(a.getCatPostId()!=null && a.getCatPostId()>0){
                categoria=categoriaDao.obtenerDetalle(a.getCatPostId());
                oResponse.put("catPostId", a.getCatPostId());
                oResponse.put("nomPostCat", categoria.getNomCat());
                oResponse.put("nomPostPla", categoria.getPlanilla().getNomPla());
            }
            else{
                oResponse.put("catPostId", 0);
                oResponse.put("nomPostCat", "");
                oResponse.put("nomPostPla", "");
            }

            if(a.getCarPostId()!=null && a.getCarPostId()>0){
                cargo=cargoDao.buscarPorId(a.getCarPostId());
                oResponse.put("carPostId", a.getCarPostId());
                oResponse.put("nomPostCar", cargo.getNomCar());            
            }
            else{
                oResponse.put("carPostId", 0);
                oResponse.put("nomPostCar", "");
            }
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los ascensos fueron listados exitosamente", miArray);
    }
    
}
