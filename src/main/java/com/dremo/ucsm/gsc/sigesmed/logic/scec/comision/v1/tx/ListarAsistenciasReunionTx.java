package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.AsistenciaReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.AsistenciaReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 09/10/16.
 */
public class ListarAsistenciasReunionTx implements ITransaction {
    private static  final Logger logger = Logger.getLogger(ListarAsistenciasReunionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String com = wr.getMetadataValue("cod");
        return listarAsistencias(Integer.parseInt(com));
    }

    private WebResponse listarAsistencias(int reu) {
        try {
            AsistenciaReunionComisionDao asistenciaDao = (AsistenciaReunionComisionDao) FactoryDao.buildDao("scec.AsistenciaReunionComisionDao");
            List<AsistenciaReunionComision> asistencias = asistenciaDao.buscarAsistenciasPorReunion(reu);
            JSONArray arryData = new JSONArray();
            for(AsistenciaReunionComision asistencia :asistencias){
                Persona persona = asistencia.getIntegranteComision().getPersona();
                JSONObject jsonIntegrante = new JSONObject(
                        EntityUtil.objectToJSONString(
                                new String[]{"perId","dni","nom","apeMat","apePat"},
                                new String[]{"cod","dni","nom","apem","apep"},
                                persona)
                );
                jsonIntegrante.put("est",asistencia.getEst());
                arryData.put(jsonIntegrante);
            }
            return WebResponse.crearWebResponseExito("Se listaron la asistencia de los participantes",WebResponse.OK_RESPONSE)
                    .setData(arryData);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarAsistencias",e);
            return WebResponse.crearWebResponseError("No se pudieron listar las asistencias",WebResponse.BAD_RESPONSE);
        }
    }
}
