package com.dremo.ucsm.gsc.sigesmed.logic.maestro.banco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.banco.BancoLecturaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.banco.LecturaBancoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.BancoLectura;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.BancoLecturaDocente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.Lectura;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 26/12/2016.
 */
public class RegistrarLecturaTx implements ITransaction {
    private static Logger logger = Logger.getLogger(RegistrarLecturaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        ///datos del banco
        int idDoc = data.getInt("doc");
        int idOrg = data.getInt("org");
        int idGrad = data.getInt("grad");
        int idAre = data.getInt("are");
        Character idSec = data.getString("sec").charAt(0);
        int year = Calendar.getInstance().get(Calendar.YEAR);
        //datos de la lectura
        String noml = data.getString("noml");
        String nom = data.optString("nom");
        JSONObject filjson = data.optJSONObject("arc");
        String aut = data.optString("aut");
        String des = data.optString("des");
        String tip = data.optString("tip");
        long fpub = data.optLong("fpub", new Date().getTime());
        Date fecCre = new Date();

        return registrarLectura(idDoc,idOrg,idGrad,idAre,idSec,year,nom,noml,filjson,aut,des,tip,fpub,fecCre);
    }

    private WebResponse registrarLectura(int idDoc, int idOrg, int idGrad, int idAre, Character idSec,int year,String nom, String noml, JSONObject filjson, String aut, String des, String tip, long fpub, Date fecCre) {
        try{
            BancoLecturaDao bancoDao = (BancoLecturaDao) FactoryDao.buildDao("maestro.banco.BancoLecturaDao");
            LecturaBancoDao lecturaDao = (LecturaBancoDao) FactoryDao.buildDao("maestro.banco.LecturaBancoDao");
            BancoLecturaDocente banco = bancoDao.buscarBancoLectura(idDoc,idOrg,idGrad,idAre,idSec,year);
            //registramos el banco si no existe
            if(banco == null){
                banco = new BancoLecturaDocente(fecCre,"",year);
                banco.setPro(bancoDao.buscarUsuario(idDoc));
                banco.setOrganizacion(bancoDao.buscarOrganizacion(idOrg));
                banco.setGrado(bancoDao.buscarGrado(idGrad));
                banco.setArea(bancoDao.buscarArea(idAre));
                banco.setSeccion(bancoDao.buscarSeccion(idSec));

                bancoDao.insert(banco);
            }
            Lectura lectura = new Lectura(noml,aut,des,tip,fecCre,new Date(fpub));
            lectura.setUbi(nom);
            lectura.setBanco(banco);
            lecturaDao.insert(lectura);
            if(filjson != null && filjson.length() > 0){
                FileJsonObject miF = new FileJsonObject( filjson ,"banc_" + banco.getBanLecId() +"_lect_"+ lectura.getLecId() + "_" );
                BuildFile.buildFromBase64("banco_lectura_docente", miF.getName(), miF.getData());
                lectura.setUbi(miF.getName());
                lecturaDao.update(lectura);
            }
            JSONObject result = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"lecId","ubi"},
                    new String[]{"id","nom"},
                    lectura
            ));
            result.put("url", Sigesmed.UBI_ARCHIVOS+ File.separator +"banco_lectura_docente" + File.separator);
            return WebResponse.crearWebResponseExito("Se registro correctamente la lectura",result);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarLectura",e);
            return WebResponse.crearWebResponseError("No se pudo registrar la lectura");
        }
    }
}
