/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaFichaInstitucionalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaTipo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class RegistrarPlantillaTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */                
        JSONObject requestData = (JSONObject)wr.getData();
        JSONArray grupos = requestData.getJSONArray("grupos");
        
        String codigo = "";
        String nombre = requestData.optString("nombre");
        int tipo = requestData.optInt("tipo");
        
        PlantillaFichaInstitucionalDao plantillaDao = (PlantillaFichaInstitucionalDao)FactoryDao.buildDao("smdg.PlantillaFichaInstitucionalDao");
        
        Object o = plantillaDao.llave(PlantillaFichaInstitucional.class, "pfiInsId");
        
        codigo =  nombre.substring(0, 3)+ "" + ((Integer)o + 1) +""+ tipo;
                
        PlantillaFichaInstitucional plantilla = new PlantillaFichaInstitucional(codigo, nombre, new Date(), new PlantillaTipo(tipo));
        
        List<PlantillaGrupo> gruposList = new ArrayList<>();
        
        PlantillaGrupo grupo = null;

        for(int i = 0; i < grupos.length(); ++i){
            grupo = new PlantillaGrupo(grupos.getJSONObject(i).optString("gruNom"), new TipoGrupo(grupos.getJSONObject(i).optInt("Tipo")), plantilla);

            JSONArray indicadores = grupos.getJSONObject(i).getJSONArray("indicadores");
            List<PlantillaIndicadores> indicadoresList = new ArrayList<>();
            
            PlantillaIndicadores indicador = null;
            for(int j = 0; j < indicadores.length(); ++j){
                indicador = new PlantillaIndicadores(indicadores.getJSONObject(j).optString("indNom"), grupo);

                indicadoresList.add(indicador);
                
            }
            grupo.setIndicadores(indicadoresList);
            gruposList.add(grupo);
        }
        
        plantilla.setGrupos(gruposList);
        plantillaDao.insert(plantilla);
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();            
        oResponse.put("plaId",plantilla.getPfiInsId());
        
        return WebResponse.crearWebResponseExito("El registro la plantilla de Ficha", oResponse);
    }
}
