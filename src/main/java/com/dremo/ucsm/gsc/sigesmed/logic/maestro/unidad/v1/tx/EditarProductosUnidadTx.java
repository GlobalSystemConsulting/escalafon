package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.MaterialRecursoProgramacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProductosUnidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/12/2016.
 */
public class EditarProductosUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EditarProductosUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idProducto = data.getInt("id");
        return editarMaterialUnidad(idProducto,data);
    }

    private WebResponse editarMaterialUnidad(int idProducto, JSONObject data) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            ProductosUnidadAprendizaje producto = new ProductosUnidadAprendizaje();
            producto.setProUniAprId(idProducto);
            producto.setNom(data.optString("nom"));
            producto.setFecMod(new Date());

            unidadDao.editarProductoUnidad(producto);
            return WebResponse.crearWebResponseExito("Se edito correctamente el producto");
        }catch (Exception e){
            return WebResponse.crearWebResponseError("error al editar el producto");
        }
    }
}
