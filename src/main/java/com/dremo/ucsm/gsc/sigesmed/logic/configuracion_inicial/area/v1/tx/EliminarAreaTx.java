/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.area.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.AreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author Administrador
 */
public class EliminarAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int areaID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            areaID = requestData.getInt("areaID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        AreaDao areaDao = (AreaDao)FactoryDao.buildDao("AreaDao");
        try{
            areaDao.delete(new Area(areaID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Area\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Area", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Area se elimino correctamente");
        //Fin
    }
}
