package com.dremo.ucsm.gsc.sigesmed.core.dao.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.AsistenciaReunionComision;

import java.util.List;

/**
 * Created by geank on 28/09/16.
 */
public interface AsistenciaReunionComisionDao extends GenericDao<AsistenciaReunionComision> {
    List<AsistenciaReunionComision> buscarAsistenciasPorReunion(int idReunion);
    AsistenciaReunionComision buscarAsistenciaPorReunion(int idReunion,int idPersona);
}
