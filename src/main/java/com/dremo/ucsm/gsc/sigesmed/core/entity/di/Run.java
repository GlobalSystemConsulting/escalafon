package com.dremo.ucsm.gsc.sigesmed.core.entity.di;

import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.MChart;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.TipoParienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di.ApoderadoDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di.TrabajadorDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import com.itextpdf.layout.element.Table; 
import java.io.FileOutputStream;
import org.apache.commons.codec.binary.Base64;
import org.jfree.data.general.DefaultPieDataset;


/**
 *
 * @author John
 */
public class Run {
 
    private static SessionFactory sessionFactory = null;
 
    public static void main(String[] args) throws IOException, Exception {
//        Session session = null;
//        
//                sessionFactory = HibernateUtil.getSessionFactory();
//                session = sessionFactory.openSession();
// 
//                System.out.println("Insertando registro");
//                Transaction tx = session.beginTransaction();
//                //Creando un Objeto
//                //Usuario p = new Usuario(4,  "a", "b", new Date(), 1, 'A');               
//                //CargoPersonal c = new CargoPersonal("Nombre2",'B');
//                /*HorarioPersonal hp = new HorarioPersonal(2);
//                Short a = 20;
//                Set<PersonalInstitucion> listaPer = new HashSet<>();
//                PersonalInstitucion pi = new PersonalInstitucion(2, a);
//                listaPer.add(pi);
//                hp.setPersonalInstitucions(listaPer);
//                */
//                //Guardando
//                //ParientesPK pk = new ParientesPK(1, 3);
//                
////                Apoderado a = new Apoderado();                
//                //DirectorioExterno d =new DirectorioExterno("Carlos Ramirez Santos", "Avenida 1234");
//                Persona p = new Persona("00000345", "Muños", "Rondon", "Carlos", new Date(), 2345456);
////                a.setPersona(p);
//                //a.setPersona(p);
//                
//                                
//            try {
//                session.save(p);                
//                //session.save(p2);
//                tx.commit();                
//                System.out.println("Finalizado...");
//            } catch (Exception e) {
//                //tx.rollback();
//                System.out.println(e.getMessage());
//            }
//        
//            session.close();
        
//        TrabajadorDaoHibernate t = new TrabajadorDaoHibernate();
//        List<Trabajador> trabajadores= t.ListarxOrganizacionxTipo(1,"A");
//        
//        System.err.println("Hola:");
//        for(Trabajador trabajador:trabajadores){
//            System.err.println(trabajador.getTraCar() + " " + trabajador.getPersona().getApeMat());
//        }
        
//        ApoderadoDaoHibernate a = new ApoderadoDaoHibernate();
//        List<Apoderado> apos= a.datosApoderado();
//        
//        System.err.println("Hola:");
//        for(Apoderado apo:apos){
//            System.err.println(apo.getOcu()+ " " + apo.getPersona().getApePat());
//        }
        
//        EstudianteDaoHibernate e = new EstudianteDaoHibernate();
//        List<Estudiante> edus= e.datosEstudiante();
//        
//        System.err.println("Hola:");
//        for(Estudiante edu:edus){
//            System.err.println(edu.getGraAct()+ " " + edu.getPersona().getApeMat());
//        }
        
        MatriculaDaoHibernate m = new MatriculaDaoHibernate();
        List<Matricula> matriculas = m.MatriculasxApoderado(6);
        
        System.err.println("Hola:");
        for(Matricula matricula:matriculas){
            System.err.println(matricula.getMatId() + " " + matricula.getApoderado().getOcu() + " " + matricula.getApoderado().getPersona().getApeMat() + " " + matricula.getOrganizacion().getNom());
        }
        
//        MatriculaDaoHibernate m = new MatriculaDaoHibernate();
//        List<Matricula> matriculas = m.MatriculasxEstudiante(1);
//        
//        System.err.println("Hola:");
//        for(Matricula matricula:matriculas){
//            System.err.println(matricula.getMatId() + " " + matricula.getEstudiante().getPersona().getApeMat());
//        }
        
//        ParientesDaoHibernate p = new ParientesDaoHibernate();
//        List<Parientes> parientes = p.listarParientesxTrabajador(9);
//        
//        System.err.println("Hola:");
//        for(Parientes pariente:parientes){
//            System.err.println(pariente.getPariente().getNom() + " " + pariente.getPariente().getApePat() + " " + pariente.getParentesco().getTpaDes());        
//        }
//        
//        PersonaDaoHibernate p = new PersonaDaoHibernate();
//        Persona persona = p.buscarPersonaxDNI(657342311);
//
//        System.err.println("HOLA -- " + persona);
//        System.err.println(persona.getApePat() + " " + persona.getNom());
        
//        Session session = null;
//        
//        sessionFactory = HibernateUtil.getSessionFactory();
//        session = sessionFactory.openSession();
//
//        System.out.println("Insertando registro");
//        Transaction tx = session.beginTransaction();
//        
//        //Parientes p = new Parientes("Tito", "Sanchez", "Dir 54", 723542, "Padre", new Trabajador(11));
//        
//        Parientes pa = new Parientes(7, 1, new TipoPariente(5));
//        Persona pe = new Persona(1L, "Osorio", "Murga", "Armando", new Date(), 65734222);
////        Persona pe = (Persona)session.load(Persona.class, 1L);
////        pe.setApeMat("Osorio");
////        pe.setApePat("Murga");
////        pe.setNom("Armando");
////        pe.setDni(65734233);
//                
//        pa.setPersona(pe);
//        
//        try {
//            session.update(pa);           
//            //session.update(pe);
//            
//            //session.save(p2);
//            tx.commit();                
//            System.out.println("Finalizado...");
//        } catch (Exception e) {
//            tx.rollback();
//            System.out.println(e.getMessage());
//        }

//        session.close();
        
        //Trabajador t = (Trabajador) session.load(Trabajador.class, 17);
//        short s = 10;
//        Date d = new Date();
        //Trabajador t = new Trabajador(19, new Persona(19L), new Organizacion(1));
        
        //List<Parientes> parientes = new ArrayList<Parientes>();
//        parientes.add(new Parientes("Jonatan", "Guti", "Direccion 11", 456763, "Hermmano"));
//        t.setParientes(parientes);
//        Parientes p = new Parientes("Uber", "Ramos", "direccion 3455", 456234, "Hijo",new Trabajador(17)); 
                  
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        parientes.add(new Parientes("Ronald", null, null, 567567, "Hijo"));
                
        
        //t.setPersona(new Persona(9L));
//        List<Parientes> parientes = new ArrayList<Parientes>();
//        
//        parientes.add(new Parientes("Ronald", null, null, 567567, "Hijo"));
//        parientes.add(new Parientes("Carol", null, null, 456234, "Hija"));
//        t.setParientes(parientes);
//        new Parientes
        
//        Session session = null;
//        
//        sessionFactory = HibernateUtil.getSessionFactory();
//        session = sessionFactory.openSession();
//        Transaction tx = session.beginTransaction();
//        
//        Parientes p = (Parientes)session.load(Parientes.class, 3);//new Parientes(3);
//        p.setEstReg("A");
//        
//        try {
//                session.update(p);                
//                //session.save(p2);
//                tx.commit();                
//                System.out.println("Finalizado...");
//            } catch (Exception e) {
//                tx.rollback();
//                System.out.println(e.getMessage());
//            }
//        
//        session.close();
        
//        DirectorioExternoDaoHibernate d = new DirectorioExternoDaoHibernate();
//        List<DirectorioExterno> dex= d.buscarTodos(DirectorioExterno.class);
//        //d.delete(new DirectorioExterno(4));
//        
//        System.err.println("Hola:");
//        for(DirectorioExterno e:dex){
//            System.err.println(e.getDexNombre()+" "+e.getDexDir());
//        }
        
//        Session session = null;
//        
//        sessionFactory = HibernateUtil.getSessionFactory();
//        session = sessionFactory.openSession();
//        Transaction tx = session.beginTransaction();
//        
//        List<Parientes> parientes = null;
//        
//        try {
//            String hql = "SELECT pa FROM Trabajador t "
//                    + "join fetch Parientes pa on t.per_id = pa.per_id "
//                    + "WHERE t.traId=:p1 AND p.estReg = 'A'";
//            Query query = session.createQuery(hql);
//            query.setParameter("p1", 9);
//            parientes = query.list();
//            } catch (Exception e) {
//                tx.rollback();
//                System.out.println(e.getMessage());
//            }
//        
//        session.close();
//        
//        for(Parientes p:parientes) 
//            System.err.println(p.getPariente().getNom() +" "+p.getPariente().getApePat());
        
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        Transaction tx = session.beginTransaction();
//        
//        Long parId = 13L,
//             perId = 5L;
//        int tpaId = 4,
//            parDni = 45672312;
//        String parMat = "Gonzales",
//               parPat = "Daza",
//               parNom = "Tito",
//               parDir = "Direccion 111",
//               parTel = "441155";               
//        
//        Persona pe = null;
//
//        //si no existe el pariente en la tabla persona
//        if(parId == 0L){            
//            pe = new Persona(parMat, parPat, parNom, parDni, parTel, parDir);               
//            parId = insertarPersona(pe);
//        }            
//        else{            
//            pe = (Persona)session.load(Persona.class, parId);//new Parientes(3);
//            pe.setApeMat(parMat);
//            pe.setApePat(parPat);
//            pe.setNom(parNom);
//            pe.setPerDir(parDir);
//            pe.setFij(parTel);
//            pe.setDni(parDni);            
//            //actualizarPersona(pe);
//            
//            try {
//               session.update(pe);
//               tx.commit();
//            }catch (Exception e) {
//               tx.rollback();
//            }
//            
//        }
//
//        ParientesDao parienteDao = (ParientesDao)FactoryDao.buildDao("di.ParientesDao");
//        Parientes pa = new Parientes(parId, perId, new TipoPariente(tpaId));
//        tx = session.beginTransaction();
//        
//        try{
//            parienteDao.insert(pa);
//            tx.commit();
//        }catch(Exception e){            
//            tx.rollback();
//            System.out.println("No se pudo registrar pariente " + e.getMessage() );
//        }
//        finally{
//            session.close();
//        }
        
//        ParientesDaoHibernate p = new ParientesDaoHibernate();
//        List<Parientes> parientes = p.listarParientesxTrabajador(9);          
//        
////        Mitext m = new Mitext("hello_world15.pdf", new int[]{20,20,20,20}, true, "NUEVO ENCABEZADO");        
//        Mitext m = new Mitext();
//        m.agregarParrafo("esta es una prueba");
//        m.agregarParrafo("esta es una prueba");
//        m.agregarParrafo("esta es una prueba");
//        
////        agregar tabla
//        GTabla t = new GTabla(new float[]{3,4,5});
//        
//        t.build(new String[]{"Nombre", "Apellido", "Parentesco"});
//        
//        for(Parientes pariente:parientes){
//            System.err.println(pariente.getPariente().getNom() + " " + pariente.getPariente().getApePat() + " " + pariente.getParentesco().getTpaDes());        
//            t.processLine(new String[]{pariente.getPariente().getNom(), pariente.getPariente().getApePat(), pariente.getParentesco().getTpaDes()});
//        }
//        
//        m.agregarTabla(t);
//        
////        agregar grafico
//        
//        MChart chart = new MChart();
//        
//        DefaultPieDataset dataset = new DefaultPieDataset( );
//        dataset.setValue( "IPhone 5s" , new Double( 20 ) );  
//        dataset.setValue( "SamSung Grand" , new Double( 20 ) );   
//        dataset.setValue( "MotoG" , new Double( 40 ) );    
//        dataset.setValue( "Nokia Lumia" , new Double( 10 ) );  
//        
//        m.agregarGrafico(chart.createPieChart(dataset, "Grafica de Ejemplo"), 400, 300);
        
//        m.cerrarDocumento();
//        
//        System.err.println("Hola=========================================");
//        System.err.println(new String(m.getBaos().toByteArray()));
//        
//        
//        System.err.println("Hola=========================================");
//        System.err.println(Base64.encodeBase64String(m.getBaos().toByteArray()));
//        
//        byte fileBinary[] = Base64.decodeBase64(Base64.encodeBase64URLSafeString(m.getBaos().toByteArray())); 
//        FileOutputStream file = new FileOutputStream("ReportesTemp/"+"/"+"helloworld11.pdf");
//        file.write(fileBinary);
//        
//        file.close();
        
    }
    public static Long insertarPersona(Persona p){
        Long perId = 0L;
        
        PersonaDao pDao = (PersonaDao)FactoryDao.buildDao("di.PersonaDao");
        try{
            pDao.insert(p);        
            perId = p.getPerId();
        }catch(Exception e){            
            System.out.println("No se pudo registrar: " + e.getMessage());
        }        
        
        return perId;
    }
    
    
}
