/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_complementario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class EliminarEstudioComplementarioTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarEstudioComplementarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer estComId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            estComId = requestData.getInt("estComId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarEstudioComplementario",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        EstudioComplementarioDao estComDao = (EstudioComplementarioDao)FactoryDao.buildDao("se.EstudioComplementarioDao");
        try{
            estComDao.delete(new EstudioComplementario(estComId));
             ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarBajaAuditoria("Se elimino estudio complem", "ESTCOMP_ID: "+estComId , wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        }catch(Exception e){
            System.out.println("No se pudo eliminar el estudio complementario\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el estudio complementario", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El estudio complementario se elimino correctamente");
    }
    
}
