/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarBienMuebleTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int bie_mue_id = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            bie_mue_id = requestData.getInt("cod_bie");
            
            BienesMueblesDAO bienes_dao = (BienesMueblesDAO)FactoryDao.buildDao("scp.BienesMueblesDAO");  
            
            bienes_dao.eliminar_bien(bie_mue_id);
            
            
        }catch(Exception e){
             System.out.println(e);
             return  WebResponse.crearWebResponseError("No se pudo eliminar el Bien Mueble, datos incorrectos", e.getMessage() );
        }
        
        return WebResponse.crearWebResponseExito("El Bien Mueble se elimino correctamente");
        
        
        
        
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
}
