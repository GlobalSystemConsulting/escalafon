package com.dremo.ucsm.gsc.sigesmed.logic.maestro.acomp.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.acomp.AcompanamientoDocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.AreaCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.acomp.AcompanamientoDocente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.acomp.CompromisosAcompanamiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 06/01/2017.
 */
public class EditarAcompanamientoTx implements ITransaction{
    private static Logger logger = Logger.getLogger(RegistrarAcompanamientoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idAcomp = data.getInt("id");
        int idNivel = data.optInt("niv",-1);
        int idGrado = data.optInt("gra",-1);
        char idSeccion = data.optString("sec"," ").charAt(0);
        int idArea = data.optInt("are",-1);
        int res = data.optInt("res");
        long fec = data.optLong("fec", new Date().getTime());
        JSONArray compromisosJSON = data.optJSONArray("comp");
        return editarAcompanamiento(idAcomp, idNivel, idGrado, idSeccion, idArea, res, fec, compromisosJSON);
    }

    private WebResponse editarAcompanamiento(int idAcomp, int idNivel, int idGrado, char idSeccion, int idArea, int res, long fec, JSONArray compromisosJSON) {

        try{
            AcompanamientoDocenteDao acomDao = (AcompanamientoDocenteDao) FactoryDao.buildDao("maestro.acomp.AcompanamientoDocenteDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            AreaCurricularDao areDao = (AreaCurricularDao) FactoryDao.buildDao("maestro.plan.AreaCurricularDao");

            AcompanamientoDocente acomp = acomDao.buscarAcompanamiento(idAcomp);

            acomp.setNivel(idNivel != -1 ? docDao.buscarNivelPorID(idNivel): acomp.getNivel());
            acomp.setGrado(idGrado != -1 ? docDao.buscarGradoPorID(idGrado) : acomp.getGrado());
            acomp.setSeccion(idSeccion != ' ' ? docDao.buscarSeccionPorID(idSeccion) : acomp.getSeccion());
            acomp.setArea(idArea!= -1 ? areDao.buscarPorId(idArea):acomp.getArea());
            acomp.getCompromisos().clear();
            acomDao.update(acomp);
            if(compromisosJSON != null){
                for(int i = 0; i < compromisosJSON.length();i++){
                    JSONObject compromisoJSON = compromisosJSON.getJSONObject(i);
                    String des = compromisoJSON.optString("des");
                    boolean est = compromisoJSON.optBoolean("est");
                    CompromisosAcompanamiento compromiso = new CompromisosAcompanamiento(des,est);
                    compromiso.setAcomp(acomp);
                    acomp.getCompromisos().add(compromiso);
                }
                acomDao.update(acomp);
            }
            JSONObject acompJSON = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"acoId", "fec", "res"},
                    new String[]{"id", "fec", "res"},
                    acomp
            ));

            return WebResponse.crearWebResponseExito("Se listo correctamente los acompanamietos",acompJSON);

        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarAcompanamiento",e);
            return WebResponse.crearWebResponseError("No se puede registrar el acompanamiento");
        }
    }
}
