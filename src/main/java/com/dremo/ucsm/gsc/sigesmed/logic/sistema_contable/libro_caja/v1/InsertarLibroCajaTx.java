/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;


import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaCorriente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentasEfectivo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Empresa;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.HechosLibro;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class InsertarLibroCajaTx implements ITransaction{   
    
    @Override    
    public WebResponse execute(WebRequest wr)  {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        LibroCaja nuevoLibro = null;
    
        try{            
             
            JSONObject requestData = (JSONObject)wr.getData();                                
       
            String nombre = requestData.getString("nombre");          
            String observacion = requestData.getString("observacion");           
            String feA = requestData.getString("fechaA");  
            String feC = requestData.getString("fechaC");                                                          
                       
            double saldoActual = requestData.optDouble("saldoActual",0);          
            int organizacionID = requestData.getInt("organizacionID"); 
            int personaID = requestData.getInt("personaID");  
            String estado = requestData.getString("estado"); 
            
            JSONArray listcuentasE= requestData.getJSONArray("cuentasEfectivo");
         
            
            nuevoLibro = new LibroCaja(0,new Organizacion(organizacionID),new Persona(personaID),nombre,observacion,new Date(feA),new Date(feC),new BigDecimal(saldoActual),new BigDecimal(saldoActual),new Date(), wr.getIdUsuario(),'I');
              if(listcuentasE.length() > 0){
               
                for(int i = 0, j=0; i < listcuentasE.length();i++){ 
                    JSONObject bo = listcuentasE.getJSONObject(i);
                    
                    int cuentaContableID = bo.getInt("cuentaContableID");
                    double importe = bo.getDouble("importe");
                    
                    nuevoLibro.getCuentasEfectivos().add(new CuentasEfectivo((short)(i+1),nuevoLibro,new CuentaContable(cuentaContableID),new BigDecimal(importe),new BigDecimal(importe),new Date(),wr.getIdUsuario(),'A'));
                    nuevoLibro.getHechosLibros().add(new HechosLibro((short)(i+1),nuevoLibro,new CuentaContable(cuentaContableID),new Date(),new BigDecimal(importe),new BigDecimal(BigInteger.ZERO),new Date(),wr.getIdUsuario(),'A'));
                                              
                    boolean flag= bo.optBoolean("f",false);
                    if(flag){
                        int bancoID = bo.getInt("banco");
                        String numeroCuenta = bo.getString("num");
                        
                        nuevoLibro.getCuentaCorrientes().add(new CuentaCorriente((short)(j+1), nuevoLibro, new CuentaContable(cuentaContableID), numeroCuenta, new Empresa(bancoID),new Date(),wr.getIdUsuario(),'A'));
                        j++;
                    }                  

                }
            }
           
         
           
            
        }catch(Exception e){
          
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
         //Fin
        
        /*
        *   Parte de Logica de Negocio    
        *
        */

        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            libroDao.insert(nuevoLibro);        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar el Libro ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        DateFormat fechaHora = new SimpleDateFormat("yyyy/MM/dd");
          
        JSONObject oResponse = new JSONObject();
        oResponse.put("libroID",nuevoLibro.getLibCajId());
        oResponse.put("nombre",nuevoLibro.getNom());
        oResponse.put("observacion",nuevoLibro.getObs());
        oResponse.put("fechaApertura",fechaHora.format(nuevoLibro.getFecApe())); 
        oResponse.put("fechaCierre",fechaHora.format(nuevoLibro.getFecCie()));  
        oResponse.put("saldoActual",nuevoLibro.getSalApe());         
        oResponse.put("estado",nuevoLibro.getEstReg());

        return WebResponse.crearWebResponseExito("El registro del Libro Caja se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
