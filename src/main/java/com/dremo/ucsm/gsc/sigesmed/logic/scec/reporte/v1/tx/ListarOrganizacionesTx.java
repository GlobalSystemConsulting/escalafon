package com.dremo.ucsm.gsc.sigesmed.logic.scec.reporte.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 16/10/16.
 */
public class ListarOrganizacionesTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarOrganizacionesTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();

        return buscarHijosOrganizacion(data.getInt("org"));
    }

    private WebResponse buscarHijosOrganizacion(int org) {
        try{
            OrganizacionDao orgDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            Organizacion padre = orgDao.buscarConTipoOrganizacionYPadre(org);
            TipoOrganizacion tipo = padre.getTipoOrganizacion();
            JSONArray orgsJson = new JSONArray();
            switch (tipo.getCod()){
                case "*":
                case "DRE":{
                    //listamos a los hijos (UGELES)
                    List<Organizacion> ugeles = orgDao.buscarHijosOrganizacion(padre.getOrgId());
                    //las agregamos  al json de respuesta
                    orgsJson = new JSONArray(EntityUtil.listToJSONString(new String[]{"orgId","nom"}, new String[]{"cod","nom"}, ugeles
                    ));
                    //buscaremos los colegios de cada ugel y los agregaremos
                    for(Organizacion ugel: ugeles){
                        List<Organizacion> ies  = orgDao.buscarHijosOrganizacion(ugel.getOrgId());
                        for(Organizacion ie : ies){
                            orgsJson.put(new JSONObject(EntityUtil.objectToJSONString(new String[]{"orgId","nom"}, new String[]{"cod","nom"}, ie)));
                        }
                    }
                }
                break;

                case "UGEL": {
                    List<Organizacion> ies = orgDao.buscarHijosOrganizacion(padre.getOrgId());
                    for (Organizacion ie : ies) {
                        orgsJson.put(new JSONObject(EntityUtil.objectToJSONString(new String[]{"orgId", "nom"}, new String[]{"cod", "nom"}, ie)));
                    }
                }
                break;
            }
            //agregamos al mismo objeto padre
            orgsJson.put(new JSONObject(EntityUtil.objectToJSONString(new String[]{"orgId", "nom"}, new String[]{"cod", "nom"}, padre)));
            return WebResponse.crearWebResponseExito("Se listo correctamente",orgsJson);
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarHijosOrganizacion",e);
            return WebResponse.crearWebResponseError("Error al listar los datos",WebResponse.BAD_RESPONSE);
        }
    }
}
