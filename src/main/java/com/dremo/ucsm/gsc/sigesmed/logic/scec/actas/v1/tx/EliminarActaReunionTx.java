package com.dremo.ucsm.gsc.sigesmed.logic.scec.actas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ActasReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ActasReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 05/10/2016.
 */
public class EliminarActaReunionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(EliminarActaReunionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return eliminarActa(data.getInt("cod"));
    }
    private WebResponse eliminarActa(int idActa){
        try{
            ActasReunionComisionDao actasReunionComisionDao = (ActasReunionComisionDao) FactoryDao.buildDao("scec.ActasReunionComisionDao");
            ActasReunionComision acta = actasReunionComisionDao.buscarConAcuerdos(idActa);
            actasReunionComisionDao.deleteAbsolute(acta);
            if(acta.getArcAdj() != null && acta.getArcAdj().length() > 0){
                //Eliminamos los archivos
                File file = new File(ServicioREST.PATH_SIGESMED+"/archivos/actas_reunion/" + acta.getArcAdj());
                file.delete();
            }
            return WebResponse.crearWebResponseExito("Exito al eliminar el acta",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE, "eliminarActa",e);
            return WebResponse.crearWebResponseError("Error al eliminar el acta",WebResponse.BAD_RESPONSE);
        }
    }
}