package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaInsDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.util.Date;
import org.json.JSONObject;

public class CambioSeccionTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        int nivId, jorEscId, graId, planNivelId, estId, orgDesId;
        char turId, secAct, secId;
        int usuMod;
        String fecMod;

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            nivId = requestData.getInt("nivId");
            jorEscId = requestData.getInt("jorEscId");
            graId = requestData.getInt("graId");
            planNivelId = requestData.getInt("planNivelId");
            estId = requestData.getInt("estId");
            orgDesId = requestData.getInt("orgDesId");

            turId = (char) requestData.getInt("turId");
            secAct = requestData.getString("secAct").charAt(0);
            secId = requestData.getString("secId").charAt(0);

            usuMod = requestData.getInt("usuMod");
            fecMod = DateUtil.convertDateToStringWithTime(new Date());

            MatriculaInsDaoHibernate dh = new MatriculaInsDaoHibernate();

            dh.CambioSeccion(nivId, jorEscId, turId, graId, secAct, secId, planNivelId, estId, orgDesId, usuMod, fecMod);

            return WebResponse.crearWebResponseExito("Se cambio de seccion");
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al canbiar de seccion! ", e.getMessage());

        }

    }
}
