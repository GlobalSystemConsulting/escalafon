/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;

/**
 *
 * @author christian
 */
public interface TrabajadorCargoDao extends GenericDao<TrabajadorCargo> {
    public TrabajadorCargo buscarByCod(int cod);
}
