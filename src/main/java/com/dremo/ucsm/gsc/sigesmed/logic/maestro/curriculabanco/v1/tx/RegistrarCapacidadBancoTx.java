package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CapacidadAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaCapacidadDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CapacidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaCapacidad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/10/2016.
 */
public class RegistrarCapacidadBancoTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(RegistrarCapacidadBancoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject) wr.getData();
            JSONObject jsonCap = data.getJSONObject("cap");
            JSONArray jsonInds = data.optJSONArray("ind");
            CapacidadAprendizaje capacidad = new CapacidadAprendizaje(jsonCap.getString("nom"), jsonCap.optString("des"));

            if(jsonInds != null){
                for(int i = 0; i < jsonInds.length(); i++){
                    JSONObject json = jsonInds.getJSONObject(i);
                    IndicadorAprendizaje indicador = new IndicadorAprendizaje(json.getString("nom"),capacidad,true);
                    capacidad.getIndicadores().add(indicador);
                }
            }
            return registrarCapacidad(capacidad, jsonCap.getInt("com"));
        }catch (Exception e){
            logger.log(Level.SEVERE,"execute",e);
            return WebResponse.crearWebResponseError("Error al realizar la consulta",WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse registrarCapacidad(CapacidadAprendizaje capacidad, int codCom) {
        try{
            CapacidadAprendizajeDao capacidadDao = (CapacidadAprendizajeDao) FactoryDao.buildDao("maestro.plan.CapacidadAprendizajeDao");
            CompetenciaAprendizajeDao competenciaDao = (CompetenciaAprendizajeDao) FactoryDao.buildDao("maestro.plan.CompetenciaAprendizajeDao");
            CompetenciaCapacidadDao comCapDao = (CompetenciaCapacidadDao) FactoryDao.buildDao("maestro.plan.CompetenciaCapacidadDao");

            CompetenciaAprendizaje competencia = competenciaDao.buscarCompetenciPorCodigo(codCom);
            capacidadDao.insert(capacidad);

            comCapDao.insert(new CompetenciaCapacidad(competencia,capacidad));
            JSONObject obj = new JSONObject().put("cod",capacidad.getCapId());

            return WebResponse.crearWebResponseExito("Exito al realizar la consula",obj);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarCapacidad",e);
            return WebResponse.crearWebResponseError("Error al realizar la consulta",e.toString());
        }
    }
}
