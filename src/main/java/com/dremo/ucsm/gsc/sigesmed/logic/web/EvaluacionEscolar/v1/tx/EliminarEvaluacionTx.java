/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaEscolar;
/**
 *
 * @author abel
 */
public class EliminarEvaluacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        TareaEscolar tarea = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int tareaID = requestData.getInt("tareaID");            
            tarea = new TareaEscolar(tareaID);
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la tarea, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        try{
            tareaDao.delete(tarea);
        }catch(Exception e){
            System.out.println("No se pudo eliminar la tarea\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la tarea", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        return WebResponse.crearWebResponseExito("La tarea se elimino correctamente");
        //Fin
    }    
    
    
}
