package com.dremo.ucsm.gsc.sigesmed.logic.scec.actas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.AcuerdosActaComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.AcuerdosActaComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 05/10/2016.
 */
public class ListarAcuerdosActaTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(ListarAcuerdosActaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String idActa = wr.getMetadataValue("cod");

        return listarAcuerdos(Integer.parseInt(idActa));
    }

    private WebResponse listarAcuerdos(int id) {
        try{
            AcuerdosActaComisionDao acuerdosActaComisionDao = (AcuerdosActaComisionDao) FactoryDao.buildDao("scec.AcuerdosActaComisionDao");
            List<AcuerdosActaComision> acuerdos = acuerdosActaComisionDao.buscarAcuerdosPorActa(id);

            return WebResponse.crearWebResponseExito("Se listo con exito",WebResponse.OK_RESPONSE).setData(
                    new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"acuActId","desAcu","fecCumAcu","estCumAcu","numVotFav","numVotCon","obs"},
                            new String[]{"cod","des","dat","est","afav","enco","obs"},
                            acuerdos
                    ))
            );
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarAcuerdos",e);
            return WebResponse.crearWebResponseError("Error al listar los acuerdos",WebResponse.BAD_RESPONSE);
        }
    }
}
