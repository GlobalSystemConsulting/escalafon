/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public class BuscarAsistenciaByFechaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Organizacion organizacion;
        String dni="";
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date fechaDesde;
        Date fechaHasta;
        try{
             JSONObject requestData = (JSONObject)wr.getData();
            Integer organizacionId = requestData.getInt("orgId");  
            dni=requestData.getString("perDni");
            String desde=requestData.getString("desde");
            String hasta=requestData.getString("hasta");
            fechaDesde=sdf.parse(desde);
            fechaHasta=sdf.parse(hasta);
            organizacion=new Organizacion(organizacionId);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }

        
        AsistenciaDao asistenciaDao= (AsistenciaDao)FactoryDao.buildDao("cpe.AsistenciaDao");
        
        List<Trabajador> trabajadores=new ArrayList<>();
       
        try{
            trabajadores=asistenciaDao.listarAsistenciaByFecha(fechaDesde, fechaHasta, organizacion, dni);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo listar las asistencias ", e.getMessage() );
        }
        JSONArray miArray=new JSONArray();
        for (Trabajador tra : trabajadores) {

            for (RegistroAsistencia ra : tra.getAsistencias()) {
                JSONObject oRes = new JSONObject();
                //trabajador
                if (tra.getTraCar() == null) {
                    oRes.put("cargo", "");
                } else {
                    oRes.put("cargo", tra.getTraCar().getCrgTraNom());
                }
                if (tra.getCondicion() == null) {
                    oRes.put("condicion", "");
                } else if (tra.getCondicion().equals('T')) {
                    oRes.put("condicion", "NOMBRADO");
                } else if (tra.getCondicion().equals('V')) {
                    oRes.put("condicion", "CONTRATADO");
                }

                if (tra.getJornada() == null) {
                    oRes.put("jornada", 0);
                } else {
                    oRes.put("jornada", tra.getJornada());
                }

                //asistencias  
                oRes.put("fecha", ra.getHoraIngreso() != null ? DateUtil.convertDateToString(ra.getHoraIngreso()) : DateUtil.convertDateToString(ra.getHoraSalida()));
                if (ra.getEstReg().equals("1")) {
                    oRes.put("estado", "ASISTIO");
                } else if (ra.getEstReg().equals("2")) {
                    oRes.put("estado", "TARDANZA");
                } else {
                    oRes.put("estado", "FALTA");
                }
                if (ra.getJustificacion() == null) {
                    oRes.put("obs", "");
                } else {
                    oRes.put("obs", "JUSTIFICADA");
                }
                miArray.put(oRes);
            }

        }

        return WebResponse.crearWebResponseExito("Se verifico correctamente", miArray);
        //Fin
    }

}
