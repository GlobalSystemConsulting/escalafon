package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.banco;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.banco.BancoLecturaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.BancoLectura;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.BancoLecturaDocente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

/**
 * Created by Administrador on 26/12/2016.
 */
public class BancoLecturaDaoHibernate extends GenericDaoHibernate<BancoLectura> implements BancoLecturaDao{
    @Override
    public BancoLecturaDocente buscarBancoLectura(int idDoc, int idOrg, int idGrad, int idAre, Character idSec, int year) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(BancoLecturaDocente.class)
                    .add(Restrictions.eq("estReg", 'A'))
                    .add(Restrictions.eq("temp", year));
            query.createCriteria("seccion", JoinType.INNER_JOIN).add(Restrictions.eq("sedId", idSec));
            query.createCriteria("area", JoinType.INNER_JOIN).add(Restrictions.eq("areCurId", idAre));
            query.createCriteria("grado", JoinType.INNER_JOIN).add(Restrictions.eq("graId", idGrad));
            query.createCriteria("organizacion", JoinType.INNER_JOIN).add(Restrictions.eq("orgId", idOrg));
            query.createCriteria("pro", JoinType.INNER_JOIN).add(Restrictions.eq("usuId", idDoc));

            query.setMaxResults(1);
            return (BancoLecturaDocente) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Organizacion buscarOrganizacion(int idOrg) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT o FROM Organizacion o WHERE o.estReg!='E' and o.orgId=:p1" ;
            Query query = session.createQuery(hql);
            query.setInteger("p1", idOrg);
            query.setMaxResults(1);

            return (Organizacion)query.uniqueResult();

        }catch(Exception e){
            throw e;
        }
        finally{
            session.close();
        }
    }

    @Override
    public Usuario buscarUsuario(int idDoc) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT u FROM Usuario u  WHERE u.usuId =:cod";
            Query query = session.createQuery(hql);
            query.setInteger("cod", idDoc);
            query.setMaxResults(1);
            return (Usuario) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Grado buscarGrado(int idGrado) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT g FROM Grado g  WHERE g.graId=:cod";
            Query query = session.createQuery(hql);
            query.setInteger("cod", idGrado);
            query.setMaxResults(1);
            return (Grado) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Seccion buscarSeccion(Character idSec) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT s FROM SeccionMaestro s  WHERE s.sedId=:cod";
            Query query = session.createQuery(hql);
            query.setCharacter("cod", idSec);
            query.setMaxResults(1);
            return (Seccion) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public AreaCurricular buscarArea(int idArea) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT a FROM AreaCurricularMaestro a  WHERE a.areCurId=:cod";
            Query query = session.createQuery(hql);
            query.setInteger("cod", idArea);
            query.setMaxResults(1);
            return (AreaCurricular) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
