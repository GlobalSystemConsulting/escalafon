/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.usuario_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author abel
 */
public class InsertarUsuarioSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        int rOpcionRegUsuario = 0;
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Usuario nuevoUsuario = null;
        Persona nuevaPersona = null;
        Trabajador nuevoTrabajador = new Trabajador();
        FichaEscalafonaria nuevaFicha = new FichaEscalafonaria();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONObject rUsuario = requestData.getJSONObject("usuario");
            System.out.println("rUsuario" + rUsuario);
            JSONObject rPersona = requestData.getJSONObject("persona");
            rOpcionRegUsuario = requestData.getInt("rOpcionRegUsuario");
            
            
            String dni = rPersona.getString("dni");
            String nombres = rPersona.getString("nombre").toUpperCase();
            String materno = rPersona.getString("materno").toUpperCase();
            String paterno = rPersona.getString("paterno").toUpperCase();            
            String email = rPersona.optString("email");
            String numero1 = rPersona.optString("numero1");
            String numero2 = rPersona.optString("numero2");
            
            
            int rolID = rUsuario.getInt("rolID");
            int organizacionID = rUsuario.getInt("organizacionID");
            String nombre = rUsuario.getString("nombre");
            String password = rUsuario.optString("password");
            String estado = rUsuario.getString("estado");
            Date fecIng = sdi.parse(rUsuario.getString("fecIng").substring(0,10));
            //nuevoUsuario = new Usuario(0, new Organizacion(organizacionID), new Rol(rolID), nombre, password, new Date(), new Date(), 1, estado.charAt(0));
            
            nuevaPersona = new Persona(0,dni,nombres,materno,paterno,new Date(),email,numero1,numero2);
            nuevoUsuario = new Usuario(0, nombre, (password.length() >0? password :paterno.substring(0, 3)+materno.substring(0, 3)+nombres.substring(0, 3)), new Date(), new Date(), wr.getIdUsuario(), estado.charAt(0));
            //trabajador
            nuevoTrabajador.setOrganizacion(new Organizacion(organizacionID));
            nuevoTrabajador.setRol(new Rol(rolID));
            nuevoTrabajador.setEstReg("A");
            nuevoTrabajador.setEstLab('1');
            nuevoTrabajador.setFecIng(fecIng);
            
            //ficha escalafonaria
            nuevaFicha.setEstReg('A');
            
            
            UsuarioSession session = new UsuarioSession(0, new Organizacion(organizacionID), new Rol(rolID), nuevoUsuario, new Date(), new Date(), wr.getIdUsuario(), 'A');
            nuevoUsuario.setSessiones(new ArrayList<UsuarioSession>());
            nuevoUsuario.getSessiones().add( session );
            
            int areaID = rUsuario.optInt("areaID");
            if(areaID>0){
                session.setAreId(areaID);
            }
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego trabajador", "Detalle: "+nombres+"_"+paterno+"_"+materno, wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////   
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        UsuarioDao usuarioDao = (UsuarioDao)FactoryDao.buildDao("UsuarioDao");
        PersonaDao personaDao = (PersonaDao)FactoryDao.buildDao("PersonaDao");
        TrabajadorDao trabajadorDao = (TrabajadorDao) FactoryDao.buildDao("TrabajadorDao");
        FichaEscalafonariaDao fichaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("FichaEscalafonariaDao");
        try{
            Persona persona = personaDao.buscarPorDNI(nuevaPersona.getDni());
            if(persona == null )
                personaDao.insert(nuevaPersona);
            else{
                nuevaPersona = persona;
            }
            
            nuevoUsuario.setUsuId(nuevaPersona.getPerId());
            usuarioDao.insert(nuevoUsuario);
            
            if (rOpcionRegUsuario == 1){
                nuevoTrabajador.setPersona(nuevaPersona);
                trabajadorDao.insert(nuevoTrabajador);


                nuevaFicha.setTrabajador(nuevoTrabajador);
                fichaDao.insert(nuevaFicha);
            }
        
        }catch(Exception e){
            System.out.println("No se pudo registrar el Usuario\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Usuario", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("usuarioID",nuevoUsuario.getUsuId());
        oResponse.put("sessionID",nuevoUsuario.getSessiones().get(0).getUsuSesId());
        oResponse.put("password",nuevoUsuario.getPas());
        if (rOpcionRegUsuario==1){
            oResponse.put("ficEscId",nuevaFicha.getFicEscId());
        }else{
            oResponse.put("ficEscId",-1);
        }
        return WebResponse.crearWebResponseExito("El registro del Usuario se realizo correctamente", oResponse);
        //Fin
    }
    
}
