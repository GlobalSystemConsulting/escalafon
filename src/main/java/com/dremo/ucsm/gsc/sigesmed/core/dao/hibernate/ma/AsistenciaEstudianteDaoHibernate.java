package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.AsistenciaEstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.AsistenciaEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.EstudianteAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.JustificacionInasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Matricula;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 11/01/2017.
 */
public class AsistenciaEstudianteDaoHibernate extends GenericDaoHibernate<AsistenciaEstudiante> implements AsistenciaEstudianteDao {
    @Override
    public List<EstudianteAsistencia> listarAsistenciasEstudiantes(int idOrg, int idGrado, char idSeccion, Date fecAsi) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT m.id as id,p.dni as dni ,p.perId as perId,p.apePat as apePat,p.apeMat as apeMat,p.nom as nom,ase.fecAsi as fecAsi,ase.tipAsi as tipAsi, ase.estAsi as estAsi,ase.asiAre as asiAre, ase.asiEstId as asiId, jus.desJus as desJus, jus.docAdjJus as docJus" +
                    " FROM MatriculaMaestro m" +
                    " INNER JOIN m.estudiante e INNER JOIN e.persona p INNER JOIN m.ie org" +
                    " INNER JOIN m.gradosMatricula gie INNER JOIN gie.grado gra" +
                    " INNER JOIN gie.seccion secc LEFT OUTER JOIN m.asistencias ase LEFT OUTER JOIN ase.justificacion jus" +
                    " WHERE org.orgId =:idOrg AND m.act =:mAct AND gie.act =:gieAct" +
                    " AND gra.graId =:idGra AND secc.sedId =:idSecc AND (ase.fecAsi =:asiFec OR ase.fecAsi IS NULL) AND ase.area.areCurId IS NULL";
            Query query = session.createQuery(hql);
            query.setInteger("idOrg",idOrg);
            query.setBoolean("mAct",true);
            query.setBoolean("gieAct",true);
            query.setInteger("idGra",idGrado);
            query.setCharacter("idSecc",idSeccion);
            query.setDate("asiFec",fecAsi);

            query.setResultTransformer(Transformers.aliasToBean(EstudianteAsistencia.class));

            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<EstudianteAsistencia> listarAsistenciasEstudiantesArea(int idOrg, int idGrado, char idSeccion, int idArea, Date fecAsi) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT m.id as id,p.dni as dni ,p.perId as perId,p.apePat as apePat,p.apeMat as apeMat,p.nom as nom,ase.fecAsi as fecAsi,ase.tipAsi as tipAsi, ase.estAsi as estAsi,ase.asiAre as asiAre,ase.asiEstId as asiId, jus.desJus as desJus, jus.docAdjJus as docJus" +
                    " FROM MatriculaMaestro m" +
                    " INNER JOIN m.estudiante e INNER JOIN e.persona p INNER JOIN m.ie org" +
                    " INNER JOIN m.gradosMatricula gie INNER JOIN gie.grado gra" +
                    " INNER JOIN gie.seccion secc LEFT OUTER JOIN m.asistencias ase LEFT OUTER JOIN ase.justificacion jus" +
                    " WHERE org.orgId =:idOrg AND m.act =:mAct AND gie.act =:gieAct" +
                    " AND gra.graId =:idGra AND secc.sedId =:idSecc AND (ase.fecAsi =:asiFec OR ase.fecAsi IS NULL) AND (ase.area.areCurId =:areaId)";
            Query query = session.createQuery(hql);
            query.setInteger("idOrg",idOrg);
            query.setBoolean("mAct",true);
            query.setBoolean("gieAct",true);
            query.setInteger("idGra",idGrado);
            query.setCharacter("idSecc",idSeccion);
            query.setDate("asiFec",fecAsi);
            query.setInteger("areaId",idArea);
            query.setResultTransformer(Transformers.aliasToBean(EstudianteAsistencia.class));

            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Matricula buscarMatricula(int idMatricula) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{

            Criteria criteria = session.createCriteria(Matricula.class)
                    .add(Restrictions.eq("id",idMatricula))
                    .setMaxResults(1);
            return (Matricula) criteria.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public AsistenciaEstudiante buscarAsistenciaEstudiante(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(AsistenciaEstudiante.class)
                    .createAlias("area","are", JoinType.LEFT_OUTER_JOIN)
                    .add(Restrictions.eq("asiEstId", id))
                    .setMaxResults(1);
            return (AsistenciaEstudiante) criteria.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public AsistenciaEstudiante buscarAsistenciaEstudiante(int idEst, int idOrg, int idGra, char idSec,int idAre, Date fec) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT ae FROM AsistenciaEstudiante ae INNER JOIN  ae.matricula m INNER JOIN m.gradosMatricula g" +
                    " WHERE m.ie.orgId =:orgId AND m.act =:mAct AND g.grado.graId =:graId AND g.seccion.sedId =:seccId" +
                    " AND g.act =:gAct AND m.estudiante.per_id =:idEst AND ae.fecAsi =:fec AND ae.area.areCurId =:areId";
            Query query = session.createQuery(hql);
            query.setInteger("orgId",idOrg);
            query.setBoolean("mAct", true);
            query.setInteger("graId", idGra);
            query.setCharacter("seccId", idSec);
            query.setBoolean("gAct", true);
            query.setInteger("idEst",idEst);
            query.setDate("fec",fec);
            query.setInteger("areId",idAre);
            query.setMaxResults(1);
            return (AsistenciaEstudiante) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public AsistenciaEstudiante buscarAsistenciaEstudiante(int idEst, int idOrg, int idGra, char idSec, Date fec) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT ae FROM AsistenciaEstudiante ae INNER JOIN  ae.matricula m INNER JOIN m.gradosMatricula g" +
                    " WHERE m.ie.orgId =:orgId AND m.act =:mAct AND g.grado.graId =:graId AND g.seccion.sedId =:seccId" +
                    " AND g.act =:gAct AND m.estudiante.per_id =:idEst AND ae.fecAsi =:fec";
            Query query = session.createQuery(hql);
            query.setInteger("orgId",idOrg);
            query.setBoolean("mAct", true);
            query.setInteger("graId", idGra);
            query.setCharacter("seccId", idSec);
            query.setBoolean("gAct", true);
            query.setInteger("idEst",idEst);
            query.setDate("fec",fec);
            query.setMaxResults(1);
            return (AsistenciaEstudiante) query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public void registrarJustificacion(JustificacionInasistencia justificacion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            session.saveOrUpdate(justificacion);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }
}
