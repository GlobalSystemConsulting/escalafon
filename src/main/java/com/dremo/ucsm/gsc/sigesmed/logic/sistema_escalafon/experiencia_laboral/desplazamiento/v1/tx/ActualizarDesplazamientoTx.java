/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.desplazamiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarDesplazamientoTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ActualizarDesplazamientoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer desId = requestData.getInt("desId");
            Character tip = requestData.getString("tip").charAt(0);
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = null;
            if(requestData.getString("fecDoc").length() > 0){
                fecDoc = sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            }
            String insEdu = requestData.getString("insEdu");
            String car = requestData.getString("car");
            String jorLab = requestData.getString("jorLab");
            Date fecIni = null;
            if(requestData.getString("fecIni").length() > 0){
                fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));
            }
            Date fecTer = null;
            if(requestData.getString("fecTer").length() > 0){
                fecTer = sdi.parse(requestData.getString("fecTer").substring(0, 10));
            }
            Date fecDocTer = null;
            if(requestData.getString("fecDocTer").length() > 0){
                fecDocTer = sdi.parse(requestData.getString("fecDocTer").substring(0, 10));
            }
            String numDocTer = requestData.getString("numDocTer");
            String tipdoc = requestData.getString("tipDoc");
            String tipdocter = requestData.getString("tipDocTer");
            String motRet = requestData.getString("motRet");
            
            return actualizarDesplazamiento(desId, tip, numDoc, fecDoc, tipdoc, insEdu, car, jorLab, fecIni, fecTer, fecDocTer, numDocTer, tipdocter, motRet);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar desplazamientoa",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarDesplazamiento(Integer desId, Character tip, String numDoc, Date fecDoc, String tipDoc,
            String insEdu, String car, String jorLab, Date fecIni, Date fecTer, Date fecDocTer, String numDocTer, String tipdocter, String motRet) {
        try{
            DesplazamientoDao desplazamientoDao = (DesplazamientoDao)FactoryDao.buildDao("se.DesplazamientoDao");        
            Desplazamiento desplazamiento = desplazamientoDao.buscarPorId(desId);

            desplazamiento.setTip(tip);
            desplazamiento.setNumDoc(numDoc);
            desplazamiento.setFecDoc(fecDoc);
            desplazamiento.setTipDoc(tipDoc);
            desplazamiento.setInsEdu(insEdu);
            desplazamiento.setCar(car);
            desplazamiento.setJorLab(jorLab);
            desplazamiento.setFecIni(fecIni);
            desplazamiento.setFecTer(fecTer);
            desplazamiento.setNumDocTer(numDocTer);
            desplazamiento.setFecDocTer(fecDocTer);
            desplazamiento.setTipDocTer(tipdocter);
            desplazamiento.setMotRet(motRet);
            
            desplazamientoDao.update(desplazamiento);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("desId", desplazamiento.getDesId());
            oResponse.put("tip", desplazamiento.getTip());
            oResponse.put("tipDes", "");
            oResponse.put("numDoc", null != desplazamiento.getNumDoc() ? desplazamiento.getNumDoc():"");
            oResponse.put("fecDoc", null != desplazamiento.getFecDoc() ? sdo.format(desplazamiento.getFecDoc()) : "");
            oResponse.put("tipDoc", null != desplazamiento.getTipDoc() ? desplazamiento.getTipDoc() : "");
            oResponse.put("numDocTer", null != desplazamiento.getNumDocTer() ? desplazamiento.getNumDocTer() : "");
            oResponse.put("fecDocTer", null != desplazamiento.getFecDocTer() ? sdo.format(desplazamiento.getFecDocTer()) : "");
            oResponse.put("tipDocTer", null != desplazamiento.getTipDocTer() ? desplazamiento.getTipDocTer() : "");
            oResponse.put("insEdu", null != desplazamiento.getInsEdu() ? desplazamiento.getInsEdu() : "");
            oResponse.put("car", null != desplazamiento.getCar() ? desplazamiento.getCar() : "");
            oResponse.put("jorLab", null != desplazamiento.getJorLab() ? desplazamiento.getJorLab() : "");
            oResponse.put("fecIni", null != desplazamiento.getFecIni() ? sdo.format(desplazamiento.getFecIni()) : "");
            oResponse.put("fecTer", null != desplazamiento.getFecTer() ? sdo.format(desplazamiento.getFecTer()) : "");
            oResponse.put("motRet", null != desplazamiento.getFecTer() && desplazamiento.getMotRet().length()>0 ? desplazamiento.getMotRet() : "");
            
            return WebResponse.crearWebResponseExito("Desplazamiento actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarDesplazamiento",e);
            return WebResponse.crearWebResponseError("Error, el desplazamiento no fue actualizad");
        }
    } 
    
}
