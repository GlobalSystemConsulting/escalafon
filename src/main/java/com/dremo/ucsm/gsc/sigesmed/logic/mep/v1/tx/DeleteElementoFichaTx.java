package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.FichaEvaPerslDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ContenidoFichaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.EscalaValoracionFicha;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.RangoPorcentualFicha;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 22/08/2016.
 */
public class DeleteElementoFichaTx extends MepGeneralTx implements ITransaction {
    private  static Logger logger = Logger.getLogger(DeleteElementoFichaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String strid = wr.getMetadataValue("id");
        String tipo = wr.getMetadataValue("tipo");
        return deleteElemento(Integer.parseInt(strid),tipo);
    }
    private WebResponse deleteElemento(int idElemento,String tipo){
        try{
            FichaEvaPerslDaoHibernate fedh = new FichaEvaPerslDaoHibernate();
            Class tipElement = null;
            switch (tipo){
                case MEP.TIPO_ELEMENTO_RANGO: tipElement = RangoPorcentualFicha.class;break;
                case MEP.TIPO_ELEMENTO_ESCALA: tipElement = EscalaValoracionFicha.class;break;
                case MEP.TIPO_ELEMENTO_CONTENIDO: tipElement = ContenidoFichaEvaluacion.class;break;
                case MEP.TIPO_ELEMENTO_INDICADOR:
                    return formWebResponse(fedh.deleteIndicadorFichaById(idElemento));
            }
            return formWebResponse(fedh.deleteElementoFichaById(idElemento, tipElement));

        }catch (JSONException e){
            logger.log(Level.SEVERE,logger.getName() + ": deleteElemento()",e);
            return formWebResponse(false);
        }
    }

}
