/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;

import java.util.Date;
import javax.persistence.*;

import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author Carlos
 */
@Entity(name="AsistenciaEstudianteMpf")
@Table(name="asistencia_estudiante", schema="pedagogico")
@DynamicUpdate(value = true)
public class AsistenciaEstudiante implements java.io.Serializable{
    @Id
    @Column(name="asi_est_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_asistencia_estudiante", sequenceName="pedagogico.asistencia_estudiante_asi_est_id_seq" )
    @GeneratedValue(generator="secuencia_asistencia_estudiante")
    private Long asiEstId;
    
    @Column(name = "mat_id")
    private Long matriculaId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mat_id",insertable = false,updatable = false)
    private Matricula matricula;
    
    @Column(name = "are_cur_id")
    private Integer areaId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "are_cur_id",insertable = false,updatable = false)
    private AreaCurricular area;
    
    @Column(name = "org_id")
    private Integer organizacionId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id",insertable = false,updatable = false)
    private Organizacion organizacion;
    
    @Column(name = "usu_id")
    private Integer usuarioId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id",insertable = false,updatable = false)
    private Usuario usuario;

    @OneToOne(mappedBy = "asistencia",fetch = FetchType.LAZY)
    private JustificacionInasistencia justificacion;
    
    @Column(name = "est_asi")
    private Integer estAsi;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "fec_asi")
    private Date fecAsi;
    
    @Column(name = "tip_asi")
    private Integer tipAsi;
    
    @Column(name = "asi_are")
    private Boolean asiAre;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public AsistenciaEstudiante() {
        this.fecMod = new Date();
        this.estReg= 'A';
    }

    public Long getAsiEstId() {
        return asiEstId;
    }

    public void setAsiEstId(Long asiEstId) {
        this.asiEstId = asiEstId;
    }

    public Long getMatriculaId() {
        return matriculaId;
    }

    public void setMatriculaId(Long matriculaId) {
        this.matriculaId = matriculaId;
    }

    public Matricula getMatricula() {
        return matricula;
    }

    public void setMatricula(Matricula matricula) {
        this.matricula = matricula;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public AreaCurricular getArea() {
        return area;
    }

    public void setArea(AreaCurricular area) {
        this.area = area;
    }

    public Integer getOrganizacionId() {
        return organizacionId;
    }

    public void setOrganizacionId(Integer organizacionId) {
        this.organizacionId = organizacionId;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public JustificacionInasistencia getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(JustificacionInasistencia justificacion) {
        this.justificacion = justificacion;
    }

    public Integer getEstAsi() {
        return estAsi;
    }

    public void setEstAsi(Integer estAsi) {
        this.estAsi = estAsi;
    }

    public Date getFecAsi() {
        return fecAsi;
    }

    public void setFecAsi(Date fecAsi) {
        this.fecAsi = fecAsi;
    }

    public Integer getTipAsi() {
        return tipAsi;
    }

    public void setTipAsi(Integer tipAsi) {
        this.tipAsi = tipAsi;
    }

    public Boolean getAsiAre() {
        return asiAre;
    }

    public void setAsiAre(Boolean asiAre) {
        this.asiAre = asiAre;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    
}
