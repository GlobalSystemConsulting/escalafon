/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.facultad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FacultadDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganismoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Facultad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organismo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarFacultadesTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarFacultadesTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{    
            JSONObject requestData = (JSONObject)wr.getData();
            Integer facId = requestData.getInt("facId");
            String codFac = requestData.optString("codFac");
            String nomFac = requestData.optString("nomFac");
            Integer orgaId = requestData.getInt("orgaId");
            OrganismoDao organismoDao = (OrganismoDao)FactoryDao.buildDao("se.OrganismoDao");
            Organismo organismo = organismoDao.buscarPorId(orgaId);
            
            return actualizarCargos( facId, codFac, nomFac, organismo);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    private WebResponse actualizarCargos(Integer facId,String codFac, String nomFac, Organismo organismo) {
        try{
            FacultadDao facultadDao = (FacultadDao)FactoryDao.buildDao("se.FacultadDao");        
            Facultad facultad = facultadDao.buscarPorId(facId);

            facultad.setCodFac(codFac);
            facultad.setNomFac(nomFac);
            facultad.setOrganismo(organismo);
            
            facultadDao.update(facultad);
            JSONObject oResponse = new JSONObject();
            oResponse.put("facId", facultad.getFacId());
            oResponse.put("codFac", facultad.getCodFac());
            oResponse.put("nomFac", facultad.getNomFac());
            return WebResponse.crearWebResponseExito(" La Facultad actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("Error, La Facultad no fue actualizado");
        }
    }
}
