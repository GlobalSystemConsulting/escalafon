/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.std.PrioridadExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.PrioridadExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class EliminarPrioridadExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int prioridadExpedienteID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            prioridadExpedienteID = requestData.getInt("prioridadExpedienteID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la prioridad expediente, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        PrioridadExpedienteDao prioridadDao = (PrioridadExpedienteDao)FactoryDao.buildDao("std.PrioridadExpedienteDao");
        try{
            prioridadDao.delete(new PrioridadExpediente(prioridadExpedienteID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la Prioridad de expediente\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la Prioridad de expediente", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La Prioridad de Expediente se elimino correctamente");
        //Fin
    }
}
