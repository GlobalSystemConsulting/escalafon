/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ListarEntidadesTx implements ITransaction{
    
    private JSONArray listDirs = new JSONArray();
    List<ItemFile> tabladocs;
    ItemFile padreTmp;
    int codPadre = 0;
    int idPadre = 0;
    String idPadreCad = "";
    
    public JSONObject getPropiedades(int id){
        JSONObject prop = new JSONObject();
        for (int i = 0; i < listDirs.length(); i++) {
            if (listDirs.getJSONObject(i).getInt("codFile") == id) {
                prop.put("roleName", listDirs.getJSONObject(i).getString("nomFile"));
                prop.put("roleId", listDirs.getJSONObject(i).getInt("codFile"));
                return prop;
            }
        }
        return prop;
    }
    public JSONArray getHijos(int idPadre) {
        JSONArray baTmp = new JSONArray();
        for (int i = 0; i < listDirs.length(); i++) {
            if (listDirs.getJSONObject(i).getInt("codFile") == idPadre) {
                baTmp = listDirs.getJSONObject(i).getJSONArray("arrayHijos");
                return baTmp;
            }
        }
        return baTmp;
    }

    public boolean containsKey(int idPadre) {
        for (int i = 0; i < listDirs.length(); i++) {
            if (listDirs.getJSONObject(i).getInt("codFile") == idPadre) {
                return true;
            }
        }
        return false;
    }
    
    
    //Primera accion a ejecutarse
    public void prepararFiles(List<Organizacion> orgs) {
        
        
        for (Organizacion org : orgs) {
            //Preparamos el hijo y el padre la organizacion encapsulado en un Object y pueda pasar
            //Para este caso solo pasaremos el id como si fuera todo la organizacion
            int idHijo = org.getOrgId();
            
            Organizacion filePadre = null;

            if(org.getOrganizacionPadre()!=null){
                filePadre = org.getOrganizacionPadre();
            }
            
            if (filePadre != null) {
                idPadre = filePadre.getOrgId();
                idPadreCad = idPadre + "";
            }else{
                codPadre = idHijo;
            }
            
            JSONArray listChildrenFinal = new JSONArray();
            JSONObject objHijo = new JSONObject();
            objHijo.put("codFile", idHijo);
            objHijo.put("nomFile", org.getNom());
            objHijo.put("arrayHijos", listChildrenFinal);

            if (filePadre != null) {
                //Preguntamos si existe ya el padre
                if (containsKey(idPadre)) {
                //Si existe retornamos el valor y agregamos una lista
                    //buscar el objTmp en la lista
                    JSONArray hijos = getHijos(idPadre);
                    //agregamos el nuevo hijo (no importa el orden)
                    hijos.put(objHijo);
                } else {
                    //Preparamos su primer hijo
                    JSONArray listChildren = new JSONArray();
                    listChildren.put(objHijo);
                    //el array tiene como id el nombre del padre
                    listChildren.opt(org.getOrganizacionPadre().getOrgId()); /*revisar el id*/

                    JSONObject padre = new JSONObject();
                    
                    //El padre de documento es el file Padre
                    
                    padre.put("arrayHijos", listChildren);
                    padre.put("nomFile", filePadre.getNom());
                    padre.put("codFile", filePadre.getOrgId());
                    

                    //No existe el padre por lo cual agregar al hashMap con un alista vacio de hijos
                    listDirs.put(padre);
                }
            } else {
                listDirs.put(objHijo);
            }
        }
    }

    public void printListDir() {
        for (int i = 0; i < listDirs.length(); i++) {
            int cod = listDirs.getJSONObject(i).getInt("codFile");
            String nom = listDirs.getJSONObject(i).getString("nomFile");

            System.out.println("CodPad: " + cod + " NomPad: " + nom);

            JSONArray hijos = listDirs.getJSONObject(i).getJSONArray("arrayHijos");
            for (int j = 0; j < hijos.length(); j++) {
                int codHijo = hijos.getJSONObject(j).getInt("codFile");
                String nomHijo = hijos.getJSONObject(j).getString("nomFile");
                System.out.println("CodHijo: " + codHijo + " NomHijo: " + nomHijo);
            }
        }
    }

    public void displayDirectoryContents(JSONArray nlistDirs, JSONArray arbolTmp) {

        for (int i = 0; i < nlistDirs.length(); i++) {
            JSONObject hijoAct = nlistDirs.getJSONObject(i);
            int idHijo = hijoAct.getInt("codFile");
            JSONArray children = new JSONArray();
            if (idHijo != 10000) {
                children = getHijos(idHijo);

                if (children.length()!= 0) {
                    //Para agregar al arbol debemos tener encapsularse como baseObjeto
                    JSONObject propDir = new JSONObject();

                    //Nombre de Directorio
                    //Del primer hijo obtenemos las propiedades del padre
                    propDir.put("roleName", hijoAct.getString("nomFile"));
                    propDir.put("roleId", hijoAct.getInt("codFile"));
                    

                    //creamos un nuevo baseArray por ser directorio
                    JSONArray contenedorArchivos = new JSONArray();
                    propDir.put("children", contenedorArchivos);

                    //agregarlo a tu arbol actual
                    arbolTmp.put(propDir);

                    //el nuevo parametro es ahora el arbol contenedor
                    //cuidado con parametros copia (depurar)
                    displayDirectoryContents(children, contenedorArchivos);
                } else {
                    //Al ser un archivo debemos acompañar con uss propiedades de fecha y tamaño y tipo
                    JSONObject propArch = new JSONObject();

                    //Agregar el nombre del archivo
                    propArch.put("roleName", hijoAct.getString("nomFile"));
                    propArch.put("roleId", hijoAct.getInt("codFile"));
                    
                    //un archivo tendra una lista de children vacio
                    JSONArray contenedorArchivos = new JSONArray();
                    propArch.put("children", contenedorArchivos);

                    arbolTmp.put(propArch);
                }
            }

        }

    }

    @Override
    public WebResponse execute(WebRequest wr) {

        /*
         *   Parte para la lectura, verificacion y validacion de datos
         */
        try {

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo Listar los Documentos", e.getMessage());
        }
        //Fin        

        /*
         *  Parte para la operacion en la Base de Datos
         */
        List<Organizacion> orgs = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            //listar Todas las Organizaciones
            String hql = "FROM Organizacion org ORDER By org.orgId";
            Query query = session.createQuery(hql);
            orgs = query.list();

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo listar las organizaciones, ", e.getMessage());
        } finally {
            session.close();
        }

        JSONArray mapaDir = new JSONArray();
        JSONArray mapaDirHijos = new JSONArray();
        if (orgs.size() != 0) {
            prepararFiles(orgs);
            printListDir();
            //Para agregar al arbol debemos tener encapsularse como baseObjeto
            JSONObject propDir = new JSONObject();
            propDir = getPropiedades(codPadre);
            JSONArray hijos = getHijos(codPadre);
            displayDirectoryContents(hijos, mapaDirHijos);
            propDir.put("children", mapaDirHijos);

            //agregarlo a tu arbol actual
            mapaDir.put(propDir);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente", mapaDir);
        //Fin
    }
}
