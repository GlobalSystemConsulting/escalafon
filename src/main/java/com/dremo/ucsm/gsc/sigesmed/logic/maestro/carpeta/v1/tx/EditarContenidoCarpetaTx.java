package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.ContenidoSeccionCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ContenidoSeccionCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 29/12/2016.
 */
public class EditarContenidoCarpetaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EditarContenidoCarpetaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idContenido = data.getInt("id");
        return editarContenido(idContenido, data);
    }

    private WebResponse editarContenido(int idContenido, JSONObject data) {
        try{
            ContenidoSeccionCarpetaDao contDao = (ContenidoSeccionCarpetaDao) FactoryDao.buildDao("maestro.carpeta.ContenidoSeccionCarpetaDao");
            ContenidoSeccionCarpeta contenido  = contDao.buscarContenidoPorId(idContenido);
            contenido.setNom(data.optString("nom",contenido.getNom()).toUpperCase());
            contenido.setTipUsu(data.optInt("tip",contenido.getTipUsu()));
            contenido.setFecMod(new Date());
            contDao.update(contenido);
            return WebResponse.crearWebResponseExito("Se edito correctamente");
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarSeccion",e);
            return WebResponse.crearWebResponseError("No se puede editar la seccion");
        }
    }
}
