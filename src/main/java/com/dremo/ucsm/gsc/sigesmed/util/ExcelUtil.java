package com.dremo.ucsm.gsc.sigesmed.util;

import com.itextpdf.kernel.pdf.PdfWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Funciona solamente para archivos .xlsx Escribe datos en un excel,
 * funcionalidad de lectura no desarrollada Color predefinido: Negro, Tamaño:
 * 12, FONT: arial
 *
 * @author GSC_H
 */
public class ExcelUtil {

    private FileInputStream file;
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private Row row;
    private Cell cell;

    private XSSFFont ITALIC_FONT;
    private XSSFFont NORMAL_FONT;
    private XSSFFont BOLD_FONT;

    public static final int ITALIC = 1;
    public static final int BOLD = 2;
    public static final int NORMAL = 3;

    public ExcelUtil(String urlResource, String nameSheet) {
        try {
            file = new FileInputStream(new File(urlResource));
            workbook = new XSSFWorkbook(file);
            sheet = workbook.getSheet(nameSheet);
            loadFonts(12, "ARIAL", HSSFColor.BLACK.index);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }

    public ExcelUtil(String urlResource, int numSheet) {
        try {
            file = new FileInputStream(new File(urlResource));
            workbook = new XSSFWorkbook(file);
            sheet = workbook.getSheetAt(numSheet);
            loadFonts(12, "ARIAL", HSSFColor.BLACK.index);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }

    public ExcelUtil(String urlResource) {
        try {
            file = new FileInputStream(new File(urlResource));
            workbook = new XSSFWorkbook(file);
            sheet = workbook.getSheetAt(0);
            loadFonts(12, "ARIAL", HSSFColor.BLACK.index);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }

    public ExcelUtil() {
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("sheet 1");
        loadFonts(12, "ARIAL", HSSFColor.BLACK.index);
    }

    public void changeSheet(int numSheet) {
        sheet = workbook.getSheetAt(numSheet);
    }

    public void changeSheet(String nameSheet) {
        sheet = workbook.getSheet(nameSheet);
    }

    public void createSheet(String nameSheet) {
        sheet = workbook.createSheet(nameSheet);
    }

    /**
     * inserta en la Hoja seleccionada
     *
     * @param numRow
     * @param content
     * @param numCell
     */
    public void insertCell(int numRow, int numCell, String content) {
        row = sheet.getRow(numRow);
        cell = row.getCell(numCell);
        cell.setCellValue(content);

//        XSSFCellStyle style = workbook.createCellStyle();
//        style.setFont(NORMAL_FONT);
//        cell.setCellStyle(style);
    }

    public void insertCell(int numRow, int numCell, String content, int font) {
        row = sheet.getRow(numRow);
        cell = row.createCell(numCell);
        cell.setCellValue(content);

        XSSFCellStyle style = workbook.createCellStyle();
        switch (font) {
            case ITALIC: {
                style.setFont(ITALIC_FONT);
                break;
            }
            case BOLD: {
                style.setFont(BOLD_FONT);
                break;
            }
            case NORMAL: {
                style.setFont(NORMAL_FONT);
                break;
            }
        }
        cell.setCellStyle(style);
    }

    public void makeXlsx(String path, String name) {
        FileOutputStream out;
        try {
            out = new FileOutputStream(new File(path + name));
            workbook.write(out);
            out.close();

        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

    public void setheinghtLetter(int heinghtLetter) {
        ITALIC_FONT.setFontHeightInPoints((short) heinghtLetter);
        BOLD_FONT.setFontHeightInPoints((short) heinghtLetter);
        NORMAL_FONT.setFontHeightInPoints((short) heinghtLetter);
    }

    /**
     *
     * @param font 1: italic, 2:bold, 3:Normal
     * @param heinghtLetter tamaño de la letra
     */
    public void setheinghtLetter(int font, int heinghtLetter) {
        switch (font) {
            case ITALIC: {
                ITALIC_FONT.setFontHeightInPoints((short) heinghtLetter);
                break;
            }
            case BOLD: {
                BOLD_FONT.setFontHeightInPoints((short) heinghtLetter);
                break;
            }
            case NORMAL: {
                NORMAL_FONT.setFontHeightInPoints((short) heinghtLetter);
                break;
            }
        }
    }

    public void setFonts(String fontName) {
        ITALIC_FONT.setFontName(fontName);
        BOLD_FONT.setFontName(fontName);
        NORMAL_FONT.setFontName(fontName);
    }

    /**
     *
     * @param font 1: italic, 2:bold, 3:Normal
     * @param fontName Nombre del font disponible para POI
     */
    public void setFonts(int font, String fontName) {
        switch (font) {
            case ITALIC: {
                ITALIC_FONT.setFontName(fontName);
                break;
            }
            case BOLD: {
                BOLD_FONT.setFontName(fontName);
                break;
            }
            case NORMAL: {
                NORMAL_FONT.setFontName(fontName);
                break;
            }
        }
    }

    public void setColors(short color) {
        ITALIC_FONT.setColor(color);
        BOLD_FONT.setColor(color);
        NORMAL_FONT.setColor(color);
    }

    /**
     *
     * @param font 1: italic, 2:bold, 3:Normal
     * @param color color predefinido en HSSFColor.Color.index
     */
    public void setColors(int font, short color) {
        switch (font) {
            case ITALIC: {
                ITALIC_FONT.setColor(color);
                break;
            }
            case BOLD: {
                BOLD_FONT.setColor(color);
                break;
            }
            case NORMAL: {
                NORMAL_FONT.setColor(color);
                break;
            }
        }
    }

    private void loadFonts(int heinghtLetter, String font, short color) {
        ITALIC_FONT = workbook.createFont();
        ITALIC_FONT.setItalic(true);
        ITALIC_FONT.setFontHeightInPoints((short) heinghtLetter);
        ITALIC_FONT.setFontName(font);
        ITALIC_FONT.setColor(color);

        BOLD_FONT = workbook.createFont();
        BOLD_FONT.setBold(true);
        BOLD_FONT.setFontHeightInPoints((short) heinghtLetter);
        BOLD_FONT.setFontName(font);
        BOLD_FONT.setColor(color);

        NORMAL_FONT = workbook.createFont();
        NORMAL_FONT.setFontHeightInPoints((short) heinghtLetter);
        NORMAL_FONT.setFontName(font);
        NORMAL_FONT.setColor(color);
    }

}
