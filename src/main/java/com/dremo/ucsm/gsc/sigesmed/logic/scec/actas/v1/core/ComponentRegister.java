package com.dremo.ucsm.gsc.sigesmed.logic.scec.actas.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.scec.actas.v1.tx.*;

/**
 * Created by Administrador on 05/10/2016.
 */
public class ComponentRegister implements IComponentRegister {
    @Override
    public WebComponent createComponent() {
        WebComponent scecComponent = new WebComponent(Sigesmed.SUBMODULO_COMISIONES);

        scecComponent.setName("actas");
        scecComponent.setVersion(1);

        scecComponent.addTransactionPOST("registrarActa", RegistrarActaReunionTx.class);
        scecComponent.addTransactionPOST("registrarAcuerdo", RegistrarAcuerdoActaTx.class);

        scecComponent.addTransactionGET("listarActas", ListarActasReunionTx.class);
        scecComponent.addTransactionGET("listarAcuerdos", ListarAcuerdosActaTx.class);

        scecComponent.addTransactionPUT("editarActa", EditarActaReunionTx.class);
        scecComponent.addTransactionPUT("editarAcuerdo", EditarAcuerdoActaTx.class);

        scecComponent.addTransactionDELETE("eliminarActa", EliminarActaReunionTx.class);
        scecComponent.addTransactionDELETE("eliminarAcuerdo", EliminarAcuerdoActaTx.class);

        return scecComponent;
    }
}
