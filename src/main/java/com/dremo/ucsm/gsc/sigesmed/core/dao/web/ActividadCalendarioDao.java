/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.web;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.Calendario;
import java.util.List;

/**
 *
 * @author abel
 */
public interface ActividadCalendarioDao extends GenericDao<ActividadCalendario>{
    
    public void crearActividad(ActividadCalendario mensaje,int destinatarioID);
    public void crearActividad(ActividadCalendario mensaje,List<Integer> destinatarios);
    
    public List<Calendario> listarCalendarioForaneo(int usuarioID);
    public List<ActividadCalendario> listarCalendarioPropio(int usuarioID);
}
