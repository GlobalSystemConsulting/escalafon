/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Asiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaCorriente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentasEfectivo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.DetalleCuenta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.HechosLibro;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroCompras;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroVentas;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ActualizarLibroYSaldosTx implements ITransaction{
    
     @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
         LibroCaja actualizarLibro = null;
         CuentasEfectivo actualizarCuenta= null;
         List<HechosLibro> actualizarHechoList=new ArrayList<>();
         String tipo;
         BigDecimal debe=null;
         BigDecimal haber=null;
    
        try{            
             
            JSONObject requestData = (JSONObject)wr.getData();  
            
            JSONObject jLibro = requestData.getJSONObject("libro");
            JSONObject jCuentaE = requestData.getJSONObject("cuentaE");
            JSONArray jAHechoL = requestData.getJSONArray("hechoL");
            
            tipo = requestData.getString("tipo");
            double importe = requestData.getDouble("importe");
            BigDecimal monto = new BigDecimal(importe).setScale(2);
            
            //datos del libro
            String nombre = jLibro.getString("nombre");          
            String observacion = jLibro.getString("observacion");           
            String feA = jLibro.getString("fechaApertura");  
            String feC = jLibro.getString("fechaCierre");                                                          
            double saldoApertura = jLibro.getDouble("saldoApertura");          
            double saldoActual = jLibro.getDouble("saldoActual");          

            int organizacionID = jLibro.getInt("organizacionID"); 
            
            int personaID = jLibro.getInt("personaID");  
            int libroID = jLibro.getInt("libroID");
            String estado = jLibro.getString("estado"); 
            
            //datos de la cuenta efectivo
            int cuentaEfectivoID = jCuentaE.getInt("cuentaEfectivoID");
            int cuentaContableID = jCuentaE.getInt("cuentaContableID");
            double SaldoAperturaCE = jCuentaE.getDouble("saldoApertura");
            double saldoActualCE = jCuentaE.getDouble("saldoActual");                                 
            
            //montos del debe y haber
            double debeL =requestData.getDouble("debe");
            double haberL= requestData.getDouble("haber");
                        
            actualizarLibro = new LibroCaja(libroID,new Organizacion(organizacionID),new Persona(personaID),nombre,observacion,new Date(feA),new Date(feC),new BigDecimal(saldoApertura),new BigDecimal(saldoActual),new Date(), wr.getIdUsuario(),estado.charAt(0));                                
            actualizarCuenta = new CuentasEfectivo((short)cuentaEfectivoID, actualizarLibro, new CuentaContable(cuentaContableID), new BigDecimal(SaldoAperturaCE),new BigDecimal(saldoActualCE), new Date(), wr.getIdUsuario(),'A');                                
            
            //datos de la hechos del libro
            for(int i=0;i<jAHechoL.length();i++){
                JSONObject jHechoL =jAHechoL.getJSONObject(i);
                
                int hechosID = jHechoL.getInt("hechosID");
                int cuentaContableHechosID = jHechoL.getInt("cuentaContableID");
                double importeD = jHechoL.getDouble("importeD");
                double importeH = jHechoL.getDouble("importeH");
                String fecha = jHechoL.getString("fecha");
                switch (tipo) {
                    case "C":
                        actualizarLibro.getHechosLibros().add(new HechosLibro((short)hechosID, actualizarLibro, new CuentaContable(cuentaContableHechosID), new Date(fecha), new BigDecimal(importeD), new BigDecimal(importeH).add(monto), new Date(), wr.getIdUsuario(),'A'));
                        break;
                    case "V":
                       actualizarLibro.getHechosLibros().add(new HechosLibro((short)hechosID, actualizarLibro, new CuentaContable(cuentaContableHechosID), new Date(fecha), new BigDecimal(importeD).add(monto), new BigDecimal(importeH), new Date(), wr.getIdUsuario(),'A'));
                        break;
                }
            }
            
            System.out.println(tipo);
             switch (tipo) {
                 case "C":
                     actualizarLibro.setSalAct(actualizarLibro.getSalAct().subtract(monto));
                     actualizarCuenta.setSalAct(actualizarCuenta.getSalAct().subtract(monto));
                     actualizarLibro.getCuentasEfectivos().add(actualizarCuenta);
                   
                     
                     haber=new BigDecimal(haberL).add(monto).setScale(2);
                     debe=new BigDecimal(debeL);

                     break;
                 case "V":
                     actualizarLibro.setSalAct(actualizarLibro.getSalAct().add(monto));
                     actualizarCuenta.setSalAct(actualizarCuenta.getSalAct().add(monto));
                     actualizarLibro.getCuentasEfectivos().add(actualizarCuenta);
                     
                     
                    haber=new BigDecimal(haberL);
                    debe=new BigDecimal(debeL).add(monto).setScale(2);

                     break;
             }
            
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo obtener datos", e.getMessage() );
        }
        
        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
      
              
        try {                        
            libroDao.update(actualizarLibro);       

              
            
        } catch (Exception e) {
            System.out.println("No se pudo encontrar datos del Libro \n"+e);
            return WebResponse.crearWebResponseError("No se pudo encontrar datos del Libro", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */ 
        DateFormat fechaHora = new SimpleDateFormat("yyyy/MM/dd");
          
        JSONObject oResponse = new JSONObject();
             
           
        oResponse.put("saldoActual",actualizarLibro.getSalAct()); 
      
        oResponse.put("debe",debe);
        oResponse.put("haber",haber);
       
        oResponse.put("saldoMes",debe.subtract(haber).setScale(2));
        
        oResponse.put("cuentaEfectivoID",actualizarCuenta.getId());     
        oResponse.put("importeCE",actualizarCuenta.getSalAct()); 
        oResponse.put("tipo",tipo); 
        
            JSONArray hechosL= new JSONArray();
            
            for(HechosLibro h:actualizarLibro.getHechosLibros()){
                
                JSONObject hecho= new JSONObject();
                
                hecho.put("hechosID",h.getId());     
                hecho.put("importeD",h.getImpDeb()); 
                hecho.put("importeH",h.getImpHab());            
                
                hechosL.put(hecho);
            }
            
        oResponse.put("hechosL",hechosL);                     

        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);        
        //Fin
    }
    
}
