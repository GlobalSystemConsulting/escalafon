package com.dremo.ucsm.gsc.sigesmed.logic.ma.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.AsistenciaEstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.AreaCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.AsistenciaEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 11/01/2017.
 */
public class RegistrarAsistenciaMasivaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(RegistrarAsistenciaMasivaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idOrg = data.getInt("org");
        int idDoc = data.getInt("doc");
        long fec = data.getLong("fec");
        int tipAsi = data.getInt("tip");//0->ie , 1->clases
        int idArea = data.optInt("are",-1);
        JSONArray matJSON = data.getJSONArray("mats");
        return registrarAsistencia(idOrg,idDoc,fec,tipAsi,idArea,matJSON);
    }

    private WebResponse registrarAsistencia(int idOrg, int idDoc, long fec, int tipAsi, int idArea, JSONArray matJSON) {
        try{
            AsistenciaEstudianteDao asisDao = (AsistenciaEstudianteDao) FactoryDao.buildDao("ma.AsistenciaEstudianteDao");
            OrganizacionDao orgDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            AreaCurricularDao areaDao = (AreaCurricularDao) FactoryDao.buildDao("maestro.plan.AreaCurricularDao");

            Organizacion org = orgDao.buscarConTipoOrganizacionYPadre(idOrg);
            Usuario usuario = docDao.buscarUsuarioPorId(idDoc);
            AreaCurricular area = areaDao.buscarPorId(idArea);
            for(int i=0; i < matJSON.length(); i++){
                JSONObject matObj = matJSON.getJSONObject(i);
                if(matObj.optInt("asid",-1)== -1){
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date(fec));
                    cal.clear(Calendar.MINUTE);
                    cal.clear(Calendar.SECOND);
                    cal.clear(Calendar.MILLISECOND);
                    AsistenciaEstudiante asistencia = new AsistenciaEstudiante(matObj.getInt("est"),cal.getTime(),tipAsi,area == null?false :true);
                    asistencia.setOrganizacion(org);
                    asistencia.setUsuario(usuario);
                    asistencia.setArea(area);
                    asistencia.setMatricula(asisDao.buscarMatricula(matObj.getInt("id")));
                    asisDao.insert(asistencia);
                    matObj.put("asid",asistencia.getAsiEstId());
                }else{
                    AsistenciaEstudiante asis = asisDao.buscarAsistenciaEstudiante(matObj.getInt("asid"));
                    if(idArea != -1 ) asis.setArea(areaDao.buscarPorId(idArea));
                    asis.setEstAsi(matObj.getInt("est"));
                    asisDao.update(asis);
                }

            }
            return WebResponse.crearWebResponseExito("exito",matJSON);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarAsistencia",e);
            return WebResponse.crearWebResponseError("Error");
        }
    }
}
