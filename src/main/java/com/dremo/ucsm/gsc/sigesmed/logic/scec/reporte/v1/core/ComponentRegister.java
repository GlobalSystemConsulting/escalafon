/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.scec.reporte.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.scec.reporte.v1.tx.*;


/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    
    @Override

    public WebComponent createComponent() {
        WebComponent scecComponent = new WebComponent(Sigesmed.SUBMODULO_COMISIONES);
        scecComponent.setName("reportes");
        scecComponent.setVersion(1);
        scecComponent.addTransactionPOST("reporteReunion",ReporteReunionTx.class);
        scecComponent.addTransactionGET("listarOrganizaciones",ListarOrganizacionesTx.class);
        scecComponent.addTransactionGET("estadisticaGlobal",ListarDataEstadisticaGlobal.class);


        return scecComponent;
    }
 
}
