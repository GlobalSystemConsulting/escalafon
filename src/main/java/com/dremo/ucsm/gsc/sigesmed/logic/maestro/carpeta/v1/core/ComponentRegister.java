package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx.*;
/**
 * Created by Administrador on 10/10/2016.
 */
public class ComponentRegister implements IComponentRegister {
    @Override
    public WebComponent createComponent() {
        WebComponent component = new WebComponent(Sigesmed.SUBMODULO_MAESTRO);
        component.setName("carpeta_digital");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarCarpetas", ListarCarpetasTx.class);
        component.addTransactionGET("listarDetalleCarpetaDocente", ListarDetalleCarpetaDocenteTx.class);
        component.addTransactionGET("listarDetalleCarpetaAdmin", ListarDetalleCarpetaAdminTx.class);
        component.addTransactionGET("listarSeccionesCarpeta", ListarSeccionesCarpetaTx.class);
        component.addTransactionGET("reporteCarpeta",ReporteCarpetaPedagogicaTx.class);
        component.addTransactionPOST("registrarCarpeta", RegistrarCarpetaTx.class);
        component.addTransactionPOST("registrarSeccion", RegistrarSeccionCarpetaTx.class);
        component.addTransactionPOST("registrarContenido", RegistrarContenidoCarpetaTx.class);
        component.addTransactionPOST("registrarArchivoCarpeta", RegistrarArchivoCarpetaTx.class);

        component.addTransactionPUT("editarCarpeta",ActualizarCarpetaTx.class);
        component.addTransactionPUT("editarSeccion",EditarSeccionCarpetaTx.class);
        component.addTransactionPUT("editarContenido",EditarContenidoCarpetaTx.class);

        component.addTransactionDELETE("eliminarCarpeta",EliminarCarpetaTx.class);
        component.addTransactionDELETE("eliminarSeccion",EliminarSeccionCarpetaTx.class);
        component.addTransactionDELETE("eliminarContenido",EliminarContenidoCarpetaTx.class);
        return component;
    }
}
