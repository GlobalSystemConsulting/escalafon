/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.zona.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ZonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Zona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class AgregarZonasTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarZonasTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        Ubicacion zona = null;
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            String codZon = requestData.optString("codZon");
            //Character estReg = requestData.optString("estReg").charAt(0);
            //Date fecMod = (Date)requestData.getString("fecMod");
            //////////historialCambioZonas
            String nomZon = requestData.optString("nomZon");
            /////////trabajadores
            //Integer usuMod = requestData.getInt("usuMod");
            //Integer zonId = requestData.getInt("zonId");
            zona = new Ubicacion();
            zona.setUbiCod(codZon);
            zona.setUbiNom(nomZon);
            zona.setEstReg('A');
            zona.setFecMod(new Date());
            zona.setUsuMod(wr.getIdUsuario());
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nueva Zona",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        UbicacionDao ubicacionDao = (UbicacionDao) FactoryDao.buildDao("se.UbicacionDao");
        try {
            ubicacionDao.insert(zona);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva Zona",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("codZon", zona.getUbiCod());
        oResponse.put("nomZon", zona.getUbiNom());
        oResponse.put("zonaId", zona.getUbiId());
                
        return WebResponse.crearWebResponseExito("El registro de la Zona se realizo correctamente", oResponse);

    }
    
}
