package com.dremo.ucsm.gsc.sigesmed.logic.ma.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.AsistenciaEstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.AsistenciaEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.JustificacionInasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 12/01/2017.
 */
public class RegistrarJustificacionInasistenciaTx implements ITransaction {
    private static Logger logger = Logger.getLogger(RegistrarJustificacionInasistenciaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idEst = data.getInt("perid");
        int idAsi = data.getInt("asid");
        String desJus = data.getString("des");
        String nom = data.optString("nom");
        JSONObject filjson = data.optJSONObject("arc");
        return registrarJustificacion(idAsi,idEst,desJus,nom,filjson);
    }

    private WebResponse registrarJustificacion(int idAsi,int idEst, String desJus,String nomf, JSONObject filjson) {
        try{
            AsistenciaEstudianteDao asiDao = (AsistenciaEstudianteDao) FactoryDao.buildDao("ma.AsistenciaEstudianteDao");
            AsistenciaEstudiante asistencia = asiDao.buscarAsistenciaEstudiante(idAsi);
            JustificacionInasistencia justificacion = new JustificacionInasistencia(asistencia,desJus,new Date());
            if(filjson != null && filjson.length() > 0){
                FileJsonObject miF = new FileJsonObject( filjson ,"just_" + idAsi +"_alum_"+ idEst + "_" + nomf);
                BuildFile.buildFromBase64("justificacion_inasistencia_alumnos", miF.getName(), miF.getData());
                justificacion.setDocAdjJus(miF.getName());
            }
            asiDao.registrarJustificacion(justificacion);
            return WebResponse.crearWebResponseExito("Se registro correctamente");
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrar justificacion",e);
            return WebResponse.crearWebResponseError("No se registro Correctamente");
        }
    }


}
