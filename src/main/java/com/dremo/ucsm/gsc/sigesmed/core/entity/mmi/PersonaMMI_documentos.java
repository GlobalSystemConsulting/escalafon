package com.dremo.ucsm.gsc.sigesmed.core.entity.mmi;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "persona", schema = "pedagogico", uniqueConstraints = @UniqueConstraint(columnNames = "dni")
)
public class PersonaMMI_documentos implements java.io.Serializable {

    private long perId;
    private Integer usuMod;
    private Date fecMod;
    private Character estReg;
    private Set documentosVarioses = new HashSet(0);

    public PersonaMMI_documentos() {
    }

    public PersonaMMI_documentos(long perId) {
        this.perId = perId;
    }

    public PersonaMMI_documentos(long perId, Integer usuMod, Date fecMod, 
            Character estReg, Set documentosVarioses) {
        this.perId = perId;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.documentosVarioses = documentosVarioses;
    }

    @Id
    @Column(name = "per_id", unique = true, nullable = false)
    public long getPerId() {
        return this.perId;
    }

    public void setPerId(long perId) {
        this.perId = perId;
    }

    @Column(name = "usu_mod")
    public Integer getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length = 29)
    public Date getFecMod() {
        return this.fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    @Column(name = "est_reg", length = 1)
    public Character getEstReg() {
        return this.estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "persona")
    public Set getDocumentosVarioses() {
        return this.documentosVarioses;
    }

    public void setDocumentosVarioses(Set documentosVarioses) {
        this.documentosVarioses = documentosVarioses;
    }
}
