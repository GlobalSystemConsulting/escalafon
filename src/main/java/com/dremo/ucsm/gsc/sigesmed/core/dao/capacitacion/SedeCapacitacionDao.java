package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;

import java.util.List;

public interface SedeCapacitacionDao extends GenericDao<SedeCapacitacion> {
    SedeCapacitacion buscarSedePorId(int id);
    SedeCapacitacion buscarConCronograma(int id);
    List<SedeCapacitacion> buscarSedesPorCapacitacion(int idCap);
    SedeCapacitacion buscarConCronogramaHorario(int id);
    SedeCapacitacion buscarSedeRegPar(int id);
    SedeCapacitacion buscarSedeReporte(int codSed);
}
