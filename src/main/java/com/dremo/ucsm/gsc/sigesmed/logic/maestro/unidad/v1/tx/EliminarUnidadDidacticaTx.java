package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ModuloAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProyectoAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 16/12/2016.
 */
public class EliminarUnidadDidacticaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EliminarUnidadDidacticaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idUnidad = data.getInt("cod");
        return eliminarUnidadDidactica(idUnidad);
    }

    private WebResponse eliminarUnidadDidactica(int idUnidad) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            unidadDao.eliminarUnidadDidactica(idUnidad);
            return WebResponse.crearWebResponseExito("Exito al eliminar la unidad didactica");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarMaterialUnidadDidactica",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la unidad didactica");
        }
    }
}
