/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.datosOrganigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DatosOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.DatosOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class ActualizarDatosOrganigramaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarDatosOrganigramaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{    
            JSONObject requestData = (JSONObject)wr.getData();
            String desDat = requestData.optString("desDat");
            String codDat = requestData.optString("codDat");
            String nomDat = requestData.optString("nomDat");
            Integer datId = requestData.getInt("datId");
            Integer anioDat= requestData.getInt("anioDat");
            //////////trabajadores
            //Integer usuMod = requestData.getInt("usuMod");
            return actualizarDatosOrganigrama(datId,codDat,nomDat,desDat,anioDat);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    private WebResponse actualizarDatosOrganigrama(Integer plaId,String codPla,String nomPla,String abrPla, Integer anioDat) {
        try{
            DatosOrganigramaDao datOrganigramaDao = (DatosOrganigramaDao)FactoryDao.buildDao("se.DatosOrganigramaDao");        
            DatosOrganigrama datosOrganigrama = datOrganigramaDao.buscarXId(plaId);
            
            datosOrganigrama.setCodDatOrgi(codPla);
            datosOrganigrama.setDesDatOrgi(abrPla);
            datosOrganigrama.setNomDatOrgi(nomPla);
            datosOrganigrama.setAnioDatOrgi(anioDat);
            
            datOrganigramaDao.update(datosOrganigrama);
            JSONObject oResponse = new JSONObject();
            oResponse.put("datId", datosOrganigrama.getDatOrgiId());
            oResponse.put("codDat", datosOrganigrama.getCodDatOrgi());
            oResponse.put("nomDat", datosOrganigrama.getNomDatOrgi());
            oResponse.put("desDat", datosOrganigrama.getDesDatOrgi());
            oResponse.put("anioDat", datosOrganigrama.getAnioDatOrgi());
            return WebResponse.crearWebResponseExito(" Datos Organigrama actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("Error, los  Datos Organigramano fue actualizada");
        }
    }
}
