/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ListarCargosTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        

        List<TrabajadorCargo> cargos = new ArrayList<>();
        PersonalDao personalDao = (PersonalDao)FactoryDao.buildDao("cpe.PersonalDao");
        try{
            cargos =personalDao.listarCargoTrabajador();
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Cargos de los Trabajadores", e.getMessage() );
        }

        JSONArray miArray = new JSONArray();
       
        
        for(TrabajadorCargo car:cargos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("id",car.getCrgTraIde());
            oResponse.put("nombre",car.getCrgTraNom());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

