/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.MatriculaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarPadresPorOrganizacionTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        int orgId = requestData.getInt("orgId");
                
        List<Matricula> matriculas = null;
        MatriculaDao matriculaDao = (MatriculaDao)FactoryDao.buildDao("di.MatriculaDao");
        
        try{
            matriculas = matriculaDao.MatriculasxApoderado(orgId);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar a los padres de familia \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar a los padres de familia ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Matricula matricula:matriculas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("perId", matricula.getApoderado().getPersona().getPerId());
            oResponse.put("perDNI",matricula.getApoderado().getPersona().getDni());
            oResponse.put("orgNom",matricula.getOrganizacion().getNom());
            oResponse.put("perMat",matricula.getApoderado().getPersona().getApeMat());
            oResponse.put("perPat",matricula.getApoderado().getPersona().getApePat());
            oResponse.put("perNom",matricula.getApoderado().getPersona().getNom());          
            oResponse.put("perDir",matricula.getApoderado().getPersona().getPerDir());
            oResponse.put("perEma",matricula.getApoderado().getPersona().getEmail());
            oResponse.put("perFij",matricula.getApoderado().getPersona().getFij());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
                
    }
    
}
