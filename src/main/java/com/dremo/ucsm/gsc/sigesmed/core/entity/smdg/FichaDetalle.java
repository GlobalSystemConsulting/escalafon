/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "ficha_detalle", schema = "institucional")
@IdClass(FichaDetallePK.class)
public class FichaDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "fev_doc_id")
    private int fevDocId;
    @Id
    @Column(name = "pin_id")
    private int pinId;
    
    @Column(name = "fde_pun")
    private Integer fdePun;
    @JoinColumn(name = "pin_id", referencedColumnName = "pin_id", insertable = false, updatable = false)
    @ManyToOne(fetch=FetchType.LAZY)
    private PlantillaIndicadores indicador;
//    @JoinColumn(name = "pgr_id", referencedColumnName = "pgr_id")
//    @ManyToOne(fetch=FetchType.LAZY)
//    private PlantillaGrupo grupo;
//    @JoinColumn(name = "pfi_ins_id", referencedColumnName = "pfi_ins_id")
//    @ManyToOne(fetch=FetchType.LAZY)
//    private PlantillaFichaInstitucional plantilla;
    @JoinColumn(name = "fev_doc_id", referencedColumnName = "fev_doc_id", insertable = false, updatable = false)
    @ManyToOne(fetch=FetchType.LAZY)
    private FichaEvaluacionDocumentos ficha;

    public FichaDetalle() {
    }
    
    public FichaDetalle(int pinId, Integer fdePun) {        
        this.pinId = pinId;
        this.fdePun = fdePun;        
//        this.grupo = grupo;
//        this.plantilla = plantilla;        
    }
    
    public FichaDetalle(int fevDocId, int pinId, Integer fdePun) {
        this.fevDocId = fevDocId;
        this.pinId = pinId;
        this.fdePun = fdePun;        
//        this.grupo = grupo;
//        this.plantilla = plantilla;        
    }
    
    public FichaDetalle(FichaEvaluacionDocumentos ficha, int pinId, Integer fdePun) {        
        this.pinId = pinId;
        this.fdePun = fdePun;       
//        this.grupo = grupo;
//        this.plantilla = plantilla;
        this.ficha = ficha;
    }

    public FichaDetalle(int fevDocId, int pinId, Integer fdePun, PlantillaIndicadores indicador, FichaEvaluacionDocumentos ficha) {
        this.fevDocId = fevDocId;
        this.pinId = pinId;
        this.fdePun = fdePun;
        this.indicador = indicador;
//        this.grupo = grupo;
//        this.plantilla = plantilla;
        this.ficha = ficha;
    }
    

    public Integer getFdePun() {
        return fdePun;
    }

    public void setFdePun(Integer fdePun) {
        this.fdePun = fdePun;
    }

    public PlantillaIndicadores getIndicador() {
        return indicador;
    }

    public void setIndicador(PlantillaIndicadores indicador) {
        this.indicador = indicador;
    }

//    public PlantillaGrupo getGrupo() {
//        return grupo;
//    }
//
//    public void setGrupo(PlantillaGrupo grupo) {
//        this.grupo = grupo;
//    }
//
//    public PlantillaFichaInstitucional getPlantilla() {
//        return plantilla;
//    }
//
//    public void setPlantilla(PlantillaFichaInstitucional plantilla) {
//        this.plantilla = plantilla;
//    }

    public FichaEvaluacionDocumentos getFicha() {
        return ficha;
    }

    public void setFicha(FichaEvaluacionDocumentos ficha) {
        this.ficha = ficha;
    }
    
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (fichaDetallePK != null ? fichaDetallePK.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof FichaDetalle)) {
//            return false;
//        }
//        FichaDetalle other = (FichaDetalle) object;
//        if ((this.fichaDetallePK == null && other.fichaDetallePK != null) || (this.fichaDetallePK != null && !this.fichaDetallePK.equals(other.fichaDetallePK))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "entidades.smdg.FichaDetalle[ fichaDetallePK=" + fichaDetallePK + " ]";
//    }

    public int getPinId() {
        return pinId;
    }

    public void setPinId(int pinId) {
        this.pinId = pinId;
    }

    public int getFevDocId() {
        return fevDocId;
    }

    public void setFevDocId(int fevDocId) {
        this.fevDocId = fevDocId;
    }
    
}
