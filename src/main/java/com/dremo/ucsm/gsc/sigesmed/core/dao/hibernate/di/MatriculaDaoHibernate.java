/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.di.MatriculaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Matricula;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class MatriculaDaoHibernate extends GenericDaoHibernate<Matricula> implements MatriculaDao{
    
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    
    @Override
    public List<Matricula> MatriculasxApoderado(int orgId){
        List<Matricula> matriculas=null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            //listar FuncionSistemas
            //String hql = "SELECT p FROM Persona p" ;
            String hql = "SELECT m FROM Matricula m "
                    + "join fetch m.apoderado a "
                    + "join fetch a.persona "
                    + "join fetch m.organizacion o "
                    + "WHERE (o.orgId = " + orgId + " OR o.organizacionPadre = "+ orgId +")";
            
            Query query = session.createQuery(hql);            
            matriculas = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return matriculas;
    }
    
    @Override
    public List<Matricula> MatriculasxEstudiante(int orgId){
        List<Matricula> matriculas=null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            //listar FuncionSistemas
            //String hql = "SELECT p FROM Persona p" ;
            String hql = "SELECT m FROM Matricula m "
                    + "join fetch m.estudiante e "
                    + "join fetch e.persona "
                    + "join fetch m.organizacion o "
                    + "WHERE (o.orgId = " + orgId + " OR o.organizacionPadre = "+ orgId +")";
            Query query = session.createQuery(hql);            
            matriculas = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar el Directorio Interno \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return matriculas;
    }
    
}
