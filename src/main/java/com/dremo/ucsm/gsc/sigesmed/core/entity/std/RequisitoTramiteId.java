/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

import java.io.Serializable;

/**
 *
 * @author abel
 */
public final class RequisitoTramiteId implements Serializable {
    
    private static final long serialVersionUID = 1562260205094677677L;
    private Integer tipoTramite;
    private int reqTraId;

    public RequisitoTramiteId() {
    }

    public RequisitoTramiteId(Integer tipoTramite, int reqTraId) {
        this.setTipoTramite(tipoTramite);
        this.setReqTraId(reqTraId);
    }

    @Override
    public int hashCode() {
        return ((this.getTipoTramite() == null
                ? 0 : this.getTipoTramite().hashCode())
                ^ ((int) this.getReqTraId()));
    }

    @Override
    public boolean equals(Object otherOb) {

        if (this == otherOb) {
            return true;
        }
        if (!(otherOb instanceof RequisitoTramiteId)) {
            return false;
        }
        RequisitoTramiteId other = (RequisitoTramiteId) otherOb;
        return ((this.getTipoTramite() == null
                ? other.getTipoTramite() == null : this.getTipoTramite()
                .equals(other.getTipoTramite()))
                && (this.getReqTraId() == other.getReqTraId()));
    }

    @Override
    public String toString() {
        return "" + getTipoTramite() + "-" + getReqTraId();
    }

    public Integer getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(Integer tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public int getReqTraId() {
        return reqTraId;
    }

    public void setReqTraId(int reqTraId) {
        this.reqTraId = reqTraId;
    }
    
}
