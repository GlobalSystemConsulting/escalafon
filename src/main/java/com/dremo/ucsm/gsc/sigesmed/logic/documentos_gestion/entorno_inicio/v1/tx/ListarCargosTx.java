/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.TrabajadorCargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author christian
 */

public class ListarCargosTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        //Lectura de base de datos
        TrabajadorCargoDao traCrgDao = (TrabajadorCargoDao)FactoryDao.buildDao("rdg.TrabajadorCargoDao");
        List<TrabajadorCargo> listCrgs = null;
        try{
            listCrgs = traCrgDao.buscarTodos(TrabajadorCargo.class);
        }
        catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo listar los cargos"+e.getMessage());
        }
        
        JSONArray listRes = new JSONArray();
        for(TrabajadorCargo iteCrg : listCrgs){
            JSONObject ob = new JSONObject();
            ob.put("id", iteCrg.getCrgTraIde());
            ob.put("nom", iteCrg.getCrgTraNom());
            listRes.put(ob);
        }
        
        return WebResponse.crearWebResponseExito("Se listo los cargos satisfactoriamente", listRes);
    }
    
}
