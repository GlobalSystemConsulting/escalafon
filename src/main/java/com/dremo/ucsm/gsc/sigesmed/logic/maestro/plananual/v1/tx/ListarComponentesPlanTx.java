package com.dremo.ucsm.gsc.sigesmed.logic.maestro.plananual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 23/10/16.
 */
public class ListarComponentesPlanTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(ListarComponentesPlanTx.class.getName());
    private static final int CURRICULA = 1;
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            JSONObject response = new JSONObject();
            List<Grado> grados = progDao.buscarGradosDisenoCurricular(CURRICULA);
            JSONArray array = new JSONArray();
            for(Grado g : grados){
                array.put(new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"graId","nom"},
                        new String[]{"cod","nom"},
                        g
                )));
            }
            response.put("grados",array);
            array = new JSONArray();
            List<AreaCurricular> areas = progDao.buscarAreaCurricularCurricula(CURRICULA);
            for (AreaCurricular a : areas){
                array.put(new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"areCurId","nom"},
                        new String[]{"cod","nom"},
                        a
                )));
            }
            response.put("areas",array);
            return WebResponse.crearWebResponseExito("Se listo correctamente",response);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listar componestes",e);
            return WebResponse.crearWebResponseError("No se pudieron listar los componentes");
        }
    }
}
