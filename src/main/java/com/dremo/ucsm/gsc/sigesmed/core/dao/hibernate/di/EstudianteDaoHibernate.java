/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.di.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Estudiante;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class EstudianteDaoHibernate extends GenericDaoHibernate<Estudiante> implements EstudianteDao{
 
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    
    @Override
    public List<Estudiante> datosEstudiante(){
        List<Estudiante> edus = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT e FROM Estudiante e JOIN FETCH e.persona";
            Query query = session.createQuery(hql);            
            edus = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return edus;
    }
    
    @Override
    public List listarEstudiantes(String s, int orgId){
        
        //codigo necesario cuando el atributo "nom" se usa
        String[] cmp = s.split(",");
        s="";
        for(int i = 0; i<cmp.length - 1; ++i){
            if(cmp[i].equals("o.nom")){
                s += cmp[i] + " as orgnom,";
            } 
            else    s+=cmp[i] + ",";
        }
        if(cmp[cmp.length-1].equals("o.nom")){
            s += cmp[cmp.length-1] + " as orgnom";
        }
        else{
            s += cmp[cmp.length-1];
        }
        //fin
        
        List edus = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            //listar ultimo codigo de tramite
            SQLQuery query = session.createSQLQuery("select "+s+" from pedagogico.estudiante e "
                    + "join pedagogico.persona p on e.per_id = p.per_id "
                    + "join pedagogico.matricula m on m.est_per_id = p.per_id "
                    + "join organizacion o on m.org_id = o.org_id " 
                    + "WHERE (m.org_id = " + orgId + " OR o.org_pad_id= " + orgId + ")");
            
            edus = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return edus;
    }
}
