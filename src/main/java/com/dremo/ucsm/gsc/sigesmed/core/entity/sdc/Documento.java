package com.dremo.ucsm.gsc.sigesmed.core.entity.sdc;


import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="documento" ,schema="administrativo")
public class Documento  implements java.io.Serializable {


    @Id
    @Column(name="doc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_ducumento", sequenceName="administrativo.documento_doc_id_seq" )
    @GeneratedValue(generator="secuencia_ducumento")
    private Integer docId;
    
    @Column(name="doc_usu_id")
    private Integer usuarioId;
  
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="doc_pla_id", nullable=false)
    private Plantilla docPlaId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="doc_fec_reg")
    private Date fecRegistro;
   
    @OneToMany(mappedBy="docId",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    private List<ContenidoDocumento> contenidoDocumentos;

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="doc_pid_id")
    private ImagenPlantilla imagenAdjunta;

  
    
    public Documento() {
    }

    public Documento(Integer docId) {
        this.docId = docId;
    }

    public Documento(Integer usuarioId, Plantilla docPlaId, Date fecRegistro) {
       
        this.usuarioId = usuarioId;
        this.docPlaId = docPlaId;
        this.fecRegistro = fecRegistro;
    }

    public Documento(Integer docId, Integer usuarioId, Plantilla docPlaId, Date fecRegistro) {
        this.docId = docId;
        this.usuarioId = usuarioId;
        this.docPlaId = docPlaId;
        this.fecRegistro = fecRegistro;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

 

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Plantilla getPlaId() {
        return docPlaId;
    }

    public void setPlaId(Plantilla docPlaId) {
        this.docPlaId = docPlaId;
    }

    public Date getFecRegistro() {
        return fecRegistro;
    }

    public void setFecRegistro(Date fecRegistro) {
        this.fecRegistro = fecRegistro;
    }

    public List<ContenidoDocumento> getContenidoDocumento() {
        return contenidoDocumentos;
    }
    
    public void setContenidoDocumento(List<ContenidoDocumento> contenidoDocumentos) {
        this.contenidoDocumentos = contenidoDocumentos;
    }

    public Plantilla getDocPlaId() {
        return docPlaId;
    }

    public void setDocPlaId(Plantilla docPlaId) {
        this.docPlaId = docPlaId;
    }

    public List<ContenidoDocumento> getContenidoDocumentos() {
        return contenidoDocumentos;
    }

    public void setContenidoDocumentos(List<ContenidoDocumento> contenidoDocumentos) {
        this.contenidoDocumentos = contenidoDocumentos;
    }

    public ImagenPlantilla getImagenAdjunta() {
        return imagenAdjunta;
    }

    public void setImagenAdjunta(ImagenPlantilla imagenAdjunta) {
        this.imagenAdjunta = imagenAdjunta;
    }
      
    
}


