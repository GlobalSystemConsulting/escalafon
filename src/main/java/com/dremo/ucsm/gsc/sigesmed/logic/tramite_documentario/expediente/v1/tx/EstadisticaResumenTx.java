/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.ExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EntidadCantidadModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abel
 */
public class EstadisticaResumenTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Date desde = null;
        Date hasta = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            if(!requestData.getString("desde").contentEquals(""))
                desde = new SimpleDateFormat("dd/M/yyyy").parse( requestData.getString("desde"));
            if(!requestData.getString("hasta").contentEquals(""))
                hasta = new SimpleDateFormat("dd/M/yyyy HH:mm:ss").parse( requestData.getString("hasta") + " 23:59:59" );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<EntidadCantidadModel> expedientes = null;
        ExpedienteDao expDao = (ExpedienteDao)FactoryDao.buildDao("std.ExpedienteDao");
        try{
            expedientes = expDao.cantidadExpedientesPorFecha(desde, hasta);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo realizar la estadistica", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray aOrg = new JSONArray();
        
        for(EntidadCantidadModel a:expedientes){
            
            JSONObject o = new JSONObject();
            
            o.put("organizacion", a.nombre );
            o.put("expedientes", a.num1 );
            o.put("finalizados", a.num2 );
            o.put("entregados", a.num3 );
            
            aOrg.put(o);
            
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",aOrg);
        //Fin
    }
    
}

