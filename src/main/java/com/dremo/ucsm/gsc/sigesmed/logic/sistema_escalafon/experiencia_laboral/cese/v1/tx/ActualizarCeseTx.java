/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.cese.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CeseDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cese;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarCeseTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(ActualizarCeseTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer cesId = requestData.getInt("cesId");
            String entEmi = requestData.getString("entEmi");
            Integer tipDocId = requestData.getInt("tipDocId");
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = null;
            if(requestData.getString("fecDoc").length() > 0){
                fecDoc = sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            }
            
            Integer jorLabId = requestData.getInt("jorLabId");
            Date fecIni = null;
            if(requestData.getString("fecIni").length() > 0){
                fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));
            }
            Date fecFin = null;
            if(requestData.getString("fecFin").length() > 0){
                fecFin = sdi.parse(requestData.getString("fecFin").substring(0, 10));
            }
            
            String motCes = requestData.getString("motCes");
            
            int aniosPro = requestData.getInt("aniosPro");
            int mesesPro = requestData.getInt("mesesPro");
            int diasPro = requestData.getInt("diasPro");
            
            return actualizarCese(cesId, entEmi, tipDocId, numDoc, fecDoc, jorLabId, fecIni, fecFin, motCes, wr.getIdUsuario(),aniosPro,mesesPro,diasPro);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar cese",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarCese(Integer cesId, String entEmi, int tipDocId, String numDoc, Date fecDoc,
            int jorLabId, Date fecIni, Date fecFin, String motCes, int usuMod, int aniosPro, int mesesPro, int diasPro) {
        try{
            CeseDao ceseDao = (CeseDao)FactoryDao.buildDao("se.CeseDao");        
            Cese cese = ceseDao.buscarPorId(cesId);

            cese.setEntEmiRes(entEmi);
            cese.setTipDocId(tipDocId);
            cese.setNumDoc(numDoc);
            cese.setFecDoc(fecDoc);
            cese.setJorLabId(jorLabId);
            //ceseDao.update(cese);
            cese.setFecIni(fecIni);
            cese.setFecFin(fecFin);
            cese.setMotCes(motCes);
            cese.setUsuMod(usuMod);
            cese.setFecMod(new Date());
            cese.setAnioPro(aniosPro);
            cese.setMesPro(mesesPro);
            cese.setDiaPro(diasPro);
            
            ceseDao.update(cese);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
            
            oResponse.put("cesId", cese.getCesId());
            oResponse.put("entEmiRes", null != cese.getEntEmiRes()?cese.getEntEmiRes():"");
            oResponse.put("numDoc", null != cese.getNumDoc() ? cese.getNumDoc():"");
            if(cese.getFecDoc()!=null)
                oResponse.put("fecDoc", sdo.format(cese.getFecDoc()));
            else
                oResponse.put("fecDoc", "");

            oResponse.put("jorLabId", cese.getJorLabId());
            oResponse.put("motCes", null != cese.getMotCes() ? cese.getMotCes():"");
            if(cese.getFecIni()!=null)
                oResponse.put("fecIni", sdo.format(cese.getFecIni()));
            else
                oResponse.put("fecIni", "");

            if(cese.getFecFin()!=null)
                oResponse.put("fecTer", sdo.format(cese.getFecFin()));
            else
                oResponse.put("fecTer", "");
            oResponse.put("tipDocId", cese.getTipDocId());
            oResponse.put("motCes", null != cese.getMotCes()?cese.getMotCes():"");
            
            oResponse.put("tipCes", null != cese.getTipIntId()?cese.getTipIntId():"");
            oResponse.put("aniosPro", null != cese.getAnioPro()?cese.getAnioPro():"");
            oResponse.put("mesesPro", null != cese.getMesPro()?cese.getMesPro():"");
            oResponse.put("diasPro", null != cese.getDiaPro()?cese.getDiaPro():"");

            return WebResponse.crearWebResponseExito("Cese actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarCese",e);
            return WebResponse.crearWebResponseError("Error, el cese no fue actualizado");
        }
    } 
}
