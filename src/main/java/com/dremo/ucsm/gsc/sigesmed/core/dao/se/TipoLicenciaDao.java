/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoLicencia;
import java.util.List;

public interface TipoLicenciaDao {
    public List<TipoLicencia> listarAll();
    public TipoLicencia buscarPorId(Integer tipLicId);
}
