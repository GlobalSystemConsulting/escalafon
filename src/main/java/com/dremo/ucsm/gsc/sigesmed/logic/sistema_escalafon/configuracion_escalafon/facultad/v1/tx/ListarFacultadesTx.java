/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.facultad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FacultadDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Facultad;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ListarFacultadesTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarFacultadesTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<Facultad> facultades = null;
        FacultadDao facultadDao = (FacultadDao)FactoryDao.buildDao("se.FacultadDao");
        
        try{
            facultades = facultadDao.listarxFacultad();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar facultades",e);
            System.out.println("No se pudo listar las facultades.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas facultades.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Facultad c: facultades ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("facId",c.getFacId());
            oResponse.put("codFac",c.getCodFac());
            oResponse.put("nomFac",c.getNomFac());
            oResponse.put("fecMod",c.getFecMod());
            oResponse.put("UsuMod",c.getUsuMod());
            oResponse.put("estReg",c.getEstReg());
            oResponse.put("orgaId",c.getOrganismo().getOrgaId());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las facultades fueron listadas exitosamente", miArray);
    }
}
