package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="plan_estudios" ,schema="institucional" )
public class PlanEstudios  implements java.io.Serializable {

    @Id
    @Column(name="pla_est_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_plan_estudios", sequenceName="institucional.plan_estudios_pla_est_id_seq" )
    @GeneratedValue(generator="secuencia_plan_estudios")
    private int plaEstId;
    @Column(name="nom",length=64)
    private String nom;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="año_esc", nullable=false)
    private Date añoEsc;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="org_id")
    private int orgId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id",updatable = false,insertable = false)
    private Organizacion organizacion;
    
    @OneToMany(mappedBy="planEstudios",cascade=CascadeType.PERSIST)
    private List<PlanNivel> niveles;

    public PlanEstudios() {
    }
    public PlanEstudios(int plaEstId) {
        this.plaEstId = plaEstId;
    }
    public PlanEstudios(int plaEstId, String nom, int orgId, Date añoEsc, Date fecMod, int usuMod, char estReg) {
       this.plaEstId = plaEstId;
       this.nom = nom;
       this.orgId = orgId;
       this.añoEsc = añoEsc;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getPlaEstId() {
        return this.plaEstId;
    }    
    public void setPlaEstId(int plaEstId) {
        this.plaEstId = plaEstId;
    }
    
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getAñoEsc() {
        return this.añoEsc;
    }
    public void setAñoEsc(Date añoEsc) {
        this.añoEsc = añoEsc;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getOrgId() {
        return this.orgId;
    }    
    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }
    
    public Organizacion getOrganizacion() {
        return this.organizacion;
    }
    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }
    
    public List<PlanNivel> getNiveles() {
        return this.niveles;
    }
    public void setNiveles(List<PlanNivel> niveles) {
        this.niveles = niveles;
    }
}


