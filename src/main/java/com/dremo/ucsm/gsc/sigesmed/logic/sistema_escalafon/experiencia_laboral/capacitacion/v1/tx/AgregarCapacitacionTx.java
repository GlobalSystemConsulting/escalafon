/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Capacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarCapacitacionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarCapacitacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Capacitacion capacitacion = null;
        
        Integer perId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            perId = requestData.getInt("perId");
            String nom = requestData.getString("nom");
            String tip = requestData.getString("tip");
            Date fec = requestData.getString("fec").equals("")?null:sdi.parse(requestData.getString("fec").substring(0, 10));
            Integer cal = requestData.getInt("cal");
            String lug = requestData.getString("lug");
            
            capacitacion = new Capacitacion(new Persona(perId), nom, tip, fec, cal, lug, wr.getIdUsuario(), new Date(), 'A');
            System.out.println(capacitacion);
            

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nueva capacitacion",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        CapacitacionDao capacitacionDao = (CapacitacionDao) FactoryDao.buildDao("se.CapacitacionDao");
        try {
            capacitacionDao.insert(capacitacion);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego capacitacion", "Detalle: "+perId+" ficha ", wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva capacitacion",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("capId", capacitacion.getCapId());
        oResponse.put("nom", capacitacion.getNom()==null?"":capacitacion.getNom());
        oResponse.put("tip", capacitacion.getTip()==null?"":capacitacion.getTip());
        oResponse.put("fec", capacitacion.getFec()==null?"":sdi.format(capacitacion.getFec()));
        oResponse.put("cal", capacitacion.getCal());
        oResponse.put("lug", capacitacion.getLug()==null?"":capacitacion.getLug());
                
        return WebResponse.crearWebResponseExito("El registro de la capacitacion se realizo correctamente", oResponse);
        //Fin
    }
    
}
