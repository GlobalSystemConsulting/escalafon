/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "licencia" , schema="administrativo")

public class Licencia implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "lic_id")
    private Integer licId;
    
    @Size(max = 2147483647)
    @Column(name = "ent_emi_res")
    private String entEmiRes;
    
    @Size(max = 2147483647)
    @Column(name = "tip_doc")
    private String tipDoc;
    
    @Size(max = 2147483647)
    @Column(name = "num_doc")
    private String numDoc;
    
    @Column(name = "fec_doc")
    @Temporal(TemporalType.DATE)
    private Date fecDoc;
    
    @Column(name = "est_goc")
    private Boolean estGoc;
    
    @Size(max = 2147483647)
    @Column(name = "car")
    private String car;
    
    @Column(name = "fec_ini")
    @Temporal(TemporalType.DATE)
    private Date fecIni;
    
    @Column(name = "fec_ter")
    @Temporal(TemporalType.DATE)
    private Date fecTer;
    
    @Size(max = 2147483647)
    @Column(name = "mot_lic")
    private String motLic;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria; 
    
    @Size(max = 2147483647)
    @Column(name = "num_inf")
    private String numInf;
    
    @Column(name = "tip")
    private Character tip;
    
    @Column(name = "fec_inf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecInf;
    
    @Column(name = "fec_rec_inf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecRecInf;
    
    @Column(name = "est_pro")
    private String estPro;

    @Column(name = "dat_orgi_id")
    private Integer datOrgiId;
    
    @Column(name = "tip_orgi_id")
    private String tipOrgiId;
        
    @Column(name = "orgi_id")
    private String orgiId;
    
    @Column(name = "tip_doc_id")
    private Integer tipDocId;
    
    @Column(name = "tip_lic_id")
    private Integer tipLicId;
    
    @Column(name = "est_pro_id")
    private Integer estProId;
    
    @Column(name = "orgi_act_id")
    private Integer orgiActId;

    @Column(name = "cat_act_id")
    private Integer catActId;

    @Column(name = "car_act_id")
    private Integer carActId; 
    
    @Column(name = "tip_doc_inf_id")
    private Integer tipDocInfId;
    
    @Column(name = "tip_doc_mem_id")
    private Integer tipDocMemId;
    
    @Column(name = "num_mem")
    private String numMem;
    
    @Column(name = "fec_doc_mem")
    @Temporal(TemporalType.DATE)
    private Date fecDocMem;
        
    @Column(name = "fec_rei_mem")
    @Temporal(TemporalType.DATE)
    private Date fecReiMem;
    
    @Column(name = "dia_pro")
    private Integer diaPro;
        
    @Column(name = "mes_pro")
    private Integer mesPro;
            
    @Column(name = "anio_pro")
    private Integer anioPro;
    
    public Licencia() {
    }

    public Licencia(Integer licId) {
        this.licId = licId;
    }

    public Licencia(FichaEscalafonaria fichaEscalafonaria, String entEmiRes, String tipDoc, String numDoc, 
            Date fecDoc, String car, Boolean estGoc, Date fecIni, Date fecTer, String motLic, String estPro, Integer usuMod, 
            Date fecMod, Character estReg, Integer tipDocInfId, Integer tipDocMemId,String numMem,Date fecDocMem, Date fecReiMem ) {
        this.entEmiRes = entEmiRes;
        this.tipDoc = tipDoc;
        this.numDoc = numDoc;
        this.fecDoc = fecDoc;
        this.car = car;
        this.estGoc = estGoc;
        this.fecIni = fecIni;
        this.fecTer = fecTer;
        this.motLic = motLic;
        this.estPro = estPro;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.tipDocInfId=tipDocInfId;
        this.tipDocMemId=tipDocMemId;
        this.numMem=numMem;
        this.fecDocMem=fecDocMem;
        this.fecReiMem=fecReiMem;
        
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    public Licencia(String entEmiRes, String numDoc, Date fecDoc, Boolean estGoc, Date fecIni, Date fecTer, String motLic, Integer usuMod, Date fecMod, Character estReg, FichaEscalafonaria fichaEscalafonaria, String numInf, Date fecInf, Date fecRecInf, Integer tipDocId, Integer tipLicId, Integer estProId,
    Integer tipDocInfId, Integer tipDocMemId,String numMem,Date fecDocMem, Date fecReiMem) {
        this.entEmiRes = entEmiRes;
        this.numDoc = numDoc;
        this.fecDoc = fecDoc;
        this.estGoc = estGoc;
        this.fecIni = fecIni;
        this.fecTer = fecTer;
        this.motLic = motLic;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.numInf = numInf;
        this.fecInf = fecInf;
        this.fecRecInf = fecRecInf;
        this.tipDocId = tipDocId;
        this.tipLicId = tipLicId;
        this.estProId = estProId;
        
        this.tipDocInfId=tipDocInfId;
        this.tipDocMemId=tipDocMemId;
        this.numMem=numMem;
        this.fecDocMem=fecDocMem;
        this.fecReiMem=fecReiMem;
    }

    public Integer getLicId() {
        return licId;
    }

    public void setLicId(Integer licId) {
        this.licId = licId;
    }
    
    public String getEstPro() {
        return estPro;
    }

    public void setEstPro(String estPro) {
        this.estPro = estPro;
    }

    public String getEntEmiRes() {
        return entEmiRes;
    }

    public void setEntEmiRes(String entEmiRes) {
        this.entEmiRes = entEmiRes;
    }

    public String getTipDoc() {
        return tipDoc;
    }

    public void setTipDoc(String tipDoc) {
        this.tipDoc = tipDoc;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public Date getFecDoc() {
        return fecDoc;
    }

    public void setFecDoc(Date fecDoc) {
        this.fecDoc = fecDoc;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public Boolean getEstGoc() {
        return estGoc;
    }

    public void setEstGoc(Boolean estGoc) {
        this.estGoc = estGoc;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecTer() {
        return fecTer;
    }

    public void setFecTer(Date fecTer) {
        this.fecTer = fecTer;
    }

    public String getMotLic() {
        return motLic;
    }

    public void setMotLic(String motLic) {
        this.motLic = motLic;
    }

    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setFichaEscalafonaria(FichaEscalafonaria fichaEscalafonaria) {
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    
    public String getNumInf() {
        return numInf;
    }

    public void setNumInf(String numInf) {
        this.numInf = numInf;
    }
    
    public Date getFecInf() {
        return fecInf;
    }

    public void setFecInf(Date fecInf) {
        this.fecInf = fecInf;
    }
    
    public Date getFecRecInf() {
        return fecRecInf;
    }

    public void setFecRecInf(Date fecRecInf) {
        this.fecRecInf = fecRecInf;
    }
    
    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public Integer getDatOrgiId() {
        return datOrgiId;
    }

    public void setDatOrgiId(Integer datOrgiId) {
        this.datOrgiId = datOrgiId;
    }

    public String getTipOrgiId() {
        return tipOrgiId;
    }

    public void setTipOrgiId(String tipOrgiId) {
        this.tipOrgiId = tipOrgiId;
    }

    public String getOrgiId() {
        return orgiId;
    }

    public void setOrgiId(String orgiId) {
        this.orgiId = orgiId;
    }

    public Integer getTipDocId() {
        return tipDocId;
    }

    public void setTipDocId(Integer tipDocId) {
        this.tipDocId = tipDocId;
    }

    public Integer getTipLicId() {
        return tipLicId;
    }

    public void setTipLicId(Integer tipLicId) {
        this.tipLicId = tipLicId;
    }

    public Integer getEstProId() {
        return estProId;
    }

    public void setEstProId(Integer estProId) {
        this.estProId = estProId;
    }

    public Integer getCarActId() {
        return carActId;
    }

    public void setCarActId(Integer carActId) {
        this.carActId = carActId;
    }

    public Integer getOrgiActId() {
        return orgiActId;
    }

    public void setOrgiActId(Integer orgiActId) {
        this.orgiActId = orgiActId;
    }

    public Integer getCatActId() {
        return catActId;
    }

    public void setCatActId(Integer catActId) {
        this.catActId = catActId;
    }

    public Integer getTipDocInfId() {
        return tipDocInfId;
    }

    public void setTipDocInfId(Integer tipDocInfId) {
        this.tipDocInfId = tipDocInfId;
    }

    public Integer getTipDocMemId() {
        return tipDocMemId;
    }

    public void setTipDocMemId(Integer tipDocMemId) {
        this.tipDocMemId = tipDocMemId;
    }

    public String getNumMem() {
        return numMem;
    }

    public void setNumMem(String numMem) {
        this.numMem = numMem;
    }

    public Date getFecDocMem() {
        return fecDocMem;
    }

    public void setFecDocMem(Date fecDocMem) {
        this.fecDocMem = fecDocMem;
    }

    public Date getFecReiMem() {
        return fecReiMem;
    }

    public void setFecReiMem(Date fecReiMem) {
        this.fecReiMem = fecReiMem;
    }

    public Integer getDiaPro() {
        return diaPro;
    }

    public void setDiaPro(Integer diaPro) {
        this.diaPro = diaPro;
    }

    public Integer getMesPro() {
        return mesPro;
    }

    public void setMesPro(Integer mesPro) {
        this.mesPro = mesPro;
    }

    public Integer getAnioPro() {
        return anioPro;
    }

    public void setAnioPro(Integer anioPro) {
        this.anioPro = anioPro;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (licId != null ? licId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Licencia)) {
            return false;
        }
        Licencia other = (Licencia) object;
        if ((this.licId == null && other.licId != null) || (this.licId != null && !this.licId.equals(other.licId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Licencia{" + "licId=" + licId + ", entEmiRes=" + entEmiRes + ", tipDoc=" + tipDoc + ", numDoc=" + numDoc + ", estGoc=" + estGoc + ", fecIni=" + fecIni + ", fecTer=" + fecTer + ", motLic=" + motLic + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + '}';
    }


    
}