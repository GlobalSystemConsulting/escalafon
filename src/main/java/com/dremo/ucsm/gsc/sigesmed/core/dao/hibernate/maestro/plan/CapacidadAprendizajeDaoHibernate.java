package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CapacidadAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CapacidadAprendizaje;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Created by Administrador on 13/10/2016.
 */
public class CapacidadAprendizajeDaoHibernate extends GenericDaoHibernate<CapacidadAprendizaje> implements CapacidadAprendizajeDao{
    @Override
    public CapacidadAprendizaje buscarPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(CapacidadAprendizaje.class)
                    .add(Restrictions.eq("capId",id));
            return (CapacidadAprendizaje) criteria.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
