package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "capacitador_curso_capacitacion", schema = "pedagogico")
public class CapacitadorCursoCapacitacion implements Serializable {

    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "sedCapId", column = @Column(name = "sed_cap_id", nullable = false)),
        @AttributeOverride(name = "perId", column = @Column(name = "per_id", nullable = false))})
    private CapacitadorCursoCapacitacionId id = new CapacitadorCursoCapacitacionId();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sed_cap_id", updatable = false, insertable = false)
    private SedeCapacitacion sedCap;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "per_id", updatable = false, insertable = false)
    private Persona per;

    @Column(name = "car", nullable = false, length = 60)
    private String car;

    public CapacitadorCursoCapacitacion() {
    }

    public CapacitadorCursoCapacitacion(SedeCapacitacion sedCap, Persona per, String car) {
        this.sedCap = sedCap;
        this.car = car;
        this.per = per;
    }

    public CapacitadorCursoCapacitacion(CapacitadorCursoCapacitacionId id, String car) {
        this.id = id;
        this.car = car;
    }

    public CapacitadorCursoCapacitacionId getId() {
        return id;
    }

    public void setId(CapacitadorCursoCapacitacionId id) {
        this.id = id;
    }

    public SedeCapacitacion getSedCap() {
        return sedCap;
    }

    public void setSedCap(SedeCapacitacion sedCap) {
        this.sedCap = sedCap;
    }

    public Persona getPer() {
        return per;
    }

    public void setPer(Persona per) {
        this.per = per;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "CapacitadorCursoCapacitacion{" + "sedCap=" + sedCap + '}';
    }
    
    
}
