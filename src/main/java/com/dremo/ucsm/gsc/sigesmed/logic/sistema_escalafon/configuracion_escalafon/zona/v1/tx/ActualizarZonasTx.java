/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.zona.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarZonasTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarZonasTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{    
            JSONObject requestData = (JSONObject)wr.getData();
            String codZon = requestData.optString("codZon");
            //Character estReg = requestData.optString("estReg").charAt(0);
            //Date fecMod = (Date)requestData.getString("fecMod");
            //////////historialCambioZonas
            String nomZon = requestData.optString("nomZon");
            /////////trabajadores
            //Integer usuMod = requestData.getInt("usuMod");
            Integer zonId = requestData.getInt("zonaId");
            return actualizarZonas(zonId,codZon,nomZon);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    private WebResponse actualizarZonas(Integer zonId,String codZon,String nomZon) {
        try{
            UbicacionDao ubicacionDao = (UbicacionDao) FactoryDao.buildDao("se.UbicacionDao");      
            Ubicacion zona = ubicacionDao.buscarPorId(zonId);
            
            zona.setUbiCod(codZon);
            zona.setUbiNom(nomZon);
            
            ubicacionDao.update(zona);
            JSONObject oResponse = new JSONObject();
            oResponse.put("codZon", zona.getUbiCod());
            oResponse.put("nomZon", zona.getUbiNom());
            return WebResponse.crearWebResponseExito(" Zona actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("Error, la  Zona no fue actualizada");
        }
    }
}

