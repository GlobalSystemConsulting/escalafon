/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoDoc;
import java.util.List;

public interface TipoDocDao {
    public List<TipoDoc> listarAll();
    public TipoDoc buscarPorId(Integer tipCatId);
}
