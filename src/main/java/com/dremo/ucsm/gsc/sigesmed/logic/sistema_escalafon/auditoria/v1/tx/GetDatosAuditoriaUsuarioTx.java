/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;

import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;


import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class GetDatosAuditoriaUsuarioTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *  Parte para la operacion en la Base de Datos
        */        
        Long usuarioID = null;
        Persona persona = null;
        PersonaDao personaDao = (PersonaDao)FactoryDao.buildDao("PersonaDao");
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            usuarioID = requestData.optLong("usu_id");
            persona = ((PersonaDao)FactoryDao.buildDao("PersonaDao")).buscarPorID(usuarioID.intValue());
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los registros de usuario de auditoria  \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los registros de usuario de auditoria ", e.getMessage() );
        }
        //EMPAQUETAMOS EN JSON///
            JSONObject oRes = new JSONObject();
            JSONObject oUsuario = new JSONObject();
            oUsuario.put("nom", persona.getNom());
            oUsuario.put("ape_pat", persona.getApePat());
            oUsuario.put("ape_mat", persona.getApeMat());
            oUsuario.put("dni", persona.getDni());
            //oUsuario.put("ape_pat", persona.getApePat());
            /*oUsuario.put("ape_mat", usuario.getPersona().getApeMat());
            oUsuario.put("user", usuario.getNom());*/
            oRes.put("usuario", oUsuario);

            
        return WebResponse.crearWebResponseExito("Se encontro usuario",oRes);
     
    }
}
