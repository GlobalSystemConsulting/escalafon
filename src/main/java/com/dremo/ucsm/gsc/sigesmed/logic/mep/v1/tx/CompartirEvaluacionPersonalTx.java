package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.EvaluacionPersonalDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 * Created by Administrador on 01/09/2016.
 */
public class CompartirEvaluacionPersonalTx extends MepGeneralTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        String strId = wr.getMetadataValue("id");
        return  compartirArchivo(Integer.parseInt(strId));
    }
    private WebResponse compartirArchivo(int idResumen){
        try {
            EvaluacionPersonalDaoHibernate dao = new EvaluacionPersonalDaoHibernate();
            return formWebResponse(dao.compartirResultado(idResumen));
        }catch (Exception e){
            return formWebResponse(false);
        }
    }
}
