/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoOrganizacion;
import java.util.List;

/**
 *
 * @author abel
 */
public interface TipoOrganizacionDao extends GenericDao<TipoOrganizacion>{
    
    public List<TipoOrganizacion> listarConRoles();
    public void eliminarRoles(int tipOrgID);
    
}

