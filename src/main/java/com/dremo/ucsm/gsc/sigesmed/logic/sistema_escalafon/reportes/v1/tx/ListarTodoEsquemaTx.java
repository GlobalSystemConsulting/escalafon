/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaGeneralDao;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
/**
 *
 * @author root
 */
public class ListarTodoEsquemaTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *  Parte para la operacion en la Base de Datos
        */        
        JSONArray requestData = (JSONArray)wr.getData();
        JSONArray consulta = new JSONArray();
        ConsultaGeneralDao consultaGeneralDao = (ConsultaGeneralDao)FactoryDao.buildDao("ConsultaGeneralDao");
        try{
            //obtenemos en el array consulta la data dinamiza producto de la consulta SQL
            consulta = consultaGeneralDao.listAllSchema();
        
        }catch(Exception e){
            System.out.println("No se pudo listar la informacion de todo esquema \n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar la informacion de todo esquema  ", e.getMessage() );
        }
        ////enviamos ladata ordenada hacia el frontEnd
        return WebResponse.crearWebResponseExito("No se pudo listar la informacion de todo esquema ",consulta);
    }
}
