/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.organismo.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.organismo.v1.tx.ActualizarOrganismosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.organismo.v1.tx.AgregarOrganismosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.organismo.v1.tx.EliminarOrganismosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.organismo.v1.tx.ListarOrganismosTx;

/**
 *
 * @author Felipe
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("organismoConfiguracion");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarOrganismos", ListarOrganismosTx.class);
        seComponent.addTransactionPUT("actualizarOrganismos", ActualizarOrganismosTx.class);
        seComponent.addTransactionPOST("agregarOrganismos", AgregarOrganismosTx.class);
        seComponent.addTransactionDELETE("eliminarOrganismos", EliminarOrganismosTx.class);
        return seComponent;
    }
}
