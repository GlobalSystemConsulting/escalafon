package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mep.TrabajadorCargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;

import java.util.List;

/**
 * Created by Administrador on 12/10/2016.
 */
public class ListarCargosTrabajadorTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            TrabajadorCargoDao cargoDao = (TrabajadorCargoDao) FactoryDao.buildDao("mep.TrabajadorCargoDao");
            List<TrabajadorCargo> cargos = cargoDao.buscarTodos(TrabajadorCargo.class);

            return WebResponse.crearWebResponseError("Se listaron los cargos",WebResponse.OK_RESPONSE).setData(
                    new JSONArray(EntityUtil.listToJSONString(
                            new String[]{"traCarId","carAli","carNom"},
                            new String[]{"id","ali","nombre"},
                            cargos
                    ))
            );
        }catch (Exception e){
            return WebResponse.crearWebResponseError("No se pudo listar los Cargos",WebResponse.BAD_RESPONSE);
        }
    }
}
