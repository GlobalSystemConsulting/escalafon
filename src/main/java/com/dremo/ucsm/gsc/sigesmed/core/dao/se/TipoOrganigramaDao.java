/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoOrganigrama;
import java.util.List;

/**
 *
 * @author felipe
 */
public interface TipoOrganigramaDao extends GenericDao<TipoOrganigrama>{
    public List<TipoOrganigrama> listarXTipoOrganigrama();
    public TipoOrganigrama buscarXId(Integer tipOrgiId);
    
    public List<TipoOrganigrama> listarxOrganigrama(int organigramaId);
    
}
