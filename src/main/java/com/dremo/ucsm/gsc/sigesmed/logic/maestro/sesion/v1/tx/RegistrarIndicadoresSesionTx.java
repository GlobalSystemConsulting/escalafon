package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Administrador on 02/12/2016.
 */
public class RegistrarIndicadoresSesionTx implements ITransaction {
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int sesId = data.getInt("sesId");
        int uniId = data.getInt("uniId");
        String uniTip = data.getString("uniTip");
        JSONArray aprenEsp = data.getJSONArray("apr");
        return registrarIndicadoresSesion(uniId,uniTip,sesId,aprenEsp);
    }

    private WebResponse registrarIndicadoresSesion(int uniId,String tip, int sesId, JSONArray aprenEsp) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            IndicadorAprendizajeDao indDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            sesionDao.eliminarIndicadoresSesionTodos(sesId);
            for(int i= 0; i < aprenEsp.length(); i++){
                JSONObject aprJSON = aprenEsp.getJSONObject(i);
                int idCom = aprJSON.getInt("com");
                int idCap = aprJSON.getInt("cap");
                int idInd = aprJSON.getInt("ind");

                CompetenciasUnidadDidactica comUnid = unidadDao.buscarCompetenciaUnidad(uniId, idCom, idCap);
                IndicadorAprendizaje indicadorAprendizaje = indDao.buscarPorId(idInd);
                SesionAprendizaje sesion = sesionDao.buscarSesionById(sesId);
                UnidadDidactica unidad = unidadDao.buscarUnidadDidactica(uniId,tip);
                IndicadoresSesionAprendizaje indicadorSesion = new IndicadoresSesionAprendizaje(sesion,unidad,comUnid.getCompetenciaCapacidad().getCom(),comUnid.getCompetenciaCapacidad().getCap(),indicadorAprendizaje);
                sesionDao.registrarIndicadorSesion(indicadorSesion);

            }
            return WebResponse.crearWebResponseExito("Se registraron los indicadores a las sesion correctamente");
        }catch (Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar los indicadores en  la sesion");
        }
    }
}
