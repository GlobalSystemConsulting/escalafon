/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

/**
 *
 * @author Administrador
 */

import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaCertificadaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaGeneralDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.MetadataConsultaGeneral;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarAtributoMetadataTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarAtributoMetadataTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        MetadataConsultaGeneral metadataConsultaGeneral = null;
        try {
            /*    
                CREATE DATA OBJECT
            */
            JSONObject data = (JSONObject) wr.getData();
            String entity_name = data.optString("entity_name");
            String atrib_name = data.optString("atrib_name");
            String alias_name = data.optString("alias_name");
            String related_function = "()-";
            String data_type = data.optString("data_type");
            String mod = data.optString("mod");
            Character est = 'A';
            
            metadataConsultaGeneral = new MetadataConsultaGeneral(entity_name,atrib_name,alias_name,related_function,data_type,mod,est );
            ConsultaGeneralDao consultaGeneralDao = (ConsultaGeneralDao) FactoryDao.buildDao("ConsultaGeneralDao");
            consultaGeneralDao.insert(metadataConsultaGeneral);
            
        } catch (Exception e) {
            logger.log(Level.SEVERE,"No se pudo almacenar el atributo metadata",e);
            System.out.println(e);
        }
        
        //Fin       
        return WebResponse.crearWebResponseExito("Registro de metadata Exito: ");
        //Fin
    }
    
}
