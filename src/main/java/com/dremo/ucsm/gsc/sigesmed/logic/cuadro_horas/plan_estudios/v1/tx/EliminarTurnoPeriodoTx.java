/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class EliminarTurnoPeriodoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        char turnoID = 0;
        char periodoID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            String turno = requestData.optString("turnoID");
            if( turno.contentEquals("") )
                periodoID = requestData.getString("periodoID").charAt(0);
            else
                turnoID = turno.charAt(0);
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        PlanEstudiosDao usuarioDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
        try{
            if(turnoID==0)
                usuarioDao.eliminarPeriodo(periodoID);
            else
                usuarioDao.eliminarTurno(turnoID);
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }    
        return WebResponse.crearWebResponseExito("Se elimino correctamente");
    }
}
