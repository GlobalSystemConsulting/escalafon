/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reconocimientos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarReconocimientoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarReconocimientoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Reconocimiento reconocimiento = null;
        
        Integer perId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            perId = requestData.getInt("perId");
            Character mot = requestData.getString("mot").charAt(0);
            String desMot = requestData.getString("desMot");
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = requestData.getString("fecDoc").equals("")?null:sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            String entEmi = requestData.getString("entEmi");
            Integer tipdocId = requestData.getInt("tipDoc");
            
            reconocimiento = new Reconocimiento(new Persona(perId), mot, desMot, numDoc, fecDoc, tipdocId, entEmi, wr.getIdUsuario(), new Date(), 'A');
            
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo reconocimiento",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
        try {
            reconocimientoDao.insert(reconocimiento);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego reconocimiento", "Detalle: "+perId+" ficha ", wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nuevo reconocimiento",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("recId", reconocimiento.getRecId());
        oResponse.put("mot", reconocimiento.getMot());
        oResponse.put("desMot", reconocimiento.getDesMot()==null?"":reconocimiento.getDesMot());
        oResponse.put("numDoc", reconocimiento.getNumDoc()==null?"":reconocimiento.getNumDoc());
        oResponse.put("fecDoc", reconocimiento.getFecDoc()==null?"":sdi.format(reconocimiento.getFecDoc()));
        oResponse.put("entEmi", reconocimiento.getEntEmi()==null?"":reconocimiento.getEntEmi());
        oResponse.put("tipDocId", reconocimiento.getTipDocId()==null?0:reconocimiento.getTipDocId());
        
        System.out.println("Reconocimiento: " + oResponse.toString());
                
        return WebResponse.crearWebResponseExito("El registro del reconocimiento se realizo correctamente", oResponse);
        //Fin
        
    }
    
}
