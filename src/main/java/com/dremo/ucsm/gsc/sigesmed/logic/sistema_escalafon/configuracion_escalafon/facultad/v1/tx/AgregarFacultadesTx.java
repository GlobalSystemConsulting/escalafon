/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.facultad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FacultadDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganismoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Facultad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organismo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class AgregarFacultadesTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarFacultadesTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        Facultad facultad = null;
        
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            String codFac = requestData.optString("codFac");
            String nomFac = requestData.optString("nomFac");
            Integer orgaId = requestData.getInt("orgaId");
            facultad = new Facultad();
            OrganismoDao organismoDao = (OrganismoDao)FactoryDao.buildDao("se.OrganismoDao");
            Organismo organismo = organismoDao.buscarPorId(orgaId);
            
            facultad.setCodFac(codFac);
            facultad.setNomFac(nomFac);
            facultad.setEstReg('A');
            facultad.setFecMod(new Date());
            facultad.setUsuMod(wr.getIdUsuario());
            facultad.setOrganismo(organismo);
            
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo Facultad",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        
        FacultadDao facultadDao = (FacultadDao) FactoryDao.buildDao("se.FacultadDao");
        try {
            facultadDao.insert(facultad);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva Facultad",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("facId", facultad.getFacId());
        oResponse.put("codFac", facultad.getCodFac());
        oResponse.put("nomFac", facultad.getNomFac());
                
        return WebResponse.crearWebResponseExito("El registro de la facultad se realizo correctamente", oResponse);

    }
    
}
