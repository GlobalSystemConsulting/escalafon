package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.AsistenciaReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.AsistenciaReunionComision;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 30/09/16.
 */
public class AsistenciaReunionComisionDaoHibernate extends GenericDaoHibernate<AsistenciaReunionComision> implements AsistenciaReunionComisionDao {
    private static final Logger logger = Logger.getLogger(AsistenciaReunionComisionDaoHibernate.class.getName());
    @Override
    public List<AsistenciaReunionComision> buscarAsistenciasPorReunion(int idReunion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(AsistenciaReunionComision.class)
                    .setFetchMode("integranteComision", FetchMode.JOIN)
                    .createCriteria("reunionComision").add(Restrictions.eq("reuId",idReunion));
            return query.list();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarAsistenciasPorReunion",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public AsistenciaReunionComision buscarAsistenciaPorReunion(int idReunion, int idPersona) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(AsistenciaReunionComision.class);
            query.createCriteria("integranteComision").createCriteria("persona").add(Restrictions.eq("perId",idPersona));
            query.createCriteria("reunionComision").add(Restrictions.eq("reuId",idReunion));
            return (AsistenciaReunionComision) query.uniqueResult();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarAsistenciasPorReunion",e);
            throw e;
        }finally {
            session.close();
        }
    }
}
