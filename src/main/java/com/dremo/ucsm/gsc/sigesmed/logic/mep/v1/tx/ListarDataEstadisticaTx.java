package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.EvaluacionPersonalDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.PuntajesEvaluacionTrabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ResultadoEstadistica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 28/08/16.
 */
public class ListarDataEstadisticaTx extends MepGeneralTx implements ITransaction {
    private final String ESTADISTICA_INDIVIUAL = "indiv";
    private final String ESTADISTICA_GRUPAL_ROL = "grup_rol";
    private final static  Logger logger = Logger.getLogger(ListarDataEstadisticaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String type = wr.getMetadataValue("type");
        JSONObject data = (JSONObject) wr.getData();
        switch (type){
            case ESTADISTICA_INDIVIUAL:
                int idTrab = data.getInt("tra");
                return dataPuntajesIndividual(idTrab);
            case ESTADISTICA_GRUPAL_ROL :
                int idOrg = data.getInt("org");
                return dataPuntajeGrupalRol(idOrg);
            default: return formWebResponse(false);
        }
    }
    private WebResponse dataPuntajeGrupalRol(int idOrg){
        try{
            EvaluacionPersonalDaoHibernate epdh = new EvaluacionPersonalDaoHibernate();
            List<ResultadoEstadistica> puntajes = epdh.getPuntajeAgrupadoTrabajadores(idOrg);
            if(puntajes.size() > 0){

                JSONObject obj = new JSONObject();
                JSONArray labels = new JSONArray();
                JSONArray values = new JSONArray();
                for(ResultadoEstadistica res: puntajes){
                    labels.put(res.getClave());
                    values.put((Double)res.getValor());
                }
                obj.put("labels",labels);
                JSONArray avalues = new JSONArray();
                avalues.put(values);
                obj.put("values",avalues);
                obj.put("series",new JSONArray("["+ "Serie A" + "]"));
                /*obj.put("key","trabajadores");
                obj.put("color","#d62728");
                obj.put("values",new JSONArray(ResultadoEstadistica.toGroupString(puntajes)));*/
                WebResponse response = formWebResponse(true);
                /*JSONArray data = new JSONArray();
                data.put(obj);
                response.setData(data);*/
                response.setData(obj);
                return response;
            }
        }catch (Exception e){
            logger.log(Level.SEVERE,logger.getName() + ":dataPuntajesIndividual()",e);
        }
        return formWebResponse(false);
    }
    private WebResponse dataPuntajesIndividual(int idTrabajador){
        try{
            EvaluacionPersonalDaoHibernate epdh = new EvaluacionPersonalDaoHibernate();
            List<PuntajesEvaluacionTrabajador> puntajes = epdh.getPuntajesEvaluacionTrabajador(idTrabajador);
            if(puntajes.size() > 0){
                JSONArray labels = new JSONArray();
                JSONArray values = new JSONArray();
                for(PuntajesEvaluacionTrabajador punt : puntajes){
                    labels.put(punt.getFecEva());
                    values.put(punt.getPun().doubleValue());
                }
                JSONObject obj = new JSONObject();
                obj.put("labels",labels);
                JSONArray avalues = new JSONArray();
                avalues.put(values);
                obj.put("values",avalues);
                obj.put("series",new JSONArray("["+ idTrabajador + "]"));
                //obj.put("key",String.valueOf(idTrabajador));
                //obj.put("values",new JSONArray(puntajes.toString()));
                //obj.put("mean",PuntajesEvaluacionTrabajador.calculateAverage(puntajes));
                WebResponse response = formWebResponse(true);
                //data.put(obj);
                //response.setData(data);
                response.setData(obj);
                return response;
            }
        }catch (Exception e){
            logger.log(Level.SEVERE,logger.getName() + ":dataPuntajesIndividual()",e);
        }
        return formWebResponse(false);
    }
}
