package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.EvaluacionesUnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.EvaluacionesUnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 03/01/2017.
 */
public class EditarEvaluacionesUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EditarEvaluacionesUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idEvaluacion = data.getInt("id");
        return editarEvaluacion(idEvaluacion,data);
    }

    private WebResponse editarEvaluacion(int idEvaluacion, JSONObject data) {
        try{
            EvaluacionesUnidadDidacticaDao evaDao = (EvaluacionesUnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.EvaluacionesUnidadDidacticaDao");

            EvaluacionesUnidadDidactica evaluacion = evaDao.buscarEvaluacionId(idEvaluacion);
            evaluacion.setDes(data.optString("des",evaluacion.getDes()));
            evaluacion.setFecMod(new Date(data.optLong("fec",evaluacion.getFecEva().getTime())));
            evaluacion.setTipEva(data.optInt("tip",evaluacion.getTipEva()));
            evaDao.update(evaluacion);
            return WebResponse.crearWebResponseExito("Se edito la evaluacion");
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarEvaluacion",e);
            return WebResponse.crearWebResponseError("no se puede regitrar la evaluacion");
        }
    }
}
