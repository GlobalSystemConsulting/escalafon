package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="plan_hora_area" ,schema="institucional" )
public class PlanHoraArea  implements java.io.Serializable {

    @Id
    @Column(name="pla_hor_are_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_planhoraarea", sequenceName="institucional.plan_hora_area_pla_hor_are_id_seq" )
    @GeneratedValue(generator="secuencia_planhoraarea")
    private int plaHorAreId;
    @Column(name="hor_obl")
    private int horDis;
    
    @Column(name="pla_niv_id")
    private int plaNivId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pla_niv_id",updatable = false,insertable = false)
    private PlanNivel planNivel;
    
    @Column(name="are_cur_id")
    private int areCurId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_cur_id",updatable = false,insertable = false)
    private AreaCurricular area;
    
    @Column(name="gra_id")
    private int graId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="gra_id",updatable = false,insertable = false)
    private Grado grado;

    public PlanHoraArea() {
    }
    public PlanHoraArea(int plaHorAreId) {
        this.plaHorAreId = plaHorAreId;
    }
    public PlanHoraArea(int plaHorAreId,int horDis,int plaNivId,int areCurId, int graId) {
       this.plaHorAreId = plaHorAreId;
       
       this.horDis = horDis;
       
       this.plaNivId = plaNivId;
       this.areCurId = areCurId;
       this.graId = graId;
    }
   
     
    public int getPlaHorAreId() {
        return this.plaHorAreId;
    }    
    public void setPlaHorAreId(int plaHorAreId) {
        this.plaHorAreId = plaHorAreId;
    }
    
    public int getHorDis() {
        return this.horDis;
    }
    public void setHorDis(int horDis) {
        this.horDis = horDis;
    }
    
    public int getPlaNivId() {
        return this.plaNivId;
    }    
    public void setPlaNivId(int plaNivId) {
        this.plaNivId = plaNivId;
    }
    
    public PlanNivel getPlanNivel() {
        return this.planNivel;
    }
    public void setPlanNivel(PlanNivel planNivel) {
        this.planNivel = planNivel;
    }
    
    public int getAreCurId() {
        return this.areCurId;
    }    
    public void setAreCurId(int areCurId) {
        this.areCurId = areCurId;
    }
    
    public AreaCurricular getArea() {
        return this.area;
    }
    public void setArea(AreaCurricular area) {
        this.area = area;
    }
    
    public int getGraId() {
        return this.graId;
    }    
    public void setGraId(int graId) {
        this.graId = graId;
    }
    
    public Grado getGrado() {
        return this.grado;
    }
    public void setGrado(Grado grado) {
        this.grado = grado;
    }
}


