/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.ver_documento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sdc.DocumentoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Documento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarDocumentosTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Integer organizacionId=-1;
        try{
        
            JSONObject requestData = (JSONObject) wr.getData();
            organizacionId = requestData.getInt("organizacionID");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar las Plantillas", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        if(organizacionId<0)
        {
            return WebResponse.crearWebResponseError("Error de Identificacion");
        }
        Organizacion org=new Organizacion(organizacionId);
        List<Documento> documentos = new ArrayList<>();
        DocumentoDao documentoDao = (DocumentoDao)FactoryDao.buildDao("sdc.DocumentoDao");
        try{
            documentos =documentoDao.listarDocumentos(org);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar las Plantillas ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
      
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        for (Documento documento : documentos) {
            JSONObject oResponse = new JSONObject();
            oResponse.put("documentoId", documento.getDocId());
            oResponse.put("plantillaId", documento.getPlaId().getPlaIde());
            oResponse.put("descripcion", documento.getPlaId().getPlaDes());
            oResponse.put("version", documento.getPlaId().getPlaVer());
            String fecha = sdf.format(documento.getFecRegistro());
            oResponse.put("fechaRegistro", fecha);
            oResponse.put("estado", documento.getPlaId().getPlaEst());
            oResponse.put("usuario", documento.getUsuarioId());
            oResponse.put("tipoDocumentoID", documento.getPlaId().getTipoDocumento().getTipDocId());
            oResponse.put("nombreDocumento", documento.getPlaId().getTipoDocumento().getNom());
            oResponse.put("archivo", documento.getPlaId().getArchivo());
            oResponse.put("url", documento.getPlaId().getUrl());
            if(documento.getImagenAdjunta()!=null)
            {
            
                oResponse.put("nombreArchivo", documento.getImagenAdjunta().getImgAdj());
            }
            miArray.put(oResponse);
        }

        return WebResponse.crearWebResponseExito("Se Listo correctamente", miArray);

    }

}
