/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Dependencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Planilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Zona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ActualizarProyeccionTx implements ITransaction{
    
    private static final Logger logger = Logger.getLogger(ActualizarProyeccionTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject response = (JSONObject)wr.getData();
        return actualizarProyeccion(response);
    }
    public WebResponse actualizarProyeccion(JSONObject data) {
        Trabajador trabajador = new Trabajador();
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        try{
            
            JSONObject rTrabajador = data.getJSONObject("trabajador");
            JSONObject tiempoServ = data.getJSONObject("tiempoServ");
            int traId=rTrabajador.getInt("traId");
            int an1 = tiempoServ.getInt("an1");
            int me1 = tiempoServ.getInt("me1");
            int di1 = tiempoServ.getInt("di1");
            
            int an2 = tiempoServ.getInt("an2");
            int me2 = tiempoServ.getInt("me2");
            int di2 = tiempoServ.getInt("di2");
            
            int totalEx = an1 * 365 + me1*30 +di1;
            int totalFE = an2 * 365 + me2*30 +di2;
            
            TrabajadorDao trabajadorDao = (TrabajadorDao)FactoryDao.buildDao("se.TrabajadorDao");
            trabajador = trabajadorDao.buscarPorId(traId);
            
            trabajador.setTiemserest(totalEx);
            trabajador.setTiemserforedu(totalFE);
            //trabajador.setTiemsertot(traId);
            
            trabajadorDao.update(trabajador);            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarProyeccion",e);
            return WebResponse.crearWebResponseError("Error, ela proyeccion del tarbajador no fue actualizado");
        }
        int dias1=trabajador.getTiemserest();
        int an1=dias1/365;
        int me1=(dias1-(an1*365))/30;
        int di1=dias1-(an1*365)-(me1*30);
        int dias2=trabajador.getTiemserforedu();
        int an2=dias2/365;
        int me2=(dias2-(an2*365))/30;
        int di2=dias2-(an2*365)-(me2*30);
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("traId", trabajador.getTraId());
        oResponse.put("an1", an1);
        oResponse.put("me1", me1);
        oResponse.put("di1", di1);
        oResponse.put("an2", an2);
        oResponse.put("me2", me2);
        oResponse.put("di2", di2);
        
        return WebResponse.crearWebResponseExito("Proyeccion actualizado exitosamente",oResponse);
    }
}
