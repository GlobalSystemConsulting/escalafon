/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.core;


import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.logic.scec.reporte.v1.tx.ReporteReunionTx;


/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    
    @Override

    public WebComponent createComponent() {
        WebComponent scecComponent = new WebComponent(Sigesmed.SUBMODULO_COMISIONES);
        scecComponent.setName("comisiones");
        scecComponent.setVersion(1);

        scecComponent.addTransactionPOST("registrarComision",RegistrarComisionTx.class);
        scecComponent.addTransactionPOST("registrarDetalleComision", RegistrarDetalleComisionTx.class);
        scecComponent.addTransactionPOST("registrarPersonaAComision", RegistrarPersonaAComisionTx.class);
        scecComponent.addTransactionPOST("registrarIntegranteComision",RegistrarIntegranteComisionTx.class);
        scecComponent.addTransactionPOST("registrarReunion",RegistrarReunionTx.class);
        scecComponent.addTransactionPOST("reporteReunion",ReporteReunionTx.class);
        scecComponent.addTransactionPOST("registrarAsistencia",RegistrarAsistenciaReunionTx.class);

        scecComponent.addTransactionGET("listarComisiones",ListarComisionesTx.class);
        scecComponent.addTransactionGET("listarComisionesDirector",ListarComisionesDirectorTx.class);
        scecComponent.addTransactionGET("listarCargosComision",ListarCargosComisionTx.class);
         scecComponent.addTransactionGET("listarDetallesComision",ListarDetallesComisionTx.class);
        scecComponent.addTransactionGET("listarIntegrantesComision",ListarIntegrantesComisionTx.class);
        scecComponent.addTransactionGET("buscarPersonaParaComision",BuscarPersonaParaComisionTx.class);
        scecComponent.addTransactionGET("listarReunionesComision",ListarReunionesComisionTx.class);
        scecComponent.addTransactionGET("listarAsistenciasReunion",ListarAsistenciasReunionTx.class);

        scecComponent.addTransactionPUT("editarComision", EditarComisionTx.class);
        scecComponent.addTransactionPUT("editarReunion", EditarReunionTx.class);
        scecComponent.addTransactionPUT("editarIntegrante", EditarIntegranteTx.class);
        scecComponent.addTransactionDELETE("eliminarComision", EliminarComisionTx.class);
        scecComponent.addTransactionDELETE("eliminarIntegranteComision",EliminarIntegranteComisionTx.class);
        scecComponent.addTransactionDELETE("eliminarReunion",EliminarReunionTx.class);
        return scecComponent;
    }
 
}
