package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.AsistenciaReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.AsistenciaReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 09/10/16.
 */
public class RegistrarAsistenciaReunionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(RegistrarAsistenciaReunionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        int com = Integer.parseInt(wr.getMetadataValue("com"));
        JSONArray data = (JSONArray)wr.getData();

        return registrarAsistencia(data,com);
    }

    private WebResponse registrarAsistencia(JSONArray data, int com) {
        try{
            AsistenciaReunionComisionDao asistenciaDao = (AsistenciaReunionComisionDao) FactoryDao.buildDao("scec.AsistenciaReunionComisionDao");
            for(int i = 0; i < data.length(); i++){
                JSONObject obj = data.getJSONObject(i);
                AsistenciaReunionComision asistencia = asistenciaDao.buscarAsistenciaPorReunion(obj.getInt("reu"),obj.getInt("cod"));
                asistencia.setEst(obj.optBoolean("est",false));
                asistenciaDao.update(asistencia);
            }
            return WebResponse.crearWebResponseExito("Se actualizo la asistencia",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarAsistencia",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la asistencia",WebResponse.BAD_RESPONSE);
        }
    }
}
