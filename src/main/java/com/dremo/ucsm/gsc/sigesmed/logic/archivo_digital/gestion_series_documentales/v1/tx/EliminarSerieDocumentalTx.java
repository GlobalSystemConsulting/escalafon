/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.SerieDocumentalDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.SerieDocumental;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
        

/**
 *
 * @author Jeferson
 */
public class EliminarSerieDocumentalTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int ser_doc_id = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            ser_doc_id = requestData.getInt("serie_id");
        }catch(Exception e){
             System.out.println(e);
             return  WebResponse.crearWebResponseError("No se pudo eliminar Serie Documental, datos incorrectos", e.getMessage() );
        }
            SerieDocumentalDAO serie_doc_dao = (SerieDocumentalDAO)FactoryDao.buildDao("sad.SerieDocumentalDAO");
         try{
             serie_doc_dao.deleteAbsolute(new SerieDocumental(ser_doc_id));
         }catch(Exception e){
             System.out.println(e);
             return  WebResponse.crearWebResponseError("No se pudo eliminar Serie Documental , datos incorrectos", e.getMessage() );
         }   
            return WebResponse.crearWebResponseExito("La serie documental se elimino correctamente");
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
    }
    
    
}
