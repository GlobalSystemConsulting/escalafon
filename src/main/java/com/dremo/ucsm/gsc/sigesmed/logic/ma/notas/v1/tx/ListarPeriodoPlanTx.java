package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 16/01/2017.
 */
public class ListarPeriodoPlanTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarPeriodoPlanTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idNiv = data.getInt("niv");
        int idPlan = data.optInt("plan",-1);
        int idOrg = data.getInt("org");
        return listarPeriodoEvaluacion(idNiv,idPlan,idOrg);

    }

    private WebResponse listarPeriodoEvaluacion(int idNiv, int idPlan,int idOrg) {
        try{
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            PlanEstudiosDao planDao = (PlanEstudiosDao) FactoryDao.buildDao("mech.PlanEstudiosDao");

            PlanEstudios plan = idPlan == -1 ? planDao.buscarVigentePorOrganizacion(idOrg) : progDao.buscarPlanEstudios(idPlan);
            Periodo periodo = progDao.listarPeriodoPlanEstudio(idNiv,plan.getPlaEstId());
            JSONObject jsonPeriodo = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"perId","nom","facPer"},
                    new String[]{"id","nom","fac"},
                    periodo
            ));
            return WebResponse.crearWebResponseExito("Se listo correctamente",jsonPeriodo);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarPeriodoEvaluacion",e);
            return WebResponse.crearWebResponseError("No se pueden listar los periodos");
        }
    }
}
