/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.categoria.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.categoria.v1.tx.*;

/**
 *
 * @author Felipe
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("categoriaConfiguracion");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarCategorias", ListarCategoriasTx.class);
        seComponent.addTransactionPUT("actualizarCategorias", ActualizarCategoriasTx.class);
        seComponent.addTransactionPOST("agregarCategorias", AgregarCategoriasTx.class);
        seComponent.addTransactionDELETE("eliminarCategorias", EliminarCategoriasTx.class);
        
        seComponent.addTransactionGET("listarxPlanilla", ListarCategoriasxPlanillaTx.class);
        return seComponent;
    }
    
}
