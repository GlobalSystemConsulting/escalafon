package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 19/12/2016.
 */
@Entity
@Table(name = "anecdotario_acciones_correctiva",schema = "pedagogico")
public class AccionesCorrectivas implements java.io.Serializable{
    @Id
    @Column(name = "ane_acc_cor",nullable = false,unique = true)
    @SequenceGenerator(name = "anecdotario_acciones_correctiva_ane_acc_cor_seq",sequenceName = "pedagogico.anecdotario_acciones_correctiva_ane_acc_cor_seq")
    @GeneratedValue(generator = "anecdotario_acciones_correctiva_ane_acc_cor_seq")
    private int aneAcCorId;

    @ManyToOne
    @JoinColumn(name="ane_id", nullable = false)
    private Anecdotario anecdotario;

    @Column(name="nom",nullable = false,length = 256)
    private String nom;

    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public AccionesCorrectivas() {
    }

    public AccionesCorrectivas(String nom) {
        this.nom = nom;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getAneAcCorId() {
        return aneAcCorId;
    }

    public void setAneAcCorId(int aneAcCorId) {
        this.aneAcCorId = aneAcCorId;
    }

    public Anecdotario getAnecdotario() {
        return anecdotario;
    }

    public void setAnecdotario(Anecdotario anecdotario) {
        this.anecdotario = anecdotario;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
