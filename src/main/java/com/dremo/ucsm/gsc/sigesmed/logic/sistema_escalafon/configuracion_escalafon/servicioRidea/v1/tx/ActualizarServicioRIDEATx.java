/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.servicioRidea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.comunication.pojo.ServicioWeb;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author User
 */
public class ActualizarServicioRIDEATx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        String servicio=null;
        try {
             JSONObject requestData = (JSONObject)wr.getData();
             servicio = requestData.optString("servicio");
        } catch (Exception e) {
            System.out.println("No se pudo obtener el valor");
        }
        try {
            //Realizamos la reflexión
            cambiarValor(servicio);
        } catch (IOException ex) {
            Logger.getLogger(ActualizarServicioRIDEATx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        AgregarOperacionAuditoria.agregarCambioAuditoria("Se modifico el servicio", "IP de servicio RIDEA" , wr.getIdUsuario(), "---", "xxx.xxx.xxx.xxx");
        return WebResponse.crearWebResponseExito("Se actualizo la dirección del servicio");
    }
    public static void cambiarValor(String ip) throws IOException{
       ServicioWeb servicioWeb=new ServicioWeb();
       servicioWeb.setIp(ip);
    }
    
}
