/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Zona;
import java.util.List;

/**
 *
 * @author Felipe
 */
public interface ZonaDao extends GenericDao<Zona>{
    public List<Zona> listarxZona ();
    public Zona buscarPorId(Integer zonId);
    
}
