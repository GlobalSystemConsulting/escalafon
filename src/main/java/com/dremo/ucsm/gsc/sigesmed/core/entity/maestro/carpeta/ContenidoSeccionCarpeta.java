package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 27/12/2016.
 */

@Entity(name = "ContenidoSeccionCarpetaMaestro")
@Table(name = "contenido_secciones_carpeta_pedagogica", schema = "pedagogico")
public class ContenidoSeccionCarpeta implements java.io.Serializable{
    @Id
    @Column(name = "con_sec_car_ped_id", unique = true, nullable = false)
    @SequenceGenerator(name = "contenido_secciones_carpeta_pedagogica_con_sec_car_ped_id_seq", sequenceName = "pedagogico.contenido_secciones_carpeta_pedagogica_con_sec_car_ped_id_seq")
    @GeneratedValue(generator = "contenido_secciones_carpeta_pedagogica_con_sec_car_ped_id_seq")
    private int conSecCarPedId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_car_ped_id",nullable = false)
    private SeccionCarpetaPedagogica seccion;
    @OneToMany(mappedBy = "contenido",fetch = FetchType.LAZY)
    private List<RutaContenidoCarpeta> rutas;
    @Column(name = "nom", length = 50)
    private String nom;
    @Column(name = "tip", length = 10)
    private String tip;
    @Column(name = "tip_usu", length = 10)
    private Integer tipUsu;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public ContenidoSeccionCarpeta() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public ContenidoSeccionCarpeta(String nom) {
        this.nom = nom;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public ContenidoSeccionCarpeta(String nom, int tipUsu){
        this.nom = nom;
        this.tipUsu = tipUsu;

        this.fecMod = new Date();
        this.estReg = 'A';
    }
    public int getConSecCarPedId() {
        return conSecCarPedId;
    }

    public void setConSecCarPedId(int conSecCarPedId) {
        this.conSecCarPedId = conSecCarPedId;
    }

    public SeccionCarpetaPedagogica getSeccion() {
        return seccion;
    }

    public void setSeccion(SeccionCarpetaPedagogica seccion) {
        this.seccion = seccion;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<RutaContenidoCarpeta> getRutas() {
        return rutas;
    }

    public void setRutas(List<RutaContenidoCarpeta> rutas) {
        this.rutas = rutas;
    }

    public Integer getTipUsu() {
        return tipUsu;
    }

    public void setTipUsu(Integer tipUsu) {
        this.tipUsu = tipUsu;
    }
}
