/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx.ActualizarCatalogoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx.EliminarCatalogoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx.InsertarCatalogoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx.ListarCatalogoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx.ListarRegistrosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx.ActualizarEntidadTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx.EliminarEntidadTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx.InsertarEntidadTx;
/**
 *
 * @author felipe
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_CONFIGURACION);        
        
        //Registrando el Nombre del componente
        component.setName("catalogo");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarCatalogo", InsertarCatalogoTx.class);
        component.addTransactionGET("listarCatalogo", ListarCatalogoTx.class);
        component.addTransactionDELETE("eliminarCatalogo", EliminarCatalogoTx.class);
        component.addTransactionPUT("actualizarCatalogo", ActualizarCatalogoTx.class);
        
        //Entidad
        component.addTransactionPOST("insertarEntidad", InsertarEntidadTx.class);
        component.addTransactionDELETE("eliminarEntidad", EliminarEntidadTx.class);
        component.addTransactionPUT("actualizarEntidad", ActualizarEntidadTx.class);
        component.addTransactionGET("listarRegistros", ListarRegistrosTx.class);
        return component;
    }
}
