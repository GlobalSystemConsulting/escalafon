/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.List;

/**
 *
 * @author abel
 */
public interface OrganizacionDao extends GenericDao<Organizacion>{
    
    public String buscarUltimoCodigo();
    public List<Organizacion> buscarConTipoOrganizacionYPadre();
    public List<Organizacion> buscarPorTipoOrganizacion(int tipoOrganizacion);
    public Organizacion buscarConTipoOrganizacionYPadre(int orgID);
    List<Organizacion> buscarHijosOrganizacion(int orgId);
    public Organizacion buscarConTipoOrganizacion(int orgId);
}
