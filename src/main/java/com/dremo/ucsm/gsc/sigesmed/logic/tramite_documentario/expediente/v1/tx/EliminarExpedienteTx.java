/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.TipoTramiteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoTramite;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class EliminarExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int tipoTramiteID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            tipoTramiteID = requestData.getInt("tipoTramiteID");
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoTramiteDao tipoTramiteDao = (TipoTramiteDao)FactoryDao.buildDao("std.TipoTramiteDao");
        try{
            tipoTramiteDao.delete(new TipoTramite(tipoTramiteID));
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar el tipo de tramite ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Tipo TipoTramite se elimino correctamente");
        //Fin
    }
}
