/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.mep;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ElementoFicha;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.FichaEvaluacionPersonal;

import java.util.List;

/**
 *
 * @author Administrador
 */
public interface FichaEvaluacionPersonalDao extends GenericDao<FichaEvaluacionPersonal>{
    void insertFichaEvaluacion(FichaEvaluacionPersonal fev);
    List<FichaEvaluacionPersonal> getFichas();
    FichaEvaluacionPersonal getFichaById(int id);
    FichaEvaluacionPersonal getFichaByCargo(String cargo);
    ElementoFicha insertElementoFicha(int id, ElementoFicha elemento);
    List<ElementoFicha> getElementos(int idFicha,Class<? extends ElementoFicha> elemento,String... variables);
}
