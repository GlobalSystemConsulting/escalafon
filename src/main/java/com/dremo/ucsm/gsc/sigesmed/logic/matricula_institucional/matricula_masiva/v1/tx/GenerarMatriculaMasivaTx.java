package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_masiva.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mech.PlanEstudiosDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GradoSeccionPlanNivelDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaInsDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.GradoIeEstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.MatriculaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.OrganizacionMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.VacanteAutomatica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import com.dremo.ucsm.gsc.sigesmed.util.MatriculaReporter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GenerarMatriculaMasivaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        GenericMMIDaoHibernate<MatriculaMMI> hb = new GenericMMIDaoHibernate<>();
        GenericMMIDaoHibernate<GradoIeEstudianteMMI> hbie = new GenericMMIDaoHibernate<>();
        GradoSeccionPlanNivelDaoHibernate gradoSeccion = new GradoSeccionPlanNivelDaoHibernate();
        PlanEstudiosDaoHibernate pev = new PlanEstudiosDaoHibernate();
        MatriculaMMIDaoHibernate matdao = new MatriculaMMIDaoHibernate();
        MatriculaReporter reporteador = new MatriculaReporter();
        MatriculaInsDaoHibernate matDaoIns = new MatriculaInsDaoHibernate();

        PlanEstudios pe;
        List<Object[]> gradosySecciones;
        ArrayList<VacanteAutomatica> myVacante = new ArrayList<>();

        long estId, regId;
        int orgOriId, orgDesId, nivId, jorEscId, graId, numEstSel, tempNumVacFree, usuMod;
        char turId;
        Integer sitMat, conMat;
        Date matFec;
        String obs, nombreEstudiante;
        Calendar cal = Calendar.getInstance();
        Date myDate;
        int year;

        ArrayList<String> idEstudiantes;
        ArrayList<String> idOrganizaciones;
        ArrayList<String> estudiantesNombres;
        ArrayList<String> estudiantesEstadoMat;

        ArrayList<Integer> numMatxSeccion = new ArrayList<>();
        ArrayList<Integer> seccionSelect = new ArrayList<>();

        JSONObject obj = new JSONObject();

        JSONArray estudiantesMatriculados = new JSONArray();
        JSONArray estudiantesError = new JSONArray();
        JSONObject temp;

        //eliminar matricula
        int oldMatId, oldGraId, oldPlaNivId;
        Character oldSecId;
        MatriculaMMI oldMattricula;

        MatriculaMMI myMatricula;
        GradoIeEstudianteMMI gradoIe;
        Object id;
        Object idie;
        EstudianteMMI estPer;
        PersonaMMI parPer;
        OrganizacionMMI orgOri;
        int flagParId;
        boolean flagError = false;
        boolean estMat;

        //matriculas multiples
        int numMatXSecSelect;
        VacanteAutomatica vacanteSelected;
        MatriculaMMI reload;

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            numEstSel = requestData.getInt("numEstSel"); //numero de matriculados
            matFec = DateUtil.getDate2String(requestData.getString("estmatFec"));  //revisar
            conMat = requestData.getInt("conMat");
            sitMat = requestData.getInt("sitMat");
            obs = requestData.getString("matObs");

            idEstudiantes = getArrayJs(requestData, "idEstudiantes");
            idOrganizaciones = getArrayJs(requestData, "idOrganizaciones");
            estudiantesNombres = getArrayJs(requestData, "estNombres");
            estudiantesEstadoMat = getArrayJs(requestData, "estEstMat");

            regId = requestData.getLong("matRegId");
            orgDesId = requestData.getInt("orgDesId");

            nivId = requestData.getInt("nivId");
            jorEscId = requestData.getInt("jorEscId");
            turId = requestData.getString("turId").charAt(0);
            graId = requestData.getInt("graId");
            usuMod = requestData.getInt("usuMod");

            PersonaMMI regPer = new PersonaMMI();
            regPer.setPerId(regId);

            OrganizacionMMI orgDes = new OrganizacionMMI();
            orgDes.setOrgId(orgDesId);

            //calcular Secciones Disponibles
            pe = pev.buscarVigentePorOrganizacion(orgDesId);
            myDate = pe.getAñoEsc();
            cal.setTime(myDate);
            year = cal.get(Calendar.YEAR);
            gradosySecciones = gradoSeccion.getGradosySeccionesDet(orgDesId, year, nivId, jorEscId, turId, graId);

            for (Object[] itm : gradosySecciones) {
                VacanteAutomatica tempVacante = new VacanteAutomatica();
                tempVacante.setGraSecId(Integer.parseInt(itm[0].toString()));
                tempVacante.setNumMaxEstMat(Integer.parseInt(itm[1].toString()));
                tempVacante.setNumTotEstMat(Integer.parseInt(itm[2].toString()));
                tempVacante.setPlaNivId(Integer.parseInt(itm[3].toString()));
                tempVacante.setNivId(Integer.parseInt(itm[4].toString()));
                tempVacante.setNivNom(itm[5].toString());
                tempVacante.setNivAbr(itm[6].toString());
                tempVacante.setJorEscId(Integer.parseInt(itm[7].toString()));
                tempVacante.setJorEscNom(itm[8].toString());
                tempVacante.setJorEscAbr(itm[9].toString());
                tempVacante.setTurId(itm[10].toString().charAt(0));
                tempVacante.setTurNom(itm[11].toString());
                tempVacante.setGraId(Integer.parseInt(itm[12].toString()));
                tempVacante.setGraNom(itm[13].toString());
                tempVacante.setSecId(itm[15].toString().charAt(0));
                myVacante.add(tempVacante);
            }

            int value;
            for (int j = 0; j < myVacante.size(); j++) {
                tempNumVacFree = myVacante.get(j).getNumMaxEstMat() - myVacante.get(j).getNumTotEstMat();
                if (tempNumVacFree > 0) {
                    value = tempNumVacFree - numEstSel;
                    if (value < 0) {
                        numMatxSeccion.add(tempNumVacFree);
                        numEstSel = numEstSel - tempNumVacFree;
                    } else {
                        numMatxSeccion.add(numEstSel);
                        numEstSel = 0;
                    }
                    seccionSelect.add(j);
                }
                if (numEstSel == 0) {
                    break;
                }
            }

            //si no hay vacantes disponibles se seleccionada la ultima seccion
            if (numEstSel != 0) {
                numMatxSeccion.add(numEstSel);
                seccionSelect.add(myVacante.size() - 1);
                numEstSel = 0;
            }

            int totalMatOkay;
            for (int k = 0; k < seccionSelect.size(); k++) {
                numMatXSecSelect = numMatxSeccion.get(k);
                totalMatOkay = numMatXSecSelect;
                vacanteSelected = myVacante.get(k);

                for (int i = 0; i < numMatXSecSelect; i++) {
                    myMatricula = new MatriculaMMI();

                    estId = Long.parseLong(idEstudiantes.get(i));
                    orgOriId = Integer.parseInt(idOrganizaciones.get(i));
                    nombreEstudiante = estudiantesNombres.get(i);
                    estMat = Boolean.parseBoolean(estudiantesEstadoMat.get(i));

                    id = hb.llave(MatriculaMMI.class, "matId");
                    if (id != null) {
                        myMatricula.setMatId((long) id + 1);
                    } else {
                        myMatricula.setMatId(1);
                    }
                    estPer = new EstudianteMMI();
                    estPer.setPerId(estId);

                    parPer = new PersonaMMI();
                    flagParId = gradoSeccion.lastPariente(estId);
                    parPer.setPerId(flagParId);

                    orgOri = new OrganizacionMMI();
                    orgOri.setOrgId(orgOriId);

                    //Agregando Elementos a Matricula
                    if (orgOriId != -1) {
                        myMatricula.setOrganizacionByOrgOriId(orgOri);
                    }

                    if (flagParId == -1) {
                        flagError = true;
                        totalMatOkay = totalMatOkay - 1;
                        temp = new JSONObject();
                        temp.put("nomEst", nombreEstudiante);
                        temp.put("error", "Falta Apoderado");
                        estudiantesError.put(temp);
                        continue;
                    }
                    myMatricula.setPersonaByApoId(parPer);

                    myMatricula.setOrganizacionByOrgDesId(orgDes);
                    myMatricula.setPersonaByRegId(regPer);

                    myMatricula.setEstudiante(estPer);
                    myMatricula.setPlaNivId(vacanteSelected.getPlaNivId());
                    myMatricula.setGraId(graId);
                    myMatricula.setSecId(vacanteSelected.getSecId());

                    myMatricula.setConMat(conMat);
                    myMatricula.setMatSit(sitMat);
                    myMatricula.setMatEst(2); //ASEPTADA
                    myMatricula.setObs(obs);

                    myMatricula.setMatFec(matFec);

                    myMatricula.setEstReg('A');
                    myMatricula.setAct(true);
                    myMatricula.setUsuMod(usuMod);
                    myMatricula.setFecMod(new Date());

                    if (estMat) {
                        oldMattricula = matdao.getMatriculaByEstId(estId);
                        oldSecId = oldMattricula.getSecId();
                        oldGraId = oldMattricula.getGraId();
                        oldPlaNivId = oldMattricula.getPlaNivId();
                        oldMatId = (int) oldMattricula.getMatId();

                        gradoSeccion.disableEstudianteMatriculado((int) estId, oldSecId, oldGraId, oldPlaNivId, oldMatId);

                    }

                    hb.saveOrUpdate(myMatricula);

                    reload = matDaoIns.load(myMatricula.getMatId());

                    //generando constancia de matricula
                    reporteador.makeConstanciaMatricula(reload,
                            vacanteSelected.getNivNom(), vacanteSelected.getJorEscAbr(),
                            vacanteSelected.getJorEscNom(), vacanteSelected.getTurNom(),
                            vacanteSelected.getGraNom());

                    gradoSeccion.ActivateEstudianteMatriculado(estId, orgDesId);

                    gradoIe = new GradoIeEstudianteMMI();
                    idie = hbie.llave(GradoIeEstudianteMMI.class, "graIeEstId");
                    if (idie != null) {
                        gradoIe.setGraIeEstId((long) idie + 1);
                    } else {
                        gradoIe.setGraIeEstId((long) 1);
                    }
                    gradoIe.setGraId(graId);
                    gradoIe.setSecId(vacanteSelected.getSecId());
                    gradoIe.setMatricula(myMatricula);
                    gradoIe.setAct(true);
                    gradoIe.setEstReg('A');
                    gradoIe.setEstGra('P');
                    
                    gradoIe.setUsuMod(usuMod);
                    gradoIe.setFecMod(new Date());

                    hbie.saveOrUpdate(gradoIe);

                    //registrando matriculados
                    temp = new JSONObject();
                    temp.put("nomEst", nombreEstudiante);
                    estudiantesMatriculados.put(temp);
                }

                if (totalMatOkay > 0) {
                    gradoSeccion.addNumMatriculasXGradoySeccion(vacanteSelected.getGraSecId(), totalMatOkay);
                }
            }

            if (flagError) {
                obj.put("arrayErrores", estudiantesError);
                obj.put("arrayMatriculados", estudiantesMatriculados);

                return WebResponse.crearWebResponseError("Estudiantes sin Pariente Registrado! ", obj);

            } else {
                return WebResponse.crearWebResponseExito("Se Generaron la matricula", "");
            }

        } catch (JSONException | NumberFormatException e) {
            obj.put("msj", "Error al Procesar Matriculas");
            return WebResponse.crearWebResponseError("Error al generar matricula! ", obj);
        }

    }

    public ArrayList<String> getArrayJs(JSONObject request, String nameReq) {
        String row = request.getString(nameReq);
        StringTokenizer str = new StringTokenizer(row.substring(1, row.length() - 1), ",");
        ArrayList<String> temp = new ArrayList<>();

        while (str.hasMoreElements()) {
            String token = str.nextToken();
            if (!token.equals(",")) {
                temp.add(token);
            }
        }

        return temp;
    }
}
