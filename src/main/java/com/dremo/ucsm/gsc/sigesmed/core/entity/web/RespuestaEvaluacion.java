package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@IdClass(RespuestaEvaluacionId.class)
@Entity
@Table(name="respuesta_evaluacion" ,schema="pedagogico")
public class RespuestaEvaluacion  implements java.io.Serializable {

    @Id 
    @Column(name="res_eva_esc_id", unique=true, nullable=false)
    private int resEvaId;
    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ban_eva_esc_id", nullable=false)
    private BandejaEvaluacion bandejaEvaluacion;
    
    @Column(name="res", length=64)
    private String respuesta;
    
    @Column(name="pun")
    private int puntos;

    public RespuestaEvaluacion() {
    }

	
    public RespuestaEvaluacion(int resEvaId, BandejaEvaluacion bandejaEvaluacion) {
        this.resEvaId = resEvaId;
        this.bandejaEvaluacion = bandejaEvaluacion;
    }
    public RespuestaEvaluacion(int resEvaId, BandejaEvaluacion bandejaEvaluacion, String respuesta) {
       this.resEvaId = resEvaId;
       this.bandejaEvaluacion = bandejaEvaluacion;
       this.respuesta = respuesta;
    }
   
     
    public int getResEvaId() {
        return this.resEvaId;
    }
    public void setResEvaId(int resEvaId) {
        this.resEvaId = resEvaId;
    }

    public BandejaEvaluacion getBandejaEvaluacion() {
        return this.bandejaEvaluacion;
    }
    
    public void setBandejaEvaluacion(BandejaEvaluacion bandejaEvaluacion) {
        this.bandejaEvaluacion = bandejaEvaluacion;
    }

    
    public String getRespuesta() {
        return this.respuesta;
    }
    
    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
    
    public int getPuntos(){
        return this.puntos;        
    }
    public void setPuntos(int puntos){
        this.puntos = puntos;
    }

}


