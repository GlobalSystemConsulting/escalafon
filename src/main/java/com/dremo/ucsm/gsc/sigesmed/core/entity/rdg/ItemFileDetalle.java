/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.rdg;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name = "item_file_detalle",schema="institucional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemFileDetalle.findAll", query = "SELECT i FROM ItemFileDetalle i"),
    @NamedQuery(name = "ItemFileDetalle.findByIfdIde", query = "SELECT i FROM ItemFileDetalle i WHERE i.ifdIde = :ifdIde"),
    @NamedQuery(name = "ItemFileDetalle.findByFecMod", query = "SELECT i FROM ItemFileDetalle i WHERE i.fecMod = :fecMod"),
    @NamedQuery(name = "ItemFileDetalle.findByUsuMod", query = "SELECT i FROM ItemFileDetalle i WHERE i.usuMod = :usuMod"),
    @NamedQuery(name = "ItemFileDetalle.findByEstReg", query = "SELECT i FROM ItemFileDetalle i WHERE i.estReg = :estReg"),
    @NamedQuery(name = "ItemFileDetalle.findByIfdUsu", query = "SELECT i FROM ItemFileDetalle i WHERE i.ifdUsu = :ifdUsu")})
public class ItemFileDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ifd_ide")
    @SequenceGenerator(name = "secuencia_itefildetalle", sequenceName="institucional.item_file_detalle_ifd_ide_seq" )
    @GeneratedValue(generator="secuencia_itefildetalle")
    private Integer ifdIde;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    @Column(name = "ifd_usu")
    private Integer ifdUsu;
//    @JoinColumn(name = "ifd_prm_ide", referencedColumnName = "prm_ide")
//    @ManyToOne
//    
//    private Permiso ifdPrmIde;
//    @JoinColumn(name = "ifd_ite_ide", referencedColumnName = "ite_ide")
//    @ManyToOne
    @JoinColumn(name = "ifd_ite_ide", referencedColumnName = "ite_ide")
    @ManyToOne
    private ItemFile ifdIteIde;

    public ItemFileDetalle() {
    }

    public ItemFileDetalle(Integer ifdIde) {
        this.ifdIde = ifdIde;
    }

    public Integer getIfdIde() {
        return ifdIde;
    }

    public void setIfdIde(Integer ifdIde) {
        this.ifdIde = ifdIde;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Integer getIfdUsu() {
        return ifdUsu;
    }

    public void setIfdUsu(Integer ifdUsu) {
        this.ifdUsu = ifdUsu;
    }

//    public Permiso getIfdPrmIde() {
//        return ifdPrmIde;
//    }
//
//    public void setIfdPrmIde(Permiso ifdPrmIde) {
//        this.ifdPrmIde = ifdPrmIde;
//    }

    public ItemFile getIfdIteIde() {
        return ifdIteIde;
    }

    public void setIfdIteIde(ItemFile ifdIteIde) {
        this.ifdIteIde = ifdIteIde;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ifdIde != null ? ifdIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemFileDetalle)) {
            return false;
        }
        ItemFileDetalle other = (ItemFileDetalle) object;
        if ((this.ifdIde == null && other.ifdIde != null) || (this.ifdIde != null && !this.ifdIde.equals(other.ifdIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.ItemFileDetalle[ ifdIde=" + ifdIde + " ]";
    }
    
}
