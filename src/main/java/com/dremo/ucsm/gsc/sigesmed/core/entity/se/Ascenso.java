/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "ascenso", schema="administrativo")
public class Ascenso implements Serializable {

    @Id
    @Column(name = "asc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_ascenso", sequenceName="administrativo.ascenso_asc_id_seq" )
    @GeneratedValue(generator="secuencia_ascenso")
    private Integer ascId;
    
    @Column(name = "num_doc")
    private String numDoc;
    
    @Column(name = "fec_doc")
    @Temporal(TemporalType.DATE)
    private Date fecDoc;
    
    @Column(name = "fec_efe")
    @Temporal(TemporalType.DATE)
    private Date fecEfe;
    
    @Column(name = "esc", length=4)
    private String esc;
    
    @Column(name = "ent_emi_res")
    private String entEmiRes;
    
    @Column(name = "tip_doc")
    private String tipDoc;
        
    @Column(name = "dep")
    private String dep;
            
    @Column(name = "car")
    private String car;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @Column(name = "tip")
    private Character tip;
    
    @Column(name = "cat_id")
    private Integer catId;
    
    @Column(name = "pla_id")
    private Integer plaId;
    
    @Column(name = "org_id")
    private Integer orgId;
    
    @Column(name = "dep_id")
    private Integer depId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria;
    
    @Column(name = "dat_orgi_id")
    private Integer datOrgiId;
    
    @Column(name = "tip_orgi_id")
    private String tipOrgiId;
        
    @Column(name = "orgi_id")
    private String orgiId;
    
    @Column(name = "orgi_ant_id")
    private Integer orgiAntId;

    @Column(name = "cat_ant_id")
    private Integer catAntId;

    @Column(name = "car_ant_id")
    private Integer carAntId; 
    
    @Column(name = "orgi_post_id")
    private Integer orgiPostId;

    @Column(name = "cat_post_id")
    private Integer catPostId;

    @Column(name = "car_post_id")
    private Integer carPostId; 
    
    @Column(name = "tip_doc_id")
    private Integer tipDocId; 
    
    @Column(name = "mot")
    private String mot; 

    public Ascenso() {
    }

    public Ascenso(Integer ascId) {
        this.ascId = ascId;
    }
    
    public Ascenso(FichaEscalafonaria fichaEscalafonaria, Character tip, String numDoc, Date fecDoc, String tipDoc, Date fecEfe, String esc, Integer catId, Integer plaId, Integer orgId, Integer depId, Integer usuMod, Date fecMod, Character estReg) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.numDoc = numDoc;
        this.fecDoc = fecDoc;
        this.fecEfe = fecEfe;
        this.esc = esc;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.tipDoc = tipDoc;
        this.catId = catId;
        this.orgId = orgId;
        this.plaId = plaId;
        this.depId = depId;
        this.tip=tip;
    }

    public int getAscId() {
        return ascId;
    }

    public void setAscId(Integer ascId) {
        this.ascId = ascId;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public Date getFecDoc() {
        return fecDoc;
    }

    public void setFecDoc(Date fecDoc) {
        this.fecDoc = fecDoc;
    }

    public Date getFecEfe() {
        return fecEfe;
    }

    public void setFecEfe(Date fecEfe) {
        this.fecEfe = fecEfe;
    }

    public String getEsc() {
        return esc;
    }

    public void setEsc(String esc) {
        this.esc = esc;
    }

    public String getEntEmiRes() {
        return entEmiRes;
    }

    public void setEntEmiRes(String entEmiRes) {
        this.entEmiRes = entEmiRes;
    }

    public String getTipDoc() {
        return tipDoc;
    }

    public void setTipDoc(String tipDoc) {
        this.tipDoc = tipDoc;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }
    
    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }
    
    public Integer getPlaId() {
        return plaId;
    }

    public void setplaId(Integer plaId) {
        this.plaId = plaId;
    }
    
    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }
    
    public Integer getDepId() {
        return depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    
    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setFichaEscalafonaria(FichaEscalafonaria fichaEscalafonaria) {
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    public Integer getDatOrgiId() {
        return datOrgiId;
    }

    public void setDatOrgiId(Integer datOrgiId) {
        this.datOrgiId = datOrgiId;
    }

    public String getTipOrgiId() {
        return tipOrgiId;
    }

    public void setTipOrgiId(String tipOrgiId) {
        this.tipOrgiId = tipOrgiId;
    }

    public String getOrgiId() {
        return orgiId;
    }

    public void setOrgiId(String orgiId) {
        this.orgiId = orgiId;
    }

    public Integer getOrgiAntId() {
        return orgiAntId;
    }

    public void setOrgiAntId(Integer orgiAntId) {
        this.orgiAntId = orgiAntId;
    }

    public Integer getCatAntId() {
        return catAntId;
    }

    public void setCatAntId(Integer catAntId) {
        this.catAntId = catAntId;
    }

    public Integer getCarAntId() {
        return carAntId;
    }

    public void setCarAntId(Integer carAntId) {
        this.carAntId = carAntId;
    }

    public Integer getOrgiPostId() {
        return orgiPostId;
    }

    public void setOrgiPostId(Integer orgiPostId) {
        this.orgiPostId = orgiPostId;
    }

    public Integer getCatPostId() {
        return catPostId;
    }

    public void setCatPostId(Integer catPostId) {
        this.catPostId = catPostId;
    }

    public Integer getCarPostId() {
        return carPostId;
    }

    public void setCarPostId(Integer carPostId) {
        this.carPostId = carPostId;
    }

    public Integer getTipDocId() {
        return tipDocId;
    }

    public void setTipDocId(Integer tipDocId) {
        this.tipDocId = tipDocId;
    }

    public String getMot() {
        return mot;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }

    
    @Override
    public String toString() {
        return "Ascenso{" + "ascId=" + ascId + ", numDoc=" + numDoc + ", fecDoc=" + fecDoc + ", fecEfe=" + fecEfe + ", esc=" + esc + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + ", fichaEscalafonaria=" + fichaEscalafonaria + '}';
    }
    
    
}
