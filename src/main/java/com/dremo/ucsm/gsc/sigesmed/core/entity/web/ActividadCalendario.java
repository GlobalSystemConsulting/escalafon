package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pedagogico.actividad_calendario" )
public class ActividadCalendario  implements java.io.Serializable {

    @Id
    @Column(name="act_cal_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_actividadcalendario", sequenceName="pedagogico.actividad_calendario_act_cal_id_seq" )
    @GeneratedValue(generator="secuencia_actividadcalendario")
    private int actCalId;
    @Column(name="tit",length=32)
    private String tit;
    @Column(name="des", length=256)
    private String des;
    
    @Column(name="pri_act")
    private char tipo;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="hor_ini", nullable=false, length=29)
    private Date horIni; 
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="hor_fin", nullable=false, length=29)
    private Date horFin;    
    
    @Column(name="usu_id")
    private int usuId;
    @Column(name="est_reg")
    private char estReg;    

    public ActividadCalendario() {
    }
    public ActividadCalendario(int actCalId) {
        this.actCalId = actCalId;
    }
    public ActividadCalendario(int actCalId,String tit, String des,Date horIni, Date horFin,char tipo,int usuId, char estReg) {
       this.actCalId = actCalId;
       this.tit = tit;
       this.des = des;
       this.horIni = horIni;
       this.horFin = horFin;
       this.tipo = tipo;
               
       this.usuId = usuId;
       this.estReg = estReg;
    }
   
     
    public int getActCalId() {
        return this.actCalId;
    }    
    public void setActCalId(int actCalId) {
        this.actCalId = actCalId;
    }
    
    public String getTit() {
        return this.tit;
    }
    public void setTit(String tit) {
        this.tit = tit;
    }    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    public Date getHorIni() {
        return this.horIni;
    }
    public void setHorIni(Date horIni) {
        this.horIni = horIni;
    }
    
    public Date getHorFin() {
        return this.horFin;
    }
    public void setHorFin(Date horFin) {
        this.horFin = horFin;
    }
    
    public char getTipo(){
        return this.tipo;
    }
    public void setTipo(char tipo){
        this.tipo = tipo;
    }
    
    public int getUsuId() {
        return this.usuId;
    }
    public void setUsuId(int usuId) {
        this.usuId = usuId;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
}


