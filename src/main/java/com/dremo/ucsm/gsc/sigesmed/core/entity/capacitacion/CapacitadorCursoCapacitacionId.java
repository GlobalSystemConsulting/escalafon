package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CapacitadorCursoCapacitacionId implements Serializable {

    @Column(name = "sed_cap_id", nullable = false)
    protected int sedCapId;

    @Column(name = "per_id", nullable = false)
    protected int perId;

    public CapacitadorCursoCapacitacionId() {
    }

    public CapacitadorCursoCapacitacionId(int sedCapId, int perId) {
        this.sedCapId = sedCapId;
        this.perId = perId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CapacitadorCursoCapacitacionId that = (CapacitadorCursoCapacitacionId) o;

        if (sedCapId != that.sedCapId) {
            return false;
        }

        return perId == that.perId;
    }

    @Override
    public int hashCode() {
        int result = sedCapId;
        result = 31 * result + perId;
        return result;
    }
}
