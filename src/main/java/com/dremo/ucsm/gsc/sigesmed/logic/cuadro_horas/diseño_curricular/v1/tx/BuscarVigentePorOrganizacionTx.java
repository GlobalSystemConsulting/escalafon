/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseño_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DiseñoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.CicloEducativo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DiseñoCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.JornadaEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.ModalidadEducacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.List;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class BuscarVigentePorOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        int orgId = 0;        
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
            orgId = requestData.getInt("organizacionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar el diseño curricular vigente, datos incorrectos", e.getMessage() );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        DiseñoCurricularDao diseñoDao = null;
        DiseñoCurricular diseño = null;
        
        try{
            diseñoDao = (DiseñoCurricularDao)FactoryDao.buildDao("mech.DiseñoCurricularDao");
            diseño = diseñoDao.buscarVigentePorOrganizacion(orgId);
            
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo encontrar diseño curricular vigente", e.getMessage() );
        }
        
        if(diseño != null){
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            int posGradoArea = 0;
            
        
            JSONObject oResponse = new JSONObject();
            oResponse.put("diseñoID",diseño.getDisCurId() );
            oResponse.put("nombre",diseño.getNom());
            oResponse.put("descripcion",diseño.getDes());
            oResponse.put("resolucion",diseño.getResDir());
            oResponse.put("fecha", sf.format(diseño.getFecCre()));
            oResponse.put("tipo",""+diseño.getTip());
            oResponse.put("estado",""+diseño.getEstReg());
            
            oResponse.put("organizacionID",diseño.getOrgId());
            
            for(ModalidadEducacion m : diseño.getModalidades()){
                JSONObject oMod = new JSONObject();
                oMod.put("modalidadID", m.getModEduId() );
                oMod.put("abreviacion", m.getAbr() );
                oMod.put("nombre", m.getNom() );
                oMod.put("descripcion", m.getDes() );

                oResponse.put("modalidad", oMod);
            }
            int pos = 0;
            
            JSONArray aCic = new JSONArray();
            for(CicloEducativo c : diseño.getCiclos()){
                JSONObject oCic = new JSONObject();
                oCic.put("cicloID", c.getCicEduId() );
                oCic.put("diseñoID", c.getDisCurId() );
                oCic.put("abreviacion", c.getAbr() );
                oCic.put("nombre", c.getNom() );
                oCic.put("descripcion", c.getDes() );
                oCic.put("i", pos++ );

                aCic.put(oCic);
            }
            oResponse.put("ciclos", aCic);
            
            pos = 0;
            JSONArray aNiv = new JSONArray();
            for(Nivel n : diseño.getNiveles()){
                JSONObject oNiv = new JSONObject();
                oNiv.put("nivelID", n.getNivId() );
                oNiv.put("diseñoID", n.getDisCurId() );
                oNiv.put("modalidadID", n.getModEduId() );
                oNiv.put("abreviacion", n.getAbr() );
                oNiv.put("nombre", n.getNom() );
                oNiv.put("descripcion", n.getDes() );
                oNiv.put("i", pos++ );

                aNiv.put( oNiv);
            }
            oResponse.put("niveles", aNiv);
            
            pos = 0;
            JSONArray aAre = new JSONArray();
            for(AreaCurricular a : diseño.getAreas()){
                JSONObject oAre = new JSONObject();
                oAre.put("areaID", a.getAreCurId() );
                oAre.put("diseñoID", a.getDisCurId() );
                oAre.put("tipo", a.getEsTal() );
                oAre.put("abreviacion", a.getAbr() );
                oAre.put("nombre", a.getNom() );
                oAre.put("descripcion", a.getDes() );
                oAre.put("i", pos++ );

                aAre.put(oAre);
            }
            oResponse.put("areas", aAre);
            
            pos = 0;
            JSONArray aGra = new JSONArray();
            for(Grado g: diseño.getGrados()){
                JSONObject oGra = new JSONObject();
                oGra.put("gradoID", g.getGraId() );
                oGra.put("diseñoID", g.getDisCurId() );
                
                for(int i=0;i<diseño.getCiclos().size();i++ )
                    if(g.getCicEduId() == diseño.getCiclos().get(i).getCicEduId()){
                        oGra.put("ciclo", diseño.getCiclos().get(i).getAbr() );
                        break;
                    }
                oGra.put("cicloID", g.getCicEduId() );
                
                for(int i=0;i<diseño.getNiveles().size();i++ )
                    if(g.getNivId() == diseño.getNiveles().get(i).getNivId()){
                        oGra.put("nivel", diseño.getNiveles().get(i).getNom() );
                        break;
                    }
                oGra.put("nivelID", g.getNivId() );
                oGra.put("abreviacion", g.getAbr() );
                oGra.put("nombre", g.getNom() );
                oGra.put("descripcion", g.getDes() );
                oGra.put("i", pos++ );

                aGra.put(oGra);
            }
            oResponse.put("grados", aGra);
            
            pos = 0;
            JSONArray aJor = new JSONArray();
            for(JornadaEscolar j: diseño.getJornadas()){
                JSONObject oJor = new JSONObject();
                oJor.put("jornadaID", j.getJorEscId() );
                oJor.put("diseñoID", j.getDisCurId() );
                
                for(int i=0;i<diseño.getNiveles().size();i++ )
                    if(j.getNivId() == diseño.getNiveles().get(i).getNivId()){
                        oJor.put("nivel", diseño.getNiveles().get(i).getNom() );
                        break;
                    }
                oJor.put("nivelID", j.getNivId() );
                oJor.put("abreviacion", j.getAbr() );
                oJor.put("nombre", j.getNom() );
                oJor.put("hObligatoria", j.getHorObl() );
                oJor.put("hLibre", j.getHorLibDis() );
                oJor.put("hTutoria", j.getHorTut() );
                oJor.put("hTotal", j.getHorTot() );
                oJor.put("descripcion", j.getDes() );
                oJor.put("i", pos++ );

                aJor.put(oJor);
            }
            oResponse.put("jornadas", aJor);
            List<Object[]> gradoAreas = diseñoDao.listarGradoAreas();
            JSONArray aGraAre = new JSONArray();
            if(gradoAreas!=null)
            for(; posGradoArea < gradoAreas.size(); posGradoArea++ ){
                Object[] row = gradoAreas.get(posGradoArea);                
                
                int jornadaID = Integer.parseInt(row[0].toString());                
                if(jornadaID != diseño.getDisCurId() )                    
                    break;
                
                JSONObject oGraAre = new JSONObject();
                oGraAre.put("nivelID", Integer.parseInt(row[1].toString()) );
                oGraAre.put("gradoID", Integer.parseInt(row[3].toString()) );
                oGraAre.put("areaID", Integer.parseInt(row[4].toString()) );
                oGraAre.put("gradoPos", Integer.parseInt(row[5].toString()) );
                oGraAre.put("areaPos", Integer.parseInt(row[6].toString()) );
                
                aGraAre.put(oGraAre);
            }
            oResponse.put("gradoAreas", aGraAre);

            return WebResponse.crearWebResponseExito("Se encontro el diseño curricular vigente",oResponse);
        }
        else{
            return WebResponse.crearWebResponseError("No se encontro el diseño curricular vigente");
        }
    }    
    
    
}
