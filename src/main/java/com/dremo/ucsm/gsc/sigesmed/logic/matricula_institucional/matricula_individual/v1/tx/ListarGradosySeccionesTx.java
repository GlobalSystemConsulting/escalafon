package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mech.PlanEstudiosDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GradoSeccionPlanNivelDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarGradosySeccionesTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        PlanEstudiosDaoHibernate pev = new PlanEstudiosDaoHibernate();
        GradoSeccionPlanNivelDaoHibernate gspnh = new GradoSeccionPlanNivelDaoHibernate();
        JSONObject requestData = (JSONObject) wr.getData();
        int orgId = requestData.getInt("orgIdUser");
        JSONArray aulas = new JSONArray();
        Calendar cal = Calendar.getInstance();
        Date myDate;
        PlanEstudios pe;
        List<Object[]> gsResponse;
        int year;
        try {
            pe = pev.buscarVigentePorOrganizacion(orgId);
            myDate = pe.getAñoEsc();
            cal.setTime(myDate);
            year = cal.get(Calendar.YEAR);

            gsResponse = gspnh.getGradosySecciones(orgId, year);
            for (Object[] itm : gsResponse) {
                JSONObject temp = new JSONObject();
                temp.put("gradoSecId", itm[0].toString());
                temp.put("gradoSecNumMax", itm[1].toString());
                temp.put("gradoSecMat", itm[2].toString());
                temp.put("planNivelId", itm[3].toString());

                temp.put("nivelId", itm[4].toString());
                temp.put("nivelNom", itm[5].toString());
                temp.put("nivelAbr", itm[6].toString());

                temp.put("jornadaId", itm[7].toString());
                temp.put("jornadaNom", itm[8].toString());
                temp.put("jornadaAbr", itm[9].toString());

                temp.put("turnoId", itm[10].toString());
                temp.put("turnoNom", itm[11].toString());

                temp.put("gradoId", itm[12].toString());
                temp.put("gradoNom", getGrado(itm[12].toString()));
                temp.put("gradoAbr", itm[14].toString());

                temp.put("seccId", itm[15].toString());
                temp.put("seccNom", itm[16].toString());
                aulas.put(temp);
            }

            return WebResponse.crearWebResponseExito("Se Listaron las Aulas", aulas);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al Listar Aulas! ", aulas);

        }

    }

    private String getGrado(String graId) {
        switch (graId) {
            case "0": {
                return "-";
            }
            case "1": {
                return "Inicial 0-2";
            }
            case "2": {
                return "Inicial 3-5";
            }
            case "3": {
                return "1er Grado";
            }
            case "4": {
                return "2do Grado";
            }
            case "5": {
                return "3er Grado";
            }
            case "6": {
                return "4to Grado";
            }
            case "7": {
                return "5to Grado";
            }
            case "8": {
                return "6to Grado";
            }
            case "9": {
                return "1er Grado";
            }
            case "10": {
                return "2do Grado";
            }
            case "11": {
                return "3er Grado ";
            }
            case "12": {
                return "4to Grado";
            }
            case "13": {
                return "5to Grado";
            }
            default: {
                return "-";
            }
        }
    }
}
