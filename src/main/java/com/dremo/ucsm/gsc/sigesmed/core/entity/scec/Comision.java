/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scec;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import org.hibernate.annotations.DynamicUpdate;

import java.util.*;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author ucsm
 */
@Entity
@Table(name="comision",schema="pedagogico")
@DynamicUpdate(value = true)
public class Comision implements java.io.Serializable{
    @Id
    @Column(name="com_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_comision", sequenceName="pedagogico.comision_com_id_seq" )
    @GeneratedValue(generator="secuencia_comision")
    private int comId;
      
    @Column(name="nom_com", length = 20)
    private String nomCom;

    @Column(name="des_com")
    private String desCom;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "fec_con_co")
    private Date fecConCo;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private Organizacion organizacion;

    @OneToMany(mappedBy = "comision", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private List<CargoDeComision> cargoDeComisiones = new ArrayList<>();

    @OneToMany(mappedBy = "comision", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private  List<ReunionComision> reuniones = new ArrayList<>();

    @OneToMany(mappedBy = "comision", fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
    private List<IntegranteComision> integrantes = new ArrayList<>();
    public Comision() {
    }

    public Comision(String nomCom, String desCom, Date fecConCo) {
        this.nomCom = nomCom;
        this.desCom = desCom;
        this.fecConCo = fecConCo;
    }

    public Comision(String nomCom, String desCom, Date fecConCo, int usuMod, Date fecMod, char estReg, Organizacion organizacion) {
        this.nomCom = nomCom;
        this.desCom = desCom;
        this.fecConCo = fecConCo;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.organizacion = organizacion;
    }

    public Comision(char estReg, Date fecMod, int usuMod, String desCom, String nomCom, Date fecConCo) {
        this.estReg = estReg;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.desCom = desCom;
        this.nomCom = nomCom;
        this.fecConCo = fecConCo;
    }

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId = comId;
    }

    public String getNomCom() {
        return nomCom;
    }

    public void setNomCom(String nomCom) {
        this.nomCom = nomCom;
    }

    public String getDesCom() {
        return desCom;
    }

    public void setDesCom(String desCom) {
        this.desCom = desCom;
    }

    public Date getFecConCo() {
        return fecConCo;
    }

    public void setFecConCo(Date fecConCo) {
        this.fecConCo = fecConCo;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public List<CargoDeComision> getCargoDeComisiones() {
        return cargoDeComisiones;
    }
    public void setCargoDeComisiones(List<CargoDeComision> cargoDeComisiones) {
        this.cargoDeComisiones = cargoDeComisiones;
    }

    public List<ReunionComision> getReuniones() {
        return reuniones;
    }

    public void setReuniones(List<ReunionComision> reuniones) {
        this.reuniones = reuniones;
    }

    public List<IntegranteComision> getIntegrantes() {
        return integrantes;
    }

    public void setIntegrantes(List<IntegranteComision> integrantes) {
        this.integrantes = integrantes;
    }
}
