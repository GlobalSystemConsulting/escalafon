/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_ficha_monitoreo.v1.tx;


import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.PlantillaFichaDao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.PlantillaFicha;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarPlantillaTx implements ITransaction{
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
                
        int plaid = requestData.optInt("plaId");
        String planom = requestData.optString("plaNom");
        String placod = requestData.optString("plaCod");
        
        PlantillaFichaDao plantillaDao = (PlantillaFichaDao)FactoryDao.buildDao("sma.PlantillaFichaDao");
        PlantillaFicha plantilla = plantillaDao.getPlantilla(plaid);
        
        plantilla.setPlfNom(planom);
        plantilla.setPlfCod(placod);
        
        plantillaDao.update(plantilla);
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("plaId",plantilla.getPlfId());
        
        return WebResponse.crearWebResponseExito("El actualizo la plantilla ", oResponse);
    }
}