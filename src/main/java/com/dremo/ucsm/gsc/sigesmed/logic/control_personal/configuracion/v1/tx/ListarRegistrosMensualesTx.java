/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ListarRegistrosMensualesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Integer organizacoinId;
        Organizacion _user =null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacoinId = requestData.getInt("organizacionID");        
            _user = new Organizacion(organizacoinId);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar la Organizacion", e.getMessage() );
        }

        Date fechaInicio;
        Date fechaHoy=new Date();
        LibroAsistenciaDao libroAsistenciaDao = (LibroAsistenciaDao)FactoryDao.buildDao("cpe.LibroAsistenciaDao");
        OrganizacionDao organizacionDao = (OrganizacionDao)FactoryDao.buildDao((String)"OrganizacionDao");
        _user = (Organizacion)organizacionDao.load(Organizacion.class, organizacoinId.intValue());
        try{
            fechaInicio =libroAsistenciaDao.getMinFechaConfiguraciones(_user);
            if(fechaInicio==null)
            {
                 return WebResponse.crearWebResponseError("No se pudo buscar de Libro Asistencia " );
            }
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo buscar de Libro Asistencia ", e.getMessage() );
        }

        Integer anhoInicio=fechaInicio.getYear()+1900;
        Integer anhoHoy=fechaHoy.getYear()+1900;
        
        JSONArray miArray = new JSONArray();
        //yyyy-MM-dd HH:mm:ss
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        for(int i=0;i<=anhoHoy-anhoInicio;i++ ){
            JSONObject obj=new JSONObject();
            obj.put("mes", "Enero");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-01-01 00:00:00");
            miArray.put(obj);
            
            obj=new JSONObject();
            obj.put("mes", "Febrero");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-02-01 00:00:00");
            miArray.put(obj);
            
            obj=new JSONObject();
            obj.put("mes", "Marzo");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-03-01 00:00:00");
            miArray.put(obj);
            
            obj=new JSONObject();
            obj.put("mes", "Abril");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-04-01 00:00:00");
            miArray.put(obj);
            
            obj=new JSONObject();
            obj.put("mes", "Mayo");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-05-01 00:00:00");
            miArray.put(obj);
            
            obj=new JSONObject();
            obj.put("mes", "Junio");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-06-01 00:00:00");
            miArray.put(obj);
            
            obj=new JSONObject();
            obj.put("mes", "Julio");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-07-01 00:00:00");
            miArray.put(obj);
            
            obj=new JSONObject();
            obj.put("mes", "Agosto");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-08-01 00:00:00");
            miArray.put(obj);
            
            obj=new JSONObject();
            obj.put("mes", "Septiembre");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-09-01 00:00:00");
            miArray.put(obj);
            
            obj=new JSONObject();
            obj.put("mes", "Octubre");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-10-01 00:00:00");
            miArray.put(obj);
            
            obj=new JSONObject();
            obj.put("mes", "Noviembre");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-11-01 00:00:00");
            miArray.put(obj);
            
            obj=new JSONObject();
            obj.put("mes", "Diciembre");
            obj.put("anho", anhoInicio+i);
            obj.put("fecha", (anhoInicio+i)+"-12-01 00:00:00");
            miArray.put(obj);
        }
        JSONObject oRes = new JSONObject();
        oRes.put("lista", miArray);
        oRes.put("nroOrg", _user.getCod());
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oRes);        
        //Fin
    }
    
}

