/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ControlPatrimonial;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.DetalleTecnicoInmuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.DetalleTecnicoMuebles;
/**
 *
 * @author Administrador
 */
public interface DetalleTecnicoInmueblesDAO extends GenericDao<DetalleTecnicoInmuebles> {
    
    
    public DetalleTecnicoInmuebles listar_detalle_inmueble(int id_inm);
}
