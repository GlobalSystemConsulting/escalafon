/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.ascenso.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarAscensoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarAscensoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            int ascId = requestData.getInt("ascId");
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            int tipDocId = requestData.getInt("tipDocId");
            Date fecEfe = sdi.parse(requestData.getString("fecEfe").substring(0, 10));
            String mot = requestData.getString("mot");
            
            return actualizarAscenso(ascId, tipDocId, numDoc, fecDoc, fecEfe, mot);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar ascenso",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarAscenso(int ascId, int tipDocId, String numDoc, Date fecDoc, Date fecEfe, String mot) {
        try{
            AscensoDao ascensoDao = (AscensoDao)FactoryDao.buildDao("se.AscensoDao");        
            Ascenso ascenso = ascensoDao.buscarPorId(ascId);

            ascenso.setTipDocId(tipDocId);
            ascenso.setNumDoc(numDoc);
            ascenso.setFecDoc(fecDoc);
            ascenso.setFecEfe(fecEfe);
            ascenso.setMot(mot);
            
            
            ascensoDao.update(ascenso);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("ascId", ascenso.getAscId());
            oResponse.put("tip", ascenso.getTip());
            oResponse.put("tipDocId", ascenso.getTipDocId());
            oResponse.put("numDoc", ascenso.getNumDoc());
            oResponse.put("fecDoc", sdo.format(ascenso.getFecDoc()));
            oResponse.put("fecEfe", sdo.format(ascenso.getFecEfe()));
            oResponse.put("mot", ascenso.getMot());
            return WebResponse.crearWebResponseExito("Ascenso actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarAscenso",e);
            return WebResponse.crearWebResponseError("Error, el ascenso no fue actualizado");
        }
    } 
    
}
