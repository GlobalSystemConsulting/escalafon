/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sdc;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sdc.PlantillaDao;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ImagenPlantilla;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Plantilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.PropiedadLetra;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author carlos
 */
public class PlantillaDaoHibernate extends GenericDaoHibernate<Plantilla> implements PlantillaDao {

    @Override
    public Plantilla buscarPorID(int clave) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Plantilla> listarPlantillas(Organizacion organizacion) {
        List<Plantilla> plantillas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT p FROM Plantilla p JOIN FETCH p.tipoDocumento tc JOIN FETCH p.plaUsu u WHERE p.plaEst != '3' AND  p.plaEst != '4' AND u.organizacion =:p1     ORDER BY p.plaIde DESC";
           
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacion);
            plantillas = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Plantillas \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo buscar las Plantillas \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return plantillas;

    }

    @Override
    public Boolean registrarPlantilla(Integer plantillaId) {
        Boolean estadoRegistro = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  Plantilla p SET p.plaEst =:p1 WHERE p.plaIde =:p2 ";

            Query query = session.createQuery(hql);
            query.setParameter("p1", "1");
            query.setParameter("p2", plantillaId);
            query.executeUpdate();
            estadoRegistro = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            estadoRegistro = Boolean.FALSE;
            System.out.println("No se pudo buscar las Plantillas \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo buscar las Plantillas \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estadoRegistro;

    }

    @Override
    public Boolean eliminarPlantilla(Integer plantillaId) {
        Boolean estado = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  Plantilla  SET plaEst = 3 WHERE plaIde = " + plantillaId;
            Query query = session.createQuery(hql);
            query.executeUpdate();
            estado = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            estado = Boolean.FALSE;
            System.out.println("No se pudo eliminar las Plantilla \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo eliminar las Plantilla \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estado;
    }

    @Override
    public Boolean plantillaEstado2editar(Integer plantillaId) {
        Boolean estadoRegistro = Boolean.FALSE;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            String hql = "UPDATE  Plantilla p SET p.plaEst =:p1 WHERE p.plaIde =:p2 ";

            Query query = session.createQuery(hql);
            query.setParameter("p1", "4");
            query.setParameter("p2", plantillaId);
            query.executeUpdate();
            estadoRegistro = Boolean.TRUE;
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            estadoRegistro = Boolean.FALSE;
            System.out.println("No se pudo cambiar estado Editado la Plantillas \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo buscar las Plantillas \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return estadoRegistro;

    }

    @Override
    public List<PropiedadLetra> listarNombresLetras() {
        List<PropiedadLetra> letras = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT DISTINCT p FROM PropiedadLetra p  ORDER BY p.proLetId";
            Query query = session.createQuery(hql);

            letras = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Fuentes \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar el nombre de Fuentes \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return letras;
    }
    
    @Override
    public Integer buscarUltimoCodigo() {
        Integer codigo = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar ultimo codigo de tramite
            String hql = "SELECT e.plaIde FROM Plantilla e  ORDER BY e.plaIde DESC ";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            codigo = ((Integer)query.uniqueResult());
            
            //solo cuando no hay ningun registro aun
            if(codigo==null)
                codigo = 0;
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return codigo;
    }

    @Override
    public List<ImagenPlantilla> listarImagenes(Plantilla plantilla) {
        List<ImagenPlantilla> imagenes = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT p FROM ImagenPlantilla p  WHERE p.plaId =:p1  ORDER BY p.plaId DESC";
           
            Query query = session.createQuery(hql);
            query.setParameter("p1", plantilla);
            imagenes = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo buscar las Imagenes \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo buscar las Imagenes \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return imagenes;
        
        
    }
}
