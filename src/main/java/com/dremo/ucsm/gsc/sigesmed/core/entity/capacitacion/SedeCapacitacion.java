package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "sedes_capacitacion", schema = "pedagogico")
public class SedeCapacitacion implements Serializable {
    @Id
    @Column(name="sed_cap_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_sedes_capacitacion", sequenceName="pedagogico.sedes_capacitacion_sed_cap_id_seq" )
    @GeneratedValue(generator="secuencia_sedes_capacitacion")
    private int sedCapId;
    
    @Column(name = "pro", length = 25)
    private String pro;

    @Column(name="dis", length = 20)
    private String dis;

    @Column(name="loc", length = 20)
    private String loc;

    @Column(name="dir",length = 20)
    private String dir;

    @Column(name="ref",length = 20)
    private String ref;

    @OneToOne(mappedBy = "sedeCapacitacion", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private CronogramaSedeCapacitacion cronograma;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cur_cap_id", nullable = false)
    CursoCapacitacion cursoCapacitacion;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sedCap")
    private Set<CapacitadorCursoCapacitacion> capacitadoresCursoCapacitacion = new HashSet<>(0);
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sedCap", cascade = CascadeType.ALL)
    private Set<TemarioCursoCapacitacion> temasCursoCapacitacion = new HashSet<>(0);
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sedeCapacitacion")
    private Set<HorarioSedeCapacitacion> horarios = new HashSet<>(0);
    
    public SedeCapacitacion() {}

    public SedeCapacitacion(CursoCapacitacion cursoCapacitacion, String pro, String dis, String loc, String dir, String ref) {
        this.cursoCapacitacion = cursoCapacitacion;
        this.pro = pro;
        this.dis = dis;
        this.loc = loc;
        this.dir = dir;
        this.ref = ref;
    }

    public int getSedCapId() {
        return sedCapId;
    }

    public void setSedCapId(int sedCapId) {
        this.sedCapId = sedCapId;
    }

    public String getPro() {
        return pro;
    }

    public void setPro(String pro) {
        this.pro = pro;
    }

    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public CronogramaSedeCapacitacion getCronograma() {
        return cronograma;
    }

    public void setCronograma(CronogramaSedeCapacitacion cronograma) {
        this.cronograma = cronograma;
    }

    public CursoCapacitacion getCursoCapacitacion() {
        return cursoCapacitacion;
    }

    public void setCursoCapacitacion(CursoCapacitacion cursoCapacitacion) {
        this.cursoCapacitacion = cursoCapacitacion;
    }

    public Set<CapacitadorCursoCapacitacion> getCapacitadoresCursoCapacitacion() {
        return capacitadoresCursoCapacitacion;
    }

    public void setCapacitadoresCursoCapacitacion(Set<CapacitadorCursoCapacitacion> capacitadoresCursoCapacitacion) {
        this.capacitadoresCursoCapacitacion = capacitadoresCursoCapacitacion;
    }

    public Set<TemarioCursoCapacitacion> getTemasCursoCapacitacion() {
        return temasCursoCapacitacion;
    }

    public void setTemasCursoCapacitacion(Set<TemarioCursoCapacitacion> temasCursoCapacitacion) {
        this.temasCursoCapacitacion = temasCursoCapacitacion;
    }

    public Set<HorarioSedeCapacitacion> getHorarios() {
        return horarios;
    }

    public void setHorarios(Set<HorarioSedeCapacitacion> horarios) {
        this.horarios = horarios;
    }

    @Override
    public String toString() {
        return "SedeCapacitacion{" + "sedCapId=" + sedCapId + '}';
    }
}
