/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectoActividadesDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectoActividadesDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoActividades;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoActividades;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Proyectos;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarActividadesTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int pacid = 0;
        int proid = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            pacid = requestData.getInt("pacid");
            proid = requestData.getInt("proid");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ProyectoActividadesDao actividadDao = (ProyectoActividadesDao)FactoryDao.buildDao("smdg.ProyectoActividadesDao");
        try{
            actividadDao.delete(new ProyectoActividades(pacid, new Proyectos(proid)));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la actividad\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la actividad", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El actividad se elimino correctamente");
        //Fin
    }
}
