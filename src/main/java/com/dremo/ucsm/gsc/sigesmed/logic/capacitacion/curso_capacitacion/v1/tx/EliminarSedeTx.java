package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 06/10/2016.
 */
public class EliminarSedeTx implements ITransaction {

    private static  final Logger logger = Logger.getLogger(EliminarSedeTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        return eliminarSede(data.getInt("cod"));
    }

    private WebResponse eliminarSede(int cod) {
        try{
            SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
            SedeCapacitacion sede = sedeCapacitacionDao.buscarConCronograma(cod);
            sedeCapacitacionDao.deleteAbsolute(sede);
            return WebResponse.crearWebResponseExito("Se elimino correctamente", WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarSede",e);
            return WebResponse.crearWebResponseError("Cambie a los participantes de la sede para eliminar",WebResponse.BAD_RESPONSE);
        }
    }

}
