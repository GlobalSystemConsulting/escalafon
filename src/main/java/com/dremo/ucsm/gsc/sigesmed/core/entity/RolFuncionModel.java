package com.dremo.ucsm.gsc.sigesmed.core.entity;

public class RolFuncionModel {
    
    public int funcionID;
    public int subModuloID;
    public String subNombre;
    public String subCodigo;
    public String subIcono;
    public int moduloID;
    public String modNombre;
    public String modCodigo;
    public String modIcono;
    public String nombre;
    public String url;
    public String clave;
    public String controlador;
    public String interfaz;
    public String icono;
    public int num;
    public int tipo;
    public String dependencias;

    public RolFuncionModel() {
    }
    
    public RolFuncionModel( int funcionID, int subModuloID,String subNombre,String subCodigo,String subIcono,
            int moduloID,String modNombre,String modCodigo,String modIcono,
            String nombre,String url,String clave,String controlador,String interfaz,String icono,
            int tipo,String dependencias,int num) {
        this.funcionID = funcionID;
        this.subModuloID = subModuloID;
        this.subNombre = subNombre;
        this.subCodigo = subCodigo;
        this.subIcono = subIcono;
        this.moduloID = moduloID;
        this.modNombre = modNombre;
        this.modCodigo = modCodigo;
        this.modIcono = modIcono;
        this.nombre = nombre;
        this.url = url;
        this.clave = clave;
        this.controlador = controlador;
        this.interfaz = interfaz;
        this.icono = icono;
        this.tipo = tipo;
        this.dependencias = dependencias;
        this.num = num;
    }
}


