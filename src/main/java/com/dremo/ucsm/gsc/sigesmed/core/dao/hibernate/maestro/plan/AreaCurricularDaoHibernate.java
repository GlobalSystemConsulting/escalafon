package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.AreaCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Administrador on 19/10/2016.
 */
public class AreaCurricularDaoHibernate extends GenericDaoHibernate<AreaCurricular> implements AreaCurricularDao{
    @Override
    public AreaCurricular buscarPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(AreaCurricular.class)
                    .setFetchMode("disenoCurr", FetchMode.JOIN)
                    .add(Restrictions.eq("areCurId", id));
            return (AreaCurricular)query.uniqueResult();
        }catch (Exception e){
            throw  e;
        }
    }

    @Override
    public List<AreaCurricular> buscarAreasConCurricula() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(AreaCurricular.class)
                    .setFetchMode("disenoCurr", FetchMode.JOIN);
            return query.list();
        }catch (Exception e){
            throw  e;
        }
    }
}
