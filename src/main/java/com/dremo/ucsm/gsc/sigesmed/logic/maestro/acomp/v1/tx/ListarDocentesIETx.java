package com.dremo.ucsm.gsc.sigesmed.logic.maestro.acomp.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 05/01/2017.
 */
public class ListarDocentesIETx implements ITransaction {
    private static Logger logger = Logger.getLogger(ListarDocentesIETx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idIE = data.getInt("org");

        return listarDocentes(idIE);
    }

    private WebResponse listarDocentes(int idIE) {
        try{
            DocenteDao docenteDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            List<Docente> docentes = docenteDao.buscarDocentesIE(idIE);
            JSONArray docentesJSON = new JSONArray();
            for(Docente docente : docentes){
                JSONObject docenteJSON = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"dni","nom","apePat","apeMat","email"},
                        new String[]{"dni","nom","pat","mat","ema"},
                        docente.getPer()
                ));
                docenteJSON.put("id",docente.getDoc_id());
                docenteJSON.put("niv",docente.getNiv());
                docentesJSON.put(docenteJSON);
            }
            return WebResponse.crearWebResponseExito("Se listo los docentes con exito",docentesJSON);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarDocentes",e);
            return WebResponse.crearWebResponseError("no se puede listar los docentes de la IE indicada");
        }
    }
}
