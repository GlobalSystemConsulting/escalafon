package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ObjetivoProgramacion;

/**
 * Created by Administrador on 24/10/2016.
 */
public interface ObjetivoProgramacionDao extends GenericDao<ObjetivoProgramacion> {
    ObjetivoProgramacion buscarObjetivoPorId(int idObj);
}
