package com.dremo.ucsm.gsc.sigesmed.logic.maestro.proys.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.proys.v1.tx.ListarProyectosDocenteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.proys.v1.tx.RegistrarProyectosDocenteTx;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx.*;

/**
 * Created by Administrador on 31/10/2016.
 */
public class ComponentRegister implements IComponentRegister {
    @Override
    public WebComponent createComponent() {
        WebComponent component = new WebComponent(Sigesmed.SUBMODULO_MAESTRO);
        component.setName("proys");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        //Competencias
        component.addTransactionGET("listarProyectosDocente", ListarProyectosDocenteTx.class);
        component.addTransactionPOST("registrarProyectoDocente", RegistrarProyectosDocenteTx.class);
        return component;
    }
}
