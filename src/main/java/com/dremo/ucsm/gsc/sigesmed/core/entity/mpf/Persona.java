package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

import com.dremo.ucsm.gsc.sigesmed.core.entity.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Entity(name="com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Persona")
@Table(name="persona" ,schema="pedagogico" )
public class Persona  implements java.io.Serializable {

    @Id
    @Column(name="per_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_persona", sequenceName="pedagogico.persona_per_id_seq" )
    @GeneratedValue(generator="secuencia_persona")
    private Long perId;
    @Column(name="dni",  nullable=false)
    private String dni;
    @Column(name="nom", nullable=false, length=30)
    private String nom;
    @Column(name="ape_mat", nullable=false, length=30)
    private String apeMat;
    @Column(name="ape_pat", nullable=false, length=30)
    private String apePat;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_nac", nullable=false, length=29)
    private Date fecNac;
    @Column(name="email", length=20)
    private String email;
    @Column(name="sex", length=1)
    private Character sexo;
    @Column(name="num_1", length=10)
    private String num1;
    @Column(name="num_2", length=10)
    private String num2;
    @Column(name="fij", length=10)
    private String fijo;
    @Column(name="per_dir")
    private String direccion;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "persona")
    private Estudiante estudiante;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="persona")
    private List<Trabajador> trabajadores = new ArrayList<>();
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="personaByPerId")
    private List<Parientes> parientes = new ArrayList<>();
    
    public Persona() {
    }
    public Persona(Long perId) {
        this.perId = perId;
    }


    public Persona(Long perId, String dni) {
        this.perId = perId;
        this.dni = dni;
    }
    public Persona(Long perId, String apeMat, String apePat, String nom, Date fecNac, String dni) {
        this.perId = perId;
        this.apeMat = apeMat;
        this.apePat = apePat;
        this.nom = nom;
        this.fecNac = fecNac;
        this.dni = dni;
    }
    public Persona(Long perId, String dni, String nom, String apeMat, String apePat, Date fecNac,String email, String num1, String num2) {
        this.perId = perId;
        this.dni = dni;
        this.nom = nom;
        this.apeMat = apeMat;
        this.apePat = apePat;
        this.fecNac = fecNac;
        this.email = email;
        this.num1 = num1;
        this.num2 = num2;
    }

    public Persona(String dni, String nom, String apeMat, String apePat, Date fecNac, String email, String num1, String num2, String fijo, String direccion) {
        this.dni = dni;
        this.nom = nom;
        this.apeMat = apeMat;
        this.apePat = apePat;
        this.fecNac = fecNac;
        this.email = email;
        this.num1 = num1;
        this.num2 = num2;
        this.fijo = fijo;
        this.direccion = direccion;
        
    }
    
    

    public Long getPerId() {
        return this.perId;
    }
    public void setPerId(Long perId) {
        this.perId = perId;
    }
    public String getDni() {
        return this.dni;
    }
    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getApeMat() {
        return this.apeMat;
    }
    public void setApeMat(String apeMat) {
        this.apeMat = apeMat;
    }

    public String getApePat() {
        return this.apePat;
    }
    public void setApePat(String apePat) {
        this.apePat = apePat;
    }

    public Date getFecNac() {
        return this.fecNac;
    }
    public void setFecNac(Date fecNac) {
        this.fecNac = fecNac;
    }

    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getNum1() {
        return this.num1;
    }
    public void setNum1(String num1) {
        this.num1 = num1;
    }

    public String getNum2() {
        return this.num2;
    }
    public void setNum2(String num2) {
        this.num2 = num2;
    }
    
    public String getFijo() {
        return fijo;
    }

    public void setFijo(String fijo) {
        this.fijo = fijo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Trabajador> getTrabajadores() {
        return this.trabajadores;
    }

    public void setTrabajadores(List<Trabajador> trabajadors) {
        this.trabajadores = trabajadors;
    }
    @Override
    public String toString(){
        return EntityUtil.objectToJSONString(new String[]{"perId", "perCod", "nom", "apePat", "apeMat", "fecNac", "dni", "email"}, null, this);
    }
    
    @Transient
    public String getNombrePersona() {
        return this.getNom() +" "+ this.getApePat() +" "+ this.getApeMat();

    }
    @Transient
    public String getNombrePersonaAP() {
        return this.getApePat() +" "+ this.getApeMat() +" "+ this.getNom()  ;

    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public List<Parientes> getParientes() {
        return parientes;
    }

    public void setParientes(List<Parientes> parientes) {
        this.parientes = parientes;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }
    
}
