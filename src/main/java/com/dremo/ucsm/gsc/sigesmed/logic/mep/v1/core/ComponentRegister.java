/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx.*;

/**
 *
 * @author geank
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent mepComponent = new WebComponent(Sigesmed.MODULO_EVALUACION_PERSONAL);
        mepComponent.setName("mep");
        mepComponent.setVersion(1);
        mepComponent.addTransactionPOST("registrarFicha", RegistrarFichaEvaluacionTx.class);
        mepComponent.addTransactionGET("listarFichas", ListarFichasEvaluacionTx.class);
        mepComponent.addTransactionPOST("addElement", RegistrarElementoFichaEvaluacionTx.class);
        mepComponent.addTransactionGET("getDetalles", ListarElementosFichaTx.class);
        mepComponent.addTransactionGET("getEvaluacionesCompartidas", ListarEvaluacionesCompartidasTx.class);
        mepComponent.addTransactionGET("listarTrabajadores", ListarTrabajadoresInstitucionTx.class);
        mepComponent.addTransactionGET("listarResumenEvaluacion", ListarResumenEvaluacionTx.class);
        mepComponent.addTransactionGET("listarDetallesEvaluacion", ListarDetallesEvaluacionTx.class);
        mepComponent.addTransactionGET("listarCargosTrabajador", ListarCargosTrabajadorTx.class);
        mepComponent.addTransactionGET("ObtenerFichaEvaluacionByRol", ObtenerFichaEvaluacionTx.class);
        mepComponent.addTransactionGET("getDataEstadistica",ListarDataEstadisticaTx.class);
        mepComponent.addTransactionPOST("registrarEvaluacion", RegistrarEvaluacionIndicadoresTx.class);
        mepComponent.addTransactionDELETE("deleteFichaEvaluacion",DeleteFichaEvaluacionTx.class);
        mepComponent.addTransactionDELETE("deleteElementoFicha",DeleteElementoFichaTx.class);
        mepComponent.addTransactionPUT("updateElementoFicha",UpdateElementoFichaEvaluacionTx.class);
        mepComponent.addTransactionGET("getReportEvaluacion",ReportesMepTx.class);
        mepComponent.addTransactionPUT("compartirEvaluacion",CompartirEvaluacionPersonalTx.class);
        return mepComponent;
    }
    
}
