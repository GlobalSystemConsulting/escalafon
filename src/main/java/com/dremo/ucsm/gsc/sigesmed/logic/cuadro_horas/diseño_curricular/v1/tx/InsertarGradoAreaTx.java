/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseño_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DiseñoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
/**
 *
 * @author abel
 */
public class InsertarGradoAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int gradoID = 0;
        int areaID = 0;
        int gradoPos = 0;
        int areaPos = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            gradoID = requestData.getInt("gradoID");
            areaID = requestData.getInt("areaID");
            gradoPos = requestData.getInt("gradoPos");
            areaPos = requestData.getInt("areaPos");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo asignar el area al grado, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        DiseñoCurricularDao diseñoDao = (DiseñoCurricularDao)FactoryDao.buildDao("mech.DiseñoCurricularDao");
        try{
            diseñoDao.insertarGradoArea(gradoID,areaID,gradoPos,areaPos);
        }catch(Exception e){
            System.out.println("No se pudo asignar el area al grado\n"+e);
            return WebResponse.crearWebResponseError("No se pudo asignar el area al grado", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        return WebResponse.crearWebResponseExito("Se asigno el area al grado correctamente", oResponse);
        //Fin
    }    
    
    
}

