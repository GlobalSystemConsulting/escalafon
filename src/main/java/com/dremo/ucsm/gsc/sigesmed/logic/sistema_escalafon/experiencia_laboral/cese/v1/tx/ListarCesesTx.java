/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.cese.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CeseDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cese;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarCesesTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarCesesTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<Cese> ceses = null;
        CeseDao ceseDao = (CeseDao)FactoryDao.buildDao("se.CeseDao");
        
        CategoriaDao categoriaDao =(CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
        Categoria categoria=null;
        CargoDao cargoDao =(CargoDao)FactoryDao.buildDao("se.CargoDao");
        Cargo cargo=null;
        
        OrganigramaDao organigramaDao =(OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
        Organigrama organigrama=null;
        
        try{
            ceses = ceseDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar ceses",e);
            System.out.println("No se pudo listar los ceses\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los ceses", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correct
        */
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
        JSONArray miArray = new JSONArray();
        for(Cese c:ceses ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("cesId", c.getCesId());
            oResponse.put("entEmiRes", null != c.getEntEmiRes()?c.getEntEmiRes():"");
            oResponse.put("numDoc", null != c.getNumDoc() ? c.getNumDoc():"");
            oResponse.put("fecDoc", null != c.getFecDoc() ? sdo.format(c.getFecDoc()) : "");
            oResponse.put("jorLabId", c.getJorLabId());
            oResponse.put("motCes", null != c.getMotCes()?c.getMotCes():"");
            oResponse.put("fecIni", null != c.getFecIni() ? sdo.format(c.getFecIni()) : "");
            oResponse.put("fecTer", null != c.getFecFin() ? sdo.format(c.getFecFin()) : "");
            oResponse.put("tipDocId", c.getTipDocId());
            oResponse.put("motCes", null != c.getMotCes()?c.getMotCes():"");
            oResponse.put("tipCes", null != c.getTipIntId()?c.getTipIntId():"");
            oResponse.put("aniosPro", null != c.getAnioPro()?c.getAnioPro():"");
            oResponse.put("mesesPro", null != c.getMesPro()?c.getMesPro():"");
            oResponse.put("diasPro", null != c.getDiaPro()?c.getDiaPro():"");
            
            //Historico
            if(c.getOrgiActId()!=null && c.getOrgiActId()>0){
                organigrama=organigramaDao.obtenerDetalle(c.getOrgiActId());
                oResponse.put("orgiActId", c.getOrgiActId());
                oResponse.put("datOrgiId", c.getDatOrgiId());
                oResponse.put("nomDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getNomDatOrgi());
                oResponse.put("anioDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getAnioDatOrgi());
                oResponse.put("nomUbi", organigrama.getUbiId());
                
                oResponse.put("nomOrgiN3", organigrama.getNomOrgi());
                oResponse.put("nomOrgiN2", organigrama.getOrganigramaPadre().getNomOrgi());
                oResponse.put("nomOrgiN1", organigrama.getOrganigramaPadre().getOrganigramaPadre().getNomOrgi());
            }
            else{
                oResponse.put("orgiActId", 0);
                oResponse.put("datOrgiId", 0);
                oResponse.put("nomOrgi", "");
                oResponse.put("nomDatOrgi", "");
                oResponse.put("anioDatOrgi", 0);
                oResponse.put("nomUbi", 0);
                oResponse.put("nomOrgiN3", "");
                oResponse.put("nomOrgiN2", "");
                oResponse.put("nomOrgiN1", "");
            }

            if(c.getCatActId()!=null && c.getCatActId()>0){
                categoria=categoriaDao.obtenerDetalle(c.getCatActId());
                oResponse.put("catActId", c.getCatActId());
                oResponse.put("nomCat", categoria.getNomCat());
                oResponse.put("nomPla", categoria.getPlanilla().getNomPla());
            }
            else{
                oResponse.put("catActId", 0);
                oResponse.put("nomCat", "");
                oResponse.put("nomPla", "");
            }

            if(c.getCarActId()!=null && c.getCarActId()>0){
                cargo=cargoDao.buscarPorId(c.getCarActId());
                oResponse.put("carActId", c.getCarActId());
                oResponse.put("nomCar", cargo.getNomCar());            
            }
            else{
                oResponse.put("carActId", 0);
                oResponse.put("nomCar", "");
            }
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los ceses fueron listados exitosamente", miArray);
    }
    
}
