package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 10/11/2016.
 */
@Entity
@Table(name = "sesion_aprendizaje", schema = "pedagogico")
public class SesionAprendizaje  implements java.io.Serializable{
    @SequenceGenerator(name = "sesion_aprendizaje_ses_apr_id_seq", sequenceName = "pedagogico.sesion_aprendizaje_ses_apr_id_seq")
    @GeneratedValue(generator = "sesion_aprendizaje_ses_apr_id_seq")
    @Id
    @Column(name = "ses_apr_id", nullable = false)
    private int sesAprId;
    @Column(name = "num_ses")
    private int numSes;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uni_did_id",nullable = false)
    private UnidadDidactica unidadDidactica;
    @Column(name = "tit")
    private String tit;
    @Column(name = "sec_did")
    private String secDid;
    @Column(name = "niv_ava")
    private String nivAva;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ini")
    private Date fecIni;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_fin")
    private Date fecFin;
    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "sesion", cascade = CascadeType.REMOVE)
    private List<SesionSequenciaDidactica> sequenciasDidacticas = new ArrayList<>();
    @OneToMany(mappedBy = "sesion",fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<IndicadoresSesionAprendizaje> indicadoresSesion = new ArrayList<>();
    @OneToMany(mappedBy = "sesion", fetch = FetchType.LAZY)
    private List<TareaSesionAprendizaje> tareas = new ArrayList<>();
    public SesionAprendizaje() {
    }

    public SesionAprendizaje(String tit, Date fecIni, Date fecFin, int numSes) {
        this.tit = tit;
        this.fecIni = fecIni;
        this.fecFin = fecFin;
        this.numSes = numSes;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public SesionAprendizaje(String tit, String secDid, String nivAva, Date fecIni, Date fecFin) {
        this.tit = tit;
        this.secDid = secDid;
        this.nivAva = nivAva;
        this.fecIni = fecIni;
        this.fecFin = fecFin;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getSesAprId() {
        return sesAprId;
    }

    public void setSesAprId(int sesAprId) {
        this.sesAprId = sesAprId;
    }

    public int getNumSes() {
        return numSes;
    }

    public void setNumSes(int numSes) {
        this.numSes = numSes;
    }

    public UnidadDidactica getUnidadDidactica() {
        return unidadDidactica;
    }

    public List<TareaSesionAprendizaje> getTareas() {
        return tareas;
    }

    public void setTareas(List<TareaSesionAprendizaje> tareas) {
        this.tareas = tareas;
    }

    public void setUnidadDidactica(UnidadDidactica unidadDidactica) {
        this.unidadDidactica = unidadDidactica;
    }

    public String getTit() {
        return tit;
    }

    public void setTit(String tit) {
        this.tit = tit;
    }

    public String getSecDid() {
        return secDid;
    }

    public void setSecDid(String secDid) {
        this.secDid = secDid;
    }

    public String getNivAva() {
        return nivAva;
    }

    public void setNivAva(String nivAva) {
        this.nivAva = nivAva;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<SesionSequenciaDidactica> getSequenciasDidacticas() {
        return sequenciasDidacticas;
    }

    public List<IndicadoresSesionAprendizaje> getIndicadoresSesion() {
        return indicadoresSesion;
    }

    @Override
    public String toString() {
        return "SesionAprendizaje{" + "sesAprId=" + sesAprId + ", estReg=" + estReg + '}';
    }
    
    
}
