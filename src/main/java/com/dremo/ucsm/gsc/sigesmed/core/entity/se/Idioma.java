/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author felipe
 */
@Entity
@Table(name = "idioma", schema="administrativo")

public class Idioma implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idi_id")
    private int idiId;
    
    @Size(max = 2147483647)
    @Column(name = "idi_nom")
    private String idiNom;
    
    @Column(name = "est_reg")
    private char estReg;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "des")
    private String des;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "idiomas")
    List<Persona> persona;
    
    public Idioma() {
    }

    public Idioma(int idiId) {
        this.idiId = idiId;
    }

    public int getIdiId() {
        return idiId;
    }

    public void setIdiId(int idiId) {
        this.idiId = idiId;
    }

    public String getIdiNom() {
        return idiNom;
    }

    public void setIdiNom(String idiNom) {
        this.idiNom = idiNom;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public List<Persona> getPersona() {
        return persona;
    }

    public void setPersona(List<Persona> persona) {
        this.persona = persona;
    }
    
    //INICIO -usado para el catalogo
    public int getId(){
        return this.idiId;
    }
    public void setId(int Id){
        this.idiId=Id;
    }
    
    public String getNom() {
        return this.idiNom;
    }
    public void setNom(String nom) {
        this.idiNom = nom;
    }

    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    //FIN -usado para el catalogo

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.se.Idioma[ idiId=" + idiId + " ]";
    }
    
}
