package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mech.PlanEstudiosDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GradoSeccionPlanNivelDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaInsDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.GradoIeEstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.MatriculaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.OrganizacionMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.VacanteAutomatica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import com.dremo.ucsm.gsc.sigesmed.util.MatriculaReporter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class GenerarMatriculaAutomaticaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        GenericMMIDaoHibernate<MatriculaMMI> hb = new GenericMMIDaoHibernate<>();
        GenericMMIDaoHibernate<GradoIeEstudianteMMI> hbie = new GenericMMIDaoHibernate<>();
        GradoSeccionPlanNivelDaoHibernate gradoSeccion = new GradoSeccionPlanNivelDaoHibernate();
        PlanEstudiosDaoHibernate pev = new PlanEstudiosDaoHibernate();
        MatriculaReporter reporteador = new MatriculaReporter();
        MatriculaInsDaoHibernate matDao = new MatriculaInsDaoHibernate();

        PlanEstudios pe;
        List<Object[]> gradosySecciones;
        ArrayList<VacanteAutomatica> myVacante = new ArrayList<>();
        VacanteAutomatica vacanteSelected;
        int vacanteSelect = -1;

        long estId, regId, parId;
        int orgOriId, orgDesId, nivId, jorEscId, graId,usuMod;
        char turId;
        Integer matSit, conMat;
        Date matFec;
        String obs;
        Calendar cal = Calendar.getInstance();
        Date myDate;
        int year;
        
        MatriculaMMI reload;

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            estId = requestData.getLong("matEstId");
            regId = requestData.getLong("matRegId");
            parId = requestData.getLong("matParId");

            orgOriId = requestData.getInt("orgOriId");
            orgDesId = requestData.getInt("orgDesId");

            matFec = DateUtil.getDate2String(requestData.getString("matFec"));

            matSit = requestData.getInt("matSit");
            conMat = requestData.getInt("conMat");
            obs = requestData.getString("matObs");

            nivId = requestData.getInt("nivId");
            jorEscId = requestData.getInt("jorEscId");
            turId = requestData.getString("turId").charAt(0);
            graId = requestData.getInt("graId");
            usuMod = requestData.getInt("usuMod");

            //calcular Secciones Disponibles
            pe = pev.buscarVigentePorOrganizacion(orgDesId);
            myDate = pe.getAñoEsc();
            cal.setTime(myDate);
            year = cal.get(Calendar.YEAR);
            gradosySecciones = gradoSeccion.getGradosySeccionesDet(orgDesId, year, nivId, jorEscId, turId, graId);

            for (Object[] itm : gradosySecciones) {
                VacanteAutomatica temp = new VacanteAutomatica();
                temp.setGraSecId(Integer.parseInt(itm[0].toString()));
                temp.setNumMaxEstMat(Integer.parseInt(itm[1].toString()));
                temp.setNumTotEstMat(Integer.parseInt(itm[2].toString()));
                temp.setPlaNivId(Integer.parseInt(itm[3].toString()));
                temp.setNivId(Integer.parseInt(itm[4].toString()));
                temp.setNivNom(itm[5].toString());
                temp.setNivAbr(itm[6].toString());
                temp.setJorEscId(Integer.parseInt(itm[7].toString()));
                temp.setJorEscNom(itm[8].toString());
                temp.setJorEscAbr(itm[9].toString());
                temp.setTurId(itm[10].toString().charAt(0));
                temp.setTurNom(itm[11].toString());
                temp.setGraId(Integer.parseInt(itm[12].toString()));
                temp.setGraNom(itm[13].toString());
                temp.setSecId(itm[15].toString().charAt(0));
                myVacante.add(temp);
            }

            for (int j = 0; j < myVacante.size(); j++) {
                if (myVacante.get(j).getNumMaxEstMat() - myVacante.get(j).getNumTotEstMat() > 0) {
                    vacanteSelect = j;
                    break;
                }
            }

            //si no hay vacantes disponibles se seleccionada la ultima seccion
            if (vacanteSelect == -1) {
                vacanteSelect = myVacante.size() - 1;
            }
            //seleccion de vacante
            vacanteSelected = myVacante.get(vacanteSelect);

            MatriculaMMI myMatricula = new MatriculaMMI();
            Object id = hb.llave(MatriculaMMI.class, "matId");
            if (id != null) {
                myMatricula.setMatId((long) id + 1);
            } else {
                myMatricula.setMatId(1);
            }

            PersonaMMI regPer = new PersonaMMI();
            regPer.setPerId(regId);

            EstudianteMMI estPer = new EstudianteMMI();
            estPer.setPerId(estId);

            PersonaMMI parPer = new PersonaMMI();
            parPer.setPerId(parId);

            OrganizacionMMI orgOri = new OrganizacionMMI();
            orgOri.setOrgId(orgOriId);

            OrganizacionMMI orgDes = new OrganizacionMMI();
            orgDes.setOrgId(orgDesId);

            if (orgOriId != -1) {
                myMatricula.setOrganizacionByOrgOriId(orgOri);
            }
            myMatricula.setOrganizacionByOrgDesId(orgDes);

            myMatricula.setPersonaByApoId(parPer);
            myMatricula.setPersonaByRegId(regPer);

            myMatricula.setEstudiante(estPer);

            myMatricula.setPlaNivId(vacanteSelected.getPlaNivId());

            myMatricula.setGraId(graId);
            myMatricula.setSecId(vacanteSelected.getSecId());

            myMatricula.setConMat(conMat);
            myMatricula.setMatEst(2); //ASEPTADA
            myMatricula.setMatSit(matSit);
            myMatricula.setObs(obs);

            myMatricula.setMatFec(matFec);

            myMatricula.setEstReg('A');
            myMatricula.setAct(true);
            
            myMatricula.setUsuMod(usuMod);
            myMatricula.setFecMod(new Date());

            hb.saveOrUpdate(myMatricula);

            reload = matDao.load(myMatricula.getMatId());
            
            //generando constancia de matricula
            reporteador.makeConstanciaMatricula(reload,
                    vacanteSelected.getNivNom(), vacanteSelected.getJorEscAbr(),
                    vacanteSelected.getJorEscNom(), vacanteSelected.getTurNom(),
                    vacanteSelected.getGraNom());

            gradoSeccion.addNumMatriculasXGradoySeccion(vacanteSelected.getGraSecId(), 1);

            gradoSeccion.ActivateEstudianteMatriculado(estId, orgDesId);

            GradoIeEstudianteMMI gradoIe = new GradoIeEstudianteMMI();
            Object idie = hbie.llave(GradoIeEstudianteMMI.class, "graIeEstId");
            if (idie != null) {
                gradoIe.setGraIeEstId((long) idie + 1);
            } else {
                gradoIe.setGraIeEstId((long) 1);
            }
            gradoIe.setGraId(graId);
            gradoIe.setSecId(vacanteSelected.getSecId());
            gradoIe.setMatricula(myMatricula);
            gradoIe.setAct(true);
            gradoIe.setEstReg('A');
            gradoIe.setEstGra('P');
            gradoIe.setUsuMod(usuMod);
            gradoIe.setFecMod(new Date());

            hbie.saveOrUpdate(gradoIe);

            return WebResponse.crearWebResponseExito("Se Generaron la matricula", "");
        } catch (JSONException | NumberFormatException e) {
            return WebResponse.crearWebResponseError("Error al generar matricula! ", e.getMessage());

        }

    }
}
