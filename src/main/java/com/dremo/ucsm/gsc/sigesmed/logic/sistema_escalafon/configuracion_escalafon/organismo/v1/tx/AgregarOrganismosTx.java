/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.organismo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganismoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organismo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class AgregarOrganismosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarOrganismosTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        Organismo organismo = null;
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            //Integer orgaId = requestData.getInt("orgaId");
            String codOrga = requestData.optString("codOrga");
            String nomOrga = requestData.optString("nomOrga");
            
            organismo = new Organismo();
            organismo.setCodOrga(codOrga);
            organismo.setNomOrga(nomOrga);
            organismo.setEstReg('A');
            organismo.setFecMod(new Date());
            organismo.setUsuMod(wr.getIdUsuario());
            
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo Organismo",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        
        OrganismoDao organismoDao = (OrganismoDao) FactoryDao.buildDao("se.OrganismoDao");
        try {
            organismoDao.insert(organismo);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nuevo organismo",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("orgaId", organismo.getOrgaId());
        oResponse.put("codOrga", organismo.getCodOrga());
        oResponse.put("nomOrga", organismo.getNomOrga());
                
        return WebResponse.crearWebResponseExito("El registro del organismo se realizo correctamente", oResponse);

    }
    
}
