/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.zona.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarZonasTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarZonasTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer zonId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            zonId = requestData.getInt("zonaId");        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminar Zona",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        UbicacionDao ubicacionDao = (UbicacionDao) FactoryDao.buildDao("se.UbicacionDao"); 
        try{
            ubicacionDao.delete(new Ubicacion(zonId));
        }catch(Exception e){
            System.out.println("No se pudo eliminar la Zona\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la Zona", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La Zona se elimino correctamente");
    }
    
}