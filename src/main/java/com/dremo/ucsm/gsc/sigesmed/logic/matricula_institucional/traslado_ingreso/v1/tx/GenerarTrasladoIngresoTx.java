package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_ingreso.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mech.PlanEstudiosDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GradoSeccionPlanNivelDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.TrasladoIngresoDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.TrasladoIngreso;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.VacanteAutomatica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.MatriculaReporter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class GenerarTrasladoIngresoTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        GenericMMIDaoHibernate<TrasladoIngreso> hb = new GenericMMIDaoHibernate<>();
        GradoSeccionPlanNivelDaoHibernate gradoSeccion = new GradoSeccionPlanNivelDaoHibernate();
        PlanEstudiosDaoHibernate pev = new PlanEstudiosDaoHibernate();
        TrasladoIngresoDaoHibernate traIngDao = new TrasladoIngresoDaoHibernate();
        MatriculaReporter reporteador = new MatriculaReporter();

        TrasladoIngreso myTraslado;
        TrasladoIngreso reload;

        long traIngEstId;
        Integer traIngAnyoOri, traIngAnyoDes, traIgnOrgOri,
                traIgnOrgDes, traIngTipTra, usuMod;

        int traIngNivId, traIngJorEscId, traIngGraId;
        Character traIngTurId, traIngEst;
        String traIgnObs, traIgnResTra;

        Object[] checkTraslado;
        JSONObject msjError = new JSONObject();

        PlanEstudios pe;
        List<Object[]> gradosySecciones;
        ArrayList<VacanteAutomatica> myVacante = new ArrayList<>();
        int indexVacSelected = -1;
        VacanteAutomatica vacanteSelected;
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        Date myDate;
        int year;

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            traIngEst = requestData.getString("traIngEst").charAt(0);
            traIngAnyoOri = requestData.getInt("traIngAnyoOri");
            traIngAnyoDes = requestData.getInt("traIngAnyoDes");
            traIgnOrgOri = requestData.getInt("traIgnOrgOri");
            traIgnOrgDes = requestData.getInt("traIgnOrgDes");
            traIngEstId = requestData.getLong("traIngEstId");

            traIngNivId = requestData.getInt("traIngNivId");
            traIngJorEscId = requestData.getInt("traIngJorEscId");
            traIngGraId = requestData.getInt("traIngGraId");
            traIngTurId = requestData.getString("traIngTurId").charAt(0);
            traIngTipTra = requestData.getInt("traIngTipTra");
            traIgnResTra = requestData.getString("traIgnResTra");
            traIgnObs = requestData.getString("traIgnObs");
            usuMod = requestData.getInt("usuMod");

            //verificar si existen traslados vigentes
            checkTraslado = gradoSeccion.getTrasladoInternoByEstudianteId(traIngEstId);

            if ((Boolean) checkTraslado[0]) {
                msjError.put("tipError", 1);
                msjError.put("traIngId", checkTraslado[1].toString());
                msjError.put("traIngTipId", checkTraslado[2].toString());
                msjError.put("traIngTipNom", getTipoTrasladoNom(Integer.parseInt(checkTraslado[2].toString())));
                msjError.put("traIngEst", checkTraslado[3].toString());
                msjError.put("traIngFec", checkTraslado[6].toString());
                msjError.put("traIngEstNom", checkTraslado[16].toString() + " " + checkTraslado[17].toString() + " " + checkTraslado[18].toString());
                msjError.put("traIngOrgOri", checkTraslado[8].toString());
                msjError.put("traIngOrgDes", checkTraslado[10].toString());

                return WebResponse.crearWebResponseError("Error, Traslado en Progreso", msjError);
            }

            //calcular Secciones Disponibles
            pe = pev.buscarVigentePorOrganizacion(traIgnOrgDes);
            myDate = pe.getAñoEsc();
            cal.setTime(myDate);
            year = cal.get(Calendar.YEAR);
            gradosySecciones = gradoSeccion.getGradosySeccionesDet(traIgnOrgDes, year, traIngNivId, traIngJorEscId, traIngTurId, traIngGraId);

            for (Object[] itm : gradosySecciones) {
                VacanteAutomatica temp = new VacanteAutomatica();
                temp.setGraSecId(Integer.parseInt(itm[0].toString()));
                temp.setNumMaxEstMat(Integer.parseInt(itm[1].toString()));
                temp.setNumTotEstMat(Integer.parseInt(itm[2].toString()));
                temp.setPlaNivId(Integer.parseInt(itm[3].toString()));
                temp.setNivId(Integer.parseInt(itm[4].toString()));
                temp.setNivNom(itm[5].toString());
                temp.setNivAbr(itm[6].toString());
                temp.setJorEscId(Integer.parseInt(itm[7].toString()));
                temp.setJorEscNom(itm[8].toString());
                temp.setJorEscAbr(itm[9].toString());
                temp.setTurId(itm[10].toString().charAt(0));
                temp.setTurNom(itm[11].toString());
                temp.setGraId(Integer.parseInt(itm[12].toString()));
                temp.setGraNom(itm[13].toString());
                temp.setSecId(itm[15].toString().charAt(0));
                myVacante.add(temp);
            }

            for (int j = 0; j < myVacante.size(); j++) {
                if (myVacante.get(j).getNumMaxEstMat() - myVacante.get(j).getNumTotEstMat() > 0) {
                    indexVacSelected = j;
                    break;
                }
            }

            //si no hay vacantes disponibles se seleccionada la ultima seccion
            if (indexVacSelected == -1) {
                indexVacSelected = myVacante.size() - 1;
            }
            vacanteSelected = myVacante.get(indexVacSelected);

            myTraslado = new TrasladoIngreso();
            Object id = hb.llave(TrasladoIngreso.class, "traIngId");
            if (id != null) {
                myTraslado.setTraIngId((int) id + 1);
            } else {
                myTraslado.setTraIngId(1);
            }

            EstudianteMMI estPer = new EstudianteMMI();
            estPer.setPerId(traIngEstId);

            myTraslado.setTraIngTip(traIngTipTra);
            myTraslado.setTraIngAnoOri(traIngAnyoOri);
            myTraslado.setTraIngAnoDes(traIngAnyoDes);
            myTraslado.setObs(traIgnObs);
            myTraslado.setTraIngEst(traIngEst);
            myTraslado.setTraIngRes(traIgnResTra);
            myTraslado.setTraIngFec(today);

            myTraslado.setUsuMod(usuMod);
            myTraslado.setFecMod(today);
            myTraslado.setEstReg('A');
            myTraslado.setOrgOriId(traIgnOrgOri);
            myTraslado.setOrgDesId(traIgnOrgDes);
            myTraslado.setGraId(vacanteSelected.getGraId());
            myTraslado.setSecId(vacanteSelected.getSecId());
            myTraslado.setPlaNivId(vacanteSelected.getPlaNivId());

            myTraslado.setEstudiante(estPer);

            hb.saveOrUpdate(myTraslado);

            reload = traIngDao.getTrasladoById(myTraslado.getTraIngId());

            String orgDesCod = gradoSeccion.getOrganizacionCod(myTraslado.getOrgDesId());
            String orgDesNom = gradoSeccion.getOrganizacionNombre(myTraslado.getOrgDesId());

            //generar constancia de vacante
            reporteador.makeConstanciaVacante(reload, orgDesCod, orgDesNom,
                    vacanteSelected.getNivNom(), vacanteSelected.getJorEscAbr(),
                    vacanteSelected.getJorEscNom(), vacanteSelected.getTurNom(),
                    vacanteSelected.getGraNom());

            gradoSeccion.addNumMatriculasXGradoySeccion(vacanteSelected.getGraSecId(), 1);

            return WebResponse.crearWebResponseExito("Traslado Exitoso! ");

        } catch (JSONException | NumberFormatException e) {
            msjError.put("tipError", 2);
            return WebResponse.crearWebResponseError("Error al generar traslado Ingreso! ", msjError);
        }
    }

    private String getTipoTrasladoNom(int tipo) {
        switch (tipo) {
            case 1:
                return "Por Cambio de Nivel";
            case 2:
                return "Por Cambio de Año";
            case 3:
                return "En el Mismo año";
            default:
                return "No espesifica";
        }
    }

}
