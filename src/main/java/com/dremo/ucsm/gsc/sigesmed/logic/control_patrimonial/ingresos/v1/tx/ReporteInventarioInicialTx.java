/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import java.text.SimpleDateFormat;
/**
 *
 * @author Administrador
 */
public class ReporteInventarioInicialTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
          SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
          
          try{
                
                
                /*Cabecera del Reporte*/
                String cab1 [] = {"Nro.Correlativo","Codigo Patrimonial","Descripcion del Bien","Estado","Ubicacion","Anexo"};
                    
                float[] columnWidths={2,2,2,2,2,2};
            
                /*Construimos la Tabla*/
            
                Mitext m = null;
                /*LLENAMOS LA CABECERA*/
                m = new Mitext(true,"REPORTE INVENTARIO INICIAL"); 
                GTabla t_general = new GTabla(columnWidths);
                t_general.build(cab1);
                GCell[] cell ={t_general.createCellCenter(1,1),t_general.createCellCenter(1,1),t_general.createCellCenter(1,1),t_general.createCellCenter(1,1),t_general.createCellCenter(1,1),t_general.createCellCenter(1,1)};
            
                
                /*Datos del Reporte*/
                JSONArray requestData = (JSONArray)wr.getData();
                
                int size = requestData.length();
                for(int i=0; i<size ; i++){
                    JSONObject data = requestData.getJSONObject(i);
                    String[] archivos_data = new String[6];
                    archivos_data[0]= Integer.toString(i+1);
                    archivos_data[1]= Integer.toString(data.getInt("cat_bie_id"));
                    archivos_data[2]= data.getString("des_bie");
                    archivos_data[3]= data.getString("est_bien");
                    archivos_data[4]= data.getString("ubi_bien");
                    archivos_data[5]= data.getString("an_bien");
                    t_general.processLineCell(archivos_data,cell);
                }

                /*Armamos y Llenamos el Reporte*/
                m.agregarTabla(t_general);
            
                  /*Mostramos el reporte*/
                JSONArray miArray = new JSONArray();
        
                m.cerrarDocumento();
                JSONObject oResponse = new JSONObject();        
                oResponse.put("datareporte",m.encodeToBase64());
                miArray.put(oResponse); 
                         
                return WebResponse.crearWebResponseExito("Se genero el Reporte con exito ...",miArray); 
                 
                
          }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("ERROR AL GENERAR REPORTE ", e.getMessage() );
          }
        
        
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
