/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.comunication.pojo;

/**
 *
 * @author GSC05
 */
public class Agrupamientos {
    
    private String Agrupamiento;
    private int nroPagAgrp;
    private int idAgrupamiento;
    private int pagInicio;

    public Agrupamientos(int idAgrupam,String Nombre, int nroPagina) {
        this.idAgrupamiento=idAgrupam;
        this.Agrupamiento = Nombre;
        this.nroPagAgrp = nroPagina;
    }

    public int getPagInicio() {
        return pagInicio;
    }

    public void setPagInicio(int pagInicio) {
        this.pagInicio = pagInicio;
    }

    
    
    public String getNombre() {
        return Agrupamiento;
    }

    public void setNombre(String Nombre) {
        this.Agrupamiento = Nombre;
    }

    public int getNroPagina() {
        return nroPagAgrp;
    }

    public void setNroPagina(int nroPagina) {
        this.nroPagAgrp = nroPagina;
    }
    
    public String toString(){
        return this.Agrupamiento;
    }
    
    
}

