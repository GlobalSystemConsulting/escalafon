/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.CatalogoTabla;
import java.util.List;

/**
 *
 * @author angel
 */
public interface CatalogoTablaDao extends GenericDao<CatalogoTabla>{
    
     public  List<Object>  buscarRegistrosTabla(String nombreTabla);
     public List<Object> buscarRegistrosTabla2(String nombreClase);
      public List<Object> buscarRegistrosTabla3(String nombreClase);

}
