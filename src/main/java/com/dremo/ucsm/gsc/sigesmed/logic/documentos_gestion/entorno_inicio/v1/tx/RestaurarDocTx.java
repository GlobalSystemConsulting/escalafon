/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class RestaurarDocTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        /*Recibimos el objeto que deseamos actualizar su estado de E eliminado a A activo*/
        JSONObject requestData = (JSONObject)wr.getData();
        Integer codDoc = requestData.getInt("idDoc");
        
        /* Interactuamos con la base de datos*/
        ItemFileDao iteFilDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
        ItemFile iteFilActual = iteFilDao.buscarPorID(codDoc);
        
        //Actualizamos
        iteFilActual.setEstReg("A");
        iteFilDao.update(iteFilActual);
        
        //En caso de tener hijos estos tambien serán restaurados
        List<ItemFile> listHijos = iteFilDao.buscarAllHijosPorIdPadre(iteFilActual);
        
        for(ItemFile doc : listHijos){
            //Actualizamos el documentos
            doc.setEstReg("A");
            iteFilDao.update(doc);
        }
        
        return WebResponse.crearWebResponseExito("Se Restauro el documento con exito");
    }
    
}
