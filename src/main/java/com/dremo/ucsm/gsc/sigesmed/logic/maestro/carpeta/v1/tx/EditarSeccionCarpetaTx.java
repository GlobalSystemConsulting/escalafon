package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.SeccionCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 29/12/2016.
 */
public class EditarSeccionCarpetaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EditarSeccionCarpetaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idSeccion = data.getInt("id");
        return editarSeccion(idSeccion, data);
    }

    private WebResponse editarSeccion(int idSeccion, JSONObject data) {
        try{
            CarpetaPedagogicaDao carpetaDao = (CarpetaPedagogicaDao) FactoryDao.buildDao("maestro.carpeta.CarpetaPedagogicaDao");
            SeccionCarpetaPedagogica seccion = carpetaDao.buscarSeccionPorId(idSeccion);
            seccion.setNom(data.optString("nom",seccion.getNom()).toUpperCase());
            seccion.setFecMod(new Date());
            carpetaDao.editarSeccionCarpeta(seccion);
            return WebResponse.crearWebResponseExito("Se registro correctamente");
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarSeccion",e);
            return WebResponse.crearWebResponseError("No se puede editar la seccion");
        }
    }
}
