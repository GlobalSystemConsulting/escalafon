/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sci;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ControlLibro;
import java.util.List;


/**
 *
 * @author Administrador
 */
public interface ControlLibroDao extends GenericDao<ControlLibro>{
    public List<ControlLibro> listarControles(int organizacionID,int year);
}
