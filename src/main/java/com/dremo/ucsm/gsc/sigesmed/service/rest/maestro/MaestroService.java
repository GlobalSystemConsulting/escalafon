package com.dremo.ucsm.gsc.sigesmed.service.rest.maestro;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.TypeTransaction;
import org.json.JSONObject;
import org.json.JSONTokener;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 10/10/2016.
 */
@Path("/maestro")
public class MaestroService {
    private static Logger logger = Logger.getLogger(MaestroService.class.getName());

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String recursoPost(String content){
        logger.log(Level.INFO,"POST {0}",content);
        JSONObject jsonObject = (JSONObject) new JSONTokener(content).nextValue();
        WebRequest request = WebRequest.createFromJSON(jsonObject);
        WebResponse response = request.invoke(Sigesmed.SUBMODULO_MAESTRO, TypeTransaction.type_transaction_POST);
        return response.toJSON().toString();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String recursoGet(@QueryParam("content") String content){
        logger.log(Level.INFO,"GET {0}",content);
        JSONObject jsonObject = (JSONObject)new JSONTokener(content).nextValue();
        WebRequest request = WebRequest.createFromJSON(jsonObject);
        WebResponse response = request.invoke(Sigesmed.SUBMODULO_MAESTRO, TypeTransaction.type_transaction_GET);
        return response.toJSON().toString();
    }

    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String recursoPut(String content){
        logger.log(Level.INFO,"PUT: {0}",content);
        JSONObject jsonObject = (JSONObject)new JSONTokener(content).nextValue();
        WebRequest request = WebRequest.createFromJSON(jsonObject);
        WebResponse response = request.invoke(Sigesmed.SUBMODULO_MAESTRO, TypeTransaction.type_transaction_PUT);
        return response.toJSON().toString();
    }
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public String recursoDelete(@QueryParam("content") String content){
        logger.log(Level.INFO,"DELETE: {0}",content);
        JSONObject jsonObject = (JSONObject)new JSONTokener(content).nextValue();
        WebRequest request = WebRequest.createFromJSON(jsonObject);
        WebResponse response = request.invoke(Sigesmed.SUBMODULO_MAESTRO, TypeTransaction.type_transaction_DELETE);
        return response.toJSON().toString();
    }

}
