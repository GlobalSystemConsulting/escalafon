package com.dremo.ucsm.gsc.sigesmed.logic.maestro.acomp.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.acomp.v1.tx.*;

/**
 * Created by Administrador on 10/10/2016.
 */
public class ComponentRegister implements IComponentRegister {
    @Override
    public WebComponent createComponent() {
        WebComponent component = new WebComponent(Sigesmed.SUBMODULO_MAESTRO);
        component.setName("acomp");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarOrganizaciones", ListarOrganizacionesHijoTx.class);
        component.addTransactionGET("listarUGELES", ListarUGELESTx.class);
        component.addTransactionGET("listarDocentesIE", ListarDocentesIETx.class);
        component.addTransactionGET("listarCursosDocente", ListarCursosDocenteTx.class);
        component.addTransactionGET("listarAcompDocente", ListarAcompanamientoDocenteTx.class);
        component.addTransactionPOST("registrarAcomp", RegistrarAcompanamientoTx.class);
        component.addTransactionDELETE("eliminarAcomp", EliminarAcompanamientoTx.class);
        component.addTransactionPUT("editarAcomp", EditarAcompanamientoTx.class);
        return component;
    }
}
