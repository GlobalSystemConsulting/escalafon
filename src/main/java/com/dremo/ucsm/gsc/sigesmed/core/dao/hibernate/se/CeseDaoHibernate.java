/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CeseDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cese;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class CeseDaoHibernate extends GenericDaoHibernate<Cese> implements CeseDao{
    @Override
    public List<Cese> listarxFichaEscalafonaria(int ficEscId) {
        List<Cese> ceses = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT ces from Cese as ces "
                    + "join fetch ces.fichaEscalafonaria as fe "
                    + "WHERE fe.ficEscId=" + ficEscId + " AND ces.estReg='A' "
                    + "order by ces.cesId asc";
            Query query = session.createQuery(hql);
            ceses = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los ceses\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los ceses \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return ceses;
    }

    @Override
    public Cese buscarPorId(Integer cesId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Cese ces = (Cese)session.get(Cese.class, cesId);
        session.close();
        return ces;
    }
}
