/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DatosOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.DatosOrganigrama;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author felipe
 */
public class DatosOrganigramaDaoHibernate extends GenericDaoHibernate<DatosOrganigrama> implements DatosOrganigramaDao{

    @Override
    public List<DatosOrganigrama> listarXDatosOrganigrama() {
        List<DatosOrganigrama> datosOrganigramas = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT d from DatosOrganigrama as d "
                    + "WHERE d.estReg='A'";
            Query query = session.createQuery(hql);
            datosOrganigramas = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los datos Organigramas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los datos Organigramas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return datosOrganigramas;
    }

    @Override
    public DatosOrganigrama buscarXId(Integer datOrgiId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        DatosOrganigrama item = (DatosOrganigrama)session.get(DatosOrganigrama.class, datOrgiId);
        session.close();
        return item;
    }
    
}
