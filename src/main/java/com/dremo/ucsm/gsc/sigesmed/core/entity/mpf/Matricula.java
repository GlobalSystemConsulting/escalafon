package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;
// Generated 13/02/2017 02:15:53 PM by Hibernate Tools 4.3.1

import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @autor Carlos
 */
@Entity(name="com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Matricula")
@Table(name = "matricula", schema = "pedagogico")
public class Matricula implements java.io.Serializable {

    @Id
    @Column(name = "mat_id", unique = true, nullable = false)
    private long matId;
    
    @Column(name = "est_id")
    private Long estudianteId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "est_id", insertable = false,updatable = false)
    private Estudiante estudiante;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "apo_id", nullable = false)
    private Persona personaByApoId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reg_id")
    private Persona personaByRegId;
    
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "niv_id")
//    private Nivel nivel;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pla_niv_id")
    private PlanNivel planNivel;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="matriculaEstudiante")
    List<GradoIeEstudiante> gradoIeEstudiantes=new ArrayList();
            
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_ori_id")
    private Organizacion organizacionByOrgOriId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_des_id", nullable = false)
    private Organizacion organizacionByOrgDesId;

    @Column(name = "mat_est")
    private Integer matEst;

    @Column(name = "con_mat")
    private Integer conMat;

    @Column(name = "obs")
    private String obs;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mat_fec")
    private Date matFec;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    @Column(name = "gra_id")
    private Integer gradoId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gra_id", insertable = false,updatable = false)
    private Grado grado;
    
    @Column(name = "sec_id")
    private Character seccionId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_id", insertable = false,updatable = false)
    private Seccion seccion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ini")
    private Date fecIni;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_fin", length = 29)
    private Date fecFin;

    @Column(name = "act")
    private Boolean act;

    public Matricula() {
    }

    public Matricula(long matId) {
        this.matId = matId;
    }
     
    public Matricula(long matId, Estudiante estudiante, Persona personaByApoId, Organizacion organizacionByOrgDesId) {
        this.matId = matId;
        this.estudiante = estudiante;
        this.personaByApoId = personaByApoId;
        this.organizacionByOrgDesId = organizacionByOrgDesId;
    }

    public Matricula(long matId, Estudiante estudiante, Persona personaByApoId, Persona personaByRegId, Organizacion organizacionByOrgOriId, Organizacion organizacionByOrgDesId, Integer matEst, Integer conMat, String obs, Date matFec, Integer usuMod, Date fecMod, Character estReg, Date fecIni, Date fecFin, Boolean act) {
        this.matId = matId;
        this.estudiante = estudiante;

        this.personaByApoId = personaByApoId;
        this.personaByRegId = personaByRegId;
        this.organizacionByOrgOriId = organizacionByOrgOriId;
        this.organizacionByOrgDesId = organizacionByOrgDesId;
        this.matEst = matEst;
        this.conMat = conMat;
        this.obs = obs;
        this.matFec = matFec;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.fecIni = fecIni;
        this.fecFin = fecFin;

        this.act = act;
    }

    public long getMatId() {
        return this.matId;
    }

    public void setMatId(long matId) {
        this.matId = matId;
    }

    public Estudiante getEstudiante() {
        return this.estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public Persona getPersonaByApoId() {
        return this.personaByApoId;
    }

    public void setPersonaByApoId(Persona personaByApoId) {
        this.personaByApoId = personaByApoId;
    }

    public Persona getPersonaByRegId() {
        return this.personaByRegId;
    }

    public void setPersonaByRegId(Persona personaByRegId) {
        this.personaByRegId = personaByRegId;
    }

    public Organizacion getOrganizacionByOrgOriId() {
        return this.organizacionByOrgOriId;
    }

    public void setOrganizacionByOrgOriId(Organizacion organizacionByOrgOriId) {
        this.organizacionByOrgOriId = organizacionByOrgOriId;
    }

    public Organizacion getOrganizacionByOrgDesId() {
        return this.organizacionByOrgDesId;
    }

    public void setOrganizacionByOrgDesId(Organizacion organizacionByOrgDesId) {
        this.organizacionByOrgDesId = organizacionByOrgDesId;
    }

    public Integer getMatEst() {
        return this.matEst;
    }

    public void setMatEst(Integer matEst) {
        this.matEst = matEst;
    }

    public Integer getConMat() {
        return this.conMat;
    }

    public void setConMat(Integer conMat) {
        this.conMat = conMat;
    }

    public String getObs() {
        return this.obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Date getMatFec() {
        return this.matFec;
    }

    public void setMatFec(Date matFec) {
        this.matFec = matFec;
    }

    public Integer getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return this.fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return this.estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Date getFecIni() {
        return this.fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return this.fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public Boolean getAct() {
        return this.act;
    }

    public void setAct(Boolean act) {
        this.act = act;
    }

    public PlanNivel getPlanNivel() {
        return planNivel;
    }

    public void setPlanNivel(PlanNivel planNivel) {
        this.planNivel = planNivel;
    }

    public Grado getGrado() {
        return grado;
    }

    public void setGrado(Grado grado) {
        this.grado = grado;
    }

    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

//    public Nivel getNivel() {
//        return nivel;
//    }
//
//    public void setNivel(Nivel nivel) {
//        this.nivel = nivel;
//    }

    public List<GradoIeEstudiante> getGradoIeEstudiantes() {
        return gradoIeEstudiantes;
    }

    public void setGradoIeEstudiantes(List<GradoIeEstudiante> gradoIeEstudiantes) {
        this.gradoIeEstudiantes = gradoIeEstudiantes;
    }

    public Long getEstudianteId() {
        return estudianteId;
    }

    public void setEstudianteId(Long estudianteId) {
        this.estudianteId = estudianteId;
    }

    public Integer getGradoId() {
        return gradoId;
    }

    public void setGradoId(Integer gradoId) {
        this.gradoId = gradoId;
    }

    public Character getSeccionId() {
        return seccionId;
    }

    public void setSeccionId(Character seccionId) {
        this.seccionId = seccionId;
    }

}
