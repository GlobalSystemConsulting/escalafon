/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "proyecto_actividades", schema = "institucional")

public class ProyectoActividades implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pac_id")
    private Integer pacId;
    @Column(name = "pac_des")
    private String pacDes;
    @Column(name = "pac_ini")
    @Temporal(TemporalType.DATE)
    private Date pacIni;
    @Column(name = "pac_fin")
    @Temporal(TemporalType.DATE)
    private Date pacFin;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg = "A";
    @Column(name = "pac_res")
    private Integer pacRes;      
    @Column(name = "pac_ava")
    private Integer pacAva;
    @OneToMany(cascade=CascadeType.ALL )
    @JoinColumn(name="pac_id")    
    @LazyCollection(LazyCollectionOption.FALSE)
//    @Filter(name="filtroplantilla", condition=" est_reg = 'A'")
    private Set<ProyectoRecursos> recursos;
    
    @JoinColumn(name = "pro_id", referencedColumnName = "pro_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Proyectos proyecto;

    public ProyectoActividades() {
    }

    public ProyectoActividades(Integer pacId) {
        this.pacId = pacId;
    }
    
    public ProyectoActividades(Integer pacId, Proyectos proyecto) {
        this.pacId = pacId;
        this.proyecto = proyecto;
    }
    
    public ProyectoActividades(String pacDes, Date pacIni, Date pacFin, Integer pacRes, Integer pacAva, Proyectos proyecto) {        
        this.pacDes = pacDes;
        this.pacIni = pacIni;
        this.pacFin = pacFin;                
        this.pacRes = pacRes;        
        this.proyecto = proyecto;
        this.pacAva = pacAva;
    }
    
    public ProyectoActividades(Integer pacId, String pacDes, Date pacIni, Date pacFin, Integer pacRes, Integer pacAva, Proyectos proyecto) {        
        this.pacId = pacId;
        this.pacDes = pacDes;
        this.pacIni = pacIni;
        this.pacFin = pacFin;                
        this.pacRes = pacRes;        
        this.proyecto = proyecto;
        this.pacAva = pacAva;
    }

    public Integer getPacId() {
        return pacId;
    }

    public void setPacId(Integer pacId) {
        this.pacId = pacId;
    }

    public String getPacDes() {
        return pacDes;
    }

    public void setPacDes(String pacDes) {
        this.pacDes = pacDes;
    }

    public Date getPacIni() {
        return pacIni;
    }

    public void setPacIni(Date pacIni) {
        this.pacIni = pacIni;
    }

    public Date getPacFin() {
        return pacFin;
    }

    public void setPacFin(Date pacFin) {
        this.pacFin = pacFin;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Integer getPacRes() {
        return pacRes;
    }

    public void setPacRes(Integer pacRes) {
        this.pacRes = pacRes;
    }

    public Integer getPacAva() {
        return pacAva;
    }

    public void setPacAva(Integer pacAva) {
        this.pacAva = pacAva;
    }
    
    @XmlTransient
    public Set<ProyectoRecursos> getRecursos() {
        return recursos;
    }

    public void setRecursos(Set<ProyectoRecursos> recursos) {
        this.recursos = recursos;
    }

    public Proyectos getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyectos proyecto) {
        this.proyecto = proyecto;
    }
    
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (pacId != null ? pacId.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof ProyectoActividades)) {
//            return false;
//        }
//        ProyectoActividades other = (ProyectoActividades) object;
//        if ((this.pacId == null && other.pacId != null) || (this.pacId != null && !this.pacId.equals(other.pacId))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "entidades.smdg.ProyectoActividades[ pacId=" + pacId + " ]";
//    }
    
}
