/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.SubModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author abel
 */
public class SubModuloSistemaDaoHibernate extends GenericDaoHibernate<SubModuloSistema> implements SubModuloSistemaDao {

    @Override
    public List<SubModuloSistema> buscarConFunciones() {
        List<SubModuloSistema> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar SubModuloSistemas
            String hql = "SELECT DISTINCT sm FROM SubModuloSistema sm LEFT JOIN FETCH sm.funcionSistemas f WHERE sm.estReg!='E' and f.estReg='A' ORDER BY sm.subModSisId ASC, f.funSisId ASC" ;
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los SubModuloSistemas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los SubModuloSistemas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<SubModuloSistema> buscarConModulo() {
        List<SubModuloSistema> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar SubModuloSistemas            
            String hql = "SELECT sm FROM SubModuloSistema sm JOIN FETCH sm.moduloSistema WHERE sm.estReg!='E' ORDER BY sm.subModSisId" ;
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los SubModuloSistemas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los SubModuloSistemas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
    @Override
    public List<SubModuloSistema> buscarConModuloYFunciones() {
        List<SubModuloSistema> objetos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            //listar SubModuloSistemas
            String hql = "SELECT DISTINCT sm FROM SubModuloSistema sm JOIN FETCH sm.moduloSistema LEFT JOIN FETCH sm.funcionSistemas f WHERE sm.estReg!='E' and ( f.estReg='A' or f IS NULL ) ORDER BY sm.subModSisId ASC, f.funSisId ASC" ;
            Query query = session.createQuery(hql);
            objetos = query.list();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los SubModuloSistemas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los SubModuloSistemas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objetos;
    }
}

