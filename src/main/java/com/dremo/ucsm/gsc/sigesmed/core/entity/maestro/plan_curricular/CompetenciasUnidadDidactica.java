package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.*;

/**
 * Created by Administrador on 10/11/2016.
 */
@Entity
@Table(name = "competencias_unidad_didactica", schema = "pedagogico")
public class CompetenciasUnidadDidactica implements java.io.Serializable{
    @Embeddable
    public static class CompetenciaUnidadId implements java.io.Serializable{
        @Column(name = "uni_did_id", nullable = false)
        private int uniDidId;
        @Column(name = "com_id", nullable = false)
        private int comId;
        @Column (name = "cap_id", nullable = false)
        private int capId;

        public CompetenciaUnidadId() {
        }

        public CompetenciaUnidadId(int uniDidId, int comId, int capId) {
            this.uniDidId = uniDidId;
            this.comId = comId;
            this.capId = capId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof CompetenciaUnidadId)) return false;

            CompetenciaUnidadId that = (CompetenciaUnidadId) o;

            if (uniDidId != that.uniDidId) return false;
            if (comId != that.comId) return false;
            return capId == that.capId;

        }

        @Override
        public int hashCode() {
            int result = uniDidId;
            result = 31 * result + comId;
            result = 31 * result + capId;
            return result;
        }
    }
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name="uniDidId", column=@Column(name="uni_did_id", nullable=false) ),
            @AttributeOverride(name="comId", column=@Column(name="com_id", nullable=false)),
            @AttributeOverride(name="capId", column=@Column(name="cap_id", nullable=false)) })
    private CompetenciaUnidadId id = new CompetenciaUnidadId();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uni_did_id",insertable = false, updatable = false)
    private  UnidadDidactica unidad;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns( {
            @JoinColumn(name = "com_id", referencedColumnName = "com_id",insertable = false, updatable = false),
            @JoinColumn(name = "cap_id", referencedColumnName = "cap_id",insertable = false, updatable = false) })
    private CompetenciaCapacidad competenciaCapacidad;

    @Column(name = "des", nullable = true)
    private String des;

    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinTable(
            name = "indicadores_unidad_aprendizaje", schema = "pedagogico",
            joinColumns = {@JoinColumn(name = "cap_id") ,@JoinColumn(name = "com_id"),@JoinColumn(name = "uni_did_id")},
            inverseJoinColumns = @JoinColumn(name = "ind_apr_id")

    )
    private Set<IndicadorAprendizaje> indicadores = new HashSet<>();
    //private List<IndicadorAprendizaje> indicadores = new ArrayList<>();

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public CompetenciasUnidadDidactica() {
    }

    public CompetenciasUnidadDidactica(UnidadDidactica unidad, CompetenciaCapacidad competenciaCapacidad, String des) {

        this.unidad = unidad;
        this.competenciaCapacidad = competenciaCapacidad;

        this.id.uniDidId = unidad.getUniDidId();
        this.id.comId = competenciaCapacidad.getCom().getComId();
        this.id.capId = competenciaCapacidad.getCap().getCapId();

        this.des = des;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public CompetenciaUnidadId getId() {
        return id;
    }

    public UnidadDidactica getUnidad() {
        return unidad;
    }

    public void setUnidad(UnidadDidactica unidad) {
        this.unidad = unidad;
    }

    public CompetenciaCapacidad getCompetenciaCapacidad() {
        return competenciaCapacidad;
    }

    public void addIndicador(IndicadorAprendizaje indicador){
        this.indicadores.add(indicador);
    }
    /*public List<IndicadorAprendizaje> getIndicadores() {
        return indicadores;
    }*/
    public Set<IndicadorAprendizaje> getIndicadores(){
        return indicadores;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
