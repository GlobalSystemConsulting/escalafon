/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.NivelAcademicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.NivelAcademico;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author alex
 */
public class ListarNivAcaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarNivAcaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        List<NivelAcademico> documentos = null;
        NivelAcademicoDao tipoFormacionDao = (NivelAcademicoDao)FactoryDao.buildDao("se.NivelAcademicoDao");
        
        try{
            documentos = tipoFormacionDao.listarAll();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar los niveles academicos",e);
            System.out.println("No se pudo listar los niveles academicos. Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listar los niveles academicos. Error: ", e.getMessage() );
        }
        //Fin
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(NivelAcademico d: documentos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("id", d.getNivAcaId());
            oResponse.put("nom", d.getNivAcaNom());
            oResponse.put("pad", d.getNivAcaPad());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los niveles academicos fueron listados exitosamente", miArray);
    }
}
