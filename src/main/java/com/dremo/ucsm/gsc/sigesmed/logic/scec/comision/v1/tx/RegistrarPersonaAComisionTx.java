package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.IntegranteComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.Comision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.IntegranteComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 01/10/16.
 */
public class RegistrarPersonaAComisionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(RegistrarPersonaAComisionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {

        String idCom = wr.getMetadataValue("com");
        String idCar = wr.getMetadataValue("car");
        //JSONObject jsondata = (JSONObject) wr.getData();
        JSONObject jsonPersona = (JSONObject) wr.getData();
        Persona persona = new Persona();
        persona.setNom(jsonPersona.getString("nom"));
        persona.setApePat(jsonPersona.getString("apep"));
        persona.setApeMat(jsonPersona.getString("apem"));
        persona.setDni(jsonPersona.getString("dni"));
        persona.setNum1(jsonPersona.optString("fij",""));
        persona.setNum2(jsonPersona.optString("cel",""));
        persona.setEmail(jsonPersona.optString("ema",""));
        String fecNacTs =jsonPersona.optString("nac","");
        try{
            persona.setFecNac(new Date(Long.parseLong(fecNacTs)));
        }catch (Exception e){
            persona.setFecNac(new Date());
        }
        //persona.setFecNac(new Date());//modificar con la fecha actual
        return registrarPersona(persona,Integer.parseInt(idCom),Integer.parseInt(idCar));
    }
    private WebResponse registrarPersona(Persona persona,int idCom,int idCar){
        try{
            PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("PersonaDao");

            if(personaDao.buscarPorDNI(persona.getDni()) == null){
                //no existe la persona y se puede realizar el registro
                personaDao.insert(persona);
                //ahora registraremos al nuevo participante

                ComisionDao comisionDao = (ComisionDao) FactoryDao.buildDao("scec.ComisionDao");
                CargoComisionDao cargoComisionDao = (CargoComisionDao) FactoryDao.buildDao("scec.CargoComisionDao");

                Comision comision = comisionDao.buscarPorIdConIntegrantes(idCom);

                IntegranteComision integranteComision = new IntegranteComision(persona,
                        comisionDao.buscarPorIdConIntegrantes(idCom),cargoComisionDao.buscarPorId(idCar),new Date(),null);
                integranteComision.setFecMod(new Date());
                integranteComision.setEstReg('A');
                integranteComision.setUsuMod(1);

                IntegranteComisionDao integranteComisionDao = (IntegranteComisionDao) FactoryDao.buildDao("scec.IntegranteComisionDao");
                integranteComisionDao.insert(integranteComision);
                WebResponse wr = WebResponse.crearWebResponseExito("Se registraron los datos",WebResponse.OK_RESPONSE);
                wr.setData(new JSONObject(EntityUtil.objectToJSONString(new String[]{"perId"}, new String[]{"cod"}, persona)));
                return wr;
            }else{
                return WebResponse.crearWebResponseError("El DNI: " + persona.getDni() + ", ya esta registrado en el sistema",WebResponse.BAD_RESPONSE);

            }
        }catch (Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar los datos",WebResponse.BAD_RESPONSE);
        }
    }
}
