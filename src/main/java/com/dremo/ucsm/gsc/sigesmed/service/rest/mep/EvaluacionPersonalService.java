/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.service.rest.mep;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.TypeTransaction;
import com.dremo.ucsm.gsc.sigesmed.service.rest.tramite_documentario.TramiteDocumentarioService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import org.json.JSONTokener;
import org.json.JSONObject;

/**
 *
 * @author geank
 */
@Path("/evaluacion_personal")
public class EvaluacionPersonalService {
    private static Logger logger = Logger.getLogger(EvaluacionPersonalService.class.getName());

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String recursoPost(String content){
        logger.log(Level.INFO,"POST: {0}",content);
        JSONObject jsonObject = (JSONObject)new JSONTokener(content).nextValue();
        WebRequest request = WebRequest.createFromJSON(jsonObject);
        WebResponse response = request.invoke(Sigesmed.MODULO_EVALUACION_PERSONAL, TypeTransaction.type_transaction_POST);
        return response.toJSON().toString();
    }
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String recursoGet(@QueryParam("content") String content){
        logger.log(Level.INFO,"GET: {0}",content);
        JSONObject jsonObject = (JSONObject)new JSONTokener(content).nextValue();
        WebRequest request = WebRequest.createFromJSON(jsonObject);
        WebResponse response = request.invoke(Sigesmed.MODULO_EVALUACION_PERSONAL, TypeTransaction.type_transaction_GET);
        return response.toJSON().toString();
    }
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public String recursoPut(String content){
        logger.log(Level.INFO,"PUT:{0}",content);
        JSONObject jsonObject = (JSONObject)new JSONTokener(content).nextValue();
        WebRequest request = WebRequest.createFromJSON(jsonObject);
        WebResponse response = request.invoke(Sigesmed.MODULO_EVALUACION_PERSONAL,TypeTransaction.type_transaction_PUT);
        return response.toJSON().toString();
    }
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public String recursoDelete(@QueryParam("content") String content){
        logger.log(Level.INFO,"DELETE: {0}",content);
        JSONObject jsonObject = (JSONObject)new JSONTokener(content).nextValue();
        WebRequest request = WebRequest.createFromJSON(jsonObject);
        WebResponse response = request.invoke(Sigesmed.MODULO_EVALUACION_PERSONAL, TypeTransaction.type_transaction_DELETE);
        return response.toJSON().toString();
    }
}
