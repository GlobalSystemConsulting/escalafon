package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "UsuarioSessionCapacitacion")
@Table(name = "usuario_session", schema = "public")
public class UsuarioSession implements Serializable {

    @Id
    @Column(name = "usu_ses_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_usuario_session", sequenceName = "usuario_session_usu_ses_id_seq")
    @GeneratedValue(generator = "secuencia_usuario_session")
    private int usuSesId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_cre")
    private Date fecCre;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg", length = 1)
    private Character estReg;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id", nullable = false)
    private Organizacion organizacion;
    
    @Column(name = "rol_id", nullable = false)
    private int rolId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id", nullable = false)
    private Usuario usuario;
    
    @Column(name = "are_id")
    private Integer areId;

    public UsuarioSession() {}
    
    public UsuarioSession(Organizacion organizacion, int rolId, Usuario usuario) {
        this.fecCre = new Date();
        this.fecMod = new Date();
        this.usuMod = usuario.getUsuId();
        this.estReg = 'A';
        this.organizacion = organizacion;
        this.rolId = rolId;
        this.usuario = usuario;
        this.areId = null;
    }

    public int getUsuSesId() {
        return usuSesId;
    }

    public void setUsuSesId(int usuSesId) {
        this.usuSesId = usuSesId;
    }

    public Date getFecCre() {
        return fecCre;
    }

    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public int getRolId() {
        return rolId;
    }

    public void setRolId(int rolId) {
        this.rolId = rolId;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getAreId() {
        return areId;
    }

    public void setAreId(Integer areId) {
        this.areId = areId;
    }
}
