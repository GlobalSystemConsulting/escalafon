/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "formacion_educativa", schema="administrativo")

public class FormacionEducativa implements Serializable {

    @Id
    @Column(name = "for_edu_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_formacion_educativa", sequenceName="administrativo.formacion_educativa_for_edu_id_seq" )
    @GeneratedValue(generator="secuencia_formacion_educativa")
    private Integer forEduId;
    
    @Column(name = "tip_for_id")
    private Integer tipForId;
    
    @Column(name = "niv_aca_id")
    private Integer nivAcaId;
    
    @Column(name = "tip_doc_id")
    private Integer tipDocId;
    
    @Column(name = "num_doc")
    private String numDoc;
    
    @Column(name = "fec_doc")
    @Temporal(TemporalType.DATE)
    private Date fecDoc;
    
    @Column(name = "esp_aca")
    private String espAca;
    
    @Column(name = "est_con")
    private Boolean estCon;
    
    @Column(name = "fec_ini_for")
    @Temporal(TemporalType.DATE)
    private Date fecIniFor;
    
    @Column(name = "fec_ter_for")
    @Temporal(TemporalType.DATE)
    private Date fecTerFor;
    
    @Column(name = "cen_est")
    private String cenEst;
    
    @Column(name = "nac_id")
    private Integer nacId;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
     
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "per_id")
    private Persona persona;

    public FormacionEducativa() {
    }

    public FormacionEducativa(Persona persona, Integer tipForId, Integer nivAcaId, Integer tipDocId, String numDoc, Date fecDoc, String espAca, Boolean estCon, Date fecIniFor, Date fecTerFor, String cenEst, Integer nacId, Integer usuMod, Date fecMod, Character estReg) {
        this.tipForId = tipForId;
        this.nivAcaId = nivAcaId;
        this.tipDocId = tipDocId;
        this.numDoc = numDoc;
        this.fecDoc = fecDoc;
        this.espAca = espAca;
        this.estCon = estCon;
        this.fecIniFor = fecIniFor;
        this.fecTerFor = fecTerFor;
        this.cenEst = cenEst;
        this.nacId = nacId;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.persona = persona;
    }

    public FormacionEducativa(Integer forEduId) {
        this.forEduId = forEduId;
    }

    public Integer getForEduId() {
        return forEduId;
    }

    public void setForEduId(Integer forEduId) {
        this.forEduId = forEduId;
    }

    public Integer getTipForId() {
        return tipForId;
    }

    public void setTipForId(Integer tipForId) {
        this.tipForId = tipForId;
    }

    public Integer getNivAcaId() {
        return nivAcaId;
    }

    public void setNivAcaId(Integer nivAcaId) {
        this.nivAcaId = nivAcaId;
    }

    public Integer getTipDocId() {
        return tipDocId;
    }

    public void setTipDocId(Integer tipDocId) {
        this.tipDocId = tipDocId;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public Date getFecDoc() {
        return fecDoc;
    }

    public void setFecDoc(Date fecDoc) {
        this.fecDoc = fecDoc;
    }

    public String getEspAca() {
        return espAca;
    }

    public void setEspAca(String espAca) {
        this.espAca = espAca;
    }

    public Boolean getEstCon() {
        return estCon;
    }

    public void setEstCon(Boolean estCon) {
        this.estCon = estCon;
    }

    public Date getFecIniFor() {
        return fecIniFor;
    }

    public void setFecIniFor(Date fecIniFor) {
        this.fecIniFor = fecIniFor;
    }

    public Date getFecTerFor() {
        return fecTerFor;
    }

    public void setFecTerFor(Date fecTerFor) {
        this.fecTerFor = fecTerFor;
    }

    public String getCenEst() {
        return cenEst;
    }

    public void setCenEst(String cenEst) {
        this.cenEst = cenEst;
    }

    public Integer getNacId() {
        return nacId;
    }

    public void setNacId(Integer nacId) {
        this.nacId = nacId;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public String toString() {
        return "FormacionEducativa{" + "forEduId=" + forEduId + ", tipForId=" + tipForId + ", nivAcaId=" + nivAcaId + ", tipDocId=" + tipDocId + ", numDoc=" + numDoc + ", fecDoc=" + fecDoc + ", espAca=" + espAca + ", estCon=" + estCon + ", fecIniFor=" + fecIniFor + ", fecTerFor=" + fecTerFor + ", cenEst=" + cenEst + ", nacId=" + nacId + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + ", persona=" + persona + '}';
    }

}
