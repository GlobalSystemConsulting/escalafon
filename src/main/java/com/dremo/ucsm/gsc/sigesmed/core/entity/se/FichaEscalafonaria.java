/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "ficha_escalafonaria", schema="administrativo")

public class FichaEscalafonaria implements Serializable {
    
    @Id
    @Column(name = "fic_esc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_ficha_escalafonaria", sequenceName="administrativo.ficha_escalafonaria_fic_esc_id_seq")
    @GeneratedValue(generator="secuencia_ficha_escalafonaria")
    private Integer ficEscId;
    
 
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "tra_id")
    private Trabajador trabajador;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<LegajoPersonal> legajos = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Vacacion> vacaciones = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Licencia> licencias = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Cese> ceses = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Ascenso> ascensos = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Desplazamiento> desplazamientos = new HashSet(0);
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tip_doc_id", nullable=false)
    private TipoDoc tipoDoc;
    
    public FichaEscalafonaria() {
    }
    
    public FichaEscalafonaria(Integer ficEscId) {
        this.ficEscId = ficEscId;
    }
    
    public FichaEscalafonaria(Trabajador trabajador, String autEss, Boolean perDis, String regCon, Integer usuMod, Date fecMod, Character estReg) {
        this.trabajador = trabajador;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    
    public FichaEscalafonaria(Trabajador trabajador, String autEss, String sisPen, String nomAfp, String codCuspp, Date fecIngAfp, Boolean perDis, String regCon, Character gruOcu, Integer usuMod, Date fecMod, Character estReg) {
        this.trabajador = trabajador;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    
    public FichaEscalafonaria(Trabajador trabajador, String autEss, String sisPen, Boolean perDis, String regCon, Character gruOcu, Integer usuMod, Date fecMod, Character estReg) {
        this.trabajador = trabajador;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
     public FichaEscalafonaria(Trabajador trabajador, Integer usuMod, Date fecMod, Character estReg) {
        this.trabajador = trabajador;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }


    public int getFicEscId() {
        return ficEscId;
    }

    public void setFicEscId(Integer ficEscId) {
        this.ficEscId = ficEscId;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }
    
    public Set<Ascenso> getAscensos() {
        return ascensos;
    }

    public void setAscensos(Set<Ascenso> ascensos) {
        this.ascensos = ascensos;
    }

    public Set<Desplazamiento> getDesplazamientos() {
        return desplazamientos;
    }

    public void setDesplazamientos(Set<Desplazamiento> desplazamientos) {
        this.desplazamientos = desplazamientos;
    }

    public Set<LegajoPersonal> getLegajos() {
        return legajos;
    }

    public void setLegajos(Set<LegajoPersonal> legajos) {
        this.legajos = legajos;
    }

    public Set<Vacacion> getVacaciones() {
        return vacaciones;
    }

    public void setVacaciones(Set<Vacacion> vacaciones) {
        this.vacaciones = vacaciones;
    }

    public Set<Licencia> getLicencias() {
        return licencias;
    }

    public void setLicencias(Set<Licencia> licencias) {
        this.licencias = licencias;
    }

    public Set<Cese> getCeses() {
        return ceses;
    }

    public void setCeses(Set<Cese> ceses) {
        this.ceses = ceses;
    }
    
    
    
    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    
    public int getTraId() {
        return trabajador.getTraId();
    }

    public void setTraId(int traId) {
        this.trabajador.setTraId(traId);
    }

    public TipoDoc getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(TipoDoc tipoDoc) {
        this.tipoDoc = tipoDoc;
    }
    
    

    @Override
    public String toString() {
        return "FichaEscalafonaria{" + "ficEscId=" + ficEscId + '}';
    }
   
    
}
