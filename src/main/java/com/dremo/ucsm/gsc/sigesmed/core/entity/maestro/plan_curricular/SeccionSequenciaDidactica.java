package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 02/12/2016.
 */
@Entity
@Table(name = "seccion_sequencia_didactica", schema = "pedagogico")
public class SeccionSequenciaDidactica implements java.io.Serializable {
    @SequenceGenerator(name = "seccion_sequencia_didactica_sec_sec_did_seq", sequenceName = "pedagogico.seccion_sequencia_didactica_sec_sec_did_seq")
    @GeneratedValue(generator = "seccion_sequencia_didactica_sec_sec_did_seq")
    @Id
    @Column(name = "sec_sec_did", nullable = false,unique = true)
    private int secSeqDid;
    @Column(name = "nom_sec")
    private String nomSec;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public SeccionSequenciaDidactica() {
    }

    public SeccionSequenciaDidactica(String nomSec) {
        this.nomSec = nomSec;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getSecSeqDid() {
        return secSeqDid;
    }

    public void setSecSeqDid(int secSeqDid) {
        this.secSeqDid = secSeqDid;
    }

    public String getNomSec() {
        return nomSec;
    }

    public void setNomSec(String nomSec) {
        this.nomSec = nomSec;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
