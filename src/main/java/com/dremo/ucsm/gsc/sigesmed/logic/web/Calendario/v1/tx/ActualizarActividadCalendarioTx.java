/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Calendario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.ActividadCalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.ActividadCalendario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abel
 */
public class ActualizarActividadCalendarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        ActividadCalendario actividad = new ActividadCalendario();
        List<Integer> listaDestinatarios = new ArrayList<Integer>();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int actividadID = requestData.getInt("actividadID");
            String titulo = requestData.getString("titulo");
            String descripcion = requestData.optString("descripcion");
            String tipo = requestData.getString("tipo");
            Date horaInicio = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(requestData.getString("horaInicio")) ;
            
            Date horaFin = null;
            if( !requestData.optString("horaFin").contentEquals("") )
                horaFin = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(requestData.getString("horaFin"));
            
            
            actividad = new ActividadCalendario(actividadID, titulo, descripcion, horaInicio,horaFin,tipo.charAt(0), wr.getIdUsuario(),'A');
            
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR, -1);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
      
            Date hoy = calendar.getTime();
            
            //verificando que la fecha de inicio de la actividad mayor a la fecha actual
            if( actividad.getHorIni().compareTo( hoy ) <0 ){
                System.out.println("las actividades deben ser programadas a partir de la fecha actual");
                return WebResponse.crearWebResponseError("error con la fecha de inicio", "las actividades deben ser programadas a partir de la fecha actual" );
            }
            
        }catch(Exception e){
            System.out.println("No se pudo actualizar la actividad programada, datos incorrectos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la actividad programada, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ActividadCalendarioDao bandejaDao = (ActividadCalendarioDao)FactoryDao.buildDao("web.ActividadCalendarioDao");
        try{
            bandejaDao.update(actividad);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar la actividad programada\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la actividad programada", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        /*JSONObject oResponse = new JSONObject();
        oResponse.put("estado",""+Mensaje.ESTADO_VISTO);*/
        return WebResponse.crearWebResponseExito("La actualizar la actividad programada se actualizo correctamente");
        //Fin
    }
    
}
