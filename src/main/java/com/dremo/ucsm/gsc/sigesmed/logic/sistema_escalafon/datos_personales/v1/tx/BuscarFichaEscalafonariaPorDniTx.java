/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.comunication.pojo.Documento;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorOrganigramaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Direccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TrabajadorOrganigramaDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class BuscarFichaEscalafonariaPorDniTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(BuscarFichaEscalafonariaPorDniTx.class.getName());
    
    static class Page {

        List<Documento> Documentos;

        public void deterPaginas (){
            int max=0;
            for (Documento documento : Documentos) {
                documento.deterPagInicio();/*
                if(max > 0) break;
                max++;*/
                System.out.println("doc "+documento.getCoddocumento());
            }
        }    
    } 

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject requestData = (JSONObject)wr.getData();
        
        String perDNI = requestData.getString("perDni");
        Integer opcion = requestData.getInt("opcion");
        int traId = requestData.getInt("traId");
        
        String json="";
        
        
        List<UsuarioSession> userSessions = null;
        UsuarioDao usuarioDao = (UsuarioDao)FactoryDao.buildDao("UsuarioDao");
        try{
            userSessions = usuarioDao.buscarPorUsuario(perDNI);   // YOU ARE HERE
        }catch(Exception e){
            System.out.println("No se pudo Listar los usuarios del Sistema\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los usuarios del Sistema ", e.getMessage() );
        }
        
        JSONArray aSessiones = new JSONArray();
        for( UsuarioSession session: userSessions){
            JSONObject oSession = new JSONObject();
            oSession.put("rolID",session.getRol().getRolId() );
            oSession.put("rol",session.getRol().getNom() );
            oSession.put("sessionID",session.getUsuSesId() );
            oSession.put("areaID",session.getAreId());
            oSession.put("organizacionID",session.getOrganizacion().getOrgId() );
            oSession.put("organizacion",session.getOrganizacion().getNom() );                
            oSession.put("estado",""+session.getEstReg());

            aSessiones.put(oSession);
        }
                
        
        FichaEscalafonariaDao fichaEscalafonariaDao = (FichaEscalafonariaDao)FactoryDao.buildDao("se.FichaEscalafonariaDao");
        TrabajadorOrganigramaDetalleDao traOrgDetDao = (TrabajadorOrganigramaDetalleDao) FactoryDao.buildDao("se.TrabajadorOrganigramaDetalleDao");
        OrganigramaDao organigramaDao =(OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
        CategoriaDao categoriaDao =(CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
        CargoDao cargoDao =(CargoDao)FactoryDao.buildDao("se.CargoDao");
        UbicacionDao ubicacionDao =(UbicacionDao)FactoryDao.buildDao("se.UbicacionDao");
        
        FichaEscalafonaria fichaEscalafonaria = new FichaEscalafonaria();
        TrabajadorOrganigramaDetalle traOrgDet = new TrabajadorOrganigramaDetalle();
        Organigrama organigrama = new Organigrama();
        Cargo cargo = new Cargo();
        Categoria categoria = new Categoria();
        Ubicacion ubicacion = new Ubicacion();
        
        try{
            fichaEscalafonaria = fichaEscalafonariaDao.buscarPorDNIyTraId(perDNI, traId);

        }catch(Exception e){
            System.out.println("No se encontro la ficha escalafonaria \n"+e);
            logger.log(Level.SEVERE,"Buscar ficha escalafonaria",e);
            return WebResponse.crearWebResponseError("No se encontro la ficha escalafonaria ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONObject data = new JSONObject();        
        if (opcion == 1){
             
            JSONObject persona = new JSONObject();
            JSONObject trabajador = new JSONObject();
            JSONObject ficha = new JSONObject();
            
            String apePat = fichaEscalafonaria.getTrabajador().getPersona().getApePat();
            String apeMat = fichaEscalafonaria.getTrabajador().getPersona().getApeMat();
            String nom = fichaEscalafonaria.getTrabajador().getPersona().getNom();
            Date fecNac = fichaEscalafonaria.getTrabajador().getPersona().getFecNac();
            String num1 = fichaEscalafonaria.getTrabajador().getPersona().getNum1();
            String num2 = fichaEscalafonaria.getTrabajador().getPersona().getNum2();
            String nac = fichaEscalafonaria.getTrabajador().getPersona().getNac();
            String fij = fichaEscalafonaria.getTrabajador().getPersona().getFij();
            String email = fichaEscalafonaria.getTrabajador().getPersona().getEmail();
            Character sex = fichaEscalafonaria.getTrabajador().getPersona().getSex();
            Character estCiv = fichaEscalafonaria.getTrabajador().getPersona().getEstCiv();
            String depNac = fichaEscalafonaria.getTrabajador().getPersona().getDepNac();
            String proNac = fichaEscalafonaria.getTrabajador().getPersona().getProNac();
            String disNac = fichaEscalafonaria.getTrabajador().getPersona().getDisNac();
            String licCond = fichaEscalafonaria.getTrabajador().getPersona().getLicCond();
            String idiom = fichaEscalafonaria.getTrabajador().getPersona().getIdioma();
            String bonCaf = fichaEscalafonaria.getTrabajador().getPersona().getBonCaf();
            String foto=fichaEscalafonaria.getTrabajador().getPersona().getFoto();
            
            persona.put("perId", fichaEscalafonaria.getTrabajador().getPersona().getPerId());
            persona.put("perCod", fichaEscalafonaria.getTrabajador().getPersona().getPerCod());
            persona.put("apePat", apePat==null?"":apePat);
            persona.put("apeMat",  apeMat==null?"":apeMat);
            persona.put("nom", nom==null?"":nom);
            persona.put("dni", fichaEscalafonaria.getTrabajador().getPersona().getDni());
            persona.put("nac", nac != null ? nac:"");
            persona.put("fecNac", fecNac==null?"":fecNac);
            persona.put("num1", num1==null?"":num1);
            persona.put("num2", num2==null?"":num2);
            persona.put("fij", fij==null?"":fij);
            persona.put("email", email==null?"":email);
            persona.put("sex", sex==null?"":sex);
            persona.put("estCiv", estCiv==null?"":estCiv);
            persona.put("depNac", depNac==null?"":depNac);
            persona.put("proNac", proNac==null?"":proNac);
            persona.put("disNac", disNac==null?"":disNac);
            persona.put("licCond", licCond==null?"":licCond);
            persona.put("idiom", idiom==null?"":idiom);
            persona.put("bonCaf", bonCaf==null?"":bonCaf);
            persona.put("foto",foto==null?"":foto);
            
            trabajador.put("traId", fichaEscalafonaria.getTrabajador().getTraId());
            if (fichaEscalafonaria.getTrabajador().getOrgiId()!= null && fichaEscalafonaria.getTrabajador().getOrgiId() > 0){ 
                traOrgDet = traOrgDetDao.buscarPorId(fichaEscalafonaria.getTrabajador().getTraId(), fichaEscalafonaria.getTrabajador().getOrgiId());
                
                organigrama = organigramaDao.obtenerDetalle(traOrgDet.getOrgiId());
                trabajador.put("orgiId", organigrama.getOrgiId());
                trabajador.put("datOrgId", organigrama.getTipoOrganigrama().getDatosOrganigramas().getDatOrgiId());
                trabajador.put("nomDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getNomDatOrgi());
                trabajador.put("anioDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getAnioDatOrgi());
                
                trabajador.put("nomOrgiN3", organigrama.getNomOrgi());
                trabajador.put("nomOrgiN2", organigrama.getOrganigramaPadre().getNomOrgi());
                trabajador.put("nomOrgiN1", organigrama.getOrganigramaPadre().getOrganigramaPadre().getNomOrgi());
                
                if (traOrgDet.getPlaId() != null){
                    trabajador.put("plaId",traOrgDet.getPlaId());
                }else{
                    trabajador.put("plaId",0);
                }
                if (traOrgDet.getCatId() != null){
                    categoria = categoriaDao.obtenerDetalle(traOrgDet.getCatId());
                    trabajador.put("catId", traOrgDet.getCatId());
                    trabajador.put("nomCat", categoria.getNomCat());
                    trabajador.put("nomPla", categoria.getPlanilla().getNomPla());
                }else{
                    trabajador.put("catId",0);
                    trabajador.put("nomCat", "");
                    trabajador.put("nomPla", "");
                }
                if (traOrgDet.getCarId() != null && traOrgDet.getCarId()>0){
                    cargo = cargoDao.buscarPorId(traOrgDet.getCarId());
                    trabajador.put("carId",traOrgDet.getCarId());
                    trabajador.put("nomCar", cargo.getNomCar());
                }else{
                    trabajador.put("carId",0);
                    trabajador.put("nomCar", "");
                }
                
                if (traOrgDet.getUbiId() != null){
                    ubicacion = ubicacionDao.buscarPorId(traOrgDet.getUbiId());
                    trabajador.put("ubiId",traOrgDet.getUbiId());
                    trabajador.put("nomUbi",ubicacion.getNom());
                }else{
                    trabajador.put("ubiId",0);
                    trabajador.put("nomUbi","");
                }
            }else{
                trabajador.put("orgiId", 0);
                trabajador.put("plaId",0);
                trabajador.put("catId",0);
                trabajador.put("nomCat", "");
                trabajador.put("nomPla", "");
                trabajador.put("carId",0);
                trabajador.put("nomCar", "");
                trabajador.put("datOrgId", 0);
                trabajador.put("nomOrgi", "");
                trabajador.put("nomDatOrgi", "");
                trabajador.put("anioDatOrgi", 0);
                trabajador.put("ubiId", 0);
                trabajador.put("nomUbi","");
                trabajador.put("nomOrgiN3", "");
                trabajador.put("nomOrgiN2", "");
                trabajador.put("nomOrgiN1", "");
            }
            
            ficha.put("ficEscId", fichaEscalafonaria.getFicEscId());
            
            data.put("persona", persona);
            data.put("trabajador", trabajador);
            data.put("ficha", ficha);
            
            data.put("sessiones", aSessiones);
 
        }else if(opcion==5){
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            Persona miPersona = fichaEscalafonaria.getTrabajador().getPersona();
            
            JSONObject persona = new JSONObject();
            JSONObject trabajador = new JSONObject();
            JSONObject ficha = new JSONObject();
            
            String apePat = miPersona.getApePat();
            String apeMat = miPersona.getApeMat();
            String nom = miPersona.getNom();
            Character sex = miPersona.getSex();
            Integer estCivId = miPersona.getEstCivId()==null?0:miPersona.getEstCivId();
            String dni=miPersona.getDni();
            String pas=miPersona.getPas();
            Boolean estAseEss=miPersona.getBoolSalud();
            String autEss = miPersona.getAutEss();
            String fij = miPersona.getFij();
            String num1 = miPersona.getNum1();
            String num2 = miPersona.getNum2();
            String email = miPersona.getEmail();
            Integer nacId = miPersona.getNacionalidad()==null?0:miPersona.getNacionalidad().getNacId();
            String depNac = miPersona.getDepNac();
            String proNac = miPersona.getProNac();
            String disNac = miPersona.getDisNac();
            Date fecNac = miPersona.getFecNac();
            
            String sisPen = miPersona.getSisPen();
            String tipAfp = miPersona.getTipAfp();
            String codCuspp = miPersona.getCodCuspp();
            Date fecIngAfp = miPersona.getFecIngAfp();
            Date fecTraAfp = miPersona.getFecTraAfp();
            Boolean perDis = miPersona.getPerDis();
            String regCon = miPersona.getRegCon();
            Integer idiomId = miPersona.getIdiomas()==null?0:miPersona.getIdiomas().getIdiId();
            String licCond = miPersona.getLicCond();
            String bonCaf = miPersona.getBonCaf();
            String foto=fichaEscalafonaria.getTrabajador().getPersona().getFoto();
            
            persona.put("perId", miPersona.getPerId());
            persona.put("perCod", miPersona.getPerCod());
            persona.put("apePat", apePat==null?"":apePat);
            persona.put("apeMat",  apeMat==null?"":apeMat);
            persona.put("nom", nom==null?"":nom);
            persona.put("sex", sex==null?"":sex);
            persona.put("estCivId", estCivId);
            //edad
            persona.put("dni", dni);
            persona.put("pas", pas==null?"":pas);
            persona.put("estAseEss", estAseEss==null?"":estAseEss);
            persona.put("autEss", autEss==null?"":autEss);
            persona.put("fij", fij==null?"":fij);
            persona.put("num1", num1==null?"":num1);
            persona.put("num2", num2==null?"":num2);
            persona.put("email", email==null?"":email);
            persona.put("nacId", nacId);
            persona.put("depNac", depNac==null?"":depNac);
            persona.put("proNac", proNac==null?"":proNac);
            persona.put("disNac", disNac==null?"":disNac);
            persona.put("fecNac", fecNac==null?"":sdi.format(fecNac));
            persona.put("sisPen", sisPen==null?"NUL":sisPen);
            persona.put("tipAfp", tipAfp==null?"":tipAfp);
            persona.put("codCuspp", codCuspp==null?"":codCuspp);
            persona.put("fecIngAfp", fecIngAfp==null?"":sdi.format(fecIngAfp));
            persona.put("fecTraAfp", fecTraAfp==null?"":sdi.format(fecTraAfp));
            persona.put("perDis", perDis==null?"":perDis);
            persona.put("regCon", regCon==null?"":regCon);
            persona.put("idiomId", idiomId);
            persona.put("licCond", licCond==null?"":licCond);
            persona.put("bonCaf", bonCaf==null?"":bonCaf);
            persona.put("foto",foto==null?"":foto);
            
            trabajador.put("traId", fichaEscalafonaria.getTrabajador().getTraId());
            ficha.put("ficEscId", fichaEscalafonaria.getFicEscId());
            
            data.put("persona", persona);
            data.put("trabajador", trabajador);
            data.put("ficha", ficha);
            data.put("sessiones", aSessiones);
        }
        else{
            data.put("ficEscId", fichaEscalafonaria.getFicEscId());
        }
        /*try {
            URL url = new URL("http://190.119.213.87:8091/rideaoffice/apijson?dni="+Integer.parseInt(perDNI));
            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            http.setConnectTimeout(5000);
            http.connect();
            int statusCode = http.getResponseCode();
            /*switch (statuscode) {
                case HttpURLConnection.HTTP_OK:
                    connected = true;
                    break; // fine, go on
                case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                    log.warning(entries + " **gateway timeout**");
                    break;// retry
                case HttpURLConnection.HTTP_UNAVAILABLE:
                    System.out.println(entries + "**unavailable**");
                    break;// retry, server is unstable
                default:
                    log.severe(entries + " **unknown response code**.");
                    break outer; // abort
            }
           //json = readUrl("http://200.37.190.57:8035/rideaoffice/apijson?dni="+perDni);
           // json = readUrl("http://localhost:8080/rideaoffice/apijson?dni="+perDni);
            System.out.println("code: "+statusCode);
            //json = readUrl("http://192.168.5.25:8080/rideaoffice/apijson?dni="+Integer.parseInt(perDNI));
            Gson gson = new Gson();
            Page page = gson.fromJson(json, Page.class);
            page.deterPaginas();
            
            String jsonready = new Gson().toJson(page);       
        
            JSONObject jsonobj = new JSONObject(jsonready); 
            data.put("Documentos", jsonobj);
         
        } catch (Exception ex) {
            Logger.getLogger(ListarLegajosRideaTx.class.getName()).log(Level.SEVERE, null, ex);
            return WebResponse.crearWebResponseExito("Se listo correctamente, pero hubo un problema al listar los legajos", data);
            //return WebResponse.crearWebResponseError("No se pudo listar los legajos", ex.getMessage() );
        }*/
        
        return WebResponse.crearWebResponseExito("Se listo correctamente", data);
    }
    
    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1) {
                buffer.append(chars, 0, read);
            }

            return buffer.toString();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }
    
}
