package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ArchivosCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ContenidoSeccionCarpeta;

import java.util.List;

/**
 * Created by Administrador on 28/12/2016.
 */
public interface ContenidoSeccionCarpetaDao extends GenericDao<ContenidoSeccionCarpeta> {
    Usuario buscarUsuarioporId(int idUser);
    List<Rol> buscarRolesUsuario(int idUsuario);
    ContenidoSeccionCarpeta buscarContenidoPorId(int idCont);
    List<ArchivosCarpeta> listarDocumentosCarpeta(int idCar,int idIE,int idUgel, int idDoc);
    List<ArchivosCarpeta> listarDocumentosCarpetaAdmin(int idCar,int idOrg,int idUser,int tipUsuario);
}
