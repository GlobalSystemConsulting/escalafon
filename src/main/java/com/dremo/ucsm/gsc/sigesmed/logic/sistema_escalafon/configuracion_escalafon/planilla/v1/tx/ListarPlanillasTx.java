/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.planilla.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PlanillaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Planilla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarPlanillasTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarPlanillasTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<Planilla> planillas = null;
        PlanillaDao planillaDao = (PlanillaDao)FactoryDao.buildDao("se.PlanillaDao");
        
        try{
            planillas = planillaDao.listarxPlanilla();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar planillas",e);
            System.out.println("No se pudo listar las planillas.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas planillas.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Planilla p: planillas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("plaId", p.getPlaId());
            oResponse.put("codPla",p.getCodPla());
            oResponse.put("abrPla",p.getAbrPla());
            oResponse.put("nomPla",p.getNomPla());
            oResponse.put("camPla",p.getCamPla());
            oResponse.put("fecMod",p.getFecMod());
            oResponse.put("UsuMod",p.getUsuMod());
            oResponse.put("estReg",p.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las planillas fueron listadas exitosamente", miArray);
    }
    
}
