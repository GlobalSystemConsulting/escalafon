/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.operacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci.OperacionDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.OperacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaOperacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Operacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ActualizarOperacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Operacion actualizarOperacion = null;
     
             
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();           
            int operacionID = requestData.getInt("operacionID");           
            String descripcion = requestData.getString("descripcion");                      
            String tipo = requestData.getString("tipo");
            JSONArray listaCuentas = requestData.getJSONArray("listaCuentas");                    
            
            actualizarOperacion = new Operacion((short)operacionID, descripcion, tipo.charAt(0),new Date(), wr.getIdUsuario(), 'A');                                    
          
             
        
            //leendo las cuentas asociadas
            if(listaCuentas.length() > 0){
                
                for(int i = 0; i < listaCuentas.length();i++){
                    Object o = listaCuentas.get(i);
                    JSONObject bo = (JSONObject)o;

                    int cuentaContableID = bo.getInt("cuentaContableID");                     
                    boolean naturaleza = bo.getBoolean("naturaleza");
                     String numero = bo.getString("numero");
                    String nombre = bo.getString("nombre");

                    
                   
                    actualizarOperacion.getCuentaOperaciones().add(new CuentaOperacion( new CuentaContable(cuentaContableID,numero,nombre), actualizarOperacion, naturaleza));
                }
            }
            
          
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *   Parte de Logica de Negocio
        *   descripcion: El Sitema debe generar el codigo para el nuevo Tipo de Tramite antes de insertar a la BD
        */
       OperacionDao operacionDao = new OperacionDaoHibernate();
      
        
       
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            operacionDao.eliminarCuentas(actualizarOperacion.getOpeId());
            operacionDao.update(actualizarOperacion);        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo actualizar la operacion ", e.getMessage() );
        }
       
        
        
        return WebResponse.crearWebResponseExito("El registro de la Operacion se actualizo correctamente");
        //Fin
    }    
  
    
}
