/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.std;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.RequisitoTramite;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.RutaTramite;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoTramite;
import java.util.List;

/**
 *
 * @author abel
 */
public interface TipoTramiteDao extends GenericDao<TipoTramite>{
    
    public TipoTramite buscarPorID(int clave);
    public String buscarUltimoCodigo();
    public List<TipoTramite> buscarConTipoOrganizacion();
    public List<TipoTramite> buscarPorTipoOrganizacion(int tipoOrganizacionID);
    public List<RequisitoTramite> buscarRequisitos(TipoTramite tipoTramite);
    public void eliminarRequisitos(TipoTramite tipoTramite);
    public List<RutaTramite> buscarRutas(TipoTramite tipoTramite);
    public void eliminarRutas(TipoTramite tipoTramite);
}
