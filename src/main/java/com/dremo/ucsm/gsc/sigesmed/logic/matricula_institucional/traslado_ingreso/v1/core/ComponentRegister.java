package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_ingreso.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_ingreso.v1.tx.BuscarEstudianteTrasladoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_ingreso.v1.tx.GenerarTrasladoIngresoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_ingreso.v1.tx.ListarTrasladosIngresosEmitidosTx;

public class ComponentRegister implements IComponentRegister {

    @Override

    public WebComponent createComponent() {
        WebComponent maComponent = new WebComponent(Sigesmed.SUBMODULO_MATRICULA_INSTITUCIONAL);
        maComponent.setName("trasladoIngreso");
        maComponent.setVersion(1);
        maComponent.addTransactionGET("buscarEstudianteTraslado", BuscarEstudianteTrasladoTx.class);
        maComponent.addTransactionPOST("generarTrasladoIngreso", GenerarTrasladoIngresoTx.class);
        maComponent.addTransactionGET("listarTrasladosIngresosEmitidos", ListarTrasladosIngresosEmitidosTx.class);

        return maComponent;
    }
}
