package com.dremo.ucsm.gsc.sigesmed.logic.scec.cargo_comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 29/09/16.
 */
public class ActualizarCargoTx  implements ITransaction {
    private static final Logger logger = Logger.getLogger(ActualizarCargoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject cargo = (JSONObject) wr.getData();

        int id = cargo.getInt("cod");
        String nom = cargo.optString("nom","");
        String des = cargo.optString("des","");

        return actualizar(id,nom,des);
    }
    public WebResponse actualizar(int idCargo,String nombre, String descripcion){
        CargoComisionDao cargoComisionDao = (CargoComisionDao) FactoryDao.buildDao("scec.CargoComisionDao");
        try {
            CargoComision cargoComision = cargoComisionDao.buscarPorId(idCargo);
            cargoComision.setNomCar(nombre);
            cargoComision.setDesCar(descripcion);
            cargoComision.setFecMod(new Date());
            cargoComisionDao.update(cargoComision);

            return WebResponse.crearWebResponseExito("La actualizacion se realizo con exito", WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizar",e);
            return WebResponse.crearWebResponseError("No se pudo realizar la transaccion", WebResponse.BAD_RESPONSE);
        }

    }
}
