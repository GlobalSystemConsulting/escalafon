/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.demeritos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarDemeritoTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ActualizarDemeritoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {

            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");

            JSONObject requestData = (JSONObject) wr.getData();

            Integer demId = requestData.getInt("demId");
            String entEmi = requestData.getString("entEmi");
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = requestData.getString("fecDoc").equals("")?null:sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            Boolean sep = requestData.getBoolean("sep");
            Date fecIni = requestData.getString("fecIni").equals("")?null:sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecFin = requestData.getString("fecFin").equals("")?null:sdi.parse(requestData.getString("fecFin").substring(0, 10));
            String mot = requestData.getString("mot");
            Integer tipdocId = requestData.getInt("tipDocId");

            return actualizarDemerito(demId, entEmi, numDoc, fecDoc, tipdocId, sep, fecIni, fecFin, mot);
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE, "Actualizar demerito", e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        }
    }

    private WebResponse actualizarDemerito(Integer demId, String entEmi, String  numDoc, Date fecDoc, Integer tipDocId,
            Boolean sep, Date fecIni, Date fecFin, String mot) {
        try {
            DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
            Demerito demerito = demeritoDao.buscarPorId(demId);

            demerito.setEntEmi(entEmi);
            demerito.setNumDoc(numDoc);
            demerito.setFecDoc(fecDoc);
            demerito.setSep(sep);
            demerito.setFecIni(fecIni);
            demerito.setFecFin(fecFin);
            demerito.setMot(mot);
            demerito.setTipDocId(tipDocId);

            demeritoDao.update(demerito);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("demId", demerito.getDemId());
            oResponse.put("entEmi", demerito.getEntEmi()==null?"":demerito.getEntEmi());
            oResponse.put("numDoc", demerito.getNumDoc()==null?"":demerito.getNumDoc());
            oResponse.put("fecDoc", demerito.getFecDoc()==null?"":sdo.format(demerito.getFecDoc()));
            oResponse.put("tipDocId", demerito.getTipDocId()==null?0:demerito.getTipDocId());
            oResponse.put("sep", demerito.getSep()==null?false:demerito.getSep());
            oResponse.put("sepDes", "");
            oResponse.put("fecIni", demerito.getFecIni()==null?"":sdo.format(demerito.getFecIni()));
            oResponse.put("fecFin", demerito.getFecFin()==null?"":sdo.format(demerito.getFecFin()));
            oResponse.put("mot", demerito.getMot()==null?"":demerito.getMot());
            return WebResponse.crearWebResponseExito("Demerito actualizado exitosamente", oResponse);

        } catch (Exception e) {
            logger.log(Level.SEVERE, "actualizarDemerito", e);
            return WebResponse.crearWebResponseError("Error, el demerito no fue actualizado");
        }
    }

}
