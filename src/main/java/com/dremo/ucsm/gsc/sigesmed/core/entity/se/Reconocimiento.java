/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "reconocimiento", schema="administrativo")
public class Reconocimiento implements Serializable {
    
    @Id
    @Column(name = "rec_id")
    @SequenceGenerator(name = "secuencia_reconocimiento", sequenceName="administrativo.reconocimiento_rec_id_seq" )
    @GeneratedValue(generator="secuencia_reconocimiento")
    private Integer recId;
    
    @Column(name = "num_doc", length=20)
    private String numDoc;
    
    @Column(name = "fec_doc")
    @Temporal(TemporalType.DATE)
    private Date fecDoc;
    
    @Column(name = "tip_mot")
    private Character mot;
    
    @Column(name = "des_mot")
    private String desMot;
    
    @Column(name = "ent_emi")
    private String entEmi;
    
    @Column(name = "tip_doc")
    private String tipDoc;
    
    @Column(name = "dep")
    private String dep;
        
    @Column(name = "car")
    private String car;
    
    @Column(name = "tip_bon")
    private String tipBon;
    
    @Column(name = "fec_efe")
    @Temporal(TemporalType.DATE)
    private Date fecEfe;
    
    @Column(name = "por_mon")
    private Integer porMon; 
    
    @Column(name = "tip_doc_id")
    private Integer tipDocId;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "per_id")
    private Persona persona;

    public Reconocimiento() {
    }

    public Reconocimiento(Integer recId) {
        this.recId = recId;
    }
    
    public Reconocimiento(Persona persona, Character mot, String desMot, String numDoc, Date fecDoc, Integer tipDocId, String entEmi,Integer usuMod, Date fecMod, Character estReg) {
        this.persona = persona;
        this.mot = mot;
        this.desMot = desMot;
        this.numDoc = numDoc;
        this.fecDoc = fecDoc;
        this.entEmi = entEmi;
        this.tipDocId=tipDocId;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public Integer getRecId() {
        return recId;
    }

    public void setRecId(Integer recId) {
        this.recId = recId;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public Date getFecDoc() {
        return fecDoc;
    }

    public void setFecDoc(Date fecDoc) {
        this.fecDoc = fecDoc;
    }

    public Character getMot() {
        return mot;
    }

    public void setMot(Character mot) {
        this.mot = mot;
    }
    
    public String getDesMot() {
        return desMot;
    }

    public void setDesMot(String desMot) {
        this.desMot = desMot;
    }

    public String getEntEmi() {
        return entEmi;
    }

    public void setEntEmi(String entEmi) {
        this.entEmi = entEmi;
    }

    public String getTipDoc() {
        return tipDoc;
    }

    public void setTipDoc(String tipDoc) {
        this.tipDoc = tipDoc;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getTipBon() {
        return tipBon;
    }

    public void setTipBon(String tipBon) {
        this.tipBon = tipBon;
    }

    public Date getFecEfe() {
        return fecEfe;
    }

    public void setFecEfe(Date fecEfe) {
        this.fecEfe = fecEfe;
    }

    public Integer getPorMon() {
        return porMon;
    }

    public void setPorMon(Integer porMon) {
        this.porMon = porMon;
    }
    
    public Integer getTipDocId() {
        return tipDocId;
    }

    public void setTipDocId(Integer tipDocId) {
        this.tipDocId = tipDocId;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recId != null ? recId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reconocimiento)) {
            return false;
        }
        Reconocimiento other = (Reconocimiento) object;
        if ((this.recId == null && other.recId != null) || (this.recId != null && !this.recId.equals(other.recId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Reconocimiento{" + "recId=" + recId + ", numDoc=" + numDoc + ", fecDoc=" + fecDoc + ", mot=" + mot + ", desMot=" + desMot + ", entEmi=" + entEmi + ", tipDoc=" + tipDoc + ", dep=" + dep + ", car=" + car + ", tipBon=" + tipBon + ", fecEfe=" + fecEfe + ", porMon=" + porMon + ", tipDocId=" + tipDocId + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + '}';
    }

    
}
