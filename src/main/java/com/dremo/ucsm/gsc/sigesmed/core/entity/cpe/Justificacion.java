package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="justificacion" ,schema="administrativo")
public class Justificacion  implements java.io.Serializable {


    @Id
    @Column(name="jus_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_justificacion", sequenceName="administrativo.justificacion_jus_id_seq" )
    @GeneratedValue(generator="secuencia_justificacion")
    private Integer jusId;
    
    @Column(name="jus_doc")
    private String jusDoc;
    
    @Column(name="jus_obs")
    private String jusObs;
    
    @Column(name="est_reg")
    private String estReg;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_reg")
    private Date fecReg;
    
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="reg_asi_id")
    private RegistroAsistencia jusRegAsiId;

    public Justificacion() {
    }

    public Justificacion(Integer jusId) {
        this.jusId = jusId;
    }

    
    public Justificacion(String jusDoc, String jusObs, String estReg, RegistroAsistencia jusRegAsiId,Date fecReg) {
        this.jusDoc = jusDoc;
        this.jusObs = jusObs;
        this.estReg = estReg;
        this.jusRegAsiId = jusRegAsiId;
        this.fecReg=fecReg;
    }

    public Integer getJusId() {
        return jusId;
    }

    public void setJusId(Integer jusId) {
        this.jusId = jusId;
    }

    public String getJusDoc() {
        return jusDoc;
    }

    public void setJusDoc(String jusDoc) {
        this.jusDoc = jusDoc;
    }

    public String getJusObs() {
        return jusObs;
    }

    public void setJusObs(String jusObs) {
        this.jusObs = jusObs;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public RegistroAsistencia getJusRegAsiId() {
        return jusRegAsiId;
    }

    public void setJusRegAsiId(RegistroAsistencia jusRegAsiId) {
        this.jusRegAsiId = jusRegAsiId;
    }

    public Date getFecReg() {
        return fecReg;
    }

    public void setFecReg(Date fecReg) {
        this.fecReg = fecReg;
    }
    
    
}


