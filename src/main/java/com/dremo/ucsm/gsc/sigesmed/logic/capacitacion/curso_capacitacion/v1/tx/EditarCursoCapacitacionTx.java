package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.OrganizacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CronogramaSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;

public class EditarCursoCapacitacionTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(EditarCursoCapacitacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject cursoObj = (JSONObject) wr.getData();
            CursoCapacitacionDao cursoCapacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            CursoCapacitacion curso = cursoCapacitacionDao.buscarPorId(cursoObj.getInt("idCap"));

            curso.setNom(cursoObj.optString("nom", curso.getNom()));

            OrganizacionCapacitacionDao organizacionDao = (OrganizacionCapacitacionDao) FactoryDao.buildDao("capacitacion.OrganizacionCapacitacionDao");
            Organizacion organizacion = organizacionDao.buscarPorId(cursoObj.getInt("organizacion"));
            Organizacion organizacionAut = organizacionDao.buscarPorId(cursoObj.getInt("organizacionAut"));

            curso.setOrganizacion(organizacion);
            curso.setOrganizacionAut(organizacionAut);
            curso.setUsuMod(cursoObj.getInt("usuMod"));
            curso.setFecMod(new Date());

            cursoCapacitacionDao.update(curso);

            JSONArray sedes = cursoObj.getJSONArray("sedes");
            SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
            
            for (int i = 0; i < sedes.length(); i++) {                
                SedeCapacitacion sede = sedeCapacitacionDao.buscarConCronogramaHorario(sedes.getJSONObject(i).getInt("cod"));

                sede.setLoc(sedes.getJSONObject(i).getString("loc"));
                sede.setDir(sedes.getJSONObject(i).getString("dir"));
                sede.setRef(sedes.getJSONObject(i).getString("ref"));

                CronogramaSedeCapacitacion cronograma = sede.getCronograma();
                cronograma.setDniRes(sedes.getJSONObject(i).getJSONObject("cronograma").getString("dniRes"));
                cronograma.setNomRes(sedes.getJSONObject(i).getJSONObject("cronograma").getString("nomRes"));

                if (sedes.getJSONObject(i).getJSONObject("horario").length() > 0) {
                    SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                    Date tur2Ini = (sedes.getJSONObject(i).getJSONObject("horario").optString("turno2horaIni").isEmpty()) ? null : format.parse(sedes.getJSONObject(i).getJSONObject("horario").optString("turno2horaIni"));
                    Date tur2Fin = (sedes.getJSONObject(i).getJSONObject("horario").optString("turno2horaFin").isEmpty()) ? null : format.parse(sedes.getJSONObject(i).getJSONObject("horario").optString("turno2horaFin"));

                    /*if (sede.getHorarioSede() == null) {
                        HorarioSedeCapacitacion horario = new HorarioSedeCapacitacion(sedes.getJSONObject(i).getJSONObject("horario").getInt("dia"),
                                format.parse(sedes.getJSONObject(i).getJSONObject("horario").getString("turno1horaIni")),
                                format.parse(sedes.getJSONObject(i).getJSONObject("horario").getString("turno1horaFin")),
                                tur2Ini,
                                tur2Fin);

                        horario.setSedeCapacitacion(sede);
                        sede.setHorarioSede(horario);
                    } else {
                        HorarioSedeCapacitacion horario = sede.getHorarioSede();
                        horario.setDia(sedes.getJSONObject(i).getJSONObject("horario").getInt("dia"));
                        horario.setTur1Ini(format.parse(sedes.getJSONObject(i).getJSONObject("horario").getString("turno1horaIni")));
                        horario.setTur1Fin(format.parse(sedes.getJSONObject(i).getJSONObject("horario").getString("turno1horaFin")));
                        horario.setTur2Ini(tur2Ini);
                        horario.setTur2Fin(tur2Fin);
                    }*/
                }
                sedeCapacitacionDao.update(sede);
            }

            return WebResponse.crearWebResponseExito("Exito al editar el curso", WebResponse.OK_RESPONSE);
        } catch (JSONException | ParseException e) {
            logger.log(Level.SEVERE, "editarCursoCapacitacion", e);
            return WebResponse.crearWebResponseError("Error al editar el curso", WebResponse.BAD_RESPONSE);
        }
    }
}
