/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.formacion_educativa.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.formacion_educativa.v1.tx.*;
/**
 *
 * @author Yemi
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("formacion_educativa");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarFormacionesEducativas", ListarFormacionesEducativasTx.class);
	seComponent.addTransactionPOST("agregarFormacionEducativa", AgregarFormacionEducativaTx.class);
        seComponent.addTransactionPUT("actualizarFormacionEducativa", ActualizarFormacionEducativaTx.class);
        seComponent.addTransactionDELETE("eliminarFormacionEducativa", EliminarFormacionEducativaTx.class);
        
        return seComponent;
    }
    
}
