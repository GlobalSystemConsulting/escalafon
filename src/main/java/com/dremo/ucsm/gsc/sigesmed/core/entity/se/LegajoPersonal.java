/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "legajo_personal", schema="administrativo")

public class LegajoPersonal implements Serializable {

    @Id
    @Column(name = "leg_per_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_legajo_personal", sequenceName="administrativo.legajo_personal_leg_per_id_seq" )
    @GeneratedValue(generator="secuencia_legajo_personal")
    private Integer legPerId;
    
    @Column(name = "nom")
    private String nom;
    
    @Column(name = "fec_ing")
    @Temporal(TemporalType.DATE)
    private Date fecIng;

    @Column(name = "url")
    private String url;
    
    @Column(name = "des")
    private String des;
    
    @Column(name = "cat_leg")
    private Character catLeg;
    
    @Column(name = "sub_cat")
    private Character subCat;
    
    @Column(name = "cod_asp_ori")
    private Integer codAspOri;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria;

    public LegajoPersonal() {
    }

    public LegajoPersonal(Integer legPerId) {
        this.legPerId = legPerId;
    }
    
    public LegajoPersonal(FichaEscalafonaria fichaEscalafonaria, String nom, Date fecIng, String url, Character catLeg) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.nom = nom;
        this.fecIng = fecIng;
        this.url = url;
        this.catLeg = catLeg;
    }
    
    public LegajoPersonal(FichaEscalafonaria fichaEscalafonaria, String nom, Date fecIng, String url, Character catLeg, Character subCat) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.nom = nom;
        this.fecIng = fecIng;
        this.url = url;
        this.catLeg = catLeg;
        this.subCat = subCat;
    }
    
    public LegajoPersonal(FichaEscalafonaria fichaEscalafonaria, String nom, Date fecIng, String url, String des, Character catLeg, Character subCat , Integer codAspOri) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.nom = nom;
        this.fecIng = fecIng;
        this.url = url;
        this.des = des;
        this.catLeg = catLeg;
        this.subCat = subCat;
        this.codAspOri = codAspOri;
    }
    
    public LegajoPersonal(Character catLeg, Character subCat , Integer codAspOri) {
        this.catLeg = catLeg;
        this.subCat = subCat;
        this.codAspOri = codAspOri;
    }

    public Integer getLegPerId() {
        return legPerId;
    }

    public void setLegPerId(Integer legPerId) {
        this.legPerId = legPerId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getFecIng() {
        return fecIng;
    }

    public void setFecIng(Date fecIng) {
        this.fecIng = fecIng;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Character getCatLeg() {
        return catLeg;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
    
    public void setCatLeg(Character catLeg) {
        this.catLeg = catLeg;
    }
    
    public Character getSubCat() {
        return subCat;
    }

    public void setSubCat(Character subCat) {
        this.subCat = subCat;
    }

    public Integer getCodAspOri() {
        return codAspOri;
    }

    public void setCodAspOri(Integer codAspOri) {
        this.codAspOri = codAspOri;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setFichaEscalafonaria(FichaEscalafonaria fichaEscalafonaria) {
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (legPerId != null ? legPerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LegajoPersonal)) {
            return false;
        }
        LegajoPersonal other = (LegajoPersonal) object;
        if ((this.legPerId == null && other.legPerId != null) || (this.legPerId != null && !this.legPerId.equals(other.legPerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LegajoPersonal{" + "legPerId=" + legPerId + ", nom=" + nom + ", fecIng=" + fecIng + ", url=" + url + ", catLeg=" + catLeg + ", fichaEscalafonaria=" + fichaEscalafonaria + '}';
    }


    
}
