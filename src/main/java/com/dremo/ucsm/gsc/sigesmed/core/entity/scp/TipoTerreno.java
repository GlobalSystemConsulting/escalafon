/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;



/**
 *
 * @author Administrador
 */
@Entity
@Table(name="tipo_terreno", schema="administrativo")
public class TipoTerreno {
    
    @Id
    @Column(name="tip_terr", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.tipo_terreno_tip_terr_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int tip_terr;
    
    @Column(name="tip_terr_nom")
    private String tip_terr_nom;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="est_reg")
    private char est_reg;

    public TipoTerreno() {
    }

    public TipoTerreno(int tip_terr, String tip_terr_nom, Date fec_mod, char est_reg) {
        this.tip_terr = tip_terr;
        this.tip_terr_nom = tip_terr_nom;
        this.fec_mod = fec_mod;
        this.est_reg = est_reg;
    }

    public void setTip_terr(int tip_terr) {
        this.tip_terr = tip_terr;
    }

    public void setTip_terr_nom(String tip_terr_nom) {
        this.tip_terr_nom = tip_terr_nom;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getTip_terr() {
        return tip_terr;
    }

    public String getTip_terr_nom() {
        return tip_terr_nom;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }
    
    
}
