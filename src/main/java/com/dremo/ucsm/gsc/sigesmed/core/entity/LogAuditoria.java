/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name="log_auditoria" ,schema="public" )
public class LogAuditoria implements java.io.Serializable {
    @Id
    @Column(name="log_aud_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_log_auditoria", sequenceName="public.log_auditoria_log_aud_id_seq" )
    @GeneratedValue(generator="secuencia_log_auditoria")
    private int logAudId;
    @Column(name="tip_ope")
    private Character tipOpe;
    @Column(name="obs_ope")
    private String obsOpe;
    @Column(name="con_ope")
    private String conOpe;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_emi")
    private Date fecEmi;
    @Column(name="usu_id")
    private int usuId;
    @Column(name="usu_emi")
    private String usuEmi;
    @Column(name="ip_acc ")
    private String ipAcc;

    public LogAuditoria(Character tipOpe, String obsOpe, String conOpe, Date fecEmi, int usuId, String usuEmi, String ipAcc) {
        this.tipOpe = tipOpe;
        this.obsOpe = obsOpe;
        this.conOpe = conOpe;
        this.fecEmi = fecEmi;
        this.usuId = usuId;
        this.usuEmi = usuEmi;
        this.ipAcc = ipAcc;
    }

    public LogAuditoria() {
    }

    public int getLogAudId() {
        return logAudId;
    }

    public void setLogAudId(int logAudId) {
        this.logAudId = logAudId;
    }

    public Character getTipOpe() {
        return tipOpe;
    }

    public void setTipOpe(Character tipOpe) {
        this.tipOpe = tipOpe;
    }

    public String getObsOpe() {
        return obsOpe;
    }

    public void setObsOpe(String obsOpe) {
        this.obsOpe = obsOpe;
    }

    public String getConOpe() {
        return conOpe;
    }

    public void setConOpe(String conOpe) {
        this.conOpe = conOpe;
    }

    public Date getFecEmi() {
        return fecEmi;
    }

    public void setFecEmi(Date fecEmi) {
        this.fecEmi = fecEmi;
    }

    public int getUsuId() {
        return usuId;
    }

    public void setUsuId(int usuId) {
        this.usuId = usuId;
    }

    public String getUsuEmi() {
        return usuEmi;
    }

    public void setUsuEmi(String usuEmi) {
        this.usuEmi = usuEmi;
    }

    public String getIpAcc() {
        return ipAcc;
    }

    public void setIpAcc(String ipAcc) {
        this.ipAcc = ipAcc;
    }
    
}
