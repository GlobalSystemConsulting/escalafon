/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Ambientes;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AmbientesDAO;


import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.text.SimpleDateFormat;


/**
 *
 * @author Administrador
 */



public class RegistrarAmbientesTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
         JSONObject requestData = (JSONObject)wr.getData();
         Ambientes amb = null;
         
         try{
             SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
             
             String descripcion = requestData.getString("descripcion");
             String ubicacion = requestData.getString("ubicacion");
             char estado = requestData.getString("estado").charAt(0);
             int area = requestData.getInt("area");
             String inst_cons = requestData.getString("inst_cons");
             Date fec_cons = formatter.parse(requestData.getString("fec_cons"));
             int user = requestData.getInt("user");
             int org_id = requestData.getInt("orgid");
             char est_reg = 'A';
             
             amb = new Ambientes(0,descripcion,ubicacion,estado,area,inst_cons,fec_cons,org_id,user,est_reg);
             
             AmbientesDAO amb_dao = (AmbientesDAO)FactoryDao.buildDao("scp.AmbientesDAO");
             amb_dao.insert(amb);

         }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Ambiente , datos incorrectos", e.getMessage() );
         }
          JSONObject oResponse = new JSONObject();
          oResponse.put("cod_bie",amb.getAmb_id());
          return WebResponse.crearWebResponseExito("El registro del Ambiente se realizo correctamente",oResponse);
 
      }
    
    
    
    
}
