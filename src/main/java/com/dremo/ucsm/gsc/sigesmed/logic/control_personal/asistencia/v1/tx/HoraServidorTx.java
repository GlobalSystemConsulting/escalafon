/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;


/**
 *
 * @author carlos
 */
public class HoraServidorTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject fecha=new JSONObject();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String s = formatter.format(new Date());
        fecha.put("fecha", s);
        return WebResponse.crearWebResponseExito("Se Asigno correctamente",fecha);        
        //Fin
    }
    
}

