/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseño_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DiseñoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DiseñoCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.ModalidadEducacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
/**
 *
 * @author abel
 */
public class InsertarDiseñoCurricularTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        DiseñoCurricular nuevo = null;
        
        ModalidadEducacion modalidad = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int organizacionID = requestData.getInt("organizacionID");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String resolucion = requestData.getString("resolucion");
            String tipo = requestData.getString("tipo");
            String estado = requestData.getString("estado");
            nuevo = new DiseñoCurricular(0, nombre, descripcion,resolucion,tipo.charAt(0), organizacionID, new Date(), new Date(), wr.getIdUsuario(), estado.charAt(0));
            
            modalidad = new ModalidadEducacion(0,"EBR", "Educacion Basica Regular", "Educacion Basica Regular",0, new Date(), wr.getIdUsuario(), 'A');
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el diseño curricular, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        DiseñoCurricularDao diseñoDao = (DiseñoCurricularDao)FactoryDao.buildDao("mech.DiseñoCurricularDao");
        try{
            diseñoDao.insert(nuevo);
            modalidad.setDisCurId(nuevo.getDisCurId());
            diseñoDao.insertarModalidad(modalidad);
        }catch(Exception e){
            System.out.println("No se pudo registrar el diseño curricular\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el diseño curricular", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("diseñoID",nuevo.getDisCurId());
        oResponse.put("fecha",nuevo.getFecCre().toString());
        
        
        JSONObject oMod = new JSONObject();
        oMod.put("modalidadID", modalidad.getModEduId() );
        oMod.put("abreviacion", modalidad.getAbr() );
        oMod.put("nombre", modalidad.getNom() );
        oMod.put("descripcion", modalidad.getDes() );

        oResponse.put("modalidad", oMod);
        
        return WebResponse.crearWebResponseExito("El registro del Diseño Curricular se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
