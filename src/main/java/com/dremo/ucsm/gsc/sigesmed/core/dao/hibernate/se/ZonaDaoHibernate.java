/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ZonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Zona;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Felipe
 */
public class ZonaDaoHibernate extends GenericDaoHibernate<Zona> implements ZonaDao{

    @Override
    public List<Zona> listarxZona() {
        List<Zona> zonas = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT z from Zona as z "
                    + "WHERE z.estReg='A'";
            Query query = session.createQuery(hql);
            zonas = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los zonas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las zonas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return zonas;
    }
    @Override
    public Zona buscarPorId(Integer zonId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Zona item = (Zona)session.get(Zona.class, zonId);
        session.close();
        return item;
    }
}
