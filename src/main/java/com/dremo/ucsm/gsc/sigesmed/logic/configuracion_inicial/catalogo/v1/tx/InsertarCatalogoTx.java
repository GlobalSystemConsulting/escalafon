/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.CatalogoTablaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.CatalogoTabla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;

/**
 *
 * @author Administrador
 */
public class InsertarCatalogoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        CatalogoTabla catalogoTabla = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
    
            String nombretabla = requestData.getString("nombretabla");
            String nombreclase = requestData.getString("nombreclase");
            String estado = requestData.getString("estado");
            
            catalogoTabla = new CatalogoTabla(0, nombretabla, nombreclase, new Date(),wr.getIdUsuario(), estado.charAt(0));
            
            
           
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        CatalogoTablaDao catalogoTablaDao = (CatalogoTablaDao)FactoryDao.buildDao("CatalogoTablaDao");
        try{
            catalogoTablaDao.insert(catalogoTabla);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar el Catalogo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Catalogo", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("catalogoID",catalogoTabla.getCatId());
        oResponse.put("fecha",catalogoTabla.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro del Catalogo se realizo correctamente", oResponse);
        //Fin
    }
    
}

