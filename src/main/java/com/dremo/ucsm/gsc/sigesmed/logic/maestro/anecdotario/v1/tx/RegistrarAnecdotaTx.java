package com.dremo.ucsm.gsc.sigesmed.logic.maestro.anecdotario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario.AnecdotarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario.AccionesCorrectivas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.anecdotario.Anecdotario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 21/12/2016.
 */
public class RegistrarAnecdotaTx implements ITransaction {
    private static Logger logger = Logger.getLogger(RegistrarAnecdotaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idOrg = data.getInt("org");
        int idDoc = data.getInt("doc");
        int idEst = data.getInt("est");
        int idGra = data.getInt("gra");

        String sec = data.getString("sec");

        long fec = data.getLong("fec");
        String des = data.getString("des");
        String der = data.getString("der");

        JSONArray acciones = data.optJSONArray("acciones");

        return registrarAnecdota(idOrg, idDoc, idEst, idGra, sec, fec, des, der, acciones);
    }

    private WebResponse registrarAnecdota(int idOrg, int idDoc, int idEst, int idGra, String sec, long fec, String des, String der, JSONArray accionesJSON) {
        try{
            AnecdotarioDao anecDao = (AnecdotarioDao) FactoryDao.buildDao("maestro.anecdotario.AnecdotarioDao");
            DocenteDao docenteDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            OrganizacionDao organizacionDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");

            Docente docente = docenteDao.buscarDocentePorId(idDoc);
            Grado grado = anecDao.buscarGrado(idGra);
            Organizacion organizacion = organizacionDao.buscarConTipoOrganizacionYPadre(idOrg);
            Estudiante estudiante = anecDao.buscarEstudiante(idEst);
            Anecdotario anecdota = new Anecdotario(new Date(fec),des,der);
            anecdota.setDocente(docente);
            anecdota.setGrado(grado);
            anecdota.setSec(sec);
            anecdota.setOrg(organizacion);
            anecdota.setEstudiante(estudiante);
            anecDao.insert(anecdota);
            if(accionesJSON != null){
                for(int i = 0 ; i < accionesJSON.length(); i++){
                    JSONObject jsonAccion = accionesJSON.getJSONObject(i);
                    String nomAccion = jsonAccion.getString("nom");
                    AccionesCorrectivas accion = new AccionesCorrectivas(nomAccion);
                    accion.setAnecdotario(anecdota);
                    anecdota.getAccionesCorrectivas().add(accion);
                }
                anecDao.update(anecdota);
            }

            return WebResponse.crearWebResponseExito("Se registro la anecdota correctamente anecdota",new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"aneId"},new String[]{"id"},anecdota
            )));
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarAnecdota",e);
            return WebResponse.crearWebResponseError("error al registrar la anecdota");
        }
    }
}