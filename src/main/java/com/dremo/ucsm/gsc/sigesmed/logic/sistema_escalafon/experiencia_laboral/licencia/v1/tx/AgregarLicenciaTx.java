/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.licencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LicenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Licencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class AgregarLicenciaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(AgregarLicenciaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        Licencia licencia = null;
        
        Integer ficEscId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            System.out.println("newLic: "+requestData.toString());
            ficEscId = requestData.getInt("ficEscId");
            String entEmi = "";
            if(requestData.getString("entEmi").length() > 0){
                entEmi = requestData.getString("entEmi");
            }
            int tipDocIdMain =0;
            if(requestData.getInt("tipDocIdMain")!=0){
                tipDocIdMain = requestData.getInt("tipDocIdMain");
            }
            String numInfMain = "";
            if(requestData.getString("numInfMain").length() > 0){
                numInfMain = requestData.getString("numInfMain");
            }
            Date fecInfMain = null;
            if(requestData.getString("fecInfMain").length() > 0){
                fecInfMain = sdi.parse(requestData.getString("fecInfMain").substring(0, 10));
            }
            Date fecRecInfMain = null;
            if(requestData.getString("fecRecInfMain").length() > 0){
                fecRecInfMain = sdi.parse(requestData.getString("fecRecInfMain").substring(0, 10));
            }
            int tipLicId = 0;
            if(requestData.getInt("tipLicId")!=0){
                tipLicId = requestData.getInt("tipLicId");
            }
            Boolean estGoc = requestData.getBoolean("estGoc");
            Date fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = sdi.parse(requestData.getString("fecTer").substring(0, 10));            
        
            String mot = "";
            if(requestData.getString("mot").length() > 0){
                mot = requestData.getString("mot");
            }
            int estProId =0;
            if(requestData.getInt("estProId")!=0){
                estProId = requestData.getInt("estProId");
            }
            //DATOS DE INFORME
            int tipDocIdSec =0;
            if(requestData.getInt("tipDocIdSec")!=0){
                tipDocIdSec = requestData.getInt("tipDocIdSec");
            }
            String numDocSec = requestData.getString("numDocSec");
            if(requestData.getString("numDocSec").length() > 0){
                numDocSec = requestData.getString("numDocSec");
            }
            Date fecDocSec = null;
            if(requestData.getString("fecDocSec").length() > 0){
                fecDocSec = sdi.parse(requestData.getString("fecDocSec").substring(0, 10));
            }
            
            //Datos de Reincorporacion
            
            int tipDocIdMemo =0;
            if(requestData.getInt("tipDocIdMemo")!=0){
                tipDocIdMemo = requestData.getInt("tipDocIdMemo");
            }
            String numInfMemo = "";
            if(requestData.getString("numInfMemo").length() > 0){
                numInfMemo = requestData.getString("numInfMemo");
            }
            
            Date fecInfMemo =null;
            if(requestData.getString("fecInfMemo").length() > 0){
                fecInfMemo = sdi.parse(requestData.getString("fecInfMemo").substring(0, 10));
            }
            Date fecRecInMemo =null;
            if(requestData.getString("fecRecInMemo").length() > 0){
                fecRecInMemo = sdi.parse(requestData.getString("fecRecInMemo").substring(0, 10));
            }
            
            
            int orgiId = requestData.getInt("orgiId");
            int carId = requestData.getInt("carId");
            int catId = requestData.getInt("catId");
            
            int anioPro = requestData.getInt("anioPro");
            int mesPro = requestData.getInt("mesPro");
            int diaPro = requestData.getInt("diaPro");
            
            licencia = new Licencia(entEmi, numInfMain, fecInfMain, estGoc, fecIni, fecTer, mot, wr.getIdUsuario(), new Date(),
                    'A', new FichaEscalafonaria(ficEscId), numDocSec, fecDocSec, fecRecInfMain, tipDocIdMain, tipLicId, estProId,tipDocIdSec,
                    tipDocIdMemo,numInfMemo,fecInfMemo,fecRecInMemo);

            licencia.setOrgiActId(orgiId);
            licencia.setCarActId(carId);
            licencia.setCatActId(catId);
            
            licencia.setAnioPro(anioPro);
            licencia.setMesPro(mesPro);
            licencia.setDiaPro(diaPro);
            
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nueva licencia",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }

        LicenciaDao licenciaDao = (LicenciaDao) FactoryDao.buildDao("se.LicenciaDao");
        try {
            licenciaDao.insert(licencia);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva licencia",e);
            System.out.println(e);
        }

        JSONObject oResponse = new JSONObject();
        
        if(licencia.getLicId() != null)
            oResponse.put("licId", licencia.getLicId());
        else
            oResponse.put("licId", -1);
        
        if(licencia.getEntEmiRes()!=null)
            oResponse.put("entEmi", licencia.getEntEmiRes());
        else
            oResponse.put("entEmi", "");
        
        if(licencia.getTipDocId()!=null)
            oResponse.put("tipDocIdMain", licencia.getTipDocId());
        else
            oResponse.put("tipDocIdMain", 0);
        
        if(licencia.getNumDoc()!=null)
            oResponse.put("numInfMain", licencia.getNumDoc());
        else
            oResponse.put("numInfMain", "");
        
        if(licencia.getFecDoc()!=null)
            oResponse.put("fecInfMain", sdo.format(licencia.getFecDoc()));
        else
            oResponse.put("fecInfMain", "");
        
        if(licencia.getFecRecInf()!=null)
            oResponse.put("fecRecInfMain", sdo.format(licencia.getFecRecInf()));
        else
            oResponse.put("fecRecInfMain", "");
        
        if(licencia.getTipLicId()!=null)
            oResponse.put("tipLicId", licencia.getTipLicId());
        else
            oResponse.put("tipLicId", 0);
                
        if(licencia.getEstGoc()!=null)
            oResponse.put("estGoc", licencia.getEstGoc());
        else
            oResponse.put("estGoc", false);
        
        if(licencia.getFecIni()!=null)
            oResponse.put("fecIni", sdo.format(licencia.getFecIni()));
        else
            oResponse.put("fecIni", "");
        
        if(licencia.getFecTer()!=null)
            oResponse.put("fecTer", sdo.format(licencia.getFecTer()));
        else
            oResponse.put("fecTer", "");
        //datos de informe
        if(licencia.getTipDocInfId()!=null)
            oResponse.put("tipDocIdSec", licencia.getTipDocInfId());
        else
            oResponse.put("tipDocIdSec", 0);
        
        if(licencia.getNumInf()!=null)
            oResponse.put("numDocSec", licencia.getNumInf());
        else
            oResponse.put("numDocSec", "");
        
        if(licencia.getFecInf()!=null)
            oResponse.put("fecDocSec", sdo.format(licencia.getFecInf()));
        else
            oResponse.put("fecDocSec", "");
        
        //memo
        if(licencia.getTipDocMemId()!=null)
            oResponse.put("tipDocIdMemo", licencia.getTipDocMemId());
        else
            oResponse.put("tipDocIdMemo", 0);
        
        if(licencia.getNumMem()!=null)
            oResponse.put("numInfMemo", licencia.getNumMem());
        else
            oResponse.put("numInfMemo", "");
        
        if(licencia.getFecDocMem()!=null)
            oResponse.put("fecInfMemo", sdo.format(licencia.getFecDocMem()));
        else
            oResponse.put("fecInfMemo", "");
        
        if(licencia.getFecReiMem()!=null)
            oResponse.put("fecRecInMemo", sdo.format(licencia.getFecReiMem()));
        else
            oResponse.put("fecRecInMemo", "");
        
        
        //otros
        if(licencia.getMotLic()!=null)
            oResponse.put("mot", licencia.getMotLic());
        else
            oResponse.put("mot", "");
        
        if(licencia.getAnioPro()!=null)
            oResponse.put("anioPro", licencia.getAnioPro());
        else
            oResponse.put("anioPro", "");

        if(licencia.getMesPro()!=null)
            oResponse.put("mesPro", licencia.getMesPro());
        else
            oResponse.put("mesPro", "");

        if(licencia.getDiaPro()!=null)
            oResponse.put("diaPro", licencia.getDiaPro());
        else
            oResponse.put("diaPro", "");     
            
        oResponse.put("tip", licencia.getTip());
        
                        
        if(licencia.getEstProId()!=null)
            oResponse.put("estProId", licencia.getEstProId());
        else
            oResponse.put("estProId", 0);
        
        return WebResponse.crearWebResponseExito("El registro de la licencia se realizo correctamente", oResponse);
    }
    
}
