package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 27/12/2016.
 */
@Entity(name = "SeccionCarpetaMaestro")
@Table(name = "seccion_carpeta_pedagogica", schema = "pedagogico")
public class SeccionCarpetaPedagogica implements java.io.Serializable{
    @Id
    @Column(name = "sec_car_ped_id", unique=true, nullable=false)
    @SequenceGenerator(name = "seccion_carpeta_pedagogica_sec_car_ped_id_seq", sequenceName = "pedagogico.seccion_carpeta_pedagogica_sec_car_ped_id_seq")
    @GeneratedValue(generator = "seccion_carpeta_pedagogica_sec_car_ped_id_seq")
    private int secCarPedId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_dig_id")
    private CarpetaPedagogica carpeta;

    @OneToMany(mappedBy = "seccion",fetch = FetchType.LAZY)
    List<ContenidoSeccionCarpeta> contenidos;
    @Column(name = "ord")
    private Integer ord;
    @Column(name = "nom", nullable = false,length = 50)
    private String nom;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public SeccionCarpetaPedagogica() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public SeccionCarpetaPedagogica(String nom, Integer ord) {
        this.nom = nom;
        this.ord = ord;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getSecCarPedId() {
        return secCarPedId;
    }

    public void setSecCarPedId(int secCarPedId) {
        this.secCarPedId = secCarPedId;
    }

    public CarpetaPedagogica getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(CarpetaPedagogica carpeta) {
        this.carpeta = carpeta;
    }

    public Integer getOrd() {
        return ord;
    }

    public void setOrd(Integer ord) {
        this.ord = ord;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<ContenidoSeccionCarpeta> getContenidos() {
        return contenidos;
    }

    public void setContenidos(List<ContenidoSeccionCarpeta> contenidos) {
        this.contenidos = contenidos;
    }
}
