/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.licencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LicenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Licencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;


public class EliminarLicenciaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EliminarLicenciaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        Integer licId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            licId = requestData.getInt("licId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarLicencia",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        LicenciaDao licenciaDao = (LicenciaDao)FactoryDao.buildDao("se.LicenciaDao");
        try{
            licenciaDao.delete(new Licencia(licId));
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarBajaAuditoria("Se elimino licencia", "LICE_ID: "+licId , wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        }catch(Exception e){
            System.out.println("No se pudo eliminar la licencia\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la licencia", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La licencia se elimino correctamente");
    }
    
}
