package com.dremo.ucsm.gsc.sigesmed.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.Date;

@Entity(name="public.Empresa")
@Table(name="empresa")
public class Empresa  implements java.io.Serializable {

    @Id
    @Column(name="emp_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_empresa", sequenceName="administrativo.empresa_empresa_id_seq" )
    @GeneratedValue(generator="secuencia_empresa")
    private int empId;
    @Column(name="ruc",  nullable=false, length=11)
    private String ruc;
    @Column(name="raz_soc", nullable=false, length=100)
    private String razonSocial;
    @Column(name="num")
    private String num;
    @Column(name="web")
    private String web;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_reg")
    private Date fecReg;
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    public Empresa() {
    }

    public Empresa(int empId) {
        this.empId = empId;
    }

    public Empresa(String ruc, String razonSocial, String num, String web, Date fecReg, int usuMod,char estReg) {
        this.ruc = ruc;
        this.razonSocial = razonSocial;
        this.num = num;
        this.web = web;
        this.fecReg = fecReg;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public Date getFecReg() {
        return fecReg;
    }

    public void setFecReg(Date fecReg) {
        this.fecReg = fecReg;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

   
    
    
}
