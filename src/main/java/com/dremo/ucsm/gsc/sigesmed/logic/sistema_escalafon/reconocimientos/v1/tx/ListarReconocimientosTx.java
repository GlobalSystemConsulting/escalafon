/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reconocimientos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarReconocimientosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarReconocimientosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer perId = requestData.getInt("perId");
                
        List<Reconocimiento> reconocimientos = null;
        ReconocimientoDao reconocimientoDao = (ReconocimientoDao)FactoryDao.buildDao("se.ReconocimientoDao");
        
        try{
            reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(perId);
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar reconocimientos",e);
            System.out.println(">>TX-EXE: No se pudo listar los reconocimientos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los reconocimientos", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        JSONArray miArray = new JSONArray();
        for(Reconocimiento rec:reconocimientos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("recId", rec.getRecId());
            oResponse.put("mot", rec.getMot());
            oResponse.put("desMot", rec.getDesMot()==null?"":rec.getDesMot());
            oResponse.put("numDoc", rec.getNumDoc()==null?"":rec.getNumDoc());
            oResponse.put("fecDoc", rec.getFecDoc()==null?"":sdi.format(rec.getFecDoc()));
            oResponse.put("entEmi", rec.getEntEmi()==null?"":rec.getEntEmi());
            oResponse.put("tipDocId", rec.getTipDocId()==null?0:rec.getTipDocId());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los reconocimientos fueron listados exitosamente", miArray);
    }

}
