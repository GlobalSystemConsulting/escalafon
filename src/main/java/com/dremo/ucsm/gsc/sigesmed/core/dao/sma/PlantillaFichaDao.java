/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.PlantillaFicha;
import java.util.List;

/**
 *
 * @author Felipe
 */
public interface PlantillaFichaDao extends GenericDao<PlantillaFicha> {

    public String buscarUltimoCodigo();

    public List<Object[]> listarPlantillas();

    public PlantillaFicha obtenerPlantilla(int plaId);

    public PlantillaFicha getPlantilla(int plaId);
}
