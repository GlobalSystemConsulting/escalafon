/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.legajo_ridea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.comunication.pojo.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author GSC05
 */
public class ListarLegajosRideaTx implements ITransaction{
    
   
    static class Page {

        List<Documento> Documentos;

        public void deterPaginas (){
            for (Documento documento : Documentos) {
                documento.deterPagInicio();
            }
        }    
    }    
    
    @Override
    public WebResponse execute (WebRequest wr){
        
        ServicioWeb serv= new ServicioWeb();
        /*
        String dbName = "jdbc:postgresql://localhost/escalafon_bd";
        String dbDriver = "org.postgresql.Driver";
        String userName = "postgres";
        String password = "1234"; 
        String url="";
        
        try{
            try {
                Class.forName(dbDriver);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ListarLegajosRideaTx.class.getName()).log(Level.SEVERE, null, ex);
            }
            Connection con = DriverManager.getConnection(dbName, userName, password);
            Statement statement = con.createStatement();
            //String sql = "select * from view_dealledoc3";
            String sql = "select serv_url from config_servicios where nom = 'RIDEA'" ;
            ResultSet rs = statement.executeQuery(sql);
            url=rs.getString("serv_url");
            con.close();
        }catch(SQLException e){
            
        }
        */
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer perDni = requestData.getInt("perDni");
        
        String json="";
        try {
           //json = readUrl("http://200.37.190.57:8035/rideaoffice/apijson?dni="+perDni);
           // json = readUrl("http://localhost:8080/rideaoffice/apijson?dni="+perDni);
           //json = readUrl("http://10.100.100.18:8091/rideaoffice/apijson?dni="+perDni);
            System.out.println(serv.getURL()+perDni);
            json = readUrl(serv.getURL()+perDni);
            //json = readUrl("http://190.119.213.87:8091/rideaoffice/apijson?dni="+perDni);
          
         
        } catch (Exception ex) {
            Logger.getLogger(ListarLegajosRideaTx.class.getName()).log(Level.SEVERE, null, ex);
            return WebResponse.crearWebResponseError("No se pudo listar los legajos", ex.getMessage() );
        }
        System.out.println(json);
        Gson gson = new Gson();
        Page page = gson.fromJson(json, Page.class);
        page.deterPaginas();
       
        
        String jsonready = new Gson().toJson(page);       
        
        JSONObject jsonobj = new JSONObject(jsonready); 
        return WebResponse.crearWebResponseExito("Los legajos fueron listados exitosamente", jsonobj);
    }
    
    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1) {
                buffer.append(chars, 0, read);
            }

            return buffer.toString();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }
   
}
