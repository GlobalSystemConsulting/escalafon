package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.CarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 10/10/2016.
 */
public class EliminarCarpetaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EliminarCarpetaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idCarpeta = data.getInt("cod");
        return eliminarCarpeta(idCarpeta);
    }

    private WebResponse eliminarCarpeta(int idCarpeta) {
        try{
            CarpetaPedagogicaDao carpDao = (CarpetaPedagogicaDao) FactoryDao.buildDao("maestro.carpeta.CarpetaPedagogicaDao");
            CarpetaPedagogica carpeta = carpDao.buscarCarpetaPorId(idCarpeta);
            carpDao.delete(carpeta);
            return WebResponse.crearWebResponseExito("Se elimino corectamente");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarCarpetas",e);
            return WebResponse.crearWebResponseError("No se puede eliminar la carpeta");
        }
    }
}
