/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.vacacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.VacacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Vacacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;


public class EliminarVacacionTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EliminarVacacionTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        Integer vacId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            vacId = requestData.getInt("vacId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarVacacion",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        VacacionDao vacacionDao = (VacacionDao)FactoryDao.buildDao("se.VacacionDao");
        try{
            vacacionDao.delete(new Vacacion(vacId));
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarBajaAuditoria("Se elimino vacacion", "VAC_ID: "+vacId , wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        }catch(Exception e){
            System.out.println("No se pudo eliminar la vacacion\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la vacacion", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("La vacacion se elimino correctamente");
    }
    
}
