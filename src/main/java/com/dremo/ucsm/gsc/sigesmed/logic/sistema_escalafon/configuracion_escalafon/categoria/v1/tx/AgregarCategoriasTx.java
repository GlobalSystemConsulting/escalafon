/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.categoria.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PlanillaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Planilla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class AgregarCategoriasTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarCategoriasTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        Categoria categoria = null;
        Planilla planilla=null;
        Integer plaCat=0;
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            //Integer catId = requestData.getInt("catId");
            String abrCat = requestData.optString("abrCat");
            //Character estReg = requestData.optString("estReg").charAt(0);
            //Date fecMod = (Date)requestData.getString("fecMod");
            ////////historialCambioCategorias
            String nomCat = requestData.optString("nomCat");
                   plaCat= requestData.getInt("plaCat");
            ////////trabajadores
            //Integer usuMod = requestData.getInt("usuMod");
           categoria = new Categoria();
            if(plaCat!=0){
                 PlanillaDao planillaDao = (PlanillaDao) FactoryDao.buildDao("se.PlanillaDao");
                 planilla=planillaDao.buscarPorId(plaCat);
                 System.out.println("PLANILLAS PRUEBA:"+planilla.getNomPla());
                 categoria.setPlanilla(planilla);
            }
                    
            
            categoria.setAbrCat(abrCat);
            categoria.setNomCat(nomCat);
            categoria.setEstReg('A');
            categoria.setFecMod(new Date());
            categoria.setUsuMod(wr.getIdUsuario());
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nueva Categoria",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        
        CategoriaDao categoriaDao = (CategoriaDao) FactoryDao.buildDao("se.CategoriaDao");
        try {
            categoriaDao.insert(categoria);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva categoria",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("catId", categoria.getCatId());
        if(plaCat==0){
            oResponse.put("plaCat","");
            oResponse.put("plaNom","");
        }
        else{   
            oResponse.put("plaCat",categoria.getPlanilla().getPlaId());
            oResponse.put("plaNom", categoria.getPlanilla().getNomPla());
        }
        oResponse.put("abrCat", categoria.getAbrCat());
        oResponse.put("nomCat", categoria.getNomCat());
                
        return WebResponse.crearWebResponseExito("El registro de la Categoria se realizo correctamente", oResponse);

    }
    
}
