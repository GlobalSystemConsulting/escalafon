/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.UnidadMedidaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.UnidadMedida;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.UnidadMedida;

/**
 *
 * @author Jeferson
 */
public class RegistrarUnidadMedidaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        UnidadMedida uni_med = null;
        
        JSONObject requestData = (JSONObject)wr.getData();
        JSONArray unidades = requestData.getJSONArray("unidades");
        
         for(int i = 0; i < unidades.length();i++){
             int a = 0;
            JSONObject unidad = unidades.getJSONObject(i);
            String nom = unidad.getString("nom");
            Date fec_mod = new Date();
            int usu_mod = unidad.getInt("usu_mod");
            char est_reg = 'A';
            uni_med = new UnidadMedida(0,nom,fec_mod,usu_mod,est_reg);
            UnidadMedidaDAO un_med_dao = (UnidadMedidaDAO)FactoryDao.buildDao("scp.GrupoGenericoDAO");
            un_med_dao.insert(uni_med);
        }
        
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
