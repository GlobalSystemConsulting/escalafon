package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import java.util.Date;

public class BandejaMensajeModel{

    public int banMenId;    
    public Date fecEnv;
    public String asunto;
    
    public String rol;
    public String nombres;
    public String apellido1;

    public BandejaMensajeModel() {
    }
    public BandejaMensajeModel(int banmenId) {
        this.banMenId = banMenId;
    }
    public BandejaMensajeModel(int banMenId,String asunto, Date fecEnv,String nombres,String apellido1) {
       this.banMenId = banMenId;
       this.fecEnv = fecEnv;
       this.asunto = asunto;
       this.nombres = nombres;
       this.apellido1 = apellido1;
    }
}


