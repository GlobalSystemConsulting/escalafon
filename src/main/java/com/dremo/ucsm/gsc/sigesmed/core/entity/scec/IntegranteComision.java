package com.dremo.ucsm.gsc.sigesmed.core.entity.scec;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 15/09/16.
 */
@Entity
@Table(name = "integrante_comision", schema = "pedagogico")
public class IntegranteComision implements java.io.Serializable{

    @Embeddable
    public static class IntegranteId implements java.io.Serializable{
        @Column(name = "per_id", nullable = false)
        protected int perId;
        @Column(name = "com_id", nullable = false)
        protected int comId;

        public IntegranteId() {
        }

        public IntegranteId(int perId, int comId) {
            this.perId = perId;
            this.comId = comId;
        }
        @Override
        public boolean equals(Object o){
            if(o != null && o instanceof IntegranteId){
                IntegranteId id = (IntegranteId) o;
                return this.perId == id.perId
                        && this.comId == id.comId;
            }
            return false;
        }
        @Override
        public int hashCode(){
            return this.perId*this.comId + 1;
        }
    }
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name="perId", column=@Column(name="per_id", nullable=false) ),
            @AttributeOverride(name="comId", column=@Column(name="com_id", nullable=false)) })
    private IntegranteId id = new IntegranteId();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "per_id",insertable = false, updatable = false)
    private Persona persona;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "com_id",insertable = false, updatable = false)
    private Comision comision;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "car_com_id")
    private CargoComision cargoComision;

    @Temporal(TemporalType.DATE)
    @Column(name = "fec_reg_com")
    private Date fecRegCom;

    @Temporal(TemporalType.DATE)
    @Column(name = "fec_sal_com")
    private Date fecSalCom;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;

    @Column(name = "est_reg",length = 1)
    private Character estReg;

    @OneToMany(mappedBy = "integranteComision",fetch =  FetchType.LAZY, cascade = CascadeType.ALL)
    List<AsistenciaReunionComision> asistenciaReuniones = new ArrayList<>();

    /*@OneToMany(mappedBy = "resAcu", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private List<AcuerdosActaComision> acuerdos = new ArrayList<>();*/

    public IntegranteComision() {
    }
    public IntegranteComision(Persona persona, Comision comision) {
        this.persona = persona;
        this.comision = comision;

        this.id.comId = comision.getComId();
        this.id.perId = persona.getPerId();

        this.comision.getIntegrantes().add(this);
    }

    public IntegranteComision(Persona persona, Comision comision, CargoComision cargoComision, Date fecRegCom, Date fecSalCom) {
        try{
            this.persona = persona;
            this.comision = comision;
            this.cargoComision = cargoComision;
            this.fecRegCom = fecRegCom;
            this.fecSalCom = fecSalCom;

            this.id.comId = comision.getComId();
            this.id.perId = persona.getPerId();

            this.comision.getIntegrantes().add(this);
        }catch (Exception e){
            System.out.println(e.toString());
            throw e;
        }

    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Comision getComision() {
        return comision;
    }

    public void setComision(Comision comision) {
        this.comision = comision;
    }

    public CargoComision getCargoComision() {
        return cargoComision;
    }

    public void setCargoComision(CargoComision cargoComision) {
        this.cargoComision = cargoComision;
    }

    public Date getFecRegCom() {
        return fecRegCom;
    }

    public void setFecRegCom(Date fecRegCom) {
        this.fecRegCom = fecRegCom;
    }

    public Date getFecSalCom() {
        return fecSalCom;
    }

    public void setFecSalCom(Date fecSalCom) {
        this.fecSalCom = fecSalCom;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<AsistenciaReunionComision> getAsistenciaReuniones() {
        return asistenciaReuniones;
    }

    public void setAsistenciaReuniones(List<AsistenciaReunionComision> asistenciaReuniones) {
        this.asistenciaReuniones = asistenciaReuniones;
    }
}
