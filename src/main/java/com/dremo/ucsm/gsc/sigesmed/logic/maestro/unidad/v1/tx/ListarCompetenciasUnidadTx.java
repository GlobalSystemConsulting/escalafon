package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.UtilMaestroLogic;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 17/11/2016.
 */
public class ListarCompetenciasUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarCompetenciasUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int codUnidad = data.getInt("cod");
        int tipLis = data.optInt("tip",-1);
        return listarCompetencias(codUnidad,tipLis);
    }

    protected WebResponse listarCompetencias(int codUnidad,int tipLis) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            List<CompetenciasUnidadDidactica> competencias = unidadDao.listarCompetenciasUnidad(codUnidad);
            Map<CompetenciaAprendizaje,List<CapacidadAprendizaje>> mapCompetencias = new HashMap<>();
            for(CompetenciasUnidadDidactica comp : competencias){
                CompetenciaCapacidad comCap = comp.getCompetenciaCapacidad();
                CompetenciaAprendizaje competencia = comCap.getCom();
                setSelectedIndicador(comCap.getCap(), comp.getIndicadores());
                if(!mapCompetencias.containsKey(competencia)){
                    List<CapacidadAprendizaje> listCapacidades = new ArrayList<>();
                    listCapacidades.add(comCap.getCap());
                    mapCompetencias.put(competencia,listCapacidades);
                }else{
                    mapCompetencias.get(competencia).add(comCap.getCap());
                }
            }
            JSONArray arrResult =  tipLis == -1 ? UtilMaestroLogic.mapCompetenciasToJSON(mapCompetencias) : UtilMaestroLogic.mapCompetenciasSinIndicadoresToJSON(mapCompetencias);
            return WebResponse.crearWebResponseExito("Se listo con exito la data",arrResult);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarCompetencias",e);
            return WebResponse.crearWebResponseError("No se pudo listar los datos listados");
        }

    }
    private void setSelectedIndicador(CapacidadAprendizaje cap,Set<IndicadorAprendizaje> indicadoresSeleccionados){
        if(indicadoresSeleccionados == null) return;
        List<IndicadorAprendizaje> indicadores = cap.getIndicadores();
        for(IndicadorAprendizaje ind :  indicadores){
            if(indicadoresSeleccionados.contains(ind))
                ind.setSelectedIndicador(true);
        }
    }
}
