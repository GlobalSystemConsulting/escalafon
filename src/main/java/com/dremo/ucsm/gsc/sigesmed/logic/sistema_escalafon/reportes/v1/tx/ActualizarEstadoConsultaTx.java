/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaCertificadaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarEstadoConsultaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarFechaCertificacionConsultaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject)wr.getData();  
            System.out.println(data);
            return actualizarEstadoConsulta(data);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar Estado de consulta",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar estado de consulta certificada", e.getMessage());
        } 
    }
    
    private WebResponse actualizarEstadoConsulta(JSONObject data) {
        try{
            int rConsultaCertificada = data.optInt("con_cer_id");
            ConsultaCertificadaDao consultaCertificadaDao = (ConsultaCertificadaDao) FactoryDao.buildDao("ConsultaCertificadaDao");
            ConsultaCertificada consultaCertificada = consultaCertificadaDao.buscarPorID(rConsultaCertificada);
            /*Date fecha_actual = new Date();
            consultaCertificada.setFecCertificacion(fecha_actual);*/
            if(consultaCertificada.getEstReg()=='C'){///solamente si el estado es C - certificado pasa a ser D - descargado, sino mantiene su estado
                consultaCertificada.setEstReg('D');
                consultaCertificadaDao.update(consultaCertificada);
            }
            JSONObject oResponse = new JSONObject();  
            JSONObject consultaCertificadaRes = new JSONObject();
            oResponse.put("consultaCertificada", consultaCertificadaRes);
            
            
            System.out.println(oResponse);
            
            return WebResponse.crearWebResponseExito("Estado de consulta certificada actualizada exitosamente", oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarEstadoConsulta",e);
            return WebResponse.crearWebResponseError("Error, el estado de la certificacion no fue actualizado");
        }
    }
    
}

