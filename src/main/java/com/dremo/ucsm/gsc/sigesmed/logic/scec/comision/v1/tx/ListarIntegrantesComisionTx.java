package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.IntegranteComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.IntegranteComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 30/09/16.
 */
public class ListarIntegrantesComisionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(ListarIntegrantesComisionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String cod = wr.getMetadataValue("cod");
        return listarIntegrantes(Integer.parseInt(cod));
    }
    private WebResponse listarIntegrantes(int idCom){
        try{
            IntegranteComisionDao integranteComisionDao = (IntegranteComisionDao) FactoryDao.buildDao("scec.IntegranteComisionDao");
            List<IntegranteComision> integrantes = integranteComisionDao.buscarPorComision(idCom);
            JSONArray jsonIntegrantes = new JSONArray();
            for(IntegranteComision integrante : integrantes){
                Persona persona = integrante.getPersona();
                CargoComision cargoComision = integrante.getCargoComision();

                JSONObject jsonIntegrante = new JSONObject(EntityUtil.objectToJSONString(new String[]{"perId","dni","nom","apeMat","apePat","num1","num2","email"},new String[]{"cod","dni","nom","apem","apep","cel","fij","ema"},persona));
                jsonIntegrante.put("car",cargoComision.getNomCar());

                jsonIntegrantes.put(jsonIntegrante);
            }
            return WebResponse.crearWebResponseExito("Exito al listar los participantes",WebResponse.OK_RESPONSE).setData(jsonIntegrantes);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarParticipantes",e);
            return WebResponse.crearWebResponseError("No se pudo obtener los participantes",WebResponse.BAD_RESPONSE);
        }
    }
}
