/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */

@Entity(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.se.Trabajador")
@Table(name = "trabajador")
public class Trabajador implements Serializable {
    
    @Id
    @Column(name="tra_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_trabajador", sequenceName="public.trabajador_tra_id_seq" )
    @GeneratedValue(generator="secuencia_trabajador")
    private Integer traId;
    
    @Column(name = "tie_serv")
    private Short tieServ;
    
    @Column(name = "fec_ing")
    @Temporal(TemporalType.DATE)
    private Date fecIng;
    
    @Column(name = "sal")
    private Double sal;   
    
    @Column(name = "tra_tip")
    private String traTip;
    
    @Column(name = "tra_con")
    private Character traCon;
    
    @Column(name = "est_lab")
    private Character estLab;
    
    @Column(name = "anios_out")
    private Integer aniosOut;
    
    @Column(name = "fec_sal")
    @Temporal(TemporalType.DATE)
    private Date fecSal;
    
    @Column(name = "tiemserforedu")
    private Integer tiemserforedu;
    
    @Column(name = "tiemserest")
    private Integer tiemserest;
     
    @Column(name = "tiemsertot")
    private Integer tiemsertot;
    
    @Column(name = "aniser25")
    @Temporal(TemporalType.DATE)
    private Date aniser25;
    
    @Column(name = "aniser30")
    @Temporal(TemporalType.DATE)
    private Date aniser30;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private Character estReg; 
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "tra_car", referencedColumnName = "crg_tra_ide")
    private TrabajadorCargo traCar;
       
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "org_id", referencedColumnName = "org_id")   
    private Organizacion organizacion;
        
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "cat_id", referencedColumnName = "cat_id")   
    private Categoria categoria;
            
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "zon_id", referencedColumnName = "zon_id")   
    private Zona zona;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "car_id", referencedColumnName = "car_id")   
    private Cargo cargo;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "dep_id", referencedColumnName = "dep_id")   
    private Dependencia dependencia;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "rol_id", referencedColumnName = "rol_id")   
    private Rol rol;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "per_id", referencedColumnName = "per_id")
    private Persona persona;   
    
    @OneToOne(fetch=FetchType.LAZY, mappedBy="trabajador")
    private FichaEscalafonaria fe;
    
    @Column(name = "orgi_id")
    private Integer orgiId;
        
    public Trabajador() {
    }
    
    public Trabajador(Integer traId) {
        this.traId = traId;
    }

    public Trabajador(Persona persona, Organizacion organizacion, Integer usuMod, Date fecMod, Character estReg) {
        this.persona = persona;
        this.organizacion = organizacion;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    
    public Trabajador(Persona persona, Organizacion organizacion, Character traCon, Integer usuMod, Date fecMod, Character estReg) {
        this.persona = persona;
        this.organizacion = organizacion;
        this.traCon = traCon;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    
    public Trabajador(Persona persona, Organizacion organizacion, Rol rol, Character estLab, Date fecIng, Integer usuMod, Date fecMod, Character estReg) {
        this.persona = persona;
        this.organizacion = organizacion;
        this.rol = rol;
        this.estLab = estLab;
        this.fecIng = fecIng;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public Integer getTraId() {
        return traId;
    }

    public void setTraId(Integer traId) {
        this.traId = traId;
    }

    public Short getTieServ() {
        return tieServ;
    }

    public void setTieServ(Short tieServ) {
        this.tieServ = tieServ;
    }

    public Date getFecIng() {
        return fecIng;
    }

    public void setFecIng(Date fecIng) {
        this.fecIng = fecIng;
    }

    public Double getSal() {
        return sal;
    }

    public void setSal(Double sal) {
        this.sal = sal;
    }

    public Character getTraCon() {
        return traCon;
    }

    public void setTraCon(Character traCon) {
        this.traCon = traCon;
    }
    
    public void setAniosOut(Integer aniosOut) {
        this.aniosOut = aniosOut;
    }

    public Integer getAniosOut() {
        return aniosOut;
    }
    
    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getTraTip() {
        return traTip;
    }

    public void setTraTip(String traTip) {
        this.traTip = traTip;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public TrabajadorCargo getTraCar() {
        return traCar;
    }

    public void setTraCar(TrabajadorCargo traCar) {
        this.traCar = traCar;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Zona getZona() {
        return zona;
    }

    public void setZona(Zona zona) {
        this.zona = zona;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }
    
    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public Dependencia getDependencia() {
        return dependencia;
    }

    public void setDependencia(Dependencia dependencia) {
        this.dependencia = dependencia;
    }

    public FichaEscalafonaria getFe() {
        return fe;
    }

    public void setFe(FichaEscalafonaria fe) {
        this.fe = fe;
    }

    public Character getEstLab() {
        return estLab;
    }

    public void setEstLab(Character estLab) {
        this.estLab = estLab;
    }

    public Date getFecSal() {
        return fecSal;
    }

    public void setFecSal(Date fecSal) {
        this.fecSal = fecSal;
    }

    public Integer getOrgiId() {
        return orgiId;
    }

    public void setOrgiId(Integer orgiId) {
        this.orgiId = orgiId;
    }

    public Integer getTiemserforedu() {
        return tiemserforedu;
    }

    public void setTiemserforedu(Integer tiemserforedu) {
        this.tiemserforedu = tiemserforedu;
    }

    public Integer getTiemserest() {
        return tiemserest;
    }

    public void setTiemserest(Integer tiemserest) {
        this.tiemserest = tiemserest;
    }

    public Integer getTiemsertot() {
        return tiemsertot;
    }

    public void setTiemsertot(Integer tiemsertot) {
        this.tiemsertot = tiemsertot;
    }

    public Date getAniser25() {
        return aniser25;
    }

    public void setAniser25(Date aniser25) {
        this.aniser25 = aniser25;
    }

    public Date getAniser30() {
        return aniser30;
    }

    public void setAniser30(Date aniser30) {
        this.aniser30 = aniser30;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (traId != null ? traId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trabajador)) {
            return false;
        }
        Trabajador other = (Trabajador) object;
        if ((this.traId == null && other.traId != null) || (this.traId != null && !this.traId.equals(other.traId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Trabajador{" + "traId=" + traId + ", organizacion=" + organizacion + ", categoria=" + categoria + ", zona=" + zona + ", cargo=" + cargo + ", dependencia=" + dependencia + ", persona=" + persona + '}';
    }

}
