/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoDeComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.Comision;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.*;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author ucsm
 */
public class ComisionDaoHibernate extends GenericDaoHibernate<Comision> implements ComisionDao{
    
    private static Logger logger = Logger.getLogger(ComisionDaoHibernate.class.getName());

    @Override
    public Comision buscarPorId(int idComision) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query  = session.createCriteria(Comision.class);
            query.setFetchMode("cargoDeComisiones",FetchMode.JOIN);
            query.add(Restrictions.eq("comId",idComision));
            //Comision comision= (Comision) session.get(Comision.class,idComision);
            return (Comision) query.uniqueResult();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Comision buscarPorIdConIntegrantes(int idComision) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query  = session.createCriteria(Comision.class);
            query.setFetchMode("integrantes",FetchMode.JOIN);
            query.add(Restrictions.eq("comId",idComision));
            //Comision comision= (Comision) session.get(Comision.class,idComision);
            return (Comision) query.uniqueResult();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Comision> listarComisionesPorOrganizacion(int idOrganizacion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria query = session.createCriteria(Comision.class);
        query.setFetchMode("organizacion", FetchMode.JOIN);
        try {
            List<Comision> comisiones = null;
            Organizacion org = (Organizacion) session.get(Organizacion.class,idOrganizacion);
            logger.log(Level.INFO,"valors {0}",org.getTipoOrganizacion().getCod());
            String tipo = org.getTipoOrganizacion().getCod().toUpperCase().trim();
            if(tipo.equals("UGEL")){
                query.createCriteria("organizacion")
                        .createCriteria("organizacionPadre")
                        .add(Restrictions.eq("orgId",idOrganizacion));
            }else if(tipo.equals("IE")){
                query.createCriteria("organizacion")
                        .add(Restrictions.eq("orgId",idOrganizacion));
            }
            //si es dremo se devuelve todas las comisiones, no hay restricciones

            query.add(Restrictions.eq("estReg",'A'));
            comisiones = query.list();
            return comisiones;

        }catch (HibernateException e){
            logger.log(Level.SEVERE,"listarComisionesPorOrganizacion",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<CargoDeComision> buscarCargosDeComision(int idCom) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(CargoDeComision.class);
            query.setFetchMode("cargoComision",FetchMode.JOIN);

            query.createCriteria("comision")
                    .add(Restrictions.eq("comId",idCom));

            return query.list();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarCargosDeComision",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Comision> listarComisionPorOrganizacionDirector(int idOrganizacion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria query = session.createCriteria(Comision.class);
        query.setFetchMode("organizacion", FetchMode.JOIN);
        try {
            List<Comision> comisiones = null;
            Organizacion org = (Organizacion) session.get(Organizacion.class,idOrganizacion);
            
                query.createCriteria("organizacion")
                        .add(Restrictions.eq("orgId",idOrganizacion));
            
            //si es dremo se devuelve todas las comisiones, no hay restricciones

            query.add(Restrictions.eq("estReg",'A'));
            comisiones = query.list();
            return comisiones;

        }catch (HibernateException e){
            logger.log(Level.SEVERE,"listarComisionesPorOrganizacion",e);
            throw e;
        }finally {
            session.close();
        } 
    }

}
