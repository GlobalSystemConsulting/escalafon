/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Calendario.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Calendario.v1.tx.ActualizarActividadCalendarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Calendario.v1.tx.CrearActividadCalendarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Calendario.v1.tx.EliminarActividadTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Calendario.v1.tx.ListarActividadesPropiasTx;

/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_WEB);        
        
        //Registrando el Nombre del componente
        component.setName("calendario");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("crearActividad", CrearActividadCalendarioTx.class);
        component.addTransactionDELETE("eliminarActividad", EliminarActividadTx.class);
        component.addTransactionGET("listarActividades", ListarActividadesPropiasTx.class);
        component.addTransactionPUT("actualizarActividad", ActualizarActividadCalendarioTx.class);
        
        return component;
    }
}


