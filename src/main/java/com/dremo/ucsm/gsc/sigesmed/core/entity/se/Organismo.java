/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "organismo" , schema="administrativo")

public class Organismo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "orga_id")
    private Integer orgaId;
    
    @Size(max = 2147483647)
    @Column(name = "cod_orga")
    private String codOrga;
    
    @Size(max = 2147483647)
    @Column(name = "nom_orga")
    private String nomOrga;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "organismo")
    List<Facultad> facultades;

    public Organismo() {
    }

    public Organismo(Integer orgaId) {
        this.orgaId = orgaId;
    }

    public Integer getOrgaId() {
        return orgaId;
    }

    public void setOrgaId(Integer orgaId) {
        this.orgaId = orgaId;
    }

    public String getCodOrga() {
        return codOrga;
    }

    public void setCodOrga(String codOrga) {
        this.codOrga = codOrga;
    }

    public String getNomOrga() {
        return nomOrga;
    }

    public void setNomOrga(String nomOrga) {
        this.nomOrga = nomOrga;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public List<Facultad> getFacultades() {
        return facultades;
    }

    public void setFacultades(List<Facultad> facultades) {
        this.facultades = facultades;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orgaId != null ? orgaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Organismo)) {
            return false;
        }
        Organismo other = (Organismo) object;
        if ((this.orgaId == null && other.orgaId != null) || (this.orgaId != null && !this.orgaId.equals(other.orgaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Organismo{" + "orgaId=" + orgaId + ", codOrga=" + codOrga + ", nomOrga=" + nomOrga + ", estReg=" + estReg + ", fecMod=" + fecMod + ", usuMod=" + usuMod + '}';
    }


}
