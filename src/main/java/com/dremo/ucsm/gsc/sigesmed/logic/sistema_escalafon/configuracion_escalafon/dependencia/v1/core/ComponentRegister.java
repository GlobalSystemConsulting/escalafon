/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.dependencia.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.dependencia.v1.tx.ActualizarDependenciasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.dependencia.v1.tx.AgregarDependenciasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.dependencia.v1.tx.EliminarDependenciasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.dependencia.v1.tx.ListarDependenciasTx;

/**
 *
 * @author Felipe
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("dependenciaConfiguracion");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarDependencias", ListarDependenciasTx.class);
        seComponent.addTransactionPUT("actualizarDependencias", ActualizarDependenciasTx.class);
        seComponent.addTransactionPOST("agregarDependencias", AgregarDependenciasTx.class);
        seComponent.addTransactionDELETE("eliminarDependencias", EliminarDependenciasTx.class);
        return seComponent;
    }
}
