package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "criterios_certificacion_capacitacion", schema = "pedagogico")
public class CriterioCertificacion implements Serializable {
    
    @Id
    @Column(name = "crit_cer_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_criterio_certificacion_capacitacion", sequenceName = "pedagogico.criterios_certificacion_capacitacion_crit_cer_cap_id_seq")
    @GeneratedValue(generator = "secuencia_criterio_certificacion_capacitacion")
    private int critCerCapId;
    
    @Column(name = "nom")
    private String nom;

    @Column(name = "est_reg", nullable = false)
    private Character estReg;
    
    @Column(name = "tip_eva")
    private Character tipEva;
    
    @Column(name = "tip_cri")
    private Character tipCri;
    
    @Column(name = "val")
    private double val;
    
    @Column(name = "not_min")
    private double notMin;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cur_cap_id")
    private CursoCapacitacion cursoCapacitacion;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "criterio")
    private Set<MotivoIncumplimiento> criterios = new HashSet<>(0);

    public CriterioCertificacion() {}

    public CriterioCertificacion(String nom, Character estReg, Character tipCri, CursoCapacitacion cursoCapacitacion) {
        this.nom = nom;
        this.estReg = estReg;
        this.tipCri = tipCri;
        this.cursoCapacitacion = cursoCapacitacion;
    }
    
    public int getCritCerCapId() {
        return critCerCapId;
    }

    public void setCritCerCapId(int critCerCapId) {
        this.critCerCapId = critCerCapId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Character getTipEva() {
        return tipEva;
    }

    public void setTipEva(Character tipEva) {
        this.tipEva = tipEva;
    }

    public Character getTipCri() {
        return tipCri;
    }

    public void setTipCri(Character tipCri) {
        this.tipCri = tipCri;
    }

    public double getVal() {
        return val;
    }

    public void setVal(double val) {
        this.val = val;
    }

    public double getNotMin() {
        return notMin;
    }

    public void setNotMin(double notMin) {
        this.notMin = notMin;
    }

    public CursoCapacitacion getCursoCapacitacion() {
        return cursoCapacitacion;
    }

    public void setCursoCapacitacion(CursoCapacitacion cursoCapacitacion) {
        this.cursoCapacitacion = cursoCapacitacion;
    }

    public Set<MotivoIncumplimiento> getCriterios() {
        return criterios;
    }

    public void setCriterios(Set<MotivoIncumplimiento> criterios) {
        this.criterios = criterios;
    }

    @Override
    public String toString() {
        return "CriterioCertificacion{" + "critCerCapId=" + critCerCapId + ", nom=" + nom + ", estReg=" + estReg + ", tipEva=" + tipEva + ", tipCri=" + tipCri + ", val=" + val + ", notMin=" + notMin + '}';
    }
    
    
}
