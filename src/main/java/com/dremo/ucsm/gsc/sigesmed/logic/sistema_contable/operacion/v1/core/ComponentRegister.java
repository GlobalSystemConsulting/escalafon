/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.operacion.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.operacion.v1.tx.ActualizarOperacionTx;

import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.operacion.v1.tx.EliminarOperacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.operacion.v1.tx.InsertarOperacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.operacion.v1.tx.ListarOperacionTx;

/**
 *
 * @author RE
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_SISTEMA_CONTABLE_INSTITUCIONAL);        
        
        //Registrnado el Nombre del componente
        component.setName("operacion");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarOperacion", InsertarOperacionTx.class);
        component.addTransactionGET("listarOperacion", ListarOperacionTx.class);        
        component.addTransactionDELETE("eliminarOperacion", EliminarOperacionTx.class);
        component.addTransactionPUT("actualizarOperacion", ActualizarOperacionTx.class);
        
        return component;
    }
}

