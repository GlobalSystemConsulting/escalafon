package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.Comision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 29/09/16.
 */
public class EliminarComisionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(EliminarComisionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String idStr = wr.getMetadataValue("cod");

        return eliminarComision(Integer.parseInt(idStr));
    }
    private WebResponse eliminarComision(int idStr){
        try{
            ComisionDao comisionDao = (ComisionDao) FactoryDao.buildDao("scec.ComisionDao");
            Comision comision = comisionDao.buscarPorId(idStr);
            comisionDao.delete(comision);
            return WebResponse.crearWebResponseExito("Operacion realizada con exito",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarComision",e);
            return WebResponse.crearWebResponseError("No se realizo la operacion",WebResponse.BAD_RESPONSE);
        }
    }

}
