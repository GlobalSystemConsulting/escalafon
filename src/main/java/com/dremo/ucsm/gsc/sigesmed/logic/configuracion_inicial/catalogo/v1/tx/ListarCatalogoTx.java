
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.CatalogoTablaDao;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.CatalogoTabla;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarCatalogoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
      /*
        *  Parte para la operacion en la Base de Datos
        */
        List<CatalogoTabla> catalogosTablas = null;
        CatalogoTablaDao catalogoTablaDao = (CatalogoTablaDao)FactoryDao.buildDao("CatalogoTablaDao");
        
        try{
            catalogosTablas = catalogoTablaDao.buscarTodos(CatalogoTabla.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Catalogos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Catalogos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(CatalogoTabla catalogos:catalogosTablas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("catalogoID",catalogos.getCatId());
            oResponse.put("nombretabla",catalogos.getNomTabla());
            oResponse.put("nombreclase",catalogos.getNomClase());
            oResponse.put("fecha",catalogos.getFecMod().toString());
            oResponse.put("estado",""+catalogos.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

