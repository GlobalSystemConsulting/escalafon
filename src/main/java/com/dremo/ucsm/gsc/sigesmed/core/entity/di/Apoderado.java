/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.di;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "apoderado", schema="pedagogico")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Apoderado.findAll", query = "SELECT a FROM Apoderado a"),
//    @NamedQuery(name = "Apoderado.findByApoId", query = "SELECT a FROM Apoderado a WHERE a.apoId = :apoId"),
//    @NamedQuery(name = "Apoderado.findByOcu", query = "SELECT a FROM Apoderado a WHERE a.ocu = :ocu"),
//    @NamedQuery(name = "Apoderado.findByTip", query = "SELECT a FROM Apoderado a WHERE a.tip = :tip"),
//    @NamedQuery(name = "Apoderado.findByNumHij", query = "SELECT a FROM Apoderado a WHERE a.numHij = :numHij"),
//    @NamedQuery(name = "Apoderado.findByFecMod", query = "SELECT a FROM Apoderado a WHERE a.fecMod = :fecMod"),
//    @NamedQuery(name = "Apoderado.findByUsuMod", query = "SELECT a FROM Apoderado a WHERE a.usuMod = :usuMod"),
//    @NamedQuery(name = "Apoderado.findByEstReg", query = "SELECT a FROM Apoderado a WHERE a.estReg = :estReg")})
public class Apoderado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "apo_id")
    private Long apoId;
    @Column(name = "ocu")
    private String ocu;
    @Column(name = "tip")
    private Integer tip;
    @Column(name = "num_hij")
    private Short numHij;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg;
    @JoinColumn(name = "apo_id", referencedColumnName = "per_id")
    @OneToOne(fetch=FetchType.LAZY)
    private Persona persona;
    
    public Apoderado() {
    }

    public Apoderado(Long apoId) {
        this.apoId = apoId;
    }

    public Long getApoId() {
        return apoId;
    }

    public void setApoId(Long apoId) {
        this.apoId = apoId;
    }

    public String getOcu() {
        return ocu;
    }

    public void setOcu(String ocu) {
        this.ocu = ocu;
    }

    public Integer getTip() {
        return tip;
    }

    public void setTip(Integer tip) {
        this.tip = tip;
    }

    public Short getNumHij() {
        return numHij;
    }

    public void setNumHij(Short numHij) {
        this.numHij = numHij;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (apoId != null ? apoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Apoderado)) {
            return false;
        }
        Apoderado other = (Apoderado) object;
        if ((this.apoId == null && other.apoId != null) || (this.apoId != null && !this.apoId.equals(other.apoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Apoderado[ apoId=" + apoId + " ]";
    }
    
}
