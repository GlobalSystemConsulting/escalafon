package com.dremo.ucsm.gsc.sigesmed.logic.maestro.acomp.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 09/01/2017.
 */
public class ListarUGELESTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarUGELESTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idOrg = data.getInt("org");
        return listarHijos(idOrg);
    }
    private WebResponse listarHijos(int idOrg) {
        try{
            OrganizacionDao orgDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            Organizacion org = orgDao.buscarConTipoOrganizacionYPadre(idOrg);
            List<Organizacion> orgs = null;

            if(org != null && org.getOrganizacionPadre() != null &&org.getOrganizacionPadre().getOrgId() == 2){//UGEL
                orgs = new ArrayList<>();
                orgs.add(org);
            }else  orgs = orgDao.buscarHijosOrganizacion(idOrg);
            JSONArray orgsJSON = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"orgId", "nom", "cod", "ali"},
                    new String[]{"id", "nom", "cod", "ali"},
                    orgs
            ));
            return WebResponse.crearWebResponseExito("Se listo correctamente los indicadores",orgsJSON);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarHijos",e);
            return WebResponse.crearWebResponseError("no se puede listar lso hijos de la organziacion");
        }
    }
}
