/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import java.util.List;

/**
 *
 * @author gscadmin
 */
public interface TrabajadorDao extends GenericDao<Trabajador>{
    Trabajador buscarPorPerId(Integer perId);
    Trabajador buscarPorId(Integer traId);
    Trabajador buscarPorUsuarioId(int codUsr);
    Trabajador buscarTrabajador(int traId);
    List<Trabajador> buscarPersonaTrabajador(int perId); 
    List<Trabajador> listarTrabajadoresxPersona(int perId);
    Trabajador mostrarDatosTrabajador(int traId, int perId);
    
}
