package com.dremo.ucsm.gsc.sigesmed.core.entity.mmi;


public class VacanteAutomatica {
    int graSecId;
    int numMaxEstMat;
    int numTotEstMat;
    int plaNivId;
    int nivId;
    String nivNom;
    String nivAbr;
    int jorEscId;
    String jorEscNom;
    String jorEscAbr;
    int turId;
    String turNom;
    int graId;
    String graNom;
    char secId;

    public VacanteAutomatica() {
    }

    public VacanteAutomatica(int graSecId, int numMaxEstMat, int numTotEstMat, int plaNivId, int nivId, String nivNom, String nivAbr, int jorEscId, String jorEscNom, String jorEscAbr, int turId, String turNom, int graId, String graNom, char secId) {
        this.graSecId = graSecId;
        this.numMaxEstMat = numMaxEstMat;
        this.numTotEstMat = numTotEstMat;
        this.plaNivId = plaNivId;
        this.nivId = nivId;
        this.nivNom = nivNom;
        this.nivAbr = nivAbr;
        this.jorEscId = jorEscId;
        this.jorEscNom = jorEscNom;
        this.jorEscAbr = jorEscAbr;
        this.turId = turId;
        this.turNom = turNom;
        this.graId = graId;
        this.graNom = graNom;
        this.secId = secId;
    }


    public int getGraSecId() {
        return graSecId;
    }

    public int getNumMaxEstMat() {
        return numMaxEstMat;
    }

    public int getNumTotEstMat() {
        return numTotEstMat;
    }

    public int getPlaNivId() {
        return plaNivId;
    }

    public int getNivId() {
        return nivId;
    }

    public int getJorEscId() {
        return jorEscId;
    }

    public int getTurId() {
        return turId;
    }

    public int getGraId() {
        return graId;
    }

    public char getSecId() {
        return secId;
    }

    public String getNivNom() {
        return nivNom;
    }

    public String getNivAbr() {
        return nivAbr;
    }

    public String getJorEscNom() {
        return jorEscNom;
    }

    public String getJorEscAbr() {
        return jorEscAbr;
    }

    public String getTurNom() {
        return turNom;
    }

    public String getGraNom() {
        return graNom;
    }

    public void setGraSecId(int graSecId) {
        this.graSecId = graSecId;
    }

    public void setNumMaxEstMat(int numMaxEstMat) {
        this.numMaxEstMat = numMaxEstMat;
    }

    public void setNumTotEstMat(int numTotEstMat) {
        this.numTotEstMat = numTotEstMat;
    }

    public void setPlaNivId(int plaNivId) {
        this.plaNivId = plaNivId;
    }

    public void setNivId(int nivId) {
        this.nivId = nivId;
    }

    public void setJorEscId(int jorEscId) {
        this.jorEscId = jorEscId;
    }

    public void setTurId(int turId) {
        this.turId = turId;
    }

    public void setGraId(int graId) {
        this.graId = graId;
    }

    public void setSecId(char secId) {
        this.secId = secId;
    }

    public void setNivNom(String nivNom) {
        this.nivNom = nivNom;
    }

    public void setNivAbr(String nivAbr) {
        this.nivAbr = nivAbr;
    }

    public void setJorEscNom(String jorEscNom) {
        this.jorEscNom = jorEscNom;
    }

    public void setJorEscAbr(String jorEscAbr) {
        this.jorEscAbr = jorEscAbr;
    }

    public void setTurNom(String turNom) {
        this.turNom = turNom;
    }

    public void setGraNom(String graNom) {
        this.graNom = graNom;
    }
 
}
