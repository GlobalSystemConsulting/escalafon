/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.planilla.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.planilla.v1.tx.ActualizarPlanillasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.planilla.v1.tx.AgregarPlanillasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.planilla.v1.tx.EliminarPlanillasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.planilla.v1.tx.ListarPlanillasTx;

/**
 *
 * @author Felipe
 */

public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("planillaConfiguracion");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarPlanillas", ListarPlanillasTx.class);
        seComponent.addTransactionPUT("actualizarPlanillas", ActualizarPlanillasTx.class);
        seComponent.addTransactionPOST("agregarPlanillas", AgregarPlanillasTx.class);
        seComponent.addTransactionDELETE("eliminarPlanillas", EliminarPlanillasTx.class);
        return seComponent;
    }
    
}
