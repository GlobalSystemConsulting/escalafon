
package com.dremo.ucsm.gsc.sigesmed.core.dao;


import com.dremo.ucsm.gsc.sigesmed.core.entity.LogAuditoria;
import java.util.List;

/**
 *
 * @author abel
 */
public interface LogAuditoriaDao extends GenericDao<LogAuditoria>{
    
    public List<Object[]> listarTodosRegistros();
    public List<String[]> listarReporteAuditoria(String fec_ini, String fec_fin, String dni);

}
