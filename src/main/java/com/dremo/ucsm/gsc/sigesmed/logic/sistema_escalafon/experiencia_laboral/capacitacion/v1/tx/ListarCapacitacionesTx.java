/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Capacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarCapacitacionesTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarCapacitacionesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer perId = requestData.getInt("perId");
                
        List<Capacitacion> capacitaciones = null;
        CapacitacionDao capacitacionDao = (CapacitacionDao)FactoryDao.buildDao("se.CapacitacionDao");
        
        try{
            capacitaciones = capacitacionDao.listarxFichaEscalafonaria(perId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar capacitaciones",e);
            System.out.println("No se pudo listar las capacitaciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar las capacitaciones", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        JSONArray miArray = new JSONArray();
        for(Capacitacion c: capacitaciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("capId", c.getCapId());
            oResponse.put("nom", c.getNom()==null?"":c.getNom());
            oResponse.put("tip", c.getTip()==null?"":c.getTip());
            oResponse.put("fec", c.getFec()==null?"":sdi.format(c.getFec()));
            oResponse.put("cal", c.getCal());
            oResponse.put("lug", c.getLug()==null?"":c.getLug());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las capacitaciones fueron listadas exitosamente", miArray);
    }
    
}
