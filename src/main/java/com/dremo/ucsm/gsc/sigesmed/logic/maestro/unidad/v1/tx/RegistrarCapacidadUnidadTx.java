package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CapacidadAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaCapacidadDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 16/11/2016.
 */
public class RegistrarCapacidadUnidadTx implements ITransaction {
    private Logger logger = Logger.getLogger(RegistrarCapacidadUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        JSONArray competencias = data.optJSONArray("competencias");
        int idUnidad = data.getInt("idUnidad");
        String tip = data.getString("tipUnidad");
        return registrarCapacidades(idUnidad, tip, competencias);
    }

    private WebResponse registrarCapacidades(int idUnidad,String tip, JSONArray competencias) {
        try{
            //Iniciamos los objetos dao que se usarane
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            CompetenciaCapacidadDao comCapDao = (CompetenciaCapacidadDao) FactoryDao.buildDao("maestro.plan.CompetenciaCapacidadDao");
            CompetenciaAprendizajeDao comDao = (CompetenciaAprendizajeDao) FactoryDao.buildDao("maestro.plan.CompetenciaAprendizajeDao");
            CapacidadAprendizajeDao capDao = (CapacidadAprendizajeDao) FactoryDao.buildDao("maestro.plan.CapacidadAprendizajeDao");

            UnidadDidactica objUni = unidadDao.buscarUnidadDidactica(idUnidad, tip);
            for(int i = 0; i < competencias.length(); i++){
                JSONObject competencia = competencias.getJSONObject(i);
                JSONArray capacidades = competencia.getJSONArray("capacidades");
                int numIndicadores = 0;
                for(int j = 0; j < capacidades.length(); j++){
                    JSONObject capacidad =  capacidades.getJSONObject(j);
                    //CapacidadAprendizaje objCap = capDao.buscarPorId(capacidad.getInt("capId"));
                    CompetenciaCapacidad objComCap = comCapDao.buscarCapacidadDeCompetencia(capacidad.getInt("id"), competencia.getInt("id"));
                    List<IndicadorAprendizaje> indicadoresJson = objComCap.getCap().getIndicadores();
                    numIndicadores += indicadoresJson.size();
                    CompetenciasUnidadDidactica comUniDi = new CompetenciasUnidadDidactica(objUni,objComCap,"");
                    try{
                        CompetenciasUnidadDidactica comAux = unidadDao.buscarCompetenciaUnidad(idUnidad,
                                comUniDi.getCompetenciaCapacidad().getCom().getComId(),
                                comUniDi.getCompetenciaCapacidad().getCap().getCapId());
                        if(comAux == null){
                            // no existe la competencia
                            unidadDao.registrarCapacidades(comUniDi);
                            capacidad.put("indicadores", new JSONArray(EntityUtil.listToJSONString(
                                    new String[]{"indAprId","nomInd"},
                                    new String[]{"id","nom"},
                                    indicadoresJson
                            )));
                        }
                        //unidadDao.registrarCapacidades(comUniDi);
                        /*capacidad.put("indicadores", new JSONArray(EntityUtil.listToJSONString(
                                    new String[]{"indAprId","nomInd"},
                                    new String[]{"id","nom"},
                                    indicadoresJson
                            )));
                        * */
                        else{
                            //ya existe la competencia
                            if(comAux.getEstReg() == 'E'){
                                //La capacidad fue eliminada entonces solo cambiamos el estado a 'A'
                                comAux.setEstReg('A');
                                unidadDao.registrarCapacidades(comAux);
                                capacidad.put("indicadores", new JSONArray(EntityUtil.listToJSONString(
                                        new String[]{"indAprId","nomInd"},
                                        new String[]{"id","nom"},
                                        indicadoresJson
                                )));
                            }else{
                                capacidades.remove(j);
                                numIndicadores -= indicadoresJson.size();
                                --j;
                            }
                        }
                    }catch (Exception e){
                        logger.log(Level.SEVERE,"Duplicidad de clave",e);
                        //la clave es duplicada verificamos que
                        /*capacidades.remove(j);
                        --j;*/
                    }
                }
                competencia.put("numInd",numIndicadores);
            }
            return WebResponse.crearWebResponseExito("Se registro correctamente las capacidades a la unidad",competencias);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarCapacidad",e);
            return WebResponse.crearWebResponseError("Error al registrar las capacidades a la unidad");
        }

    }
}
