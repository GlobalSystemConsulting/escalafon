package com.dremo.ucsm.gsc.sigesmed.logic.maestro.anecdotario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario.AnecdotarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 21/12/2016.
 */
public class ListarEstudiantesTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarEstudiantesTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idOrg = data.getInt("org");
        int idGrad = data.getInt("gra");
        String secc = data.getString("sec");
        return listarEstudiantes(idOrg,idGrad,secc);
    }

    private WebResponse listarEstudiantes(int idOrg, int idGrad, String secc) {
        try{
            AnecdotarioDao anecDao = (AnecdotarioDao) FactoryDao.buildDao("maestro.anecdotario.AnecdotarioDao");
            List<Estudiante> estudiantes = anecDao.listarEstudiantes(idOrg,idGrad,secc.charAt(0));
            JSONArray estudiantesJSON = new JSONArray();
            for(Estudiante estudiante : estudiantes){
                JSONObject jsonEstudiante = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"perId","dni","nom","apePat","apeMat","fecNac"},
                        new String[]{"id","dni","nom","pat","mat","nac"},
                        estudiante.getPersona()
                ));
                estudiantesJSON.put(jsonEstudiante);
            }
            return WebResponse.crearWebResponseExito("se listo correctamente",estudiantesJSON);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarEstudiantes",e);
            return WebResponse.crearWebResponseError("no se puede listar los estudiantes");
        }
    }
}
