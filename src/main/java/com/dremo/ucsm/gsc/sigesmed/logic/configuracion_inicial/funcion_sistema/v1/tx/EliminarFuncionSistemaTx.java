/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.funcion_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FuncionSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author Administrador
 */
public class EliminarFuncionSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int funcionID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            funcionID = requestData.getInt("funcionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        FuncionSistemaDao funcionDao = (FuncionSistemaDao)FactoryDao.buildDao("FuncionSistemaDao");
        try{
            funcionDao.delete(new FuncionSistema(funcionID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar la funcion del Sistema \n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar la funcion del Sistema ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("La funcion del Sistema se elimino correctamente");
        //Fin
    }
}
