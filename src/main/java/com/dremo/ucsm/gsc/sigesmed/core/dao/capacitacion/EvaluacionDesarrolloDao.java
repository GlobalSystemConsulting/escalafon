package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionDesarrollo;
import java.util.List;
import java.util.Map;

public interface EvaluacionDesarrolloDao extends  GenericDao<EvaluacionDesarrollo> {
    EvaluacionDesarrollo buscarPorId(int desCod, int perId);
    List<EvaluacionDesarrollo> buscarParticipantes(int perId);
    List<Double> obtenerParaCertificacion(int codSed, int perId, double notMin, Character tipDes);
    Map<Character, Double> contarReporte(int codSed, int perId);
    List<Object []> reporteParticipante(int codSed, int perId, int temCurCapId);
}
