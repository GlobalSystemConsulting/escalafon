package com.dremo.ucsm.gsc.sigesmed.util;

import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonObject;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class JSONUtil {

    private final static String URL_DEPARTAMENTOS = ServicioREST.PATH_SIGESMED + "/recursos/json/departamentos.json";
    private final static String URL_PROVINCIAS = ServicioREST.PATH_SIGESMED + "/recursos/json/provincias.json";
    private final static String URL_DISTRITOS = ServicioREST.PATH_SIGESMED + "/recursos/json/distritos.json";

    public static String[] getUbigeoLocation(String ubigeo) {
        JsonParser parser = new JsonParser();
        String[] response = new String[3];
        response[0] = "";
        response[1] = "";
        response[2] = "";
        if (ubigeo.length() != 6) {
            return response;
        }

        String ubiDptoCod = "";
        String ubiProvCod = "";
        String ubiCod;
        try {
            Object objDpto = parser.parse(new FileReader(URL_DEPARTAMENTOS));
            JsonArray jsonArrayDpto = (JsonArray) objDpto;

            for (JsonElement element : jsonArrayDpto) {
                JsonObject jsonObject = element.getAsJsonObject();
                ubiCod = jsonObject.get("codigo_ubigeo").getAsString();

                if (ubiCod.equals(ubigeo.substring(0, 2))) {
                    response[0] = jsonObject.get("nombre_ubigeo").getAsString();
                    ubiDptoCod = jsonObject.get("id_ubigeo").getAsString();
                    break;
                }
            }
            
            if(!ubiDptoCod.equals(""))
            {
                Object objProv = parser.parse(new FileReader(URL_PROVINCIAS));
                JsonObject prov = (JsonObject) objProv;
                JsonArray jsonArrayProv = prov.getAsJsonArray(ubiDptoCod);

                for (JsonElement element : jsonArrayProv) {
                    JsonObject jsonObject = element.getAsJsonObject();
                    ubiCod = jsonObject.get("codigo_ubigeo").getAsString();

                    if (ubiCod.equals(ubigeo.substring(2, 4))) {
                        response[1] = jsonObject.get("nombre_ubigeo").getAsString();
                        ubiProvCod = jsonObject.get("id_ubigeo").getAsString();
                        break;
                    }
                }
                if(!ubiProvCod.equals(""))
                {
                    Object objDist = parser.parse(new FileReader(URL_DISTRITOS));
                    JsonObject dist = (JsonObject) objDist;
                    JsonArray jsonArrayDist = dist.getAsJsonArray(ubiProvCod);

                    for (JsonElement element : jsonArrayDist) {
                        JsonObject jsonObject = element.getAsJsonObject();
                        ubiCod = jsonObject.get("codigo_ubigeo").getAsString();

                        if (ubiCod.equals(ubigeo.substring(4, 6))) {
                            response[2] = jsonObject.get("nombre_ubigeo").getAsString();
                            break;
                        }
                    }
                }
            }

        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        return response;
    }

}
