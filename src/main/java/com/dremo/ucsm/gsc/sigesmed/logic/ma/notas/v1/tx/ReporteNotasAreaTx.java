package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.NotaEvaluacionIndicadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.GradoIEEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 03/02/17.
 */
public class ReporteNotasAreaTx implements ITransaction{
    private Logger logger = Logger.getLogger(ReporteNotasAreaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idUsr = data.getInt("usr");
        int idOrg = data.getInt("org");
        int idPlan = data.optInt("plan");
        int idPeriodo = data.getInt("per");
        int idArea = data.getInt("are");
        int idGrado = data.getInt("gra");
        String idSecc = data.getString("secc");
        return generarReporteAcademico(idUsr,idOrg,idPlan,idPeriodo,idArea,idGrado,idSecc);
    }

    private WebResponse generarReporteAcademico(int idUsr, int idOrg, int idPlan, int idPeriodo, int idArea, int idGrado, String idSecc) {
        try{

            String b64 = generarReporteb64(idUsr,idOrg,idPlan,idPeriodo,idArea,idGrado,idSecc);
            return WebResponse.crearWebResponseExito("Se registro el reporte",new JSONObject().put("b64",b64));
        }catch (Exception e){
            logger.log(Level.SEVERE,"generarReporte",e);
            return WebResponse.crearWebResponseError("No se puede generar el reporte");
        }
    }

    private String generarReporteb64(int idUsr, int idOrg, int idPlan, int idPeriodo, int idArea, int idGrado, String idSecc)
            throws Exception{
        Mitext mitext = new Mitext();

        NotaEvaluacionIndicadorDao notaDao = (NotaEvaluacionIndicadorDao) FactoryDao.buildDao("ma.NotaEvaluacionIndicadorDao");
        List<GradoIEEstudiante> estudiantesGrado = notaDao.listarEstudiantesGradoActual(idOrg, idGrado, idSecc.charAt(0));

        //buscamos las competencias de
        PlanEstudiosDao planDao = (PlanEstudiosDao) FactoryDao.buildDao("mech.PlanEstudiosDao");
        RegistroAuxiliarDao regisDao = (RegistroAuxiliarDao) FactoryDao.buildDao("ma.RegistroAuxiliarDao");

        List<CompetenciaAprendizaje> competencias = regisDao.listarCompetenciasPeriodo(idPeriodo, idArea, idOrg, idUsr, idGrado,
                idPlan == -1 ? planDao.buscarVigentePorOrganizacion(idOrg).getPlaEstId() : idPlan);
        //listamos los datos de los indicadores dentro de las competencias
        Map<CompetenciaAprendizaje,List<IndicadorAprendizaje>> indicadoresAprendizajes = new HashMap<>();
        //listar l
        return null;
    }
}
