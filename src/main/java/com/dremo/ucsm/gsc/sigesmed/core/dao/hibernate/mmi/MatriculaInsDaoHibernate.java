package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.GradoMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.GradoSeccionPlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.MatriculaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PlanNivelMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Vacantes;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;

public class MatriculaInsDaoHibernate extends GenericQueryDaoHibernate {

    public MatriculaInsDaoHibernate() {
        super();
    }

    public void CambioSeccion(int nivId, int jorEscId, char turId, int graId,
            char secAct, char secId, int plaNivId, int estId, int orgDesId,
            int usuMod, String fecMod) {

        String sql
                = "UPDATE pedagogico.grado_ie_estudiante AS graie\n"
                + "SET sec_id = '" + secId + "', usu_mod = " + usuMod + ", fec_mod= '" + fecMod + "'\n"
                + "FROM pedagogico.matricula AS mat\n"
                + "INNER JOIN institucional.plan_nivel AS pln ON pln.pla_niv_id = mat.pla_niv_id\n"
                + "INNER JOIN jornada_escolar AS joe ON joe.jor_esc_id = pln.jor_esc_id\n"
                + "INNER JOIN organizacion AS org ON org.org_id = mat.org_des_id\n"
                + "WHERE  mat.act = true AND  mat.est_reg != 'E' AND mat.org_des_id = " + orgDesId + " AND\n"
                + "joe.niv_id = " + nivId + " AND pln.jor_esc_id = " + jorEscId + " AND pln.tur_id = '" + turId + "' AND  mat.gra_id = " + graId + " AND  \n"
                + "mat.mat_id = graie.mat_id AND mat.sec_id = '" + secAct + "' AND mat.est_id =  " + estId + ";\n"
                + "\n"
                + "UPDATE pedagogico.matricula AS mymat\n"
                + "SET sec_id = '" + secId + "', usu_mod = " + usuMod + ", fec_mod= '" + fecMod + "'\n"
                + "FROM pedagogico.grado_ie_estudiante AS graie\n"
                + "INNER JOIN pedagogico.matricula AS mat ON mat.mat_id = graie.mat_id\n"
                + "INNER JOIN institucional.plan_nivel AS pln ON pln.pla_niv_id = mat.pla_niv_id\n"
                + "INNER JOIN jornada_escolar AS joe ON joe.jor_esc_id = pln.jor_esc_id\n"
                + "INNER JOIN organizacion AS org ON org.org_id = mat.org_des_id\n"
                + "WHERE  mat.act = true AND  mat.est_reg != 'E' AND mat.org_des_id = " + orgDesId + " AND\n"
                + "joe.niv_id = " + nivId + " AND pln.jor_esc_id = " + jorEscId + " AND pln.tur_id = '" + turId + "' AND  mat.gra_id = " + graId + " AND  \n"
                + "mymat.mat_id = graie.mat_id AND mat.sec_id = '" + secAct + "' AND mat.est_id = " + estId + ";\n"
                + "\n"
                + "UPDATE grado_seccion_plan_nivel\n"
                + "SET  num_alu_mat= num_alu_mat -1,  fec_mod= '" + fecMod + "', usu_mod= " + usuMod + "\n"
                + "WHERE gra_id = " + graId + " AND sec_id = '" + secAct + "' AND pla_niv_id = " + plaNivId + " AND est_reg = 'A';\n"
                + "\n"
                + "UPDATE grado_seccion_plan_nivel\n"
                + "SET  num_alu_mat= num_alu_mat +1,  fec_mod= '" + fecMod + "', usu_mod= " + usuMod + "\n"
                + "WHERE gra_id = " + graId + " AND sec_id = '" + secId + "' AND pla_niv_id = " + plaNivId + " AND est_reg = 'A';\n";
        
        ExecuteSqlQuery(sql);
    }

    public void EliminarGradosSeccionPasados(int orgId, int year) {
        String sql = "UPDATE grado_seccion_plan_nivel AS gspn \n"
                + "SET est_reg= 'E' \n"
                + "FROM institucional.plan_estudios AS pe \n"
                + "INNER JOIN institucional.plan_nivel AS pn ON pe.pla_est_id = pn.pla_est_id \n"
                + "WHERE pe.\"org_id\" = " + orgId + " AND pe.\"año_esc\" = '" + year + "-01-01' AND pn.pla_niv_id = gspn.pla_niv_id";

        ExecuteSqlQuery(sql);
    }

    public List<Vacantes> getNumVacantes(int orgId, int year) {

        String sql = "SELECT pn.pla_niv_id AS ele0, nv.niv_id AS ele1, nv.nom AS ele2 , pn.jor_esc_id AS ele3, \n"
                + "je.nom AS ele4, pn.tur_id AS ele5, tr.nom AS ele6, gr.gra_id AS ele7, gr.nom AS ele8, \n"
                + "ma.num_sec AS ele9, ma.num_alu AS ele10, je.abr as ele11, pn.des AS ele12\n"
                + "FROM institucional.plan_estudios AS pe\n"
                + "INNER JOIN institucional.plan_nivel AS pn ON pe.pla_est_id = pn.pla_est_id\n"
                + "INNER JOIN institucional.meta_atencion AS ma ON ma.pla_niv_id = pn.pla_niv_id\n"
                + "INNER JOIN institucional.turno AS tr ON tr.tur_id = pn.tur_id\n"
                + "INNER JOIN jornada_escolar AS je ON je.jor_esc_id = pn.jor_esc_id\n"
                + "INNER JOIN nivel AS nv ON nv.niv_id = je.niv_id\n"
                + "INNER JOIN grado AS gr ON gr.gra_id = ma.gra_id\n"
                + "WHERE pe.\"org_id\" = " + orgId + " AND pe.\"año_esc\" = '" + year + "-01-01'\n"
                + "ORDER BY nv.niv_id, je.jor_esc_id, tr.tur_id, gr.gra_id";

        List<Object[]> query = sqlQuery(sql);
        List<Vacantes> result = new ArrayList<>();
        for (Object[] element : query) {
            Vacantes temp = new Vacantes();
            temp.setPlanNivId(Integer.parseInt(element[0].toString()));
            temp.setNivId(Integer.parseInt(element[1].toString()));
            temp.setNivNom(element[2].toString());
            temp.setJorEscId(Integer.parseInt(element[3].toString()));
            temp.setJorEscNom(element[4].toString());
            temp.setTurId(element[5].toString().charAt(0));
            temp.setTurNom(element[6].toString());
            temp.setGraId(Integer.parseInt(element[7].toString()));
            temp.setGraNom(element[8].toString());
            temp.setNumSec(Integer.parseInt(element[9].toString()));
            temp.setNumEstxSec(Integer.parseInt(element[10].toString()));
            temp.setNivNomCom(element[11].toString() + ": " + element[12].toString());
            result.add(temp);
        }
        return result;
    }

    public void generateGradoSeccionPlanEstudios(int orgId, int year) {
        List<Vacantes> myVacantes = getNumVacantes(orgId, year);
        GenericMMIDaoHibernate<GradoSeccionPlanNivel> hb = new GenericMMIDaoHibernate<>();

        //inhabilita otras secciones previas a la matricula
        EliminarGradosSeccionPasados(orgId, year);

        Object count;

        // 'A' =  65
        for (Vacantes itm : myVacantes) {
            for (int i = 65; i < 65 + itm.getNumSec(); i++) {
                GradoSeccionPlanNivel temp = new GradoSeccionPlanNivel();

                count = hb.llave(GradoSeccionPlanNivel.class, "graSecPlaEstId");
                if (count != null) {
                    temp.setGraSecPlaEstId((int) count + 1);
                } else {
                    temp.setGraSecPlaEstId(1);
                }

                GradoMMI gm = new GradoMMI();
                gm.setGraId(itm.getGraId());
                temp.setGrado(gm);

                Seccion se = new Seccion();
                se.setSedId((char) i);
                temp.setSeccion(se);

                PlanNivelMMI pn = new PlanNivelMMI();
                pn.setPlaNivId(itm.getPlanNivId());
                temp.setPlanNivel(pn);

                temp.setNumAluMat(0);
                temp.setNumAluMax(itm.getNumEstxSec());
                temp.setEstReg("A");

                hb.insert(temp);
            }
        }
    }

    public List<Object[]> getNominaEstudiantes(int orgId, int nivelId, int jornadaEscId, char turnoId, int gradoId, char secId) {
        String sql = "SELECT "
                + "per.dni AS ele0, est.cod_est AS ele1, per.ape_pat AS ele2, per.ape_mat AS ele3, per.nom AS ele4, per.fec_nac AS ele5, per.sex AS ele6, "
                + "mat.mat_sit AS ele7, dtn.pai_id AS ele8, per.len_mat AS ele9, per.len_seg AS ele10, dtn.nac_reg AS ele11, est.tip_dis AS ele12, "
                + "org.cod AS ele13, org.nom AS ele14\n"
                + "FROM pedagogico.matricula AS mat\n"
                + "INNER JOIN pedagogico.estudiante AS est ON est.per_id = mat.est_id\n"
                + "INNER JOIN pedagogico.persona AS per ON per.per_id = est.per_id\n"
                + "INNER JOIN pedagogico.datos_nacimiento AS dtn ON dtn.per_id = per.per_id\n"
                + "INNER JOIN institucional.plan_nivel AS pln ON pln.pla_niv_id = mat.pla_niv_id\n"
                + "INNER JOIN jornada_escolar AS joe ON joe.jor_esc_id = pln.jor_esc_id\n"
                + "INNER JOIN organizacion AS org ON org.org_id = est.org_id\n"
                + "WHERE  mat.act = true AND  mat.est_reg != 'E' AND mat.org_des_id = " + orgId + " AND\n"
                + "mat.sec_id = '" + secId + "' AND  mat.gra_id = " + gradoId + " AND\n"
                + "pln.tur_id = '" + turnoId + "' AND pln.jor_esc_id = " + jornadaEscId + " AND\n"
                + "joe.niv_id = " + nivelId + "\n"
                + "ORDER BY per.ape_pat;";
        List<Object[]> query = sqlQuery(sql);
        return query;
    }

    public List<Object[]> getNumVacanteslibres(int orgId) {
        String sql = "SELECT mt.jor_esc_id, mt.gra_id, gr.nom, mt.tur_id, mt.sec_id, COUNT(*)\n"
                + "FROM pedagogico.matricula AS mt\n"
                + "INNER JOIN grado AS gr ON gr.gra_id = mt.gra_id\n"
                + "WHERE mt.org_des_id = " + orgId + " AND mt.act = true \n"
                + "GROUP BY mt.gra_id, mt.sec_id, gr.nom, mt.jor_esc_id, mt.tur_id\n"
                + "ORDER BY mt.gra_id,  mt.tur_id, mt.sec_id";

        List<Object[]> query = sqlQuery(sql);
        return query;
    }

    public long getMatIdByEstId(long estId) {
        String sql = "SELECT mat_id, est_reg\n"
                + "  FROM pedagogico.matricula\n"
                + "  WHERE est_id = " + estId + " AND act = true AND est_reg != 'E';";

        List<Object[]> query = sqlQuery(sql);

        long rpt = -1;

        for (Object[] element : query) {
            rpt = (long) ((BigInteger) element[0]).intValue();
        }

        return rpt;
    }

    public MatriculaMMI load(long key) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String hql;
        MatriculaMMI myMatricula = null;
        Query query;
        try {
            hql = "FROM MatriculaMMI WHERE matId = :hqlKey";
            query = session.createQuery(hql);
            query.setLong("hqlKey", key);
            query.setMaxResults(1);
            myMatricula = (MatriculaMMI) query.uniqueResult();
        } catch (ObjectNotFoundException ex) {
            throw ex;
        } finally {
            session.close();
        }
        return myMatricula;
    }

    public PlanNivelMMI loadPlanNivel(Integer key) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String hql;
        PlanNivelMMI myMatricula = null;
        Query query;
        try {
            hql = "FROM PlanNivelMMI WHERE plaNivId = :hqlKey";
            query = session.createQuery(hql);
            query.setInteger("hqlKey", key);
            query.setMaxResults(1);
            myMatricula = (PlanNivelMMI) query.uniqueResult();
        } catch (ObjectNotFoundException ex) {
            throw ex;
        } finally {
            session.close();
        }
        return myMatricula;
    }
}
