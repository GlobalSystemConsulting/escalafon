package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;


import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 18/01/2017.
 */
public class ListarPeriodosPlanTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarPeriodosPlanTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();

        int idPlan = data.optInt("plan",-1);
        int idOrg = data.getInt("org");
        int idNivel = data.getInt("niv");
        return listarNiveles(idPlan,idOrg,idNivel);
    }

    private WebResponse listarNiveles(int idPlan,int idOrg, int idNivel) {
        try{
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            PlanEstudiosDao planDao = (PlanEstudiosDao) FactoryDao.buildDao("mech.PlanEstudiosDao");
            UnidadDidacticaDao unidDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");

            PlanEstudios plan = idPlan == -1 ? planDao.buscarVigentePorOrganizacion(idOrg) : progDao.buscarPlanEstudios(idPlan);
            Periodo periodo = progDao.listarPeriodoPlanEstudio(idNivel,plan.getPlaEstId());
            List<PeriodosPlanEstudios> periodos = unidDao.listarPeridosPLanEstudios(plan.getPlaEstId(),idNivel,periodo.getPerId());
            JSONArray jsonPeriodos = new JSONArray();
            for(PeriodosPlanEstudios periodoPlan : periodos){
                JSONObject jsonPeriodo = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"perPlaEstId","fecIni","fecFin","eta"},
                        new String[]{"id","ini","fin","eta"},
                        periodoPlan
                ));
                jsonPeriodo.put("nom", periodoPlan.getPeriodo().getNom() +" "+periodoPlan.getEta());
                jsonPeriodos.put(jsonPeriodo);
            }
        return WebResponse.crearWebResponseExito("Se listo correctamente",jsonPeriodos);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarNiveles",e);
            return WebResponse.crearWebResponseError("No se puede listar los datos");
        }
    }
}
