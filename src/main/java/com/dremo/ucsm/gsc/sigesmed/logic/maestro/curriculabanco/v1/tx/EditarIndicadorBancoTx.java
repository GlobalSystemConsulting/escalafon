package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/10/2016.
 */
public class EditarIndicadorBancoTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EditarIndicadorBancoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return editarIndicador(data);
    }

    private WebResponse editarIndicador(JSONObject data) {
        try{
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            IndicadorAprendizaje indicador = indicadorDao.buscarPorId(data.getInt("cod"));
            indicador.setNomInd(data.optString("nom", indicador.getNomInd()));
            indicador.setEva(data.optBoolean("des", indicador.getEva()));

            indicador.setFecMod(new Date());

            indicadorDao.update(indicador);
            return WebResponse.crearWebResponseExito("Error al realizar el registro", WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarCapacitacion",e);
            return WebResponse.crearWebResponseError("Error al realizar el registro",e.getMessage());
        }
    }
}
