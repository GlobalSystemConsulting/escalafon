package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 01/10/16.
 */
public class BuscarPersonaParaComisionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(BuscarPersonaParaComisionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonData= (JSONObject)wr.getData();
        String dni = jsonData.optString("dni");
        String apep = jsonData.optString("apep");
        String apem = jsonData.optString("apem");
        String opt = jsonData.optString("opt","");
        int idOrg = jsonData.optInt("org");
        switch(opt){
            case "pre": return buscarPersonaParaPresidente(dni,apep,apem,idOrg);
            default:return buscarPersona(dni,apep,apem);
        }
        
    }
    private WebResponse buscarPersona(String dni, String apep, String apem){
        try{
            PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("PersonaDao");
            if(dni != null && !dni.equals("")){
                Persona persona = personaDao.buscarPorDNI(dni);
                if(persona == null){
                    return WebResponse.crearWebResponseError("No se encontro la persona indicada",WebResponse.BAD_RESPONSE);
                }else{
                    JSONArray jsonResult = new JSONArray();
                    jsonResult.put(new JSONObject(EntityUtil.objectToJSONString(new String[]{"dni", "nom", "apePat", "apeMat"},
                            new String[]{"dni", "nom", "apep", "apem"},persona)));
                    return WebResponse.crearWebResponseExito("Resultado de busqueda",WebResponse.OK_RESPONSE).setData(jsonResult);
                }
            }else {
                List<Persona> personas = personaDao.buscarPorApellido(apep,apem);
                if(personas != null && personas.size() > 0){
                    return WebResponse.crearWebResponseExito("Resultado de busqueda",WebResponse.OK_RESPONSE).setData(
                            new JSONArray(EntityUtil.listToJSONString(new String[]{"dni", "nom", "apePat", "apeMat"},
                                    new String[]{"dni", "nom", "apep", "apem"},personas)));
                }
                else{
                    return WebResponse.crearWebResponseError("No se encontraron resultados",WebResponse.BAD_RESPONSE);
                }
            }

        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPersona",e);
            return WebResponse.crearWebResponseError("Error al buscar persona",WebResponse.BAD_RESPONSE);
        }
    }
    
     private WebResponse buscarPersonaParaPresidente(String dni, String apep, String apem, int org){
        try{
            PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("PersonaDao");
            if(dni != null && !dni.equals("")){
                Persona persona = personaDao.buscarPorOrganizacionConUsuario(dni,org);
                if(persona == null){
                    return WebResponse.crearWebResponseError("La persona buscada no es usuario de SIGESMED",WebResponse.BAD_RESPONSE);
                }else{
                    JSONArray jsonResult = new JSONArray();
                    jsonResult.put(new JSONObject(EntityUtil.objectToJSONString(new String[]{"dni", "nom", "apePat", "apeMat"},
                            new String[]{"dni", "nom", "apep", "apem"},persona)));
                    return WebResponse.crearWebResponseExito("Resultado de busqueda",WebResponse.OK_RESPONSE).setData(jsonResult);
                }
            }else {
                List<Persona> personas = personaDao.buscarPorOrganizacionConUsuario(apep,apem,org);
                if(personas != null && personas.size() > 0){
                    return WebResponse.crearWebResponseExito("Resultado de busqueda",WebResponse.OK_RESPONSE).setData(
                            new JSONArray(EntityUtil.listToJSONString(new String[]{"dni", "nom", "apePat", "apeMat"},
                                    new String[]{"dni", "nom", "apep", "apem"},personas)));
                }
                else{
                    return WebResponse.crearWebResponseError("No se encontraron resultados",WebResponse.BAD_RESPONSE);
                }
            }
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarPersona",e);
            return WebResponse.crearWebResponseError("Error al buscar persona",WebResponse.BAD_RESPONSE);
        }
    }
}
