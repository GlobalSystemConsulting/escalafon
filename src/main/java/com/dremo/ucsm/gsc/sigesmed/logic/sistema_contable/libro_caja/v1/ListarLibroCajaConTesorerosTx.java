/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Tesorero;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ListarLibroCajaConTesorerosTx implements ITransaction{
    
     @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
        
        JSONObject requestData = (JSONObject)wr.getData();
        int organizacionID = requestData.getInt("organizacionID");
        
        List<LibroCaja> libros=null;      
        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
       
       
        try {
            libros= libroDao.listarLibroCajaConTesoreros(organizacionID);
            
        } catch (Exception e) {
            System.out.println("No se pudo Listar los Libros\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Libros", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        
        for(int i=0;i<libros.size();i++ ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("libroID",libros.get(i).getLibCajId());
            oResponse.put("year",libros.get(i).getFecApe().getYear());          
            oResponse.put("descripcion",libros.get(i).getNom());
            oResponse.put("director",libros.get(i).getPersona().getNombrePersona());
            oResponse.put("fechaA",libros.get(i).getFecApe()); 
            oResponse.put("fechaC",libros.get(i).getFecCie());
            oResponse.put("saldoI",libros.get(i).getSalApe());
            oResponse.put("saldoA",libros.get(i).getSalAct());
            oResponse.put("estado",""+libros.get(i).getEstReg());
            
       
           
         
          /***  if(libros.get(i).getTesoreros()!=null){
                Set<Tesorero> tesoreros = libros.get(i).getTesoreros();                  
                JSONArray aDetalle = new JSONArray();
                for( Tesorero tesorero:tesoreros ){
                    JSONObject oTesorero = new JSONObject();
                    oTesorero.put("tesorero",tesorero.getPersona().getApePat()+" "+tesorero.getPersona().getApeMat()+", "+tesorero.getPersona().getNom());
                    oTesorero.put("fechaI",tesorero.getFecIni());
                    oTesorero.put("fechaC",""+tesorero.getFecFin());
                    oTesorero.put("resolucion",tesorero.getNumRes());                    
                    aDetalle.put(oTesorero);
                }
                oResponse.put("tesoreros",aDetalle);
            }            
            else{
                JSONArray aDetalle = new JSONArray();
                oResponse.put("tesoreros",aDetalle);
            }***/
                                                
            miArray.put(oResponse);
        }
                
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

