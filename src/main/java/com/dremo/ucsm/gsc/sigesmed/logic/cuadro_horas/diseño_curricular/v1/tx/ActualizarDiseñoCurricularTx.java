/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseño_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DiseñoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DiseñoCurricular;
import java.util.Date;

/**
 *
 * @author abel
 */
public class ActualizarDiseñoCurricularTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        DiseñoCurricular diseño = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int organizacionID = requestData.getInt("organizacionID");
            int diseñoID = requestData.getInt("diseñoID");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String resolucion = requestData.getString("resolucion");
            String tipo = requestData.getString("tipo");
            String estado = requestData.getString("estado");
            diseño = new DiseñoCurricular(diseñoID, nombre, descripcion,resolucion,tipo.charAt(0), organizacionID, new Date(), new Date(), wr.getIdUsuario(), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el diseño curricular, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        DiseñoCurricularDao diseñoDao = (DiseñoCurricularDao)FactoryDao.buildDao("mech.DiseñoCurricularDao");
        try{
            diseñoDao.update(diseño);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar el diseño curricular\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el diseño curricular", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Diseño curricular se actualizo correctamente");
        //Fin
    }
    
}
