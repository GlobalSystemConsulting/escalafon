/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.std.EstadoExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EstadoExpediente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

/**
 *
 * @author abel
 */
public class InsertarEstadoExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        EstadoExpediente estadoExpediente = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String estado = requestData.getString("estado");
            
            estadoExpediente = new EstadoExpediente(0, nombre, descripcion, new Date(),wr.getIdUsuario(), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos" );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        EstadoExpedienteDao areaDao = (EstadoExpedienteDao)FactoryDao.buildDao("std.EstadoExpedienteDao");
        try{
            areaDao.insert(estadoExpediente);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar el estado de expediente\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el estado de expediente", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("estadoExpedienteID",estadoExpediente.getEstExpId());
        oResponse.put("fecha",estadoExpediente.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro de Estado Expediente se realizo correctamente", oResponse);
        //Fin
    }
    
}
