/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.MontoListaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ListarNivelesTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarMontoTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        return listarNiveles();
    }
    
    private WebResponse listarNiveles() {
       MontoListaDao montoListaDao = (MontoListaDao) FactoryDao.buildDao("ma.MontoListaDao");
       try{
           List<Nivel> niveles = montoListaDao.listarNiveles();
           
           String strjson = EntityUtil.listToJSONString(new String[]{"nivId","nom"},new String[]{"id","nom"},niveles);
           JSONArray array = new JSONArray(strjson);
           
           /*for(Nivel niv: niveles){
            
            String strjson = EntityUtil.objectToJSONString(new String[]{"nivId","nom"},new String[]{"id","nom"},niv);
            JSONObject jsonNivel = new JSONObject(strjson);
     
            array.put(jsonNivel);
           }*/
           
           return WebResponse.crearWebResponseExito("Exito al listar los niveles",array);
       }catch (Exception e){
           logger.log(Level.SEVERE,"listarNiveles",e);
            return WebResponse.crearWebResponseError("Error al listar los niveles",WebResponse.BAD_RESPONSE);
        }      
    }
}
