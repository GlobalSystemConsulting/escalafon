/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.cese.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CeseDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cese;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class AgregarCeseTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(AgregarCeseTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        Cese cese = null;
        
        Integer ficEscId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            ficEscId = requestData.getInt("ficEscId");
            
            String entEmi = requestData.getString("entEmi");
            int tipDocId = requestData.getInt("tipDocId");
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = null;
            if(requestData.getString("fecDoc").length() > 0){
                fecDoc = sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            }
            
            int jorLabId = requestData.getInt("jorLabId");
            Date fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecFin = null;
            if(requestData.getString("fecFin").length() > 0){
                fecFin = sdi.parse(requestData.getString("fecFin").substring(0, 10));
            }
            
            String motCes = requestData.getString("motCes");
            int orgiId = requestData.getInt("orgiId");
            int carId = requestData.getInt("carId");
            int catId = requestData.getInt("catId");
            
            char tipCes = requestData.getString("tipCes").charAt(0);
            int aniosPro = requestData.getInt("aniosPro");
            int mesesPro = requestData.getInt("mesesPro");
            int diasPro = requestData.getInt("diasPro");
            
            cese = new Cese(entEmi, numDoc, fecDoc, jorLabId, motCes, wr.getIdUsuario(), new Date(), 'A', new FichaEscalafonaria(ficEscId), fecIni, fecFin, tipDocId);
                    
            cese.setOrgiActId(orgiId);
            cese.setCarActId(carId);
            cese.setCatActId(catId);
            
            cese.setTipIntId(tipCes);
            cese.setAnioPro(aniosPro);
            cese.setMesPro(mesesPro);
            cese.setDiaPro(diasPro);
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo cese",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        CeseDao ceseDao = (CeseDao) FactoryDao.buildDao("se.CeseDao");
        try {
            ceseDao.insert(cese);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego cese", "Detalle: "+ficEscId+" ficha ", wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nuevo cese",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("cesId", cese.getCesId());
        oResponse.put("entEmiRes", null != cese.getEntEmiRes()?cese.getEntEmiRes():"");
        oResponse.put("numDoc", null != cese.getNumDoc() ? cese.getNumDoc():"");
        
        if(cese.getFecDoc()!=null)
            oResponse.put("fecDoc", sdo.format(cese.getFecDoc()));
        else
            oResponse.put("fecDoc", "");
        
        oResponse.put("jorLabId", cese.getJorLabId());
        oResponse.put("motCes", null != cese.getMotCes()?cese.getMotCes():"");
        
        if(cese.getFecIni()!=null)
            oResponse.put("fecIni", sdo.format(cese.getFecIni()));
        else
            oResponse.put("fecIni", "");
        
        if(cese.getFecFin()!=null)
            oResponse.put("fecTer", sdo.format(cese.getFecFin()));
        else
            oResponse.put("fecTer", "");
        
        
        oResponse.put("tipDocId", cese.getTipDocId());
        oResponse.put("motCes", null != cese.getMotCes()?cese.getMotCes():"");
        oResponse.put("tipCes", null != cese.getTipIntId()?cese.getTipIntId():"");
        oResponse.put("aniosPro", null != cese.getAnioPro()?cese.getAnioPro():"");
        oResponse.put("mesesPro", null != cese.getMesPro()?cese.getMesPro():"");
        oResponse.put("diasPro", null != cese.getDiaPro()?cese.getDiaPro():"");
        
        
        return WebResponse.crearWebResponseExito("El registro de la interrupción se realizo correctamente", oResponse);
        //Fin
    }
}
