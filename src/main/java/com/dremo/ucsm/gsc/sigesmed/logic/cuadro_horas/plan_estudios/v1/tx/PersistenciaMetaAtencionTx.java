/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.plan_estudios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.MetaAtencion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
/**
 *
 * @author abel
 */
public class PersistenciaMetaAtencionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        MetaAtencion nuevo = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int metaID = requestData.optInt("metaID");
            int planNivelID = requestData.getInt("planNivelID");
            int gradoID = requestData.getInt("gradoID");
            int secciones = requestData.getInt("secciones");
            int alumnos = requestData.getInt("alumnos");
            int horas = requestData.getInt("horas");
            double carga = requestData.getDouble("carga");
            
            nuevo = new MetaAtencion(metaID,secciones,alumnos,horas,carga,planNivelID,gradoID,new Date(),wr.getIdUsuario(),'A');
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo asignar la meta de atencion, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        PlanEstudiosDao planDao = (PlanEstudiosDao)FactoryDao.buildDao("mech.PlanEstudiosDao");
        try{
            planDao.mergeMetaAtencion(nuevo);
        }catch(Exception e){
            System.out.println("No se pudo asignar la meta de atencion\n"+e);
            return WebResponse.crearWebResponseError("No se pudo asignar la meta de atencion", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("metaID",nuevo.getMetAteId());
        return WebResponse.crearWebResponseExito("Se asigno la meta de atencion correctamente", oResponse);
        //Fin
    }    
    
    
}

