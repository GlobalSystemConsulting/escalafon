package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PersonaCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.UsuarioSessionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeElectronico;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class RegistrarMensajeTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(RegistrarMensajeTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            UsuarioSessionCapacitacionDao usuarioSessionCapacitacionDao = (UsuarioSessionCapacitacionDao) FactoryDao.buildDao("capacitacion.UsuarioSessionCapacitacionDao");
            UsuarioSession usuarioSession = usuarioSessionCapacitacionDao.buscarPorPersona(data.getInt("usuPerId"));

            PersonaCapacitacionDao personaCapacitacionDao = (PersonaCapacitacionDao) FactoryDao.buildDao("capacitacion.PersonaCapacitacionDao");
            Persona persona = personaCapacitacionDao.buscarPorId(data.getInt("perId"));
            
            MensajeElectronicoDao mensajeDao = (MensajeElectronicoDao) FactoryDao.buildDao("web.MensajeElectronicoDao");
            
            String contenido = persona.getNom() + " " + persona.getApePat() + " " + persona.getApeMat() + " con DNI " + persona.getDni() + " ha solicitado su inclusión "
                    + "en el curso de capacitación '" + data.getString("curNom") + "' ha llevarse a cado desde " + data.getString("fecIni") + " hasta " + data.getString("fecFin") +
                    "; el correo electrónico de la persona es " + persona.getEmail();
            
            MensajeElectronico mensaje = new MensajeElectronico(0, "SOLICITUD DE CAPACITACIÓN", contenido, "", "", new Date(), data.getInt("usuAut"));
            mensajeDao.enviarMensaje(mensaje, usuarioSession.getUsuSesId());

            return WebResponse.crearWebResponseExito("Los mensajes fueron enviados correctamente", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarMensaje", e);
            return WebResponse.crearWebResponseError("No se pudo registra el mensaje de la persona", WebResponse.BAD_RESPONSE);
        }
    }
}
