package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_masiva.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.Organizacion4NominaDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.JornadaDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaInsDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.JornadaEscolarMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import com.dremo.ucsm.gsc.sigesmed.util.JSONUtil;
import com.dremo.ucsm.gsc.sigesmed.util.MatriculaReporter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.json.JSONException;
import org.json.JSONObject;

public class GenerarNominaMatriculaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String nombreSeccion, periodoLecIni, periodoLecFin;
        int orgId, nivId, jorEscId, graId;
        char turId, secId;

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            orgId = requestData.getInt("orgId");
            nivId = requestData.getInt("nivId");
            jorEscId = requestData.getInt("jorEscId");
            graId = requestData.getInt("graId");

            turId = requestData.getString("turId").charAt(0);
            secId = requestData.getString("secId").charAt(0);

            nombreSeccion = requestData.getString("nombreSeccion");
            periodoLecIni = dateFormat.format(DateUtil.getDate2String(requestData.getString("periodoLecIni")));
            periodoLecFin = dateFormat.format(DateUtil.getDate2String(requestData.getString("periodoLecFin")));

            MatriculaInsDaoHibernate matDao = new MatriculaInsDaoHibernate();
            List<Object[]> estudiantes = matDao.getNominaEstudiantes(orgId, nivId, jorEscId, turId, graId, secId);

            Organizacion4NominaDaoHibernate daoOrg = new Organizacion4NominaDaoHibernate();
            Organizacion myOrganizacion = daoOrg.find4OrgId(orgId);
            Organizacion myPadre = myOrganizacion.getOrganizacionPadre();

            String[] ubicacion = JSONUtil.getUbigeoLocation(myOrganizacion.getUbiCod());
            
            JornadaDaoHibernate daoJor = new JornadaDaoHibernate();
            JornadaEscolarMMI myJornada = daoJor.find4JorEscId(jorEscId);

            MatriculaReporter matRep = new MatriculaReporter();
            matRep.makeNominaMatricula(myOrganizacion, myPadre, nombreSeccion, 
                    periodoLecIni, periodoLecFin, ubicacion[0], ubicacion[1], ubicacion[2], 
                    myJornada.getNom(), orgId, nivId, jorEscId, turId, graId, secId, estudiantes);

            return WebResponse.crearWebResponseExito("Se Genero la Nomina de Matricula");

        } catch (JSONException | NumberFormatException e) {
            return WebResponse.crearWebResponseError("Error al generar Nomina Matricula!");
        }

    }

    public ArrayList<String> getArrayJs(JSONObject request, String nameReq) {
        String row = request.getString(nameReq);
        StringTokenizer str = new StringTokenizer(row.substring(1, row.length() - 1), ",");
        ArrayList<String> temp = new ArrayList<>();

        while (str.hasMoreElements()) {
            String token = str.nextToken();
            if (!token.equals(",")) {
                temp.add(token);
            }
        }

        return temp;
    }
}
