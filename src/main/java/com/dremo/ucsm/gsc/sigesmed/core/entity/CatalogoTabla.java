/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity;

/**
 *
 * @author felipe
 */

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="catalogo_tabla",schema="public")
public class CatalogoTabla implements java.io.Serializable {

    @Id
    @Column(name="cat_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_catalogotabla", sequenceName="catalogo_tabla_cat_id_seq" )
    @GeneratedValue(generator="secuencia_catalogotabla")
    private int catId;
    
    @Column(name="nom_tabla", nullable=false, length=50)
    private String nomTabla;
    
    @Column(name="nom_clase", nullable=false, length=50)
    private String nomClase;
   
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;
    @Column(name="usu_mod", nullable=false)
    private int usuMod;
    @Column(name="est_reg", nullable=false, length=1)
    private char estReg;

    public CatalogoTabla() {
    }

    public CatalogoTabla(int catId) {
        this.catId = catId;
    }

    public CatalogoTabla(int catId, String nomTabla, String nomClase, Date fecMod, int usuMod, char estReg) {
        this.catId = catId;
        this.nomTabla = nomTabla;
        this.nomClase = nomClase;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }

    public CatalogoTabla(String nomTabla, String nomClase, Date fecMod, int usuMod, char estReg) {
        this.nomTabla = nomTabla;
        this.nomClase = nomClase;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }

    public CatalogoTabla(String nomTabla, String nomClase, int usuMod, char estReg) {
        this.nomTabla = nomTabla;
        this.nomClase = nomClase;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getNomTabla() {
        return nomTabla;
    }

    public void setNomTabla(String nomTabla) {
        this.nomTabla = nomTabla;
    }

    public String getNomClase() {
        return nomClase;
    }

    public void setNomClase(String nomClase) {
        this.nomClase = nomClase;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    
}