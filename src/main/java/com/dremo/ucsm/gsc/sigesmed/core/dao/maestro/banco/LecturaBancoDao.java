package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.banco;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.Lectura;

import java.util.List;

/**
 * Created by Administrador on 26/12/2016.
 */
public interface LecturaBancoDao extends GenericDao<Lectura> {
    Lectura buscarLecturaId(int idLec);
    List<Lectura> listarLecturasDocente(int idDoc,int idGrad,Character idSecc,int idArea,int idOrg,int year);
}
