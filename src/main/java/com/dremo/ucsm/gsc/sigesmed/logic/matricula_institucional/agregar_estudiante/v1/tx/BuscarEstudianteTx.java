package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.EstudianteMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.DatosNacimiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Lengua;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class BuscarEstudianteTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        EstudianteMMIDaoHibernate dh = new EstudianteMMIDaoHibernate();
        EstudianteMMI estudianteMmi = null;
        JSONObject requestData = (JSONObject) wr.getData();
        JSONObject datosGenerales = new JSONObject();

        String codigoBusqueda = requestData.getString("codigoBusqueda");
        int tipoBusqueda = requestData.getInt("tipoBusqueda");

        try {
            switch (tipoBusqueda) {
                case 1: //por DNI
                    estudianteMmi = dh.find4Dni(codigoBusqueda);
                    break;
                case 2: //por Codigo de Estudiante
                    estudianteMmi = dh.find4CodEst(codigoBusqueda);
                    break;
                case 3:
                    estudianteMmi = dh.find4PerId(codigoBusqueda);
                    break;
                default:
                    estudianteMmi = dh.find4Dni(codigoBusqueda);
                    break;
            }
            if (estudianteMmi == null) {
                return WebResponse.crearWebResponseError("No se encontro el estudiante! ", "Usuario null");
            }

            /**
             * ObjetoJSON 
             * datosGenerales 
             *  persona 
             *      perId 
             *      lenguaByLenMat 
             *          lenId
             *          lenNom
             *      lenguaByLenSeg
             *          lenId 
             *          lenNom 
             *      nom 
             *      apePat 
             *      apeMat 
             *      dni 
             *      perDir
             *      sex
             *      estCiv 
             *  estudiante 
             *      codEst
             *      datosNacimiento
             *          pais
             *              paiId
             *              paiNom
             *          ubiCod
             *          fecNac
             *          nacReg
             */
            
            JSONObject persona = new JSONObject();
            datosGenerales.put("persona", persona);

            PersonaMMI pe1 = estudianteMmi.getPersona();
            if (pe1 != null) {
                if ((Long) pe1.getPerId() != null) {
                    persona.put("perId", pe1.getPerId());
                }
                
                Lengua le1 = pe1.getLenguaByLenMat();
                if (le1 != null) {
                    JSONObject lenguaByLenMat = new JSONObject();
                    if ((Integer) le1.getLenId() != null) {
                        lenguaByLenMat.put("lenId",""+ le1.getLenId());
                    }
                    persona.put("lenguaByLenMat", lenguaByLenMat);
                }

                Lengua le2 = pe1.getLenguaByLenSeg();
                if (le2 != null) {
                    JSONObject lenguaByLenSeg = new JSONObject();
                    if ((Integer) le2.getLenId() != null) {
                        lenguaByLenSeg.put("lenId",""+ le2.getLenId());
                    }
                    persona.put("lenguaByLenSeg", lenguaByLenSeg);
                }

                if (pe1.getNom() != null) {
                    persona.put("nom", pe1.getNom());
                }
                if (pe1.getApePat() != null) {
                    persona.put("apePat", pe1.getApePat());
                }
                if (pe1.getApeMat() != null) {
                    persona.put("apeMat", pe1.getApeMat());
                }
                if (pe1.getDni() != null) {
                    persona.put("dni", pe1.getDni());
                }
                if (pe1.getPerDir() != null) {
                    persona.put("perDir", pe1.getPerDir());
                }
                if (pe1.getSex() != null) {
                    persona.put("sex", pe1.getSex());
                }
                if (pe1.getEstCiv() != null) {
                    persona.put("estCiv", ""+pe1.getEstCiv().trim());
                }
            }

            JSONObject estudiante = new JSONObject();
            datosGenerales.put("estudiante", estudiante);

            if (estudianteMmi.getCodEst() != null) {
                estudiante.put("codEst", estudianteMmi.getCodEst());
            }
            

            JSONObject datosNacimiento = new JSONObject();
            estudiante.put("datosNacimiento", datosNacimiento);

            DatosNacimiento dna = estudianteMmi.getDatosNacimiento();
            if (dna != null) {
                if (dna.getPais() != null) {
                    JSONObject pais = new JSONObject();
                    if ((Integer) dna.getPais().getPaiId() != null) {
                        pais.put("paiId", ""+ dna.getPais().getPaiId());
                    }
                    datosNacimiento.put("pais", pais);
                }

                if (dna.getUbiCod() != null) {
                    datosNacimiento.put("ubiCod", dna.getUbiCod());
                }

                if (dna.getFecNac() != null) {
                    datosNacimiento.put("fecNac", DateUtil.getString2Date(dna.getFecNac()));
                }
                if (dna.getNacReg() != null) {
                    datosNacimiento.put("nacReg", dna.getNacReg());
                }

            }
        } catch (NumberFormatException | JSONException e) {
            return WebResponse.crearWebResponseError("No se encontro el estudiante! ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Se encontro al estudiante", datosGenerales);
    }

}
