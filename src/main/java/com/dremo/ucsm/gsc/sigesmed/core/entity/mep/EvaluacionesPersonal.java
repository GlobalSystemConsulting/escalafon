/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;

/**
 *
 * @author geank
 */
public class EvaluacionesPersonal {
    private Organizacion organizacion;
    private String nom;
    private String apeMat;
    private String apePat;
    private String traCar;
    private short tieServ;
    private int traId;
    private long evaluaciones;
    private long perId;
    
    public int getTraId() {
        return traId;
    }

    public void setTraId(int traId) {
        this.traId = traId;
    }

    public long getPerId() {
        return perId;
    }

    public void setPerId(long perId) {
        this.perId = perId;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getApePat() {
        return apePat;
    }

    public void setApePat(String apePat) {
        this.apePat = apePat;
    }

    public String getApeMat() {
        return apeMat;
    }

    public void setApeMat(String apeMat) {
        this.apeMat = apeMat;
    }

    public String getTraCar() {
        return traCar;
    }

    public void setTraCar(String traCar) {
        this.traCar = traCar;
    }

    public short getTieServ() {
        return tieServ;
    }

    public void setTieServ(short tieServ) {
        this.tieServ = tieServ;
    }

    public long getEvaluaciones() {
        return evaluaciones;
    }

    public void setEvaluaciones(long evaluaciones) {
        this.evaluaciones = evaluaciones;
    }
    
    @Override
    public String toString(){
        return EntityUtil.objectToJSONString(new String[]{"perId","traId","nom","apePat","apeMat","traCar","evaluaciones"},new String[]{"per","tra","nom","apep","apem","car","eval"},this);//,
    }
}
