/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.familiares.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoParienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoPariente;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarTipoParentescoTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        List<TipoPariente> tipoParientes = null;
        TipoParienteDao tipoParientesDao = (TipoParienteDao)FactoryDao.buildDao("se.TipoParienteDao");
        
        try{
            tipoParientes = tipoParientesDao.listar(TipoPariente.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los tipos de Parientes\n "+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los tipos de Parientes ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoPariente tipoPariente:tipoParientes ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tpaId",tipoPariente.getTpaId());
            oResponse.put("tpaDes",tipoPariente.getTpaDes());                       
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray); 
    }
    
}
