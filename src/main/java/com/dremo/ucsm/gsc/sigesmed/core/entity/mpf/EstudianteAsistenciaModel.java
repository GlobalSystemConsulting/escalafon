package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.*;
import java.util.Date;

/**
 * @author Carlos
 */
public class EstudianteAsistenciaModel implements java.io.Serializable {
    private Long asiId;
    private Date fecAsi;
    private Integer tipAsi;
    private Integer estAsi;
    private Long jusId;
    private String desJus;
    private String docJus;


    public EstudianteAsistenciaModel() {
    }

    public EstudianteAsistenciaModel(Long asiId, Date fecAsi, Integer tipAsi, Integer estAsi, Long jusId, String desJus, String docJus) {
        this.asiId = asiId;
        this.fecAsi = fecAsi;
        this.tipAsi = tipAsi;
        this.estAsi = estAsi;
        this.jusId = jusId;
        this.desJus = desJus;
        this.docJus = docJus;
    }
    
    

    public Long getAsiId() {
        return asiId;
    }

    public void setAsiId(Long asiId) {
        this.asiId = asiId;
    }

    public Date getFecAsi() {
        return fecAsi;
    }

    public void setFecAsi(Date fecAsi) {
        this.fecAsi = fecAsi;
    }

    public Integer getTipAsi() {
        return tipAsi;
    }

    public void setTipAsi(Integer tipAsi) {
        this.tipAsi = tipAsi;
    }

    public Integer getEstAsi() {
        return estAsi;
    }

    public void setEstAsi(Integer estAsi) {
        this.estAsi = estAsi;
    }

    public Long getJusId() {
        return jusId;
    }

    public void setJusId(Long jusId) {
        this.jusId = jusId;
    }

    public String getDesJus() {
        return desJus;
    }

    public void setDesJus(String desJus) {
        this.desJus = desJus;
    }

    public String getDocJus() {
        return docJus;
    }

    public void setDocJus(String docJus) {
        this.docJus = docJus;
    }

    
}
