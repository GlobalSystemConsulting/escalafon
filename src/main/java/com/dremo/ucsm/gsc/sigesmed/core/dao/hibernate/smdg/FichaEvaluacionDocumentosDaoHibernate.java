/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.FichaEvaluacionDocumentosDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.FichaEvaluacionDocumentos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.FichaEvaluacionDocumentos;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class FichaEvaluacionDocumentosDaoHibernate extends GenericDaoHibernate<FichaEvaluacionDocumentos> implements FichaEvaluacionDocumentosDao{
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    @Override
    public List<Object[]> listarFichasxOrganizacion(int orgId){
        List<Object[]> fichas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{            
            Query query = session.createSQLQuery("select f.fev_doc_id, i.ite_nom, f.fev_fec, f.fev_tot, p.pfi_ins_id, p.pfi_nom, pe.nom, pe.ape_mat, pe.ape_pat, c.crg_tra_nom, i.ite_url_des \n" +
                            "from institucional.ficha_evaluacion_documentos f \n" +
                            "join institucional.plantilla_ficha_institucional p on f.pla_id = p.pfi_ins_id \n" +
                            "join institucional.item_file i on i.ite_ide = f.ite_ide \n" +
                            "join trabajador t ON f.fev_esp = t.tra_id \n" +
                            "join pedagogico.persona pe on t.per_id = pe.per_id \n" +
                            "join trabajador_cargo c on t.tra_car = c.crg_tra_ide \n" +
                            "WHERE i.ite_cod_cat = 'A'");  
//                            "WHERE i.ite_ide = " + iteide);  

            fichas = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar las fichas de evaluacion \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las fichas de evaluacion \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return fichas;
    }
    
    @Override
    public List<Object[]> listarFichasxOrganizacion(int orgId, int iteide){
        List<Object[]> fichas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{            

            Query query = session.createSQLQuery("select f.fev_doc_id, i.ite_nom, f.fev_fec, f.fev_tot, p.pfi_ins_id, p.pfi_nom, pe.nom, pe.ape_mat, pe.ape_pat, c.crg_tra_nom \n" +
                            "from institucional.ficha_evaluacion_documentos f \n" +
                            "join institucional.plantilla_ficha_institucional p on f.pla_id = p.pfi_ins_id \n" +
                            "join institucional.item_file i on i.ite_ide = f.ite_ide \n" +
                            "join trabajador t ON f.fev_esp = t.tra_id \n" +
                            "join pedagogico.persona pe on t.per_id = pe.per_id \n" +
                            "join trabajador_cargo c on t.tra_car = c.crg_tra_ide \n"
                            + "WHERE f.ite_ide = " + iteide);
            
            fichas = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar las fichas de evaluacion \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar las fichas de evaluacion \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return fichas;
    }
    
    @Override
    public FichaEvaluacionDocumentos buscarxId(int ficId){
        Session session = HibernateUtil.getSessionFactory().openSession();                
        FichaEvaluacionDocumentos ficha = (FichaEvaluacionDocumentos)session.get(FichaEvaluacionDocumentos.class, ficId);
        session.close();        
        return ficha;
    }
}
