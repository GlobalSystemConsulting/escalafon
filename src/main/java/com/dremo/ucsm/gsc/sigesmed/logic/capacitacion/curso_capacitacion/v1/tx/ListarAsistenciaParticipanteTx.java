package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AsistenciaParticipanteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AsistenciaParticipante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.JustificacionInasistenciaCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarAsistenciaParticipanteTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(ListarAsistenciaParticipanteTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int opt = data.getInt("opt");
        WebResponse response = null;

        switch (opt) {
            case 0:
                response = listarGrupal(data.getInt("horCod"));
                break;

            case 1:
                response = listarIndividual(data.getInt("codDoc"), data.getInt("codHor"), data.getString("url"));
                break;
                
            case 2:
                response = listarSede(data.getInt("codSed"), data.getString("url"));
                break;
        }

        return response;
    }    
    
    private WebResponse listarGrupal(int horCod) {
        try {
            AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
            List<AsistenciaParticipante> asistencias = asistenciaParticipanteDao.buscarParticipantes(horCod, new Date());

            JSONArray array = new JSONArray();
            
            for(AsistenciaParticipante attendance: asistencias) {
                JSONObject object = new JSONObject();
                object.put("cod", attendance.getAsiParCapId());
                object.put("est", (attendance.getEstReg().equals('G'))?"A":attendance.getEstReg());
                
                Persona person = attendance.getPersona();
                object.put("doc", person.getApePat() + " " + person.getApeMat() + " " + person.getNom());
                array.put(object);
            }
            
            return WebResponse.crearWebResponseExito("Las asistencias de los participantes se listaron correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "ListarAsistenciaParticipanteTx", e);
            return WebResponse.crearWebResponseError("No se pudo listar las asistencias de los participantes", WebResponse.BAD_RESPONSE);
        }
    }
    
    private WebResponse listarIndividual(int perId, int codHor, String url) {
        try {
            AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
            List<AsistenciaParticipante> asistencias = asistenciaParticipanteDao.buscarPorHorarioParticipante(perId, codHor);
            
            JSONArray array = new JSONArray();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            
            for(AsistenciaParticipante attendance: asistencias) {
                JSONObject object = new JSONObject();
                object.put("cod", attendance.getAsiParCapId());
                object.put("est", attendance.getEstReg());
                object.put("fec", format.format(attendance.getFec()));
                
                JSONArray arrayJust = new JSONArray();
                
                for(JustificacionInasistenciaCapacitacion justificacion: attendance.getJustificaciones()) {
                    JSONObject objectJust = new JSONObject();
                    objectJust.put("cod", justificacion.getJusInaCapId());
                
                    String nomFile = justificacion.getNom();
                    nomFile = nomFile.substring(nomFile.lastIndexOf("_jus_") + 6);
                    nomFile = nomFile.substring(nomFile.indexOf("_") + 1);
                    nomFile = nomFile.substring(0, nomFile.lastIndexOf("."));

                    objectJust.put("nom", nomFile);
                    objectJust.put("url", url + Sigesmed.UBI_ARCHIVOS + HelpTraining.attachments_J_Address + justificacion.getNom());

                    arrayJust.put(objectJust);
                }
                
                object.put("adj", arrayJust);
                array.put(object);
            }
            
            return WebResponse.crearWebResponseExito("Las asistencias de los participantes se listaron correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "ListarAsistenciaParticipanteTx", e);
            return WebResponse.crearWebResponseError("No se pudo listar las asistencias de los participantes", WebResponse.BAD_RESPONSE);
        }
    }
    
    private WebResponse listarSede(int codSed, String url) {
        try {
            AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
            List<AsistenciaParticipante> asistencias = asistenciaParticipanteDao.buscarPorSede(codSed);
            
            JSONArray array = new JSONArray();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            
            for(AsistenciaParticipante attendance: asistencias) {
                JSONObject object = new JSONObject();
                object.put("cod", attendance.getAsiParCapId());
                object.put("est", attendance.getEstReg());
                object.put("fec", format.format(attendance.getFec()));
                
                Persona person = attendance.getPersona();
                object.put("doc", person.getApePat() + " " + person.getApeMat() + " " + person.getNom());
                
                JSONArray arrayJust = new JSONArray();
                
                for(JustificacionInasistenciaCapacitacion justificacion: attendance.getJustificaciones()) {
                    JSONObject objectJust = new JSONObject();
                    objectJust.put("cod", justificacion.getJusInaCapId());
                
                    String nomFile = justificacion.getNom();
                    nomFile = nomFile.substring(nomFile.lastIndexOf("_jus_") + 6);
                    nomFile = nomFile.substring(nomFile.indexOf("_") + 1);
                    nomFile = nomFile.substring(0, nomFile.lastIndexOf("."));

                    objectJust.put("nom", nomFile);
                    objectJust.put("url", url + Sigesmed.UBI_ARCHIVOS + HelpTraining.attachments_J_Address + justificacion.getNom());

                    arrayJust.put(objectJust);
                }
                
                object.put("adj", arrayJust);
                array.put(object);
            }
            
            return WebResponse.crearWebResponseExito("Las asistencias de los participantes se listaron correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "ListarAsistenciaParticipanteTx", e);
            return WebResponse.crearWebResponseError("No se pudo listar las asistencias de los participantes", WebResponse.BAD_RESPONSE);
        }
    }
}
