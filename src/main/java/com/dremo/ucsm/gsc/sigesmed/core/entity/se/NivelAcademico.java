/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author felipe
 */
@Entity
@Table(name = "nivel_academico", schema="public")
public class NivelAcademico implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "niv_aca_id")
    private int nivAcaId;
    
    @Size(max = 2147483647)
    @Column(name = "niv_aca_nom")
    private String nivAcaNom;
    
    @Column(name = "des")
    private String des;
    
    @Column(name = "niv_aca_pad")
    private Integer nivAcaPad;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private char estReg;

    public NivelAcademico() {
    }

    public NivelAcademico(int nivAcaId) {
        this.nivAcaId = nivAcaId;
    }

    public int getNivAcaId() {
        return nivAcaId;
    }

    public void setNivAcaId(int nivAcaId) {
        this.nivAcaId = nivAcaId;
    }

    public String getNivAcaNom() {
        return nivAcaNom;
    }

    public void setNivAcaNom(String nivAcaNom) {
        this.nivAcaNom = nivAcaNom;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Integer getNivAcaPad() {
        return nivAcaPad;
    }

    public void setNivAcaPad(Integer nivAcaPad) {
        this.nivAcaPad = nivAcaPad;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    //INICIO -usado para el catalogo
    public int getId(){
        return this.nivAcaId;
    }
    public void setId(int Id){
        this.nivAcaId=Id;
    }
    
    public String getNom() {
        return this.nivAcaNom;
    }
    public void setNom(String nom) {
        this.nivAcaNom = nom;
    }

    //FIN -usado para el catalogo
    
    @Override
    public String toString() {
        return "NivelAcademico{" + "nivAcaId=" + nivAcaId + '}';
    }

    
}
