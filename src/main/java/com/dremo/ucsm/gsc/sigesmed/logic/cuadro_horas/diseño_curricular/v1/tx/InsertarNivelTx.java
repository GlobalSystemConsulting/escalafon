/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseño_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DiseñoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
/**
 *
 * @author abel
 */
public class InsertarNivelTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Nivel nuevo = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int nivelID = requestData.optInt("nivelID");
            int diseñoID = requestData.getInt("diseñoID");
            int modalidadID = requestData.getInt("modalidadID");
            String abreviacion = requestData.getString("abreviacion");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            
            nuevo = new Nivel(nivelID,abreviacion, nombre, descripcion, diseñoID, modalidadID, new Date(), wr.getIdUsuario(), 'A');
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Nivel Educativo, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        DiseñoCurricularDao diseñoDao = (DiseñoCurricularDao)FactoryDao.buildDao("mech.DiseñoCurricularDao");
        try{
            diseñoDao.insertarNivel(nuevo);
        }catch(Exception e){
            System.out.println("No se pudo registrar el Nivel Educativo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Nivel Educativo", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("nivelID",nuevo.getNivId());
        oResponse.put("fecha",nuevo.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro del Nivel Educativo se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
