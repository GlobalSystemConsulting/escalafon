/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Tarea.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.TareaEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaTarea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.TareaDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class ResolverTareaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        BandejaTarea tarea = new BandejaTarea();
        List<FileJsonObject> listaArchivos = new ArrayList<FileJsonObject>();
        Date fechaMax = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            int planID = requestData.getInt("planID");
            int tareaID =  requestData.getInt("tareaID");
            
            tarea.setBanTarId( requestData.getInt("bandejaTareaID") );
            tarea.setAlumnoId( requestData.getInt("alumnoID") );
            tarea.setEstado(Tarea.ESTADO_ENVIADO);//estado de la bandeja a enviado = E
            JSONArray listaDocumentos = requestData.getJSONArray("documentos");
            
            fechaMax =  new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(requestData.getString("fechaMaxEntrega")) ;;
            
            //Date fechaMaxEntrega = requestData.getInt("bandejaTareaID");
            
            //leendo los documentos adjuntos          
            if(listaDocumentos.length() > 0){
                tarea.setDocumentos( new ArrayList<TareaDocumento>());
                for(int i = 0; i < listaDocumentos.length();i++){
                    JSONObject bo =listaDocumentos.getJSONObject(i);

                    String nombreArchivo = "";                    
                    //verificamos si existe un archivo adjunto al requisito
                    JSONObject jsonArchivo = bo.optJSONObject("archivo");
                    if( jsonArchivo !=null && jsonArchivo.length() > 0 ){
                        //nombre del documento adjunto tarea -> planID_tareaID_tar_res_usuarioID_numero_secuencia
                        FileJsonObject miF = new FileJsonObject( jsonArchivo ,planID+"-"+tareaID+"_tar_res_"+tarea.getAlumnoId()+"_"+(i+1));
                        nombreArchivo = miF.getName();
                        listaArchivos.add(miF);
                    }
                    tarea.getDocumentos().add( new TareaDocumento(i+1, tarea,nombreArchivo ) );
                }
            }
            
        
        }catch(Exception e){
            System.out.println("No se pudo resolver la tarea, datos incorrectos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo resolver la tarea, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        TareaEscolarDao tareaDao = (TareaEscolarDao)FactoryDao.buildDao("web.TareaEscolarDao");
        try{
            tarea.setFecEnt( new Date() );
            
            //verificando que la fecha de envio sea menor que la fecha maxima permita
            //por seguridad se vuelve a verificar la hora de envio
            if( fechaMax.compareTo( tarea.getFecEnt() ) <0 ){
                
                tarea.setDocumentos(new ArrayList<TareaDocumento>());
                tarea.setEstado(Tarea.ESTADO_FUERA_TIEMPO);
                
                //actualizando la fecha de envio con el estado fuera de tiempo
                tareaDao.enviarTareaResuelta(tarea);
                
                JSONObject oResponse = new JSONObject();
                oResponse.put("estado",""+tarea.getEstado());
                
                System.out.println("Tiempo vencido, ya no le es permitido enviar la tarea");
                return WebResponse.crearWebResponseError("Tiempo vencido, ya no le es permitido enviar la tarea",oResponse );
            }
            
            tareaDao.enviarTareaResuelta(tarea);
        }catch(Exception e){
            System.out.println("No se pudo resolver la tarea\n"+e);
            return WebResponse.crearWebResponseError("No se pudo resolver la tarea", e.getMessage() );
        }
        //Fin
        
        //si ya se envio la tarea resuelta
        //ahora creamos los archivos que se desean subir
        for(FileJsonObject archivo : listaArchivos){
            BuildFile.buildFromBase64(Tarea.BANDEJA_TAREA_PATH, archivo.getName(), archivo.getData());
        }
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("estado",""+tarea.getEstado());
        
        JSONArray aDoc= new JSONArray();
        for(TareaDocumento td : tarea.getDocumentos()){
            JSONObject oDoc = new JSONObject();
            oDoc.put("tareaDocumentoID", td.getTarDocId());
            oDoc.put("documento", td.getNomDoc());
            oResponse.put("url",Sigesmed.UBI_ARCHIVOS+Tarea.BANDEJA_TAREA_PATH);
            aDoc.put(oDoc);
        }
        
        oResponse.put("documentos",aDoc);
        return WebResponse.crearWebResponseExito("Se envio la tarea resuelta correctamente",oResponse);
        //Fin
    }    
    
    
}
