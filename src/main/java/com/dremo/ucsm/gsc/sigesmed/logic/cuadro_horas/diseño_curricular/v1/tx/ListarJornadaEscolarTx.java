/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseño_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DiseñoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaCurricularHora;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.JornadaEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarJornadaEscolarTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las jornadas escolares, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<JornadaEscolar> jornadas = null;
        
        try{
            DiseñoCurricularDao diseñoDao = (DiseñoCurricularDao)FactoryDao.buildDao("mech.DiseñoCurricularDao");
            jornadas = diseñoDao.listarJornadaEscolarConHoras();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las jornadas escolares\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las jornadas escolares", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        
        JSONArray aJor = new JSONArray();
        int pos = 0;
        for(JornadaEscolar j: jornadas){
            JSONObject oJor = new JSONObject();
            oJor.put("jornadaID", j.getJorEscId() );
            
            oJor.put("nivelID", j.getNivId() );
            oJor.put("abreviacion", j.getAbr() );
            oJor.put("nombre", j.getNom() );
            oJor.put("hObligatoria", j.getHorObl() );
            oJor.put("hLibre", j.getHorLibDis() );
            oJor.put("hTutoria", j.getHorTut() );
            oJor.put("hTotal", j.getHorTot() );
            oJor.put("descripcion", j.getDes() );
            oJor.put("i", pos++ );
            
            JSONArray aAreaHora = new JSONArray();
            
            for(AreaCurricularHora areaHora: j.getAreaHoras()){
                JSONObject oH = new JSONObject();
                oH.put("areaHoraID", areaHora.getAreHorId() );
                oH.put("jornadaID", areaHora.getJorEscId() );
                oH.put("gradoID", areaHora.getGraId() );
                oH.put("areaID", areaHora.getAreCurId() );
                oH.put("hora", areaHora.getHorObl() );
                
                aAreaHora.put(oH);
            }
            oJor.put("horas", aAreaHora);

            aJor.put(oJor);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",aJor);        
        //Fin
    }
    
}

