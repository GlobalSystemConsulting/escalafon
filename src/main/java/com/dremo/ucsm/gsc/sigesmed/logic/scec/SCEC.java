/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.scec;

/**
 *
 * @author ucsm
 */

public interface SCEC {
   
    String SUCC_RESPONSE_COD = "succ";
    String ERR_RESPONSE_COD = "err";
    String SUCC_RESPONSE_MESS_LIST = "Operacion realizada con exito";
    String ERR_RESPONSE_MESS_LIST = "No se pudo obtener los elementos de la ficha de evaluacion";
    
    String SUCC_RESPONSE_MESS_INS = "Se realizo el registro correctamente";
    String ERR_RESPONSE_MESS_INS = "No se pudo realizar el registro";
    
    String[] TIPO_ELEMENTOS = {"escalas","rangos","columnas","contenido"};
    String TIPO_ELEMENTO_ESCALA = "escalas";
    String TIPO_ELEMENTO_RANGO = "rangos";
    String TIPO_ELEMENTO_COLUMNA = "columnas";
    String TIPO_ELEMENTO_CONTENIDO = "contenido";
    String TIPO_ELEMENTO_INDICADOR = "indicador";
    
    String CAMPO_ELEMENTO_FICHA = "fic";
    ///campos de escala de valoracion
    String CAMPO_ESCALA_ID = "id";
    String CAMPO_ESCALA_NOMBRE = "nom";
    String CAMPO_ESCALA_DESCRIPCION = "des";
    String CAMPO_ESCALA_MAX = "max";
    String CAMPO_ESCALA_MIN = "min";
    
    //campos de Columna Valoracion
    String CAMPO_COL_ID = "id";
    String CAMPO_COL_VAL = "val";
    
    //campos de Rango Porcentual
    String CAMPO_RANGO_ID = "id";
    String CAMPO_RANGO_NOMBRE = "nom";
    String CAMPO_RANGO_DESCRIPCION = "des";
    String CAMPO_RANGO_MAX = "max";
    String CAMPO_RANGO_MIN = "min";
    
    //campos contenido
    String CAMPO_CONTENIDO_ID = "id";
    String CAMPO_CONTENIDO_TIPO = "tip";
    String CAMPO_CONTENIDO_NOMBRE = "nom";
    
    //campos indicador
    String CAMPO_INDICADOR_ID = "id";
    String CAMPO_INDICADOR_CONTENIDO = "fic";
    String CAMPO_INDICADOR_DESC = "des";
    String CAMPO_INDICADOR_NOMBRE = "nom";
    String CAMPO_INDICADOR_DOC_VER = "doc";
}

