package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.IntegranteComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.IntegranteComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 * Created by geank on 02/10/16.
 */
public class EliminarIntegranteComisionTx implements ITransaction {
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonData = (JSONObject) wr.getData();
        String dni = jsonData.getString("dni");
        int comId = jsonData.getInt("com");
        return eliminarIntegrante(dni,comId);
    }
    public WebResponse eliminarIntegrante(String dni,int com){
        try {
            IntegranteComisionDao integranteComisionDao = (IntegranteComisionDao) FactoryDao.buildDao("scec.IntegranteComisionDao");
            PersonaDao personaDao = (PersonaDao)FactoryDao.buildDao("PersonaDao");
            Persona persona = personaDao.buscarPorDNI(dni);
            IntegranteComision integranteComision = integranteComisionDao.buscarPorComisionConAsistencias(persona.getPerId(),com);

            integranteComisionDao.deleteAbsolute(integranteComision);
            return WebResponse.crearWebResponseExito("Se elimino correctamente el integrante",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            return WebResponse.crearWebResponseError("Ocurrio un error al ejecutar la operacion",WebResponse.BAD_RESPONSE);
        }
    }
}
