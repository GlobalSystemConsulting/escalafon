package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ObjetivoProgramacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProgramacionAnual;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ValoresActitudes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.util.List;

/**
 * Created by Administrador on 21/10/2016.
 */
public class ProgramacionAnualDaoHibernate extends GenericDaoHibernate<ProgramacionAnual> implements ProgramacionAnualDao {
    @Override
    public ProgramacionAnual buscarProgramacionConObjetivos(int idProg) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM ProgramacionAnual p LEFT JOIN FETCH p.objetivos WHERE p.proAnuId  =:id";
            Query query = session.createQuery(hql);
            query.setParameter("id",idProg);

            return (ProgramacionAnual)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ProgramacionAnual buscarProgramacionConValores(int idProg) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM ProgramacionAnual p LEFT JOIN FETCH p.valores WHERE p.proAnuId  =:id";
            Query query = session.createQuery(hql);
            query.setParameter("id",idProg);

            return (ProgramacionAnual)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ProgramacionAnual buscarProgramacionConDatos(int idProg) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(ProgramacionAnual.class)
                    .createAlias("are", "a", JoinType.INNER_JOIN)
                    .createAlias("org", "o", JoinType.INNER_JOIN)
                    .createAlias("gra", "g", JoinType.INNER_JOIN)
                    .createAlias("planEstudios", "pl", JoinType.INNER_JOIN)
                    .add(Restrictions.eq("proAnuId", idProg))
                    .setMaxResults(1);
            ProgramacionAnual prog = (ProgramacionAnual) criteria.uniqueResult();
            prog.getObjetivos().size();
            prog.getValores().size();
            return prog;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<ProgramacionAnual> buscarProgramacionDocente(int idDocente) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(ProgramacionAnual.class)
                    .createAlias("org","o", JoinType.INNER_JOIN)
                    .createAlias("are","a", JoinType.INNER_JOIN)
                    .createAlias("gra","g",JoinType.INNER_JOIN)
                    .createAlias("g.nivel","n",JoinType.INNER_JOIN)
                    .add(Restrictions.eq("estReg",'A'))
                    .createCriteria("doc").add(Restrictions.eq("doc_id",idDocente));

            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<ProgramacionAnual> buscarProgramacionParaUgel(int idUgel) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(ProgramacionAnual.class)
                    .createAlias("are","a", JoinType.INNER_JOIN)
                    .createAlias("gra","g",JoinType.INNER_JOIN)
                    .add(Restrictions.eq("estReg",'A'))
                    .createCriteria("org",JoinType.INNER_JOIN).createCriteria("organizacionPadre").add(Restrictions.eq("orgId",idUgel));

            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Grado> buscarGradosDisenoCurricular(int dis) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "select g from Grado g inner join g.diseñoCurricular dc where dc.id =:dis and g.estReg = 'A'" ;
            Query query = session.createQuery(hql);
            query.setParameter("dis",dis);

            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<AreaCurricular> buscarAreaCurricularCurricula(int dis) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "select a from AreaCurricularMaestro a inner join a.disenoCurr dc where dc.disCurId =:dis and a.estReg = 'A'";
            Query query = session.createQuery(hql);
            query.setParameter("dis",dis);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Grado buscarGrado(int idGrado) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Grado grado = (Grado) session.get(Grado.class,idGrado);
            return grado;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public AreaCurricular buscarArea(int idArea) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            AreaCurricular area = (AreaCurricular) session.get(AreaCurricular.class,idArea);
            return area;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }

    }

    @Override
    public List<ValoresActitudes> buscarValoresDeProgramacion(int idProg) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT v FROM ValoresActitudes v INNER JOIN v.programaciones p WHERE p.proAnuId =:id";
            Query query = session.createQuery(hql);
            query.setInteger("id",idProg);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<ObjetivoProgramacion> buscarObjetivosDeProgramacion(int idProg) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT o FROM ObjetivoProgramacion o INNER JOIN o.programaciones p WHERE p.proAnuId =:id";
            Query query = session.createQuery(hql);
            query.setInteger("id",idProg);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
    @Override
    public List<ObjetivoProgramacion> buscarObjetivosDeProgramacion(int idProg,String tip) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT o FROM ObjetivoProgramacion o INNER JOIN o.programaciones p WHERE p.proAnuId =:id AND o.tip =:tip";
            Query query = session.createQuery(hql);
            query.setInteger("id",idProg);
            query.setString("tip",tip);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<ObjetivoProgramacion> buscarObjetivosDeProgramacion(String tip) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT o FROM ObjetivoProgramacion o WHERE  o.tip =:tip";
            Query query = session.createQuery(hql);
            query.setParameter("tip",tip);
            return query.list();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ValoresActitudes buscarValorPorId(int idVal) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT v FROM ValoresActitudes v WHERE  v.valActId =:id";
            Query query = session.createQuery(hql);
            query.setParameter("id",idVal);

            return (ValoresActitudes)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public PlanEstudios buscarPlanEstudios(int idPlan) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM PlanEstudios p WHERE  p.plaEstId =:id";
            Query query = session.createQuery(hql);
            query.setParameter("id",idPlan);
            return (PlanEstudios)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ProgramacionAnual buscarProgramacionAnualPorPlanEstudios(int idPLan,int idOrg,int idDoc,int idArea,int idGrado) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT pc FROM ProgramacionAnual pc WHERE  pc.planEstudios.plaEstId =:id AND pc.org.orgId =:orgId AND pc.doc.doc_id =:docId AND pc.are.areCurId =:areId AND pc.gra.graId =:graId";
            Query query = session.createQuery(hql);
            query.setInteger("id",idPLan);
            query.setInteger("orgId",idOrg);
            query.setInteger("docId",idDoc);
            query.setInteger("areId",idArea);
            query.setInteger("graId",idGrado);
            query.setMaxResults(1);
            return (ProgramacionAnual)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public Periodo listarPeriodoPlanEstudio(int idNivel, int idPlan) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT p FROM PlanNivel pn INNER JOIN pn.periodo p" +
                    " INNER JOIN pn.jornada j INNER JOIN j.nivel niv INNER JOIN pn.planEstudios pe" +
                    " WHERE niv.nivId =:nivId AND pe.plaEstId =:planId";
            Query query = session.createQuery(hql);
            query.setInteger("nivId",idNivel);
            query.setInteger("planId",idPlan);
            query.setMaxResults(1);
            return (Periodo)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
