/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ColegiaturaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ExposicionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PublicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ReconocimientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Capacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Colegiatura;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Exposicion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Reconocimiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class SeleccionReporteFichaEscalafonariaTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(SeleccionReporteFichaEscalafonariaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {

        long idPer = wr.getIdUsuario();
        JSONObject data = (JSONObject) wr.getData();
        Integer traId = data.optInt("traId");
        JSONArray seleccionados = data.optJSONArray("seleccionados");

        System.out.println("Seleccionados: " + seleccionados);

        FichaEscalafonariaDao fichaEscalafonariaDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");

        ParientesDao parientesDao = (ParientesDao) FactoryDao.buildDao("se.ParientesDao");
        FormacionEducativaDao formacionEducativaDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
        ColegiaturaDao colegiaturaDao = (ColegiaturaDao) FactoryDao.buildDao("se.ColegiaturaDao");
        EstudioComplementarioDao estudioComplementarioDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
        ExposicionDao exposicionDao = (ExposicionDao) FactoryDao.buildDao("se.ExposicionDao");
        PublicacionDao publicacionDao = (PublicacionDao) FactoryDao.buildDao("se.PublicacionDao");
        CapacitacionDao capacitacionDao = (CapacitacionDao) FactoryDao.buildDao("se.CapacitacionDao");
        ReconocimientoDao reconocimientoDao = (ReconocimientoDao) FactoryDao.buildDao("se.ReconocimientoDao");
        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");

        FichaEscalafonaria datosGenerales = null;
        //List<TrabajadorOrganigramaDetalle> cargos = new ArrayList<Trabajador>();

        List<Parientes> parientes = new ArrayList<Parientes>();
        List<FormacionEducativa> formacionesEducativas = new ArrayList<FormacionEducativa>();
        List<Colegiatura> colegiaturas = new ArrayList<Colegiatura>();
        List<EstudioComplementario> estudiosComplementarios = new ArrayList<EstudioComplementario>();
        List<EstudioComplementario> estudiosEspecializacion = new ArrayList<EstudioComplementario>();
        List<EstudioComplementario> conocimientosInformaticos = new ArrayList<EstudioComplementario>();
        List<EstudioComplementario> idiomas = new ArrayList<EstudioComplementario>();
        List<Exposicion> exposiciones = new ArrayList<Exposicion>();
        List<Publicacion> publicaciones = new ArrayList<Publicacion>();
        List<Capacitacion> capacitaciones = new ArrayList<Capacitacion>();
        List<Reconocimiento> reconocimientos = new ArrayList<Reconocimiento>();
        List<Reconocimiento> meritos = new ArrayList<Reconocimiento>();
        List<Reconocimiento> bonificaciones = new ArrayList<Reconocimiento>();
        List<Demerito> demeritos = new ArrayList<Demerito>();

        datosGenerales = fichaEscalafonariaDao.buscarPorTraId(traId);
        parientes = parientesDao.listarxTrabajador(traId);
        
        
        
        //Extrayendo el id de la Persona de un Trabajador
        Integer perId = datosGenerales.getTrabajador().getPersona().getPerId();
        formacionesEducativas = formacionEducativaDao.listarxFichaEscalafonaria(perId);
        colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(perId);
        estudiosComplementarios = estudioComplementarioDao.listarxFichaEscalafonaria(perId);
        exposiciones = exposicionDao.listarxFichaEscalafonaria(perId);
        publicaciones = publicacionDao.listarxFichaEscalafonaria(perId);
        capacitaciones = capacitacionDao.listarxFichaEscalafonaria(perId);
        reconocimientos = reconocimientoDao.listarxFichaEscalafonaria(perId);
        demeritos = demeritoDao.listarxFichaEscalafonaria(perId);

        Persona pe = null;
        PersonaDao peDao = (PersonaDao) FactoryDao.buildDao("di.PersonaDao");
        pe = peDao.buscarPersonaxId(idPer);

        //Estudios complementarios
        for (EstudioComplementario ec : estudiosComplementarios) {
            switch (ec.getTipId()) {
                case 1:
                    conocimientosInformaticos.add(ec);
                    break;
                case 4:
                    idiomas.add(ec);
                    break;
                default:
                    estudiosEspecializacion.add(ec);
                    break;
            }
        }
        //Otro
        for (Reconocimiento r : reconocimientos) {
            if (r.getMot() == '3' || r.getMot() == '4' || r.getMot() == '5') {
                bonificaciones.add(r);
            } else {
                meritos.add(r);
            }
        }
        
        //$scope.seleccionados = [false, false,false, false, false,false, false, false,false, ---, false,false, false,-----,------];
        JSONObject response = new JSONObject();
        JSONArray selec = new JSONArray();
        selec.put(0,true);//Datos personales
        selec.put(1,!parientes.isEmpty());//Familiares
        selec.put(2,!formacionesEducativas.isEmpty());//FormacionesED
        selec.put(3,!colegiaturas.isEmpty());//Colegiaturas
        selec.put(4,!estudiosEspecializacion.isEmpty());//Estudios de especializacion
        selec.put(5,!conocimientosInformaticos.isEmpty());//Conocimientos Info
        selec.put(6,!idiomas.isEmpty());//Idiomas
        selec.put(7,!exposiciones.isEmpty());//Expos y Ponen
        selec.put(8,!publicaciones.isEmpty());//Publicaciones
        selec.put(9,!capacitaciones.isEmpty());//Capacitaciones
        selec.put(10,!meritos.isEmpty());//Meritos
        selec.put(11,!bonificaciones.isEmpty());//Bonificaciones
        selec.put(12,!demeritos.isEmpty());//Demeritos
        selec.put(13,false);
        selec.put(14,false);
        response.put("seleccionados", selec);
        return WebResponse.crearWebResponseExito("Se listo correctamente", response);
    }

}
