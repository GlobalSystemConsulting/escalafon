/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LicenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Licencia;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class LicenciaDaoHibernate extends GenericDaoHibernate<Licencia> implements LicenciaDao{

    @Override
    public List<Licencia> listarxFichaEscalafonaria(int ficEscId) {
        List<Licencia> licencias = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT lic from Licencia as lic "
                    + "join fetch lic.fichaEscalafonaria as fe "
                    + "WHERE fe.ficEscId=" + ficEscId + " AND lic.estReg='A' "
                    + "order by lic.licId asc";
            Query query = session.createQuery(hql);
            licencias = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las licencias\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las licencias \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return licencias;
    }

    @Override
    public Licencia buscarPorId(Integer licId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Licencia lic = (Licencia)session.get(Licencia.class, licId);
        session.close();
        return lic;
    }

    @Override
    public List<Licencia> listarSGxTrabajador(int traId) {
        List<Licencia> licencias = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT lic FROM Licencia AS lic "
                    + "JOIN FETCH lic.fichaEscalafonaria AS fe "
                    + "JOIN FETCH fe.trabajador AS tr "
                    + "WHERE tr.traId=:workerId "
                    + "AND lic.estGoc=false "
                    + "AND lic.estReg='A'";
            Query query = session.createQuery(hql);
            query.setParameter("workerId", traId);
            licencias = query.list();
            t.commit();
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las licencias\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las licencias \\n "+ e.getMessage());            
        }finally{
            session.close();
        }
        return licencias;
    }
    
}
