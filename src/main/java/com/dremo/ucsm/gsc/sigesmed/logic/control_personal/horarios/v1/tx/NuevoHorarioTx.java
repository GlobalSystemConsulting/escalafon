/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;

import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiaSemana;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public class NuevoHorarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
       
        HorarioCab nuevoHorario=null;
        List<HorarioDet> horarioDet=new ArrayList<>();
       
        try{
            JSONObject requestData = (JSONObject)wr.getData();
             
            JSONObject ho=requestData.getJSONObject("horario");
            Integer org=requestData.getInt("organizacionID");
            Organizacion organizacion=new Organizacion(org);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            String descripcion=ho.getString("des");
            nuevoHorario =new HorarioCab(null, "A", descripcion, new Date(), organizacion, null);
            JSONArray detalle = (JSONArray) ho.getJSONArray("horario");
            for (int i = 0; i < detalle.length(); i++) {

                JSONObject  h = (JSONObject) detalle.get(i);
                HorarioDet dt=null;
                String lunDesde=h.getString("lunDesde");
                String lunHasta=h.getString("lunHasta");
                
                if(lunDesde.length()>0)
                {
                    Date _lunDesde=df.parse(lunDesde);
                    Date _lunHasta=df.parse(lunHasta);
                    dt=new HorarioDet(DateUtil.sumarRestarHorasFecha(_lunDesde,-5),DateUtil.sumarRestarHorasFecha( _lunHasta,-5), "A", nuevoHorario, new DiaSemana('L'));
                    horarioDet.add(dt);
                }
                
                
                String marDesde=h.getString("marDesde");
                String marHasta=h.getString("marHasta");
                
                if(marDesde.length()>0)
                {
                    Date _marDesde=df.parse(marDesde);
                    Date _marHasta=df.parse(marHasta);
                    dt=new HorarioDet(DateUtil.sumarRestarHorasFecha(_marDesde,-5), DateUtil.sumarRestarHorasFecha(_marHasta,-5), "A", nuevoHorario, new DiaSemana('M'));
                    horarioDet.add(dt);
                }
                
                String mieDesde=h.getString("mieDesde");
                String mieHasta=h.getString("mieHasta");
               
                if(mieDesde.length()>0)
                {
                    Date _mieDesde=df.parse(mieDesde);
                    Date _mieHasta=df.parse(mieHasta);
                    dt=new HorarioDet(DateUtil.sumarRestarHorasFecha(_mieDesde,-5), DateUtil.sumarRestarHorasFecha(_mieHasta,-5), "A", nuevoHorario, new DiaSemana('W'));
                    horarioDet.add(dt);
                }
                
                String jueDesde=h.getString("jueDesde");
                String jueHasta=h.getString("jueHasta");
                
                if(jueDesde.length()>0)
                {
                    Date _jueDesde=df.parse(jueDesde);
                    Date _jueHasta=df.parse(jueHasta);
                    dt=new HorarioDet(DateUtil.sumarRestarHorasFecha(_jueDesde,-5), DateUtil.sumarRestarHorasFecha(_jueHasta,-5), "A", nuevoHorario, new DiaSemana('J'));
                    horarioDet.add(dt);
                }
                
                String vieDesde=h.getString("vieDesde");
                String vieHasta=h.getString("vieHasta");
                
                if(vieDesde.length()>0)
                {
                    Date _vieDesde=df.parse(vieDesde);
                    Date _vieHasta=df.parse(vieHasta);
                    dt=new HorarioDet(DateUtil.sumarRestarHorasFecha(_vieDesde,-5), DateUtil.sumarRestarHorasFecha(_vieHasta,-5), "A", nuevoHorario, new DiaSemana('V'));
                    horarioDet.add(dt);
                }
                
                String sabDesde=h.getString("sabDesde");
                String sabHasta=h.getString("sabHasta");
                
                if(sabDesde.length()>0)
                {
                    Date _sabDesde=df.parse(sabDesde);
                    Date _sabHasta=df.parse(sabHasta);
                    dt=new HorarioDet(DateUtil.sumarRestarHorasFecha(_sabDesde,-5), DateUtil.sumarRestarHorasFecha(_sabHasta,-5), "A", nuevoHorario, new DiaSemana('S'));
                    horarioDet.add(dt);
                }
                
                String domDesde=h.getString("domDesde");
                String domHasta=h.getString("domHasta");
                
                if(domDesde.length()>0)
                {
                    Date _domDesde=df.parse(domDesde);
                    Date _domHasta=df.parse(domHasta);
                    dt=new HorarioDet(DateUtil.sumarRestarHorasFecha(_domDesde,-5), DateUtil.sumarRestarHorasFecha(_domHasta,-5), "A", nuevoHorario, new DiaSemana('D'));
                    horarioDet.add(dt);
                }
                      
                
            }
            nuevoHorario.setHorarioDetalle(horarioDet);
            
            
           
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }
        
       
        HorarioDao horarioDao = (HorarioDao)FactoryDao.buildDao("cpe.HorarioDao");
        
        try{
            horarioDao.insert(nuevoHorario);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Registrar el Horario", e.getMessage() );
        }

        return WebResponse.crearWebResponseExito("Se Registro correctamente");        
        //Fin
    }
    
}

