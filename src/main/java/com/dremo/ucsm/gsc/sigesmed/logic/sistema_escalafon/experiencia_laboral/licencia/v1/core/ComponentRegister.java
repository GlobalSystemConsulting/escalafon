/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.licencia.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.licencia.v1.tx.*;

public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("licencia");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarLicencias", ListarLicenciasTx.class);
	seComponent.addTransactionPOST("agregarLicencia", AgregarLicenciaTx.class);
        seComponent.addTransactionPUT("actualizarLicencia", ActualizarLicenciaTx.class);
        seComponent.addTransactionDELETE("eliminarLicencia", EliminarLicenciaTx.class);
        
        return seComponent;
    }
    
}
