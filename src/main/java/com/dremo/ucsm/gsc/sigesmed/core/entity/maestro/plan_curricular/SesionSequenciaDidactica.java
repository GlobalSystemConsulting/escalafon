package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 02/12/2016.
 */
@Entity
@Table(name = "sesion_sequencia_didactica",schema = "pedagogico")
public class SesionSequenciaDidactica implements java.io.Serializable {
    @SequenceGenerator(name = "sesion_sequencia_didactica_ses_sec_did_id_seq", sequenceName = "pedagogico.sesion_sequencia_didactica_ses_sec_did_id_seq")
    @GeneratedValue(generator = "sesion_sequencia_didactica_ses_sec_did_id_seq")
    @Id
    @Column(name = "ses_sec_did_id")
    private int sesSeqDidId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ses_apr_id")
    private SesionAprendizaje sesion;

    @OneToMany(mappedBy = "sequenciaDidactica",fetch = FetchType.LAZY,cascade = CascadeType.REMOVE)
    private List<SeccionSequenciaDidacticaSesion> seccionesSequenciaDidactica;

    /*@OneToMany(mappedBy = "sequencia",fetch = FetchType.LAZY,cascade = CascadeType.REMOVE)
    private List<ActividadSeccionSequenciaDidactica> actividadesSecuencia;*/

    @Column(name = "nom")
    private String nom;
    @Column(name = "dur_tot")
    private Integer durTot;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length=29)
    private Date fecMod;
    @Column(name = "est_reg",length = 1)
    private Character estReg;


    /*@OneToMany(mappedBy = "sequencia",fetch = FetchType.EAGER)
    List<ActividadSeccionSequenciaDidactica> actividades;*/

    public SesionSequenciaDidactica() {
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public SesionSequenciaDidactica(String nom) {
        this.nom = nom;

        this.durTot = 0;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public SesionSequenciaDidactica(String nom, int durTot) {
        this.nom = nom;
        this.durTot = durTot;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getSesSeqDidId() {
        return sesSeqDidId;
    }

    public void setSesSeqDidId(int sesSeqDidId) {
        this.sesSeqDidId = sesSeqDidId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getDurTot() {
        return durTot;
    }

    public void setDurTot(int durTot) {
        this.durTot = durTot;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public SesionAprendizaje getSesion() {
        return sesion;
    }

    public void setSesion(SesionAprendizaje sesion) {
        this.sesion = sesion;
    }

    /*public List<ActividadSeccionSequenciaDidactica> getActividadesSecuencia() {
        return actividadesSecuencia;
    }*/

    public List<SeccionSequenciaDidacticaSesion> getSeccionesSequenciaDidactica() {
        return seccionesSequenciaDidactica;
    }
    /*public List<ActividadSeccionSequenciaDidactica> getActividades() {
        return actividades;
    }*/
}
