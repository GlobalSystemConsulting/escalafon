/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.organigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author forev
 */
public class IMPORTAEXCEL  { 

public static void main(String[] args){
    
     
    ArrayList <Cargo> cargos=null;
    CargoDao cargoDao = (CargoDao)FactoryDao.buildDao("se.CargoDao");
    cargos=(ArrayList<Cargo>) cargoDao.listarxCargo();
    
    ArrayList <Categoria> categoria=null;
    CategoriaDao categoriaDao = (CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
    categoria= (ArrayList<Categoria>) categoriaDao.listarxCategoria();
    int cargo=0;
    int cat=0;
    ArrayList <Trabajador> trabajador= new ArrayList<>();
    Trabajador tra=null;
    String nombreArchivo = "P0PERSON211.xlsx";
    String rutaArchivo = "C:\\Users\\forev\\Documents\\ESCALAFON\\"+nombreArchivo;
    String hoja = "PLANILLA";
    System.out.println("tamaño: "+rutaArchivo);
    //lectura excel
            try {
                FileInputStream file = new FileInputStream(new File(rutaArchivo));
                //Crear el objeto que tendra el libro de Excel 	
                XSSFWorkbook workbook = new XSSFWorkbook(file);
                /**
                 * Obtenemos la primera pestaña a la que se quiera procesar
                 * indicando el indice. Una vez obtenida la hoja excel con las
                 * filas que se quieren leer obtenemos el iterator que nos
                 * permite recorrer cada una de las filas que contiene.
                 */
                XSSFSheet sheet = workbook.getSheetAt(5);
                Iterator<Row> rowIterator = sheet.iterator();
                Row row;
                String[] DatCel = new String[8];
                System.out.println("tamaño: "+DatCel.length);
                int con = 0;//permitara acceder a segundo if para el registro de cuenta contable luegio de validar las cabezaras de cada columna del excel
                // Recorremos todas las filas para mostrar el contenido de cada celda
                
                while (rowIterator.hasNext()) {
                    
                    //contador=contador+1;
                    
                    row = rowIterator.next();
                    // Obtenemos el iterator que permite recorres todas las celdas de una fila
                    Iterator<Cell> cellIterator = row.cellIterator();
                    Cell celda;
                    int i = 0;
                    while (cellIterator.hasNext()) {
                         System.out.println("Campono: "+DatCel[1]);
                        celda = cellIterator.next();
                        // Dependiendo del formato de la celda el valor se debe mostrar como String, Fecha, boolean, entero...
                        switch (celda.getCellType()) {
                          
                            case Cell.CELL_TYPE_STRING: {
                                System.out.println("Campo: "+DatCel[i]);
                                System.out.println("Campo de: "+i);
                                DatCel[i] = celda.getStringCellValue();
                                break;
                            }
                        }
                        i++;

                    }
                    if (DatCel[5].equals("ZCARGO") && DatCel[6].equals("PCATEGO")) {
                        con = 1;
                        continue;
                    }
                    if (con == 1) {
                            
                            if(DatCel[5]!=""){
                                for(Cargo t:cargos){
                                    if(t.getNomCar()==DatCel[5])
                                        cargo=t.getCarId();
                                        
                                }
                            }
                            
                            if(DatCel[6]!=""){
                                for(Categoria t:categoria){
                                    if(t.getAbrCat()==DatCel[6])
                                        cat=t.getCatId();
                                        
                                }
                            }
                            
                            
                            System.out.println("("+DatCel[0]+","+"2,"+DatCel[0]+"'"+DatCel[2]+"-"+DatCel[3]+"-"+DatCel[4]+"',"+"'2019-01-01',"+1+"'A',"+5+","+1+","+cargo+","+cat+"),");
                                
                        
                                                                                                                               
                        
                    } else {
                        System.out.println("Error en el formato de Excel");
                    }
                     
                }

                workbook.close();

            } catch (Exception e) {
                System.out.println("ERRO HOJA");
            }
            //Fin de lectura de Excel .xlsx
//            for (int j = 0; j < organigramas.size(); j++) {
//                System.out.print(organigramas.get(j).getTipoOrganigrama().getTipOrgiId()+ "--");
//                System.out.print(organigramas.get(j).getOrganigramaPadre().getCamOrgi()+"--");
//                System.out.print(organigramas.get(j).getCamOrgi()+ "--");
//                System.out.print(organigramas.get(j).getCodOrgi()+ "--");
//                System.out.print(organigramas.get(j).getAbrOrgi()+ "--");
//                System.out.print(organigramas.get(j).getNomOrgi()+ "--");
//                
//                System.out.print(organigramas.get(j).getUsuMod()+ "--");
//                System.out.println(organigramas.get(j).getEstReg()+ "--");
//            }
           // System.out.println(organigramas.size());
          //  System.out.println("contador:"+contador);
            // Ingresando el Excel a la Base de Datos
//            try {
//                for (int j = 0; j < organigramas.size(); j++) {
//                    
//                        organigramaDao.insert(organigramas.get(j));                  
//
//                }
//                System.out.println(contador);
//            } catch (Exception e) {
//                System.out.println("No se pudo ingresar");
//            }
           
           
    
}
    
}