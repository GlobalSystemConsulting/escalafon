/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.List;

/**
 *
 * @author Administrador
 */
@Entity(name="com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Ubigeo")
@Table(name = "ubigeo")

public class Ubigeo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ubi_ide")
    private String ubiIde;
    @Column(name = "ubi_dto_nom")
    private String ubiDtoNom;
    @Column(name = "ubi_pro_nom")
    private String ubiProNom;
    @Column(name = "ubi_dis_nom")
    private String ubiDisNom;
    @OneToMany(mappedBy = "ubigeo")
    private List<Organizacion> organizacion;

    public Ubigeo() {
    }

    public Ubigeo(String ubiIde) {
        this.ubiIde = ubiIde;
    }

    public String getUbiIde() {
        return ubiIde;
    }

    public void setUbiIde(String ubiIde) {
        this.ubiIde = ubiIde;
    }

    public String getUbiDtoNom() {
        return ubiDtoNom;
    }

    public void setUbiDtoNom(String ubiDtoNom) {
        this.ubiDtoNom = ubiDtoNom;
    }

    public String getUbiProNom() {
        return ubiProNom;
    }

    public void setUbiProNom(String ubiProNom) {
        this.ubiProNom = ubiProNom;
    }

    public String getUbiDisNom() {
        return ubiDisNom;
    }

    public void setUbiDisNom(String ubiDisNom) {
        this.ubiDisNom = ubiDisNom;
    }

    public List<Organizacion> getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(List<Organizacion> organizacion) {
        this.organizacion = organizacion;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ubiIde != null ? ubiIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ubigeo)) {
            return false;
        }
        Ubigeo other = (Ubigeo) object;
        if ((this.ubiIde == null && other.ubiIde != null) || (this.ubiIde != null && !this.ubiIde.equals(other.ubiIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.Ubigeo[ ubiIde=" + ubiIde + " ]";
    }
    
}
