package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CapacidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaCapacidad;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 03/11/2016.
 */
public class ListarCompetenciasAreaTx implements ITransaction{

    private static Logger logger = Logger.getLogger(ListarCompetenciasAreaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idArea = data.getInt("are");
        return listarCompetencias(idArea);
    }
    private WebResponse listarCompetencias(int idArea){
        try{
            CompetenciaAprendizajeDao compDao = (CompetenciaAprendizajeDao) FactoryDao.buildDao("maestro.plan.CompetenciaAprendizajeDao");
            List<CompetenciaAprendizaje> competencias = compDao.buscarCompetenciaPorArea(idArea);
            JSONArray jsonComps= new JSONArray();
            for(CompetenciaAprendizaje comp : competencias){
                JSONObject jsonComp = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"comId","dcn","nom"},
                        new String[]{"id","dcn","nom"},
                        comp
                ));
                List<CompetenciaCapacidad> comCaps = comp.getCapacidades();
                List<CapacidadAprendizaje> capacidades = new ArrayList<>();
                JSONArray jsonCaps = new JSONArray();
                for(CompetenciaCapacidad comCap : comCaps){
                    CapacidadAprendizaje cap = comCap.getCap();
                    if(!capacidades.contains(cap)){
                        capacidades.add(cap);
                        JSONObject jsonCap = new JSONObject(EntityUtil.objectToJSONString(
                                new String[]{"capId","nom"},
                                new String[]{"id","nom"},
                                cap
                        ));
                        /*List<IndicadorAprendizaje> indicadores = cap.getIndicadores();
                        JSONArray jsonInds= new JSONArray();
                        for(IndicadorAprendizaje ind : indicadores){
                            jsonInds.put(new JSONObject(EntityUtil.objectToJSONString(
                                    new String[]{"indAprId","nomInd"},
                                    new String[]{"id","nom"},
                                    ind
                            )));
                        }
                        jsonCap.put("indicadores",jsonInds);*/
                        jsonCaps.put(jsonCap);
                    }

                }
                jsonComp.put("capacidades",jsonCaps);
                jsonComps.put(jsonComp);
            }
            return WebResponse.crearWebResponseExito("Se listo con exito las competencias",jsonComps);
        }catch (Exception e){
            logger.log(Level.INFO,"listarCompentencias",e);
            return WebResponse.crearWebResponseError("No se pudieron listar las competencias");
        }
    }
}
