/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_area_archivo_central.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.UnidadOrganicaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.UnidadOrganica;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author Jeferson
 */
public class RegistrarUnidadOrganicaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        
        UnidadOrganica uni_org = null;
    //    List<FileJsonObject> lista_series_documentales = new ArrayList<FileJsonObject>();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            int areaID = requestData.getInt("codigo_area");
            String abr = requestData.getString("abrev");
            String nombre = requestData.getString("nom_uni");
            String des = requestData.getString("descripcion");
            
           // JSONArray listaSeries = requestData.getJSONArray("series_doc");
            
            //Registrando la nueva Unidad Organica
            uni_org = new UnidadOrganica(0,areaID,abr,nombre,des);
            
        }catch(Exception e){
             System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la Unidad Organica, datos incorrectos", e.getMessage() );
        }
        // Operaciones con la Base de Datos
        UnidadOrganicaDAO uni_org_dao = (UnidadOrganicaDAO)FactoryDao.buildDao("UnidadOrganicaDAO");
        try{
            uni_org_dao.insert(uni_org);
        }catch(Exception e){
              System.out.println("No se pudo registrar Unidad Organica \n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar Unidad Organica", e.getMessage() );
        }
          JSONObject oResponse = new JSONObject();
           oResponse.put("uni_org_id",uni_org.getUniOrgId());
           return WebResponse.crearWebResponseExito("El registro de la Unidad Organica  se realizo correctamente", oResponse);
        
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
