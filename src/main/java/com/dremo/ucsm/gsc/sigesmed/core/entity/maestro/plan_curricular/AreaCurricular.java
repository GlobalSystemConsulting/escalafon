package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 19/10/2016.
 */
@Entity(name = "AreaCurricularMaestro")
@Table(name = "area_curricular")
public class AreaCurricular implements java.io.Serializable{
    @Id
    @Column(name = "are_cur_id", nullable = false, unique = true)
    @SequenceGenerator(name = "area_curricular_area_cur_id_sequence", sequenceName = "area_curricular_are_cur_id_seq")
    @GeneratedValue(generator = "area_curricular_area_cur_id_sequence")
    private int areCurId;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "dis_cur_id")
    private DisenoCurricular disenoCurr;

    @Column(name = "abr", length = 8)
    private String abr;
    @Column(name = "nom", length = 64)
    private String nom;
    @Column(name = "des", length = 256)
    private String des;
    @Column(name = "est_tal")
    private Boolean estTal;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public AreaCurricular() {
    }

    public AreaCurricular(int areCurId) {
        this.areCurId = areCurId;
    }
    
    public AreaCurricular(String abr, String nom, String des, Boolean estTal) {
        this.abr = abr;
        this.nom = nom;
        this.des = des;
        this.estTal = estTal;

        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public int getAreCurId() {
        return areCurId;
    }

    public void setAreCurId(int areCurId) {
        this.areCurId = areCurId;
    }

    public DisenoCurricular getDisenoCurr() {
        return disenoCurr;
    }

    public void setDisenoCurr(DisenoCurricular disenoCurr) {
        this.disenoCurr = disenoCurr;
    }

    public String getAbr() {
        return abr;
    }

    public void setAbr(String abr) {
        this.abr = abr;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Boolean getEstTal() {
        return estTal;
    }

    public void setEstTal(Boolean estTal) {
        this.estTal = estTal;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
