/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.operacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci.OperacionDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.OperacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaOperacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaOperacionId;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Operacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class InsertarOperacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Operacion nuevaOperacion = null;
       
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            String descripcion = requestData.getString("descripcion");
            String tipo = requestData.getString("tipo");
            JSONArray listaCuentas = requestData.getJSONArray("listaCuentas");
          
            
            nuevaOperacion = new Operacion((short)0, descripcion,tipo.charAt(0), new Date(), wr.getIdUsuario(), 'A');                                    
          
            //leendo las cuentas asociadas
            if(listaCuentas.length() > 0){
                
                for(int i = 0; i < listaCuentas.length();i++){
                    Object o = listaCuentas.get(i);
                    JSONObject bo = (JSONObject)o;

                    int cuentaContableID = bo.getInt("cuentaContableID");                     
                    boolean naturaleza = bo.getBoolean("naturaleza");
                     String numero = bo.getString("numero");
                    String nombre = bo.getString("nombre");

                    
                   
                    nuevaOperacion.getCuentaOperaciones().add(new CuentaOperacion( new CuentaContable(cuentaContableID,numero,nombre), nuevaOperacion, naturaleza));
                }
            }
        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *   Parte de Logica de Negocio
        *   descripcion: El Sitema debe generar el codigo para el nuevo Tipo de Tramite antes de insertar a la BD
        */
       OperacionDao operacionDao = new OperacionDaoHibernate();
      
        
       
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            operacionDao.insert(nuevaOperacion);        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar la operacion ", e.getMessage() );
        }
        //Fin
        
       
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
       
        oResponse.put("operacionID",nuevaOperacion.getOpeId());
        oResponse.put("descripcion",nuevaOperacion.getDesOpe());
        oResponse.put("estado",""+nuevaOperacion.getEstReg());
        
                
         Set<CuentaOperacion> cuentaOperaciones = nuevaOperacion.getCuentaOperaciones();            
            if(  cuentaOperaciones.size() > 0 ){
                JSONArray acuentaOperaciones = new JSONArray();
                for( CuentaOperacion c:cuentaOperaciones ){
                    JSONObject oCuentaOperacion = new JSONObject();
                    oCuentaOperacion.put("cuentaContableID",c.getCuentaContable().getCueConId() );
                    oCuentaOperacion.put("nombre",c.getCuentaContable().getNomCue());
                    oCuentaOperacion.put("numero",c.getCuentaContable().getNumCue());
                    oCuentaOperacion.put("estado",""+c.getCuentaContable().getEstReg() );
                    oCuentaOperacion.put("naturaleza",""+c.getNatCueOpe());
                    acuentaOperaciones.put(oCuentaOperacion);
                }
                oResponse.put("cuentaOperaciones", acuentaOperaciones);
            }
        
        
        return WebResponse.crearWebResponseExito("El registro de la Operacion se realizo correctamente", oResponse);
        //Fin
    }    
  
    
}
