/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.funcion_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FuncionSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class InsertarFuncionSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FuncionSistema nuevoFuncion = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int subModuloID = requestData.getInt("subModuloID");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            String url = requestData.getString("url");
            String clave = requestData.getString("clave");
            String controlador = requestData.getString("controlador");
            String interfaz = requestData.getString("interfaz");
            String urlNom;            
            String estado = requestData.getString("estado");
            
            
            //Añadir archivo logo
            String icono = requestData.optString("urlNom");        
            JSONObject arcJSON = requestData.optJSONObject("archivo");
            FileJsonObject file=null;
            if(arcJSON != null){
                        

                if(arcJSON!=null && arcJSON.length()>0){
                    file=new FileJsonObject(arcJSON,icono);
                }else{
                    return WebResponse.crearWebResponseError("No se pudo leer el archvo, datos incorrectos");  
                }
                BuildFile.buildFromBase64("ico/", file.getName(),file.getData());
                urlNom="../archivos/ico/"+file.getName();
            }
            else{
                urlNom="../recursos/img/module1.png";
            }
            
            nuevoFuncion = new FuncionSistema(0,new SubModuloSistema(subModuloID), nombre, descripcion, url,clave,controlador,interfaz,urlNom,new Date(), 1, estado.charAt(0),null); 


            }catch(Exception e){
                return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
            }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        FuncionSistemaDao funcionDao = (FuncionSistemaDao)FactoryDao.buildDao("FuncionSistemaDao");
        try{
            funcionDao.insert(nuevoFuncion);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar \n"+e);            
            return WebResponse.crearWebResponseError("No se pudo registrar, ", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("funcionID",nuevoFuncion.getFunSisId());
        oResponse.put("fecha",nuevoFuncion.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro de la Funcion se realizo correctamente", oResponse);
        //Fin
    }
    
}
