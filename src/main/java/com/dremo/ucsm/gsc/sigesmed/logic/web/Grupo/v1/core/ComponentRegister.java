/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.Grupo.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Grupo.v1.tx.ActualizarGrupoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Grupo.v1.tx.EliminarGrupoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Grupo.v1.tx.InsertarGrupoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.Grupo.v1.tx.ListarGrupoTx;

/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_WEB);        
        
        //Registrnado el Nombre del componente
        component.setName("grupo");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarGrupo", InsertarGrupoTx.class);
        component.addTransactionDELETE("eliminarGrupo", EliminarGrupoTx.class);
        component.addTransactionPUT("actualizarGrupo", ActualizarGrupoTx.class);
        component.addTransactionGET("listarGrupos", ListarGrupoTx.class);
        
        return component;
    }
}
