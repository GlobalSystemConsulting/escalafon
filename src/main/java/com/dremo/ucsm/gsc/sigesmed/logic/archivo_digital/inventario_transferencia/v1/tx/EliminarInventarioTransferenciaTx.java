/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.InventarioTransferenciaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.InventarioTransferencia;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author admin
 */
public class EliminarInventarioTransferenciaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int inv_tra_id = 0;
        int ser_doc_id = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            inv_tra_id = requestData.getInt("inv_tra_id");
            ser_doc_id = requestData.getInt("ser_doc_id");
        }catch(Exception e){
                System.out.println(e);
           return  WebResponse.crearWebResponseError("No se pudo eliminar el Inventario de Transferencia, datos incorrectos", e.getMessage() );
        }
        
        //Operaciones con la Base de Datos
            InventarioTransferenciaDAO inv_tran_dao = (InventarioTransferenciaDAO)FactoryDao.buildDao("sad.InventarioTransferenciaDAO");
            
        try{
            inv_tran_dao.delete(new InventarioTransferencia(inv_tra_id,ser_doc_id));
            
        }catch(Exception e){
            System.out.println(e);
             return  WebResponse.crearWebResponseError("No se pudo eliminar Inventario de Transferencia, datos incorrectos", e.getMessage() );
        }
         return WebResponse.crearWebResponseExito("El inventario de Transferencia se elimino correctamente");
         
        
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
