/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.cargo.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.cargo.v1.tx.ActualizarCargosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.cargo.v1.tx.AgregarCargosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.cargo.v1.tx.EliminarCargosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.cargo.v1.tx.ListarCargosTx;

/**
 *
 * @author Felipe
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("cargoConfiguracion");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarCargos", ListarCargosTx.class);
        seComponent.addTransactionPUT("actualizarCargos", ActualizarCargosTx.class);
        seComponent.addTransactionPOST("agregarCargos", AgregarCargosTx.class);
        seComponent.addTransactionDELETE("eliminarCargos", EliminarCargosTx.class);
        return seComponent;
    }
    
}
