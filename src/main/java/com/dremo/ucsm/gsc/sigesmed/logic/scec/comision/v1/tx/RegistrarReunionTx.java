package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.Comision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 03/10/16.
 */
public class RegistrarReunionTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(RegistrarReunionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonReu = (JSONObject) wr.getData();
        int idCom = Integer.parseInt(jsonReu.getString("com"));

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(jsonReu.getLong("fec"));
        ReunionComision reunionComision = new ReunionComision(
                calendar.getTime(),
                jsonReu.getString("res"),
                jsonReu.getString("lug"));

        return registrarReunion(idCom,reunionComision);
        //return null;
    }
    private WebResponse registrarReunion(int com,ReunionComision reunionComision){
        try{
            ReunionComisionDao reunionComisionDao = (ReunionComisionDao) FactoryDao.buildDao("scec.ReunionComisionDao");
            ComisionDao comisionDao = (ComisionDao) FactoryDao.buildDao("scec.ComisionDao");
            reunionComision.setComision(comisionDao.buscarPorId(com));

            reunionComisionDao.insert(reunionComision);

            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            calendar.setTime(reunionComision.getFecReu());
            return WebResponse.crearWebResponseExito("Se realizo correctame el registro",WebResponse.OK_RESPONSE).setData(
                    new JSONObject(
                            EntityUtil.objectToJSONString(new String[]{"reuId","fecReu", "resConReu", "lugReu"}, new String[]{"cod","fec", "res", "lug"}, reunionComision)
            ));
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarReunion",e);
            return WebResponse.crearWebResponseError("No se pudo registrar a la comision",WebResponse.BAD_RESPONSE);
        }
    }
}
