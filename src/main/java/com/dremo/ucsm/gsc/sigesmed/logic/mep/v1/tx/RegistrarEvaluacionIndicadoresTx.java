/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.EvaluacionPersonalDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.DetalleResumenEvaluacionPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.IndicadorValor;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.IndicadoresEvaluarPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ResumenEvaluacionPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author geank
 */
public class RegistrarEvaluacionIndicadoresTx extends MepGeneralTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();

        double total = data.getDouble("total");
        String des = data.getString("des");
        char etapa = data.getString("etapa").charAt(0);
        int idTrabajador = data.getInt("employee");
        Set<IndicadorValor> indicadores = crearIndicadorValor(data.getJSONArray("inds"));
        Set<DetalleResumenEvaluacionPersonal> detalleResumen = crearDetalleResumen(data.getJSONArray("detalle"));
        ResumenEvaluacionPersonal resumen = new ResumenEvaluacionPersonal(new Date(), BigDecimal.valueOf(total),etapa,des);
        resumen.setFecMod(new Date());
        resumen.setEstReg('A');
        return registrarIndicadores(resumen,idTrabajador, indicadores,detalleResumen);
    }
    private WebResponse registrarIndicadores(ResumenEvaluacionPersonal resumen,int trabajadorId, Set<IndicadorValor> indicadores, Set<DetalleResumenEvaluacionPersonal> detalleResumen){
        EvaluacionPersonalDaoHibernate epdh = new EvaluacionPersonalDaoHibernate();
        Trabajador trabajador = epdh.getTrabajadorById(trabajadorId);
        if(trabajador != null){
            resumen.setTrabajador(trabajador);
            if(epdh.registrarEvaluacionFicha(resumen,indicadores,detalleResumen)){
                WebResponse response = formWebResponse(true);
                JSONArray data = new JSONArray();
                data.put(resumen.getResEvaPerId());
                response.setData(data);
                return response;
            }else{
                return formWebResponse(false);
            }
        }
        else{
            return formWebResponse(false);
        }
    }

    private Set<IndicadorValor> crearIndicadorValor(JSONArray indicadores){
        Set<IndicadorValor> result = new HashSet<>();
        for(int i = 0 ; i < indicadores.length(); i++){
            JSONObject obj = indicadores.getJSONObject(i);
            result.add(new IndicadorValor(obj.getInt("id"),obj.getInt("val")));
        }
        return result;
    }
    private Set<DetalleResumenEvaluacionPersonal> crearDetalleResumen(JSONArray resumenes){
        Set<DetalleResumenEvaluacionPersonal> result= new HashSet<>();
        for(int i = 0 ; i < resumenes.length(); i++){
            JSONObject obj = resumenes.getJSONObject(i);
            //double subTot, double pun, Character tip, String nom
            result.add(new DetalleResumenEvaluacionPersonal(obj.getDouble("subt"),
                    obj.getDouble("dt"),obj.getString("tip").charAt(0),obj.getString("nom"),obj.optInt("con",0)));
        }
        return result;
    }
}
