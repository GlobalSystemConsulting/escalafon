/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.directorio_externo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.DirectorioExternoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.DirectorioExterno;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class InsertarDirectorioExternoTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        DirectorioExterno dex = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            String dexDir = requestData.optString("dexDir");
            String dexEma = requestData.optString("dexEma");
            String dexNom = requestData.optString("dexNom");            
            Integer dexTel = (requestData.optString("dexTel").isEmpty()? null: Integer.parseInt(requestData.optString("dexTel")));
            
            dex = new DirectorioExterno(dexNom, dexDir, dexTel, dexEma);
            
        
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
                
        /*
        *  Parte para la operacion en la Base de Datos
        */
        DirectorioExternoDao dexDao = (DirectorioExternoDao)FactoryDao.buildDao("di.DirectorioExternoDao");
        try{
            dexDao.insert(dex);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("dexId",dex.getDexId());
        //oResponse.put("fecha",dex.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro del Directorio Externo se realizo correctamente", oResponse);
        //Fin
    }
    
}
