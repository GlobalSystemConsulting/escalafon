package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.ComentarioTemaCapacitacion;
import java.util.List;
import org.json.JSONObject;

public interface ComentarioTemaCapacitacionDao extends  GenericDao<ComentarioTemaCapacitacion> {
    List<ComentarioTemaCapacitacion> listarComentarios(int temCod);
    List<ComentarioTemaCapacitacion> listarRespuestas(int comCod);
    JSONObject obtenerSesion(int usuId);
    ComentarioTemaCapacitacion buscarPorId(int comCod);
}
