/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class EliminarComparticionTx implements ITransaction {
    
    //Codigo del Item File Detalle
    Integer ideIfd = 0;
    Integer ideDoc=0;
    @Override   
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            ideIfd = requestData.getInt("ideIfd");
            ideDoc=requestData.getInt("ideDoc");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo leer datos para Eliminar Comparticion");
        }
        
        ItemFileDetalleDao ifdDao = (ItemFileDetalleDao)FactoryDao.buildDao("rdg.ItemFileDetalleDao");
        try{
            ifdDao.eliminarComparticionPorDocYUsu(ideDoc,ideIfd);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo eliminar la comparticion");
        }
        
        
        return WebResponse.crearWebResponseExito("Se elimino con exito la comparticion");
    }
    
}
