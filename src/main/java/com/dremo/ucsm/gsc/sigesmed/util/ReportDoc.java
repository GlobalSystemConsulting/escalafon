/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

import com.itextpdf.io.source.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Header;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Matricula;
import java.util.ArrayList;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Jerson Herrera
 */
public class ReportDoc {

    private static final String URI_WORD;

    private XWPFDocument document;

    static {

        URI_WORD = "data:application/msword;base64,";

    }

    public ReportDoc() {
        document = new XWPFDocument();
    }

    public void cabeceraTabla(ArrayList<String> object) {

        for (int i = 0; i < object.size(); i++) {
            XWPFParagraph parrafo = document.createParagraph();
            parrafo.setAlignment(ParagraphAlignment.LEFT);
            XWPFRun r = parrafo.createRun();
            r.setText(object.get(i) + object.get(i+1));
            i++;
        }
        
    }

    public void contenido(ArrayList<String> array) {

        for (String array1 : array) {
            XWPFParagraph parrafo = document.createParagraph();
            parrafo.setAlignment(ParagraphAlignment.LEFT);
            XWPFRun r = parrafo.createRun();
            r.setText(array1);
        }
    }
    public void piePagina(ArrayList<String> array) {

        for (String array1 : array) {
            XWPFParagraph parrafo = document.createParagraph();
            parrafo.setAlignment(ParagraphAlignment.LEFT);
            XWPFRun r = parrafo.createRun();
            r.setText(array1);
        }
    }

    public void setImageTop(String strPathImage) throws FileNotFoundException, InvalidFormatException, IOException {
        XWPFParagraph title = document.createParagraph();
        XWPFRun run = title.createRun();
        //run.setText("Fig.1 A Natural Scene");
        //run.setBold(true);
        title.setAlignment(ParagraphAlignment.LEFT);

        String imgFile = strPathImage;
        try (FileInputStream is = new FileInputStream(imgFile)) {
            run.addBreak();
            run.addPicture(is, XWPFDocument.PICTURE_TYPE_PNG, imgFile, Units.toEMU(100), Units.toEMU(40)); // 200x200 pixels

        }

    }

    public void setDate() {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
        String fecha = formato.format(new Date());

        XWPFParagraph parrafo = document.createParagraph();
        parrafo.setAlignment(ParagraphAlignment.RIGHT);
        XWPFRun r = parrafo.createRun();

        r.setText(fecha);

    }

    public void closeReport() {
        try {
            document.close();

        } catch (IOException ex) {
            Logger.getLogger(ReportDoc.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String encodeToBase64() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] doc = null;
        try {
            document.write(baos);
            doc = baos.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(ReportDoc.class.getName()).log(Level.SEVERE, null, ex);
        }
        return URI_WORD + Base64.encodeBase64String(doc);
    }
}
