/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author carlos
 */
public class VerJustificacionInasistenciaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {   
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Trabajador trab;
       Date fechaInasistencia;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            Integer  idTrab = requestData.getInt("idTrab");
            String  fecha = requestData.getString("fecha");
            
            trab=new Trabajador(idTrab);
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
            fechaInasistencia=sdfDate.parse(fecha);
            
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo verificar los Datos" );
        }
        /*
        *  Parte para la operacion en la Base de Datos
        */
        JSONObject oRes = new JSONObject();
        AsistenciaDao asistenciaDao = (AsistenciaDao) FactoryDao.buildDao("cpe.AsistenciaDao");
        
        try {
            Inasistencia ina=asistenciaDao.buscarInasistencia(trab,fechaInasistencia);
            
            oRes.put("id", ina.getInaId());
            if(ina.getJustificacion()==null)
            {
                oRes.put("justificacion", "");
            }
            else
            {
                JSONObject oJusti = new JSONObject();
                oJusti.put("id", ina.getJustificacion().getJusInaId());
                oJusti.put("doc", ina.getJustificacion().getJusInaDoc());
                oJusti.put("tipo", ina.getJustificacion().getEstReg());
                oJusti.put("obs", ina.getJustificacion().getJusInaObs());
                oRes.put("justificacion", oJusti);
            }

        } catch (Exception e) {
            System.out.println("\n" + e);
            return WebResponse.crearWebResponseError("No se pudo buscar la Inasistencia", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Consulta con exito", oRes);
        //validar el usuario

    }
}
