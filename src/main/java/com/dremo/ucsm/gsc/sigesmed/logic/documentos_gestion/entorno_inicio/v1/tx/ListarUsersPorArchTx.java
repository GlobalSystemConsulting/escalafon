/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFileDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ListarUsersPorArchTx implements ITransaction{
    JSONArray listUsers = new JSONArray();
    Integer coddoc = 0;
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            //Consiguiendo el codigo de documento para buscar los usuarios a los cuales se comparte
            JSONObject requestData = (JSONObject)wr.getData();
            coddoc = requestData.getInt("coddoc");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo leer los datos enviados al listar Usuarios Por File"+e.getMessage());
        }
        
        //Lectura con base de datos
        
        //Buscar el documento de gestion
        ItemFileDao  iteFilDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
        ItemFile iteFilActual = iteFilDao.buscarPorID(coddoc);
        
        ItemFileDetalleDao iteFilDetDao = (ItemFileDetalleDao)FactoryDao.buildDao("rdg.ItemFileDetalleDao");
        if(iteFilActual != null){
        List<ItemFileDetalle> listItemDet = iteFilDetDao.buscarByArch(iteFilActual);

        PersonaDao persDao = (PersonaDao)FactoryDao.buildDao("PersonaDao");
        TrabajadorDao trabDao= (TrabajadorDao)FactoryDao.buildDao("rdg.TrabajadorDao");
        //Una vez con la lista de itemFileDetalle con sus permisos pasamos a recolectar los usuarios
        for(ItemFileDetalle ifd : listItemDet){
            //Agremos el codigo de usuario a la lista
            JSONObject ob = new JSONObject();
            //Ide de Permiso
            ob.put("ideIfd",ifd.getIfdIde());
//            ob.put("idePer", ifd.getIfdPrmIde().getPrmIde());
            //Se asume que el Codigo de un Trabajador es el mismo al de la Persona
            //por lo cual todas las personas deberan ser trabajadores
            //Persona p = persDao.buscarPorCod(ifd.getIfdUsu());
            
            
            JSONObject user = new JSONObject();
            UsuarioSession session=trabDao.getByID(ifd.getIfdUsu());                                                       
            user.put("usuarioID",session.getUsuario().getUsuId());
            user.put("nombre",session.getPersona().getNom());
            user.put("apellidoPat",session.getPersona().getApePat());
            user.put("apellidoMat",session.getPersona().getApeMat());
            user.put("nombreCompleto", session.getPersona().getNom() +" "+ session.getPersona().getApePat() +" "+ session.getPersona().getApeMat());
            user.put("rolID",session.getRol().getRolId() );
            user.put("rol",session.getRol().getNom() );
            user.put("sessionID",session.getUsuSesId() );
            user.put("organizacion",session.getOrganizacion().getNom());
            user.put("organizacionID",session.getOrganizacion().getOrgId());               
                
            

            
     
            //Datos de Usuario
            ob.put("usuario", user);
            
            //Actualizando datos de respuesta
            listUsers.put(ob);
        }
        }else{
            return WebResponse.crearWebResponseError("No se pudo encontrar el doc para encontrar su detalle");
        }

        //Como identificador de la lista insertamos el codigo de documento
        return WebResponse.crearWebResponseExito("Se pudo listar los usuarios por Arch",listUsers);
    }
    
}
