package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PerfilCapacitador;
import java.util.List;

public interface PerfilCapacitadorDao extends GenericDao<PerfilCapacitador> {
    List<PerfilCapacitador> listarPerfilesCursoCapacitacion(int curCapId);
    PerfilCapacitador buscarPorId(int idPer);
}
