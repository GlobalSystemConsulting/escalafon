package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionDesarrolloDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionDesarrollo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EditarEvaluacionDesarrolloTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(EditarEvaluacionDesarrolloTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            EvaluacionDesarrolloDao evaluacionDesarrolloDao = (EvaluacionDesarrolloDao) FactoryDao.buildDao("capacitacion.EvaluacionDesarrolloDao");
            EvaluacionDesarrollo evaDes = evaluacionDesarrolloDao.buscarPorId(data.getInt("des"), data.getInt("doc"));            
            JSONObject state = new JSONObject();
            state.put("state", false);
            
            if(data.getString("est").charAt(0) == 'E' || data.getString("est").charAt(0) == 'C') {
                evaDes.setFecEnt(new Date());
                evaDes.setNotPar(data.getDouble("not"));
                state.put("state", true);
                evaDes.setEstReg('C');
                evaluacionDesarrolloDao.update(evaDes);
            }
            
            return WebResponse.crearWebResponseExito("Exito al editar la evaluacion del desarrollo", WebResponse.OK_RESPONSE).setData(state);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarComentario", e);
            return WebResponse.crearWebResponseError("Error al editar la evaluacion del desarrollo", WebResponse.BAD_RESPONSE);
        }        
    }    
}
