package com.dremo.ucsm.gsc.sigesmed.logic.scec.comision.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.IntegranteComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.Comision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.IntegranteComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 02/10/16.
 */
public class RegistrarIntegranteComisionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(RegistrarIntegranteComisionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonData = (JSONObject) wr.getData();
        String dni = jsonData.getString("dni");
        String comId = jsonData.getString("com");
        return registrarIntegrante(dni,Integer.parseInt(comId));
    }
    public WebResponse registrarIntegrante(String dni,int com){
        try{
            PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("PersonaDao");
            IntegranteComisionDao integranteComisionDao = (IntegranteComisionDao) FactoryDao.buildDao("scec.IntegranteComisionDao");
            ComisionDao comisionDao = (ComisionDao)FactoryDao.buildDao("scec.ComisionDao");
            CargoComisionDao cargoComisionDao = (CargoComisionDao)FactoryDao.buildDao("scec.CargoComisionDao");

            Persona persona = personaDao.buscarPorDNI(dni);
            Comision comision = comisionDao.buscarPorIdConIntegrantes(com);
            CargoComision cargoComision = cargoComisionDao.buscarCargoDefecto();

            IntegranteComision integranteComision = new IntegranteComision(persona,comision,cargoComision,new Date(),null);
            integranteComision.setEstReg('A');
            integranteComision.setUsuMod(1);
            integranteComision.setFecMod(new Date());

            JSONObject jsonIntegrante = new JSONObject(EntityUtil.objectToJSONString(new String[]{"perId","dni","nom","apeMat","apePat","num1","num2","email"},new String[]{"cod","dni","nom","apem","apep","cel","fij","ema"},persona));
            jsonIntegrante.put("car",cargoComision.getNomCar());

            integranteComisionDao.insert(integranteComision);

            return WebResponse.crearWebResponseExito("Se registro correctamente al integrante", WebResponse.OK_RESPONSE).setData(jsonIntegrante);
        } catch (Exception e){
            logger.log(Level.SEVERE,"registrarIntegrante",e);
            return WebResponse.crearWebResponseError("Error al registrar el integrante",WebResponse.BAD_RESPONSE);
        }
    }
}
