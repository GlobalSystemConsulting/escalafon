package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.MaterialRecursoProgramacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 15/12/2016.
 */
public class EditarSesionAprendizajeTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EditarSesionAprendizajeTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idSesion = data.getInt("id");
        return editarSesionAprendizaje(idSesion, data);
    }

    private WebResponse editarSesionAprendizaje(int idSesion, JSONObject data) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            SesionAprendizaje sesion = sesionDao.buscarSesionById(idSesion);
            if(sesion != null){
                String tit = data.optString("tit",sesion.getTit());
                long fecIni = data.optLong("ini", sesion.getFecIni().getTime());
                long fecFin = data.optLong("fin",sesion.getFecFin().getTime());
                int numSesion = data.optInt("numSes",sesion.getNumSes());

                sesion.setTit(tit);
                sesion.setFecIni(new Date(fecIni));
                sesion.setFecFin(new Date(fecFin));
                sesion.setNumSes(numSesion);

                sesionDao.update(sesion);
                //unidadDao.editarMaterial(material);
                return WebResponse.crearWebResponseExito("Se edito correctamente la sesion");
            }else{
                return WebResponse.crearWebResponseError("no se puede encontrar la sesion");
            }
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarSesionAprendizaje",e);
            return WebResponse.crearWebResponseError("error al editar la sesion");
        }
    }
}
