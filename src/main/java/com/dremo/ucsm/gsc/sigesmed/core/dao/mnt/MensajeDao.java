/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Mensaje;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface MensajeDao extends GenericDao<Mensaje>{
    public int buscarSessionPorIdUsuario(int usuID);
    public List<Mensaje> buscarMensajesEnviadosPorSession(int sessionID);
    public List<Mensaje> buscarMensajesRecibidosPorSession(int sessionID);
    public void archivarMensajeAutor(int menID);
    public void archivarMensajeDestinatario(int menID);
    public void eliminarMensajeAutor(int menID);
    public void eliminarMensajeDestinatario(int menID);
}
