/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.categoria.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ListarCategoriasTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarCategoriasTx.class.getName());
    
    @Override
        public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<Categoria> categorias = null;
        CategoriaDao categoriaDao = (CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
        
        try{
            categorias = categoriaDao.listarxCategoria();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar categorias",e);
            System.out.println("No se pudo listar las categorias.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas categorias.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Categoria c: categorias ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("catId",c.getCatId());
            if(c.getPlanilla()==null){
                oResponse.put("plaCat","");
                oResponse.put("plaNom","");
            }
            else{
                oResponse.put("plaCat",c.getPlanilla().getPlaId());
                oResponse.put("plaNom", c.getPlanilla().getNomPla());
            }
            oResponse.put("abrCat",c.getAbrCat());
            oResponse.put("nomCat",c.getNomCat());
            oResponse.put("fecMod",c.getFecMod());
            oResponse.put("UsuMod",c.getUsuMod());
            oResponse.put("estReg",c.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las categorias fueron listadas exitosamente", miArray);
    }
    
}
