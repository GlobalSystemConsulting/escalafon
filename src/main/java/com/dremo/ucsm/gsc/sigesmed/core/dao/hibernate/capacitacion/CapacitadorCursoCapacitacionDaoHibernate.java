package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CapacitadorCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CapacitadorCursoCapacitacion;

public class CapacitadorCursoCapacitacionDaoHibernate extends GenericDaoHibernate<CapacitadorCursoCapacitacion> implements CapacitadorCursoCapacitacionDao {
    
}
