package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Lengua;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarLenguasTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        GenericMMIDaoHibernate<Lengua> hb = new GenericMMIDaoHibernate<>();
        JSONArray lenguas = new JSONArray();

        List lenguasSQL;

        try {
            lenguasSQL = hb.buscarTodos(Lengua.class);

            for (Object obj : lenguasSQL) {
                Lengua lengua = (Lengua) obj;
                JSONObject temp = new JSONObject();
                temp.put("lenId", lengua.getLenId());
                temp.put("lenNom", lengua.getLenNom());
                lenguas.put(temp);
            }

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al cargar las lenguas! ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Listaron las lenguas", lenguas);
    }

}
