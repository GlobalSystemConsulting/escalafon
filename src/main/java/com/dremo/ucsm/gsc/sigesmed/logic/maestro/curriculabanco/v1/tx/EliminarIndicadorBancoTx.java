package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/10/2016.
 */
public class EliminarIndicadorBancoTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EliminarIndicadorBancoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return eliminarIndicador(data.getInt("cod"));
    }

    private WebResponse eliminarIndicador(int cod) {
        try {
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            IndicadorAprendizaje indicador = indicadorDao.buscarPorId(cod);
            indicadorDao.delete(indicador);
            return WebResponse.crearWebResponseExito("Exito al realizar la eliminacion", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarCapacitacion", e);
            return WebResponse.crearWebResponseError("Error al realizar la eliminacion", e.getMessage());
        }
    }
}
