/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author felipe
 */
@Entity
@Table(name = "trabajador_organigrama_detalle", schema ="administrativo")
@IdClass(TrabajadorOrganigramaDetallePK.class)
public class TrabajadorOrganigramaDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
   
    @Id
    @Column(name = "tra_id")
    private Integer traId;
    
    @Id
    @Column(name = "orgi_id")
    private Integer orgiId;
    
    @JoinColumn(name = "tra_id", referencedColumnName = "tra_id", insertable = false, updatable = false)
    @ManyToOne(fetch=FetchType.LAZY)
    private Trabajador trabajador;
    
    @JoinColumn(name = "orgi_id", referencedColumnName = "orgi_id", insertable = false, updatable = false)
    @ManyToOne(fetch=FetchType.LAZY)
    private Organigrama organigrama;
    
    
    @Column(name = "ubi_id")
    private Integer ubiId;
    
    @Column(name = "pla_id")
    private Integer plaId;
    
    @Column(name = "cat_id")
    private Integer catId;
    
    @Column(name = "car_id")
    private Integer carId;
    
    @Column(name = "tip_doc")
    private Integer tipDoc;
    
    @Size(max = 2147483647)
    @Column(name = "nom_doc")    
    private String nomDoc;
    
    @Column(name = "fec_doc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecDoc;

    public TrabajadorOrganigramaDetalle() {
    }

    public TrabajadorOrganigramaDetalle(int traId, int orgiId, int ubiId, int plaId, int catId, int carId) {
        this.traId = traId;
        this.orgiId = orgiId;
        this.ubiId = ubiId;
        this.plaId = plaId;
        this.catId = catId;
        this.carId = carId;
    }

    public int getTraId() {
        return traId;
    }

    public void setTraId(Integer traId) {
        this.traId = traId;
    }

    public Integer getOrgiId() {
        return orgiId;
    }

    public void setOrgiId(Integer orgiId) {
        this.orgiId = orgiId;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    public Organigrama getOrganigrama() {
        return organigrama;
    }

    public void setOrganigrama(Organigrama organigrama) {
        this.organigrama = organigrama;
    }  
    

    public Integer getUbiId() {
        return ubiId;
    }

    public void setUbiId(Integer ubiId) {
        this.ubiId = ubiId;
    }

    public Integer getPlaId() {
        return plaId;
    }

    public void setPlaId(Integer plaId) {
        this.plaId = plaId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getTipDoc() {
        return tipDoc;
    }

    public void setTipDoc(Integer tipDoc) {
        this.tipDoc = tipDoc;
    }

    public String getNomDoc() {
        return nomDoc;
    }

    public void setNomDoc(String nomDoc) {
        this.nomDoc = nomDoc;
    }

    public Date getFecDoc() {
        return fecDoc;
    }

    public void setFecDoc(Date fecDoc) {
        this.fecDoc = fecDoc;
    }

    @Override
    public String toString() {
        return "TrabajadorOrganigramaDetalle{" + "traId=" + traId + ", orgiId=" + orgiId + ", ubiId=" + ubiId + ", plaId=" + plaId + ", catId=" + catId + ", carId=" + carId + '}';
    }

    
}
