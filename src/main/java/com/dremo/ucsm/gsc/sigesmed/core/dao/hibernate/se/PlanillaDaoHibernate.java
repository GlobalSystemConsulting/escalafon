/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PlanillaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Planilla;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Felipe
 */
   
public class PlanillaDaoHibernate  extends GenericDaoHibernate<Planilla> implements PlanillaDao{

    @Override
    public List<Planilla> listarxPlanilla() {
        List<Planilla> planillas = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT p from Planilla as p "
                    + "WHERE p.estReg='A'";
            Query query = session.createQuery(hql);
            planillas = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los planillas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las planillas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return planillas;
    }
    @Override
    public Planilla buscarPorId(Integer plaId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Planilla a = (Planilla)session.get(Planilla.class, plaId);
        session.close();
        return a;
    }


}