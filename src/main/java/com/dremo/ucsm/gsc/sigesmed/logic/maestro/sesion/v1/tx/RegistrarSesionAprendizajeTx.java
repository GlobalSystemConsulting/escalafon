package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 29/11/2016.
 */
public class RegistrarSesionAprendizajeTx implements ITransaction {
    private static Logger logger = Logger.getLogger(RegistrarSesionAprendizajeTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{

            JSONObject data = (JSONObject) wr.getData();
            int uniId = data.getInt("unidad");
            String tipUnidad = data.getString("tipUnidad");
            String tit = data.getString("tit");
            long fecIni = data.getLong("ini");
            long fecFin = data.getLong("fin");
            int numSesion = data.getInt("numSes");
            SesionAprendizaje sesionApr = new SesionAprendizaje(tit,new Date(fecIni),new Date(fecFin),numSesion);
            return registrarSesion(uniId,tipUnidad,sesionApr);
        }catch (Exception e){
            logger.log(Level.INFO,"execute",e);
            return WebResponse.crearWebResponseError("No se pudo registrar la sesion de aprendizaje");
        }

    }

    private WebResponse registrarSesion(int uniId,String tipUnidad, SesionAprendizaje sesionApr) throws Exception{
        UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
        SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");

        UnidadDidactica unidad = unidadDao.buscarUnidadDidactica(uniId, tipUnidad);
        sesionApr.setNivAva("I");
        sesionApr.setUnidadDidactica(unidad);
        sesionDao.insert(sesionApr);

        return WebResponse.crearWebResponseExito("Se registro la session correctamente", new JSONObject(EntityUtil.objectToJSONString(
                new String[]{"sesAprId","nivAva"},
                new String[]{"id","est"},
                sesionApr
        )));
    }
}
