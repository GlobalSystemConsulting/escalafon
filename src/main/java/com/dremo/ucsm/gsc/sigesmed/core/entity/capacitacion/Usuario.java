package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "UsuarioCapacitacion")
@Table(name = "usuario", schema = "public")
public class Usuario implements Serializable {

    @Id
    @Column(name = "usu_id", unique = true, nullable = false)
    private int usuId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id", nullable = false, updatable = false, insertable = false)
    private Persona persona;

    @Column(name = "nom", unique = true, nullable = false, length = 32)
    private String nom;

    @Column(name = "pas", nullable = false, length = 16)
    private String pas;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_cre", nullable = false)
    private Date fecCre;

    @Column(name = "usu_mod", nullable = false)
    private int usuMod;

    @Column(name = "est_reg", nullable = false, length = 1)
    private Character estReg;

    public Usuario() {
    }

    public Usuario(Persona persona, String nom, String pas) {
        this.persona = persona;
        this.usuId = persona.getPerId();
        this.nom = nom;
        this.pas = pas;
        this.fecCre = new Date();
        this.usuMod = persona.getPerId();
        this.estReg = 'A';
    }

    public int getUsuId() {
        return usuId;
    }

    public void setUsuId(int usuId) {
        this.usuId = usuId;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPas() {
        return pas;
    }

    public void setPas(String pas) {
        this.pas = pas;
    }

    public Date getFecCre() {
        return fecCre;
    }

    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
