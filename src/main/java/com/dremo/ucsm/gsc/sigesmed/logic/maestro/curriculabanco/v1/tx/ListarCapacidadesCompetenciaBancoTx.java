package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaCapacidadDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CapacidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaCapacidad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/10/2016.
 */
public class ListarCapacidadesCompetenciaBancoTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(ListarCapacidadesCompetenciaBancoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return listarCapacidades(data.getInt("cod"));
    }

    private WebResponse listarCapacidades(int cod) {
        try{
            CompetenciaCapacidadDao comCapDao = (CompetenciaCapacidadDao) FactoryDao.buildDao("maestro.plan.CompetenciaCapacidadDao");
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            List<CompetenciaCapacidad> comCaps = comCapDao.buscarCapacidadesDeCompetencia(cod);
            JSONArray capacidades = new JSONArray();
            for(CompetenciaCapacidad comCap : comCaps){
                CapacidadAprendizaje capacidad = comCap.getCap();
                JSONObject obj = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"capId","nom","des"},
                        new String[]{"cod","nom","des"},
                        capacidad
                ));
                List<IndicadorAprendizaje> indicadores = capacidad.getIndicadores();
                JSONArray jsonInds = new JSONArray();
                for(IndicadorAprendizaje indicador : indicadores){
                    if(indicador.getEstReg() != 'E'){
                        JSONObject jsoInd = new JSONObject(EntityUtil.objectToJSONString(
                                new String[]{"indAprId","nomInd","eva"},
                                new String[]{"cod","nom","eva"},
                                indicador
                        ));
                        jsonInds.put(jsoInd);
                    }
                }
                obj.put("inds",jsonInds);
                capacidades.put(obj);
            }
            return WebResponse.crearWebResponseExito("Exito al listar las capacidades", capacidades);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarCapacidades",e);
            return WebResponse.crearWebResponseError("No se pudieron listar las capacidades",e.toString());
        }
    }
}
