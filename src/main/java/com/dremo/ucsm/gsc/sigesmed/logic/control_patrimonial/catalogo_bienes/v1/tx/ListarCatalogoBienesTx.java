/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CatalogoBienesDAO;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author Administrador
 */
public class ListarCatalogoBienesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONArray miArray = new JSONArray();
        try{
            List<CatalogoBienes>cat_bienes = null;
            CatalogoBienesDAO cat_bie_dao = (CatalogoBienesDAO)FactoryDao.buildDao("scp.CatalogoBienesDAO");
            cat_bienes =  cat_bie_dao.listarCatalogo();
            
            for(CatalogoBienes catalogo :cat_bienes ){ 
                JSONObject oResponse = new JSONObject();
                oResponse.put("cat_bie_id",catalogo.getCat_bie_id());
                
                oResponse.put("gru_gen_id",catalogo.getFamilia().getClase_generica().getGrupo().getGru_gen_id());
                oResponse.put("gru_gen_nom",catalogo.getFamilia().getClase_generica().getGrupo().getNom());
                oResponse.put("cla_gen_id",catalogo.getFamilia().getClase_generica().getCla_gen_id());
                oResponse.put("cla_gen_nom",catalogo.getFamilia().getClase_generica().getNom());
                oResponse.put("fam_gen_id",catalogo.getFamilia().getFam_gen_id());
                oResponse.put("fam_gen_nom",catalogo.getFamilia().getNom());
                
                oResponse.put("uni_med_id",catalogo.getUni_med_id());
                oResponse.put("uni_med_nom",catalogo.getUnidad_medida().getNom());
                oResponse.put("cod",catalogo.getCod());
                oResponse.put("id_cat",catalogo.getCat_bie_id());
                oResponse.put("den_bie",catalogo.getDen_bie());
                oResponse.put("fec_cre",catalogo.getFec_cre());
                oResponse.put("num_res",catalogo.getNum_res());
                miArray.put(oResponse);
            }       
             
        }catch(Exception e){
            System.out.println("No se pudo Listar el Catalogo de bienes\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar el Catalogo de Bienes", e.getMessage() );

        }
        
         return WebResponse.crearWebResponseExito("Se Listo el Catalogo de Bienes Correctamente",miArray); 

    }  
}
