/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organizacion;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Felipe
 */
public class OrganizacionDaoHibernate extends GenericDaoHibernate<Organizacion> implements OrganizacionDao{

    @Override
    public List<Organizacion> listarxOrganizacion() {
        List<Organizacion> organizaciones = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT o from OrganizacionSE as o "
                    + "WHERE o.estReg='A'";
            Query query = session.createQuery(hql);
            organizaciones = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los organizaciones \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las organizaciones \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return organizaciones;
    }    
}
