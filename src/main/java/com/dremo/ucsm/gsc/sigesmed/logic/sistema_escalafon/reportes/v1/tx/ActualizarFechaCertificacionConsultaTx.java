/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaCertificadaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Direccion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarFechaCertificacionConsultaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ActualizarFechaCertificacionConsultaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            JSONObject data = (JSONObject)wr.getData();  
            System.out.println(data);
            
            JSONObject oResponse = new JSONObject();  
            oResponse.put("isVerified", false);
            int usu_id = data.optInt("usu_id");
            String pas = data.optString("pas");
            //ConsultaCertificadaDao consultaCertificadaDao = (ConsultaCertificadaDao) FactoryDao.buildDao("ConsultaCertificadaDao");
            //boolean response = consultaCertificadaDao.verificarPassword(usu_id,pas);
            /////////////////////////////CERTIFICAR CONSULTA///////////////////////////
            int rConsultaCertificada = data.optInt("con_cer_id");
            ConsultaCertificadaDao consultaCertificadaDao = (ConsultaCertificadaDao) FactoryDao.buildDao("ConsultaCertificadaDao");
            ConsultaCertificada consultaCertificada = consultaCertificadaDao.buscarPorID(rConsultaCertificada);
            boolean verificado = consultaCertificadaDao.verificarPassword(usu_id,pas);
            Date fecha_actual = new Date();
            consultaCertificada.setFecCertificacion(fecha_actual);
            consultaCertificada.setEstReg('C');
            consultaCertificada.setIdUsuCer(usu_id);
            if(verificado){
                consultaCertificadaDao.update(consultaCertificada);
                oResponse.put("isVerified", true);
                ///////////AUDITORIA/////////////
                AgregarOperacionAuditoria.agregarAltaAuditoria("Se Certifico Consulta", "Id consulta certificada: "+data.optInt("con_cer_id"), wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
                ///////////AUDITORIA/////////////
            }
            
            
            //JSONObject consultaCertificadaRes = new JSONObject();
            //oResponse.put("consultaCertificada", true);
            
            
            System.out.println(oResponse);
            
            return WebResponse.crearWebResponseExito("Fecha de consulta certificada actualizada exitosamente", oResponse);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar Fecha certificacion de consulta",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar consulta certificacion, datos incorrectos", e.getMessage());
        } 
    }
    
}

