/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.desplazamiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarDesplazamientoTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(AgregarDesplazamientoTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Desplazamiento desplazamiento = null;
        
        Integer ficEscId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd/MM/yyyy");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            ficEscId = requestData.getInt("ficEscId");
            Character tip = requestData.getString("tip").charAt(0);
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = null;
            if(requestData.getString("fecDoc").length() > 0){
                fecDoc = sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            }
            String insEdu = requestData.getString("insEdu");
            String car = requestData.getString("car");
            String jorLab = requestData.getString("jorLab");
            Date fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = null;
            if(requestData.getString("fecTer").length() > 0){
                fecTer = sdi.parse(requestData.getString("fecTer").substring(0, 10));
            }
            Date fecDocTer = null;
            if(requestData.getString("fecDocTer").length() > 0){
                fecDocTer = sdi.parse(requestData.getString("fecDocTer").substring(0, 10));
            }
            String numDocTer = "";
            String tipdoc = requestData.getString("tipDoc");
            if(requestData.getString("numDocTer").length() > 0){
                numDocTer = requestData.getString("numDocTer");
            }
            
            String motRet = "";
            
            if(requestData.getString("motRet").length() > 0){
                motRet = requestData.getString("motRet");
            }
            
            desplazamiento = new Desplazamiento(new FichaEscalafonaria(ficEscId), tip, numDoc, fecDoc, tipdoc, insEdu, car, jorLab, fecIni, fecTer, wr.getIdUsuario(), new Date(), 'A', fecDocTer, numDocTer);
            desplazamiento.setMotRet(motRet);
            //System.out.println(desplazamiento);
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo desplazamiento",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        DesplazamientoDao publicacionDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
        try {
            publicacionDao.insert(desplazamiento);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego desplazamiento", "Detalle: "+ficEscId+" ficha ", wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nuevo desplazamiento",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("desId", desplazamiento.getDesId());
        oResponse.put("tip", desplazamiento.getTip());
        oResponse.put("tipDes", "");
        oResponse.put("numDoc", null != desplazamiento.getNumDoc() ? desplazamiento.getNumDoc():"");
        oResponse.put("fecDoc", null != desplazamiento.getFecDoc() ? sdo.format(desplazamiento.getFecDoc()) : "");
        oResponse.put("tipDoc", null != desplazamiento.getTipDoc() ? desplazamiento.getTipDoc() : "");
        oResponse.put("numDocTer", null != desplazamiento.getNumDocTer() ? desplazamiento.getNumDocTer() : "");
        oResponse.put("fecDocTer", null != desplazamiento.getFecDocTer() ? sdo.format(desplazamiento.getFecDocTer()) : "");
        oResponse.put("tipDocTer", null != desplazamiento.getTipDocTer() ? desplazamiento.getTipDocTer() : "");
        oResponse.put("insEdu", null != desplazamiento.getInsEdu() ? desplazamiento.getInsEdu() : "");
        oResponse.put("car", null != desplazamiento.getCar() ? desplazamiento.getCar() : "");
        oResponse.put("jorLab", null != desplazamiento.getJorLab() ? desplazamiento.getJorLab() : "");
        oResponse.put("fecIni", null != desplazamiento.getFecIni() ? sdo.format(desplazamiento.getFecIni()) : "");
        oResponse.put("fecTer", null != desplazamiento.getFecTer() ? sdo.format(desplazamiento.getFecTer()) : "");
        oResponse.put("motRet", null != desplazamiento.getFecTer() && desplazamiento.getMotRet().length()>0 ? desplazamiento.getMotRet() : "");
        
        return WebResponse.crearWebResponseExito("El registro del desplazamiento se realizo correctamente", oResponse);
        //Fin
    }
}
