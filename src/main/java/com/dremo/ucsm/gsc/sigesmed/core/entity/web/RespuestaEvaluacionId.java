/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import java.io.Serializable;

/**
 *
 * @author abel
 */
public final class RespuestaEvaluacionId implements Serializable {
    
    private static final long serialVersionUID = 1562260205094677677L;
    private Integer bandejaEvaluacion;
    private int resEvaId;

    public RespuestaEvaluacionId() {
    }

    public RespuestaEvaluacionId(Integer bandejaEvaluacion, int resEvaId) {
        this.setBandejaEvaluacion(bandejaEvaluacion);
        this.setResEvaId(resEvaId);
    }

    @Override
    public int hashCode() {
        return ((this.getBandejaEvaluacion() == null
                ? 0 : this.getBandejaEvaluacion().hashCode())
                ^ ((int) this.getResEvaId()));
    }

    @Override
    public boolean equals(Object otherOb) {

        if (this == otherOb) {
            return true;
        }
        if (!(otherOb instanceof RespuestaEvaluacionId)) {
            return false;
        }
        RespuestaEvaluacionId other = (RespuestaEvaluacionId) otherOb;
        return ((this.getBandejaEvaluacion() == null
                ? other.getBandejaEvaluacion() == null : this.getBandejaEvaluacion()
                .equals(other.getBandejaEvaluacion()))
                && (this.getResEvaId() == other.getResEvaId()));
    }

    @Override
    public String toString() {
        return "" + getBandejaEvaluacion() + "-" + getResEvaId();
    }

    public Integer getBandejaEvaluacion() {
        return bandejaEvaluacion;
    }

    public void setBandejaEvaluacion(Integer bandejaEvaluacion) {
        this.bandejaEvaluacion = bandejaEvaluacion;
    }

    public int getResEvaId() {
        return resEvaId;
    }

    public void setResEvaId(int resEvaId) {
        this.resEvaId = resEvaId;
    }
    
}
