package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Nivel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 18/01/2017.
 */
@Entity
@Table(name = "periodos_plan_estudios",schema = "institucional")
public class PeriodosPlanEstudios implements java.io.Serializable {
    @Id
    @Column(name = "per_pla_est_id",nullable = false,unique = true)
    private int perPlaEstId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pla_est_id")
    private PlanEstudios planEstudios;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tip_per")
    private Periodo periodo;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "niv_id")
    private Nivel nivel;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ini" ,length = 29)
    private Date fecIni;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_fin" ,length = 29)
    private Date fecFin;
    @Column(name = "eta")
    private Integer eta;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public PeriodosPlanEstudios() {
        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public PeriodosPlanEstudios(int perPlaEstId, Date fecIni, Date fecFin, Integer eta) {
        this.perPlaEstId = perPlaEstId;
        this.fecIni = fecIni;
        this.fecFin = fecFin;
        this.eta = eta;

        this.estReg = 'A';
        this.fecMod = new Date();
    }

    public int getPerPlaEstId() {
        return perPlaEstId;
    }

    public void setPerPlaEstId(int perPlaEstId) {
        this.perPlaEstId = perPlaEstId;
    }

    public PlanEstudios getPlanEstudios() {
        return planEstudios;
    }

    public void setPlanEstudios(PlanEstudios planEstudios) {
        this.planEstudios = planEstudios;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public Integer getEta() {
        return eta;
    }

    public void setEta(Integer eta) {
        this.eta = eta;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
