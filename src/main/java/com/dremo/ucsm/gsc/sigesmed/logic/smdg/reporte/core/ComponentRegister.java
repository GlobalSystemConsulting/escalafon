/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.reporte.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.ficha_evaluacion.v1.tx.ActualizarFichaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.ficha_evaluacion.v1.tx.EliminarFichaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.ficha_evaluacion.v1.tx.FiltrarFichasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.ficha_evaluacion.v1.tx.InsertarFichaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.ficha_evaluacion.v1.tx.ListarFichaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.ficha_evaluacion.v1.tx.ListarFichasxOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.reporte.tx.ReporteActividadesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.reporte.tx.ReporteFichaIndividualTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.reporte.tx.ReporteFichasEvaluacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.reporte.tx.ReporteObjetivosTx;

/**
 *
 * @author gscadmin
 */

public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_MONITOREO_DOCUMENTOS_GESTION);        
        
        //Registrando el Nombre del componente
        component.setName("reporte");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("reporteFichasEvaluacion", ReporteFichasEvaluacionTx.class);        
        component.addTransactionPOST("reporteFichaIndividual", ReporteFichaIndividualTx.class);        
        component.addTransactionPOST("reporteObjetivos", ReporteObjetivosTx.class);        
        component.addTransactionPOST("ReporteActividades", ReporteActividadesTx.class);        
        
        return component;
    }
}


