/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Objetivos;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class ObjetivosDaoHibernate extends GenericDaoHibernate<Objetivos> implements ObjetivosDao{
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    
    @Override
    public List<Objetivos> listar(int iteide, char tipo) {
        List<Objetivos> objetivos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT o FROM Objetivos o "
                    + "LEFT JOIN FETCH o.detalle "
                    + "WHERE o.iteide = "+ iteide +" "
                    + "AND o.objTip = '" + tipo +"' AND o.estReg = 'A' "
                    + "ORDER BY o.objId";
            Query query = session.createQuery(hql);
//            query.setParameter("p1", iteide);
//            query.setParameter("p2", tipo);
            objetivos = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los objetivos o actividades \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los objetivos o actividades \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return objetivos;
    }
    
}
