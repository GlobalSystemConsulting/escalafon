/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.PrestamoSerieDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.PrestamoSerieDocumental;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author Jeferson
 */
public class RegistrarPrestamoSerieTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        PrestamoSerieDocumental psd = null;
        
        JSONObject requestData = (JSONObject)wr.getData();
        
        PrestamoSerieDAO psd_dao = (PrestamoSerieDAO)FactoryDao.buildDao("sad.PrestamoSerieDAO");
        
        try{
            
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            
            int inv_tra_id = requestData.getInt("inv_tra_id");
            int ser_doc_id = requestData.getInt("ser_doc_id");
            String obs = requestData.getString("obs");
            String dni = requestData.getString("dni");
            String nom = requestData.getString("nom");
            Date fec_dev = formatter.parse(requestData.getString("fec_dev"));
            Date fec_pre = formatter.parse(requestData.getString("fec_prestamo"));
            
            psd = new PrestamoSerieDocumental(0,inv_tra_id,ser_doc_id,obs,dni,nom,fec_dev,fec_pre);
            psd_dao.insert(psd);
            
        }catch(Exception e){
             System.out.println(e);
             return WebResponse.crearWebResponseError("No se pudo registrar el Prestamos de Serie Documental, datos incorrectos", e.getMessage() );
            
        }
        
           return WebResponse.crearWebResponseExito("El registro de Prestamo de Serie Documental se realizo correctamente");
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
}
