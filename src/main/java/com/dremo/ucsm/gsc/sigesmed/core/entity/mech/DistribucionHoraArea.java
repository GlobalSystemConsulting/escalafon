package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="distribucion_hora_area" ,schema="institucional" )
public class DistribucionHoraArea  implements java.io.Serializable {

    @Id
    @Column(name="dis_hor_are_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_distribucionhoraarea", sequenceName="institucional.distribucion_hora_area_dis_hor_are_id_seq" )
    @GeneratedValue(generator="secuencia_distribucionhoraarea")
    private int disHorAreId;
    @Column(name="hor_asi")
    private int horAsi;
    
    @Column(name="dis_hor_gra_id",updatable = false,insertable = false)
    private int disHorGraId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="dis_hor_gra_id")
    private DistribucionHoraGrado distribucionGrado;
    
    @Column(name="are_cur_id")
    private int areCurId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_cur_id",updatable = false,insertable = false)
    private AreaCurricular area;

    public DistribucionHoraArea() {
    }
    public DistribucionHoraArea(int disHorAreId) {
        this.disHorAreId = disHorAreId;
    }
    public DistribucionHoraArea(int disHorAreId,int horAsi,int disHorGraId,int areCurId) {
       this.disHorAreId = disHorAreId;
       
       this.horAsi = horAsi;
       
       this.disHorGraId = disHorGraId;
       this.areCurId = areCurId;
    }
    public DistribucionHoraArea(int disHorAreId,int horAsi,DistribucionHoraGrado disHorGra,int areCurId) {
       this.disHorAreId = disHorAreId;
       
       this.horAsi = horAsi;
       
       this.distribucionGrado = disHorGra;
       this.areCurId = areCurId;
    }
   
     
    public int getDisHorAreId() {
        return this.disHorAreId;
    }    
    public void setDisHorAreId(int disHorAreId) {
        this.disHorAreId = disHorAreId;
    }
    
    public int getHorAsi() {
        return this.horAsi;
    }
    public void setHorAsi(int horAsi) {
        this.horAsi = horAsi;
    }
    
    public int getDisHorGraId() {
        return this.disHorGraId;
    }    
    public void setDisHorGraId(int disHorGraId) {
        this.disHorGraId = disHorGraId;
    }
    
    public DistribucionHoraGrado getDistribucionGrado() {
        return this.distribucionGrado;
    }
    public void setDistribucionGrado(DistribucionHoraGrado distribucionGrado ) {
        this.distribucionGrado = distribucionGrado;
    }
    
    public int getAreCurId() {
        return this.areCurId;
    }    
    public void setAreCurId(int areCurId) {
        this.areCurId = areCurId;
    }
    
    public AreaCurricular getArea() {
        return this.area;
    }
    public void setArea(AreaCurricular area) {
        this.area = area;
    }
}


