/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.area.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.TipoAreaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoArea;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class InsertarTipoAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        TipoArea nuevaTipoArea = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            String nombre = requestData.getString("nombre");
            String alias = requestData.getString("alias");
            String estado = requestData.getString("estado");
            
            nuevaTipoArea = new TipoArea(0, nombre, alias, new Date(), Integer.parseInt( wr.getIdentity() ), estado.charAt(0));
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos" );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoAreaDao areaDao = (TipoAreaDao)FactoryDao.buildDao("TipoAreaDao");
        try{
            areaDao.insert(nuevaTipoArea);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar el tipo de Area\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el tipo de Area", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("tipoAreaID",nuevaTipoArea.getTipAreId());
        oResponse.put("fecha",nuevaTipoArea.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro del TipoArea se realizo correctamente", oResponse);
        //Fin
    }
    
}
