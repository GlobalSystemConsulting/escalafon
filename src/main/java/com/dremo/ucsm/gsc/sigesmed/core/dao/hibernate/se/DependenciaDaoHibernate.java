/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DependenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Dependencia;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Felipe
 */
public class DependenciaDaoHibernate extends GenericDaoHibernate<Dependencia> implements DependenciaDao{
    
    @Override
    public List<Dependencia> listarxDependencia() {
        List<Dependencia> dependencias = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT d from Dependencia as d "
                    + "JOIN FETCH d.facultad f "
                    + "WHERE d.estReg='A'";
            Query query = session.createQuery(hql);
            dependencias = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar las dependencias \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las dependencias \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return dependencias;
    }
    @Override
    public Dependencia buscarPorId(Integer depId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Dependencia item = (Dependencia)session.get(Dependencia.class, depId);
        session.close();
        return item;
    }
}
