/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstadoCivilDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstadoCivil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EstadoCivilDaoHibernate extends GenericDaoHibernate<EstadoCivil> implements EstadoCivilDao{

    @Override
    public List<EstadoCivil> listarAll() {
        List<EstadoCivil> documentos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT ec from EstadoCivil as ec "
                    + "WHERE ec.estReg='A'";
            Query query = session.createQuery(hql);
            documentos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los estados civiles \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los estados civiles \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return documentos;
    }

    @Override
    public EstadoCivil buscarPorId(Integer estCivId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        EstadoCivil des = (EstadoCivil)session.get(EstadoCivil.class, estCivId);
        session.close();
        return des;
    }
    
}
