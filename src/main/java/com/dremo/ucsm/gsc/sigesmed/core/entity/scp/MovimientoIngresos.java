/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;

/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="movimiento_ingresos", schema="administrativo")
public class MovimientoIngresos implements java.io.Serializable{
    
    @Id
    @Column(name="mov_ing_id", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.movimiento_ingresos_mov_ing_id_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int mov_ing_id;
     
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tip_mov_ing_id" , insertable=false , updatable=false)
    private TipoMovimientoIngresos tipo_movimiento; 
    /*
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="con_pat_id" , insertable=false , updatable=false)
    private ControlPatrimonial control_patrimonial; 
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id" , insertable=false , updatable=false)
    private Organizacion org; 
    */
    
    @Column(name="tip_mov_ing_id")
    private int tip_mov_ing_id;
    
    /*
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name="con_pat_id",referencedColumnName="con_pat_id",insertable=false,updatable=false),
        @JoinColumn(name="org_id",referencedColumnName="org_id",insertable=false,updatable=false),
    })
    private ControlPatrimonial control_patrimonial;
    */
    
   
    
    
    
    @Column(name="fec_mov")
    private Date fec_mov;
    
    @Column(name="num_res")
    private String num_res;
    
    @Column(name="fec_res")
    private Date fec_res;
    
    @Column(name="obs")
    private String obs;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="est_reg")
    private char est_reg;
    
    @Column(name="con_pat_id")
    private int con_pat_id;

   

    
    
    public int getTip_mov_ing_id() {
        return tip_mov_ing_id;
    }

    
    public void setTip_mov_ing_id(int tip_mov_ing_id) {
        this.tip_mov_ing_id = tip_mov_ing_id;
    }

    public void setMov_ing_id(int mov_ing_id) {
        this.mov_ing_id = mov_ing_id;
    }

    public void setTipo_movimiento(TipoMovimientoIngresos tipo_movimiento) {
        this.tipo_movimiento = tipo_movimiento;
    }

    /*
    public void setControl_patrimonial(ControlPatrimonial control_patrimonial) {
        this.control_patrimonial = control_patrimonial;
    }
*/
    
    public void setFec_mov(Date fec_mov) {
        this.fec_mov = fec_mov;
    }

    public void setNum_res(String num_res) {
        this.num_res = num_res;
    }

    public void setFec_res(Date fec_res) {
        this.fec_res = fec_res;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public void setCon_pat_id(char con_pat_id) {
        this.con_pat_id = con_pat_id;
    }

    public int getMov_ing_id() {
        return mov_ing_id;
    }

    public TipoMovimientoIngresos getTipo_movimiento() {
        return tipo_movimiento;
    }

    /*
    public ControlPatrimonial getControl_patrimonial() {
        return control_patrimonial;
    }
    */
    

    public Date getFec_mov() {
        return fec_mov;
    }

    public String getNum_res() {
        return num_res;
    }

    public Date getFec_res() {
        return fec_res;
    }

    public String getObs() {
        return obs;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public int getCon_pat_id() {
        return con_pat_id;
    }

    public MovimientoIngresos() {
    }

    public MovimientoIngresos(int mov_ing_id, Date fec_mov, String num_res, Date fec_res, int usu_mod, char est_reg, int con_pat_id) {
        this.mov_ing_id = mov_ing_id;
        this.fec_mov = fec_mov;
        this.num_res = num_res;
        this.fec_res = fec_res;
        this.usu_mod = usu_mod;
        this.est_reg = est_reg;
        this.con_pat_id = con_pat_id;
    }

    public MovimientoIngresos(int mov_ing_id, int tip_mov_ing_id, Date fec_mov, String num_res, Date fec_res, String obs, Date fec_mod, int usu_mod, char est_reg, int con_pat_id) {
        this.mov_ing_id = mov_ing_id;
        this.tip_mov_ing_id = tip_mov_ing_id;
        this.fec_mov = fec_mov;
        this.num_res = num_res;
        this.fec_res = fec_res;
        this.obs = obs;
        this.fec_mod = fec_mod;
        this.usu_mod = usu_mod;
        this.est_reg = est_reg;
        this.con_pat_id = con_pat_id;
    }
    
    
}
