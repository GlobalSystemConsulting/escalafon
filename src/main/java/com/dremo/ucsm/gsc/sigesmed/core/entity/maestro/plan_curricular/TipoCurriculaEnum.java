package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PersistentEnum;

/**
 * Created by Administrador on 13/10/2016.
 */
public enum TipoCurriculaEnum implements PersistentEnum {
    CURRICULA_NACIONAL("Curriculo Nacional"),
    CURRICULA_REGIONAL("Curriculo Regional"),
    CURRICULA_LOCAL("Curriculo Local"),
    CURRICULA_OTROS("Otros");

    private String nombre;
    TipoCurriculaEnum(String nombre){
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    @Override
    public String getValue() {
        return nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
