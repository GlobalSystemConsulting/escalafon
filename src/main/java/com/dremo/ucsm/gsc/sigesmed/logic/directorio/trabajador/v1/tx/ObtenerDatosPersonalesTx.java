/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ObtenerDatosPersonalesTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        int orgId = requestData.getInt("orgId");
        int perId = requestData.getInt("perId");
                
        Trabajador trabajador = null;
        TrabajadorDao trabajadorDao = (TrabajadorDao)FactoryDao.buildDao("di.TrabajadorDao");
        
        try{
            trabajador = trabajadorDao.obtenerDatosPersonales(orgId, perId);
        
        }catch(Exception e){
            System.out.println("No se pudo obtener los datos personales \n"+e);
            return WebResponse.crearWebResponseError("No se pudo obtener los datos personales ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        
            JSONObject oResponse = new JSONObject();
            
            oResponse.put("perDNI",trabajador.getPersona().getDni());
            oResponse.put("orgNom",trabajador.getOrganizacion().getNom());
            String nombres = trabajador.getPersona().getApeMat() +" "+ trabajador.getPersona().getApePat() +" "+trabajador.getPersona().getNom();
            oResponse.put("nombres",nombres);
            oResponse.put("cargo",trabajador.getTraCar().getCrgTraNom());
            
//            oResponse.put("perDir",trabajador.getApoderado().getPersona().getPerDir());
//            oResponse.put("perEma",trabajador.getApoderado().getPersona().getEmail());
//            oResponse.put("perFij",trabajador.getApoderado().getPersona().getFij());
         
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);        
                
    }
    
}
