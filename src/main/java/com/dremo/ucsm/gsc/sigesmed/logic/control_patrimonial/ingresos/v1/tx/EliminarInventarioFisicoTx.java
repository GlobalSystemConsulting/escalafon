/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioFisico;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioFisicoDAO;

/**
 *
 * @author Administrador
 */
public class EliminarInventarioFisicoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
            InventarioFisico invfis = null;
        
       try{
            JSONObject requestData = (JSONObject)wr.getData();
            int inv_fis_id = requestData.getInt("inv_ini_id");
           invfis = new InventarioFisico();
           invfis.setInv_ini_id(inv_fis_id);
           InventarioFisicoDAO inv_fis_dao = (InventarioFisicoDAO)FactoryDao.buildDao("scp.InventarioFisicoDAO");
           inv_fis_dao.eliminar_inventario_fisico(inv_fis_id);
           
       }catch(Exception e){
             System.out.println(e);
             return  WebResponse.crearWebResponseError("No se pudo eliminar el Inventario Fisico, datos incorrectos", e.getMessage() );
       }
        
       return WebResponse.crearWebResponseExito("El Inventario Fisico se elimino correctamente"); 
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
