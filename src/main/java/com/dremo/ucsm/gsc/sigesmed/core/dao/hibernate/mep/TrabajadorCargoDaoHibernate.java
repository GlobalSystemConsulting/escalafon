package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mep.TrabajadorCargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.TrabajadorCargo;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.awt.image.RescaleOp;

/**
 * Created by Administrador on 12/10/2016.
 */
public class TrabajadorCargoDaoHibernate extends GenericDaoHibernate<TrabajadorCargo> implements TrabajadorCargoDao{
    @Override
    public TrabajadorCargo buscarPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            TrabajadorCargo cargo =(TrabajadorCargo)session.get(TrabajadorCargo.class,id);
            return cargo;
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public TrabajadorCargo buscarPorNombre(String nombre) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(TrabajadorCargo.class)
                    .add(Restrictions.eq("carNom",nombre));
            return (TrabajadorCargo) criteria.uniqueResult();
        }catch (Exception e){
            throw e;
        }
    }
}
