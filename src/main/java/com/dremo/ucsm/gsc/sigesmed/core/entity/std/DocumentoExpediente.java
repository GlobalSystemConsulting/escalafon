package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@IdClass(DocumentoExpedienteId.class)
@Entity
@Table(name="documento_expediente" ,schema="administrativo")
public class DocumentoExpediente  implements java.io.Serializable  {

    @Id
    @Column(name="doc_exp_id", unique=true, nullable=false)
    private int docExpId;
    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="exp_id", nullable=false)    
    private Expediente expediente;
    
    @Column(name="des", nullable=false, length=128)
    private String des;
    @Column(name="arc_adj", length=64)
    private String arcAdj;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tip_doc_id", nullable=false )
    private TipoDocumento tipoDocumento;
    
    @Column(name="his_exp_id")
    private Integer historialId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name="his_exp_id",referencedColumnName="his_exp_id",insertable=false,updatable=false),
        @JoinColumn(name="exp_id",referencedColumnName="exp_id",insertable=false,updatable=false)
    })
    //@JoinColumn(name="his_exp_id", insertable=false,updatable=false)
    private HistorialExpediente historial;

    public DocumentoExpediente() {
    }
    public DocumentoExpediente(int docExpId) {
        this.docExpId = docExpId;
    }
    public DocumentoExpediente(int docExpId, Expediente expediente, String des, String arcAdj, int tipoDocumentoId) {
       this.docExpId = docExpId;
       this.expediente = expediente;
       this.des = des;
       this.arcAdj = arcAdj;
       
       this.tipoDocumento = new TipoDocumento(tipoDocumentoId);
    }
    public DocumentoExpediente(int docExpId, Expediente expediente, String des, String arcAdj, int tipoDocumentoId, int historialId) {
       this.docExpId = docExpId;
       this.expediente = expediente;
       this.des = des;
       this.arcAdj = arcAdj;
       
       this.tipoDocumento = new TipoDocumento(tipoDocumentoId);
       this.historialId = historialId;
    }
   
    public int getDocExpId() {
        return this.docExpId;
    }
    public void setDocExpId(int docExpId) {
        this.docExpId = docExpId;
    }
    
    public Expediente getExpediente() {
        return this.expediente;
    }
    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }
    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    public String getArcAdj() {
        return this.arcAdj;
    }
    public void setArcAdj(String arcAdj) {
        this.arcAdj = arcAdj;
    }

    public TipoDocumento getTipoDocumento() {
        return this.tipoDocumento;
    }
    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    
    public Integer getHistorialId() {
        return this.historialId;
    }
    public void setHistorialId(Integer historialId) {
        this.historialId = historialId;
    } 
    /*
    public HistorialExpediente getHistorial() {
        return this.historial;
    }
    public void setHistorial(HistorialExpediente historial) {
        this.historial = historial;
    }*/
    
}


