/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaCertificadaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarConsultasFrecuentesxUsuarioTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
            /*
        *  Parte para la operacion en la Base de Datos
        */        
        JSONObject requestData = (JSONObject)wr.getData();
        int usuId = requestData.getInt("usuId"); 
        List<ConsultaCertificada> consultasUser = new ArrayList<ConsultaCertificada>();
        ConsultaCertificadaDao consultaCertificadaDaoUser = (ConsultaCertificadaDao)FactoryDao.buildDao("ConsultaCertificadaDao");
        try{
            consultasUser = consultaCertificadaDaoUser.listarConsultasxUsuario(usuId);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las consultas certificadas user \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las consultas certificadas user", e.getMessage() );
        }
        //EMPAQUETAMOS EN JSON///
        JSONArray miArray = new JSONArray();
        for(ConsultaCertificada cCertificadas:consultasUser ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("con_cer_id", cCertificadas.getCon_cer_id());
            oResponse.put("atr_usa", cCertificadas.getAtributos());
            oResponse.put("cat", cCertificadas.getCatalogo());
            oResponse.put("log", cCertificadas.getLogica());
            oResponse.put("fil", cCertificadas.getFiltros());
            System.out.println("logica " + cCertificadas.getLogica());
            System.out.println("filtros " + cCertificadas.getFiltros());
            oResponse.put("tit", cCertificadas.getTitulo());
            oResponse.put("obs", cCertificadas.getObservaciones());
            oResponse.put("fec_emi", cCertificadas.getFecEmision());
            oResponse.put("usu_emi", cCertificadas.getUsuEmisor());
            oResponse.put("fec_cer", cCertificadas.getFecCertificacion());
            oResponse.put("est", cCertificadas.getEstReg());
            oResponse.put("sql_que", cCertificadas.getConsultaSql());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamenteUSER",miArray);
    }
}
