/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesInmuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.DetalleTecnicoInmuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ValorContable;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienInmuebleDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.MovimientoIngresosDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ValorContableDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleTecnicoInmueblesDAO;

import java.text.SimpleDateFormat;
/**
 *
 * @author Administrador
 */
public class ObtenerBienInmuebleTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONArray miArray = new JSONArray();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try{
            BienesInmuebles bim = null;
            MovimientoIngresos mi = null;
            DetalleTecnicoInmuebles dti = null;
            
             JSONObject requestData = (JSONObject)wr.getData();
             
             int id_bie_inm = requestData.getInt("id_bie_inm");
             
            BienInmuebleDAO bie_inmue_dao = (BienInmuebleDAO)FactoryDao.buildDao("scp.BienInmuebleDAO");
            bim = bie_inmue_dao.obtener_bien(id_bie_inm);
            
            DetalleTecnicoInmueblesDAO dti_dao = (DetalleTecnicoInmueblesDAO)FactoryDao.buildDao("scp.DetalleTecnicoInmueblesDAO");
            dti = dti_dao.listar_detalle_inmueble(id_bie_inm);
            
            JSONObject oResponse = new JSONObject();
            
             oResponse.put("descripcion",bim.getDescripcion());
             oResponse.put("tip_dire",bim.getTipo_dir());
             oResponse.put("direccion",bim.getBie_inm_dir());
             oResponse.put("nro",bim.getDir_numero());
             oResponse.put("mz",bim.getDir_mz());
             oResponse.put("lt",bim.getDir_lte());
             oResponse.put("departamento",bim.getDepartamento());
             oResponse.put("provincia",bim.getProvincia());
             oResponse.put("distrito",bim.getDistrito());
             
             /*Obtenemos la Data del Detalle del Inmueble*/
             oResponse.put("propiedad",dti.getDet_tec_prop());
             oResponse.put("tipo_terreno",dti.getTip_terr());
             if(dti.getEst_sanea()=='S'){oResponse.put("est_sanea",true);}
             if(dti.getEst_sanea()=='N'){oResponse.put("est_sanea",false);}
             oResponse.put("area",dti.getArea());
             oResponse.put("um",dti.getUni_area());
             oResponse.put("part_elec",dti.getPart_elec());
             oResponse.put("nro_fic_reg",dti.getNro_fic_reg());
             oResponse.put("reg_sinabip",dti.getReg_sinabip());
             
             oResponse.put("amb_id",bim.getAmb_id());
             miArray.put(oResponse);
  
            
        }catch(Exception e){
            System.out.println("No se pudo Obtener el Bien Inmueble\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Obtener el Bien Inmueble", e.getMessage() );
        }
        
         return WebResponse.crearWebResponseExito("Se obtuvo el Bien Inmueble Correctamente",miArray); 
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
