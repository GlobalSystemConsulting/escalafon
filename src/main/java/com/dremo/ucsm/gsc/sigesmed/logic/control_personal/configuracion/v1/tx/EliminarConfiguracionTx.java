/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioSessionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;


/**
 *
 * @author carlos
 */
public class EliminarConfiguracionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        
        Integer configToUpdate;
        LibroAsistenciaDao libroAsistenciaDao = (LibroAsistenciaDao)FactoryDao.buildDao("cpe.LibroAsistenciaDao");
        ConfiguracionControl prevConfig=null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
             
            configToUpdate=requestData.getInt("id");
            prevConfig=new ConfiguracionControl(configToUpdate);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos de Configuracion", e.getMessage() );
        }
        UsuarioSessionDao usuarioSessionDao = (UsuarioSessionDao)FactoryDao.buildDao("UsuarioSessionDao");
        try{
            //eliminar tamb rol de usuario
            UsuarioSession prevToUpdate=libroAsistenciaDao.getSessionByConfiguracionId(Sigesmed.ROL_RESPONSABLE_CONTROL_PERSONAL, configToUpdate);
            usuarioSessionDao.delete(prevToUpdate);
            
            libroAsistenciaDao.delete(prevConfig);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Eliminar la configuracion ", e.getMessage() );
        }

        JSONArray miArray = new JSONArray();
       
    
        return WebResponse.crearWebResponseExito("Se Elimino correctamente",miArray);        
        //Fin
    }
    
}

