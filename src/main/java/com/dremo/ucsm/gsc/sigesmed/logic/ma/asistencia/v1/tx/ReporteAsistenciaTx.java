package com.dremo.ucsm.gsc.sigesmed.logic.ma.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.AsistenciaEstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.anecdotario.AnecdotarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.AsistenciaEstudiante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Estudiante;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.element.Paragraph;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 20/01/2017.
 */
public class ReporteAsistenciaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ReporteAsistenciaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idOrg = data.optInt("org");
        int idGrado = data.optInt("gra");
        char idSeccion = data.optString("secc").charAt(0);
        long ini = data.getLong("ini");
        long fin = data.getLong("fin");
        int idArea = data.optInt("are", -1);
        return crearReporte(idOrg, idGrado, idSeccion, ini,fin, idArea);
    }

    private WebResponse crearReporte(int idOrg, int idGrado, char idSeccion, long ini,long fin, int idArea) {
        try{
            //obtenemos la cantidad de dias que sera el largo de nuestra tabla
            int numDays = (int)Math.abs((fin - ini) / (1000 * 60 * 60 * 24)) + 1;
            Date iniDate = new Date(ini);

            Calendar cal = Calendar.getInstance();
            cal.setTime(iniDate);

            logger.log(Level.INFO,"rango de dias {0}",numDays);
            String[] ndays = new String[numDays+1];
            ndays[0] = "";
            ndays[1] = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

            String[] mdays = new String[numDays+1];
            mdays[0] = "Alumnos:";
            mdays[1] = mostrarDiaSemana(cal.get(Calendar.DAY_OF_WEEK));

            for(int i = 2; i < numDays + 1; i++){
                cal.add(Calendar.DATE,1);
                ndays[i] = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
                mdays[i] = mostrarDiaSemana(cal.get(Calendar.DAY_OF_WEEK));
            }
            String data = crearArchivo(ndays,mdays,idOrg,idGrado,idArea,idSeccion,iniDate,new Date(fin));
            return WebResponse.crearWebResponseExito("", new JSONObject().put("file",data));
        }catch (Exception e){
            logger.log(Level.SEVERE,"crearReporte",e);
            return WebResponse.crearWebResponseError("No se puede crear el reporte");
        }
    }
    private String  crearArchivo(String[] ndays,String[] mdays,int idOrg, int idGrado,int idArea, char idSeccion,Date fec,Date fin) throws Exception{
        Mitext mitext = mdays.length < 20? new Mitext(false,"SIGESMED") : new Mitext(true,"SIGESMED");
        mitext.agregarTitulo("Reporte de Asistencia");

        JSONObject jsonSub = new JSONObject();
        jsonSub.put("Desde: ",new SimpleDateFormat("dd-MM-yyyy").format(fec));
        jsonSub.put("Hasta: ",new SimpleDateFormat("dd-MM-yyyy").format(fin));
        mitext.agregarSubtitulos(jsonSub);

        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        Paragraph p = new Paragraph("Asistencias");
        p.setKeepTogether(true);
        p.setFont(bold).setFontSize(12);
        mitext.getDocument().add(p);

        float[] dsize = new float[ndays.length];
        dsize[0] = 1.5f;
        for(int i = 1; i < dsize.length; i++){
            dsize[i] = 0.5f;
        }
        GTabla gtabla = new GTabla(dsize);
        gtabla.build(mdays);
        gtabla.processLine(ndays);


        ///Registro listar a alumnos

        AnecdotarioDao anecDao = (AnecdotarioDao) FactoryDao.buildDao("maestro.anecdotario.AnecdotarioDao");
        AsistenciaEstudianteDao asisDao = (AsistenciaEstudianteDao) FactoryDao.buildDao("ma.AsistenciaEstudianteDao");
        List<Estudiante> estudiantes = anecDao.listarEstudiantes(idOrg,idGrado,idSeccion);

        for(Estudiante estudiante : estudiantes){
            gtabla.addCell(estudiante.getPersona().getApePat() + " " +estudiante.getPersona().getApeMat() + " " + estudiante.getPersona().getNom(), Color.WHITE);
            Calendar cal = Calendar.getInstance();
            cal.setTime(fec);
            for(int i = 1; i < ndays.length; i++){
                AsistenciaEstudiante ae = idArea == -1 ? asisDao.buscarAsistenciaEstudiante(estudiante.getPersona().getPerId(),idOrg,idGrado,idSeccion,cal.getTime()) : asisDao.buscarAsistenciaEstudiante(estudiante.getPersona().getPerId(),idOrg,idGrado,idSeccion,idArea,cal.getTime());
                if(ae == null){
                    gtabla.addCell(" ",Color.WHITE);
                }else{
                    gtabla.addCell(mostarEstadoAsistencia(ae.getEstAsi()),mostrarColorAsistencia(ae.getEstAsi()));
                }
                cal.add(Calendar.DATE,1);
            }
        }
        mitext.agregarTabla(gtabla);
        mitext.cerrarDocumento();
        return mitext.encodeToBase64();
    }
    private String mostrarDiaSemana(int d){
        switch (d){
            case 0 : return "S";
            case 1 : return "D";
            case 2: return "L";
            case 3: return "M";
            case 4: return "M";
            case 5:return  "J";
            case 6: return "V";
            default: return "S";
        }
    }
    private String mostarEstadoAsistencia(int e){
        switch (e){
            case 0: return "A";
            case 1: return "F";
            case 2: return "T";
            case 3: return "";
            default:return "";
        }
    }
    private Color mostrarColorAsistencia(int e){
        switch (e){
            case 0: return Color.GREEN;
            case 1: return Color.RED;
            case 2: return Color.ORANGE;
            case 3: return Color.WHITE;
            default:return Color.WHITE;
        }
    }
}
