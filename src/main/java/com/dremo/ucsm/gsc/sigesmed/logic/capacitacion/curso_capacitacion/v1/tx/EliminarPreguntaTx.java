package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.PreguntaEvaluacionCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.PreguntaEvaluacionCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EliminarPreguntaTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(EliminarPreguntaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();            
            PreguntaEvaluacionCapacitacionDao preguntaEvaluacionCapacitacionDao = (PreguntaEvaluacionCapacitacionDao) FactoryDao.buildDao("capacitacion.PreguntaEvaluacionCapacitacionDao");
            PreguntaEvaluacionCapacitacion pregunta = preguntaEvaluacionCapacitacionDao.buscarPorId(data.getInt("id"));
            
            pregunta.setEstReg('E');
            pregunta.setUsuMod(data.getInt("usuMod"));
            pregunta.setFecMod(new Date());
            preguntaEvaluacionCapacitacionDao.update(pregunta);
            
            return WebResponse.crearWebResponseExito("Exito al eliminar la pregunta de la evaluación", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "eliminarEvaluacion", e);
            return WebResponse.crearWebResponseError("Error al eliminar la pregunta de la evaluación", WebResponse.BAD_RESPONSE);
        }        
    }
}
