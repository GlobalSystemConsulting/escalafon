/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.JustificacionAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.JustificacionInasistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Justificacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.JustificacionInasistenciaTrabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;

import org.json.JSONObject;


/**
 *
 * @author carlos
 */
public class EditarJustificacionAsistenciaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
          
        Justificacion justificacion=null;
        Justificacion prevJustificacion=null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            Integer idReg = requestData.getInt("idReg");  
            Integer idJus = requestData.getInt("idJusti");
            Integer tipo = requestData.getInt("tipo");  
            String tramite=requestData.getString("tramite");
            String obs=requestData.getString("obs");       
            
            justificacion=new Justificacion(tramite, obs, tipo+"", new RegistroAsistencia(idReg),new Date());
            prevJustificacion=new Justificacion(idJus);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage() );
        }

        
        try{
            JustificacionAsistenciaDao justificacionAsistenciaDao = (JustificacionAsistenciaDao)FactoryDao.buildDao("cpe.JustificacionAsistenciaDao");
            justificacionAsistenciaDao.delete(prevJustificacion);
            justificacionAsistenciaDao.insert(justificacion);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo editar la Regularizacion ", e.getMessage() );
        }

        return WebResponse.crearWebResponseExito("Se edito correctamente");        
        //Fin
    }
    
}

