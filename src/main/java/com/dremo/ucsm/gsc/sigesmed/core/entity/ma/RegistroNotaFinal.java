/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.ma;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author Carlos
 */
@Entity
@Table(name="registro_nota_final", schema="pedagogico")
public class RegistroNotaFinal implements java.io.Serializable{
    @Id
    @Column(name="reg_not_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_registro_nota_final", sequenceName="pedagogico.registro_nota_final_reg_not_id_seq" )
    @GeneratedValue(generator="secuencia_registro_nota_final")
    private int regNotFinId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_reg")
    private Date fecReg;

    @Column(name = "usu_mod")
    private int usuMod;
    
    @Column(name = "estReg")
    private char estReg;
    
    @Column(name = "are_cur_id")
    private int areaId;
    
    @Column(name = "not_prom")
    private String notProm;
    
    @Column(name = "not_rec")
    private String notRec;
    
    @Column(name = "gra_ie_est_id")
    private Long gradoIeEstudiante;
   
    
    public RegistroNotaFinal() {
    }

    public RegistroNotaFinal(int regNotFinId) {
        this.regNotFinId = regNotFinId;
    }

    public RegistroNotaFinal(Long gradoIeEstudiante) {
        this.gradoIeEstudiante = gradoIeEstudiante;
    }

    public RegistroNotaFinal(Date fecReg, int usuMod, char estReg, int areaId, String notProm, String notRec, Long gradoIeEstudiante) {
        this.fecReg = fecReg;
        this.usuMod = usuMod;
        this.estReg = estReg;
        this.areaId = areaId;
        this.notProm = notProm;
        this.notRec = notRec;
        this.gradoIeEstudiante = gradoIeEstudiante;
    }

    public int getRegNotFinId() {
        return regNotFinId;
    }

    public void setRegNotFinId(int regNotFinId) {
        this.regNotFinId = regNotFinId;
    }

    public Date getFecReg() {
        return fecReg;
    }

    public void setFecReg(Date fecReg) {
        this.fecReg = fecReg;
    }

    public int getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }

    public char getEstReg() {
        return estReg;
    }

    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getNotProm() {
        return notProm;
    }

    public void setNotProm(String notProm) {
        this.notProm = notProm;
    }

    public String getNotRec() {
        return notRec;
    }

    public void setNotRec(String notRec) {
        this.notRec = notRec;
    }

    public Long getGradoIeEstudiante() {
        return gradoIeEstudiante;
    }

    public void setGradoIeEstudiante(Long gradoIeEstudiante) {
        this.gradoIeEstudiante = gradoIeEstudiante;
    }

    
    
}
