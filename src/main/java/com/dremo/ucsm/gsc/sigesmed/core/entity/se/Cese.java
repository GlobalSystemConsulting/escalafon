/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "cese" , schema="administrativo")

public class Cese implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ces_id")
    private Integer cesId;
    
    @Size(max = 2147483647)
    @Column(name = "ent_emi_res")
    private String entEmiRes;
    
    @Size(max = 2147483647)
    @Column(name = "tip_doc")
    private String tipDoc;
    
    @Size(max = 2147483647)
    @Column(name = "num_doc")
    private String numDoc;
    
    @Column(name = "fec_doc")
    @Temporal(TemporalType.DATE)
    private Date fecDoc;
    
    @Size(max = 2147483647)
    @Column(name = "dep")
    private String dep;
    
    @Size(max = 2147483647)
    @Column(name = "car")
    private String car;
    
    @Column(name = "jor_lab")
    private Integer jorLabId;
    
    @Column(name = "fec_ces")
    @Temporal(TemporalType.DATE)
    private Date fecCes;
    
    @Size(max = 2147483647)
    @Column(name = "mot_ces")
    private String motCes;
    
    @Size(max = 2147483647)
    @Column(name = "des_adi")
    private String desAdi;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria;

    @Column(name = "dat_orgi_id")
    private Integer datOrgiId;
    
    @Column(name = "tip_orgi_id")
    private String tipOrgiId;
        
    @Column(name = "orgi_id")
    private String orgiId;
    
    @Column(name = "fec_ini")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecIni;
        
    @Column(name = "fec_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecFin;
    
    @Column(name = "tip_doc_id")
    private Integer tipDocId;
    
    @Column(name = "orgi_act_id")
    private Integer orgiActId;

    @Column(name = "cat_act_id")
    private Integer catActId;

    @Column(name = "car_act_id")
    private Integer carActId; 
    
    @Column(name = "tip_int_id")
    private Character tipIntId;
    
    @Column(name = "dia_pro")
    private Integer diaPro;
        
    @Column(name = "mes_pro")
    private Integer mesPro;
            
    @Column(name = "anio_pro")
    private Integer anioPro;
    
    public Cese() {
    }

    public Cese(Integer cesId) {
        this.cesId = cesId;
    }

    public Cese(String entEmiRes, String numDoc, Date fecDoc, Integer jorLabId, String motCes, Integer usuMod, Date fecMod, Character estReg, FichaEscalafonaria fichaEscalafonaria, Date fecIni, Date fecFin, int tipDocId) {
        this.entEmiRes = entEmiRes;
        this.numDoc = numDoc;
        this.fecDoc = fecDoc;
        this.jorLabId = jorLabId;
        this.motCes = motCes;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.fecIni = fecIni;
        this.fecFin = fecFin;
        this.tipDocId = tipDocId;
    }
    
    public Integer getCesId() {
        return cesId;
    }

    public void setCesId(Integer cesId) {
        this.cesId = cesId;
    }

    public String getEntEmiRes() {
        return entEmiRes;
    }

    public void setEntEmiRes(String entEmiRes) {
        this.entEmiRes = entEmiRes;
    }

    public String getTipDoc() {
        return tipDoc;
    }

    public void setTipDoc(String tipDoc) {
        this.tipDoc = tipDoc;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public Date getFecDoc() {
        return fecDoc;
    }

    public void setFecDoc(Date fecDoc) {
        this.fecDoc = fecDoc;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public Integer getJorLabId() {
        return jorLabId;
    }

    public void setJorLabId(Integer jorLabId) {
        this.jorLabId = jorLabId;
    }

    public Date getFecCes() {
        return fecCes;
    }

    public void setFecCes(Date fecCes) {
        this.fecCes = fecCes;
    }

    public String getMotCes() {
        return motCes;
    }

    public void setMotCes(String motCes) {
        this.motCes = motCes;
    }

    public String getDesAdi() {
        return desAdi;
    }

    public void setDesAdi(String desAdi) {
        this.desAdi = desAdi;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    
    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setFichaEscalafonaria(FichaEscalafonaria fichaEscalafonaria) {
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    public Integer getDatOrgiId() {
        return datOrgiId;
    }

    public void setDatOrgiId(Integer datOrgiId) {
        this.datOrgiId = datOrgiId;
    }

    public String getTipOrgiId() {
        return tipOrgiId;
    }

    public void setTipOrgiId(String tipOrgiId) {
        this.tipOrgiId = tipOrgiId;
    }

    public String getOrgiId() {
        return orgiId;
    }

    public void setOrgiId(String orgiId) {
        this.orgiId = orgiId;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public Integer getTipDocId() {
        return tipDocId;
    }

    public void setTipDocId(int tipDocId) {
        this.tipDocId = tipDocId;
    }

    public Integer getOrgiActId() {
        return orgiActId;
    }

    public void setOrgiActId(Integer orgiActId) {
        this.orgiActId = orgiActId;
    }

    public Integer getCatActId() {
        return catActId;
    }

    public void setCatActId(Integer catActId) {
        this.catActId = catActId;
    }

    public Integer getCarActId() {
        return carActId;
    }

    public void setCarActId(Integer carActId) {
        this.carActId = carActId;
    }

    public Character getTipIntId() {
        return tipIntId;
    }

    public void setTipIntId(Character tipIntId) {
        this.tipIntId = tipIntId;
    }

    public Integer getDiaPro() {
        return diaPro;
    }

    public void setDiaPro(Integer diaPro) {
        this.diaPro = diaPro;
    }

    public Integer getMesPro() {
        return mesPro;
    }

    public void setMesPro(Integer mesPro) {
        this.mesPro = mesPro;
    }

    public Integer getAnioPro() {
        return anioPro;
    }

    public void setAnioPro(Integer anioPro) {
        this.anioPro = anioPro;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cesId != null ? cesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cese)) {
            return false;
        }
        Cese other = (Cese) object;
        if ((this.cesId == null && other.cesId != null) || (this.cesId != null && !this.cesId.equals(other.cesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cese{" + "cesId=" + cesId + ", entEmiRes=" + entEmiRes + ", tipDoc=" + tipDoc + ", numDoc=" + numDoc + ", fecDoc=" + fecDoc + ", dep=" + dep + ", car=" + car + ", jorLab=" + jorLabId + ", fecCes=" + fecCes + ", motCes=" + motCes + ", desAdi=" + desAdi + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + '}';
    }

    
}
