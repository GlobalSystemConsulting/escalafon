/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Felipe
 */
public class TrabajadorDaoHibernate extends GenericDaoHibernate<Trabajador>  implements TrabajadorDao{

    @Override
    public Trabajador buscarTrabajadorxPerIdyRolId(int perId, int rolId) {
        Trabajador trabajador = null;        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
                
        try{   
            String hql = "SELECT t FROM com.dremo.ucsm.gsm.sigesmed.core.entity.Trabajador AS t "
                    + "join fetch t.persona as p "
                    + "join fetch t.rol as r "
                    + "WHERE p.perId= '" + perId + "' "
                    + "AND r.rolId= '" + rolId + "'";
            
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            trabajador = (Trabajador)query.uniqueResult();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo  \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo  \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return trabajador;
    }
    
}
