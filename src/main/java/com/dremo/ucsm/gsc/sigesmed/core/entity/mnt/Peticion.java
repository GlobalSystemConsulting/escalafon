/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name="peticion", schema="pedagogico")
public class Peticion implements java.io.Serializable{
    @Id
    @Column(name="pet_id", unique=true, nullable=false)
    @SequenceGenerator (name="secuencia_peticion", sequenceName="pedagogico.peticion_pet_id_seq")
    @GeneratedValue(generator="secuencia_peticion")
    private int peticionID;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="men_ide")
    private Mensaje mensaje;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fun_sis_id")
    private FuncionSistema funcion;
    
    @Column(name="pet_tip", nullable=false, length=1)
    private char petTip;
    
    @Column(name="est_reg", nullable=false, length=1)
    private char petEstReg;

    public Peticion(){}
    public Peticion(int peticionID) {
        this.peticionID = peticionID;
    }

    public Peticion(int peticionID, Mensaje mensaje, FuncionSistema funcion, char petTip, char petEstReg) {
        this.peticionID = peticionID;
        this.mensaje = mensaje;
        this.funcion = funcion;
        this.petTip = petTip;
        this.petEstReg = petEstReg;
    }

    public int getPeticionID() {
        return peticionID;
    }

    public void setPeticionID(int peticionID) {
        this.peticionID = peticionID;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    public FuncionSistema getFuncion() {
        return funcion;
    }

    public void setFuncion(FuncionSistema funcion) {
        this.funcion = funcion;
    }

    public char getPetTip() {
        return petTip;
    }

    public void setPetTip(char petTip) {
        this.petTip = petTip;
    }

    public char getPetEstReg() {
        return petEstReg;
    }

    public void setPetEstReg(char petEstReg) {
        this.petEstReg = petEstReg;
    }
    
    
}
