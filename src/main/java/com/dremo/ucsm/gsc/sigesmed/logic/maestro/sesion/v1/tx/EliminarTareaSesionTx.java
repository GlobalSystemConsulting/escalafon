package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 15/12/2016.
 */
public class EliminarTareaSesionTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EliminarTareaSesionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idTarea = data.getInt("id");
        return editarTareaSesion(idTarea);
    }

    private WebResponse editarTareaSesion(int idTarea) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            sesionDao.eliminarTareaSesion(idTarea);
            return WebResponse.crearWebResponseExito("Exito al eliminar el material");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarMaterialUnidadDidactica",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el material");
        }
    }
}
