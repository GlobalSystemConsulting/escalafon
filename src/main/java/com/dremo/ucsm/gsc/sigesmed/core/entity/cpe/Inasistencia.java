package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoDocumento;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="inasistencia" ,schema="administrativo")
public class Inasistencia  implements java.io.Serializable {


    @Id
    @Column(name="ina_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_inasistencia", sequenceName="administrativo.inasistencia_ina_id_seq" )
    @GeneratedValue(generator="secuencia_inasistencia")
    private Integer inaId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="ina_fec")
    private Date inaFecha;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_reg")
    private Date fecReg;
    
    @Column(name="est_reg")
    private String estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tra_id", nullable=false )
    private Trabajador trabajadorId;  
    
    @OneToOne(fetch=FetchType.EAGER, mappedBy="jusIna")
    @JoinColumn(name="ina_id")
    private JustificacionInasistenciaTrabajador justificacion;

    public Inasistencia() {
    }

    public Inasistencia(Integer inaId) {
        this.inaId = inaId;
    }

    public Inasistencia(Date inaFecha, Date fecReg, String estReg, Trabajador trabajadorId) {
        this.inaFecha = inaFecha;
        this.fecReg = fecReg;
        this.estReg = estReg;
        this.trabajadorId = trabajadorId;
    }

    public Inasistencia(Date inaFecha, Date fecReg, String estReg, Trabajador trabajadorId, JustificacionInasistenciaTrabajador justificacion) {
        this.inaFecha = inaFecha;
        this.fecReg = fecReg;
        this.estReg = estReg;
        this.trabajadorId = trabajadorId;
        this.justificacion = justificacion;
    }

    public Integer getInaId() {
        return inaId;
    }

    public void setInaId(Integer inaId) {
        this.inaId = inaId;
    }

    public Date getInaFecha() {
        return inaFecha;
    }

    public void setInaFecha(Date inaFecha) {
        this.inaFecha = inaFecha;
    }

    public Date getFecReg() {
        return fecReg;
    }

    public void setFecReg(Date fecReg) {
        this.fecReg = fecReg;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Trabajador getTrabajadorId() {
        return trabajadorId;
    }

    public void setTrabajadorId(Trabajador trabajadorId) {
        this.trabajadorId = trabajadorId;
    }

    public JustificacionInasistenciaTrabajador getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(JustificacionInasistenciaTrabajador justificacion) {
        this.justificacion = justificacion;
    }

   
    
   

}


