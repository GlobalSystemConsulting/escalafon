package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mech.PlanEstudiosDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GradoSeccionPlanNivelDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaInsDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONObject;

public class GenerarGradosySeccionTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        try {
            PlanEstudiosDaoHibernate pev = new PlanEstudiosDaoHibernate();

            JSONObject requestData = (JSONObject) wr.getData();
            int orgId = requestData.getInt("orgIdUser");

            PlanEstudios pe = pev.buscarVigentePorOrganizacion(orgId);

            Date myDate = pe.getAñoEsc();
            Calendar cal = Calendar.getInstance();
            cal.setTime(myDate);
            int year = cal.get(Calendar.YEAR);

            MatriculaInsDaoHibernate dh = new MatriculaInsDaoHibernate();
            GradoSeccionPlanNivelDaoHibernate gspn = new GradoSeccionPlanNivelDaoHibernate();

            dh.generateGradoSeccionPlanEstudios(orgId, year);

            //inhabilita matriculas de la organizacion
            gspn.disableMatriculas(orgId);
                    
            return WebResponse.crearWebResponseExito("Se Generaron las Aulas", "");
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al generar Aulas! ", e.getMessage());

        }

    }
}
