package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.EvaluacionParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.EvaluacionParticipanteCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EditarEvaluacionParticipanteTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(EditarEvaluacionParticipanteTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            EvaluacionParticipanteCapacitacionDao evaluacionParticipanteCapacitacionDao = (EvaluacionParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.EvaluacionParticipanteCapacitacionDao");
            EvaluacionParticipanteCapacitacion evaPar = evaluacionParticipanteCapacitacionDao.buscarUltima(data.getInt("codEva"), data.getInt("codDoc"));
            
            evaPar.setEstReg('C');
            evaPar.setFecApl(new Date());
            evaPar.setNotObt(data.getDouble("not"));
            
            evaluacionParticipanteCapacitacionDao.update(evaPar);
            
            return WebResponse.crearWebResponseExito("Exito al editar la evaluacion del participante", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarComentario", e);
            return WebResponse.crearWebResponseError("Error al editar la evaluacion del participante", WebResponse.BAD_RESPONSE);
        }        
    }    
}
