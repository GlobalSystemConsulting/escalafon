package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.DisenoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.DisenoCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 20/10/2016.
 */
public class ListarDisenosCurricularesTx implements ITransaction {
    private static final Logger logger = Logger.getLogger( ListarDisenosCurricularesTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            DisenoCurricularDao disenoDao = (DisenoCurricularDao) FactoryDao.buildDao("maestro.plan.DisenoCurricularDao");
            List<DisenoCurricular> disenos = disenoDao.buscarTodos(DisenoCurricular.class);

            return WebResponse.crearWebResponseExito("No se puede listar las curriculas", new JSONArray(
                    EntityUtil.listToJSONString(
                            new String[]{"disCurId","nom","des"},
                            new String[]{"cod","nom","des"},
                            disenos
                    )
            ));
        }catch (Exception e){
            logger.log(Level.SEVERE,"listar Curriculas",e);
            return WebResponse.crearWebResponseError("No se puede listar las curriculas");
        }
    }
}
