package com.dremo.ucsm.gsc.sigesmed.logic.maestro.curriculabanco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CapacidadAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.CompetenciaCapacidadDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.IndicadorAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CapacidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaCapacidad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.IndicadorAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/10/2016.
 */
public class EliminarCapacidadBancoTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(EliminarCapacidadBancoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return eliminarCapacitacion(data.getInt("cap"),data.getInt("com"));
    }

    private WebResponse eliminarCapacitacion(int cap,int com) {
        try {
            CompetenciaCapacidadDao comCapDao = (CompetenciaCapacidadDao) FactoryDao.buildDao("maestro.plan.CompetenciaCapacidadDao");
            /*CapacidadAprendizajeDao capacidadDao = (CapacidadAprendizajeDao) FactoryDao.buildDao("maestro.plan.CapacidadAprendizajeDao");
            IndicadorAprendizajeDao indicadorDao = (IndicadorAprendizajeDao) FactoryDao.buildDao("maestro.plan.IndicadorAprendizajeDao");
            CapacidadAprendizaje capacidad = capacidadDao.buscarPorId(cap);
            for (IndicadorAprendizaje indicador : capacidad.getIndicadores()) {
                indicadorDao.delete(indicador);
            }*/
            CompetenciaCapacidad comCap = comCapDao.buscarCapacidadDeCompetencia(cap,com);
            comCapDao.delete(comCap);
            return WebResponse.crearWebResponseExito("Exito al realizar la eliminacion", WebResponse.OK_RESPONSE);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "editarCapacitacion", e);
            return WebResponse.crearWebResponseError("Error al realizar la eliminacion", e.getMessage());
        }
    }
}