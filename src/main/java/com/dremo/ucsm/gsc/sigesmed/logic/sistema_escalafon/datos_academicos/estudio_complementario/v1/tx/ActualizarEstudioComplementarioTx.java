/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.estudio_complementario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.EstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.EstudioComplementario;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarEstudioComplementarioTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ActualizarEstudioComplementarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {

            JSONObject requestData = (JSONObject) wr.getData();
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");

            Integer estComId = requestData.getInt("estComId");
            Integer tipId = requestData.getInt("tipId");
            String des = requestData.getString("des");
            Integer nivId = requestData.getInt("nivId");
            String insCer = requestData.getString("insCer");
            String tipPar = requestData.getString("tipPar");
            String pais = requestData.getString("pais");
            Date fecIni = requestData.getString("fecIni").equals("") ? null : sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = requestData.getString("fecTer").equals("") ? null : sdi.parse(requestData.getString("fecTer").substring(0, 10));
            Integer horLec = requestData.getInt("horLec");

            return actualizarEstPos(estComId, tipId, des, nivId, insCer, tipPar, fecIni, fecTer, horLec, pais);
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE, "Actualizar formacion educativa", e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        }
    }

    private WebResponse actualizarEstPos(Integer estComId, Integer tipId, String des, Integer nivId, String insCer,
            String tipPar, Date fecIni, Date fecTer, Integer horLec, String pais) {
        try {
            EstudioComplementarioDao estComDao = (EstudioComplementarioDao) FactoryDao.buildDao("se.EstudioComplementarioDao");
            EstudioComplementario estCom = estComDao.buscarPorId(estComId);

            estCom.setTipId(tipId);
            estCom.setDes(des);
            estCom.setNivId(nivId);
            estCom.setInsCer(insCer);
            estCom.setTipPar(tipPar);
            estCom.setFecIni(fecIni);
            estCom.setFecTer(fecTer);
            estCom.setHorLec(horLec);
            estCom.setPais(pais);

            estComDao.update(estCom);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("estComId", estCom.getEstComId());
            oResponse.put("tipId", estCom.getTipId() == null ? 0 : estCom.getTipId());
            oResponse.put("des", estCom.getDes() == null ? "" : estCom.getDes());
            oResponse.put("nivId", estCom.getNivId() == null ? 0 : estCom.getNivId());
            oResponse.put("niv", "");
            oResponse.put("insCer", estCom.getInsCer() == null ? "" : estCom.getInsCer());
            oResponse.put("tipPar", estCom.getTipPar() == null ? "" : estCom.getTipPar());
            oResponse.put("fecIni", estCom.getFecIni() == null ? "" : sdo.format(estCom.getFecIni()));
            oResponse.put("fecTer", estCom.getFecTer() == null ? "" : sdo.format(estCom.getFecTer()));
            oResponse.put("horLec", estCom.getHorLec() == null ? "" : estCom.getHorLec());
            oResponse.put("pais", estCom.getPais() == null ? "" : estCom.getPais());
            
            return WebResponse.crearWebResponseExito("Estudio complementario actualizado exitosamente", oResponse);

        } catch (Exception e) {
            logger.log(Level.SEVERE, "actualizarFEstudioComplementario", e);
            return WebResponse.crearWebResponseError("Error, el estudio complementario no fue actualizado");
        }
    }
}
