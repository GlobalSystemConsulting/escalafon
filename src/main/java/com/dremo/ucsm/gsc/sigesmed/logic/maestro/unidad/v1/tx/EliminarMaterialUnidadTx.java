package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 12/12/2016.
 */
public class EliminarMaterialUnidadTx implements ITransaction {
    private static Logger logger = Logger.getLogger(EliminarMaterialUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idMaterial = data.getInt("id");
        return eliminarMaterialUnidadDidactica(idMaterial);
    }

    private WebResponse eliminarMaterialUnidadDidactica(int idMaterial) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            unidadDao.eliminarMaterial(idMaterial);
            return WebResponse.crearWebResponseExito("Exito al eliminar el material");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarMaterialUnidadDidactica",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el material");
        }
    }
}
