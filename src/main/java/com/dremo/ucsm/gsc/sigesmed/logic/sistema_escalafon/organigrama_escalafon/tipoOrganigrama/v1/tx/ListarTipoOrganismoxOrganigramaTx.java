/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.tipoOrganigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FacultadDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Facultad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class ListarTipoOrganismoxOrganigramaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarTipoOrganismoxOrganigramaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */      
        List<TipoOrganigrama> tiposOrganigrama = null;
        TipoOrganigramaDao tipoOrganigramaDao = (TipoOrganigramaDao)FactoryDao.buildDao("se.TipoOrganigramaDao");
        OrganigramaDao organigramaDao = (OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
        
        try{
            JSONObject request = (JSONObject)wr.getData();
            int organigramaId = request.getInt("organigramaId");
            tiposOrganigrama = tipoOrganigramaDao.listarxOrganigrama(organigramaId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar Tipo Organigrama",e);
            System.out.println("No se pudo listar Tipo Organigrama.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas Tipo Organigrama.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoOrganigrama c: tiposOrganigrama ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tipId", c.getTipOrgiId());
            oResponse.put("datId", c.getDatosOrganigramas().getNomDatOrgi()+" "+c.getDatosOrganigramas().getAnioDatOrgi());
            oResponse.put("codTipOrga", c.getCodTipOrgi());
            oResponse.put("nomTipOrga", c.getNomTipOrgi());
            oResponse.put("desTipOrga", c.getDesTipOrgi());     
            oResponse.put("fecMod",c.getFecMod());
            oResponse.put("UsuMod",c.getUsuMod());
            oResponse.put("estReg",c.getEstReg());
            
            JSONArray jsonArrayOrganismos = new JSONArray();
            List<Organigrama> organismos = organigramaDao.listarxTipo(c.getTipOrgiId());
            for(Organigrama o: organismos ){
                JSONObject objectJSON = new JSONObject();
                if (o.getOrganigramaPadre() == null){
                    objectJSON.put("orgIdPad", -1);
                    objectJSON.put("orgNomPad","*");
                }else{
                    objectJSON.put("orgIdPad", o.getOrganigramaPadre().getOrgiId());
                    objectJSON.put("orgNomPad",o.getOrganigramaPadre().getNomOrgi());
                }
                
                if (o.getUbiId() == null){
                    objectJSON.put("ubiId", -1);
                    objectJSON.put("ubiNom","");
                }else{
                    objectJSON.put("ubiId", o.getUbiId());
                    objectJSON.put("ubiNom",o.getUbicacion().getNom());
                }
                
                objectJSON.put("tipId", o.getTipoOrganigrama().getTipOrgiId());
                objectJSON.put("datId", o.getTipoOrganigrama().getDatosOrganigramas().getDatOrgiId());
                objectJSON.put("codOrg", o.getCodOrgi());
                
                objectJSON.put("abrOrg", o.getAbrOrgi()); 
                
                objectJSON.put("nomOrg", o.getNomOrgi());
                
                objectJSON.put("datOrg", o.getCamOrgi());        
                
                objectJSON.put("fecMod",o.getFecMod());
                
                objectJSON.put("UsuMod",o.getUsuMod());
                
                objectJSON.put("estReg",o.getEstReg());
                
                objectJSON.put("orgId",o.getOrgiId());

                jsonArrayOrganismos.put(objectJSON);
            }
            oResponse.put("organismos", jsonArrayOrganismos);
            oResponse.put("organismosFil", jsonArrayOrganismos);
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Tipo Organigrama fueron listadas exitosamente", miArray);
    }
}

