package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.EvaluacionPersonalDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mep.EvaluacionPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ResumenEvaluacionPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by geank on 02/09/16.
 */
public class ListarEvaluacionesCompartidasTx extends MepGeneralTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarEvaluacionesCompartidasTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String idOrg = wr.getMetadataValue("org");
        String idTra = wr.getMetadataValue("tra");
        return getEvaluaciones(Integer.parseInt(idTra),Integer.parseInt(idOrg));
    }
    public WebResponse getEvaluaciones(int idTrabajador, int idOrg){
        try{
            EvaluacionPersonalDao evaluacionDao = (EvaluacionPersonalDao) FactoryDao.buildDao("mep.EvaluacionPersonalDao");
            List<ResumenEvaluacionPersonal> resumenes = evaluacionDao.getEvaluacionesCompartidas(idTrabajador,idOrg);
            Trabajador trab = evaluacionDao.buscarTrabajadorPorPersona(idTrabajador);
            JSONObject trabJSON = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"traId","tieServ","fecIng"},
                    new String[]{"id","tie","fec"},
                    trab
            ));
            trabJSON.put("car",trab.getTraCar().getCrgTraNom());
            JSONObject rspData = new JSONObject()
                    .put("trab",trabJSON)
                    .put("eval",new JSONArray(resumenes.toString()));
            //JSONArray data = new JSONArray(resumenes.toString());
            return WebResponse.crearWebResponseExito("Se obtuvieron los datos exitosamente",WebResponse.OK_RESPONSE).setData(rspData);


        }catch (Exception e){
            logger.log(Level.SEVERE,"getEvaluaciones",e);
            return WebResponse.crearWebResponseError("No se pudieron obtener los datos",WebResponse.BAD_RESPONSE);
        }
    }
}
