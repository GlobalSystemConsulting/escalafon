/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.organismo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganismoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organismo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class EliminarOrganismosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarOrganismosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        Integer orgaId = 0;
        
        try{
           JSONObject requestData = (JSONObject)wr.getData();
           orgaId = requestData.getInt("orgaId");
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarOrganismo",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        OrganismoDao organismoDao = (OrganismoDao)FactoryDao.buildDao("se.OrganismoDao");
        try{
            organismoDao.delete(new Organismo(orgaId));
        }catch(Exception e){
            System.out.println("No se pudo eliminar el organismo\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el organismo", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El organismo se elimino correctamente");
    }
    
}
