package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.MaterialRecursoProgramacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import javax.swing.text.html.parser.Entity;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 12/12/2016.
 */
public class RegistrarMaterialUnidadTx implements ITransaction {
    private Logger logger = Logger.getLogger(RegistrarMaterialUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idUnidad = data.getInt("unid");
        String  tipUnidad = data.getString("untip");

        Character tip = data.getString("tip").charAt(0);
        String nom = data.getString("nom").toUpperCase();
        String des = data.optString("des");
        int can = data.optInt("can");

        MaterialRecursoProgramacion materialRecurso = new MaterialRecursoProgramacion(nom,tip,des,can);
        
        return registrarMaterialUnidad(idUnidad,tipUnidad,materialRecurso);
    }

    private WebResponse registrarMaterialUnidad(int idUni,String tip,MaterialRecursoProgramacion materialRecurso) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            UnidadDidactica unidad = unidadDao.buscarUnidadDidactica(idUni, tip);
            materialRecurso.setUnidadDidactica(unidad);
            unidadDao.registrarMaterial(materialRecurso);
            JSONObject jsonMaterial = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"matId","tip"},
                    new String[]{"id","tip"},
                    materialRecurso
            ));
            /*String tipoMat = materialRecurso.getTip().equals('L')? "Libro" : "Escritorio";
            jsonMaterial.put("tip",tipoMat);*/
            return WebResponse.crearWebResponseExito("Se registro el material",jsonMaterial);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarMaterialUnidad",e);
            return WebResponse.crearWebResponseError("No se puede registrar el material");
        }
    }
}
