/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ListarDRTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        
        
        Integer orgID;
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            orgID = requestData.getInt("organizacionID");
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage());
        }

        OrganizacionDao organizacionDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");

        Organizacion organizacion = null;
        try {
            organizacion = organizacionDao.buscarConTipoOrganizacionYPadre(orgID);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo listar las Organizaciones ", e.getMessage());
        }
        List<Organizacion> organizaciones=null;
        JSONArray miArray = new JSONArray();
        try {
            if (organizacion.getTipoOrganizacion().getTipOrgId()== Sigesmed.ORGANIZACION_TIPO_GLOBAL) {
                organizaciones=organizacionDao.buscarPorTipoOrganizacion(Sigesmed.ORGANIZACION_TIPO_DR);
                for(Organizacion org:organizaciones)
                {
                    JSONObject oRes = new JSONObject();
                    oRes.put("id", org.getOrgId());
                    oRes.put("nom", org.getAli());
                    oRes.put("mostrarDR", false);
                    oRes.put("mostrarUGEL", false);
                    oRes.put("mostrarIE", false);
                    miArray.put(oRes);
                }
                
            } else {
                if (organizacion.getTipoOrganizacion().getTipOrgId()==Sigesmed.ORGANIZACION_TIPO_DR) {
                    JSONObject oRes = new JSONObject();
                    oRes.put("id", organizacion.getOrgId());
                    oRes.put("nom", organizacion.getAli());
                    oRes.put("mostrarDR", true);
                    oRes.put("mostrarUGEL", false);
                    oRes.put("mostrarIE", false);
                    miArray.put(oRes);
                } else if(organizacion.getTipoOrganizacion().getTipOrgId()==Sigesmed.ORGANIZACION_TIPO_UGEL) {
                    JSONObject oRes = new JSONObject();
                    JSONObject oRes2 = new JSONObject();
                    JSONArray oArray2=new JSONArray();
                    oRes.put("id", organizacion.getOrganizacionPadre().getOrgId());
                    oRes.put("nom", organizacion.getOrganizacionPadre().getAli());
                    
                    oRes2.put("id", organizacion.getOrgId());
                    oRes2.put("nom", organizacion.getAli());
                    oArray2.put(oRes2);
                    oRes.put("ugel", oArray2);
                    
                    oRes.put("mostrarDR", true);
                    oRes.put("mostrarUGEL", true);
                    oRes.put("mostrarIE", false);
                    miArray.put(oRes);
                } else if(organizacion.getTipoOrganizacion().getTipOrgId()==Sigesmed.ORGANIZACION_TIPO_IE) {
                    
                    Organizacion ugel= organizacionDao.buscarConTipoOrganizacionYPadre(organizacion.getOrganizacionPadre().getOrgId());
                    JSONObject oRes = new JSONObject();
                    JSONObject oRes2 = new JSONObject();
                    JSONObject oRes3 = new JSONObject();
                    JSONArray oArray2=new JSONArray();
                    JSONArray oArray3=new JSONArray();
                    oRes.put("id", ugel.getOrganizacionPadre().getOrgId());
                    oRes.put("nom", ugel.getOrganizacionPadre().getAli());
                    
                    oRes2.put("id", organizacion.getOrganizacionPadre().getOrgId());
                    oRes2.put("nom", organizacion.getOrganizacionPadre().getAli());
                    oArray2.put(oRes2);
                    oRes.put("ugel", oArray2);
                    
                    oRes3.put("id", organizacion.getOrgId());
                    oRes3.put("nom", organizacion.getNom());
                    oArray3.put(oRes3);
                    oRes2.put("cole", oArray3);
                    
                    oRes.put("mostrarDR", true);
                    oRes.put("mostrarUGEL", true);
                    oRes.put("mostrarIE", true);
                    miArray.put(oRes);
                   
                }
            }
             return WebResponse.crearWebResponseExito("Se Listo correctamente", miArray);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo listar las Organizaciones ", e.getMessage());
        }
        
        
        
            
        //Fin
    }
    
}

