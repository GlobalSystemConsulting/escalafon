/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.colegiatura.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ColegiaturaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Colegiatura;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarColegiaturasTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarColegiaturasTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer perId = requestData.getInt("perId");
                
        List<Colegiatura> colegiaturas = null;
        ColegiaturaDao colegiaturaDao = (ColegiaturaDao)FactoryDao.buildDao("se.ColegiaturaDao");
        
        try{
            colegiaturas = colegiaturaDao.listarxFichaEscalafonaria(perId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar exposiciones",e);
            System.out.println("No se pudo listar las colegiaturas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar las colegiaturas", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Colegiatura col: colegiaturas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("colId", col.getColId());
            oResponse.put("nomColPro", col.getNomColPro());
            oResponse.put("numRegCol", col.getNumRegCol());
            oResponse.put("conReg", col.getConReg());
            oResponse.put("conRegDes", "");
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las colegiaturas fueron listadas exitosamente", miArray);
    }
    
}
