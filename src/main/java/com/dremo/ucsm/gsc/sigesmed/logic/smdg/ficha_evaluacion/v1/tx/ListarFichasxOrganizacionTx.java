/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.ficha_evaluacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.FichaEvaluacionDocumentosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.FichaEvaluacionDocumentos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarFichasxOrganizacionTx implements ITransaction{
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<Object[]> fichas = null;
        FichaEvaluacionDocumentosDao fichasDao = (FichaEvaluacionDocumentosDao)FactoryDao.buildDao("smdg.FichaEvaluacionDocumentosDao");        
        
        try{
            fichas = fichasDao.listarFichasxOrganizacion(1);        
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se encontro la fichas", e.getMessage() );
        }
        //Fin
            
        /*
        *  Repuesta Correcta
        */
               
        JSONArray miArray = new JSONArray();
                
        for(Object[] f : fichas){
            JSONObject oResponse = new JSONObject();
            oResponse.put("ficide",f[0]);
            oResponse.put("ficdoc",f[1]);
            oResponse.put("ficfec",f[2]);
            oResponse.put("fictot",f[3]);
            oResponse.put("plaid",f[4]);
            oResponse.put("planom",f[5]);
            oResponse.put("ficeva",f[6] +" "+ f[7] +" "+ f[8]);
            oResponse.put("evacar",f[9]);
            oResponse.put("url",f[10]);

            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);  
                
    }    
}
