/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.organigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DatosOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.DatosOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class ActualizarOrganigramaTx implements ITransaction{
    
    private static final Logger logger = Logger.getLogger(ActualizarOrganigramaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        
        Organigrama organigrama=null;
        TipoOrganigrama tipoOrganigrama = null;
        DatosOrganigrama datosOrganigrama=null;
        Integer orgIdPad=0;
        Integer zonaId=0;
        try{    
            JSONObject requestData = (JSONObject)wr.getData();
            Integer orgId =requestData.getInt("orgId");
            Integer tipId = requestData.getInt("tipId");
                    orgIdPad = requestData.getInt("orgIdPad");
            String codOrg = requestData.optString("codOrg");
            String abrOrg = requestData.optString("abrOrg");
            String nomOrg = requestData.optString("nomOrg");
            String datOrg = requestData.optString("datOrg");
                zonaId=requestData.getInt("ubiId");
            
            TipoOrganigramaDao tipoOrganigramaDao = (TipoOrganigramaDao)FactoryDao.buildDao("se.TipoOrganigramaDao");
            tipoOrganigrama = tipoOrganigramaDao.buscarXId(tipId);
            
            return actualizarCargos( orgId, orgIdPad, codOrg, abrOrg, nomOrg,datOrg ,tipoOrganigrama,zonaId);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    private WebResponse actualizarCargos(Integer facId,Integer orgIdPad,String codFac,String abrOrg, String nomFac,String des, TipoOrganigrama organismo,Integer zonaId) {
        try{
            UbicacionDao ubicacionDao =(UbicacionDao)FactoryDao.buildDao("se.UbicacionDao");            
            OrganigramaDao organigramaDao = (OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");        
            Organigrama organigrama = organigramaDao.buscarXId(facId);
            Ubicacion ubicacion=null;
            
            Organigrama organigramaPadre=new Organigrama();
            if(orgIdPad!=0){
               organigramaPadre = organigramaDao.buscarXId(orgIdPad);
               organigrama.setOrgiPadId(organigramaPadre.getOrgiId());
            }
            else{
                organigrama.setOrgiPadId(null);
            }
            if(zonaId!=0){
                ubicacion=ubicacionDao.buscarPorId(zonaId);               
                organigrama.setZona(ubicacion);
            }
            else{
                if(null!=organigrama.getZona())
                    organigrama.setZona(null);
            }

            organigrama.setCodOrgi(codFac);
            organigrama.setAbrOrgi(abrOrg);
            organigrama.setNomOrgi(nomFac);
            organigrama.setCamOrgi(des);
            organigrama.setTipoOrganigrama(organismo);
            
            
            
            organigramaDao.update(organigrama);
            
            JSONObject oResponse = new JSONObject();
            oResponse.put("orgId", organigrama.getOrgiId());
            oResponse.put("tipId", organigrama.getTipoOrganigrama().getNomTipOrgi());
            if(orgIdPad==0)
                oResponse.put("orgIdPad","");
            else{
                oResponse.put("orgIdPad", organigrama.getOrgiPadId());}
            if(zonaId==0)
                oResponse.put("ubiId","");
            else{
                oResponse.put("ubiId",organigrama.getZona().getUbiId());
            }
            oResponse.put("codOrg", organigrama.getCodOrgi());
            oResponse.put("abrOrg", organigrama.getAbrOrgi());
            oResponse.put("nomOrg", organigrama.getNomOrgi());
            oResponse.put("datOrg", organigrama.getCamOrgi());
            return WebResponse.crearWebResponseExito(" Organigrama actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"Actualizar",e);
            return WebResponse.crearWebResponseError("Error, El Organigrama no fue actualizado");
        }
    }
}

