/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.servicioRidea.v1.core;

/**
 *
 * @author User
 */
import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.servicioRidea.v1.tx.ActualizarServicioRIDEATx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.servicioRidea.v1.tx.VerServcioRIDEATx;

public class ComponentRegister implements IComponentRegister {
    
    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);
        seComponent.setName("servicioRidea");
        seComponent.setVersion(1);
        seComponent.addTransactionPUT("actualizarServicioRIDEA", ActualizarServicioRIDEATx.class);
        seComponent.addTransactionGET("verServicioRIDEA", VerServcioRIDEATx.class);
        return seComponent;
    }
}
