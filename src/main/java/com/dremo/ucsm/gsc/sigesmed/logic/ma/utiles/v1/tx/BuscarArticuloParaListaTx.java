/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ArticuloEscolarDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ArticuloEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class BuscarArticuloParaListaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(BuscarArticuloParaListaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject jsonData = (JSONObject)wr.getData();
        String nomArticulo = jsonData.getString("nom");
        return buscarArticulo(nomArticulo);
    }
    private WebResponse buscarArticulo (String articulo){
        try{
            ArticuloEscolarDao articuloEscolarDao = (ArticuloEscolarDao) FactoryDao.buildDao("ma.ArticuloEscolarDao");
            
                List<ArticuloEscolar> articuloEscolar = articuloEscolarDao.buscarArtiPorNombre(articulo);
                if (articulo == null){
                    return WebResponse.crearWebResponseError("No se encontro el articulo",WebResponse.BAD_RESPONSE);
                }else{
                    return WebResponse.crearWebResponseExito("Resultado de busqueda",WebResponse.OK_RESPONSE).setData(
                            new JSONArray(EntityUtil.listToJSONString(new String[]{"artEscId", "tipoArticulo", "preArticulo", "nomArticulo","desArticulo","resMinArticulo"},
                                    new String[]{"id","tip","pre","nom","des","res"},articuloEscolar)));
                }

        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarArticulo",e);
            return WebResponse.crearWebResponseError("Error al buscar Articulo",WebResponse.BAD_RESPONSE);
        }   
        
    }
}