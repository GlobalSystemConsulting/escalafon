/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TipoEspecializadoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ListarTiposEspTx  implements ITransaction{
    TipoEspecializado tipesp = new TipoEspecializado();
    List<TipoEspecializado> listTiposEsp;
 
    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*Lectura de Datos*/
        JSONObject requestData = (JSONObject)wr.getData();    

        
        
        /*Parte para la operacion en la Base de Dato*/
        
        TipoEspecializadoDao tipoEspDao = (TipoEspecializadoDao) FactoryDao.buildDao("rdg.TipoEspecializadoDao");        
        
        try {
            listTiposEsp = tipoEspDao.getAllTiposEspActivos();
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo Listar TiposEsp, ", e.getMessage());
        }
       

        JSONArray resTiposEsp = new JSONArray();

        
        //Construir array de TiposEspecializados
        int i=1;
        for(TipoEspecializado tipesp : listTiposEsp){
            JSONObject bo = new JSONObject();
            bo.put("index",i);
            bo.put("id", tipesp.getTesIde());
            bo.put("abr",tipesp.getTesAli());
            bo.put("nom",tipesp.getTesNom());
            bo.put("des",tipesp.getTesDes());
            bo.put("usu",tipesp.getUsuMod());
            bo.put("fec",tipesp.getFecMod());
            bo.put("est",tipesp.getEstReg());
            resTiposEsp.put(bo);
            ++i;
        }

        return WebResponse.crearWebResponseExito("Se Listo correctamente", resTiposEsp);
        //Fin
    }
    
}
