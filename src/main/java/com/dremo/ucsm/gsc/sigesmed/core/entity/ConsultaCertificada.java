/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name="consulta_certificada" ,schema="public" )
public class ConsultaCertificada implements java.io.Serializable {


    @Id
    @Column(name="con_cer_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_consulta_certificada", sequenceName="public.consulta_certificada_con_cer_id_seq" )
    @GeneratedValue(generator="secuencia_consulta_certificada")
    private int con_cer_id;
    @Column(name="atr_usa")
    private String atributos;
    @Column(name="cat")
    private String catalogo;
    @Column(name="log")
    private String logica;
    @Column(name="fil")
    private String filtros;
    @Column(name="tit")
    private String titulo;
    @Column(name="obs")
    private String observaciones;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_emi")
    private Date fecEmision;
    @Column(name="usu_id")
    private int IdUsuEmisor;
    @Column(name="usu_emi")
    private String usuEmisor;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_cer")
    private Date fecCertificacion;
    @Column(name="est")
    private Character estReg;
    @Column(name="sql_que")
    private String consultaSql;
    @Column(name="usu_cer")
    private int IdUsuCer;
    
    public ConsultaCertificada() {
    }
    public ConsultaCertificada(String atributos,String catalogo,String logica,String filtros,String titulo,String observaciones,Date fecEmision,int IdUsuEmisor,String usuEmisor,Date fecCertificacion,Character estReg,String consultaSql) {
        this.atributos = atributos;
        this.catalogo = catalogo;
        this.logica = logica;
        this.filtros = filtros;
        this.titulo = titulo;
        this.observaciones = observaciones;
        this.fecEmision = fecEmision;
        this.IdUsuEmisor = IdUsuEmisor;
        this.usuEmisor = usuEmisor;
        this.fecCertificacion = fecCertificacion;
        this.estReg = estReg;
        this.consultaSql = consultaSql;
    }
    ////////////////////////GETTERS//////////////////
    
    public int getCon_cer_id() {
        return con_cer_id;
    }

    public String getAtributos() {
        return atributos;
    }

    public String getCatalogo() {
        return catalogo;
    }

    public String getLogica() {
        return logica;
    }

    public String getFiltros() {
        return filtros;
    }
    
    public String getTitulo() {
        return titulo;
    }
    
    public String getObservaciones() {
        return observaciones;
    }

    public Date getFecEmision() {
        return fecEmision;
    }
    
    public int getIdUsuEmisor() {
        return IdUsuEmisor;
    }

    public String getUsuEmisor() {
        return usuEmisor;
    }

    public Date getFecCertificacion() {
        return fecCertificacion;
    }

    public Character getEstReg() {
        return estReg;
    }

    public String getConsultaSql() {
        return consultaSql;
    }
    ////////////////////////SETTERS//////////////////
    
    public void setCon_cer_id(int con_cer_id) {
        this.con_cer_id = con_cer_id;
    }

    public void setAtributos(String atributos) {
        this.atributos = atributos;
    }

    public void setCatalogo(String catalogo) {
        this.catalogo = catalogo;
    }

    public void setLogica(String logica) {
        this.logica = logica;
    }

    public void setFiltros(String filtros) {
        this.filtros = filtros;
    }
    
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public void setFecEmision(Date fecEmision) {
        this.fecEmision = fecEmision;
    }
    
    public void setUsuEmisor(int IdUsuEmisor) {
        this.IdUsuEmisor = IdUsuEmisor;
    }

    public void setUsuEmisor(String usuEmisor) {
        this.usuEmisor = usuEmisor;
    }

    public void setFecCertificacion(Date fecCertificacion) {
        this.fecCertificacion = fecCertificacion;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public void setConsultaSql(String consultaSql) {
        this.consultaSql = consultaSql;
    }

    public int getIdUsuCer() {
        return IdUsuCer;
    }

    public void setIdUsuCer(int IdUsuCer) {
        this.IdUsuCer = IdUsuCer;
    }
    
}
