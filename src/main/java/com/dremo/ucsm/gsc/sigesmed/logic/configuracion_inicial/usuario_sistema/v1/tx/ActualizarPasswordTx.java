/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.usuario_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author Administrador
 */
public class ActualizarPasswordTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int usuarioID = 0;
        String passwordNuevo ="---";
        Persona persona=null;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONObject rUsuario = requestData.getJSONObject("usuario");
            JSONObject rPersona = requestData.getJSONObject("persona");
            
            String dni = rPersona.getString("dni");
            String nombres = rPersona.getString("nombre");
            String materno = rPersona.getString("materno");
            String paterno = rPersona.getString("paterno");            
            String email = rPersona.optString("email");
            String numero1 = rPersona.optString("numero1");
            String urlNom;
            usuarioID = rUsuario.getInt("usuarioID");
            
            String password = rUsuario.getString("password");
            passwordNuevo = rUsuario.getString("nuevoPassword");
            
            //Añadir archivo logo
            String icono = rPersona.optString("urlNom");        
            JSONObject arcJSON = rPersona.optJSONObject("archivo");
            
            FileJsonObject file=null;
            if(arcJSON != null){
                        

                if(arcJSON!=null && arcJSON.length()>0){
                    file=new FileJsonObject(arcJSON,icono);
                }else{
                    return WebResponse.crearWebResponseError("No se pudo leer el archvo, datos incorrectos");  
                }
                BuildFile.buildFromBase64("per/", file.getName(),file.getData());
                urlNom="../archivos/per/"+file.getName();
            }
            else{
                urlNom=null;
            }
            
            PersonaDao personaDao=(PersonaDao)FactoryDao.buildDao("PersonaDao");
            persona=personaDao.buscarPorID(usuarioID);
            
            persona.setDni(dni);
            persona.setApePat(paterno);
            persona.setApeMat(materno);
            persona.setEmail(email);
            persona.setNom(nombres);
            persona.setNum1(numero1);
            persona.setFoto(urlNom);
            
            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la contrasena, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        UsuarioDao usuarioDao = (UsuarioDao)FactoryDao.buildDao("UsuarioDao");
        PersonaDao personaDao=(PersonaDao)FactoryDao.buildDao("PersonaDao");
        try{
            if(!passwordNuevo.contentEquals("---"))
                 usuarioDao.cambiarPassword(usuarioID, passwordNuevo);
            personaDao.update(persona);
        }catch(Exception e){
            System.out.println("No se pudo actualizar la contrasena\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la contrasena", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("Se actualizo el perfil correctamente");
        //Fin
    }
    
}
