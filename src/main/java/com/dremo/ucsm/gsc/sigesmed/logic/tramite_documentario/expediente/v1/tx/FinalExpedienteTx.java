/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.ExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abel
 */
public class FinalExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        List<Integer> expedientes = new ArrayList<Integer>();
        try{
            JSONArray requestData = (JSONArray)wr.getData();
            
            for( int i = 0 ; i < requestData.length(); i++){
                JSONObject bo = requestData.getJSONObject(i);
                expedientes.add(bo.getInt("expedienteID"));
            }            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo finalizar expediente, datos incorrectos", e.getMessage() );
        }
        
        ExpedienteDao expediente = (ExpedienteDao)FactoryDao.buildDao("std.ExpedienteDao");
        try{
            expediente.finalizarExpedientes(expedientes,wr.getIdUsuario());
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo finalizar expediente", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("Los expedientes se actualizaron correctamente");
        //Fin
    }
    
}
