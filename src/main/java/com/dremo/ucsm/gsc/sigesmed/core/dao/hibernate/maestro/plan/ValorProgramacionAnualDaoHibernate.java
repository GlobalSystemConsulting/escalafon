package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ValorProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ValoresActitudes;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * Created by Administrador on 31/10/2016.
 */
public class ValorProgramacionAnualDaoHibernate extends GenericDaoHibernate<ValoresActitudes> implements ValorProgramacionAnualDao {
    @Override
    public ValoresActitudes buscarValorPorId(int idVal) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT v FROM ValoresActitudes v WHERE  v.valActId =:id";
            Query query = session.createQuery(hql);
            query.setParameter("id",idVal);

            return (ValoresActitudes)query.uniqueResult();
        }catch (Exception e){
            throw e;
        }finally {
            session.close();
        }
    }
}
