/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorOrganigramaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TrabajadorOrganigramaDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 *
 * @author gscadmin
 */
public class ListarPorOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *  Parte para la operacion en la Base de Datos
        */   
        int orgId = 0;
        long canTotReg = 0;
        int limit=0;
        int offset =0;
        int opcion = 0;
        String nomCampo = "";
        String dataCampo = "";
        
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            orgId = requestData.getInt("orgId");
            limit = requestData.getInt("limit");
            offset = requestData.getInt("offset");
            opcion = requestData.getInt("opcion");
            nomCampo = requestData.getString("nomCampo");
            dataCampo = requestData.getString("dataCampo");
        } catch (Exception e) {
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo listar datos escalafon por organizacion, datos incorrectos", e.getMessage());
        }
                
        List<FichaEscalafonaria> fichas = null;
        FichaEscalafonariaDao fichaEscalafonariaDao = (FichaEscalafonariaDao)FactoryDao.buildDao("se.FichaEscalafonariaDao");
        
        TrabajadorDao trabajadorDao = (TrabajadorDao)FactoryDao.buildDao("se.TrabajadorDao");
        TrabajadorOrganigramaDetalleDao traOrgDetDao = (TrabajadorOrganigramaDetalleDao) FactoryDao.buildDao("se.TrabajadorOrganigramaDetalleDao");
        CategoriaDao categoriaDao =(CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
        
        try{
            switch(opcion){
                case 0: fichas = fichaEscalafonariaDao.listarxOrgTopTen(orgId,limit,offset);
                        canTotReg = fichaEscalafonariaDao.mostrarNumeroRegistros(FichaEscalafonaria.class);
                        break;
                
                case 1: 
                        canTotReg = fichaEscalafonariaDao.canRegBusqPar(orgId,nomCampo, dataCampo); 
                        fichas = fichaEscalafonariaDao.busquedaParametrizada(orgId,limit, offset, nomCampo, dataCampo);
                        break;
            }
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los datos de escalafon  \n"+e);
            return WebResponse.crearWebResponseError("No se pudo los datpos de escalafon ", e.getMessage() );
        }
        
       
        JSONArray miArray = new JSONArray();
        for(FichaEscalafonaria datosFE:fichas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("ficEscId", datosFE.getFicEscId());
            oResponse.put("perApePat", datosFE.getTrabajador().getPersona().getApePat());
            oResponse.put("perApeMat", datosFE.getTrabajador().getPersona().getApeMat());
            oResponse.put("perNom", datosFE.getTrabajador().getPersona().getNom());
            oResponse.put("perId", datosFE.getTrabajador().getPersona().getPerId());
            oResponse.put("perDni", datosFE.getTrabajador().getPersona().getDni());
            oResponse.put("traId", datosFE.getTrabajador().getTraId());
            if (datosFE.getTrabajador().getOrgiId() != null)
                oResponse.put("orgiId", datosFE.getTrabajador().getOrgiId());
            else
                oResponse.put("orgiId", -1);
            
            if(datosFE.getTrabajador().getTieServ() != null) 
                oResponse.put("traAnoSer", datosFE.getTrabajador().getTieServ());
            else
                oResponse.put("traAnoSer", "Años de servicio no definidos");
            
            List<Trabajador> trabajadores = trabajadorDao.listarTrabajadoresxPersona(datosFE.getTrabajador().getPersona().getPerId());
            JSONArray aTrabajadores = new JSONArray(); 
            for(Trabajador t:trabajadores){
                JSONObject traJSON = new JSONObject();                
                traJSON.put("traId", t.getTraId());

                if (t.getRol() != null) {
                    traJSON.put("rolId", t.getRol().getRolId());
                    traJSON.put("rolNom", t.getRol().getNom());
                    traJSON.put("rolAbr", t.getRol().getAbr());
                } else {
                    traJSON.put("rolId", -1);
                    traJSON.put("rolNom", "");
                }

                traJSON.put("estLab", t.getEstLab());

                if (t.getSal() != null) {
                    traJSON.put("sal", t.getSal());
                } else {
                    traJSON.put("sal", 0);
                }

                if (t.getFecIng() != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    traJSON.put("fecIng", sdf.format((t.getFecIng())));
                    traJSON.put("fecIngEst", true);
                }else {
                    traJSON.put("fecIngEst", false);
                }

                if (t.getAniosOut() != null) {
                    traJSON.put("aniosOut", t.getAniosOut());
                }
                else{
                    traJSON.put("aniosOut", 0);
                }

                //Datos de persona
                traJSON.put("perId", t.getPersona().getPerId());
                traJSON.put("perDni", t.getPersona().getDni());
                if (t.getOrgiId()!= null && t.getOrgiId()>0){ 
                    traJSON.put("orgiId", t.getOrgiId());
                    TrabajadorOrganigramaDetalle traOrgDet = traOrgDetDao.buscarPorId(t.getTraId(), t.getOrgiId());
                    traJSON.put("tipOrgId", traOrgDet.getOrganigrama().getTipoOrganigrama().getTipOrgiId());
                    traJSON.put("datOrgId", traOrgDet.getOrganigrama().getTipoOrganigrama().getDatosOrganigramas().getDatOrgiId());
                    if (traOrgDet.getPlaId() != null)
                        traJSON.put("plaId",traOrgDet.getPlaId());
                    else
                        traJSON.put("plaId",0);

                    if (traOrgDet.getCatId() != null && traOrgDet.getCatId()>0){
                        Categoria categoria = categoriaDao.obtenerDetalle(traOrgDet.getCatId());
                        traJSON.put("catId",traOrgDet.getCatId());
                        traJSON.put("nomCat", categoria.getNomCat());
                        traJSON.put("nomPla", categoria.getPlanilla().getNomPla());
                    }else{
                        traJSON.put("catId",0);
                        traJSON.put("nomCat", "");
                        traJSON.put("nomPla", "");
                    }
                    if (traOrgDet.getCarId() != null && traOrgDet.getCarId()>0)
                        traJSON.put("carId",traOrgDet.getCarId());
                    else
                        traJSON.put("carId",0);

                    if (traOrgDet.getUbiId() != null && traOrgDet.getUbiId()>0)
                        traJSON.put("ubiId",traOrgDet.getUbiId());
                    else
                        traJSON.put("ubiId",0);

                }else{
                    traJSON.put("orgiId", 0);
                    traJSON.put("plaId",0);
                    traJSON.put("catId",0);
                    traJSON.put("carId",0);
                    traJSON.put("ubiId",0);
                    traJSON.put("tipOrgId",0);
                    traJSON.put("datOrgId",0);
                }
                
                aTrabajadores.put(traJSON);
            }
            
            
            oResponse.put("trabajadores", aTrabajadores);
            miArray.put(oResponse);
        }
        
        JSONObject respuesta = new JSONObject();
        respuesta.put("dataEscalafon",miArray);
        respuesta.put("canTotReg", canTotReg);
        
        return WebResponse.crearWebResponseExito("Se listo correctamente los trabajadores",respuesta);
    }
    
}
