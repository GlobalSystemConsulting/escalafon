/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.util;

/**
 *
 * @author Carlos
 */
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class DateUtil {

    public static int obtenerAnioSegunFecha(Date fecha) {
        int anio;
        Calendar calen = Calendar.getInstance();
        calen.setTime(fecha);

        anio = calen.get(Calendar.YEAR);
        return anio;
    }

    public static int obtenerMesSegunFecha(Date fecha) {
        int anio;
        Calendar calen = Calendar.getInstance();
        calen.setTime(fecha);

        anio = calen.get(Calendar.MONTH);
        return anio;
    }

    public static int obtenerDiaSegunFecha(Date fecha) {
        int anio;
        Calendar calen = Calendar.getInstance();
        calen.setTime(fecha);

        anio = calen.get(Calendar.DAY_OF_MONTH);
        return anio;
    }

    public static int obtenerDiaSemanaSegunFecha(Date fecha) {
        int anio;
        Calendar calen = Calendar.getInstance();
        calen.setTime(fecha);

        anio = calen.get(Calendar.DAY_OF_WEEK);
        return anio;
    }

    public static int obtenerUltimoDiaMesSegunFecha(Date fecha) {
        int dia;
        Calendar calen = Calendar.getInstance();
        calen.setTime(fecha);
        calen.set(Calendar.DAY_OF_MONTH, calen.getActualMaximum(Calendar.DAY_OF_MONTH));
        dia = calen.get(Calendar.DAY_OF_MONTH);
        return dia;
    }

    public static String obtenerAnioActual() {
        Calendar cal = GregorianCalendar.getInstance();
        int anio = cal.get(Calendar.YEAR);
        return anio + "";
    }

    private static final java.sql.Timestamp DateToSqlTimestamp(java.util.Date utilDate) {
        return new java.sql.Timestamp(utilDate.getTime());
    }

    public static java.sql.Date getCurrentDateOnly() {
        Calendar cal = GregorianCalendar.getInstance();
        return new java.sql.Date(cal.getTimeInMillis());
    }

    public static Timestamp getCurrentDate() {
        Calendar cal = GregorianCalendar.getInstance();
        return new Timestamp(cal.getTimeInMillis());
    }

    public static Integer getAnioActual() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR);
    }

    public static Integer getAnhoFecha(Date fecha) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        return calendar.get(Calendar.YEAR);
    }

    public static Integer getMesActual() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.MONTH) + 1;
    }

    public static Integer getDiaActual() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DATE) + 1;
    }

    public static boolean esFechaValida(Date fecha) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.AM_PM, Calendar.AM);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Calendar fec = Calendar.getInstance();
        fec.setTime(fecha);
        fec.set(Calendar.AM_PM, Calendar.AM);
        fec.set(Calendar.HOUR, 0);
        fec.set(Calendar.MINUTE, 0);
        fec.set(Calendar.SECOND, 0);
        fec.set(Calendar.MILLISECOND, 0);

        if (!fec.after(cal)) {
            return true;
        }
        return false;
    }

    private static Pattern emailPattern = Pattern.compile("\\w[\\w\\._]*\\w@[\\w_]+\\.(\\w{2,3})(\\.\\w{2,3})?");

    public static boolean isEmail(String value) {
        return emailPattern.matcher(value).matches();
    }

    public static String convertDateToString(Date fecha) {
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        String str = sdf.format(fecha);
        return str;
    }

    public static String convertDateToStringWithTime(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        String str = sdf.format(fecha);
        return str;
    }

    public static Date convertStringToDate(String fecha) {
        Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            date = (Date) formatter.parse(fecha);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static final java.sql.Date dateToSqlDate(java.util.Date utilDate) {
        return new java.sql.Date(utilDate.getTime());
    }

    public static final java.sql.Time dateToSqlTime(java.util.Date utilDate) {
        return new java.sql.Time(utilDate.getTime());
    }

    public static final java.sql.Timestamp dateToSqlTimestamp(java.util.Date utilDate) {
        return new java.sql.Timestamp(utilDate.getTime());
    }

    public static final String getHoraActual() {
        Calendar calendario = Calendar.getInstance();
        Integer hora = calendario.get(Calendar.HOUR);
        Integer minutos = calendario.get(Calendar.MINUTE);
        int ampm = calendario.get(Calendar.AM_PM);
        return String.valueOf(hora) + ":" + String.valueOf(minutos) + " " + (ampm == Calendar.AM ? "am" : "pm");
    }

    public static int diferenciaFechas(Calendar fechaInicio, Calendar fechaFin, int valor) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        int retorno = 0;
        java.util.Date date1 = null;
        java.util.Date date2 = null;
        try {

            Calendar cal1 = null;
            date1 = fechaInicio.getTime();
            cal1 = Calendar.getInstance();

            Calendar cal2 = null;
            date2 = fechaFin.getTime();
            cal2 = Calendar.getInstance();

            cal1.setTime(date1);
            long ldate1 = date1.getTime() + cal1.get(Calendar.ZONE_OFFSET) + cal1.get(Calendar.DST_OFFSET);

            cal2.setTime(date2);
            long ldate2 = date2.getTime() + cal2.get(Calendar.ZONE_OFFSET) + cal2.get(Calendar.DST_OFFSET);

            int hr1 = (int) (ldate1 / 3600000);
            int hr2 = (int) (ldate2 / 3600000);

            int days1 = (int) hr1 / 24;
            int days2 = (int) hr2 / 24;

            int dateDiff = days2 - days1;
            int yearDiff = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
            int monthDiff = yearDiff * 12 + cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
            int minDiff = cal2.get(Calendar.MINUTE) - cal1.get(Calendar.MINUTE);

            if (valor == 1) {
                retorno = dateDiff;
            } else if (valor == 2) {
                retorno = monthDiff;
            } else if (valor == 3) {
                retorno = yearDiff;
            } else if (valor == 4) {
                retorno = minDiff;
            }
        } catch (Exception pe) {
            pe.printStackTrace();
        }
        return retorno;
    }

    public static int diferenciaFechas(Date fechaInicio, Date fechaFin, int valor) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        int retorno = 0;

        try {

            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(fechaInicio);

            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(fechaFin);

            cal1.setTime(fechaInicio);
            long ldate1 = fechaInicio.getTime() + cal1.get(Calendar.ZONE_OFFSET) + cal1.get(Calendar.DST_OFFSET);

            cal2.setTime(fechaFin);
            long ldate2 = fechaFin.getTime() + cal2.get(Calendar.ZONE_OFFSET) + cal2.get(Calendar.DST_OFFSET);

            int hr1 = (int) (ldate1 / 3600000);
            int hr2 = (int) (ldate2 / 3600000);

            int days1 = (int) hr1 / 24;
            int days2 = (int) hr2 / 24;

            int dateDiff = days2 - days1;
            int yearDiff = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
            int monthDiff = yearDiff * 12 + cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
            int minDiff = cal2.get(Calendar.MINUTE) - cal1.get(Calendar.MINUTE);

            if (valor == 1) {
                retorno = dateDiff;
            } else if (valor == 2) {
                retorno = monthDiff;
            } else if (valor == 3) {
                retorno = yearDiff;
            } else if (valor == 4) {
                retorno = minDiff;
            }
        } catch (Exception pe) {
            pe.printStackTrace();
        }
        return retorno;
    }

    public static int diferenciasDeFechas(Date fechaInicial, Date fechaFinal) {

        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String fechaInicioString = df.format(fechaInicial);
        try {
            fechaInicial = df.parse(fechaInicioString);
        } catch (Exception ex) {
        }

        String fechaFinalString = df.format(fechaFinal);
        try {
            fechaFinal = df.parse(fechaFinalString);
        } catch (Exception ex) {
        }

        long fechaInicialMs = fechaInicial.getTime();
        long fechaFinalMs = fechaFinal.getTime();
        long diferencia = fechaFinalMs - fechaInicialMs;
        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
        return ((int) dias);
    }

    public static Date moverHoraAlFinalDelDia(Date fecha) {
        Calendar cal = null;
        try {
            cal = Calendar.getInstance();
            cal.setTime(fecha);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
        } catch (Exception e) {
            return null;
        }

        return cal.getTime();
    }

    public static Date moverHoraAlInicioDelDia(Date fecha) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        cal.set(Calendar.HOUR_OF_DAY, 00);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 00);

        return cal.getTime();
    }

    public static String getTimestampToString(Timestamp date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-5"));
        return sdf.format(date);
    }

    /**
     * Devuelve la Fecha en formato String yyyy/MM/dd
     *
     * @param myDate atributo fecha
     * @return String de la fecha
     */
    public static String getString2Date(Date myDate) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(myDate);
    }

    /**
     * Devuelve la Fecha en formato Date yyyy/MM/dd
     *
     * @param myDate atributo fecha como String
     * @return Date de la fecha
     */
    public static Date getDate2String(String myDate) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return format.parse(myDate);
        } catch (ParseException ex) {
            return null;
        }
    }

    /**
     * Devuelve la fecha actual
     *
     * @return
     */
    public static Date getthisDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    /**
     * Añade dias a una fecha
     *
     * @param fecha Fecha que se desea añadir los dias
     * @param dias Dias que se desean añadir
     * @return
     */
    public static Date addDays(Date fecha, int dias) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        cal.add(Calendar.DAY_OF_YEAR, dias);
        return cal.getTime();
    }

    /**
     * Suma o resta las horas recibidos a la fecha
     *
     * @param fecha Fecha que se desea añadir las horas
     * @param horas si es positivo aumenta, si es negativo resta
     * @return
     *
     */
    public static Date sumarRestarHorasFecha(Date fecha, int horas) {

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(fecha); // Configuramos la fecha que se recibe

        calendar.add(Calendar.HOUR, horas);  // numero de horas a añadir, o restar en caso de horas<0

        return calendar.getTime(); // Devuelve el objeto Date con las nuevas horas añadidas

    }

    /**
     * Suma o resta las horas recibidos a la fecha
     *
     * @param fecha Fecha que se desea añadir las horas
     * @param minutos si es positivo aumenta, si es negativo resta
     * @return
     *
     */
    public static Date sumarRestarMinutosFecha(Date fecha, int minutos) {

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(fecha); // Configuramos la fecha que se recibe

        calendar.add(Calendar.MINUTE, minutos);  // numero de horas a añadir, o restar en caso de horas<0

        return calendar.getTime(); // Devuelve el objeto Date con las nuevas horas añadidas

    }

    /**
     * Retorna True si hora1 es mayor o igual a hora2
     *
     * @param hora1 Fecha donde solo importa las horas,minutos y segundos
     * @param hora2 Fecha donde solo importa las horas,minutos y segundos
     * @return
     *
     */
    public static Boolean compararHora1yHora2(Date hora1, Date hora2) {

        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss");
        String actual = sdf.format(hora1);
        String limit = sdf.format(hora2);

        String[] parts = actual.split(":");
        Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
        cal1.set(Calendar.MINUTE, Integer.parseInt(parts[1]));
        cal1.set(Calendar.SECOND, Integer.parseInt(parts[2]));

        parts = limit.split(":");
        Calendar cal2 = Calendar.getInstance();
        cal2.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
        cal2.set(Calendar.MINUTE, Integer.parseInt(parts[1]));
        cal2.set(Calendar.SECOND, Integer.parseInt(parts[2]));

        //cal2.add(Calendar.DATE, 1);
        if (cal1.before(cal2)) {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;

    }

    /**
     * Retorna la fecha setiando la hora,minuto,segundo y milisegundo en 0
     *
     * @param date Fecha que se va modificar
     * @return
     *
     */
    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Retorna el date del inicio de semana semana de la fecha dada
     *
     * @param date Fecha
     * @return
     *
     */
    public static Date getInicioSemana(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        // get start of this week in milliseconds
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        return cal.getTime();
    }

    /**
     * Retorna el date del fin de semana semana de la fecha dada que sera el
     * inicio de la siguiente semana
     *
     * @param date Fecha
     * @return
     *
     */
    public static Date getFinSemana(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        // get start of this week in milliseconds
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        cal.add(Calendar.WEEK_OF_YEAR, 1);
        return cal.getTime();
    }

    /**
     * Retorna la diferencia de Date d2 con d1
     *
     * @param d1 Fecha menor
     * @param d2 Fecha mayor
     * @param tipo 1:segundo,2:minutos,3:horas
     * @return
     *
     */
    public static Integer getDiferenciaFechas(Date d1, Date d2, Integer tipo) {
        long diff = d2.getTime() - d1.getTime();

        if (tipo == 1) //segundos
        {
            return Integer.valueOf((diff / 1000) + "");
        } else if (tipo == 2)//minutos
        {
            return Integer.valueOf((diff / (60 * 1000)) + "");
        } else if (tipo == 3) {
            return Integer.valueOf((diff / (60 * 60 * 1000)) + "");
        }
        return -1;

    }
    /**
     * Retorna la la fecha indicando el primer dia del mes
     * @param fecha Fecha a ingresar
     * @return 
     **/
    public static Date getInicioMes(Date fecha) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(fecha);
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        setTimeToBeginningOfDay(calendar);
        return calendar.getTime();
    }
    
    /**
     * Retorna la la fecha indicando el ultimo dia del mes
     * @param fecha Fecha a ingresar
     * @return 
     **/
    public static Date getFinMes(Date fecha) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(fecha);
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setTimeToEndofDay(calendar);
        return calendar.getTime();
    }
    
    /**
     * Retorna la la fecha indicando el primer dia del mes
     * @param anho anho de la fecha
     * @param mes mes de la fecha
     * @param dia dia de la fecha
     * @return 
     **/
    public static Date getInicioMes(Integer anho,Integer mes,Integer dia) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(anho,mes,dia);
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        setTimeToBeginningOfDay(calendar);
        return calendar.getTime();
    }
    
    /**
     * Retorna la la fecha indicando el ultimo dia del mes
     * @param anho anho de la fecha
     * @param mes mes de la fecha
     * @param dia dia de la fecha
     * @return 
     **/
    public static Date getFinMes(Integer anho,Integer mes,Integer dia) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(anho,mes,dia);
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setTimeToEndofDay(calendar);
        return calendar.getTime();
    }
    

    public static void setTimeToBeginningOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public static void setTimeToEndofDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }

}
