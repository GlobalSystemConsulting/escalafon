package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.TipoParienteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarTipoParienteTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        GenericMMIDaoHibernate<TipoParienteMMI> hb = new GenericMMIDaoHibernate<>();
        JSONArray tipoPariente = new JSONArray();

        List tipoParientesSQL;

        try {
            tipoParientesSQL = hb.buscarTodos(TipoParienteMMI.class);

            for (Object obj : tipoParientesSQL) {
                TipoParienteMMI tempTipoPariente = (TipoParienteMMI) obj;
                JSONObject temp = new JSONObject();
                temp.put("tpaId", tempTipoPariente.getTpaId());
                temp.put("tpaDes", tempTipoPariente.getTpaDes());
                tipoPariente.put(temp);
            }

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al cargar los tipos de pariente! ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Listaron los tipos de pariente", tipoPariente);
    }

}
