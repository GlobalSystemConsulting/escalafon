package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx.*;


/**
 * Created by Administrador on 04/10/2016.
 */
public class ComponentRegister implements IComponentRegister {
    @Override
    public WebComponent createComponent() {
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.SUBMODULO_CAPACITACION);

        //Registrando el Nombre del componente
        component.setName("cursos");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarCursos", ListarCursosCapacitacionTx.class);
        component.addTransactionGET("listarSedes", ListarSedesTx.class);

        component.addTransactionPOST("registrarCurso", RegistrarCursoCapacitacionTx.class);
        component.addTransactionPOST("registrarMetodologia", RegistrarMetodologiaTx.class);
        component.addTransactionPOST("registrarMaterial", RegistrarMaterialTx.class);
        component.addTransactionPOST("registrarPersona", RegistrarPersonaTx.class);
        component.addTransactionPOST("registrarCapacitadores", RegistrarCapacitadorTx.class);
        component.addTransactionPOST("registrarTemario", RegistrarTemarioTx.class);
        component.addTransactionPOST("registrarEvaluacion", RegistrarEvaluacionTx.class);
        component.addTransactionPOST("registrarCriterio", RegistrarCriterioTx.class);
        component.addTransactionPOST("registrarParticipante", RegistrarParticipanteTx.class);
        component.addTransactionPOST("registrarDesarrollo", RegistrarDesarrolloTemaTx.class);
        component.addTransactionPOST("registrarComentario", RegistrarComentarioTemaTx.class);
        component.addTransactionPOST("registrarEvaluacionDesarrollo", RegistrarEvaluacionDesarrolloTx.class);
        component.addTransactionPOST("registrarPregunta", RegistrarPreguntaTx.class);
        component.addTransactionPOST("reasignarPregunta", ReasignarPreguntaTx.class);
        component.addTransactionPOST("registrarEvaluacionParticipante", RegistrarEvaluacionParticipanteTx.class);
        component.addTransactionPOST("registrarRespuestaEncuesta", RegistrarRespuestaEncuestaTx.class);
        component.addTransactionPOST("registrarAsistencia", RegistrarAsistenciaTx.class);
        component.addTransactionPOST("registrarJustificacion", RegistrarJustificacionTx.class);
        component.addTransactionPOST("registrarMensaje", RegistrarMensajeTx.class);
        
        component.addTransactionPUT("editarCurso",EditarCursoCapacitacionTx.class);
        component.addTransactionPUT("editarTemario",EditarTemarioTx.class);
        component.addTransactionPUT("editarComentario",EditarComentarioTemaTx.class);
        component.addTransactionPUT("editarEvaluacion",EditarEvaluacionTx.class);
        component.addTransactionPUT("editarPregunta", EditarPreguntaTx.class);
        component.addTransactionPUT("editarEvaluacionDesarrollo", EditarEvaluacionDesarrolloTx.class);
        component.addTransactionPUT("editarEvaluacionParticipante", EditarEvaluacionParticipanteTx.class);
        component.addTransactionPUT("editarAsistenciaParticipante", EditarAsistenciaParticipanteTx.class);

        component.addTransactionDELETE("eliminarCurso",EliminarCursoCapacitacionTx.class);
        component.addTransactionDELETE("eliminarSede",EliminarSedeTx.class);
        component.addTransactionDELETE("eliminarTemario",EliminarTemarioTx.class);
        component.addTransactionDELETE("eliminarComentario",EliminarComentarioTemaTx.class);
        component.addTransactionDELETE("eliminarEvaluacion",EliminarEvaluacionTx.class);
        component.addTransactionDELETE("eliminarPregunta",EliminarPreguntaTx.class);

        component.addTransactionGET("listarOrganizaciones", ListarOrganizacionesTx.class);
        component.addTransactionGET("listarObjetivos", ListarObjetivosCapacitacionTx.class);
        component.addTransactionGET("listarPerfiles", ListarPerfilesCapacitacionTx.class);
        component.addTransactionGET("listarPersonas", ListarPersonasTx.class);
        component.addTransactionGET("listarTemas", ListarTemasTx.class);
        component.addTransactionGET("listarComentarios", ListarComentariosTemaTx.class);
        component.addTransactionGET("listarDesarrollo", ListarDesarrolloTemaTx.class);
        component.addTransactionGET("listarEvaluacionesDesarrollo", ListarEvaluacionesDesarrolloTx.class);
        component.addTransactionGET("listarEvaluaciones", ListarEvaluacionesTx.class);
        component.addTransactionGET("listarPreguntas", ListarPreguntasTx.class);
        component.addTransactionGET("listarHorarios", ListarHorariosTx.class);
        component.addTransactionGET("listarAsistenciaParticipante", ListarAsistenciaParticipanteTx.class);
        component.addTransactionGET("calcularCriterios", CalcularCriteriosTx.class);
        component.addTransactionGET("generarReportes", GenerarReportesTx.class);
        component.addTransactionGET("generarCertificados", GenerarCertificadosTx.class);
        
        component.addTransactionPOST("enviarMensaje", EnviarMensajeCapacitacionTx.class);
        return component;
    }
}
