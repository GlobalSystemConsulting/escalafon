package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;

/**
 * Created by Administrador on 13/10/2016.
 */
public enum CargosTrabajadorDefault {
    DOCENTE("Docente"),
    COORDINADOR_ADMINISTRATIVO("Coordinador administrativo y de recursos educativos"),
    COORDINADOR_INNOVACION("Coordinador de innovación y soporte tecnológico"),
    PERSONAL_APOYO_PEDAGOGICO("Personal de apoyo pedagógico"),
    PERSONAL_MANTENIMIENTO("Personal de mantenimiento"),
    PERSONAL_VIGILANCIA("Personal de vigilancia"),
    PERSONAL_SECRETARIA("Personal de secretaría"),
    PSICOLOGO("Psicólogo o trabajador social"),
    ACOMPAÑANTE_INGLES("Acompañante especilizado en inglés");
    private String nombre;
    CargosTrabajadorDefault(String nombre){
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValue() {
        return nombre;
    }

    public String toString() {
        return nombre;
    }
}
