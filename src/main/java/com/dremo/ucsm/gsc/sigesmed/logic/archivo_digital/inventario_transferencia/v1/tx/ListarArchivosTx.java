/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.ArchivoInventarioDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.InventarioTransferenciaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.InventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.ArchivosInventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.DetalleInventarioTransferencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author admin
 */
public class ListarArchivosTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int inv_tra_id = 0;
        int det_inv_tra = 0;
         JSONObject requestData = (JSONObject)wr.getData();
         List<ArchivosInventarioTransferencia> arch_inv = null;
         List<InventarioTransferencia> inventarios = null;
        inv_tra_id = requestData.getInt("inv_tra_id");
           
        
        
        ArchivoInventarioDAO archivo_dao = (ArchivoInventarioDAO)FactoryDao.buildDao("sad.ArchivoInventarioDAO");
         
        InventarioTransferenciaDAO inv_dao = (InventarioTransferenciaDAO)FactoryDao.buildDao("sad.InventarioTransferenciaDAO");
        
        try{
            inventarios = inv_dao.buscarPorInventarioTransferencia(inv_tra_id);
            for(InventarioTransferencia it : inventarios){
                List<DetalleInventarioTransferencia> dit;
                dit = it.getInvTransDet();
                for(DetalleInventarioTransferencia det_inv : dit){
                   det_inv_tra = det_inv.getdet_inv_tra();
            //    arch_inv =  det_inv.getArchivoInventarioTrans();
                   break; // ojo: como la relacion es de 1 a 1 no es necesario que continue con la busqueda 
                }
                break;
            }
            
            
            arch_inv = archivo_dao.listarArchivos(det_inv_tra);
            
            
        }catch(Exception e){
            System.out.println("No se pudo Listar los Archivos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Archivos", e.getMessage() );
        }
         JSONArray miArray = new JSONArray();
        
        for(ArchivosInventarioTransferencia ait :arch_inv ){
            
            JSONObject oResponse = new JSONObject();
            
            
            oResponse.put("arc_inv_tra_id",ait.getarcinvtraID());
            oResponse.put("det_inv_trans",ait.getdetinvtrans());
            oResponse.put("num_fol",ait.getnumfol());
            oResponse.put("titulo",ait.gettitulo());
            oResponse.put("fecha",ait.getfecha());
            oResponse.put("cod_expediente",ait.getcod_exp());
            oResponse.put("nombre_archivo","archivos/"+"sad"+"/"+ait.getNombre_archivo());
            
            miArray.put(oResponse);
        }
           return WebResponse.crearWebResponseExito("Se Listo los Prestamos de Serie Correctamente",miArray); 
         
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
