package com.dremo.ucsm.gsc.sigesmed.core.entity.sdc;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="plantilla_imagen_detalle" ,schema="administrativo")
public class ImagenPlantilla  implements java.io.Serializable {


    @Id
    @Column(name="pid_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_imagen_plantilla", sequenceName="administrativo.plantilla_imagen_detalle_pid_id_seq" )
    @GeneratedValue(generator="secuencia_imagen_plantilla")
    private Integer imgPlaId;
    
    @Column(name="pid_img_adj")
    private String imgAdj;
  
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="pid_pla_id", nullable=false)
    private Plantilla plaId;

    
    @Column(name="pid_des")
    private String descripcion;
    
    public ImagenPlantilla(Integer imgPlaId) {
        this.imgPlaId = imgPlaId;
    }

    public ImagenPlantilla() {
    }

    public ImagenPlantilla(String imgAdj, String descripcion) {
        this.imgAdj = imgAdj;
        this.descripcion = descripcion;
    }

    
    public ImagenPlantilla(String imgAdj, Plantilla plaId,String descripcion) {
        this.imgAdj = imgAdj;
        this.plaId = plaId;
        this.descripcion=descripcion;
    }

    public Integer getImgPlaId() {
        return imgPlaId;
    }

    public void setImgPlaId(Integer imgPlaId) {
        this.imgPlaId = imgPlaId;
    }

    public String getImgAdj() {
        return imgAdj;
    }

    public void setImgAdj(String imgAdj) {
        this.imgAdj = imgAdj;
    }

    public Plantilla getDocId() {
        return plaId;
    }

    public void setDocId(Plantilla plaId) {
        this.plaId = plaId;
    }

    public Plantilla getPlaId() {
        return plaId;
    }

    public void setPlaId(Plantilla plaId) {
        this.plaId = plaId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
   
    
    
    
}


