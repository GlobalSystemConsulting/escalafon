/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author felipe
 */

public class TrabajadorOrganigramaDetallePK implements Serializable {

    
    @Column(name = "tra_id")
    private int traId;
    
    @Column(name = "orgi_id")
    private int orgiId;

    public TrabajadorOrganigramaDetallePK() {
    }

    public TrabajadorOrganigramaDetallePK(int traId, int orgiId) {
        this.traId = traId;
        this.orgiId = orgiId;
    }

    public int getTraId() {
        return traId;
    }

    public void setTraId(int traId) {
        this.traId = traId;
    }

    public int getOrgiId() {
        return orgiId;
    }

    public void setOrgiId(int orgiId) {
        this.orgiId = orgiId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) traId;
        hash += (int) orgiId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrabajadorOrganigramaDetallePK)) {
            return false;
        }
        TrabajadorOrganigramaDetallePK other = (TrabajadorOrganigramaDetallePK) object;
        if (this.traId != other.traId) {
            return false;
        }
        if (this.orgiId != other.orgiId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.se.TrabajadorOrganigramaDetallePK[ traId=" + traId + ", orgiId=" + orgiId + " ]";
    }
    
}
