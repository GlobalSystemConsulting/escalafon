/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;

import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx.EditarSerieDocumentalTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx.EliminarSerieDocumentalTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx.EliminarTablaRetencionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx.ListarSeriesDocumentalesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx.ObtenerTablaRetencionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx.RegistrarCronogramaTransTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx.RegistrarSerieDocumentalTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx.RegistrarTablaRetencionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx.EditarTablaRetencionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx.Reporte_SeriesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx.Reporte_UnidadTx;

/**
 *
 * @author Jeferson
 */
public class ComponentRegister implements IComponentRegister {

    @Override
    public WebComponent createComponent() {

        WebComponent component = new WebComponent(Sigesmed.SISTEMA_ARCHIVO_DIGITAL);

        //Registrando el Nombre del Componente
        component.setName("gestion_series_documentales");
        //version del componente
        component.setVersion(1);

        component.addTransactionPUT("editar_serie_documental", EditarSerieDocumentalTx.class);
        component.addTransactionDELETE("eliminar_serie_documental", EliminarSerieDocumentalTx.class);
        component.addTransactionDELETE("eliminar_tabla_retencion", EliminarTablaRetencionTx.class);
        component.addTransactionGET("listar_series_documentales", ListarSeriesDocumentalesTx.class);
        component.addTransactionGET("obtener_tabla_retencion", ObtenerTablaRetencionTx.class);
        component.addTransactionPOST("registrar_cronograma_trans", RegistrarCronogramaTransTx.class);
        component.addTransactionPOST("registrar_serie_documental", RegistrarSerieDocumentalTx.class);
        component.addTransactionPOST("registrar_tabla_retencion", RegistrarTablaRetencionTx.class);
        component.addTransactionPUT("editar_tabla_retencion", EditarTablaRetencionTx.class);
        component.addTransactionGET("reporte_series", Reporte_SeriesTx.class);
        component.addTransactionGET("reporte_unidad", Reporte_UnidadTx.class);

        return component;

    }

}
