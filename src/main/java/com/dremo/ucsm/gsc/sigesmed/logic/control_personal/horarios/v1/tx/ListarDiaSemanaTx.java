/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiaSemana;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ListarDiaSemanaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
       
        
        
       
        HorarioDao horarioDao = (HorarioDao)FactoryDao.buildDao("cpe.HorarioDao");
        List<DiaSemana> semana= new ArrayList<>();;
        try{
            semana=horarioDao.listarDiaSemana();
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Dia Semana", e.getMessage() );
        }

        JSONArray miArray = new JSONArray();
        for(DiaSemana ho:semana ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("id",ho.getDiaSemId());
            oResponse.put("nom",ho.getNombre());
            

            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

