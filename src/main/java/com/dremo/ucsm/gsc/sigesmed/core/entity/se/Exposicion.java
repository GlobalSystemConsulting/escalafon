/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "exposicion", schema="administrativo")
public class Exposicion implements Serializable {

    @Id
    @Column(name = "exp_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_exposicion", sequenceName="administrativo.exposicion_exp_id_seq" )
    @GeneratedValue(generator="secuencia_exposicion")
    private Integer expId;
    
    @Column(name = "des")
    private String des;
    
    @Column(name = "ins_org")
    private String insOrg;
    
    @Column(name = "tip_par")
    private String tipPar;
    
    @Column(name = "fec_ini")
    @Temporal(TemporalType.DATE)
    private Date fecIni;
    
    @Column(name = "fec_ter")
    @Temporal(TemporalType.DATE)
    private Date fecTer;
    
    @Column(name = "hor_lec")
    private Integer horLec;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "per_id")
    private Persona persona;

    public Exposicion() {
    }

    public Exposicion(Integer expId) {
        this.expId = expId;
    }
    
    public Exposicion(Persona persona, String des, String insOrg, String tipPar, Date fecIni, Date fecTer, Integer horLec, Integer usuMod, Date fecMod, Character estReg) {
        this.persona = persona;
        this.des = des;
        this.insOrg = insOrg;
        this.tipPar = tipPar;
        this.fecIni = fecIni;
        this.fecTer = fecTer;
        this.horLec = horLec;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public Integer getExpId() {
        return expId;
    }

    public void setExpId(Integer expId) {
        this.expId = expId;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getInsOrg() {
        return insOrg;
    }

    public void setInsOrg(String insOrg) {
        this.insOrg = insOrg;
    }

    public String getTipPar() {
        return tipPar;
    }

    public void setTipPar(String tipPar) {
        this.tipPar = tipPar;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecTer() {
        return fecTer;
    }

    public void setFecTer(Date fecTer) {
        this.fecTer = fecTer;
    }

    public Integer getHorLec() {
        return horLec;
    }

    public void setHorLec(Integer horLec) {
        this.horLec = horLec;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (expId != null ? expId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Exposicion)) {
            return false;
        }
        Exposicion other = (Exposicion) object;
        if ((this.expId == null && other.expId != null) || (this.expId != null && !this.expId.equals(other.expId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Exposicion{" + "expId=" + expId + ", des=" + des + ", insOrg=" + insOrg + ", tipPar=" + tipPar + ", fecIni=" + fecIni + ", fecTer=" + fecTer + ", horLec=" + horLec + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + ", persona=" + persona + '}';
    }


}
