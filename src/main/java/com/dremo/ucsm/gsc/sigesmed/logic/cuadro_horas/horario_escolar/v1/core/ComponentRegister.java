/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx.BuscarDocentesDisponiblesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx.BuscarHorarioEscolarTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx.EliminarDetalleHorarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx.InsertarDetalleHorarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx.InsertarHorarioEscolarTx;
import com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.horario_escolar.v1.tx.ListarDistribucionHorariaPlazasTx;

/**
 *
 * @author abel
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_ELABORACION_CUADRO_HORAS);        
        
        //Registrando el Nombre del componente
        component.setName("horario");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarHorario", InsertarHorarioEscolarTx.class);
        component.addTransactionPOST("insertarDetalleHorario", InsertarDetalleHorarioTx.class);
        component.addTransactionGET("buscarHorario", BuscarHorarioEscolarTx.class);
        component.addTransactionGET("buscarDocentesDisponibles", BuscarDocentesDisponiblesTx.class);
        component.addTransactionGET("listarDistribucionHoraria", ListarDistribucionHorariaPlazasTx.class);
        
        component.addTransactionDELETE("eliminarDetalleHorario", EliminarDetalleHorarioTx.class);
        
        return component;
    }
}
