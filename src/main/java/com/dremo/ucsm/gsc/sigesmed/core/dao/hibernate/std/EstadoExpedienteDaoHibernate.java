/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.std;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.EstadoExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.EstadoExpediente;

/**
 *
 * @author abel
 */
public class EstadoExpedienteDaoHibernate extends GenericDaoHibernate<EstadoExpediente> implements EstadoExpedienteDao {

}
