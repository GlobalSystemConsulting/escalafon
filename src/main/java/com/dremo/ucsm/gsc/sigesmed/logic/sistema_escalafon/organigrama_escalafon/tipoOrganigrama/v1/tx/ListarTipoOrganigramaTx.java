/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.tipoOrganigrama.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FacultadDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoOrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Facultad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoOrganigrama;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author forev
 */
public class ListarTipoOrganigramaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarTipoOrganigramaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<TipoOrganigrama> tipoOrganigrama = null;
        TipoOrganigramaDao tipoOrganigramaDao = (TipoOrganigramaDao)FactoryDao.buildDao("se.TipoOrganigramaDao");
        
        try{
            tipoOrganigrama = tipoOrganigramaDao.listarXTipoOrganigrama();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar Tipo Organigrama",e);
            System.out.println("No se pudo listar Tipo Organigrama.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas Tipo Organigrama.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoOrganigrama c: tipoOrganigrama ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tipId", c.getTipOrgiId());
            oResponse.put("datNom", c.getDatosOrganigramas().getNomDatOrgi()+" "+c.getDatosOrganigramas().getAnioDatOrgi());
            oResponse.put("datId",c.getDatosOrganigramas().getDatOrgiId());
            oResponse.put("codTipOrga", c.getCodTipOrgi());
            oResponse.put("nomTipOrga", c.getNomTipOrgi());
            oResponse.put("desTipOrga", c.getDesTipOrgi());     
            oResponse.put("fecMod",c.getFecMod());
            oResponse.put("UsuMod",c.getUsuMod());
            oResponse.put("estReg",c.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Tipo Organigrama fueron listadas exitosamente", miArray);
    }
}

