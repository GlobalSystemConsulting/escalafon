package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.SesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.TareaSesionAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 15/12/2016.
 */
public class RegistrarTareaSesionTx implements ITransaction{
    private Logger logger = Logger.getLogger(RegistrarTareaSesionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idUnidad = data.getInt("sesid");

        String nom= data.getString("nom").toUpperCase();
        String des = data.optString("des");
        long fec = data.optLong("fec");


        TareaSesionAprendizaje tareaSesion = new TareaSesionAprendizaje(nom,new Date(fec),des);

        return registrarTareaSesion(idUnidad, tareaSesion);
    }

    private WebResponse registrarTareaSesion(int idSesion,TareaSesionAprendizaje tareaSesion) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            SesionAprendizaje sesion= sesionDao.buscarSesionById(idSesion);
            tareaSesion.setSesion(sesion);
            sesionDao.registrarTareaSesion(tareaSesion);
            JSONObject jsonTarea = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"tarIdSes"},
                    new String[]{"id"},
                    tareaSesion
            ));
            /*String tipoMat = materialRecurso.getTip().equals('L')? "Libro" : "Escritorio";
            jsonMaterial.put("tip",tipoMat);*/
            return WebResponse.crearWebResponseExito("Se registro el material",jsonTarea);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarMaterialUnidad",e);
            return WebResponse.crearWebResponseError("No se puede registrar el material");
        }
    }
}
