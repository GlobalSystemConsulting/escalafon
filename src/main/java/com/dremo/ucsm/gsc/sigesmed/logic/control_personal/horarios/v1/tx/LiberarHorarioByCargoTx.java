/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;

import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiaSemana;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioDet;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public class LiberarHorarioByCargoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
       
        Integer  horarioToLiberar=null;
        HorarioCab horarioCab=null;
        Organizacion organizacion=null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            horarioToLiberar=requestData.getInt("id");
            Integer org=requestData.getInt("organizacionID");
            horarioCab=new HorarioCab(horarioToLiberar);
            organizacion=new Organizacion(org);

        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }
        
       
        HorarioDao horarioDao = (HorarioDao)FactoryDao.buildDao("cpe.HorarioDao");
        
        try{
            horarioDao.liberarHorarioById(horarioToLiberar);
            horarioDao.actualizarHorarioFromTrabajador(horarioCab,null, organizacion);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Liberar el Horario", e.getMessage() );
        }

        return WebResponse.crearWebResponseExito("Se Libero correctamente");        
        //Fin
    }
    
}

