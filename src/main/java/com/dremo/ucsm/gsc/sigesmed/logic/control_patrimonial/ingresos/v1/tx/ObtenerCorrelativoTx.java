/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;


import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioFisico;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioFisicoDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ValorContable;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioFisicoDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.MovimientoIngresosDAO;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ValorContableDAO;
import java.text.SimpleDateFormat;
/**
 *
 * @author Administrador
 */
public class ObtenerCorrelativoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
              JSONObject oResponse = new JSONObject();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            int org_id = requestData.getInt("org_id");
            BienesMueblesDAO bie_mue_dao = (BienesMueblesDAO)FactoryDao.buildDao("scp.BienesMueblesDAO");
            int num_corr = bie_mue_dao.obtener_correlativo(org_id);
          
            oResponse.put("num_corr",num_corr);
            
            
        }catch(Exception e){
             System.out.println("No se pudo Obtener el Correlativo del Bien\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Obtener el Correlativo del Bien", e.getMessage() );
        }

        return WebResponse.crearWebResponseExito("Se obtuvo el Numero Correlativo del  Bien Correctamente",oResponse); 
               
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
