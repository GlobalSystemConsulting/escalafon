package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@IdClass(PreguntaEvaluacionId.class)
@Entity
@Table(name="pregunta_evaluacion" ,schema="pedagogico")
public class PreguntaEvaluacion  implements java.io.Serializable {

    @Id 
    @Column(name="pre_eva_id", unique=true, nullable=false)
    private int preEvaId;
    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="eva_esc_id", nullable=false)
    private EvaluacionEscolar evaluacion;
    
    @Column(name="tit", length=64)
    private String titulo;
    
    @Column(name="pun")
    private int puntos;
    
    @Column(name="tie_est")
    private int tiempo;
    
    @Column(name="con_pre", length=256)
    private String pregunta;
    @Column(name="res_pre", length=32)
    private String respuesta;
    
    @Column(name="alt_pre", length=64)
    private String alternativa;
    
    @Column(name="ord_pre", length=8)
    private String orden;
    
    @Column(name="tip_pre")
    private char tipo;

    public PreguntaEvaluacion() {
    }

	
    public PreguntaEvaluacion(int preEvaId, EvaluacionEscolar evaluacion) {
        this.preEvaId = preEvaId;
        this.evaluacion = evaluacion;
    }
    public PreguntaEvaluacion(int preEvaId, EvaluacionEscolar evaluacion,String titulo, int tiempo,int puntos, String pregunta,String respuesta, String alternativa, String orden,char tipo) {
       this.preEvaId = preEvaId;
       this.evaluacion = evaluacion;
       this.titulo = titulo;
       this.tiempo = tiempo;
       this.puntos = puntos;
       this.pregunta = pregunta;
       this.respuesta = respuesta;
       this.orden = orden;
       this.alternativa = alternativa;
       this.orden = orden;
       this.tipo = tipo;
              
    }
   
     
    public int getPreEvaId() {
        return this.preEvaId;
    }
    public void setPreEvaId(int preEvaId) {
        this.preEvaId = preEvaId;
    }

    public EvaluacionEscolar getEvaluacion() {
        return this.evaluacion;
    }
    
    public void setEvaluacion(EvaluacionEscolar evaluacion) {
        this.evaluacion = evaluacion;
    }

    
    public String getTitulo() {
        return this.titulo;
    }
    
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
    public int getPuntos(){
        return this.puntos;        
    }
    public void setPuntos(int puntos){
        this.puntos = puntos;
    }
    
    public int getTiempo(){
        return this.tiempo;        
    }
    public void setTiempo(int tiempo){
        this.tiempo = tiempo;
    }
    
    public String getPregunta() {
        return this.pregunta;
    }    
    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }
    public String getRespuesta() {
        return this.respuesta;
    }    
    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getAlternativa() {
        return this.alternativa;
    }    
    public void setAlternativa(String alternativa) {
        this.alternativa = alternativa;
    }
    public String getOrden() {
        return this.orden;
    }    
    public void setOrden(String orden) {
        this.orden = orden;
    }
    
    public char getTipo(){
        return this.tipo;
    }
    public void setTipo(char tipo){
        this.tipo = tipo;
    }
}


