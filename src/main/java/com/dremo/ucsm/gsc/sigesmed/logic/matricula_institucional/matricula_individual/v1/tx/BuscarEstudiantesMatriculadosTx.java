package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.EstudianteMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BuscarEstudiantesMatriculadosTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        EstudianteMMIDaoHibernate dh = new EstudianteMMIDaoHibernate();
        List<EstudianteMMI> estudianteMmi;
        JSONObject requestData = (JSONObject) wr.getData();
        JSONArray estudiantes = new JSONArray();

        String codigoBusqueda = requestData.getString("codigoBusqueda");
        int tipoBusqueda = requestData.getInt("tipoBusqueda");
        try {
            switch (tipoBusqueda) {
                case 1: //por DNI
                    estudianteMmi = dh.findAll4Dni(codigoBusqueda);
                    break;
                case 2: //por Codigo de Estudiante
                    estudianteMmi = dh.findAll4CodEst(codigoBusqueda);
                    break;
                case 3: //por Nombres
                    estudianteMmi = dh.findAll4NomCom(codigoBusqueda);
                    break;
                case 4: //por Apellido Paterno
                    estudianteMmi = dh.findAll4ApePat(codigoBusqueda);
                    break;
                case 5: //por Apellido Materno
                    estudianteMmi = dh.findAll4ApeMat(codigoBusqueda);
                    break;
                default:
                    estudianteMmi = dh.findAll4Dni(codigoBusqueda);
                    break;
            }

            if (estudianteMmi == null) {
                return WebResponse.crearWebResponseError("No se encontraron estudiantes! ", "Usuario null");
            }

            if (estudianteMmi != null) {
                for (EstudianteMMI estudiante : estudianteMmi) {
                    JSONObject temp = new JSONObject();

                    if ((Long) estudiante.getPersona().getPerId() != null) {
                        temp.put("estId", estudiante.getPersona().getPerId());
                    } else {
                        temp.put("estId", -1);
                    }
                    if (estudiante.getPersona().getNom() != null) {
                        temp.put("estNom", estudiante.getPersona().getNom());
                    } else {
                        temp.put("estNom", "-");
                    }
                    if (estudiante.getPersona().getNom() != null) {
                        temp.put("estApePat", estudiante.getPersona().getApePat());
                    } else {
                        temp.put("estApePat", "-");
                    }
                    if (estudiante.getPersona().getNom() != null) {
                        temp.put("estApeMat", estudiante.getPersona().getApeMat());
                    } else {
                        temp.put("estApeMat", "-");
                    }
                    if (estudiante.getPersona().getNom() != null) {
                        temp.put("estDni", estudiante.getPersona().getDni());
                    } else {
                        temp.put("estDni", "desconocido");
                    }
                    if (estudiante.getPersona().getNom() != null) {
                        temp.put("estCodEst", estudiante.getCodEst());
                    } else {
                        temp.put("estCodEst", "desconocido");
                    }

                    if (estudiante.getEstMat() != null) {
                        temp.put("estMat", estudiante.getEstMat());
                        if (estudiante.getEstMat()) {
                            temp.put("estMatNom", "Activa");
                        } else {
                            temp.put("estMatNom", "Inactiva");
                        }
                    } else {
                        temp.put("estMat", false);
                        temp.put("estMatNom", "Inactiva");
                    }

                    if (estudiante.getUltGraCul() != null) {
                        temp.put("ultGraCul", estudiante.getUltGraCul());
                        temp.put("ultGraCulNom", selectLastGrade(estudiante.getUltGraCul()));
                    } else {
                        temp.put("ultGraCul", 0);
                        temp.put("ultGraCulNom", "Sin Antecedentes");
                    }

                    String lastSec = "";
                    if (estudiante.getUltSec() != null) {
                        lastSec = lastSec + estudiante.getUltSec().toString();
                        temp.put("ultSec", lastSec);
                    } else {
                        lastSec = "-";
                        temp.put("ultSec", lastSec);
                    }

                    if (estudiante.getOrgId() != null) {
                        temp.put("estOrgOri", estudiante.getOrgId());
                    }else {
                        temp.put("estOrgOri", -1);
                    }

                    estudiantes.put(temp);
                }
            } else {
                return WebResponse.crearWebResponseError("No se encontro estudiantes! ", "exepcion");
            }

        } catch (NumberFormatException | JSONException e) {
            return WebResponse.crearWebResponseError("No se encontro estudiantes! ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Se encontro los estudiantes", estudiantes);
    }

    private String selectLastGrade(String tipo) {
        String returnKey;
        try {
            switch (Integer.parseInt(tipo)) {
                case 0: {
                    returnKey = "Sin Antecedentes";
                    break;
                }
                case 1: {
                    returnKey = "Inicial 0 a 2 años";
                    break;
                }
                case 2: {
                    returnKey = "Inicial 3 a 5 años";
                    break;
                }
                case 3: {
                    returnKey = "1ro de Primaria";
                    break;
                }
                case 4: {
                    returnKey = "2do de Primaria";
                    break;
                }
                case 5: {
                    returnKey = "3ro de Primaria";
                    break;
                }
                case 6: {
                    returnKey = "4to de Primaria";
                    break;
                }
                case 7: {
                    returnKey = "5to de Primaria";
                    break;
                }
                case 8: {
                    returnKey = "6to de Primaria";
                    break;
                }
                case 9: {
                    returnKey = "1ro de Secundaria";
                    break;
                }
                case 10: {
                    returnKey = "2do se Secundaria";
                    break;
                }
                case 11: {
                    returnKey = "3ro de Secundaria";
                    break;
                }
                case 12: {
                    returnKey = "4to de Secundaria";
                    break;
                }
                case 13: {
                    returnKey = "5to de Secundaria";
                    break;
                }
                default: {
                    returnKey = "Sin Antecedentes";
                    break;
                }

            }
        } catch (Exception e) {
            returnKey = "Sin Antecedentes";
        }
        return returnKey;
    }

}
