package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "horarios_sedes_capacitacion",schema = "pedagogico")
public class HorarioSedeCapacitacion implements Serializable {
    @Id
    @Column(name = "hor_sed_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_horarios_sedes_capacitacion", sequenceName = "pedagogico.horarios_sedes_capacitacion_hor_sed_cap_id_seq")
    @GeneratedValue(generator = "secuencia_horarios_sedes_capacitacion")
    private int horSedCapId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sed_cap_id")
    private SedeCapacitacion sedeCapacitacion;

    @Column(name = "dia",nullable = false)
    private int dia;

    @Temporal(TemporalType.TIME)
    @Column(name = "tur_ini")
    private Date turIni;

    @Temporal(TemporalType.TIME)
    @Column(name = "tur_fin")
    private Date turFin;

    public HorarioSedeCapacitacion() {}

    public HorarioSedeCapacitacion(SedeCapacitacion sedeCapacitacion, int dia, Date turIni, Date turFin) {
        this.sedeCapacitacion = sedeCapacitacion;
        this.dia = dia;
        this.turIni = turIni;
        this.turFin = turFin;
    }
    
    public int getHorSedCapId() {
        return horSedCapId;
    }

    public void setHorSedCapId(int horSedCapId) {
        this.horSedCapId = horSedCapId;
    }

    public SedeCapacitacion getSedeCapacitacion() {
        return sedeCapacitacion;
    }

    public void setSedeCapacitacion(SedeCapacitacion sedeCapacitacion) {
        this.sedeCapacitacion = sedeCapacitacion;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public Date getTurIni() {
        return turIni;
    }

    public void setTurIni(Date turIni) {
        this.turIni = turIni;
    }

    public Date getTurFin() {
        return turFin;
    }

    public void setTurFin(Date turFin) {
        this.turFin = turFin;
    }
}
