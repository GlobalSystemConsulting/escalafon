/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.rdg;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name = "tipo_especializado",schema="institucional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoEspecializado.findAll", query = "SELECT t FROM TipoEspecializado t"),
    @NamedQuery(name = "TipoEspecializado.findByTesIde", query = "SELECT t FROM TipoEspecializado t WHERE t.tesIde = :tesIde"),
    @NamedQuery(name = "TipoEspecializado.findByTesAli", query = "SELECT t FROM TipoEspecializado t WHERE t.tesAli = :tesAli"),
    @NamedQuery(name = "TipoEspecializado.findByTesNom", query = "SELECT t FROM TipoEspecializado t WHERE t.tesNom = :tesNom"),
    @NamedQuery(name = "TipoEspecializado.findByFecMod", query = "SELECT t FROM TipoEspecializado t WHERE t.fecMod = :fecMod"),
    @NamedQuery(name = "TipoEspecializado.findByUsuMod", query = "SELECT t FROM TipoEspecializado t WHERE t.usuMod = :usuMod"),
    @NamedQuery(name = "TipoEspecializado.findByEstReg", query = "SELECT t FROM TipoEspecializado t WHERE t.estReg = :estReg"),
    @NamedQuery(name = "TipoEspecializado.findByTesDes", query = "SELECT t FROM TipoEspecializado t WHERE t.tesDes = :tesDes")})
public class TipoEspecializado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "tes_ide")
    @SequenceGenerator(name = "secuencia_tipesp", sequenceName="institucional.tipo_especializado_tes_ide_seq" )
    @GeneratedValue(generator="secuencia_tipesp")
    private Short tesIde;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "tes_ali")
    private String tesAli;
    @Size(max = 100)
    @Column(name = "tes_nom")
    private String tesNom;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Size(max = 1)
    @Column(name = "est_reg")
    private String estReg;
    @Size(max = 300)
    @Column(name = "tes_des")
    private String tesDes;
    
//    @OneToMany(mappedBy = "tifTesIde")
//    private List<TipoItemFile> tipoItemFileList;

    public TipoEspecializado() {
    }

    public TipoEspecializado(Short tesIde) {
        this.tesIde = tesIde;
    }

    public TipoEspecializado(Short tesIde, String tesAli) {
        this.tesIde = tesIde;
        this.tesAli = tesAli;
    }
    
    public TipoEspecializado(Short tesIde, String tesAli,String tesNom,String tesDes,Date fecha,Integer usuMod,String estReg){
        this.tesIde = tesIde;
        this.tesAli = tesAli;
        this.tesNom = tesNom;
        this.tesDes = tesDes;
        this.fecMod = fecha;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }
    
    public TipoEspecializado(String tesAli,String tesNom,String tesDes,Date fecMod,Integer usuMod, String estReg){
        this.tesAli = tesAli;
        this.tesNom = tesNom;
        this.tesDes = tesDes;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;
    }


    public Short getTesIde() {
        return tesIde;
    }

    public void setTesIde(Short tesIde) {
        this.tesIde = tesIde;
    }

    public String getTesAli() {
        return tesAli;
    }

    public void setTesAli(String tesAli) {
        this.tesAli = tesAli;
    }

    public String getTesNom() {
        return tesNom;
    }

    public void setTesNom(String tesNom) {
        this.tesNom = tesNom;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public String getTesDes() {
        return tesDes;
    }

    public void setTesDes(String tesDes) {
        this.tesDes = tesDes;
    }

//    @XmlTransient
//    public List<TipoItemFile> getTipoItemFileList() {
//        return tipoItemFileList;
//    }
//
//    public void setTipoItemFileList(List<TipoItemFile> tipoItemFileList) {
//        this.tipoItemFileList = tipoItemFileList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tesIde != null ? tesIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoEspecializado)) {
            return false;
        }
        TipoEspecializado other = (TipoEspecializado) object;
        if ((this.tesIde == null && other.tesIde != null) || (this.tesIde != null && !this.tesIde.equals(other.tesIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.TipoEspecializado[ tesIde=" + tesIde + " ]";
    }
    
}
