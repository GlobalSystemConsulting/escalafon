/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Ambientes;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AmbientesDAO;



/**
 *
 * @author Administrador
 */
public class ListarAmbientesTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
            JSONArray miArray = new JSONArray();
        try{
            List<Ambientes> ambientes = null;
            JSONObject requestData = (JSONObject)wr.getData();
            int org_id = requestData.getInt("org_id");
            
            AmbientesDAO amb_dao = (AmbientesDAO)FactoryDao.buildDao("scp.AmbientesDAO");
            ambientes = amb_dao.listarAmbientes(org_id);

            for(Ambientes amb : ambientes){
               JSONObject oResponse = new JSONObject();
               oResponse.put("amb_id", amb.getAmb_id());
               oResponse.put("amb_des", amb.getDes());
               oResponse.put("amb_ubi",amb.getUbi());
               oResponse.put("amb_est",String.valueOf(amb.getEst_amb()));
               miArray.put(oResponse);
            }
            
        }
        catch(Exception e){
            System.out.println("No se pudo Listar los Ambientes\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Ambientes", e.getMessage() );        
        }
        return WebResponse.crearWebResponseExito("Se Listo Los Ambientes Correctamente",miArray); 
        
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
