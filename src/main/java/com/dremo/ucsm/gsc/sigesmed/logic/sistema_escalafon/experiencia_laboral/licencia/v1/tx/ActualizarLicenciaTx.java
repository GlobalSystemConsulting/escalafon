/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.licencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LicenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Licencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarLicenciaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarLicenciaTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer licId = requestData.getInt("licId");
            String entEmi = requestData.getString("entEmi");
            int tipDocIdMain = requestData.getInt("tipDocIdMain");
            String numInfMain = requestData.getString("numInfMain");
            Boolean estGoc = requestData.getBoolean("estGoc");
            Date fecInfMain = null;
            if(requestData.getString("fecInfMain").length() > 0){
                fecInfMain = sdi.parse(requestData.getString("fecInfMain").substring(0, 10));
            }
            Date fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));            
            Date fecTer = sdi.parse(requestData.getString("fecTer").substring(0, 10));
            String mot = requestData.getString("mot");
            int estProId = requestData.getInt("estProId");
            
            int tipDocIdSec=requestData.getInt("tipDocIdSec");
            String numDocSec = requestData.getString("numDocSec");
            Date fecDocSec = null;
            if(requestData.getString("fecDocSec").length() > 0){
                fecDocSec = sdi.parse(requestData.getString("fecDocSec").substring(0, 10));
            }
            Date fecRecInfMain = null;
            if(requestData.getString("fecRecInfMain").length() > 0){
                fecRecInfMain = sdi.parse(requestData.getString("fecRecInfMain").substring(0, 10));
            }
            int tipLicId = requestData.getInt("tipLicId");
            
            int tipDocIdMemo=requestData.getInt("tipDocIdMemo");
            String numInfMemo=requestData.getString("numInfMemo");
            Date fecInfMemo=null;
            if(requestData.getString("fecInfMemo").length() > 0){
                fecInfMemo = sdi.parse(requestData.getString("fecInfMemo").substring(0, 10));
            }
            Date fecRecInMemo=null;
            if(requestData.getString("fecRecInMemo").length() > 0){
                fecRecInMemo = sdi.parse(requestData.getString("fecRecInMemo").substring(0, 10));
            }
            
            int anioPro = requestData.getInt("anioPro");
            int mesPro = requestData.getInt("mesPro");
            int diaPro = requestData.getInt("diaPro");
            
            return actualizarLicencia(licId, entEmi, tipDocIdMain, numInfMain, fecInfMain, estGoc, fecIni, fecTer, mot, estProId, numDocSec,
                    fecDocSec, fecRecInfMain, tipLicId,tipDocIdSec,tipDocIdMemo,numInfMemo,fecInfMemo,fecRecInMemo,anioPro,mesPro,diaPro);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar desplazamientoa",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarLicencia(Integer licId, String entEmi, int tipDocId,  
            String numDoc, Date fecDoc, Boolean estGoc, Date fecIni, Date fecTer, String mot, int estProId,
            String numInf, Date fecInf, Date fecRecInf, int tipLicId, int tipDocSecId, int tipDocMemId, String numMemo,Date fecMem,Date fecReiMem,int anioPro, int mesPro, int diaPro) {
        try{
            LicenciaDao licenciaDao = (LicenciaDao)FactoryDao.buildDao("se.LicenciaDao");        
            Licencia licencia = licenciaDao.buscarPorId(licId);

            licencia.setEntEmiRes(entEmi);
            licencia.setTipDocId(tipDocId);
            licencia.setNumDoc(numDoc);
            licencia.setFecDoc(fecDoc);
            licencia.setEstGoc(estGoc);
            licencia.setFecIni(fecIni);
            licencia.setFecTer(fecTer);
            licencia.setMotLic(mot);
            licencia.setEstProId(estProId);
            
            licencia.setFecInf(fecInf);
            licencia.setFecRecInf(fecRecInf);
            licencia.setNumInf(numInf);
            licencia.setTipLicId(tipLicId);
            
            licencia.setTipDocInfId(tipDocSecId);
            licencia.setTipDocMemId(tipDocMemId);
            licencia.setNumMem(numMemo);
            licencia.setFecDocMem(fecMem);
            licencia.setFecReiMem(fecReiMem);
            licencia.setAnioPro(anioPro);
            licencia.setMesPro(mesPro);
            licencia.setDiaPro(diaPro);
            
            licenciaDao.update(licencia);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
            if(licencia.getLicId() != null)
                oResponse.put("licId", licencia.getLicId());
            else
                oResponse.put("licId", -1);

            if(licencia.getEntEmiRes()!=null)
                oResponse.put("entEmi", licencia.getEntEmiRes());
            else
                oResponse.put("entEmi", "");

            if(licencia.getTipDocId()!=null)
                oResponse.put("tipDocIdMain", licencia.getTipDocId());
            else
                oResponse.put("tipDocIdMain", 0);

            if(licencia.getNumDoc()!=null)
                oResponse.put("numInfMain", licencia.getNumDoc());
            else
                oResponse.put("numInfMain", "");

            if(licencia.getFecDoc()!=null)
                oResponse.put("fecInfMain", sdo.format(licencia.getFecDoc()));
            else
                oResponse.put("fecInfMain", "");

            if(licencia.getFecRecInf()!=null)
                oResponse.put("fecRecInfMain", sdo.format(licencia.getFecRecInf()));
            else
                oResponse.put("fecRecInfMain", "");

            if(licencia.getTipLicId()!=null)
                oResponse.put("tipLicId", licencia.getTipLicId());
            else
                oResponse.put("tipLicId", 0);

            if(licencia.getEstGoc()!=null)
                oResponse.put("estGoc", licencia.getEstGoc());
            else
                oResponse.put("estGoc", false);

            if(licencia.getFecIni()!=null)
                oResponse.put("fecIni", sdo.format(licencia.getFecIni()));
            else
                oResponse.put("fecIni", "");

            if(licencia.getFecTer()!=null)
                oResponse.put("fecTer", sdo.format(licencia.getFecTer()));
            else
                oResponse.put("fecTer", "");
            //datos de informe
            if(licencia.getTipDocInfId()!=null)
                oResponse.put("tipDocIdSec", licencia.getTipDocInfId());
            else
                oResponse.put("tipDocIdSec", 0);

            if(licencia.getNumInf()!=null)
                oResponse.put("numDocSec", licencia.getNumInf());
            else
                oResponse.put("numDocSec", "");

            if(licencia.getFecInf()!=null)
                oResponse.put("fecDocSec", sdo.format(licencia.getFecInf()));
            else
                oResponse.put("fecDocSec", "");

            //memo
            if(licencia.getTipDocMemId()!=null)
                oResponse.put("tipDocIdMemo", licencia.getTipDocMemId());
            else
                oResponse.put("tipDocIdMemo", 0);

            if(licencia.getNumMem()!=null)
                oResponse.put("numInfMemo", licencia.getNumMem());
            else
                oResponse.put("numInfMemo", "");

            if(licencia.getFecDocMem()!=null)
                oResponse.put("fecInfMemo", sdo.format(licencia.getFecDocMem()));
            else
                oResponse.put("fecInfMemo", "");

            if(licencia.getFecReiMem()!=null)
                oResponse.put("fecRecInMemo", sdo.format(licencia.getFecReiMem()));
            else
                oResponse.put("fecRecInMemo", "");


            //otros
            if(licencia.getMotLic()!=null)
                oResponse.put("mot", licencia.getMotLic());
            else
                oResponse.put("mot", "");

             
            if(licencia.getAnioPro()!=null)
                oResponse.put("anioPro", licencia.getAnioPro());
            else
                oResponse.put("anioPro", "");

            if(licencia.getMesPro()!=null)
                oResponse.put("mesPro", licencia.getMesPro());
            else
                oResponse.put("mesPro", "");

            if(licencia.getDiaPro()!=null)
                oResponse.put("diaPro", licencia.getDiaPro());
            else
                oResponse.put("diaPro", ""); 
            
            oResponse.put("tip", licencia.getTip());


            if(licencia.getEstProId()!=null)
                oResponse.put("estProId", licencia.getEstProId());
            else
                oResponse.put("estProId", 0);

            return WebResponse.crearWebResponseExito("Licencia actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarLicencia",e);
            return WebResponse.crearWebResponseError("Error, la Licencia no fue actualizada");
        }
    } 
    
}
