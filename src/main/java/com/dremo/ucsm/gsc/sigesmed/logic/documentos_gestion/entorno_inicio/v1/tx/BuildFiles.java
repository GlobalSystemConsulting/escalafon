/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author christian
 */
public class BuildFiles {

    private String cadBase64;
    private String cabecera;
    private String esquemaFile;
    private String tipoArchivo;
    private String archivoFinal;
    private String nombreFile;
    private FileOutputStream fos = null;

    public BuildFiles(String cadena,String nombreFile) {
        this.nombreFile = nombreFile;
        this.cadBase64 = cadena;
        cabecera = cadena.split(",")[0];
        esquemaFile = cabecera.split(";")[0].split("/")[0].substring(5);
        tipoArchivo = cabecera.split("/")[1].split(";")[0];
        archivoFinal = cadena.split(",")[1];
    }

    public void buildArchFinal(String url) throws IOException {
        //la parte de cotenido del archivo se convierte a bytes y preparar el archivo
        byte archivoBytes[] = Base64.decodeBase64(archivoFinal);
        clasificar(url);
        //Escritura del archivo
        fos.write(archivoBytes);
        fos.close();
    }

    public void clasificar(String url) {
        //clasificacion de acuerdo al esquema y tipo de archivo
        try {
            if (esquemaFile.equals("image")) {
                if (tipoArchivo.equals("png")) {
                    fos = new FileOutputStream(url+"/"+nombreFile+".png");
                } else if (tipoArchivo.equals("jpeg")) {
                    fos = new FileOutputStream(url+"/"+nombreFile+".jpg");
                }
            } else if (esquemaFile.equals("application")) {
                if (tipoArchivo.equals("pdf")) {
                    fos = new FileOutputStream(url+"/"+nombreFile+".pdf");
                }
            }
        } catch (Exception e) {

        }
    }
    public String getTipoArchivo(){
        return tipoArchivo;
    }
    public void setCadBase64(String cadena) {
        this.cadBase64 = cadena;
    }

}
