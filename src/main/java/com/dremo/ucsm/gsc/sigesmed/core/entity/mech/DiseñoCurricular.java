package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="diseño_curricular" )
public class DiseñoCurricular  implements java.io.Serializable {

    @Id
    @Column(name="dis_cur_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_diseño_curricular", sequenceName="diseño_curricular_dis_cur_id_seq" )
    @GeneratedValue(generator="secuencia_diseño_curricular")
    private int disCurId;
    @Column(name="nom",length=64)
    private String nom;
    @Column(name="des", length=256)
    private String des;
    @Column(name="res_dir", nullable=false, length=16)
    private String resDir;
    @Column(name="tip")
    private char tip;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_cre", nullable=false, length=29)
    private Date fecCre;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", nullable=false, length=29)
    private Date fecMod;    
    @Column(name="usu_mod")
    private int usuMod;
    @Column(name="est_reg")
    private char estReg;
    
    @Column(name="org_id")
    private int orgId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id",updatable = false,insertable = false)
    private Organizacion organizacion;
    
    @OneToMany(fetch=FetchType.EAGER, mappedBy="diseñoCurricular")
    private List<ModalidadEducacion> modalidades;
    
    @OneToMany(fetch=FetchType.EAGER, mappedBy="diseñoCurricular")
    @OrderBy("cicEduId")
    private List<CicloEducativo> ciclos;
    
    @OneToMany(fetch=FetchType.EAGER, mappedBy="diseñoCurricular")
    @OrderBy("nivId")
    private List<Nivel> niveles;
    
    @OneToMany(fetch=FetchType.EAGER, mappedBy="diseñoCurricular")
    @OrderBy("cicEduId,graId")
    private List<Grado> grados;
    
    @OneToMany(fetch=FetchType.EAGER, mappedBy="diseñoCurricular")
    @OrderBy("areCurId")
    private List<AreaCurricular> areas;
    
    @OneToMany(fetch=FetchType.EAGER, mappedBy="diseñoCurricular")
    @OrderBy("nivId,jorEscId")
    private List<JornadaEscolar> jornadas;

    public DiseñoCurricular() {
    }
    public DiseñoCurricular(int disCurId) {
        this.disCurId = disCurId;
    }
    public DiseñoCurricular(int disCurId, String nom, String des, String resDir, char tip, int orgId, Date fecCre, Date fecMod, int usuMod, char estReg) {
       this.disCurId = disCurId;
       this.nom = nom;
       this.des = des;
       this.resDir = resDir;
       this.tip = tip;
       this.orgId = orgId;
       this.fecCre = fecCre;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getDisCurId() {
        return this.disCurId;
    }    
    public void setDisCurId(int disCurId) {
        this.disCurId = disCurId;
    }
    
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    public String getResDir() {
        return this.resDir;
    }
    public void setResDir(String resDir) {
        this.resDir = resDir;
    }

    public Date getFecCre() {
        return this.fecCre;
    }
    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }
    
    public char getTip() {
        return this.tip;
    }
    public void setTip(char tip) {
        this.tip = tip;
    }
    
    public Date getFecMod() {
        return this.fecMod;
    }
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public int getUsuMod() {
        return this.usuMod;
    }
    public void setUsuMod(int usuMod) {
        this.usuMod = usuMod;
    }
    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getOrgId() {
        return this.orgId;
    }    
    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }
    
    public Organizacion getOrganizacion() {
        return this.organizacion;
    }
    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }
    
    public List<CicloEducativo> getCiclos() {
        return this.ciclos;
    }
    public void setCiclos(List<CicloEducativo> ciclos) {
        this.ciclos = ciclos;
    }
    
    public List<Nivel> getNiveles() {
        return this.niveles;
    }
    public void setNiveles(List<Nivel> niveles) {
        this.niveles = niveles;
    }
    
    public List<ModalidadEducacion> getModalidades() {
        return this.modalidades;
    }
    public void setModalidades(List<ModalidadEducacion> modalidades) {
        this.modalidades = modalidades;
    }
    
    public List<Grado> getGrados() {
        return this.grados;
    }
    public void setGrados(List<Grado> grados) {
        this.grados = grados;
    }
    
    public List<AreaCurricular> getAreas() {
        return this.areas;
    }
    public void setAreas(List<AreaCurricular> areas) {
        this.areas = areas;
    }
    
    public List<JornadaEscolar> getJornadas() {
        return this.jornadas;
    }
    public void setJornadas(List<JornadaEscolar> jornadas) {
        this.jornadas = jornadas;
    }
}


