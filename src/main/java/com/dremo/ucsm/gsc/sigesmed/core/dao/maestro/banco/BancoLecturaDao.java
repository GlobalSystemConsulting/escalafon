package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.banco;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.BancoLectura;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.BancoLecturaDocente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;

/**
 * Created by Administrador on 26/12/2016.
 */
public interface BancoLecturaDao extends GenericDao<BancoLectura> {
    BancoLecturaDocente buscarBancoLectura(int idDoc,int idOrg,int idGrad,int idAre,Character idSec,int year);
    Organizacion buscarOrganizacion(int idOrg);
    Usuario buscarUsuario(int idDoc);
    Grado buscarGrado(int idGrado);
    Seccion buscarSeccion(Character idSec);
    AreaCurricular buscarArea(int idArea);
}
