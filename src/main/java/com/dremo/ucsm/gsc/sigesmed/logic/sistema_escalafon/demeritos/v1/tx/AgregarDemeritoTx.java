/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.demeritos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DemeritoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Demerito;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarDemeritoTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(AgregarDemeritoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Demerito demerito = null;
        
        Integer perId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            perId = requestData.getInt("perId");
            String entEmi = requestData.getString("entEmi");
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = requestData.getString("fecDoc").equals("")?null:sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            Boolean sep = requestData.getBoolean("sep");
            Date fecIni = requestData.getString("fecIni").equals("")?null:sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecFin = requestData.getString("fecFin").equals("")?null:sdi.parse(requestData.getString("fecFin").substring(0, 10));
            String mot = requestData.getString("mot");
            Integer tipdocId = requestData.getInt("tipDoc");
            
            demerito = new Demerito(new Persona(perId), entEmi, numDoc, fecDoc, tipdocId, sep, fecIni, fecFin, entEmi, wr.getIdUsuario(), new Date(), 'A');
            demerito.setMot(mot);

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nuevo sobre demerito",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        DemeritoDao demeritoDao = (DemeritoDao) FactoryDao.buildDao("se.DemeritoDao");
        try {
            demeritoDao.insert(demerito);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego demerito", "Detalle: "+perId+" ficha ", wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nuevo demerito",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("demId", demerito.getDemId());
        oResponse.put("entEmi", demerito.getEntEmi() == null ? "" : demerito.getEntEmi());
        oResponse.put("numDoc", demerito.getNumDoc() == null ? "" : demerito.getNumDoc());
        oResponse.put("fecDoc", demerito.getFecDoc() == null ? "" : sdi.format(demerito.getFecDoc()));
        oResponse.put("tipDocId", demerito.getTipDocId() == null ? 0 : demerito.getTipDocId());
        oResponse.put("sep", demerito.getSep() == null ? false : demerito.getSep());
        oResponse.put("sepDes", "");
        oResponse.put("fecIni", demerito.getFecIni() == null ? "" : sdi.format(demerito.getFecIni()));
        oResponse.put("fecFin", demerito.getFecFin() == null ? "" : sdi.format(demerito.getFecFin()));
        oResponse.put("mot", demerito.getMot() == null ? "" : demerito.getMot());
                
        return WebResponse.crearWebResponseExito("El registro del demerito se realizo correctamente", oResponse);
        //Fin
    }
    
}
