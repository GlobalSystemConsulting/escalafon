/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class PlantillaGrupoDaoHibernate extends GenericDaoHibernate<PlantillaGrupo> implements PlantillaGrupoDao{
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    @Override
    public PlantillaGrupo obtenerPlantillaGrupo(int gruId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        PlantillaGrupo grupo= null;
        Transaction t = session.beginTransaction();
        try{                                        
//            plantilla = (PlantillaFichaInstitucional)session.get(PlantillaFichaInstitucional.class, plaId);
            String hql = "SELECT g FROM PlantillaGrupo g "
                    + "join fetch g.indicadores i "                    
                    + "WHERE g.pgrId = " + gruId;
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            grupo = (PlantillaGrupo)query.uniqueResult();            
            t.commit();
                      
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo obtener el grupo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el grupo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return grupo;
    }
    
    public List<PlantillaIndicadores> obtenerIndicadores(int gruId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<PlantillaIndicadores> indicadores= new ArrayList<PlantillaIndicadores>();
        Transaction t = session.beginTransaction();
        try{                                        
//            plantilla = (PlantillaFichaInstitucional)session.get(PlantillaFichaInstitucional.class, plaId);
            String hql = "SELECT i FROM PlantillaIndicadores i "                    
                    + "WHERE i.grupo = " + gruId;
            Query query = session.createQuery(hql);            
            indicadores = query.list();            
            t.commit();
                      
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo obtener el grupo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el grupo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return indicadores;
    }    
}
