/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.ExpedienteDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.Expediente;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarExpedienteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int organizacionID = 0;
        Date desde = null;
        Date hasta = null;
        boolean expedientesFinalizados = false;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            organizacionID = requestData.getInt("organizacionID");
            
            expedientesFinalizados = requestData.optBoolean("finalizados");
            
            if(!requestData.getString("desde").contentEquals(""))
                desde = new SimpleDateFormat("dd/M/yyyy").parse( requestData.getString("desde"));
            if(!requestData.getString("hasta").contentEquals(""))
                hasta = new SimpleDateFormat("dd/M/yyyy HH:mm:ss").parse( requestData.getString("hasta") + " 23:59:59" );
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Expedientes ", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<Expediente> expedientes = null;
        ExpedienteDao expedienteDao = (ExpedienteDao)FactoryDao.buildDao("std.ExpedienteDao");
        try{
            if(expedientesFinalizados)
                expedientes =expedienteDao.buscarFinalizadosPorOrganizacionYFecha(organizacionID,desde,hasta);
            else
                expedientes =expedienteDao.buscarPorOrganizacionYFecha(organizacionID,desde,hasta);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Expedientes", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Expediente expediente:expedientes ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("expedienteID",expediente.getExpId());
            oResponse.put("codigo",expediente.getCodigo());
            oResponse.put("asunto",expediente.getAsunto());            
            oResponse.put("modo",expediente.getTipo());
            oResponse.put("folios",expediente.getFolios());
            oResponse.put("fecInicio",expediente.getFecIni());
            oResponse.put("fecFin",expediente.getFecFin());
            oResponse.put("fecEntrega",expediente.getFecEnt());
            
            oResponse.put("tipoTramiteID",expediente.getTipoTramite().getTipTraId());
            oResponse.put("nombreTramite",expediente.getTipoTramite().getNom());
            if(expediente.getPersona()==null)
            {
                oResponse.put("DNI",expediente.getEmpresa().getRuc());
                oResponse.put("persona",expediente.getEmpresa().getRazonSocial());
            }
            else
            {
                oResponse.put("DNI",expediente.getPersona().getDni());
                oResponse.put("persona",expediente.getPersona().getNombrePersona());
            }
            
            
            oResponse.put("prioridadID",expediente.getPrioridad().getPriExpId());
            oResponse.put("nombrePrioridad",expediente.getPrioridad().getNom());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

