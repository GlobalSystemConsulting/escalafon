/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.cese.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CeseDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cese;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class EliminarCeseTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EliminarCeseTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        Integer cesId = 0;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            cesId = requestData.getInt("cesId");          
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarDesplazamiento",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }

        CeseDao ceseDao = (CeseDao)FactoryDao.buildDao("se.CeseDao");
        try{
            ceseDao.delete(new Cese(cesId));
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarBajaAuditoria("Se elimino cese", "CESE_ID: "+cesId , wr.getIdUsuario() , "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        }catch(Exception e){
            System.out.println("No se pudo eliminar el cese\n" + e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el cese", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El cese se elimino correctamente");
    }
    
}
