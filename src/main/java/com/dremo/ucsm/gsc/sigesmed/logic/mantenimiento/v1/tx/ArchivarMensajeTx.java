/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mantenimiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.MensajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Mensaje;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ArchivarMensajeTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {        
        int menID=0;
        boolean flag=false;//true=propio, false=destinatario
        try{
            JSONObject requestData=(JSONObject)wr.getData();
            menID=requestData.getInt("menID");
            flag=requestData.getBoolean("flag");
        }catch(Exception e){
             System.out.println("No se pudo obtener el mensaje: " +e.getMessage());
            return WebResponse.crearWebResponseError("No se pudo listar los mensajes enviados",e.getMessage());
        }
        
   
        
        
        
        MensajeDao menDao=(MensajeDao)FactoryDao.buildDao("mnt.MensajeDao");
        if(flag)
            menDao.archivarMensajeAutor(menID);
        else
            menDao.archivarMensajeDestinatario(menID);
        
        
        return WebResponse.crearWebResponseExito("Se archivo con éxito el mensaje");
    }
    
}

