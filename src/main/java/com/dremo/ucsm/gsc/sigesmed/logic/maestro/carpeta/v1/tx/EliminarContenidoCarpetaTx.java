package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.ContenidoSeccionCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ContenidoSeccionCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 29/12/2016.
 */
public class EliminarContenidoCarpetaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EliminarContenidoCarpetaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idContendio = data.getInt("id");

        return eliminarContenidoCarpeta(idContendio);
    }

    private WebResponse eliminarContenidoCarpeta(int idContendio) {
        try{
            ContenidoSeccionCarpetaDao contDao = (ContenidoSeccionCarpetaDao) FactoryDao.buildDao("maestro.carpeta.ContenidoSeccionCarpetaDao");
            ContenidoSeccionCarpeta contenido = contDao.buscarContenidoPorId(idContendio);
            contDao.delete(contenido);
            return WebResponse.crearWebResponseExito("Se elimino corectamente");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarSeccionCarpeta",e);
            return WebResponse.crearWebResponseError("No se puede eliminar la seccion");
        }
    }
}
