/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.di;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Administrador
 */
@Embeddable
public class EstudianteId implements Serializable {
    @Basic(optional = false)
    @Column(name = "est_id")
    private long estId;
    @Basic(optional = false)
    @Column(name = "per_id")
    private long perId;

    public EstudianteId() {
    }

    public EstudianteId(long estId, long perId) {
        this.estId = estId;
        this.perId = perId;
    }

    public long getEstId() {
        return estId;
    }

    public void setEstId(long estId) {
        this.estId = estId;
    }

    public long getPerId() {
        return perId;
    }

    public void setPerId(long perId) {
        this.perId = perId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) estId;
        hash += (int) perId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstudianteId)) {
            return false;
        }
        EstudianteId other = (EstudianteId) object;
        if (this.estId != other.estId) {
            return false;
        }
        if (this.perId != other.perId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.EstudianteId[ estId=" + estId + ", perId=" + perId + " ]";
    }
    
}
