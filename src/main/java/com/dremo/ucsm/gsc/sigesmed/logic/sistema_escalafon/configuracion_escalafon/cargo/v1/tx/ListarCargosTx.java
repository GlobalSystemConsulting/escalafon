/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.cargo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ListarCargosTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarCargosTx.class.getName());
    
    @Override
        public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<Cargo> cargos = null;
        CargoDao cargoDao = (CargoDao)FactoryDao.buildDao("se.CargoDao");
        
        try{
            cargos = cargoDao.listarxCargo();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar cargos",e);
            System.out.println("No se pudo listar las cargos.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listarlas cargos.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Cargo c: cargos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("carId", c.getCarId());
            oResponse.put("codCar",c.getCodCar());
            oResponse.put("nomCar",c.getNomCar());
            oResponse.put("fecMod",c.getFecMod());
            oResponse.put("UsuMod",c.getUsuMod());
            oResponse.put("estReg",c.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las cargos fueron listadas exitosamente", miArray);
    }
    
}
