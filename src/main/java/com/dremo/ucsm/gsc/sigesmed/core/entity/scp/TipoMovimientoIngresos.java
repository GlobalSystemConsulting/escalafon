/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;

/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="tipo_movimiento_ingresos", schema="administrativo")
public class TipoMovimientoIngresos {
    
    @Id
    @Column(name="tip_mov_ing_id", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.tipo_movimiento_ingresos_tip_mov_ing_id_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int tip_mov_ing_id;
    
    @Column(name="des")
    private String des;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="usu_mod")
    private int usu_mod; 
    
    @Column(name="est_reg")
    private char est_reg;
    
    @Column(name="des_tip_doc")
    private String des_tip_doc;
    
   

    public void setTip_mov_ing_id(int tip_mov_ing_id) {
        this.tip_mov_ing_id = tip_mov_ing_id;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public void setDes_tip_doc(String des_tip_doc) {
        this.des_tip_doc = des_tip_doc;
    }

   

    public int getTip_mov_ing_id() {
        return tip_mov_ing_id;
    }

    public String getDes() {
        return des;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public String getDes_tip_doc() {
        return des_tip_doc;
    }

    
    
}
