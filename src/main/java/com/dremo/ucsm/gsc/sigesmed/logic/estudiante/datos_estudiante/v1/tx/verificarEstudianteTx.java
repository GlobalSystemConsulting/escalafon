/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.estudiante.datos_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class verificarEstudianteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Long usuarioId;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            usuarioId = requestData.getLong("usuarioID");  
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }

        Matricula hijo = null;
        EstudianteDao estudianteDao = (EstudianteDao)FactoryDao.buildDao("mpf.EstudianteDao");
        try{
            hijo=estudianteDao.getEstudiante(usuarioId);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Estudiantes del Pariente ", e.getMessage() );
        }
//
        JSONArray miArray = new JSONArray();
        
        if(hijo==null)
        {
            return WebResponse.crearWebResponseError("No existe el rol o matricula " );
       
        }
            JSONObject oResponse = new JSONObject();
            oResponse.put("personaID",hijo.getEstudiante().getPerId());
            oResponse.put("matriculaID",hijo.getMatId());
            oResponse.put("gradoID",hijo.getGradoId());
            oResponse.put("planID",hijo.getPlanNivel().getPlaEstId());
            oResponse.put("dni",hijo.getEstudiante().getPersona().getDni());
            oResponse.put("nombre",hijo.getEstudiante().getPersona().getNombrePersonaAP());
            oResponse.put("orgId",hijo.getOrganizacionByOrgDesId().getOrgId());
            oResponse.put("nombreOrganizacion",hijo.getOrganizacionByOrgDesId().getNom());

            miArray.put(oResponse);
        
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

