package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 26/10/2016.
 */
@Entity
@Table(name = "unidad_aprendizaje", schema = "pedagogico")
public class UnidadAprendizaje extends UnidadDidactica implements java.io.Serializable {
    /*@Id
    @GeneratedValue(generator = "unidadAprendizajeKeyGenerator")
    @org.hibernate.annotations.GenericGenerator(
            name = "unidadAprendizajeKeyGenerator",
            strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(
                    name = "property",value = "unidadDidactica"
            )
    )
    private int uni_did_id;*/

    /*@OneToOne(fetch = FetchType.LAZY,optional = false)
    @PrimaryKeyJoinColumn
    private UnidadDidactica unidadDidactica;*/

    public UnidadAprendizaje() {
    }

    public UnidadAprendizaje(String tit, Character tip, String sitSig, Date fecIni, Date fecFin) {
        super(tit, tip, sitSig, fecIni, fecFin);
    }

    public UnidadAprendizaje(String tit, Character tip, String sitSig, Date fecIni, Date fecFin, Integer num, String prod) {
        super(tit, tip, sitSig, fecIni, fecFin, num, prod);
    }
    /*public UnidadAprendizaje(UnidadDidactica unidadDidactica) {
        this.unidadDidactica = unidadDidactica;
        this.uni_did_id = unidadDidactica.getUniDidId();
    }

    public UnidadDidactica getUnidadDidactica() {
        return unidadDidactica;
    }

    public void setUnidadDidactica(UnidadDidactica unidadDidactica) {
        this.unidadDidactica = unidadDidactica;
    }*/
}
