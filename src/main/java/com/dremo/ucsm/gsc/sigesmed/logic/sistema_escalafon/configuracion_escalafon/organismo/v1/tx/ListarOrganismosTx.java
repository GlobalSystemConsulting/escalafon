/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.configuracion_escalafon.organismo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganismoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organismo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ListarOrganismosTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarOrganismosTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
                
        List<Organismo> organismos = null;
        OrganismoDao organismoDao = (OrganismoDao)FactoryDao.buildDao("se.OrganismoDao");
        
        try{
            organismos = organismoDao.listarxOrganismo();
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar organismos",e);
            System.out.println("No se pudo listar los organismos.Error: "+e);
            return WebResponse.crearWebResponseError("No se pudo listar los organismos.Error: ", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Organismo c: organismos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("orgaId",c.getOrgaId());
            oResponse.put("codOrga",c.getCodOrga());
            oResponse.put("nomOrga",c.getNomOrga());
            oResponse.put("fecMod",c.getFecMod());
            oResponse.put("UsuMod",c.getUsuMod());
            oResponse.put("estReg",c.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los organismos fueron listadas exitosamente", miArray);
    }
}
