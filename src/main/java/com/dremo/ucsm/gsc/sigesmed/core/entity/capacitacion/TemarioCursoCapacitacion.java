package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "temario_curso_capacitacion", schema = "pedagogico")
public class TemarioCursoCapacitacion implements Serializable {

    @Id
    @Column(name = "tem_cur_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_temario_curso_capacitacion", sequenceName = "pedagogico.temario_curso_capacitacion_tem_cur_cap_id_seq")
    @GeneratedValue(generator = "secuencia_temario_curso_capacitacion")
    private int temCurCapId;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sed_cap_id", referencedColumnName = "sed_cap_id", insertable = false, updatable = false)
    private SedeCapacitacion sedCap;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_ini", nullable = false)
    private Date fecIni;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_fin")
    private Date fecFin;

    @Column(name = "tem")
    private String tem;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    @OneToMany(mappedBy = "tema", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<ComentarioTemaCapacitacion> comentarios = new HashSet<>(0);
    
    @OneToMany(mappedBy = "tema", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<DesarrolloTemaCapacitacion> desarrollos = new HashSet<>(0);

    public TemarioCursoCapacitacion() {
    }

    public TemarioCursoCapacitacion(SedeCapacitacion sedCap, Date fecIni, Date fecFin, String tem, Integer usuMod) {
        this.sedCap = sedCap;
        this.fecIni = fecIni;
        this.fecFin = fecFin;
        this.tem = tem;
        this.usuMod = usuMod;
        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getTemCurCapId() {
        return temCurCapId;
    }

    public void setTemCurCapId(int temCurCapId) {
        this.temCurCapId = temCurCapId;
    }

    public SedeCapacitacion getSedCap() {
        return sedCap;
    }

    public void setSedCap(SedeCapacitacion sedCap) {
        this.sedCap = sedCap;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public String getTem() {
        return tem;
    }

    public void setTem(String tem) {
        this.tem = tem;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Set<ComentarioTemaCapacitacion> getComentarios() {
        return comentarios;
    }

    public void setComentarios(Set<ComentarioTemaCapacitacion> comentarios) {
        this.comentarios = comentarios;
    }

    public Set<DesarrolloTemaCapacitacion> getDesarrollos() {
        return desarrollos;
    }

    public void setDesarrollos(Set<DesarrolloTemaCapacitacion> desarrollos) {
        this.desarrollos = desarrollos;
    }
}
