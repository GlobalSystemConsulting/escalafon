package com.dremo.ucsm.gsc.sigesmed.logic.maestro.banco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.banco.LecturaBancoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.Lectura;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 26/12/2016.
 */
public class ListarLecturasDocenteTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarLecturasDocenteTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        ///datos del banco
        int idDoc = data.getInt("doc");
        int idOrg = data.getInt("org");
        int idGrad = data.getInt("gra");
        int idAre = data.getInt("are");
        Character idSec = data.getString("sec").charAt(0);
        int year = Calendar.getInstance().get(Calendar.YEAR);

        return listarLecturas(idDoc, idOrg, idGrad, idAre, idSec, year);
    }

    private WebResponse listarLecturas(int idDoc, int idOrg, int idGrad, int idAre, Character idSec, int year) {
        try{
            LecturaBancoDao lecturaDao = (LecturaBancoDao) FactoryDao.buildDao("maestro.banco.LecturaBancoDao");
            List<Lectura> lecturas = lecturaDao.listarLecturasDocente(idDoc, idGrad, idSec, idAre, idOrg, year);
            JSONArray jsonLecturas = new JSONArray();
            for(Lectura lectura : lecturas){
                JSONObject jsonLectura = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"lecId","nom","aut","des","ubi","tip","fecCre","fecPub"},
                        new String[]{"id","noml","aut","des","nom","tip","cre","fpub"},
                        lectura
                ));
                jsonLectura.put("url", Sigesmed.UBI_ARCHIVOS + File.separator +"banco_lectura_docente" + File.separator);
                jsonLecturas.put(jsonLectura);
            }
            return WebResponse.crearWebResponseExito("Exito al listar las lecturas", jsonLecturas);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarLecturas",e);
            return WebResponse.crearWebResponseError("Error al listar las lecturas");
        }
    }
}
