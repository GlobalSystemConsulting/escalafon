package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.IndicadorFichaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.IndicadoresEvaluarPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ResumenEvaluacionPersonal;
import org.apache.commons.math3.util.Pair;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 31/08/2016.
 */
public class MepReportDaoHibernate extends GenericDaoHibernate<IndicadoresEvaluarPersonal> {
    public final static Logger logger = Logger.getLogger(MepReportDaoHibernate.class.getName());
    public  Pair<Integer,Map<String,List<IndicadorFichaEvaluacion>>> getContenidoResumenEvaluacion(int idResumen,char tipo)
        throws HibernateException
    {
        Session session  = HibernateUtil.getSessionFactory().openSession();
        String sql = "SELECT\n" +
                "  c.fic_eva_per_id fic,c.nom cnom,i.nom inom,ip.pun\n" +
                "from\n" +
                "  pedagogico.indicador_ficha_evaluacion i\n" +
                "  INNER JOIN pedagogico.indicadores_evaluar_personal ip ON ip.ind_fic_eva_id = i.ind_fic_eva_id\n" +
                "  INNER JOIN pedagogico.contenido_ficha_evaluacion c ON c.con_fic_eva_id = i.con_fic_eva_id\n" +
                "  INNER JOIN pedagogico.resumen_evaluacion_personal r ON r.res_eva_per_id = ip.det_eva_per_id\n" +
                "WHERE\n" +
                "  r.res_eva_per_id = ? AND c.fic_eva_per_id = (\n" +
                "    SELECT DISTINCT f.fic_eva_per_id from pedagogico.resumen_evaluacion_personal r\n" +
                "      INNER JOIN trabajador t ON r.tra_id = t.tra_id\n" +
                "      INNER JOIN trabajador_cargo tc ON t.tra_car = tc.crg_tra_ide\n" +
                "      INNER JOIN pedagogico.ficha_evaluacion_personal f ON f.crg_tra_ide = tc.crg_tra_ide \n" +
                "    WHERE r.res_eva_per_id = ? AND f.est_reg = 'A') AND c.tip = ? \n" +
                "ORDER BY c.tip DESC , c.nom;";
        SQLQuery query = session.createSQLQuery(sql);
        query.setParameter(0,idResumen);
        query.setParameter(1,idResumen);
        query.setParameter(2,tipo);
        List<Object[]> result = query.list();

        Map<String,List<IndicadorFichaEvaluacion>> mresult = new TreeMap<>();
        int idFicha = -1;
        for(Object[] res : result){
            idFicha = (int)res[0];
            if(!mresult.containsKey(res[1])){
                List<IndicadorFichaEvaluacion> lindicadores = new ArrayList<>();
                lindicadores.add(new IndicadorFichaEvaluacion((String)res[2],(Short)res[3]));
                mresult.put((String)res[1],lindicadores);
            }
            else{
                List<IndicadorFichaEvaluacion> lindicadores = mresult.get(res[1]);
                lindicadores.add(new IndicadorFichaEvaluacion((String)res[2],(Short)res[3]));
            }
        }
        Pair<Integer,Map<String,List<IndicadorFichaEvaluacion>>> pair = new Pair<Integer, Map<String, List<IndicadorFichaEvaluacion>>>(idFicha,mresult) ;
        session.close();
        return pair;
    }
}
