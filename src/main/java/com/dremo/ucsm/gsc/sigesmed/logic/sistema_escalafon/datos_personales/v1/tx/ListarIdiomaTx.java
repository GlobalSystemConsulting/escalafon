/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.IdiomaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Idioma;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author 
 */
public class ListarIdiomaTx implements ITransaction {
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        List<Idioma> idiomas = null;
        IdiomaDao idiomasDao = (IdiomaDao)FactoryDao.buildDao("se.IdiomaDao");
        
        try{
            idiomas = idiomasDao.listar(Idioma.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las Idiomas\n "+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Idiomas ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Idioma id:idiomas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("idiId",id.getIdiId());
            oResponse.put("idiNom",id.getIdiNom());                       
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray); 
    }
}
