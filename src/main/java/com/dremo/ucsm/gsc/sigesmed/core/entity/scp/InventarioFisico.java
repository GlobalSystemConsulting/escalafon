/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;

/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="inventario_fisico", schema="administrativo")
public class InventarioFisico implements java.io.Serializable {
    
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="mov_ing_id", insertable = false , updatable = false)
    private MovimientoIngresos mov_ingresos;
    
    @Id
    @Column(name="inv_fis_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.inventario_fisico_inv_fis_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int inv_ini_id;
    
    @Id
    @Column(name="mov_ing_id")
    private int mov_ing_id;

    
    @Column(name="con_pat_id")
    private int con_pat_id; 
    
    
    @Column(name="fla_cie")
    private char fla_cie;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="org_id")
    private int org_id;
    
    @Column(name="est_reg")
    private char est_reg;
    
    @OneToMany(mappedBy="inv_inicial" , cascade= CascadeType.ALL)
    private List<InventarioFisicoDetalle> inv_ini_det;
    
    public void setInv_ini_det(List<InventarioFisicoDetalle> inv_ini_det) {
        this.inv_ini_det = inv_ini_det;
    }

    public List<InventarioFisicoDetalle> getInv_ini_det() {
        return this.inv_ini_det;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public int getOrg_id() {
        return org_id;
    }

    
    
    
    public void setMov_ingresos(MovimientoIngresos mov_ingresos) {
        this.mov_ingresos = mov_ingresos;
    }

    public void setInv_ini_id(int inv_ini_id) {
        this.inv_ini_id = inv_ini_id;
    }

    public void setMov_ing_id(int mov_ing_id) {
        this.mov_ing_id = mov_ing_id;
    }

    public void setCon_pat_id(int con_pat_id) {
        this.con_pat_id = con_pat_id;
    }

    public void setFla_cie(char fla_cie) {
        this.fla_cie = fla_cie;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public MovimientoIngresos getMov_ingresos() {
        return mov_ingresos;
    }

    public int getInv_ini_id() {
        return inv_ini_id;
    }

    public int getMov_ing_id() {
        return mov_ing_id;
    }

    public int getCon_pat_id() {
        return con_pat_id;
    }

    public char getFla_cie() {
        return fla_cie;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }

    public InventarioFisico() {
    }

    public InventarioFisico(int inv_ini_id, int mov_ing_id, int con_pat_id, char fla_cie, Date fec_mod, int usu_mod, char est_reg) {
        this.inv_ini_id = inv_ini_id;
        this.mov_ing_id = mov_ing_id;
        this.con_pat_id = con_pat_id;
        this.fla_cie = fla_cie;
        this.fec_mod = fec_mod;
        this.usu_mod = usu_mod;
        this.est_reg = est_reg;
    }
    
   
    
    
}

