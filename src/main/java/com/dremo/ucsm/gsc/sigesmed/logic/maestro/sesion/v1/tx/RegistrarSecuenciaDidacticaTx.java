package com.dremo.ucsm.gsc.sigesmed.logic.maestro.sesion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.SesionAprendizajeDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.*;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 05/12/2016.
 */
public class RegistrarSecuenciaDidacticaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(RegistrarSecuenciaDidacticaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        String nomSequencia = data.getString("nom");
        JSONArray conts = data.optJSONArray("cont");
        int idSesion = data.getInt("ses");
        return registrarSecuencia(idSesion,nomSequencia,conts);
    }

    private WebResponse registrarSecuencia(int idSesion, String nomSequencia, JSONArray conts) {
        try{
            SesionAprendizajeDao sesionDao = (SesionAprendizajeDao) FactoryDao.buildDao("maestro.plan.SesionAprendizajeDao");
            SesionAprendizaje sesion = sesionDao.buscarSesionById(idSesion);
            SesionSequenciaDidactica sequencia = new SesionSequenciaDidactica(nomSequencia);
            sequencia.setSesion(sesion);
            sesionDao.registrarSequenciaDidactica(sequencia);
            for(int i = 0; i < conts.length(); i++){
                JSONObject jsonCont = conts.getJSONObject(i);
                SeccionSequenciaDidactica seccion = sesionDao.getSeccionSequenciaDidactica(jsonCont.getInt("id"));

                SeccionSequenciaDidacticaSesion secSes = new SeccionSequenciaDidacticaSesion(seccion,sequencia,jsonCont.optInt("dur"));
                sesionDao.registrarSeccionDeSequencia(secSes);

                JSONArray jsonActivs = jsonCont.getJSONArray("activs");
                for(int j = 0; j < jsonActivs.length();  j++){
                    JSONObject jsonActiv = jsonActivs.getJSONObject(j);
                    ActividadSeccionSequenciaDidactica activs = new ActividadSeccionSequenciaDidactica(seccion,sequencia,jsonActiv.optString("nom"),jsonActiv.optString("des"));
                    sesionDao.registrarActividadSequencia(activs);

                }
            }
            return WebResponse.crearWebResponseExito("Se registro correctamente los datos",new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"sesSeqDidId"},
                    new String[]{"id"},
                    sequencia
            )));
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarSecuenciaTx",e);
            return WebResponse.crearWebResponseError("Error al registrar los datos");
        }
    }
}
