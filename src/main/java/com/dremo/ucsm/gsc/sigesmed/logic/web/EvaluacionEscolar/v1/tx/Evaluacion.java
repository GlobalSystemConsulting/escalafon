/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.EvaluacionEscolar.v1.tx;

/**
 *
 * @author abel
 */
public class Evaluacion {
    
    public static final String EVALUACION_PATH = "/evaluacion/";    
    public static final String BANDEJA_EVALUACION_PATH = "/evaluacion/bandeja/";
    
    public static final char ESTADO_NUEVO = 'N';
    public static final char ESTADO_ENVIADO = 'E';
    public static final char ESTADO_CALIFICADO = 'C';
    public static final char ESTADO_FUERA_TIEMPO = 'F';
    
}
