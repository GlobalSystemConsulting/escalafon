/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author felipe
 */
@Entity
@Table(name = "organigrama", schema="administrativo")

public class Organigrama implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "orgi_id")
    private Integer orgiId;
    
    @Size(max = 2147483647)
    @Column(name = "cod_orgi")
    private String codOrgi;
    
    @Size(max = 2147483647)
    @Column(name = "abr_orgi")
    private String abrOrgi;
    @Size(max = 2147483647)
    @Column(name = "nom_orgi")
    private String nomOrgi;
    @Size(max = 2147483647)
    @Column(name = "cam_orgi")
    private String camOrgi;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="orgi_pad_id", updatable=false, insertable=false)
    private Organigrama organigramaPadre;
    
    @Column(name="orgi_pad_id")
    private Integer orgiPadId;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="organigramaPadre")
    List<Organigrama> organigramas;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tip_orgi_id", nullable=false)
    private TipoOrganigrama tipoOrganigrama;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ubi_id")
    private Ubicacion ubicacion;
    
    @Column(name="ubi_id", insertable = false, updatable = false)
    private Integer ubiId;

    public Organigrama() {}
    
    public Organigrama(int orgiId) {
        this.orgiId = orgiId;
    }
    
    //CONSIDERANDO EL CODIGP PADRE
    public Organigrama( TipoOrganigrama tipoOrganigrama,Organigrama organiPadre, String codigoOrg, String abreOrgi, String nomOrgi,String camOrgi,Date fech,Integer user,Character estado){
        this.tipoOrganigrama=tipoOrganigrama;
        this.codOrgi=codigoOrg;
        this.abrOrgi=abreOrgi;
        this.nomOrgi=nomOrgi;
        this.camOrgi=camOrgi;
        this.fecMod=fech;
        this.usuMod=user;
        this.estReg=estado;
        this.organigramaPadre=organiPadre;
    
    }
    //SIN CONSIDERAR EL CODIGO PADRE
    public Organigrama( TipoOrganigrama tipoOrganigrama, String codigoOrg, String abreOrgi, String nomOrgi,String camOrgi,Date fech,Integer user,Character estado){
        this.tipoOrganigrama=tipoOrganigrama;
        this.codOrgi=codigoOrg;
        this.abrOrgi=abreOrgi;
        this.nomOrgi=nomOrgi;
        this.camOrgi=camOrgi;
        this.fecMod=fech;
        this.usuMod=user;
        this.estReg=estado;
    
    }
    public Organigrama( TipoOrganigrama tipoOrganigrama, String codigoOrg, String abreOrgi, String nomOrgi,String camOrgi,Integer user,Character estado){
        this.tipoOrganigrama=tipoOrganigrama;
        this.codOrgi=codigoOrg;
        this.abrOrgi=abreOrgi;
        this.nomOrgi=nomOrgi;
        this.camOrgi=camOrgi;
        this.usuMod=user;
        this.estReg=estado;
    
    }
    public Organigrama(Integer orgiId) {
        this.orgiId = orgiId;
    }

    public Integer getOrgiId() {
        return orgiId;
    }

    public void setOrgiId(Integer orgiId) {
        this.orgiId = orgiId;
    }

    public String getCodOrgi() {
        return codOrgi;
    }

    public void setCodOrgi(String codOrgi) {
        this.codOrgi = codOrgi;
    }

    public String getAbrOrgi() {
        return abrOrgi;
    }

    public void setAbrOrgi(String abrOrgi) {
        this.abrOrgi = abrOrgi;
    }

    public String getNomOrgi() {
        return nomOrgi;
    }

    public void setNomOrgi(String nomOrgi) {
        this.nomOrgi = nomOrgi;
    }

    public String getCamOrgi() {
        return camOrgi;
    }

    public void setCamOrgi(String camOrgi) {
        this.camOrgi = camOrgi;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public Organigrama getOrganigramaPadre() {
        return organigramaPadre;
    }

    public void setOrganigramaPadre(Organigrama organigramaPadre) {
        this.organigramaPadre = organigramaPadre;
    }

    public List<Organigrama> getOrganigramas() {
        return organigramas;
    }

    public void setOrganigramas(List<Organigrama> organigramas) {
        this.organigramas = organigramas;
    }

    public TipoOrganigrama getTipoOrganigrama() {
        return tipoOrganigrama;
    }

    public void setTipoOrganigrama(TipoOrganigrama tipoOrganigrama) {
        this.tipoOrganigrama = tipoOrganigrama;
    }

    public Integer getOrgiPadId() {
        return orgiPadId;
    }

    public void setOrgiPadId(Integer orgiPadId) {
        this.orgiPadId = orgiPadId;
    }

    public Ubicacion getZona() {
        return ubicacion;
    }

    public void setZona(Ubicacion zona) {
        this.ubicacion = zona;
    }    

    public Integer getUbiId() {
        return ubiId;
    }

    public void setUbiId(Integer ubiId) {
        this.ubiId = ubiId;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orgiId != null ? orgiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Organigrama)) {
            return false;
        }
        Organigrama other = (Organigrama) object;
        if ((this.orgiId == null && other.orgiId != null) || (this.orgiId != null && !this.orgiId.equals(other.orgiId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Organigrama{" + "orgiId=" + orgiId + ", codOrgi=" + codOrgi + ", abrOrgi=" + abrOrgi + ", nomOrgi=" + nomOrgi + ", camOrgi=" + camOrgi + ", fecMod=" + fecMod + ", usuMod=" + usuMod + ", estReg=" + estReg + '}';
    }

    
}
