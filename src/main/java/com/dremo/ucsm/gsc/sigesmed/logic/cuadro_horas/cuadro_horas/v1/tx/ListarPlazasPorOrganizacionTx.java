/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DistribucionHoraGradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlazaMagisterial;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarPlazasPorOrganizacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int organizacionID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            organizacionID = requestData.getInt("organizacionID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las plazas magisteriales, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<PlazaMagisterial> plazas = null;
        
        try{
            DistribucionHoraGradoDao disDao = (DistribucionHoraGradoDao)FactoryDao.buildDao("mech.DistribucionHoraGradoDao");
            
            plazas = disDao.buscarPlazasPorOrganizacion(organizacionID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las plazas magisteriales\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las plazas magisteriales", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        
        JSONArray miArray = new JSONArray();
        int i = 0;
        for(PlazaMagisterial plaza:plazas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("plazaID",plaza.getPlaMagId());
            
            oResponse.put("modalidad",plaza.getModalidad());
            oResponse.put("nivel",plaza.getNivel());
            oResponse.put("caracteristica",plaza.getCaracteristica());
            oResponse.put("tipo",""+plaza.getTipo());
            oResponse.put("nexus",plaza.getCodigoNexus());
            oResponse.put("cargo",plaza.getCargo());
            oResponse.put("jornadaLaboral",plaza.getJornadaLaboral());
            oResponse.put("jornadaPedagogica",plaza.getJornadaPedagogica());
            oResponse.put("especialidad",plaza.getEspecialidad());
            oResponse.put("motivo",plaza.getMotivo());
            oResponse.put("condicionID",""+plaza.getCondicionId());
            oResponse.put("naturalezaID",""+plaza.getNaturalezaId());
            oResponse.put("docenteID",plaza.getDocenteId());
            
            oResponse.put("organizacionID",plaza.getOrgId());
            oResponse.put("i",i++);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las plazas se listo correctamente",miArray);        
        //Fin
    }
    
}

