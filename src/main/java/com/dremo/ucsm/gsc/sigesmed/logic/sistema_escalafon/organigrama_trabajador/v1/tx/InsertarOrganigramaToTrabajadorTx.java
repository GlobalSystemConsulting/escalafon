/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorOrganigramaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TrabajadorOrganigramaDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author felipe
 */
public class InsertarOrganigramaToTrabajadorTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(InsertarOrganigramaToTrabajadorTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        
        TrabajadorOrganigramaDetalle traOrgDet = null;
        Trabajador tra  = null;
        int traId = 0;
        int orgiId = 0;
        try {
            JSONObject requestData = (JSONObject)wr.getData();
            traId = requestData.getInt("traId");
            orgiId = requestData.getInt("orgiId");
            //int ubiId = requestData.getInt("ubiId");
            int plaId = requestData.getInt("plaId");
            int catId = requestData.getInt("catId");
            int carId = requestData.getInt("carId");
            
            TrabajadorOrganigramaDetalleDao traOrgDetDao = (TrabajadorOrganigramaDetalleDao)FactoryDao.buildDao("se.TrabajadorOrganigramaDetalleDao");
            traOrgDet = new TrabajadorOrganigramaDetalle(traId,orgiId,1,plaId,catId,carId);
            traOrgDetDao.insert(traOrgDet);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva relacion trabajador organigrama ",e);
            System.out.println(e);
        }
        
        try {
            TrabajadorDao traDao = (TrabajadorDao)FactoryDao.buildDao("se.TrabajadorDao");
            tra = traDao.buscarPorId(traId);
            tra.setOrgiId(orgiId);
            traDao.update(tra);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Actualizar orgiId en Trabajador ",e);
            System.out.println(e);
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("orgiId", traOrgDet.getOrgiId());
        oResponse.put("plaId", traOrgDet.getPlaId());
        oResponse.put("catId", traOrgDet.getCatId());
        oResponse.put("carId", traOrgDet.getCarId());

        return WebResponse.crearWebResponseExito("El registro Organigrama se realizo correctamente", oResponse);

    }
    
}
