/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.ascenso.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.AscensoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ascenso;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarAscensoByTipoTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarAscensoByTipoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer ascId = requestData.getInt("ascId");
            Character tip = requestData.getString("tip").charAt(0);
            int tipDocId = requestData.getInt("tipDocId");
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            Date fecEfe = sdi.parse(requestData.getString("fecEfe").substring(0, 10));
            String mot = requestData.getString("mot");
            
            int orgiPostId = requestData.getInt("orgiPostId");
            int catPostId = requestData.getInt("catPostId");
            int carPostId = requestData.getInt("carPostId");
            
            return actualizarAscenso(ascId, numDoc,tip, fecDoc, tipDocId, fecEfe, mot, orgiPostId, catPostId, carPostId);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar ascenso",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarAscenso(Integer ascId, String numDoc, Character tip, Date fecDoc, int tipDocId, Date fecEfe, String mot, Integer orgiPostId, Integer catPostId, Integer carPostId) {
        try{
            AscensoDao ascensoDao = (AscensoDao)FactoryDao.buildDao("se.AscensoDao");
            CategoriaDao categoriaDao = (CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
            CargoDao cargoDao = (CargoDao)FactoryDao.buildDao("se.CargoDao");
            Categoria categoria = null;
            Cargo cargo = null;
            Ascenso ascenso = ascensoDao.buscarPorId(ascId);

            ascenso.setTipDocId(tipDocId);
            ascenso.setNumDoc(numDoc);
            ascenso.setFecDoc(fecDoc);
            ascenso.setFecEfe(fecEfe);
            ascenso.setMot(mot);
            ascenso.setOrgiPostId(orgiPostId);
            ascenso.setCatPostId(catPostId);
            ascenso.setCarPostId(carPostId);
            
            ascensoDao.update(ascenso);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("ascId", ascenso.getAscId());
            oResponse.put("tip", ascenso.getTip());
            oResponse.put("tipDocId", ascenso.getTipDocId());
            oResponse.put("numDoc", ascenso.getNumDoc());
            oResponse.put("fecDoc", sdo.format(ascenso.getFecDoc()));
            oResponse.put("fecEfe", sdo.format(ascenso.getFecEfe()));
            oResponse.put("mot", sdo.format(ascenso.getMot()));
            
            //Historico
            if(ascenso.getOrgiAntId()!=null)
                oResponse.put("orgiAntId", ascenso.getOrgiAntId());
            else
                oResponse.put("orgiAntId", 0);

            if(ascenso.getCatAntId()!=null){
                categoria=categoriaDao.obtenerDetalle(ascenso.getCatAntId());
                oResponse.put("catAntId", ascenso.getCatAntId());
                oResponse.put("nomAntCat", categoria.getNomCat());
                oResponse.put("nomAntPla", categoria.getPlanilla().getNomPla());
            }
            else{
                oResponse.put("catAntId", 0);
                oResponse.put("nomAntCat", "");
                oResponse.put("nomAntPla", "");
            }

            if(ascenso.getCarAntId()!=null){
                cargo=cargoDao.buscarPorId(ascenso.getCarAntId());
                oResponse.put("carAntId", ascenso.getCarAntId());
                oResponse.put("nomAntCar", cargo.getNomCar());            
            }
            else{
                oResponse.put("carAntId", 0);
                oResponse.put("nomAntCar", "");
            }
            
            //post
            if(ascenso.getOrgiPostId()!=null)
                oResponse.put("orgiPostId", ascenso.getOrgiPostId());
            else
                oResponse.put("orgiPostId", 0);

            if(ascenso.getCatPostId()!=null){
                categoria=categoriaDao.obtenerDetalle(ascenso.getCatPostId());
                oResponse.put("catPostId", ascenso.getCatPostId());
                oResponse.put("nomPostCat", categoria.getNomCat());
                oResponse.put("nomPostPla", categoria.getPlanilla().getNomPla());
            }
            else{
                oResponse.put("catPostId", 0);
                oResponse.put("nomPostCat", "");
                oResponse.put("nomPostPla", "");
            }

            if(ascenso.getCarPostId()!=null){
                cargo=cargoDao.buscarPorId(ascenso.getCarPostId());
                oResponse.put("carPostId", ascenso.getCarPostId());
                oResponse.put("nomPostCar", cargo.getNomCar());            
            }
            else{
                oResponse.put("carPostId", 0);
                oResponse.put("nomPostCar", "");
            }
            return WebResponse.crearWebResponseExito("Ascenso actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarAscenso",e);
            return WebResponse.crearWebResponseError("Error, el ascenso no fue actualizado");
        }
    } 
    
}
