/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ConstruirUrlArchTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
            //Consulta del tamaño de la url general

        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo leer la ruta general");
        }
        JSONArray a = new JSONArray();
        
        JSONObject obj = new JSONObject();

        obj.put("ruta",(ServicioREST.PATH_SIGESMED + "/app").replace('\\','/'));
        return WebResponse.crearWebResponseExito("El tamaño de Ruta se leyo exitosamente",obj);
    }
    
}
