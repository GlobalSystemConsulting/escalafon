package com.dremo.ucsm.gsc.sigesmed.logic.maestro.anecdotario.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.anecdotario.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx.RegistrarCarpetaTx;

/**
 * Created by Administrador on 10/10/2016.
 */
public class ComponentRegister implements IComponentRegister {
    @Override
    public WebComponent createComponent() {
        WebComponent component = new WebComponent(Sigesmed.SUBMODULO_MAESTRO);
        component.setName("anecdotario");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarGrados", ListarGradosMaestroTx.class);
        component.addTransactionGET("listarEstudiantes", ListarEstudiantesTx.class);
        component.addTransactionGET("listarAnecdotas", ListarAnecdotasTx.class);
        component.addTransactionGET("listarAAccionesCorrectivas", ListarAccionesCorrectivasTx.class);
        component.addTransactionPOST("registrarAnecdota", RegistrarAnecdotaTx.class);
        component.addTransactionPUT("editarAnecdota",EditarAnecdotaTx.class);
        component.addTransactionDELETE("eliminarAnecdota", EliminarAnecdotaTx.class);
        return component;
    }
}
