package com.dremo.ucsm.gsc.sigesmed.core.entity.web;

import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSessionPersona;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pedagogico.mensaje_electronico" )
public class MensajeElectronico  implements java.io.Serializable {

    @Id
    @Column(name="men_ele_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_mensajeelectronico", sequenceName="pedagogico.mensaje_electronico_men_ele_id_seq" )
    @GeneratedValue(generator="secuencia_mensajeelectronico")
    private int menEleId;
    @Column(name="asu",length=128)
    private String asu;
    @Column(name="des", length=512)
    private String des;
    
    @Column(name="lis_gru",length=128)
    private String lisGru;
    
    @Column(name="lis_usu",length=128)
    private String lisUsu;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_env", nullable=false, length=29)
    private Date fecEnv;
      
    @Column(name="usu_ses_id")
    private int usuSesId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="usu_ses_id",insertable = false,updatable = false)
    private UsuarioSessionPersona session;
    
    @OneToMany(mappedBy="mensaje",cascade=CascadeType.ALL)
    private List<MensajeDocumento> documentos;

    public MensajeElectronico() {
    }
    public MensajeElectronico(int menEleId) {
        this.menEleId = menEleId;
    }
    public MensajeElectronico(int menEleId,String asu, String des, String lisGru,String lisUsu,Date fecEnv,int usuSesId ) {
       this.menEleId = menEleId;
       this.asu = asu;
       this.des = des;
       this.lisGru = lisGru;
       this.lisUsu = lisUsu;
       this.fecEnv = fecEnv;
       this.usuSesId = usuSesId;
    }
   
     
    public int getMenEleId() {
        return this.menEleId;
    }    
    public void setMenEleId(int menEleId) {
        this.menEleId = menEleId;
    }
    
    public String getAsu() {
        return this.asu;
    }
    public void setAsu(String asu) {
        this.asu = asu;
    }    
    public String getDes() {
        return this.des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
    public String getLisGru(){
        return this.lisGru;
    }
    public void setLisGru(String lisGru){
        this.lisGru = lisGru;
    }
    
    public String getLisUsu(){
        return this.lisUsu;
    }
    public void setLisUsu(String lisUsu){
        this.lisUsu = lisUsu;
    }
    
    public Date getFecEnv() {
        return this.fecEnv;
    }
    public void setFecEnv(Date fecEnv) {
        this.fecEnv = fecEnv;
    }
    
    public int getUsuSesId() {
        return this.usuSesId;
    }
    public void setUsuSesId(int usuSesId) {
        this.usuSesId = usuSesId;
    }
    
    public UsuarioSessionPersona getSession() {
        return this.session;
    }    
    public void setSession(UsuarioSessionPersona session) {
        this.session = session;
    }
    
    public List<MensajeDocumento> getDocumentos(){
        return this.documentos;
    }
    public void setDocumentos(List<MensajeDocumento> documentos){
        this.documentos = documentos;
    }
}


