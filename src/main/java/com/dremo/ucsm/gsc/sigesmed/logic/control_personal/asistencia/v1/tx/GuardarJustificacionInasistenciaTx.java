/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.JustificacionInasistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.Inasistencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.JustificacionInasistenciaTrabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;


/**
 *
 * @author carlos
 */
public class GuardarJustificacionInasistenciaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
          
        JustificacionInasistenciaTrabajador justificacion=null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            Integer idIna = requestData.getInt("idIna");  
            Integer tipo = requestData.getInt("tipo");  
            String tramite=requestData.getString("tramite");
            String obs=requestData.getString("obs");       
            
            justificacion=new JustificacionInasistenciaTrabajador(tramite, obs, tipo+"", new Inasistencia(idIna),new Date());
          
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos ", e.getMessage() );
        }

        
        try{
            JustificacionInasistenciaDao justificacionInasistenciaDao = (JustificacionInasistenciaDao)FactoryDao.buildDao("cpe.JustificacionInasistenciaDao");
            justificacionInasistenciaDao.insert(justificacion);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo ingresar la justificacion ", e.getMessage() );
        }

        return WebResponse.crearWebResponseExito("Se Inserto correctamente");        
        //Fin
    }
    
}

