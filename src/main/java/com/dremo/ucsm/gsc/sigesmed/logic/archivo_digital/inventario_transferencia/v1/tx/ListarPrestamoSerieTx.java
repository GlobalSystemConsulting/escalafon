/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.inventario_transferencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.PrestamoSerieDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.PrestamoSerieDocumental;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 *
 * @author Jeferson
 */
public class ListarPrestamoSerieTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        JSONObject requestData = (JSONObject)wr.getData();
        List<PrestamoSerieDocumental> pres_serie = null;
        
        try{
             PrestamoSerieDAO inv_tra_dao = (PrestamoSerieDAO)FactoryDao.buildDao("sad.PrestamoSerieDAO");
             pres_serie = inv_tra_dao.listarPrestamos();
        }catch(Exception e){
             System.out.println("No se pudo Listar los Prestamos de Serie\n"+e);
             return WebResponse.crearWebResponseError("No se pudo Listar los Prestamos de Serie", e.getMessage() );
            
        }
        JSONArray miArray = new JSONArray();
        for(PrestamoSerieDocumental ps :pres_serie ){
            
          
            
            JSONObject oResponse = new JSONObject();
            
            oResponse.put("inv_tra_id",ps.getInvTransId());
            oResponse.put("pre_ser_doc_id",ps.getPrestamoSerieDocumentalId());
            oResponse.put("fec_dev",ps.getFecDev());
            oResponse.put("fec_pre",ps.getFecPres());
            
            miArray.put(oResponse);
        }
           return WebResponse.crearWebResponseExito("Se Listo los Prestamos de Serie Correctamente",miArray); 
         
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
