/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Felipe
 */

@Entity
@Table(name = "planilla", schema="administrativo")

public class Planilla implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pla_id")
    private Integer plaId;
    
    
    @Size(max = 2147483647)
    @Column(name = "pla_rol_nom")
    private String plaRolNom;
    
    @Size(max = 2147483647)
    @Column(name = "cod_pla")
    private String codPla;
    
    @Size(max = 2147483647)
    @Column(name = "abr_pla")
    private String abrPla;
    
    @Size(max = 2147483647)
    @Column(name = "nom_pla")
    private String nomPla;
    
    @Size(max = 2147483647)
    @Column(name = "cam_pla")
    private String camPla;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "planilla")
    List<Categoria> categoria;

    public Planilla() {
    }

    public Planilla(Integer plaId) {
        this.plaId = plaId;
    }

    public Integer getPlaId() {
        return plaId;
    }

    public void setPlaId(Integer plaId) {
        this.plaId = plaId;
    }

    public String getCodPla() {
        return codPla;
    }

    public void setCodPla(String codPla) {
        this.codPla = codPla;
    }

    public String getAbrPla() {
        return abrPla;
    }

    public void setAbrPla(String abrPla) {
        this.abrPla = abrPla;
    }

    public String getNomPla() {
        return nomPla;
    }

    public void setNomPla(String nomPla) {
        this.nomPla = nomPla;
    }

    public String getCamPla() {
        return camPla;
    }

    public void setCamPla(String camPla) {
        this.camPla = camPla;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public String getPlaRolNom() {
        return plaRolNom;
    }

    public void setPlaRolNom(String plaRolNom) {
        this.plaRolNom = plaRolNom;
    }

    public List<Categoria> getCategoria() {
        return categoria;
    }

    public void setCategoria(List<Categoria> categoria) {
        this.categoria = categoria;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (plaId != null ? plaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Planilla)) {
            return false;
        }
        Planilla other = (Planilla) object;
        if ((this.plaId == null && other.plaId != null) || (this.plaId != null && !this.plaId.equals(other.plaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Planilla{" + "codPla=" + codPla + ", abrPla=" + abrPla + ", nomPla=" + nomPla + '}';
    }

    
    
}
