package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.reports.mep.MepReport;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by geank on 01/09/16.
 */
public class ReportesMepTx  extends  MepGeneralTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        String idResumen = wr.getMetadataValue("resumen");
        try {
            return getReport(Integer.parseInt(idResumen));
        }catch (Exception e){
            return formWebResponse(false);
        }
    }
    private WebResponse getReport(int idResumen){
        try{
            MepReport report = new MepReport();
            report.createReport(idResumen);
            String buffer = report.encodeToBase64();
            JSONArray data = new JSONArray();
            data.put(buffer);
            WebResponse response = formWebResponse(true);
            response.setData(data);
            return response;
        }catch (IOException | HibernateException e){
            return formWebResponse(false);
        }
    }

}
