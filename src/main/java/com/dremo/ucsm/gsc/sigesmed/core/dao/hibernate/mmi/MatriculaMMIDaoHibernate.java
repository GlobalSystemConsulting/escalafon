package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.DatosNacimiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Domicilio;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.MatriculaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.ParientesMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.util.ExcelUtil;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Table;
import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class MatriculaMMIDaoHibernate extends GenericMMIDaoHibernate<MatriculaMMI> {

    //Suponiendo que un estudiante solo tiene una matricula activa
    public MatriculaMMI getMatriculaByEstId(long estId) {
        MatriculaInsDaoHibernate mat = new MatriculaInsDaoHibernate();
        GenericMMIDaoHibernate<MatriculaMMI> hb = new GenericMMIDaoHibernate<>();
        long key;
        MatriculaMMI matricula = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            key = mat.getMatIdByEstId(estId);
            matricula = mat.load(key);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
        return matricula;
    }

    //suponiendo que solo tenga una activa
    public void generarFichaMatriculaByEstId(long estId) {
        EstudianteMMIDaoHibernate estDao = new EstudianteMMIDaoHibernate();
        try {
            makeFichaMatricula(estDao.find4CodPer(estId));
        } catch (Exception e) {
            throw e;
        }
    }

    private void makeFichaMatricula(EstudianteMMI estudiante) {
        String urlFichaMatricula = ServicioREST.PATH_SIGESMED + File.separator + "archivos" + File.separator + "matricula_institucional" + File.separator + "templates" + File.separator + "FichaMatricula.xlsx";
        ExcelUtil myExcel = new ExcelUtil(urlFichaMatricula, "ANVERSO");

        PersonaMMI personaEstudiante = estudiante.getPersona();
        DatosNacimiento nacimiento = estudiante.getDatosNacimiento();

        myExcel.insertCell(12, 0, personaEstudiante.getApePat());  //Apellido Paterno
        myExcel.insertCell(12, 8, personaEstudiante.getApeMat());  //Apellido Materno
        myExcel.insertCell(12, 17, personaEstudiante.getNom());    //Nombres

        // Sexo
        if (personaEstudiante.getSex() == 'M') {
            myExcel.insertCell(12, 29, "X");
        } else if (personaEstudiante.getSex() == 'F') {
            myExcel.insertCell(12, 31, "X");
        }

        myExcel.insertCell(12, 32, personaEstudiante.getEstCiv()); // estado civil

        //Codigo de Estudiante numerico length=14
        String codigoEstudiante;
        int numLetters;
        try {
            codigoEstudiante = String.format("%014d", Integer.parseInt(estudiante.getCodEst()));
        } catch (Exception e) {
            codigoEstudiante = estudiante.getCodEst();
            if (codigoEstudiante != null) {
                numLetters = codigoEstudiante.length();
                if (numLetters < 14) {
                    for (int i = 0; i < 14 - numLetters; i++) {
                        codigoEstudiante = codigoEstudiante + " ";
                    }
                }
            }
        }
        for (int i = 44; i < 58; i++) {
            myExcel.insertCell(9, i, "" + codigoEstudiante.charAt(i - 44));
        }

        if (nacimiento != null) {
            //Nacimiento Registrado
            if (nacimiento.getNacReg() != null) {
                if (nacimiento.getNacReg()) {
                    myExcel.insertCell(12, 39, "X");
                } else {
                    myExcel.insertCell(12, 41, "X");
                }
            }

            //fecha de nacimiento
            if (nacimiento.getFecNac() != null) {
                Calendar fechaNacimiento = Calendar.getInstance();
                fechaNacimiento.setTime(nacimiento.getFecNac());
                int diaNac = fechaNacimiento.get(Calendar.DAY_OF_MONTH);
                int mesNac = fechaNacimiento.get(Calendar.MONTH) + 1;
                int anoNac = fechaNacimiento.get(Calendar.YEAR);
                myExcel.insertCell(16, 6, "" + diaNac);
                myExcel.insertCell(16, 8, "" + mesNac);
                myExcel.insertCell(16, 11, "" + anoNac);
            }

            if (nacimiento.getPais() != null) {
                myExcel.insertCell(17, 6, nacimiento.getPais().getPaiNom());
            }
        }

        if (personaEstudiante.getLenguaByLenMat() != null) {
            myExcel.insertCell(15, 20, personaEstudiante.getLenguaByLenMat().getLenNom());
        }

        if (personaEstudiante.getLenguaByLenSeg() != null) {
            myExcel.insertCell(17, 20, personaEstudiante.getLenguaByLenSeg().getLenNom());
        }

        //DNI
        myExcel.insertCell(21, 8, "X");
        myExcel.insertCell(21, 20, personaEstudiante.getDni());

        DomicilioDaoHibernate domDao = new DomicilioDaoHibernate();
        List<Domicilio> myDomicilios = domDao.domiciliosByEstIdOrdered(estudiante.getPerId());
        if (myDomicilios != null) {
            for (int i = 0; i < myDomicilios.size() && i < 9; i++) {
                myExcel.insertCell(35 + i, 0, "" + myDomicilios.get(i).getDomAno());
                myExcel.insertCell(35 + i, 2, myDomicilios.get(i).getDomDir());
                myExcel.insertCell(35 + i, 14, myDomicilios.get(i).getDomRef());
                myExcel.insertCell(35 + i, 32, myDomicilios.get(i).getDomTel());
            }
        }

        ParienteEstudianteDaoHibernate parDao = new ParienteEstudianteDaoHibernate();
        List<ParientesMMI> myParientes = parDao.find4Estudiante(estudiante.getPerId());
        boolean flagMadre = false, flagPadre = false;
        ParientesMMI temp;
        if (myParientes != null) {
            for (int i = 0; i < myParientes.size(); i++) {
                temp = myParientes.get(i);
                if (temp.getTipoPariente().getTpaId() == 1 && !flagPadre) { //padre
                    myExcel.insertCell(35, 42, temp.getPersonaByParId().getApePat());
                    myExcel.insertCell(36, 42, temp.getPersonaByParId().getApeMat());
                    myExcel.insertCell(37, 42, temp.getPersonaByParId().getNom());

                    if (temp.getParViv()) {
                        myExcel.insertCell(38, 44, "X");
                    } else {
                        myExcel.insertCell(38, 48, "X");
                    }

                    if (temp.getPersonaByParId().getGradoInstruccion() != null) {
                        myExcel.insertCell(41, 42, temp.getPersonaByParId().getGradoInstruccion().getGraInsNom());
                    }

                    myExcel.insertCell(42, 42, temp.getPersonaByParId().getOcu());

                    if (temp.getParVivEst()) {
                        myExcel.insertCell(43, 44, "X");
                    } else {
                        myExcel.insertCell(43, 48, "X");
                    }
                    flagPadre = true;
                }
                if (temp.getTipoPariente().getTpaId() == 2 && !flagMadre) { //madre
                    myExcel.insertCell(35, 50, temp.getPersonaByParId().getApePat());
                    myExcel.insertCell(36, 50, temp.getPersonaByParId().getApeMat());
                    myExcel.insertCell(37, 50, temp.getPersonaByParId().getNom());

                    if (temp.getParViv()) {
                        myExcel.insertCell(38, 52, "X");
                    } else {
                        myExcel.insertCell(38, 56, "X");
                    }

                    if (temp.getPersonaByParId().getGradoInstruccion() != null) {
                        myExcel.insertCell(41, 50, temp.getPersonaByParId().getGradoInstruccion().getGraInsNom());
                    }
                    myExcel.insertCell(42, 50, temp.getPersonaByParId().getOcu());

                    if (temp.getParVivEst()) {
                        myExcel.insertCell(43, 52, "X");
                    } else {
                        myExcel.insertCell(43, 56, "X");
                    }
                    flagMadre = true;
                }
            }
        }

        String path = ServicioREST.PATH_SIGESMED + File.separator + "archivos" + File.separator + "matricula_institucional" + File.separator + "fichaMatricula" + File.separator;
//        DateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
//        Date date = new Date();
//        String name = "fichaMatricula_" + personaEstudiante.getDni() + "_" + formatDate.format(date.getTime()) + ".xlsx";
//        myExcel.makeXlsx(path, name); //copia generada
        myExcel.makeXlsx(path, "fichaMatricula_" + personaEstudiante.getDni() + ".xlsx"); //copia generada
    }
}
