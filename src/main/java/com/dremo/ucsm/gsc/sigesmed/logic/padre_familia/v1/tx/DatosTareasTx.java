/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.padre_familia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mpf.EstudianteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Matricula;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mpf.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlos
 */
public class DatosTareasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
       
        Integer usuarioId;
        
        Persona pariente=null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            usuarioId = requestData.getInt("usuarioID");  
            pariente=new Persona(Long.parseLong(usuarioId+""));
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }

        List<Matricula> hijos = new ArrayList<>();
        EstudianteDao estudianteDao = (EstudianteDao)FactoryDao.buildDao("mpf.EstudianteDao");
        try{
            hijos=estudianteDao.listarHijosDisponibles(usuarioId);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar los Estudiantes del Pariente ", e.getMessage() );
        }
//
        JSONArray miArray = new JSONArray();
//        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//        
        for(Matricula per:hijos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("personaID",per.getEstudiante().getPerId());
            oResponse.put("matriculaID",per.getMatId());
            oResponse.put("dni",per.getEstudiante().getPersona().getDni());
            oResponse.put("nombre",per.getEstudiante().getPersona().getNombrePersonaAP());
            oResponse.put("nombreOrganizacion",per.getOrganizacionByOrgDesId().getNom());

            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

