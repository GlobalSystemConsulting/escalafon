/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author geank
 */
public interface MEP {
   
    String SUCC_RESPONSE_COD = "succ";
    String ERR_RESPONSE_COD = "err";
    String SUCC_RESPONSE_MESS_LIST = "Operacion realizada con exito";
    String ERR_RESPONSE_MESS_LIST = "No se pudo obtener los elementos de la ficha de evaluacion";
    
    String SUCC_RESPONSE_MESS_INS = "Se realizo el registro correctamente";
    String ERR_RESPONSE_MESS_INS = "No se pudo realizar el registro";
    
    String[] TIPO_ELEMENTOS = {"escalas","rangos","contenido"};
    String TIPO_ELEMENTO_ESCALA = "escalas";
    String TIPO_ELEMENTO_RANGO = "rangos";
    String TIPO_ELEMENTO_CONTENIDO = "contenido";
    String TIPO_ELEMENTO_INDICADOR = "indicador";
    
    String CAMPO_ELEMENTO_FICHA = "fic";
    ///campos de escala de valoracion
    String CAMPO_ESCALA_ID = "id";
    String CAMPO_ESCALA_NOMBRE = "nom";
    String CAMPO_ESCALA_DESCRIPCION = "des";
    String CAMPO_ESCALA_VAL = "val";
    
    //campos de Columna Valoracion
    String CAMPO_COL_ID = "id";
    String CAMPO_COL_VAL = "val";
    
    //campos de Rango Porcentual
    String CAMPO_RANGO_ID = "id";
    String CAMPO_RANGO_NOMBRE = "nom";
    String CAMPO_RANGO_DESCRIPCION = "des";
    String CAMPO_RANGO_MAX = "max";
    String CAMPO_RANGO_MIN = "min";
    
    //campos contenido
    String CAMPO_CONTENIDO_ID = "id";
    String CAMPO_CONTENIDO_TIPO = "tip";
    String CAMPO_CONTENIDO_NOMBRE = "nom";
    
    //campos indicador
    String CAMPO_INDICADOR_ID = "id";
    String CAMPO_INDICADOR_CONTENIDO = "fic";
    String CAMPO_INDICADOR_DESC = "des";
    String CAMPO_INDICADOR_NOMBRE = "nom";
    String CAMPO_INDICADOR_DOC_VER = "doc";

    String[] ESCALAS = new String[]{"DEFICIENTE:Raramente realiza las tareas y obligaciones inherentes a su puesto:1",
            "REGULAR:Frecuentemente presenta dificultades en el desempeño:2","BUENO:Se desempeña de acuerdo a lo esperado:3",
            "MUY BUENO:Se desempeña por encima de lo esperado:4","EXCELENTE:Constantemente supera de manera excepcional el desempeño esperado en el puesto:5"};
    String[] RANGOS = new String[]{"0:33:No se recomienda la ampliación de su contrato.",
            "34:67:Se recomienda la ampliación de su contrato con observación y compromiso de actualización o capacitación personal o desde la I. E.",
            "68:100:Se recomienda la ampliación de su contrato"};
    String[] ESCALAS_DOCENTE = new String[]{"_INICIO:El docente está empezando a desarrollar los desempeños previstos o evidencia dificultades y requiere acompañamiento pedagógico permanente en la escuela y la intervención de otras instituciones:1",
            "_PROCESO:El docente está en camino de lograr los desempeños previstos y requiere acompañamiento pedagógico periódico en la escuela y la intervención de otras instituciones:3",
            "_LOGRO PREVISTO:El docente evidencia el logro de los desempeños previstos y es potencial acompañante pedagógico de sus pares profesionales en la escuela:5"};
    String[] RANGOS_DOCENTE = new String[]{"0:10:El docente está empezando a desarrollar los desempeños previstos o evidencia dificultades y requiere acompañamiento pedagógico permanente en la escuela y la intervención de otras instituciones.",
            "11:15:El docente está en camino de lograr los desempeños previstos y requiere acompañamiento pedagógico periódico en la escuela y la intervención de otras instituciones.",
            "16:20:El docente evidencia el logro de los desempeños previstos y es potencial acompañante pedagógico de sus pares profesionales en la escuela"};;
    String[] DOMINIOS_DOCENTE = new String[]{"Preparación para el aprendizaje de los estudiantes.",
            "Enseñanza para el aprendizaje de los estudiantes",
            "Participación en la gestión de la escuela articulada a la comunidad",
            "Desarrollo de la profesionalidad y la identidad docente"};
    String[] COMPETENCIAS_DOCENTE = new String[]{"1:Conoce y comprende las características de todos sus estudiantes y sus contextos, los contenidos disciplinares que enseña, los enfoques y procesos pedagógicos, con el propósito de promover capacidades de alto nivel y su formación integral",
            "1:Planifica la enseñanza de forma colegiada garantizando la coherencia entre los aprendizajes que quiere lograr en sus estudiantes, el proceso pedagógico el uso de los recursos disponibles y la evaluación, en una programación curricular en permanente revisión",
            "2:Crea un clima propicio para el aprendizaje, la convivencia democrática y la vivencia de la diversidad en todas sus expresiones con miras a formar ciudadanos críticos e interculturales.",
            "2:Conduce el proceso de enseñanza con dominio de los contenidos disciplinares y el uso de estrategias y recursos pertinentes, para que todos los estudiantes aprendan de manera reflexiva y crítica en torno a la solución de problemas relacionados con sus experiencias, intereses y contextos.",
            "2:Evalúa permanentemente el aprendizaje de acuerdo a los objetivos institucionales previstos, para tomar decisiones y retroalimentar a sus estudiantes y a la comunidad educativa, teniendo en cuenta las diferencias individuales y contextos culturales",
            "3:Participa activamente con actitud democrática, crítica y colaborativa en la gestión de la escuela, contribuyendo a la construcción y mejora continua del proyecto educativo institucional que genere aprendizajes de calidad.",
            "3:Establece relaciones de respeto, colaboración y corresponsabilidad con las familias, la comunidad y otras instituciones del estado y la sociedad civil, aprovecha sus saberes y recursos en los procesos educativos y da cuenta de los resultados",
            "4:Reflexiona sobre su práctica y experiencia institucional; y desarrolla procesos de aprendizaje continuo de modo individual y colectivo para construir y afirmar su identidad y responsabilidad profesional.",
            "4:Ejerce su profesión desde una ética de respeto a los derechos fundamentales de las personas, demostrando honestidad, justicia, responsabilidad y compromiso con su función social."};
}
