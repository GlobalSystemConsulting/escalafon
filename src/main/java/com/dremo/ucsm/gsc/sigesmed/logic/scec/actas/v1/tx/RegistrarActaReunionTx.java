package com.dremo.ucsm.gsc.sigesmed.logic.scec.actas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ActasReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.ReunionComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ActasReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ReunionComision;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildCodigo;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 05/10/2016.
 */
public class RegistrarActaReunionTx implements ITransaction {
    private static final Logger logger = Logger.getLogger(RegistrarActaReunionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        ActasReunionComision acta = new ActasReunionComision(data.getString("des"));
        JSONObject filjson = data.optJSONObject("arc");
        return registrarActa(acta,data.getInt("reu"),filjson);
    }
    public WebResponse registrarActa(ActasReunionComision acta, int idReu,JSONObject  filjson){
        try{
            ActasReunionComisionDao actasReunionComisionDao = (ActasReunionComisionDao) FactoryDao.buildDao("scec.ActasReunionComisionDao");
            ReunionComisionDao reunionComisionDao = (ReunionComisionDao) FactoryDao.buildDao("scec.ReunionComisionDao");

            ReunionComision reunionComision = reunionComisionDao.buscarReunionPorId(idReu);

            acta.setReunionComision(reunionComision);
            actasReunionComisionDao.insert(acta);

            JSONObject actajson = new JSONObject(
                    EntityUtil.objectToJSONString(
                            new String[]{"actComId", "des", "fecRegAct"},
                            new String[]{"cod", "des", "reg"},
                            acta)
            );

            if(filjson != null && filjson.length() > 0){
                FileJsonObject miF = new FileJsonObject( filjson ,"reu_" + idReu +"_act_"+ acta.getActComId() + "_" );
                BuildFile.buildFromBase64("actas_reunion", miF.getName(), miF.getData());
                acta.setArcAdj(miF.getName());
                actajson.put("url", Sigesmed.UBI_ARCHIVOS+"/actas_reunion/");
                actajson.put("nom",acta.getArcAdj());
            }
            actasReunionComisionDao.update(acta);

            return WebResponse.crearWebResponseError("Exito al registrar el acta",WebResponse.OK_RESPONSE).setData(actajson);
        }catch (Exception e){
            logger.log(Level.SEVERE, "registrarArea",e);
            return WebResponse.crearWebResponseError("Error al registrar el acta",WebResponse.BAD_RESPONSE);
        }
    }
}