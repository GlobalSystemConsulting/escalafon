/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.alerta_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.AlertaSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.AlertaSistema;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarAlertaSistemaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<AlertaSistema> alertas = null;
        AlertaSistemaDao alertaDao = (AlertaSistemaDao)FactoryDao.buildDao("AlertaSistemaDao");
        try{
            alertas = alertaDao.buscarTodos();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las alertas del Sistema \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las alertas del Sistema ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        int i = 0;
        for(AlertaSistema alerta:alertas ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("alertaID",alerta.getAleId() );
            oResponse.put("funcionID",alerta.getFunSisId() );
            oResponse.put("nombre",alerta.getNom());
            oResponse.put("codigo",alerta.getCod());
            oResponse.put("descripcion",alerta.getDes());
            oResponse.put("accion",alerta.getAcc());
            oResponse.put("tipo",""+alerta.getTipAle());
            oResponse.put("i",i++);
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);
        //Fin
    }
}

