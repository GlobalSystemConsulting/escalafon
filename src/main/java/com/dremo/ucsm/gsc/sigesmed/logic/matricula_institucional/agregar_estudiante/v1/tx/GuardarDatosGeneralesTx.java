package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.DatosNacimiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI_datosGenerales;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Lengua;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Pais;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.PersonaMMI_datosGenerales;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.util.Date;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class GuardarDatosGeneralesTx implements ITransaction {

    private static Logger logger = Logger.getLogger(GuardarDatosGeneralesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        JSONObject persona = data.getJSONObject("persona");
        long perId;
        int lenId_1, lenId_2, paiId;
        String nom, apePat, apeMat, dni, perDir, estCiv, codEst, ubiCod;
        Character sex;
        Date fecNac;
        boolean nacReg;

        try {
            perId = persona.getInt("perId");

            JSONObject lenguaByLenMat = persona.getJSONObject("lenguaByLenMat");
            lenId_1 = lenguaByLenMat.getInt("lenId");

            JSONObject lenguaByLenSeg = persona.getJSONObject("lenguaByLenSeg");
            lenId_2 = lenguaByLenSeg.getInt("lenId");

            nom = persona.getString("nom");
            apePat = persona.getString("apePat");
            apeMat = persona.getString("apeMat");
            dni = persona.getString("dni");
            perDir = persona.getString("perDir");
            sex = persona.getString("sex").charAt(0);
            estCiv = persona.getString("estCiv");

            JSONObject estudiante = data.getJSONObject("estudiante");
            codEst = estudiante.getString("codEst");

            JSONObject datosNacimiento = estudiante.getJSONObject("datosNacimiento");
            JSONObject pais = datosNacimiento.getJSONObject("pais");
            paiId = pais.getInt("paiId");
            fecNac = DateUtil.getDate2String(datosNacimiento.getString("fecNac"));  //revisar
            nacReg = datosNacimiento.getBoolean("nacReg");
            ubiCod = datosNacimiento.getString("ubiCod");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error al cargar datos persona/estudiante/datosNacimiento", e);
            return WebResponse.crearWebResponseError("Nose pudo insertar estudiante");
        }

        GenericMMIDaoHibernate<PersonaMMI_datosGenerales> dhpersona = new GenericMMIDaoHibernate<>();
        GenericMMIDaoHibernate<EstudianteMMI_datosGenerales> dhestudiante = new GenericMMIDaoHibernate<>();
        GenericMMIDaoHibernate<DatosNacimiento> dhdatosNacimiento = new GenericMMIDaoHibernate<>();

        try {
            if ((Long) perId == null || perId < 0) {
                long k = (Long) dhpersona.llave(PersonaMMI_datosGenerales.class, "perId");
                perId = k + 1;
            }

            PersonaMMI_datosGenerales pp = new PersonaMMI_datosGenerales();
            pp.setPerId(perId);
            pp.setNom(nom);
            pp.setApePat(apePat);
            pp.setApeMat(apeMat);
            pp.setDni(dni);
            pp.setPerDir(perDir);
            pp.setSex(sex);
            pp.setEstCiv(estCiv);
            pp.setLenguaByLenMat(new Lengua(lenId_1));
            pp.setLenguaByLenSeg(new Lengua(lenId_2));
            if (ubiCod.length() >= 6) {
                pp.setDepNac(ubiCod.substring(0, 2));
                pp.setProNac(ubiCod.substring(2, 4));
                pp.setDisNac(ubiCod.substring(4, 6));
            }
            pp.setEstReg('A');
            pp.setFecNac(fecNac);

            dhpersona.saveOrUpdate(pp);

            EstudianteMMI_datosGenerales ee = new EstudianteMMI_datosGenerales();
            ee.setPerId(perId);
            ee.setCodEst(codEst);
            ee.setEstReg("A");

            dhestudiante.saveOrUpdate(ee);

            DatosNacimiento dd = new DatosNacimiento();
            dd.setPerId(perId);
            dd.setPais(new Pais(paiId));
            dd.setNacReg(nacReg);
            dd.setUbiCod(ubiCod);
            dd.setFecNac(fecNac);
            dd.setEstReg('A');

            dhdatosNacimiento.saveOrUpdate(dd);

            return WebResponse.crearWebResponseExito("Se guardo al alumno exitosamente", persona);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error Insertar persona/estudiante/datosNacimiento", e);
            return WebResponse.crearWebResponseError("Nose pudo insertar estudiante");
        }
    }
}
