/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseño_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DiseñoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DiseñoCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class EliminarDiseñoCurricularTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int diseñoID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            diseñoID = requestData.getInt("diseñoID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        DiseñoCurricularDao diseñoDao = (DiseñoCurricularDao)FactoryDao.buildDao("mech.DiseñoCurricularDao");
        try{
            diseñoDao.delete(new DiseñoCurricular(diseñoID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Diseño curricular\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Diseño curricular", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Diseño curricular se elimino correctamente");
        //Fin
    }
}
