/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class FichaEscalafonariaDaoHibernate extends GenericDaoHibernate<FichaEscalafonaria> implements FichaEscalafonariaDao {

    @Override
    public FichaEscalafonaria obtenerDatosPersonales(int orgId, int perId) {
        FichaEscalafonaria datosEscalafon = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();

        try {
            String hql = "SELECT fe FROM FichaEscalafonaria AS fe "
                    + "join fetch fe.trabajador t "
                    + "join fetch t.persona p "
                    + "join fetch t.organizacion o "
                    + "WHERE p.perId = " + perId
                    + " AND o.orgId = " + orgId
                    + " AND fe.estReg = 'A'"
                    + " AND t.estReg = 'A'";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            datosEscalafon = (FichaEscalafonaria) query.uniqueResult();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo Listar los datos escalafon \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los datos de escalafon \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return datosEscalafon;
    }

    @Override
    public List<FichaEscalafonaria> listarxOrganizacion(int orgId) {
        List<FichaEscalafonaria> fichaEscalafonaria = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            String hql = "SELECT fe FROM FichaEscalafonaria as fe "
                    + "join fetch fe.trabajador t "
                    + "join fetch t.persona p "
                    + "join fetch t.organizacion o "
                    + "WHERE o.orgId=:orgId";
            Query query = session.createQuery(hql);
            query.setParameter("orgId", orgId);
            fichaEscalafonaria = query.list();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo Listar los datos escalafon por organización \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los datos escalalfon por organización \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fichaEscalafonaria;
    }

    @Override
    public FichaEscalafonaria buscarPorDNI(String perDNI) {
        FichaEscalafonaria fichaEscalafonaria = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();

        try {
            String hql = "SELECT fe FROM FichaEscalafonaria AS fe "
                    + "join fetch fe.trabajador as t "
                    + "join fetch t.persona as p "
                    + "WHERE p.dni= '" + perDNI + "'";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            fichaEscalafonaria = (FichaEscalafonaria) query.uniqueResult();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo  \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo  \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fichaEscalafonaria;

    }
    @Override
    public FichaEscalafonaria buscarPorTraId(Integer traId) {
        FichaEscalafonaria fichaEscalafonaria = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();

        try {
            String hql = "SELECT fe FROM FichaEscalafonaria AS fe "
                    + "join fetch fe.trabajador as t "
                    + "WHERE t.traId= '" + traId + "'";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            fichaEscalafonaria = (FichaEscalafonaria) query.uniqueResult();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo  \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo  \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fichaEscalafonaria;
    }

    @Override
    public FichaEscalafonaria buscarPorId(Integer ficEscId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        FichaEscalafonaria f = (FichaEscalafonaria) session.get(FichaEscalafonaria.class, ficEscId);
        session.close();
        return f;
    }

    @Override
    public FichaEscalafonaria buscarPorUsuId(Integer usuId) {
        FichaEscalafonaria fichaEscalafonaria = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();

        try {
            String hql = "SELECT fe FROM FichaEscalafonaria AS fe"
                    + " join fetch fe.trabajador as t"
                    + " join fetch t.persona as p"
                    + " join fetch p.usuario u"
                    + " WHERE u.usuId =:cod";

            Query query = session.createQuery(hql);
            query.setParameter("cod", usuId);
            query.setMaxResults(1);
            fichaEscalafonaria = (FichaEscalafonaria) query.uniqueResult();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo  \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo  \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fichaEscalafonaria;
    }

    @Override
    public List<FichaEscalafonaria> listarxOrgxCadInicio(int orgId, String cadenaInicio) {
        String entrada = cadenaInicio.toUpperCase();
        System.out.println(entrada);
        String filtro = "";

        if (entrada.equals("")) {
            filtro = " AND p.apePat like 'A%' ";
        } else {
            String sc= entrada.replace(' ','_');
            String separador[] = sc.split("_");

            for (int i = 0; i < separador.length; i++) {
                if (i == 0) {
                    filtro += " AND p.apePat like '" + separador[i] + "%' ";
                }
                if (i == 1) {
                    filtro += " AND p.apeMat like '" + separador[i] + "%' ";
                }
                if (i == 2) {
                    filtro += " AND p.nom like '" + separador[i] + "%'";
                }
                if (i == 3) {
                    break;
                }
            }
        }
        System.out.println(">>>>>>>>:" + filtro);
        List<FichaEscalafonaria> fichasEsc = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            String hql = "SELECT fe FROM FichaEscalafonaria as fe "
                    + "left join fetch fe.trabajador t "
                    + "join fetch t.persona p "
                    + "join fetch t.organizacion o "
                    + "WHERE o.orgId=:orgId "
                    //+ "AND p.apePat like '"+entrada+"%'";
                    + filtro;

            Query query = session.createQuery(hql);
            query.setParameter("orgId", orgId);
            fichasEsc = query.list();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo listar los datos escalafon por organización e inicial\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los datos escalafon por organización e inicial\\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fichasEsc;
    }

    @Override
    public List<FichaEscalafonaria> listarxOrgTopTen(int orgId, int limit, int offset) {
        if(offset < 0)
            offset = 0;
        
        List<FichaEscalafonaria> fichaEscalafonaria = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            String hql = "SELECT fe FROM FichaEscalafonaria as fe "
                    + "join fetch fe.trabajador t "
                    + "join fetch t.persona p "
                    + "join fetch t.organizacion o "
                    + "WHERE o.orgId=:orgId";
            Query query = session.createQuery(hql);
            query.setParameter("orgId", orgId);
            
            if (limit != 0) { //para extraer datos para la actual pagina, si fuera limit = 0, extrae todo
                query.setFirstResult(offset); //equivalent to OFFSET
                query.setMaxResults(limit); //equivalent to LIMIT
            }
            
            fichaEscalafonaria = query.list();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo listar los datos escalafon por organización \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los datos escalalfon por organización \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fichaEscalafonaria;
    }

    @Override
    public List<FichaEscalafonaria> busquedaParametrizada(int orgId, int limit, int offset, String campo, String data) {
        if(offset < 0)
            offset = 0;
        
        List<FichaEscalafonaria> fichaEscalafonaria = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            String nomBDCampo = "";
            switch(campo){
                case "DNI": nomBDCampo = "dni";
                                break;
                case "apePat": nomBDCampo = "apePat";
                                break;
                case "apeMat": nomBDCampo = "apeMat";
                                break;
                case "nombre": nomBDCampo = "nom";
            }
            String hql = "SELECT fe FROM FichaEscalafonaria as fe "
                    + "join fetch fe.trabajador t "
                    + "join fetch t.persona p "
                    + "join fetch t.organizacion o "
                    + "WHERE o.orgId=:orgId "
                    + "AND p."+nomBDCampo+" LIKE '%"+data+"%'";
            Query query = session.createQuery(hql);
            query.setParameter("orgId", orgId);
            
            if (limit != 0) { //para extraer datos para la actual pagina, si fuera limit = 0, extrae todo
                query.setFirstResult(offset); //equivalent to OFFSET
                query.setMaxResults(limit); //equivalent to LIMIT
            }
            
            fichaEscalafonaria = query.list();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo listar los datos escalafon por organización \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los datos escalalfon por organización \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fichaEscalafonaria;
    }

    @Override
    public long canRegBusqPar(int orgId,String nomCampo, String dataCampo) {
        long numRegistros = 0;
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        try {
            //listar Usuarios
            String nomBDCampo = "";
            
            switch(nomCampo){
                case "DNI": nomBDCampo = "dni";
                                break;
                case "apePat": nomBDCampo = "apePat";
                               dataCampo = dataCampo.toUpperCase();
                                break;
                case "apeMat": nomBDCampo = "apeMat";
                               dataCampo = dataCampo.toUpperCase();
                                break;
                case "nombre": nomBDCampo = "nom";
                               dataCampo = dataCampo.toUpperCase();
                                break;
            }
            String hql = "SELECT count(*) FROM FichaEscalafonaria as fe "
                    + "JOIN fe.trabajador t "
                    + "JOIN t.persona p "
                    + "JOIN t.organizacion o "
                    + "WHERE o.orgId=:orgId "
                    + "AND p."+nomBDCampo+" LIKE '%"+dataCampo+"%'";
            Query query = session.createQuery(hql);
            query.setParameter("orgId", orgId);
            System.out.println("queryCountFil: " + query.toString());
            query.setMaxResults(1);
            numRegistros = (long)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo listar los datos escalafon por filtro \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los datos escalafon por filtro \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return numRegistros;
    }
    
    @Override
    public List<FichaEscalafonaria> searchWithFilters(int orgId, int limit, int offset, JSONArray filters) {
        if(offset < 0)
            offset = 0;
        
        List<FichaEscalafonaria> fichaEscalafonaria = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        
        try {     
            String hql = "SELECT fe FROM FichaEscalafonaria as fe "
                    + "join fetch fe.trabajador t "
                    + "join fetch t.persona p "
                    + "join fetch t.organizacion o "
                    + "WHERE o.orgId=:orgId ";
            JSONObject obj;
            for(int i=0; i<filters.length(); i++){
                obj = filters.getJSONObject(i);
                if(obj.getBoolean("est")){
                    hql = hql + "AND p."+ obj.getString("nomFil") + " LIKE '%" +obj.getString("data").toUpperCase() + "%' ";
                }
            }
            
            System.out.println(hql);
            Query query = session.createQuery(hql);
            query.setParameter("orgId", orgId);
            
            if (limit != 0) { //para extraer datos para la actual pagina, si fuera limit = 0, extrae todo
                query.setFirstResult(offset); //equivalent to OFFSET
                query.setMaxResults(limit); //equivalent to LIMIT
            }
            
            fichaEscalafonaria = query.list();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo listar los datos escalafon por organización \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los datos escalalfon por organización \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fichaEscalafonaria;
    }
    
    @Override
    public long canRegBusqxFiltros(int orgId, JSONArray filters) {
        long numRegistros = 0;
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        try {
            String hql = "SELECT count(*) FROM FichaEscalafonaria as fe "
                    + "JOIN fe.trabajador t "
                    + "JOIN t.persona p "
                    + "JOIN t.organizacion o "
                    + "WHERE o.orgId=:orgId ";
            JSONObject obj;
            for(int i=0; i<filters.length(); i++){
                obj = filters.getJSONObject(i);
                
                
                if(obj.getBoolean("est")){
                    hql = hql + "AND p."+ obj.getString("nomFil") + " LIKE '%" +obj.getString("data").toUpperCase() + "%' ";
                }
            }
            Query query = session.createQuery(hql);
            query.setParameter("orgId", orgId);
            System.out.println("queryCountFil: " + query.toString());
            query.setMaxResults(1);
            numRegistros = (long)query.uniqueResult();

        } catch (Exception e) {
            System.out.println("No se pudo listar los datos escalafon por filtro \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los datos escalafon por filtro \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return numRegistros;
    }
    
    @Override
    public FichaEscalafonaria buscarPorDNIyTraId(String perDNI, int traId) {
        FichaEscalafonaria fichaEscalafonaria = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();

        try {
            String hql = "SELECT fe FROM FichaEscalafonaria AS fe "
                    + "join fetch fe.trabajador as t "
                    + "join fetch t.persona as p "
                    + "WHERE p.dni= '" + perDNI + "' "
                    + "AND t.traId= '" + traId + "'";

            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            fichaEscalafonaria = (FichaEscalafonaria) query.uniqueResult();
            t.commit();

        } catch (Exception e) {
            t.rollback();
            System.out.println("No se pudo  \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo  \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return fichaEscalafonaria;

    }
}
