/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sad;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.PrestamoSerieDocumental;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;
/**
 *
 * @author Jeferson
 */
public interface PrestamoSerieDAO extends GenericDao<PrestamoSerieDocumental> {
    
    public List<PrestamoSerieDocumental> buscarPorCodigo(int codigo);
    public List<PrestamoSerieDocumental> listarPrestamos();
}
