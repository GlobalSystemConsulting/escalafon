/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.exposicion_ponencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.ExposicionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Exposicion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarExposicionesTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarExposicionesTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer perId = requestData.getInt("perId");
                
        List<Exposicion> expPon = null;
        ExposicionDao expPonDao = (ExposicionDao)FactoryDao.buildDao("se.ExposicionDao");
        
        try{
            expPon = expPonDao.listarxFichaEscalafonaria(perId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar exposiciones",e);
            System.out.println("No se pudo listar las exposiciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar las exposiciones", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        JSONArray miArray = new JSONArray();
        for(Exposicion eyp:expPon ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("expId", eyp.getExpId());
            oResponse.put("des", eyp.getDes()==null?"":eyp.getDes());
            oResponse.put("insOrg", eyp.getInsOrg()==null?"":eyp.getInsOrg());
            oResponse.put("tipPar", eyp.getTipPar()==null?"":eyp.getTipPar());
            oResponse.put("fecIni", eyp.getFecIni()==null?"":sdi.format(eyp.getFecIni()));
            oResponse.put("fecTer", eyp.getFecTer()==null?"":sdi.format(eyp.getFecTer()));
            oResponse.put("horLec", eyp.getHorLec()==null?0:eyp.getHorLec());
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las exposiciones fueron listadas exitosamente", miArray);
    }
    
}
