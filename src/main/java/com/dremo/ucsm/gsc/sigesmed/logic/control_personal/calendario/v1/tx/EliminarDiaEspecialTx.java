/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.calendario.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.CalendarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.DiasEspeciales;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author carlos
 */
public class EliminarDiaEspecialTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {

        
        DiasEspeciales nuevoDiaEsp = null;
       
        try {
            JSONObject requestData = (JSONObject) wr.getData();

            JSONObject dia_ = requestData.getJSONObject("dia");         
           
            Integer id = dia_.getInt("id");
            nuevoDiaEsp = new DiasEspeciales(id);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar los datos del dia", e.getMessage());
        }
        try {
            CalendarioDao calendarioDao = (CalendarioDao) FactoryDao.buildDao("cpe.CalendarioDao");
            calendarioDao.delete(nuevoDiaEsp);
           

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo registrar el dia ", e.getMessage());
        }

        JSONObject oResponse = new JSONObject();
        

        return WebResponse.crearWebResponseExito("Se Elimino correctamente", oResponse);
        //Fin
    }

}
