package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import java.util.Date;

public class HorarioDetalleModel {

    public int grado;
    public char seccion;
    
    public int horDetId;
    
    public Date horIni;
    public Date horFin;
    
    public int horarioId;
    
    public int aulId;
    
    public char diaId;
    
    public int plaMagId;
    
    public int areCurId;

    public HorarioDetalleModel() {
    }
    public HorarioDetalleModel(int horDetId) {
        this.horDetId = horDetId;
    }
    public HorarioDetalleModel(int grado, char seccion,int horDetId,Date horIni,Date horFin,int horarioId,int plaMagId,int areCurId,int aulId, char diaId) {
        
        this.grado = grado;
        this.seccion = seccion;
        
       this.horDetId = horDetId;
       
       this.horIni = horIni;
       this.horFin = horFin;
       
       this.horarioId = horarioId;
       this.plaMagId = plaMagId;
       this.areCurId = areCurId;
       this.aulId = aulId;
       this.diaId = diaId;
    }
     
    public int getHorDetId() {
        return this.horDetId;
    }    
    public void setHorDetId(int horDetId) {
        this.horDetId = horDetId;
    }
    
    public Date getHorIni() {
        return this.horIni;
    }
    public void setHorIni(Date horIni) {
        this.horIni = horIni;
    }
    
    public Date getHorFin() {
        return this.horFin;
    }
    public void setHorFin(Date horFin) {
        this.horFin = horFin;
    }
    
    public int getHorarioId() {
        return this.horarioId;
    }    
    public void setHorarioId(int horarioId) {
        this.horarioId = horarioId;
    }
    
    public int getPlaMagId() {
        return this.plaMagId;
    }    
    public void setPlaMagId(int plaMagId) {
        this.plaMagId = plaMagId;
    }
    
    public int getAreCurId() {
        return this.areCurId;
    }    
    public void setAreCurId(int areCurId) {
        this.areCurId = areCurId;
    }
    
    public int getAulId() {
        return this.aulId;
    }    
    public void setAulId(int aulId) {
        this.aulId = aulId;
    }
    
    public char getDisId() {
        return this.diaId;
    }    
    public void setDia(char diaId) {
        this.diaId = diaId;
    }
}


