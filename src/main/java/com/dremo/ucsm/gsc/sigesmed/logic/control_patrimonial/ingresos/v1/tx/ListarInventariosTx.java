/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioInicial;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.InventarioFisico;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioInicialDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.InventarioFisicoDAO;



/**
 *
 * @author Administrador
 */
public class ListarInventariosTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        List<InventarioInicial> ii = null;
        List<InventarioFisico> ifi = null;
        JSONArray miArray = new JSONArray();
        
        try{     
            JSONObject requestData = (JSONObject)wr.getData();
            int org_id = requestData.getInt("org_id");
            
            /*Listamos los Inventarios Iniciales*/
            InventarioInicialDAO inv_ini_dao = (InventarioInicialDAO)FactoryDao.buildDao("scp.InventarioInicialDAO");
            ii = inv_ini_dao.listarInventarioInicial(org_id);
            
            /*Listamos los Inventarios Fisicos*/
            InventarioFisicoDAO inv_fis_dao = (InventarioFisicoDAO)FactoryDao.buildDao("scp.InventarioFisicoDAO");
            ifi = inv_fis_dao.listarInventarioFisico(org_id);
            
            String inv_ini ="Inventario Inicial";
            String inv_fin ="Inventario Fisico";
            for(InventarioInicial i : ii){ 
               JSONObject oResponse = new JSONObject();
               oResponse.put("con_pat_id",i.getCon_pat_id());
               oResponse.put("inv_ini_id",i.getInv_ini_id());
               oResponse.put("fec_mod",i.getFec_mod());
               oResponse.put("fla_cie",String.valueOf(i.getFla_cie()));
               oResponse.put("tipo_inv",inv_ini);
               miArray.put(oResponse); 
            }
            for(InventarioFisico i : ifi){
               JSONObject oResponse = new JSONObject();
               oResponse.put("con_pat_id",i.getCon_pat_id());
               oResponse.put("inv_ini_id",i.getInv_ini_id());
               oResponse.put("fec_mod",i.getFec_mod());
               oResponse.put("fla_cie",String.valueOf(i.getFla_cie()));
               oResponse.put("tipo_inv",inv_fin);
               miArray.put(oResponse); 
            }
            
        }
        catch(Exception e){
            System.out.println("No se pudo Mostrar los Inventarios\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Inventarios", e.getMessage() );  
        }
       
         return WebResponse.crearWebResponseExito("Se Listo Los Inventarios Correctamente",miArray); 
        
        
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
