package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.CargoDeComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoDeComision;
import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;

/**
 * Created on 29/09/16.
 */
public class CargoDeComisionDaoHibernate extends GenericDaoHibernate<CargoDeComision> implements CargoDeComisionDao{
    private static Logger logger = Logger.getLogger(CargoDeComisionDaoHibernate.class.getName());
    @Override
    public void eliminarCargosDeComision(int idCom) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String sql1 = "delete from pedagogico.cargo_de_comision  where com_id = :comision_id";
            SQLQuery query = session.createSQLQuery(sql1);
            query.setParameter("comision_id",idCom);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            logger.log(Level.SEVERE,"eliminarCargosDeComision",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<CargoDeComision> buscarPorCargo(int idCargo) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(CargoDeComision.class)
                    .setFetchMode("cargoComision", FetchMode.JOIN)
                    .createCriteria("cargoComision").add(Restrictions.eq("carComId", idCargo));
            
            /*String hql = "SELECT cdc FROM CargoDeComision cc INNER JOIN FETCH cc.cargoComision c WHERE c.carComId =:var";
            SQLQuery query = session.createSQLQuery(hql);
            query.setInteger("var", idCargo);
            return query.list();*/
            return criteria.list();
            
        }
        catch(Exception e){
            throw e;
        }finally{
            session.close();
        }
    }

    @Override
    public void eliminarCargoComision(int cargoAnterior) {
       Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        try{
            String sql1 = "DELETE FROM  pedagogico.cargo_de_comision WHERE car_com_id = :acar";
            SQLQuery query = session.createSQLQuery(sql1);
            //query.setInteger("ncar",cargoNuevo);
            query.setInteger("acar",cargoAnterior);
            query.executeUpdate();
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            logger.log(Level.SEVERE,"actualizarCargoComision",e);
            throw e;
        }finally {
            session.close();
        }
    }
}
