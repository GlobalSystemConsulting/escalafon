package com.dremo.ucsm.gsc.sigesmed.core.dao.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.CargoComision;

/**
 * Created by geank on 28/09/16.
 */
public interface CargoComisionDao extends GenericDao<CargoComision> {
    CargoComision buscarPorId(int id);
    CargoComision buscarCargoDefecto();
    CargoComision buscarCargoPorNombre(String nombre);
}
