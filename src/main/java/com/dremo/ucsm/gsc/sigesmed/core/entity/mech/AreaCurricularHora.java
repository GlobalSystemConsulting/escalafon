package com.dremo.ucsm.gsc.sigesmed.core.entity.mech;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="area_curricular_hora" )
public class AreaCurricularHora  implements java.io.Serializable {

    @Id
    @Column(name="are_cur_hor_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_areacurricularhora", sequenceName="area_curricular_hora_are_cur_hor_id_seq" )
    @GeneratedValue(generator="secuencia_areacurricularhora")
    private int areHorId;
    
    @Column(name="hor_obl")
    private int horObl;
    
    @Column(name="jor_esc_id")
    private int jorEscId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="jor_esc_id",updatable = false,insertable = false)
    private JornadaEscolar jornada;
    
    @Column(name="gra_id")
    private int graId;    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="gra_id",updatable = false,insertable = false)
    private Grado grado;
    
    @Column(name="are_cur_id")
    private int areCurId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_cur_id",updatable = false,insertable = false)
    private AreaCurricular area;

    public AreaCurricularHora() {
    }
    public AreaCurricularHora(int areHorId) {
        this.areHorId = areHorId;
    }
    public AreaCurricularHora(int areHorId, int jorEscId, int graId, int areCurId, int hora) {
       this.areHorId = areHorId;
       this.jorEscId = jorEscId;       
       this.graId = graId;
       this.areCurId = areCurId;
       this.horObl = hora;
    }
   
     
    public int getAreHorId() {
        return this.areHorId;
    }    
    public void setAreHorId(int areHorId) {
        this.areHorId = areHorId;
    }
    
    public int getHorObl() {
        return this.horObl;
    }    
    public void setHorObl(int horObl) {
        this.horObl = horObl;
    }
    
    public int getGraId() {
        return this.graId;
    }    
    public void setGraId(int graId) {
        this.graId = graId;
    }
    
    public Grado getGrado() {
        return this.grado;
    }
    public void setGrado(Grado grado) {
        this.grado = grado;
    }
    
    public int getJorEscId() {
        return this.jorEscId;
    }    
    public void setJorEscId(int jorEscId) {
        this.jorEscId = jorEscId;
    }
    
    public JornadaEscolar getJornada() {
        return this.jornada;
    }
    public void setJornada(JornadaEscolar jornada) {
        this.jornada = jornada;
    }
    
    public int getAreCurId() {
        return this.areCurId;
    }    
    public void setAreCurId(int areCurId) {
        this.areCurId = areCurId;
    }
    
    public AreaCurricular getArea() {
        return this.area;
    }
    public void setArea(AreaCurricular area) {
        this.area = area;
    }
}


