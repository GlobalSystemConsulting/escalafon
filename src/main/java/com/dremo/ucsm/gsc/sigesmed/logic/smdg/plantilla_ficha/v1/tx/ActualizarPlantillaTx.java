/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.plantilla_ficha.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaFichaInstitucionalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaTipo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ActualizarPlantillaTx implements ITransaction{
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
                
        int plaid = requestData.optInt("plaId");
        String planom = requestData.optString("plaNom");
        int platip = requestData.getInt("plaTip");
        String placod = requestData.optString("plaCod");
        
        PlantillaFichaInstitucionalDao plantillaDao = (PlantillaFichaInstitucionalDao)FactoryDao.buildDao("smdg.PlantillaFichaInstitucionalDao");
        PlantillaFichaInstitucional plantilla = plantillaDao.getPlantilla(plaid);
        
        plantilla.setPfiNom(planom);
        plantilla.setTipo(new PlantillaTipo(platip));
        plantilla.setPfiCod(placod);
        
        plantillaDao.update(plantilla);
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("plaId",plantilla.getPfiInsId());
        
        return WebResponse.crearWebResponseExito("El actualizo la plantilla ", oResponse);
    }
}
