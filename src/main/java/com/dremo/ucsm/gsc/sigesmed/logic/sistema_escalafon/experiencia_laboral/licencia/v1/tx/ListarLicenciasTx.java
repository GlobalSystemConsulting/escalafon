/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.licencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LicenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Licencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarLicenciasTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ListarLicenciasTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<Licencia> licencias = null;
        LicenciaDao licenciaDao = (LicenciaDao)FactoryDao.buildDao("se.LicenciaDao");
        
        CategoriaDao categoriaDao =(CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
        Categoria categoria=null;
        CargoDao cargoDao =(CargoDao)FactoryDao.buildDao("se.CargoDao");
        Cargo cargo=null;
        
        OrganigramaDao organigramaDao =(OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
        Organigrama organigrama=null;
        
        UbicacionDao ubiDao =(UbicacionDao)FactoryDao.buildDao("se.UbicacionDao");
        Ubicacion ubi = null;
        
        try{
            licencias = licenciaDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar licencias",e);
            System.out.println("No se pudo listar las licencias\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar las licencias", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
        JSONArray miArray = new JSONArray();
        for(Licencia l:licencias){
            JSONObject oResponse = new JSONObject();
             if(l.getLicId() != null)
                oResponse.put("licId", l.getLicId());
            else
                oResponse.put("licId", -1);

            if(l.getEntEmiRes()!=null)
                oResponse.put("entEmi", l.getEntEmiRes());
            else
                oResponse.put("entEmi", "");

            if(l.getTipDocId()!=null)
                oResponse.put("tipDocIdMain", l.getTipDocId());
            else
                oResponse.put("tipDocIdMain", 0);

            if(l.getNumDoc()!=null)
                oResponse.put("numInfMain", l.getNumDoc());
            else
                oResponse.put("numInfMain", "");

            if(l.getFecDoc()!=null)
                oResponse.put("fecInfMain", sdo.format(l.getFecDoc()));
            else
                oResponse.put("fecInfMain", "");

            if(l.getFecRecInf()!=null)
                oResponse.put("fecRecInfMain", sdo.format(l.getFecRecInf()));
            else
                oResponse.put("fecRecInfMain", "");

            if(l.getTipLicId()!=null)
                oResponse.put("tipLicId", l.getTipLicId());
            else
                oResponse.put("tipLicId", 0);

            if(l.getEstGoc()!=null)
                oResponse.put("estGoc", l.getEstGoc());
            else
                oResponse.put("estGoc", false);

            if(l.getFecIni()!=null)
                oResponse.put("fecIni", sdo.format(l.getFecIni()));
            else
                oResponse.put("fecIni", "");

            if(l.getFecTer()!=null)
                oResponse.put("fecTer", sdo.format(l.getFecTer()));
            else
                oResponse.put("fecTer", "");
            //datos de informe
            if(l.getTipDocInfId()!=null)
                oResponse.put("tipDocIdSec", l.getTipDocInfId());
            else
                oResponse.put("tipDocIdSec", 0);

            if(l.getNumInf()!=null)
                oResponse.put("numDocSec", l.getNumInf());
            else
                oResponse.put("numDocSec", "");

            if(l.getFecInf()!=null)
                oResponse.put("fecDocSec", sdo.format(l.getFecInf()));
            else
                oResponse.put("fecDocSec", "");

            //memo
            if(l.getTipDocMemId()!=null)
                oResponse.put("tipDocIdMemo", l.getTipDocMemId());
            else
                oResponse.put("tipDocIdMemo", 0);

            if(l.getNumMem()!=null)
                oResponse.put("numInfMemo", l.getNumMem());
            else
                oResponse.put("numInfMemo", "");

            if(l.getFecDocMem()!=null)
                oResponse.put("fecInfMemo", sdo.format(l.getFecDocMem()));
            else
                oResponse.put("fecInfMemo", "");

            if(l.getFecReiMem()!=null)
                oResponse.put("fecRecInMemo", sdo.format(l.getFecReiMem()));
            else
                oResponse.put("fecRecInMemo", "");


            //otros
            if(l.getMotLic()!=null)
                oResponse.put("mot", l.getMotLic());
            else
                oResponse.put("mot", "");


            oResponse.put("tip", l.getTip());
            
            if(l.getAnioPro()!=null)
                oResponse.put("anioPro", l.getAnioPro());
            else
                oResponse.put("anioPro", "");

            if(l.getMesPro()!=null)
                oResponse.put("mesPro", l.getMesPro());
            else
                oResponse.put("mesPro", "");
            
            if(l.getDiaPro()!=null)
                oResponse.put("diaPro", l.getDiaPro());
            else
                oResponse.put("diaPro", "");
            
            if(l.getEstProId()!=null)
                oResponse.put("estProId", l.getEstProId());
            else
                oResponse.put("estProId", 0);

            if(l.getOrgiActId()!=null && l.getOrgiActId()>0){
                organigrama=organigramaDao.obtenerDetalle(l.getOrgiActId());
                oResponse.put("orgiActId", l.getOrgiActId());
                oResponse.put("datOrgiId", l.getCatActId());
                oResponse.put("nomDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getNomDatOrgi());
                oResponse.put("anioDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getAnioDatOrgi());
                oResponse.put("nomUbi", organigrama.getUbiId());
                
                if(organigrama.getUbiId() != null && organigrama.getUbiId()>0){
                    ubi = ubiDao.buscarPorId(organigrama.getUbiId());
                    oResponse.put("nomUbi", ubi.getDes());
                }else{
                    oResponse.put("nomUbi", "");
                }
                oResponse.put("nomOrgiN3", organigrama.getNomOrgi());
                oResponse.put("nomOrgiN2", organigrama.getOrganigramaPadre().getNomOrgi());
                oResponse.put("nomOrgiN1", organigrama.getOrganigramaPadre().getOrganigramaPadre().getNomOrgi());
            }
            else{
                oResponse.put("orgiActId", 0);
                oResponse.put("datOrgiId", 0);
                oResponse.put("nomOrgi", "");
                oResponse.put("nomDatOrgi", "");
                oResponse.put("anioDatOrgi", "");
                oResponse.put("nomUbi", "");
            }

            if(l.getCatActId()!=null && l.getCatActId()>0){
                categoria=categoriaDao.obtenerDetalle(l.getCatActId());
                oResponse.put("catActId", l.getCatActId());
                oResponse.put("nomCat", categoria.getNomCat());
                oResponse.put("nomPla", categoria.getPlanilla().getNomPla());
            }
            else{
                oResponse.put("catActId", 0);
                oResponse.put("nomCat", "");
                oResponse.put("nomPla", "");
            }

            if(l.getCarActId()!=null && l.getCarActId()>0){
                cargo=cargoDao.buscarPorId(l.getCarActId());
                oResponse.put("carActId", l.getCarActId());
                oResponse.put("nomCar", cargo.getNomCar());            
            }
            else{
                oResponse.put("carActId", 0);
                oResponse.put("nomCar", "");
            }
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Las licencias fueron listados exitosamente", miArray);
    }
    
}
