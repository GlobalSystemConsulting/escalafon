/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.NivelEstudioComplementarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.NivelEstudioComplementario;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author alex
 */
public class NivelEstudioComplementarioDaoHibernate extends GenericDaoHibernate<NivelEstudioComplementario> implements NivelEstudioComplementarioDao{
    @Override
    public List<NivelEstudioComplementario> listarAll() {
        List<NivelEstudioComplementario> nivelesEstCom = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT t from NivelEstudioComplementario as t "
                    + "WHERE t.estReg='A'";
            Query query = session.createQuery(hql);
            nivelesEstCom = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los niveles de estudio complementario\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los niveles de estudio complementario\\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return nivelesEstCom;
    }

    @Override
    public NivelEstudioComplementario buscarPorId(Integer nivEstComId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        NivelEstudioComplementario des = (NivelEstudioComplementario)session.get(NivelEstudioComplementario.class, nivEstComId);
        session.close();
        return des;
    }
}
