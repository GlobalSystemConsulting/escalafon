/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.empresas.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.empresas.v1.tx.ActualizarEmpresaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.empresas.v1.tx.BuscarEmpresaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.empresas.v1.tx.EliminarEmpresaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.empresas.v1.tx.InsertarEmpresaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.empresas.v1.tx.ListarEmpresasTx;


/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_CONFIGURACION);        
        
        //Registrando el Nombre del componente
        component.setName("empresas");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarEmpresas", ListarEmpresasTx.class);
        component.addTransactionGET("buscarEmpresa", BuscarEmpresaTx.class);
        component.addTransactionPOST("insertarEmpresa", InsertarEmpresaTx.class);
        component.addTransactionDELETE("eliminarEmpresa", EliminarEmpresaTx.class);
        component.addTransactionPUT("actualizarEmpresa", ActualizarEmpresaTx.class);
        
        return component;
    }
}
