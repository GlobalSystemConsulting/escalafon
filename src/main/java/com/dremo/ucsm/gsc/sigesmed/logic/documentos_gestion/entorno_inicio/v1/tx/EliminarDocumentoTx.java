/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.io.File;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class EliminarDocumentoTx implements ITransaction{

    int coddoc = 0;
    @Override
    public WebResponse execute(WebRequest wr) {
        /* Lectura de Datos*/
        try{
        JSONObject requestData = (JSONObject)wr.getData();
        coddoc = requestData.getInt("idDoc");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("Error de Lectura de datos enviados"+e.toString());
        }
        
        /*Interaccion con Base de Datos*/
        ItemFileDao iteFilDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
        ItemFile docActual = iteFilDao.buscarPorID(coddoc);
        iteFilDao.delete(docActual);
        
        //Una vez eliminado el archivo de la base pasamos a eliminar fisicamente
        //Este documento debe considerar su extension y su formato de (PDF, XLS, DOCX, )
        
        File filetoDelete = new File(docActual.getIteUrlDes()+"/"+docActual.getIteNom());
        
        return WebResponse.crearWebResponseExito("Se envio a papelera exitosamente");
    }
    
}
