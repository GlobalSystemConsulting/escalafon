package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AsistenciaParticipanteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CapacitadorCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

public class GenerarCertificadosTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(GenerarCertificadosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            JSONArray part = data.getJSONArray("parts");
            int capCod = data.getInt("capCod");
            int sedCod = data.getInt("sedCod");

            CursoCapacitacionDao cursoCapacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            CursoCapacitacion course = cursoCapacitacionDao.buscarPorId(capCod);
            String curNom = course.getNom();
            String orgOrg = course.getOrganizacion().getNom();

            SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
            SedeCapacitacion sede = sedeCapacitacionDao.buscarSedeReporte(sedCod);

            String caps = "";
            for (CapacitadorCursoCapacitacion capacitador : sede.getCapacitadoresCursoCapacitacion()) {
                caps += ", " + capacitador.getPer().getApePat() + " " + capacitador.getPer().getApeMat() + " " + capacitador.getPer().getNom();
            }

            caps = caps.substring(1);

            String fecIni = getDate(sede.getCronograma().getFecIni());
            String fecFin = getDate(sede.getCronograma().getFecFin());
            String fecAct = getDate(new Date());

            AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");

            JSONObject response = new JSONObject();
            String path = ServicioREST.PATH_SIGESMED + File.separator + "ReportesTemp" + File.separator;
            Path pathAbs = Paths.get(path);
            
            if (!Files.exists(pathAbs)) {
                Files.createDirectories(pathAbs);
            }
            
            if (part.length() == 1) {
                String nomPar = part.getJSONObject(0).getString("nom");
                String numHor = asistenciaParticipanteDao.contarHoras(sedCod, part.getJSONObject(0).getInt("cod"));
                String nomFile = generateCertificate(path, orgOrg, nomPar, curNom, caps, fecIni, fecFin, numHor, fecAct);
                
                Path pathFile = Paths.get(nomFile);
                byte[] array = Files.readAllBytes(pathFile);
                response.put("route", "data:application/pdf;base64," + Base64.encodeBase64String(array));
            } else {
                String zipNom = path + String.valueOf((new Date()).getTime()) + ".zip";
                File file = new File(zipNom);
                FileOutputStream fileOut = new FileOutputStream(file);
                ZipOutputStream fileZip = new ZipOutputStream(fileOut);

                for (int i = 0; i < part.length(); i++) {
                    String nomPar = part.getJSONObject(i).getString("nom");
                    String numHor = asistenciaParticipanteDao.contarHoras(sedCod, part.getJSONObject(i).getInt("cod"));
                    String nomFile = generateCertificate(path, orgOrg, nomPar, curNom, caps, fecIni, fecFin, numHor, fecAct);
                    
                    addToZipFile(nomFile, fileZip);
                }

                fileZip.close();
                fileOut.close();   
                
                Path pathFile = Paths.get(zipNom);
                byte[] array = Files.readAllBytes(pathFile);
                response.put("route", "data:application/zip;base64," + Base64.encodeBase64String(array));
            }

            return WebResponse.crearWebResponseExito("Los certificados fueron generados correctamente", WebResponse.OK_RESPONSE).setData(response);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "generarCertificados", e);
            return WebResponse.crearWebResponseError("No se pudo generar los certificados", WebResponse.BAD_RESPONSE);
        }
    }

    private String generateCertificate(String path, String orgOrg, String nomPar, String curNom, String caps, String fecIni, String fecFin, String numHor, String fecAct) {
        String nomFile = "";

        try {
            int[] margens = {2, 2, 2, 2};
            nomFile = String.valueOf((new Date()).getTime()) + ".pdf";

            Mitext document = new Mitext(nomFile, margens, true, "");
            document.newLine(5);
            document.setStyle(1, 40, true, false, false);
            document.agregarParrafoMyEstilo("UNIDAD DE GESTIÓN " + orgOrg, 1);
            document.newLine(1);
            document.setStyle(1, 25, false, false, false);
            document.agregarParrafoMyEstilo("VINCULACIÓN CON LA SOCIEDAD", 1);
            document.newLine(1);
            document.setStyle(1, 20, true, false, false);
            document.agregarParrafoMyEstilo("Confiere el presente certificado de asistencia al Sr.(a)(ita):", 1);
            document.newLine(2);
            document.setStyle(1, 25, true, false, false);
            document.agregarParrafoMyEstilo(nomPar.toUpperCase(), 1);
            document.newLine(1);
            document.setStyle(1, 15, false, false, false);
            document.agregarParrafoMyEstilo("Al curso de capacitación '" + curNom + " guiado por el/los docente(s)" + caps.toUpperCase()
                    + "; realizado desde el " + fecIni + " al " + fecFin + " por la Unidad de Gestión " + orgOrg + " (" + numHor + " horas)", 2);
            document.newLine(1);
            document.setStyle(1, 13, false, false, false);
            document.agregarParrafoMyEstilo("Perú. " + fecAct, 4);
            document.cerrarDocumento();

            nomFile = path + nomFile;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "generateCertificate", e);
        }
        return nomFile;
    }

    private String getDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String result = calendar.get(Calendar.DAY_OF_MONTH) + " ";

        switch (calendar.get(Calendar.MONTH)) {
            case 0:
                result += "de Enero del ";
                break;
            case 1:
                result += "de Febrero del ";
                break;
            case 2:
                result += "de Marzo del ";
                break;
            case 3:
                result += "de Abril del ";
                break;
            case 4:
                result += "de Mayo del ";
                break;
            case 5:
                result += "de Junio del ";
                break;
            case 6:
                result += "de Julio del ";
                break;
            case 7:
                result += "de Agosto del ";
                break;
            case 8:
                result += "de Septiembre del ";
                break;
            case 9:
                result += "de Octubre del ";
                break;
            case 10:
                result += "de Noviembre del ";
                break;
            case 11:
                result += "de Diciembre del ";
                break;
        }

        result += calendar.get(Calendar.YEAR);

        return result;
    }

    public static void addToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {
        File file = new File(fileName);
        FileInputStream fis = new FileInputStream(file);
        ZipEntry zipEntry = new ZipEntry(file.getName());
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }
}
