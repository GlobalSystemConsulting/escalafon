/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import static com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.MepReportDaoHibernate.logger;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ListaUtilesDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ListaUtiles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Seccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Nivel;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.Comision;
import java.util.List;
import java.util.logging.Level;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author ucsm
 */
public class ListaUtilesDaoHibernate extends GenericDaoHibernate<ListaUtiles> implements ListaUtilesDao{

    @Override
    public ListaUtiles buscarPorId(int id) {
        ListaUtiles objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT u FROM ListaUtiles u INNER JOIN FETCH u.nivel INNER JOIN FETCH u.area INNER JOIN FETCH u.grado INNER JOIN FETCH u.seccion  INNER JOIN FETCH u.usuId usu WHERE u.listUtiId =:idLis";
        
            Query query = session.createQuery(hql);
            query.setParameter("idLis", id);
            query.setMaxResults(1);
            //buscando 
            objeto =  (ListaUtiles)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar la lista \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar la lista \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }

    @Override
    public List<ListaUtiles> listarListasPorOrganizacion(int idOrganizacion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria query = session.createCriteria(ListaUtiles.class);
        query.setFetchMode("organizacion", FetchMode.JOIN);
        try {
            List<ListaUtiles> listas = null;
            Organizacion org = (Organizacion) session.get(Organizacion.class,idOrganizacion);
            logger.log(Level.INFO,"valors {0}",org.getTipoOrganizacion().getCod());
            String tipo = org.getTipoOrganizacion().getCod().toUpperCase().trim();
            if(tipo.equals("UGEL")){
                query.createCriteria("organizacion")
                        .createCriteria("organizacionPadre")
                        .add(Restrictions.eq("orgId",idOrganizacion));
            }else if(tipo.equals("IE")){
                query.createCriteria("organizacion")
                        .add(Restrictions.eq("orgId",idOrganizacion));
            }
            //si es dremo se devuelve todas las comisiones, no hay restricciones

            query.add(Restrictions.eq("estReg",'A'));
            listas = query.list();
            return listas;

        }catch (HibernateException e){
            logger.log(Level.SEVERE,"listarComisionesPorOrganizacion",e);
            throw e;
        }finally {
            session.close();
        }
    }
    
    @Override
    public List<ListaUtiles> listarListasPorUsuario(int idUsuario) {
        
        List<ListaUtiles> listas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{

            String hql = "SELECT l FROM ListaUtiles l INNER JOIN l.usuario t INNER JOIN t.persona p WHERE p.perId =:idUs  and l.estReg!='E'";
            Query query = session.createQuery(hql);
            query.setInteger("idUs", idUsuario);
            listas = query.list();
        }catch(Exception e){
            System.out.println("No se pudo Listar las listas de utiles" );
            throw e;            
        }
        finally{  
        }
        
        return listas;
    }
    @Override
    public List<ListaUtiles> listarListasPorOrganizacionUsuario(int idOrganizacion, int idUsuario) {
        List<ListaUtiles> listas = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT l FROM ListaUtiles l INNER JOIN FETCH l.area a INNER JOIN FETCH l.grado g INNER JOIN FETCH l.nivel n INNER JOIN l.usuId u WHERE u.usuId =:idUs and l.estReg!='E'";
            Query query = session.createQuery(hql);
            query.setInteger("idUs", idUsuario);
            listas = query.list();
        }catch(Exception e){
            System.out.println("No se pudo Listar las listas de utiles" );
            throw e;            
        }
        finally{  
        }
        
        return listas;
    }
    
    

    @Override
    public Nivel buscarNivelPorId(int id) {
        Nivel objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT u FROM Nivel u WHERE u.nivId =:idUser";
        
            Query query = session.createQuery(hql);
            query.setParameter("idUser", id);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Nivel)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el nivel \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el nivel \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }

    @Override
    public AreaCurricular buscarAreaCurricularPorId(int id) {
        AreaCurricular objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT u FROM AreaCurricularMaestro u WHERE u.areCurId =:idArea";
        
            Query query = session.createQuery(hql);
            query.setParameter("idArea", id);
            query.setMaxResults(1);
            //buscando 
            objeto =  (AreaCurricular)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el area \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el area \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }

    @Override
    public Grado buscarGradoPorId(int id) {
        Grado objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT u FROM Grado u WHERE u.graId =:idGrado";
        
            Query query = session.createQuery(hql);
            query.setParameter("idGrado", id);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Grado)query.uniqueResult(); 
        
        }catch(Exception e){
            System.out.println("No se pudo encontrar el nivel \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo encontrar el nivel \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return objeto;
    }

    @Override
    public Seccion buscarSeccionPorId(char id) {
        Seccion objeto = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT DISTINCT u FROM SeccionMaestro u WHERE u.sedId =:idSec";
        
            Query query = session.createQuery(hql);
            query.setParameter("idSec", id);
            query.setMaxResults(1);
            //buscando 
            objeto =  (Seccion)query.uniqueResult(); 
        
        }catch(Exception e){
       
            throw e;
        }
        finally{
            session.close();
        }
        return objeto;
    }
}
