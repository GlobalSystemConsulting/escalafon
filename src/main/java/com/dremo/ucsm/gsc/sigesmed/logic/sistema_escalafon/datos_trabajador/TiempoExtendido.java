/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_trabajador;

public class TiempoExtendido {
    
    private Boolean estado;
    private long anios;
    private long meses;
    private long dias;
    private long totalDias;

    public TiempoExtendido() {
        this.estado = false;
    }

    public TiempoExtendido(long anios, long meses, long dias) {
        this.estado = false;
        this.anios = anios;
        this.meses = meses;
        this.dias = dias;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
    
    public long getAnios() {
        return anios;
    }

    public void setAnios(long anios) {
        this.anios = anios;
    }

    public long getMeses() {
        return meses;
    }

    public void setMeses(long meses) {
        this.meses = meses;
    }

    public long getDias() {
        return dias;
    }

    public void setDias(long dias) {
        this.dias = dias;
    }

    public long getTotalDias() {
        return totalDias;
    }

    public void setTotalDias(long totalDias) {
        this.totalDias = totalDias;
    }
    
    @Override
    public String toString() {
        return "TiempoExtendido{" + "anios=" + anios + ", meses=" + meses + ", dias=" + dias + '}';
    }
    
    public void sumar(TiempoExtendido input){
        this.anios += input.getAnios();
        this.meses += input.getMeses();
        this.dias += input.getDias();
        verificarYCorregir();
    }
    
    public void restar(TiempoExtendido input){
        this.totalDias-=input.getTotalDias();
        formatearDias();
    }
    
    private void verificarYCorregir(){
        if(this.dias>=30){
            this.dias=this.dias-30;
            this.meses++;
        }
        
        if(this.meses>=12){
            this.meses=this.meses-12;
            this.anios++;
        }
    }
    
    private void formatearDias(){
        long days, months,years, aux;
        
        days = this.totalDias%30;
        aux = this.totalDias/30;
        months = aux%12;
        years = aux/12;
        
        this.dias = days;
        this.meses = months;
        this.anios = years;
    }
        
    /*public static void main(String[] args) {
        TiempoExtendido a=new TiempoExtendido(8,9,29);
        a.setTotalDias(3219);
        System.out.println(a.getTotalDias());
        TiempoExtendido b=new TiempoExtendido(1,8,1);
        b.setTotalDias(606);
        a.restar(b);
        
        System.out.println(a);
    }*/
}
