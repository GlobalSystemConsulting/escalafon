/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.desplazamiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class AgregarDesplazamientoByTipoTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(AgregarDesplazamientoByTipoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        Desplazamiento desplazamiento = null;
        Trabajador trabajador = null;
        int ficEscId = 0;
        int traId = 0;
        int opcion = 0;
        int tipDocId = 0;
        int jorLabId = 0;
        char tip = ' ';

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            System.out.println("data: " +  requestData.toString());
            traId = requestData.getInt("traId");
            opcion = requestData.getInt("opcion");

            ficEscId = requestData.getJSONObject("desplazamiento").getInt("ficEscId");
            tip = requestData.getJSONObject("desplazamiento").getString("tip").charAt(0);
            tipDocId = requestData.getJSONObject("desplazamiento").getInt("tipDocId");

            desplazamiento = new Desplazamiento(tip, new FichaEscalafonaria(ficEscId), tipDocId);

            switch (opcion) {
                case 2:
                    String numDoc = requestData.getJSONObject("desplazamiento").getString("numDoc");
                    Date fecDoc = null;
                    if (requestData.getJSONObject("desplazamiento").getString("fecDoc").length() > 0) {
                        fecDoc = sdi.parse(requestData.getJSONObject("desplazamiento").getString("fecDoc").substring(0, 10));
                    }
                    jorLabId = requestData.getJSONObject("desplazamiento").getInt("jorLabId");
                    Date fecIni = sdi.parse(requestData.getJSONObject("desplazamiento").getString("fecIni").substring(0, 10));

                    desplazamiento.setNumDoc(numDoc);
                    desplazamiento.setFecDoc(fecDoc);
                    desplazamiento.setJorLabId(jorLabId);
                    desplazamiento.setFecIni(fecIni);

                    break;

                case 3:
                    String numDocTer = "";
                    if (requestData.getJSONObject("desplazamiento").getString("numDocTer").length() > 0) {
                        numDocTer = requestData.getJSONObject("desplazamiento").getString("numDocTer");
                    }

                    Date fecDocTer = null;
                    if (requestData.getJSONObject("desplazamiento").getString("fecDocTer").length() > 0) {
                        fecDocTer = sdi.parse(requestData.getJSONObject("desplazamiento").getString("fecDocTer").substring(0, 10));
                    }

                    Date fecTer = null;
                    if (requestData.getJSONObject("desplazamiento").getString("fecTer").length() > 0) {
                        fecTer = sdi.parse(requestData.getJSONObject("desplazamiento").getString("fecTer").substring(0, 10));
                    }

                    String motRet = "";
                    if (requestData.getJSONObject("desplazamiento").getString("motRet").length() > 0) {
                        motRet = requestData.getJSONObject("desplazamiento").getString("motRet");
                    }

                    desplazamiento.setNumDocTer(numDocTer);
                    desplazamiento.setFecDocTer(fecDocTer);
                    desplazamiento.setJorLabId(jorLabId);
                    desplazamiento.setFecTer(fecTer);
                    desplazamiento.setMotRet(motRet);

                    break;
            }
            
            int orgiId = requestData.getInt("orgiId");
            int carId = requestData.getInt("carId");
            int catId = requestData.getInt("catId");
            
            desplazamiento.setOrgiActId(orgiId);
            desplazamiento.setCarActId(carId);
            desplazamiento.setCatActId(catId);

            desplazamiento.setUsuMod(wr.getIdUsuario());
            desplazamiento.setFecMod(new Date());
            desplazamiento.setEstReg('A');

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE, "Datos nuevo desplazamiento", e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        DesplazamientoDao desDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
        try {
            desDao.insert(desplazamiento);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego desplazamiento", "Detalle: " + ficEscId + " ficha ", wr.getIdUsuario(), "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Agregar nuevo desplazamiento", e);
            System.out.println(e);
        }

        TrabajadorDao traDao = (TrabajadorDao) FactoryDao.buildDao("se.TrabajadorDao");
        try {
            trabajador = traDao.buscarPorId(traId);
            trabajador.setEstLab(tip);
            traDao.update(trabajador);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se modifico trabajador", "Detalle: " + traId + " trabajador ", wr.getIdUsuario(), "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA/////////////
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Actualizar estado laboral de trabajador", e);
            System.out.println(e);
        }
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("desId", desplazamiento.getDesId());
        oResponse.put("tip", desplazamiento.getTip());
        oResponse.put("tipDes", "");
        switch (desplazamiento.getTip()) {
            case '1':
            case '2':
                oResponse.put("numeroDoc", null != desplazamiento.getNumDoc() ? desplazamiento.getNumDoc() : "");
                oResponse.put("fechaDoc", null != desplazamiento.getFecIni() ? sdo.format(desplazamiento.getFecIni()) : "");
                oResponse.put("fechaDes", null != desplazamiento.getFecIni() ? sdo.format(desplazamiento.getFecIni()) : "");
                break;
            case '3':
                oResponse.put("numeroDoc", null != desplazamiento.getNumDocTer() ? desplazamiento.getNumDocTer() : "");
                oResponse.put("fechaDoc", null != desplazamiento.getFecDocTer() ? sdo.format(desplazamiento.getFecDocTer()) : "");
                oResponse.put("fechaDes", null != desplazamiento.getFecTer() ? sdo.format(desplazamiento.getFecTer()) : "");
                break;
        }
        oResponse.put("numDoc", null != desplazamiento.getNumDoc() ? desplazamiento.getNumDoc() : "");
        oResponse.put("fecDoc", null != desplazamiento.getFecDoc() ? sdo.format(desplazamiento.getFecDoc()) : "");
        oResponse.put("tipDocId", null != desplazamiento.getTipDocId() ? desplazamiento.getTipDocId() : 0);
        oResponse.put("numDocTer", null != desplazamiento.getNumDocTer() ? desplazamiento.getNumDocTer() : "");
        oResponse.put("fecDocTer", null != desplazamiento.getFecDocTer() ? sdo.format(desplazamiento.getFecDocTer()) : "");
        oResponse.put("tipDocTer", null != desplazamiento.getTipDocTer() ? desplazamiento.getTipDocTer() : "");
        oResponse.put("jorLabId", null != desplazamiento.getJorLabId() ? desplazamiento.getJorLabId() : 0);
        oResponse.put("fecIni", null != desplazamiento.getFecIni() ? sdo.format(desplazamiento.getFecIni()) : "");
        oResponse.put("fecTer", null != desplazamiento.getFecTer() ? sdo.format(desplazamiento.getFecTer()) : "");
        oResponse.put("motRet", null != desplazamiento.getFecTer() && desplazamiento.getMotRet().length() > 0 ? desplazamiento.getMotRet() : "");

        return WebResponse.crearWebResponseExito("El registro del desplazamiento se realizo correctamente", oResponse);
        //Fin
    }

}
