/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_area_archivo_central.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.UnidadOrganicaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.UnidadOrganica;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author Jeferson
 */
public class EliminarUnidadOrganicaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int uni_org_id = 0;
        try{
           JSONObject requestData = (JSONObject)wr.getData();
           uni_org_id = requestData.getInt("uni_org_id");
           
        }catch(Exception e){
           System.out.println(e);
           return  WebResponse.crearWebResponseError("No se pudo eliminar Unidad Organica, datos incorrectos", e.getMessage() );
        }
        
        //Operaciones con la Base de Datos
            UnidadOrganicaDAO uni_org_dao = (UnidadOrganicaDAO)FactoryDao.buildDao("UnidadOrganicaDAO");
        try{
            uni_org_dao.delete(new UnidadOrganica(uni_org_id));
        }catch(Exception e){
             System.out.println(e);
             return  WebResponse.crearWebResponseError("No se pudo eliminar Unidad Organica, datos incorrectos", e.getMessage() );
        }                                      
       
         return WebResponse.crearWebResponseExito("La Unidad Organica se elimino correctamente");
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
