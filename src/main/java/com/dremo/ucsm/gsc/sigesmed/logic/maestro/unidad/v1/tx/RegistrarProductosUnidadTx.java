package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProductosUnidadAprendizaje;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 14/12/2016.
 */
public class RegistrarProductosUnidadTx implements ITransaction{
    private Logger logger = Logger.getLogger(RegistrarProductosUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idUnidad = data.getInt("unid");
        String  tipUnidad = data.getString("untip");

        String nom = data.getString("nom").toUpperCase();

        ProductosUnidadAprendizaje producto = new ProductosUnidadAprendizaje(nom);
        return registrarProductoUnidad(idUnidad, tipUnidad, producto);
    }

    private WebResponse registrarProductoUnidad(int idUnidad, String tipUnidad, ProductosUnidadAprendizaje producto) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            UnidadDidactica unidad = unidadDao.buscarUnidadDidactica(idUnidad, tipUnidad);
            producto.setUnidad(unidad);
            unidadDao.registrarProductoUnidad(producto);
            JSONObject jsonProducto = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"proUniAprId", "nom"},
                    new String[]{"id", "nom"},
                    producto
            ));
            jsonProducto.put("edit",false);
            return WebResponse.crearWebResponseExito("Se registro el material",jsonProducto);
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarMaterialUnidad",e);
            return WebResponse.crearWebResponseError("No se puede registrar el material");
        }
    }
}
