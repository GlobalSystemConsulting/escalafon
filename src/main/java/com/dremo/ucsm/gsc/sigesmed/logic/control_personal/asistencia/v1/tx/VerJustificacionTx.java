/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.asistencia.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.RegistroAsistencia;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.DateUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author carlos
 */
public class VerJustificacionTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        Integer asistenciaId = 0;
        Date fechaAsistencia;
        Trabajador trabajador;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            asistenciaId = requestData.getInt("regId");      
            String fecha=requestData.getString("fecha");
            Integer trabId=requestData.getInt("trabId");
            trabajador=new Trabajador(trabId);
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
            fechaAsistencia=sdfDate.parse(fecha);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("Error al validar los datos" );
        }
        /*
        *  Parte para la operacion en la Base de Datos
        */
        JSONObject oRes = new JSONObject();
        
        try {
            
            SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm a");//dd/MM/yyyy
            List<RegistroAsistencia> registros = null;
            AsistenciaDao asistenciaDao = (AsistenciaDao) FactoryDao.buildDao("cpe.AsistenciaDao");
            
            registros = asistenciaDao.listarRegistroAsistenciaByFecha(trabajador, fechaAsistencia);

            JSONArray miArray = new JSONArray();
            for (RegistroAsistencia reg : registros) {
                JSONObject oRegistro = new JSONObject();
                oRegistro.put("id", reg.getRegAsiId());
                if(reg.getHoraIngreso()== null)
                {
                    oRegistro.put("ingreso", "");
                }
                else
                {
                   oRegistro.put("ingreso", sdfDate.format(reg.getHoraIngreso()));
                }
                
                if (reg.getHoraSalida() == null) {
                    oRegistro.put("salida", "");
                } else {
                    oRegistro.put("salida", sdfDate.format(reg.getHoraSalida()));
                }
                oRegistro.put("estado", reg.getEstAsi());
                JSONObject oJustificacion = new JSONObject();
                
                if(reg.getJustificacion() != null)
                {
                    oJustificacion.put("id", reg.getJustificacion().getJusId());
                    oJustificacion.put("obs", reg.getJustificacion().getJusObs());
                    oJustificacion.put("num", reg.getJustificacion().getJusDoc());
                    oJustificacion.put("estado", reg.getJustificacion().getEstReg());
                }
                else
                {
                    oJustificacion.put("id", "");
                    oJustificacion.put("obs", "");
                    oJustificacion.put("num", "");
                    oJustificacion.put("estado", "");
                }
                
                oRegistro.put("justificacion", oJustificacion);
                miArray.put(oRegistro);
            }
            oRes.put("registros", miArray);

               
            

        } catch (Exception e) {
            System.out.println("\n" + e);
            return WebResponse.crearWebResponseError("No se pudo cargar las asistencias", e.getMessage());
        }
        
        return WebResponse.crearWebResponseExito("Consulta correcta", oRes);
        //validar el usuario
    
    }
}
