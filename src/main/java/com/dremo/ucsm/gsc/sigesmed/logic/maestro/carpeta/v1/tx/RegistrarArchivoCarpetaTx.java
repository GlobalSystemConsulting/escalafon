package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.ContenidoSeccionCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.RutaContenidoCarpetaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ContenidoSeccionCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.RutaContenidoCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import org.json.JSONObject;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 30/12/2016.
 */
public class RegistrarArchivoCarpetaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(RegistrarArchivoCarpetaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        ///datos del banco
        int idCar = data.getInt("carId");
        int idDoc = data.getInt("doc");
        int idOrg = data.getInt("org");
        int idCont = data.getInt("conId");
        int tip = data.getInt("tipUsu");

        String nom = data.optString("nomFil").split("\\.")[0];
        JSONObject filjson = data.optJSONObject("arc");

        int rutId = data.optInt("rutId",-1);
        if(rutId != -1) return editarArchivo(rutId,idCar,idOrg,idDoc,idCont,nom,filjson);
        else return registrarArchivo(idDoc, idCar, tip, idOrg, idCont, nom, filjson);

    }

    private WebResponse editarArchivo(int rutId,int idCar,int idOrg,int idDoc,int idCont, String nom, JSONObject filjson) {
        try{
            RutaContenidoCarpetaDao rutaDao = (RutaContenidoCarpetaDao) FactoryDao.buildDao("maestro.carpeta.RutaContenidoCarpetaDao");
            if(filjson != null && filjson.length() > 0){
                String path = "carpeta_digital_maestro";
                FileJsonObject miF = new FileJsonObject( filjson ,idCar + "_" + idOrg + "_" + idDoc + "_" + idCont + "_" + nom);
                BuildFile.buildFromBase64(path, miF.getName(), miF.getData());
                RutaContenidoCarpeta ruta = rutaDao.buscarRutaPorId(rutId);
                ruta.setNomFil(miF.getName());
                rutaDao.update(ruta);
                return WebResponse.crearWebResponseExito("Se registro correctamente la lectura",
                        new JSONObject().put("pat",ruta.getPat()).put("nomFil",ruta.getNomFil()));
            }
            else
                return WebResponse.crearWebResponseError("No Selecciono ningun archivo");
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarArchivo",e);
            return WebResponse.crearWebResponseError("No se pudo registrar el archivo");
        }
    }

    private WebResponse registrarArchivo(int idDoc,int idArc,int tipUsu, int idOrg, int idCont, String nom, JSONObject filjson) {
        try{
            OrganizacionDao orgDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            ContenidoSeccionCarpetaDao contDao = (ContenidoSeccionCarpetaDao) FactoryDao.buildDao("maestro.carpeta.ContenidoSeccionCarpetaDao");
            RutaContenidoCarpetaDao rutaDao = (RutaContenidoCarpetaDao) FactoryDao.buildDao("maestro.carpeta.RutaContenidoCarpetaDao");
            if(filjson != null && filjson.length() > 0){
                String path = "carpeta_digital_maestro";
                FileJsonObject miF = new FileJsonObject( filjson ,idArc + "_" + idOrg + "_" + idDoc + "_" + idCont + "_" + nom);
                BuildFile.buildFromBase64(path, miF.getName(), miF.getData());
                RutaContenidoCarpeta ruta = new RutaContenidoCarpeta(Sigesmed.UBI_ARCHIVOS+ "/" + path + "/",
                        miF.getName(),tipUsu,contDao.buscarUsuarioporId(idDoc),orgDao.buscarConTipoOrganizacionYPadre(idOrg),contDao.buscarContenidoPorId(idCont));
                rutaDao.insert(ruta);
                return WebResponse.crearWebResponseExito("Se registro correctamente la lectura",
                        new JSONObject().put("rutId", ruta.getRutConCarId()).put("pat", ruta.getPat()).put("nomFil", ruta.getNomFil()));
            }
            else
                return WebResponse.crearWebResponseError("No Selecciono ningun archivo");
        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarArchivo",e);
            return WebResponse.crearWebResponseError("No se pudo registrar el archivo");
        }
    }
}
