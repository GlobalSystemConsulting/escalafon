package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AdjuntoComentarioTemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AdjuntoComentarioTema;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

public class AdjuntoComentarioTemaDaoHibernate extends GenericDaoHibernate<AdjuntoComentarioTema> implements AdjuntoComentarioTemaDao {

    private static final Logger logger = Logger.getLogger(AdjuntoComentarioTemaDaoHibernate.class.getName());
    
    @Override
    public AdjuntoComentarioTema buscarPorId(int adjCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            return (AdjuntoComentarioTema) session.get(AdjuntoComentarioTema.class, adjCod);
        } catch (Exception e){
            logger.log(Level.SEVERE,"buscarPorId",e);
            throw e;
        } finally {
            session.close();
        }
    }
}
