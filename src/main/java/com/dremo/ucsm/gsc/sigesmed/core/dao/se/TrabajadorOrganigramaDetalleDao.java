/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Planilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TrabajadorOrganigramaDetalle;

/**
 *
 * @author felipe
 */
public interface TrabajadorOrganigramaDetalleDao extends GenericDao<TrabajadorOrganigramaDetalle>{
    TrabajadorOrganigramaDetalle buscarPorId(int traId, int orgiId);
    Planilla buscarPlanilla(int plaId);
    Categoria buscarCategoria(int catId);
    Cargo buscarCargo(int carId);
    void eliminarRegistroBD(int traId, int orgiId);
}
