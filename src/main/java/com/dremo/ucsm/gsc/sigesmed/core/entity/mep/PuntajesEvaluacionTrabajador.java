package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by geank on 28/08/16.
 */
public class PuntajesEvaluacionTrabajador {
    private BigDecimal pun;
    private Date fecEva;

    public BigDecimal getPun() {
        return pun;
    }

    public void setPun(BigDecimal pun) {
        this.pun = pun;
    }

    public String getFecEva() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        return sdf.format(fecEva);
    }

    public void setFecEva(Date fecEva) {
        this.fecEva = fecEva;
    }



    public static double calculateAverage(List<PuntajesEvaluacionTrabajador> puntajes){
        SummaryStatistics stats = new SummaryStatistics();
        for(PuntajesEvaluacionTrabajador puntaje : puntajes ) stats.addValue(puntaje.getPun().doubleValue());
        return stats.getMean();
    }
    @Override
    public String toString() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecEva);

        return "[" +
                calendar.getTimeInMillis() +
                "," + pun +
                "]";
    }
}
