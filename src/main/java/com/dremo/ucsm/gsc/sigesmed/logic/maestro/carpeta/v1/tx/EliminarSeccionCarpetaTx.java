package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.SeccionCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 29/12/2016.
 */
public class EliminarSeccionCarpetaTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EliminarSeccionCarpetaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idSeccion = data.getInt("id");

        return eliminarSeccionCarpeta(idSeccion);
    }

    private WebResponse eliminarSeccionCarpeta(int idSeccion) {
        try{
            CarpetaPedagogicaDao carpetaDao = (CarpetaPedagogicaDao) FactoryDao.buildDao("maestro.carpeta.CarpetaPedagogicaDao");
            carpetaDao.eliminarSeccionCarpeta(idSeccion);
            return WebResponse.crearWebResponseExito("Se elimino corectamente");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarSeccionCarpeta",e);
            return WebResponse.crearWebResponseError("No se puede eliminar la seccion");
        }
    }
}
