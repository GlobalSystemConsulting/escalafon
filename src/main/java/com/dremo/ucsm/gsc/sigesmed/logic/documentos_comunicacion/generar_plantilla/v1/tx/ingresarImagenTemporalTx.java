/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.generar_plantilla.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;

import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;

/**
 *
 * @author Administrador
 */
public class ingresarImagenTemporalTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        FileJsonObject miF = null;
        String nombreArchivo = "";
        try {
            nombreArchivo = "temporalSDC";
            JSONObject requestData = (JSONObject) wr.getData();
            

                JSONObject jsonArchivo = requestData.optJSONObject("archivo");
                if (jsonArchivo != null && jsonArchivo.length() > 0) {
                    miF = new FileJsonObject(jsonArchivo, nombreArchivo);

                }

            
            if (nombreArchivo.length() > 0) {
                BuildFile.buildFromBase64("documentos_comunicacion_imagenes", miF.getName(), miF.getData());
            }
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo obtener la imagen de la Plantilla", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Se ingreso correctamente");
        //Fin
    }

}
