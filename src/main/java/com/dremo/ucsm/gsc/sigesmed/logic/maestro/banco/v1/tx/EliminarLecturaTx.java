package com.dremo.ucsm.gsc.sigesmed.logic.maestro.banco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.banco.LecturaBancoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.Lectura;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 26/12/2016.
 */
public class EliminarLecturaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EliminarLecturaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idLec = data.getInt("id");
        return eliminarLectura(idLec);
    }

    private WebResponse eliminarLectura(int idLec) {
        try{
            LecturaBancoDao lecturaDao = (LecturaBancoDao) FactoryDao.buildDao("maestro.banco.LecturaBancoDao");
            Lectura lectura = lecturaDao.buscarLecturaId(idLec);
            lecturaDao.deleteAbsolute(lectura);
            if(lectura.getUbi() != null && lectura.getUbi().length() > 0){
                //Eliminamos los archivos
                File file = new File(ServicioREST.PATH_SIGESMED+ File.separator +"archivos"+File.separator+"banco_lectura_docente" +File.separator + lectura.getUbi());
                file.delete();
            }
            return WebResponse.crearWebResponseExito("Exito al eliminar la lectura");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarLectura",e);
            return WebResponse.crearWebResponseError("no se pudo eliminar la lectura");
        }
    }
}
