package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.CompetenciaCapacidad;

import java.util.List;

/**
 * Created by Administrador on 13/10/2016.
 */
public interface CompetenciaCapacidadDao extends GenericDao<CompetenciaCapacidad> {
    List<CompetenciaCapacidad> buscarCapacidadesDeCompetencia(int id);
    CompetenciaCapacidad buscarCapacidadDeCompetencia(int idCap,int idCom);

}
