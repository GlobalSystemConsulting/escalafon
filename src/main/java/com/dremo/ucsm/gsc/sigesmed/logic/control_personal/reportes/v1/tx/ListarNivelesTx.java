/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.configuracion.v1.tx.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.AsistenciaAulaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.PersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.ConfiguracionControl;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanNivel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author carlos
 */
public class ListarNivelesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Integer orgID;
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            orgID = requestData.getInt("organizacionID");
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage());
        }

        AsistenciaAulaDao asistenciaAulaDao = (AsistenciaAulaDao) FactoryDao.buildDao("cpe.AsistenciaAulaDao");

        List<PlanNivel> niveles=null;
        try {
            niveles = asistenciaAulaDao.listarNivelesByOrganizacion(orgID);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo listar las organizaciones ", e.getMessage());
        }
        
        JSONArray miArray = new JSONArray();
        for(PlanNivel org:niveles)
        {
            JSONObject oRes=new JSONObject();
            oRes.put("id", org.getPlaNivId());
            oRes.put("nom", org.getDes());
            miArray.put(oRes);
        }

        return WebResponse.crearWebResponseExito("Se Listo correctamente", miArray);
       
        
        
        
            
        //Fin
    }
    
}

