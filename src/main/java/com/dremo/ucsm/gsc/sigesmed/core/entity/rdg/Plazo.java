/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.rdg;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ucsm
 */
@Entity
@Table(name = "plazo",schema="institucional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Plazo.findAll", query = "SELECT p FROM Plazo p"),
    @NamedQuery(name = "Plazo.findByPlzIde", query = "SELECT p FROM Plazo p WHERE p.plzIde = :plzIde"),
    @NamedQuery(name = "Plazo.findByPlzFecIniSub", query = "SELECT p FROM Plazo p WHERE p.plzFecIniSub = :plzFecIniSub"),
    @NamedQuery(name = "Plazo.findByPlzFecFinSub", query = "SELECT p FROM Plazo p WHERE p.plzFecFinSub = :plzFecFinSub")})
public class Plazo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "plz_ide")
    @SequenceGenerator(name = "secuencia_plazo", sequenceName="institucional.plazo_plz_ide_seq" )
    @GeneratedValue(generator="secuencia_plazo")
    private Integer plzIde;
    @Column(name = "plz_fec_ini_sub")
    @Temporal(TemporalType.TIMESTAMP)
    private Date plzFecIniSub;
    @Column(name = "plz_fec_fin_sub")
    @Temporal(TemporalType.TIMESTAMP)
    private Date plzFecFinSub;
    
   
    
    
    @OneToMany(mappedBy = "itePlaIde")
    private List<ItemFile> itemFileList;

    public Plazo() {
    }

    public Plazo(Date plzFecIniSub, Date plzFecFinSub) {
     
        this.plzFecIniSub = plzFecIniSub;
        this.plzFecFinSub = plzFecFinSub;
    }

    public Plazo(Integer plzIde) {
        this.plzIde = plzIde;
    }

    public Integer getPlzIde() {
        return plzIde;
    }

    public void setPlzIde(Integer plzIde) {
        this.plzIde = plzIde;
    }

    public Date getPlzFecIniSub() {
        return plzFecIniSub;
    }

    public void setPlzFecIniSub(Date plzFecIniSub) {
        this.plzFecIniSub = plzFecIniSub;
    }

    public Date getPlzFecFinSub() {
        return plzFecFinSub;
    }

    public void setPlzFecFinSub(Date plzFecFinSub) {
        this.plzFecFinSub = plzFecFinSub;
    }


    @XmlTransient
    public List<ItemFile> getItemFileList() {
        return itemFileList;
    }

    public void setItemFileList(List<ItemFile> itemFileList) {
        this.itemFileList = itemFileList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (plzIde != null ? plzIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Plazo)) {
            return false;
        }
        Plazo other = (Plazo) object;
        if ((this.plzIde == null && other.plzIde != null) || (this.plzIde != null && !this.plzIde.equals(other.plzIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsc.sigesmed.core.entity.Plazo[ plzIde=" + plzIde + " ]";
    }
    
}
