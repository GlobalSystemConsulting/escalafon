/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import java.util.List;

/**
 *
 * @author Administrador
 * @param <T>
 */
public interface GenericDao<T> {
    void insert(T dato);
    void update(T dato);
    void delete(T dato);    
    void deleteAbsolute(T dato);    
    List<T> buscarTodos(Class dato);
    List<T> buscarTodosOrdenados(Class dato,String campos);
    List<T> listar(Class dato);
    T load(Class c, int key);
    void merge(T dato);
    Object llave(Class dato,String campo);
    long mostrarNumeroRegistros(Class tabla);
}
