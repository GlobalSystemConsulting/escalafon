package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.GradoInstruccion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarGradoInstruccionTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        GenericMMIDaoHibernate<GradoInstruccion> hb = new GenericMMIDaoHibernate<>();
        JSONArray tipoPariente = new JSONArray();

        List tipoParientesSQL;

        try {
            tipoParientesSQL = hb.buscarTodos(GradoInstruccion.class);

            for (Object obj : tipoParientesSQL) {
                GradoInstruccion tempGradoInst = (GradoInstruccion) obj;
                JSONObject temp = new JSONObject();
                temp.put("graInsId", tempGradoInst.getGraInsId());
                temp.put("graInsDes", tempGradoInst.getGraInsNom());
                tipoPariente.put(temp);
            }

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al cargar los grados de instruccion! ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Listaron los grados de instruccion", tipoPariente);
    }

}
