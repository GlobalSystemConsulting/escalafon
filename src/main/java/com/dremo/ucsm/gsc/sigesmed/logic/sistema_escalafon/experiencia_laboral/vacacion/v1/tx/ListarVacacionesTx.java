/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.vacacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import static com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.MepReportDaoHibernate.logger;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.VacacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Vacacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import org.json.JSONArray;
import org.json.JSONObject;


public class ListarVacacionesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<Vacacion> vacaciones = null;
        VacacionDao vacacionDao = (VacacionDao)FactoryDao.buildDao("se.VacacionDao");
        
        CategoriaDao categoriaDao =(CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
        Categoria categoria=null;
        CargoDao cargoDao =(CargoDao)FactoryDao.buildDao("se.CargoDao");
        Cargo cargo=null;
        OrganigramaDao organigramaDao =(OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
        Organigrama organigrama=null;
        
        try{
            vacaciones = vacacionDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar vacaciones",e);
            System.out.println("No se pudo listar las vacaciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar las vacaciones", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
        JSONArray miArray = new JSONArray();
        for(Vacacion v:vacaciones){
            JSONObject oResponse = new JSONObject();
            oResponse.put("vacId", v.getVacId());
            oResponse.put("estGocDes", "");
            
            if(v.getEntEmiRes()!=null)
                oResponse.put("entEmi", v.getEntEmiRes());
            else
                oResponse.put("entEmi", "");

            if(v.getTipDocId()!=null)
                oResponse.put("tipDocId", v.getTipDocId());
            else
                oResponse.put("tipDocId", 0);

            if(v.getNumDoc()!=null)
                oResponse.put("numDoc", v.getNumDoc());
            else
                oResponse.put("numDoc", "");

            if(v.getFecDoc()!=null)
                oResponse.put("fecDoc", sdo.format(v.getFecDoc()));
            else
                oResponse.put("fecDoc", "");

            if(v.getEstGoc()!=null)
                oResponse.put("estGoc", v.getEstGoc());
            else
                oResponse.put("estGoc", false);

            if(v.getFecIni()!=null)
                oResponse.put("fecIni", sdo.format(v.getFecIni()));
            else
                oResponse.put("fecIni", "");

            if(v.getFecTer()!=null)
                oResponse.put("fecTer", sdo.format(v.getFecTer()));
            else
                oResponse.put("fecTer", "");

            if(v.getMotLic()!=null)
                oResponse.put("mot", v.getMotLic());
            else
                oResponse.put("mot", "");

            if(v.getNumInf()!=null)
                oResponse.put("numInf", v.getNumInf());
            else
                oResponse.put("numInf", "");

            if(v.getFecInf()!=null)
                oResponse.put("fecInf", sdo.format(v.getFecInf()));
            else
                oResponse.put("fecInf", "");

            if(v.getFecRecInf()!=null)
                oResponse.put("fecRecInf", sdo.format(v.getFecRecInf()));
            else
                oResponse.put("fecRecInf", "");

            if(v.getEstProId()!=null)
                oResponse.put("estProId", v.getEstProId());
            else
                oResponse.put("estProId", 0);
            
            //Historico
            if(v.getOrgiActId()!=null && v.getOrgiActId()>0){
                organigrama=organigramaDao.obtenerDetalle(v.getOrgiActId());
                oResponse.put("orgiActId", v.getOrgiActId());
                oResponse.put("datOrgiId", v.getCatActId());
                oResponse.put("nomDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getNomDatOrgi());
                oResponse.put("anioDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getAnioDatOrgi());
                oResponse.put("nomUbi", organigrama.getUbiId());
                
                oResponse.put("nomOrgiN3", organigrama.getNomOrgi());
                oResponse.put("nomOrgiN2", organigrama.getOrganigramaPadre().getNomOrgi());
                oResponse.put("nomOrgiN1", organigrama.getOrganigramaPadre().getOrganigramaPadre().getNomOrgi());
            }
            else{
                oResponse.put("orgiActId", 0);
                oResponse.put("datOrgiId", 0);
                oResponse.put("nomOrgi", "");
                oResponse.put("nomDatOrgi", "");
                oResponse.put("anioDatOrgi", 0);
                oResponse.put("nomUbi", 0);
            }

            if(v.getCatActId()!=null && v.getCatActId()>0){
                categoria=categoriaDao.obtenerDetalle(v.getCatActId());
                oResponse.put("catActId", v.getCatActId());
                oResponse.put("nomCat", categoria.getNomCat());
                oResponse.put("nomPla", categoria.getPlanilla().getNomPla());
            }
            else{
                oResponse.put("catActId", 0);
                oResponse.put("nomCat", "");
                oResponse.put("nomPla", "");
            }

            if(v.getCarActId()!=null && v.getCarActId()>0){
                cargo=cargoDao.buscarPorId(v.getCarActId());
                oResponse.put("carActId", v.getCarActId());
                oResponse.put("nomCar", cargo.getNomCar());            
            }
            else{
                oResponse.put("carActId", 0);
                oResponse.put("nomCar", "");
            }
            miArray.put(oResponse);
            
        }
        
        return WebResponse.crearWebResponseExito("Las vacaciones fueron listadas exitosamente", miArray);
    }
    
}
