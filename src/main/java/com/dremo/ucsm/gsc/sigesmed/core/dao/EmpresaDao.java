/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Empresa;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.List;

/**
 *
 * @author Carlos
 */
public interface EmpresaDao extends GenericDao<Empresa>{
    
    public List<Empresa> lsitarEmpresas();
    public Empresa buscarEmpresa(String ruc);
}
