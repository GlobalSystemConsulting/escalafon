/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_personales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DireccionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FichaEscalafonariaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.PersonaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorOrganigramaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Direccion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Idioma;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Nacionalidad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TrabajadorOrganigramaDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarDatosPersonalesTx implements ITransaction {
    private static  final Logger logger = Logger.getLogger(AgregarDatosPersonalesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {

        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
        Persona persona = null;
        Direccion direccion = null;
        Organizacion organizacion = null;
        Trabajador trabajador = null;
        FichaEscalafonaria fe = null;
        TrabajadorOrganigramaDetalle traOrgDet = null;
        Desplazamiento desplazamiento = null;

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            JSONObject rPersona = requestData.getJSONObject("persona");
            
            int caso= rPersona.getInt("caso");
            
            if (caso!=1)//Tiene persona con el DNI/o no tiene registro con el DNI
            {
                int perId = rPersona.getInt("perId");

                PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("se.PersonaDao");
                JSONArray rDirecciones = requestData.getJSONArray("direcciones");

                String apePat = rPersona.getString("apePat").toUpperCase();
                String apeMat = rPersona.getString("apeMat").toUpperCase();
                String nom = rPersona.getString("nom").toUpperCase();
                Character sex = rPersona.getString("sex").charAt(0);
                Integer estCivId = rPersona.getInt("estCivId") != 0 ? rPersona.getInt("estCivId") : null;
                String dni = rPersona.getString("dni");
                String pas = rPersona.getString("pas");
                Boolean estAseEss = rPersona.getBoolean("estAseEss");
                String autEss = rPersona.getString("autEss");
                String fij = rPersona.getString("fij");
                String num1 = rPersona.getString("num1");
                String num2 = rPersona.getString("num2");
                String email = rPersona.getString("email");
                Integer nacId = rPersona.getInt("nacId");
                String depNac = rPersona.getString("depNac");
                String proNac = rPersona.getString("proNac");
                String disNac = rPersona.getString("disNac");
                Date fecNac = new Date();
                fecNac = rPersona.getString("fecNac").equals("") ? fecNac : sdi.parse(rPersona.getString("fecNac").substring(0, 10));
                String sisPen = rPersona.getString("sisPen");
                String tipAfp = rPersona.getString("tipAfp");
                String codCuspp = rPersona.getString("codCuspp");
                String fecIngAfp = rPersona.getString("fecIngAfp");
                String fecTraAfp = rPersona.getString("fecTraAfp");
                Boolean perDis = rPersona.getBoolean("perDis");
                String regCon = rPersona.getString("regCon");
                Integer idiomId = rPersona.getInt("idiomId");
                String licCond = rPersona.getString("licCond");
                String bonCaf = rPersona.getString("bonCaf");
                
                if(caso==0)//No tiene: persona, trabajador, ficha
                {
                    persona = new Persona(apePat, apeMat, nom, dni, fecNac, num1, num2, fij, email, sex, estCivId, depNac, proNac, disNac, wr.getIdUsuario(), new Date(), 'A');
                }
                else { //caso 2 = Tiene persona
                    persona = personaDao.buscarPersonaPorId(perId);

                    if (!"".equals(apePat)) {
                        persona.setApePat(apePat.toUpperCase());
                    }
                    persona.setApeMat(apeMat.toUpperCase());
                    if (!"".equals(nom)) {
                        persona.setNom(nom.toUpperCase());
                    }
                    persona.setSex(sex);
                    
                    if (!"".equals(fij)) {
                        persona.setFij(fij);
                    }
                    if (!"".equals(num1)) {
                        persona.setNum1(num1);
                    }
                    if (!"".equals(num2)) {
                        persona.setNum2(num2);
                    }
                    if (!"".equals(email)) {
                        persona.setEmail(email);
                    }
                    persona.setFecNac(fecNac);

                    if (!"".equals(depNac)) {
                        persona.setDepNac(depNac);
                    }
                    if (!"".equals(proNac)) {
                        persona.setProNac(proNac);
                    }
                    if (!"".equals(disNac)) {
                        persona.setDisNac(disNac);
                    }
                }
                persona.setDiaFecNac(Integer.parseInt(new SimpleDateFormat("dd").format(fecNac)));
                persona.setMesFecNac(Integer.parseInt(new SimpleDateFormat("MM").format(fecNac)));
                persona.setAnioFecNac(Integer.parseInt(new SimpleDateFormat("yyyy").format(fecNac)));
                persona.setPas(pas);
                persona.setBoolSalud(estAseEss);
                if (estAseEss) {
                    if (!"".equals(autEss)) {
                        persona.setAutEss(autEss);
                    }
                }
                if (nacId != 0) {
                    persona.setNacionalidad(new Nacionalidad(nacId));
                }
                persona.setSisPen(sisPen);
                if (sisPen.equals("AFP")) {
                    if (!"".equals(tipAfp)) {
                        persona.setTipAfp(tipAfp);
                    }
                    if (!"".equals(codCuspp)) {
                        persona.setCodCuspp(codCuspp);
                    }
                    if (!"".equals(fecIngAfp)) {
                        persona.setFecIngAfp(sdi.parse(fecIngAfp.substring(0, 10)));
                    }
                    if (!"".equals(fecTraAfp)) {
                        persona.setFecTraAfp(sdi.parse(fecTraAfp.substring(0, 10)));
                    }
                }
                
                persona.setPerDis(perDis);
                if (perDis) {
                    if (!"".equals(regCon)) {
                        persona.setRegCon(regCon);
                    }
                }
                if (idiomId != 0) {
                    persona.setIdiomas(new Idioma(idiomId));
                }
                persona.setLicCond(licCond);
                persona.setBonCaf(bonCaf);
                //Insertar/actualizar->(persona)
                if (caso == 0) {
                    personaDao.insert(persona);
                } else {//Caso 2
                    personaDao.update(persona);
                }
                ///////////////////Dirección///////////////////
                DireccionDao direccionDao = (DireccionDao) FactoryDao.buildDao("se.DireccionDao");
                for (int i = 0; i < rDirecciones.length(); i++) {
                    String tipDir = rDirecciones.getJSONObject(i).getString("tipDir");
                    String depDir = rDirecciones.getJSONObject(i).getString("depDir");
                    String proDir = rDirecciones.getJSONObject(i).getString("proDir");
                    String disDir = rDirecciones.getJSONObject(i).getString("disDir");
                    String nomDir = rDirecciones.getJSONObject(i).getString("nomDir");
                    String num = rDirecciones.getJSONObject(i).getString("num");
                    String dpto = rDirecciones.getJSONObject(i).getString("dpto");
                    String interior = rDirecciones.getJSONObject(i).getString("int");
                    String mz = rDirecciones.getJSONObject(i).getString("mz");
                    String lote = rDirecciones.getJSONObject(i).getString("lote");
                    String km = rDirecciones.getJSONObject(i).getString("km");
                    String bloque = rDirecciones.getJSONObject(i).getString("bloque");
                    String etapa = rDirecciones.getJSONObject(i).getString("etapa");
                    String nomZon = rDirecciones.getJSONObject(i).getString("nomZon");
                    String desRef = rDirecciones.getJSONObject(i).getString("desRef");
                    String fulDir = nomDir + (num.equals("")?"":" Nº ") + num + (dpto.equals("")?"":" Dpto. ") + dpto + (interior.equals("")?"":" Int. ") + interior + (mz.equals("")?"":" Mz. ")+ mz + (lote.equals("")?"":" Lt.")+ lote + (km.equals("")?"":" Km. ")+ km + (bloque.equals("")?"":" Bloque ")+ bloque + (etapa.equals("")?"":" Etapa ")+ etapa;
                    direccion = new Direccion(persona, tipDir.charAt(0), fulDir, depDir, proDir, disDir, nomZon, desRef, wr.getIdUsuario(), new Date(), 'A');
                    direccionDao.insert(direccion);
                }
                ///////////AUDITORIA/////////////
                if(caso==0)
                    AgregarOperacionAuditoria.agregarAltaAuditoria("Se agregó una persona con datos generales", nom + " " + apePat + " " + apeMat, wr.getIdUsuario(), "---", "xxx.xxx.xxx.xxx");
                else
                    AgregarOperacionAuditoria.agregarCambioAuditoria("Se actualizó una persona con datos generales", nom + " " + apePat + " " + apeMat, wr.getIdUsuario(), "---", "xxx.xxx.xxx.xxx");
                ///////////AUDITORIA/////////////
            }
            if(caso==1)
            {
                int perId = rPersona.getInt("perId");
                PersonaDao personaDao = (PersonaDao) FactoryDao.buildDao("se.PersonaDao");
                persona = personaDao.buscarPersonaPorId(perId);
            }
            
            ////////////////////////TRABAJADOR////////////////////////
            Integer orgId = requestData.getInt("orgId");
            trabajador = new Trabajador(persona, new Organizacion(orgId), new Rol(5), '1', new Date(), wr.getIdUsuario(), new Date(), 'A');
            trabajador.setFecIng(new Date());//Fecha de Ingreso es la misma que el registro

            TrabajadorDao trabajadorDao = (TrabajadorDao) FactoryDao.buildDao("se.TrabajadorDao");
            try {
                trabajadorDao.insert(trabajador);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Agregar Trabajador", e);
                System.out.println("No se pudo registrar el trabajador\n" + e);
                return WebResponse.crearWebResponseError("No se pudo registrar el trabajador", e.getMessage());
            }
            ////////////////////////FICHA ESCALAFONARIA////////////////////////
            fe = new FichaEscalafonaria(trabajador, wr.getIdUsuario(), new Date(), 'A');
            FichaEscalafonariaDao feDao = (FichaEscalafonariaDao) FactoryDao.buildDao("se.FichaEscalafonariaDao");
            try {
                feDao.insert(fe);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Agregar ficha escalafonaria", e);
                System.out.println("No se pudo registrar la ficha escalafonaria\n" + e);
                return WebResponse.crearWebResponseError("No se pudo registrar la ficha escalafonaria", e.getMessage());
            }
            ///////////////////////ORGANIGRAMA/////////////////////////////
            TrabajadorOrganigramaDetalleDao traOrgDetDao = (TrabajadorOrganigramaDetalleDao) FactoryDao.buildDao("se.TrabajadorOrganigramaDetalleDao"); 
            int traId = trabajador.getTraId();
            int orgiId = 0, carId = 0, catId = 0;
            try {
                JSONObject rTraOrgDet = requestData.getJSONObject("traOrgDet");
                orgiId = rTraOrgDet.get("orgiId")==null?0:rTraOrgDet.getInt("orgiId");
                int plaId = rTraOrgDet.get("plaId")==null?0:rTraOrgDet.getInt("plaId");
                catId = rTraOrgDet.get("catId")==null?0:rTraOrgDet.getInt("catId");
                carId = rTraOrgDet.get("carId")==null?0:rTraOrgDet.getInt("carId");
                
                traOrgDet = new TrabajadorOrganigramaDetalle(traId, orgiId, 1, plaId, catId, carId);
                traOrgDetDao.insert(traOrgDet);
            }catch (Exception e) {
                logger.log(Level.SEVERE, "Agregar nueva relacion trabajador organigrama ", e);
                System.out.println("No se pudo registrar la relacion trabajador organigrama\n" + e);
                return WebResponse.crearWebResponseError("No se pudo registrar la relacion trabajador organigrama", e.getMessage());
            }

            try {
                TrabajadorDao traDao = (TrabajadorDao) FactoryDao.buildDao("se.TrabajadorDao");
                //trabajador.setCargo(new Cargo(carId));
                //trabajador.setCategoria(new Categoria(catId));
                trabajador.setOrgiId(orgiId);
                traDao.update(trabajador);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Actualizar orgiId en Trabajador ", e);
                System.out.println(e);
            }
            ///////////////////////INGRESO/////////////////////////////
            DesplazamientoDao desplazamientoDao = (DesplazamientoDao) FactoryDao.buildDao("se.DesplazamientoDao");
            try {
                JSONObject rNuevoDespla = requestData.getJSONObject("nuevoDespla");
                int tipDocIdIngRe = rNuevoDespla.getInt("tipDocIdIngRe");
                String numDoc = rNuevoDespla.getString("numDoc");
                Date fecDoc = rNuevoDespla.getString("fecDoc").equals("")?null:sdi.parse(rNuevoDespla.getString("fecDoc").substring(0, 10));
                int jorLabIdIngRe = rNuevoDespla.getInt("jorLabIdIngRe");
                Date fecIni = rNuevoDespla.getString("fecIni").equals("")?null:sdi.parse(rNuevoDespla.getString("fecIni").substring(0, 10));
                
                desplazamiento = new Desplazamiento(fe, '1', tipDocIdIngRe, numDoc, fecDoc, jorLabIdIngRe, fecIni, wr.getIdUsuario(), new Date(), 'A');
                desplazamiento.setOrgiActId(orgiId);
                //if(carId!=0)
                    desplazamiento.setCarActId(carId);
                //if(catId!=0)
                    desplazamiento.setCatActId(catId);
                desplazamientoDao.insert(desplazamiento);
                
                ///////////AUDITORIA/////////////
                AgregarOperacionAuditoria.agregarAltaAuditoria("Se agregó una ficha escalafonaria", "Detalle: " + desplazamiento.getFichaEscalafonaria().getFicEscId() + " ficha ", wr.getIdUsuario(), "---", "xxx.xxx.xxx.xxx");
                ///////////AUDITORIA/////////////

            } catch (Exception e) {
                logger.log(Level.SEVERE, "Agregar desplazamiento de ingreso. ", e);
                System.out.println("No se pudo registrar el desplazamiento de ingreso\n" + e);
                return WebResponse.crearWebResponseError("No se pudo registrar el desplazamiento de ingreso", e.getMessage());
            }
            
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar trabajador con datos generales",e);
            System.out.println(e);
            return WebResponse.crearWebResponseError("Datos incorrectos");
        }

        JSONObject oResponse = new JSONObject();  
        oResponse.put("perId", persona.getPerId());
        
        return WebResponse.crearWebResponseExito("El registro de la ficha escalafonaria se realizo correctamente", oResponse);
        //Fin
    }
}
