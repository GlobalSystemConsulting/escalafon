package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AsistenciaParticipanteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CronogramaSedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.HorarioSedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.ParticipanteCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AsistenciaParticipante;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CapacitadorCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CronogramaSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.HorarioSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListarCursosCapacitacionTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarCursosCapacitacionTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int opt = data.getInt("opt");
        WebResponse response = null;

        switch (opt) {
            case 0:
                response = listarCurso(data.getInt("cod"));
                break;

            case 1:
                response = listarCursos();
                break;

            case 2:
                response = listarCursosFuente(data.getInt("org"));
                break;

            case 3:
                response = listarCursosFuenteyAno(data.getInt("org"), data.getInt("ano"));
                break;

            case 4:
                response = listarCursoSedes(data.getInt("cod"));
                break;

            case 5:
                response = listarCursosTipo(data.getString("tip"));
                break;

            case 6:
                response = listarCursosRol(data.getInt("rol"), data.getInt("usu"));
                break;
                
            case 7:
                response = listarCursosBanPre(data.getInt("codOrg"));
                break;
                
            case 8:
                response = listarCursosReportes();
                break;
                
            case 9:
                response = listarDisponibles(data.getInt("perId"));
                break;
        }

        return response;
    }

    private WebResponse listarCurso(int cod) {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            CursoCapacitacion curso = capacitacionDao.buscarPorId(cod);

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE)
                    .setData(new JSONArray(EntityUtil.objectToJSONString(
                                            new String[]{"curCapId", "fecIni", "fecFin"},
                                            new String[]{"cod", "ini", "fin"},
                                            curso
                                    )));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCursos", e);
            return WebResponse.crearWebResponseError("Error al listar los cursos activos", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarCursos() {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            List<CursoCapacitacion> cursos = capacitacionDao.listarCapacitaciones();
            JSONArray array = new JSONArray();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            
            for (CursoCapacitacion capacitacion : cursos) {
                switch (capacitacion.getEst()) {
                    case 'A':
                        if (capacitacion.getFecIni().before(new Date())) {
                            if (capacitacion.getFecFin().before(new Date()))
                                capacitacion.setEst('F');
                             else
                                capacitacion.setEst('I');
                           
                            capacitacionDao.update(capacitacion);
                        }
                        break;
                    case 'I':
                        if (capacitacion.getFecFin().before(new Date())) {
                            capacitacion.setEst('F');
                            capacitacionDao.update(capacitacion);
                        }
                        break;
                }
                
                JSONObject object = new JSONObject();
                object.put("cod", capacitacion.getCurCapId());
                object.put("nom", capacitacion.getNom());
                object.put("tip", capacitacion.getTip());
                object.put("ini", format.format(capacitacion.getFecIni()));
                object.put("fin", format.format(capacitacion.getFecFin()));
                object.put("est", capacitacion.getEst());
                
                JSONArray sedes = new JSONArray();

                for (SedeCapacitacion sede : capacitacion.getSedesCapacitacion()) {
                    JSONObject objSede = new JSONObject();
                    objSede.put("id", sede.getSedCapId());
                    objSede.put("nom", sede.getPro() + " - " + sede.getDis() + " - " + sede.getDir());
                    objSede.put("iniC", sede.getCronograma().getFecIni());
                    objSede.put("finC", sede.getCronograma().getFecFin());
                    
                    if (sede.getCronograma().getFecIni().before(new Date()))
                        if (sede.getCronograma().getFecFin().before(new Date()))
                            objSede.put("est", "F");
                        else
                            objSede.put("est", "I");
                    else
                        objSede.put("est", "A");
                    
                    sedes.put(objSede);
                }

                object.put("sedes", sedes);
                array.put(object);
            }
            
            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCursos", e);
            return WebResponse.crearWebResponseError("Error al listar los cursos activos", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarCursosFuente(int idOrg) {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            List<CursoCapacitacion> cursosGenerales = capacitacionDao.listarCapacitacionPorOrganizacion(idOrg);
            Map<Integer, CursoCapacitacion> cursosAno = new HashMap<>();

            for (CursoCapacitacion curso : cursosGenerales) {
                int year = curso.getFecIni().getYear();
                if (!cursosAno.containsKey(year)) {
                    cursosAno.put(year, curso);
                }
            }

            List<CursoCapacitacion> cursos = new ArrayList<>(cursosAno.values());

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE)
                    .setData(new JSONArray(EntityUtil.listToJSONString(
                                            new String[]{"fecIni"},
                                            new String[]{"ini"},
                                            cursos
                                    )));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCursosFuente", e);
            return WebResponse.crearWebResponseError("Error al listar los cursos por organización", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarCursosFuenteyAno(int idOrg, int year) {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            List<CursoCapacitacion> cursos = capacitacionDao.listarCapacitacionPorOrganizacionYAno(idOrg, year);

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE)
                    .setData(new JSONArray(EntityUtil.listToJSONString(
                                            new String[]{"curCapId", "nom"},
                                            new String[]{"cod", "nom"},
                                            cursos
                                    )));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCursosFuente", e);
            return WebResponse.crearWebResponseError("Error al listar los cursos por organización", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarCursoSedes(int cod) {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            CursoCapacitacion curso = capacitacionDao.buscarPorId(cod);

            JSONObject objCurso = new JSONObject();
            objCurso.put("idCap", curso.getCurCapId());
            objCurso.put("tip", curso.getTip());
            objCurso.put("organizacion", curso.getOrganizacion().getOrgId());
            objCurso.put("nom", curso.getNom());
            objCurso.put("organizacionAut", curso.getOrganizacionAut().getOrgId());
            objCurso.put("numPar", curso.getNumPar());

            SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
            CronogramaSedeCapacitacionDao cronogramaSedeCapacitacionDao = (CronogramaSedeCapacitacionDao) FactoryDao.buildDao("capacitacion.CronogramaSedeCapacitacionDao");
            HorarioSedeCapacitacionDao horarioSedeCapacitacionDao = (HorarioSedeCapacitacionDao) FactoryDao.buildDao("capacitacion.HorarioSedeCapacitacionDao");

            List<SedeCapacitacion> sedes = sedeCapacitacionDao.buscarSedesPorCapacitacion(cod);

            JSONArray objSedes = new JSONArray();

            for (SedeCapacitacion sede : sedes) {
                JSONObject objSede = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"sedCapId", "pro", "dis", "loc", "dir", "ref"},
                        new String[]{"cod", "pro", "dis", "loc", "dir", "ref"},
                        sede
                ));

                CronogramaSedeCapacitacion cronograma = cronogramaSedeCapacitacionDao.buscarCronogramaPorSede(sede.getSedCapId());

                JSONObject objCronograma = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"fecIni", "fecFin", "numPar", "dniRes", "nomRes"},
                        new String[]{"fecIni", "fecFin", "numPar", "dniRes", "nomRes"},
                        cronograma
                ));

                objSede.put("cronograma", objCronograma);
                /*HorarioSedeCapacitacion horario = horarioSedeCapacitacionDao.buscarHorarioPorSede(sede.getSedCapId());

                JSONObject objHorario = new JSONObject();

                if (horario != null) {
                    objHorario.put("dia", horario.getDia());
                    objHorario.put("turno1horaIni", horario.getTur1Ini().getHours() + ":" + horario.getTur1Ini().getMinutes());
                    objHorario.put("turno1horaFin", horario.getTur1Fin().getHours() + ":" + horario.getTur1Fin().getMinutes());

                    if (horario.getTur2Ini() != null) {
                        objHorario.put("turno2horaIni", horario.getTur2Ini().getHours() + ":" + horario.getTur2Ini().getMinutes());
                    } else {
                        objHorario.put("turno2horaIni", "");
                    }

                    if (horario.getTur2Fin() != null) {
                        objHorario.put("turno2horaFin", horario.getTur2Fin().getHours() + ":" + horario.getTur2Fin().getMinutes());
                    } else {
                        objHorario.put("turno2horaFin", "");
                    }
                }

                objSede.put("horario", objHorario);*/
                objSedes.put(objSede);
            }

            objCurso.put("sedes", objSedes);

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE)
                    .setData(objCurso);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCursoSedes", e);
            return WebResponse.crearWebResponseError("Error al listar el curso y sus sedes", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarCursosTipo(String tipo) {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            List<CursoCapacitacion> cursos = capacitacionDao.listarCapacitacionPorTipo(tipo);
            JSONArray array = new JSONArray();

            for (CursoCapacitacion curso : cursos) {
                JSONObject object = new JSONObject();
                object.put("cod", curso.getCurCapId());
                object.put("nom", curso.getNom());
                array.put(object);
            }

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCursosFuente", e);
            return WebResponse.crearWebResponseError("Error al listar los cursos por organización", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarCursosRol(int rolCod, int usuCod) {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            JSONObject curso = new JSONObject();
            JSONArray cursos = null;

            if (rolCod == 11) {
                cursos = capacitacionDao.listarCapacitacionesCapacitador(usuCod);                
                curso.put("state", true);
                curso.put("cursos", cursos);
            } else {
                cursos = capacitacionDao.listarCapacitacionesPersona(usuCod);
                curso.put("state", false);
                curso.put("cursos", cursos);
                
                AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
                List<AsistenciaParticipante> asistencias = capacitacionDao.asistenciaDocente(usuCod);
                for(AsistenciaParticipante attendance: asistencias) {
                    attendance.setUsuMod(usuCod);
                    attendance.setEstReg('F');
                    asistenciaParticipanteDao.update(attendance);
                }
            }

            for(int i = 0;i < cursos.length();i++) {
                switch(cursos.getJSONObject(i).getString("sedEst")){
                    case "I":
                        generarHorarios(cursos.getJSONObject(i).getInt("sedCod"));
                        break;
                }
            }
            
            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(curso);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCursosRol", e);
            return WebResponse.crearWebResponseError("Error al listar el curso y sus sedes", WebResponse.BAD_RESPONSE);
        }
    }
    
    private WebResponse listarCursosBanPre(int codOrg) {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            List<CursoCapacitacion> courses = capacitacionDao.listarCapacitacionPorOrganizacion(codOrg);
            JSONArray array = new JSONArray();
            
            for(CursoCapacitacion curso: courses) {
                JSONObject object = new JSONObject();
                object.put("cod", curso.getCurCapId());
                object.put("tip", curso.getTip());
                object.put("nom", curso.getNom());
                
                array.put(object);
            }
            
            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCursosBanPre", e);
            return WebResponse.crearWebResponseError("Error al listar el curso", WebResponse.BAD_RESPONSE);
        }
    }
    
    private WebResponse listarCursosReportes() {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            List<CursoCapacitacion> cursos = capacitacionDao.listarReportes();
            JSONArray array = new JSONArray();
            
            for(CursoCapacitacion curso: cursos) {
                JSONObject object = new JSONObject();
                object.put("cod", curso.getCurCapId());
                object.put("tip", curso.getTip());
                object.put("nom", curso.getNom());
                
                array.put(object);
            }

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCursosFuente", e);
            return WebResponse.crearWebResponseError("Error al listar los cursos", WebResponse.BAD_RESPONSE);
        }
    }
    
    private void generarHorarios(int codSed) {
        HorarioSedeCapacitacionDao horarioSedeCapacitacionDao = (HorarioSedeCapacitacionDao) FactoryDao.buildDao("capacitacion.HorarioSedeCapacitacionDao");
        AsistenciaParticipanteDao asistenciaParticipanteDao = (AsistenciaParticipanteDao) FactoryDao.buildDao("capacitacion.AsistenciaParticipanteDao");
        ParticipanteCapacitacionDao participanteCapacitacionDao = (ParticipanteCapacitacionDao) FactoryDao.buildDao("capacitacion.ParticipanteCapacitacionDao");
        CronogramaSedeCapacitacionDao cronogramaSedeCapacitacionDao = (CronogramaSedeCapacitacionDao) FactoryDao.buildDao("capacitacion.CronogramaSedeCapacitacionDao");
        
        List<HorarioSedeCapacitacion> horarios = horarioSedeCapacitacionDao.buscarPorSede(codSed);
        List<BigInteger> participantes = participanteCapacitacionDao.listarParticipantes(codSed);
        CronogramaSedeCapacitacion cronograma = cronogramaSedeCapacitacionDao.buscarCronogramaPorSede(codSed);
        
        for(HorarioSedeCapacitacion shift: horarios) {
            if(!asistenciaParticipanteDao.existenRegistros(shift.getHorSedCapId())) {
                List<Date> dates = new ArrayList<>();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(cronograma.getFecIni());
                
                if(shift.getDia() > calendar.get(Calendar.DAY_OF_WEEK))
                    calendar.add(Calendar.DATE, shift.getDia() - calendar.get(Calendar.DAY_OF_WEEK));
                else if(shift.getDia() < calendar.get(Calendar.DAY_OF_WEEK))
                    calendar.add(Calendar.DATE, 7 - calendar.get(Calendar.DAY_OF_WEEK) + shift.getDia());

                while(calendar.getTime().before(cronograma.getFecFin())) {
                    dates.add(calendar.getTime());
                    calendar.add(Calendar.DATE, 7);
                }  
                                
                for(BigInteger codPar: participantes) {
                    for(Date date: dates){
                        asistenciaParticipanteDao.insert(new AsistenciaParticipante(codPar.intValue(), shift, 'G', date));
                    }
                }
            }
        }
    }
    
    private WebResponse listarDisponibles(int perId) {
        try {
            CursoCapacitacionDao capacitacionDao = (CursoCapacitacionDao) FactoryDao.buildDao("capacitacion.CursoCapacitacionDao");
            List<SedeCapacitacion> sedes = capacitacionDao.listarDisponibles(perId);
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

            JSONArray array = new JSONArray();
            
            for(SedeCapacitacion sede: sedes) {
                JSONObject object = new JSONObject();
                
                CursoCapacitacion curso = sede.getCursoCapacitacion();
                object.put("cod", sede.getSedCapId());
                object.put("org", curso.getOrganizacion().getNom());
                object.put("aut", curso.getOrganizacionAut().getNom());
                object.put("tip", curso.getTip());
                object.put("usu", curso.getUsuMod());
                
                if(curso.getTip().equals("Online"))
                    object.put("nom", curso.getNom());
                else
                    object.put("nom", curso.getNom() + " SEDE: " + sede.getPro() + " - " + sede.getDis() + " - " + sede.getLoc());
                
                CronogramaSedeCapacitacion cronograma = sede.getCronograma();
                object.put("ini", format.format(cronograma.getFecIni()));
                object.put("fin", format.format(cronograma.getFecFin()));
                
                JSONArray horarios = new JSONArray();
                for(HorarioSedeCapacitacion horario: sede.getHorarios()) {
                    horarios.put(getDay(horario.getDia(), horario.getTurIni(), horario.getTurFin()));
                }
                
                object.put("hor", horarios);
                
                JSONArray capacitadores = new JSONArray();
                for(CapacitadorCursoCapacitacion capacitador: sede.getCapacitadoresCursoCapacitacion()) {
                    Persona persona = capacitador.getPer();
                    capacitadores.put(persona.getNom() + " " + persona.getApePat() + " " + persona.getApeMat());
                }
                
                object.put("cap", capacitadores);
                
                array.put(object);
            }

            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarCursosFuente", e);
            return WebResponse.crearWebResponseError("Error al listar los cursos", WebResponse.BAD_RESPONSE);
        }
    }
    
    private String getDay(int day, Date ini, Date fin) {
        String name = "";
        DateFormat format = new SimpleDateFormat("HH:mm");
        
        switch(day) {
            case 1: name = "Domingo " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 2: name = "Lunes " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 3: name = "Martes " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 4: name = "Miércoles " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 5: name = "Jueves " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 6: name = "Viernes " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 7: name = "Sábado " + format.format(ini) + " - " + format.format(fin);
                    break;
        }
        
        return name;
    }
}
