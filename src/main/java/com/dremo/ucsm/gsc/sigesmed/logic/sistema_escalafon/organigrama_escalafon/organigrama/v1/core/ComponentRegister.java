/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.organigrama.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.organigrama.v1.tx.ActualizarOrganigramaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.organigrama.v1.tx.AgregarOrganigramaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.organigrama.v1.tx.EliminarOrganigramaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.organigrama.v1.tx.ListarOrganigramaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.organigrama_escalafon.organigrama.v1.tx.ListarOrganismosxTipoTx;

/**
 *
 * @author forev
 */
public class ComponentRegister implements IComponentRegister {
    
     @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("organigramaConfiguracion");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarOrganigrama", ListarOrganigramaTx.class);
        seComponent.addTransactionPUT("actualizarOrganigrama", ActualizarOrganigramaTx.class);
        seComponent.addTransactionPOST("agregarOrganigrama", AgregarOrganigramaTx.class);
        seComponent.addTransactionDELETE("eliminarOrganigrama", EliminarOrganigramaTx.class);
        
        seComponent.addTransactionGET("listarxTipo", ListarOrganismosxTipoTx.class);
        
        return seComponent;
    }
    
}

