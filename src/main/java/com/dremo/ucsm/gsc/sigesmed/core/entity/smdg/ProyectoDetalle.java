/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "proyecto_detalle", schema = "institucional")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "ProyectoDetalle.findAll", query = "SELECT p FROM ProyectoDetalle p"),
//    @NamedQuery(name = "ProyectoDetalle.findByPdeId", query = "SELECT p FROM ProyectoDetalle p WHERE p.pdeId = :pdeId"),
//    @NamedQuery(name = "ProyectoDetalle.findByPdeAva", query = "SELECT p FROM ProyectoDetalle p WHERE p.pdeAva = :pdeAva"),
//    @NamedQuery(name = "ProyectoDetalle.findByPdeDoc", query = "SELECT p FROM ProyectoDetalle p WHERE p.pdeDoc = :pdeDoc"),
//    @NamedQuery(name = "ProyectoDetalle.findByPdeUrl", query = "SELECT p FROM ProyectoDetalle p WHERE p.pdeUrl = :pdeUrl"),
//    @NamedQuery(name = "ProyectoDetalle.findByFecMod", query = "SELECT p FROM ProyectoDetalle p WHERE p.fecMod = :fecMod"),
//    @NamedQuery(name = "ProyectoDetalle.findByUsuMod", query = "SELECT p FROM ProyectoDetalle p WHERE p.usuMod = :usuMod"),
//    @NamedQuery(name = "ProyectoDetalle.findByEstReg", query = "SELECT p FROM ProyectoDetalle p WHERE p.estReg = :estReg")})
public class ProyectoDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id    
    @Column(name = "pro_id")
    private Integer pdeId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "pde_ava")
    private Float pdeAva;
    @Column(name = "pde_doc")
    private String pdeDoc;
    @Column(name = "pde_url")
    private String pdeUrl;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "est_reg")
    private String estReg = "A";
//    @JoinColumn(name = "pro_id", referencedColumnName = "pro_id")
//    @ManyToOne
//    private Proyectos proId;

    public ProyectoDetalle() {
    }

    public ProyectoDetalle(Integer pdeId) {
        this.pdeId = pdeId;
    }

    public ProyectoDetalle(String pdeDoc) {
        this.pdeDoc = pdeDoc;    
    }
    
    public ProyectoDetalle(Integer pdeId, String pdeDoc) {
        this.pdeId = pdeId;
        this.pdeDoc = pdeDoc;    
    }
    
    public ProyectoDetalle(String pdeDoc, String pdeUrl) {
        this.pdeDoc = pdeDoc;
        this.pdeUrl = pdeUrl;        
    }
    
    public ProyectoDetalle(Integer pdeId, String pdeDoc, String pdeUrl) {
        this.pdeId = pdeId;
        this.pdeDoc = pdeDoc;
        this.pdeUrl = pdeUrl;
    }
    
    public Integer getPdeId() {
        return pdeId;
    }

    public void setPdeId(Integer pdeId) {
        this.pdeId = pdeId;
    }

    public Float getPdeAva() {
        return pdeAva;
    }

    public void setPdeAva(Float pdeAva) {
        this.pdeAva = pdeAva;
    }

    public String getPdeDoc() {
        return pdeDoc;
    }

    public void setPdeDoc(String pdeDoc) {
        this.pdeDoc = pdeDoc;
    }

    public String getPdeUrl() {
        return pdeUrl;
    }

    public void setPdeUrl(String pdeUrl) {
        this.pdeUrl = pdeUrl;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

//    public Proyectos getProId() {
//        return proId;
//    }
//
//    public void setProId(Proyectos proId) {
//        this.proId = proId;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdeId != null ? pdeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProyectoDetalle)) {
            return false;
        }
        ProyectoDetalle other = (ProyectoDetalle) object;
        if ((this.pdeId == null && other.pdeId != null) || (this.pdeId != null && !this.pdeId.equals(other.pdeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.ProyectoDetalle[ pdeId=" + pdeId + " ]";
    }
    
}
