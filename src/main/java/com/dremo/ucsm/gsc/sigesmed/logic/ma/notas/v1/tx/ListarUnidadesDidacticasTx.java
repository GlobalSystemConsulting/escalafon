package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.PlanEstudiosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 13/01/2017.
 */
public class ListarUnidadesDidacticasTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarUnidadesDidacticasTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idPlan = data.optInt("idPlan", -1);
        int idArea = data.getInt("are");
        int idGrado = data.getInt("gra");
        int idOrg = data.getInt("org");
        int idUsr = data.getInt("usr");

        return listarUnidades(idPlan,idArea,idGrado,idOrg,idUsr);
    }

    private WebResponse listarUnidades(int idPlan, int idArea, int idGrado, int idOrg, int idUsr) {
        try{
            ProgramacionAnualDao progDao = (ProgramacionAnualDao) FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            PlanEstudiosDao planDao = (PlanEstudiosDao) FactoryDao.buildDao("mech.PlanEstudiosDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");

            PlanEstudios plan = idPlan == -1 ? planDao.buscarVigentePorOrganizacion(idOrg) : progDao.buscarPlanEstudios(idPlan);
            Docente docente = docDao.buscarDocentePorUsuario(idUsr);

            List<UnidadDidactica> unidades = unidadDao.listarUnidadesArea(plan.getPlaEstId(),docente.getDoc_id(),idArea,idGrado);
            JSONArray unidadesJSON = new JSONArray(EntityUtil.listToJSONString(
                    new String[]{"uniDidId", "tit","tip","fecIni","fecFin"},
                    new String[]{"cod", "tit","tip","ini","fin"},
                    unidades
            ));
            return WebResponse.crearWebResponseExito("Se listo con exito",unidadesJSON);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarUnidades",e);
            return WebResponse.crearWebResponseError("No se puede listar las unidades");
        }
    }
}
