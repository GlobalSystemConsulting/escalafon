package com.dremo.ucsm.gsc.sigesmed.logic.maestro.acomp.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.acomp.AcompanamientoDocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.AreaCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.acomp.AcompanamientoDocente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.acomp.CompromisosAcompanamiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 06/01/2017.
 */
public class RegistrarAcompanamientoTx implements ITransaction{
    private static Logger logger = Logger.getLogger(RegistrarAcompanamientoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject)wr.getData();
        int idDoc = data.getInt("doc");
        int idOrg = data.getInt("org");
        int idAutor = data.optInt("usu", -1);
        int idNivel = data.optInt("niv",-1);
        int idGrado = data.optInt("gra",-1);
        char idSeccion = data.optString("sec"," ").charAt(0);
        int idArea = data.optInt("are",-1);
        int res = data.optInt("res");
        long fec = data.optLong("fec", new Date().getTime());
        JSONArray compromisosJSON = data.optJSONArray("comp");
        return registrarAcompanamiento(idDoc,idOrg,idAutor,idNivel,idGrado,idSeccion,idArea,res,fec,compromisosJSON);
    }

    private WebResponse registrarAcompanamiento(int idDoc,int idOrg, int idAutor,int idNivel,int idGrado,char idSeccion,int idArea, int res, long fec, JSONArray compromisosJSON) {
        try{
            AcompanamientoDocenteDao acomDao = (AcompanamientoDocenteDao) FactoryDao.buildDao("maestro.acomp.AcompanamientoDocenteDao");
            DocenteDao docDao = (DocenteDao) FactoryDao.buildDao("maestro.DocenteDao");
            OrganizacionDao orgDao = (OrganizacionDao) FactoryDao.buildDao("OrganizacionDao");
            AreaCurricularDao areDao = (AreaCurricularDao) FactoryDao.buildDao("maestro.plan.AreaCurricularDao");


            AcompanamientoDocente acomp = new AcompanamientoDocente(res,new Date(fec));
            acomp.setDocente(docDao.buscarDocentePorId(idDoc));
            acomp.setOrganizacion(orgDao.buscarConTipoOrganizacionYPadre(idOrg));
            if(idAutor!= -1)acomp.setAutor(docDao.buscarUsuarioPorId(idAutor));
            if(idNivel != -1) acomp.setNivel(docDao.buscarNivelPorID(idNivel));
            if(idGrado != -1) acomp.setGrado(docDao.buscarGradoPorID(idGrado));
            if(idSeccion != ' ') acomp.setSeccion(docDao.buscarSeccionPorID(idSeccion));
            if(idArea!= -1)acomp.setArea(areDao.buscarPorId(idArea));
            acomDao.insert(acomp);
            if(compromisosJSON != null){
                for(int i = 0; i < compromisosJSON.length();i++){
                    JSONObject compromisoJSON = compromisosJSON.getJSONObject(i);
                    String des = compromisoJSON.optString("des");
                    boolean est = compromisoJSON.optBoolean("est");
                    CompromisosAcompanamiento compromiso = new CompromisosAcompanamiento(des,est);
                    compromiso.setAcomp(acomp);
                    acomp.getCompromisos().add(compromiso);
                }
                acomDao.update(acomp);
            }
            JSONObject acompJSON = new JSONObject(EntityUtil.objectToJSONString(
                    new String[]{"acoId", "fec", "res"},
                    new String[]{"id", "fec", "res"},
                    acomp
            ));
            /*acompJSON.put("nivel",new JSONObject(EntityUtil.objectToJSONString(new String[]{"nivId","nom"},new String[]{"id","nom"},acomp.getNivel())));
            acompJSON.put("grado",new JSONObject(EntityUtil.objectToJSONString(new String[]{"graId","nom"},new String[]{"id","nom"},acomp.getGrado())));
            acompJSON.put("seccion",new JSONObject(EntityUtil.objectToJSONString(new String[]{"sedId","nom"},new String[]{"id","nom"},acomp.getSeccion())));
            acompJSON.put("area",new JSONObject(EntityUtil.objectToJSONString(new String[]{"areCurId","nom","abr"},new String[]{"id","nom","abr"},acomp.getArea())));*/

            return WebResponse.crearWebResponseExito("Se listo correctamente los acompanamietos",acompJSON);

        }catch (Exception e){
            logger.log(Level.SEVERE,"registrarAcompanamiento",e);
            return WebResponse.crearWebResponseError("No se puede registrar el acompanamiento");
        }
    }
}
