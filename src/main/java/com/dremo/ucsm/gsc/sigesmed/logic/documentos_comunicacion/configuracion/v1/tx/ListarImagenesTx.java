/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_comunicacion.configuracion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sdc.PlantillaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.ImagenPlantilla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sdc.Plantilla;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarImagenesTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Integer plantillaId;
        Plantilla _plantilla =null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            plantillaId = requestData.getInt("plantillaID");        
            _plantilla = new Plantilla(plantillaId);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar las Imagenes Adjuntas", e.getMessage() );
        }

        List<ImagenPlantilla> imagenes = new ArrayList<>();
        PlantillaDao plantillaDao = (PlantillaDao)FactoryDao.buildDao("sdc.PlantillaDao");
        try{
            imagenes =plantillaDao.listarImagenes(_plantilla);
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Listar las Imagenes ", e.getMessage() );
        }

        JSONArray miArray = new JSONArray();
       
        for(ImagenPlantilla img:imagenes ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("descripcion",img.getDescripcion());
            oResponse.put("nombreArchivo",img.getImgAdj());
            oResponse.put("url",_plantilla.getUrlImagenes());
            oResponse.put("imgID",img.getImgPlaId());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

