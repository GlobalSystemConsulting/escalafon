/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TipoDocDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.TipoDoc;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TipoDocDaoHibernate extends GenericDaoHibernate<TipoDoc> implements TipoDocDao{

    @Override
    public List<TipoDoc> listarAll() {
        List<TipoDoc> documentos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT d from TipoDoc as d "
                    + "WHERE d.estReg='A'";
            Query query = session.createQuery(hql);
            documentos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los datos Organigramas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los datos Organigramas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return documentos;
    }

    @Override
    public TipoDoc buscarPorId(Integer tipCatId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        TipoDoc des = (TipoDoc)session.get(TipoDoc.class, tipCatId);
        session.close();
        return des;
    }
    
}
