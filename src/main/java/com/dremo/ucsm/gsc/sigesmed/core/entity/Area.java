package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="area" ,schema="administrativo")
public class Area  implements java.io.Serializable {

    @Id
    @Column(name="are_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_subare", sequenceName="administrativo.area_are_id_seq" )
    @GeneratedValue(generator="secuencia_subare")
    private int areId;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="org_id", nullable=false)
    private Organizacion organizacion;
        
    @Column(name="tip_are_id", insertable=false,updatable=false )
    private int tipAreID;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tip_are_id")
    private TipoArea tipoArea;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_pad_id")
    private Area areaPadre;
    @Column(name="cod", nullable=false, length=16)
    private String cod;
    @Column(name="nom", nullable=false, length=64)
    private String nom;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    @Column(name="usu_mod")
    private Integer usuMod;
    @Column(name="est_reg", length=1)
    private char estReg;
    
    @Column(name="res_id")
    private Integer resId;

    @OneToMany( mappedBy="areaPadre")
    private List<Area> areas;
    
    @OneToMany( mappedBy="area")
    private List<UsuarioSession> usuarios;

    public Area() {
    }
    public Area(int areId) {
        this.areId = areId;
    }

    public Area(int areId, Organizacion organizacion,TipoArea tipoArea,String cod, String nom, Date fecMod, Integer usuMod, char estReg) {
       this.areId = areId;
       this.organizacion = organizacion;
       this.tipoArea = tipoArea;
       this.cod = cod;
       this.nom = nom;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
    public Area(int areId, Organizacion organizacion, Area areaPadre,TipoArea tipoArea, String cod, String nom, Date fecMod, Integer usuMod, char estReg) {
       this.areId = areId;
       this.organizacion = organizacion;
       this.tipoArea = tipoArea;
       this.areaPadre = areaPadre;
       this.cod = cod;
       this.nom = nom;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
    public int getAreId() {
        return this.areId;
    }
    
    public void setAreId(int areId) {
        this.areId = areId;
    }

    public Organizacion getOrganizacion() {
        return this.organizacion;
    }    
    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }
    
    public TipoArea getTipoArea() {
        return this.tipoArea;
    }    
    public void setTipoArea(TipoArea tipoArea) {
        this.tipoArea = tipoArea;
    }
    
    public int getTipAreID() {
        return this.tipAreID;
    }    
    public void setTipAreID(int tipAreID) {
        this.tipAreID = tipAreID;
    }
    
    public Area getAreaPadre() {
        return this.areaPadre;
    }    
    public void setAreaPadre(Area areaPadre) {
        this.areaPadre = areaPadre;
    }

    
    public String getCod() {
        return this.cod;
    }
    
    public void setCod(String cod) {
        this.cod = cod;
    }

    
    public String getNom() {
        return this.nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    public Integer getUsuMod() {
        return this.usuMod;
    }    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }
    
    public Integer getResId() {
        return this.resId;
    }    
    public void setResId(Integer redId) {
        this.resId = resId;
    }
    
    public char getEstReg() {
        return this.estReg;
    }    
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public List<Area> getAreas() {
        return this.areas;
    }
    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public List<UsuarioSession> getUsuarios() {
        return this.usuarios;
    }
    public void setUsuarios(List<UsuarioSession> usuarios) {
        this.usuarios = usuarios;
    }


}


