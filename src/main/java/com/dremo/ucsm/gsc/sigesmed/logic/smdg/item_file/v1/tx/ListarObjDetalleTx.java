/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ObjetivosDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ObjetivosDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONObject;

/**
 *
 * @author zeta
 */
public class ListarObjDetalleTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        JSONObject requestData = (JSONObject)wr.getData();
        int objide = requestData.getInt("objide");
              
        ObjetivosDetalleDao detalleDao = (ObjetivosDetalleDao)FactoryDao.buildDao("smdg.ObjetivosDetalleDao");
        ObjetivosDetalle detalle = null;
        
        try{
            detalle = detalleDao.listar(objide);
            
        }catch(Exception e){            
            return WebResponse.crearWebResponseError("No se pudo Listar los detalle ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("odeide",detalle.getOdeId());
        oResponse.put("indicador",detalle.getOdeInd());
        oResponse.put("meta",detalle.getOdeMet());
        oResponse.put("odeava",detalle.getOdeAva());
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);
                
    }    
}
