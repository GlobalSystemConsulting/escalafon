/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate;

import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaCertificadaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ConsultaCertificada;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ConsultaCertificadaDaoHibernate extends GenericDaoHibernate<ConsultaCertificada> implements ConsultaCertificadaDao{
    @Override
    public ConsultaCertificada buscarPorID(int con_cer_id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        ConsultaCertificada c = (ConsultaCertificada)session.get(ConsultaCertificada.class, con_cer_id);
        session.close();
        return c;
    }
    @Override
    public List<ConsultaCertificada> listarTodasConsultas() {
        List<ConsultaCertificada> consultaCertificada = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT cc FROM ConsultaCertificada as cc "+
                    "WHERE cc.estReg != 'F'";
            Query query = session.createQuery(hql);            
            consultaCertificada = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return consultaCertificada;
    }
    @Override
    public List<ConsultaCertificada> listarConsultasxUsuario(int usuId) {
        List<ConsultaCertificada> consultaCertificada = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT cc FROM ConsultaCertificada as cc "+
                    "WHERE cc.IdUsuEmisor = " + usuId+ "AND cc.estReg = 'F' ";
            Query query = session.createQuery(hql);            
            consultaCertificada = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return consultaCertificada;
    }
    @Override
    public List<ConsultaCertificada> listarConsultasCertificadasxUsuario(int usuId) {
        List<ConsultaCertificada> consultaCertificada = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT cc FROM ConsultaCertificada as cc "+
                    "WHERE cc.IdUsuEmisor = " + usuId+ "AND cc.estReg != 'F' ";
            Query query = session.createQuery(hql);            
            consultaCertificada = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return consultaCertificada;
    }
    
    @Override
    public boolean verificarPassword(int usuId, String pass) {
        Usuario usuario = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        boolean verificacion = false;
        try{
            String hql = "SELECT us FROM Usuario as us "+
                    "WHERE us.usuId = " + usuId+ "AND us.pas = '" + pass+"'";
            Query query = session.createQuery(hql);            
            if(!(query.list().isEmpty())){
                verificacion = true;
            }
            t.commit();
            
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return verificacion;
    }
    
    /*@Override
    public List<ConsultaCertificada> listarxConsultasFrecuentes() {
        List<ConsultaCertificada> consultaCertificada = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT cc FROM ConsultaCertificada as cc ";
            Query query = session.createQuery(hql);            
            consultaCertificada = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar Consultas Certificadas \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return consultaCertificada;
    }*/
    
}
