/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ConsultaGeneralDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.MetadataConsultaGeneral;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
/**
 *
 * @author root
 */
public class ListarCatalogoConsultaGeneral2Tx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *  Parte para la operacion en la Base de Datos
        */        
        JSONObject requestData = (JSONObject)wr.getData();  
        List<MetadataConsultaGeneral> catalogo = new ArrayList<MetadataConsultaGeneral>();
        ConsultaGeneralDao metadataConsultaGeneral = (ConsultaGeneralDao)FactoryDao.buildDao("ConsultaGeneralDao");
        try{
            catalogo = metadataConsultaGeneral.listarTodaMetadata();
        
        }catch(Exception e){
            System.out.println("No se pudo Listar las consultas certificadas  \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las consultas certificadas ", e.getMessage() );
        }
        //EMPAQUETAMOS EN JSON///
        JSONArray miArray = new JSONArray();
        for(MetadataConsultaGeneral cMetadata:catalogo ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("entity_name", cMetadata.getNombreTabla());
            oResponse.put("atrib_name", cMetadata.getNombreAtributo());
            oResponse.put("alias_name", cMetadata.getAliasAtributo());
            oResponse.put("related_function", cMetadata.getFuncionRelacionada());
            oResponse.put("data_type", cMetadata.getTipoDato());
            oResponse.put("id_met", cMetadata.getId());
            oResponse.put("mod", cMetadata.getModulo());
            oResponse.put("est", cMetadata.getEstReg());
            /*oResponse.put("fec_emi", cMetadata.getFecEmision());
            oResponse.put("usu_emi", cMetadata.getUsuEmisor());
            oResponse.put("fec_cer", cMetadata.getFecCertificacion());
            oResponse.put("est", cMetadata.getEstReg());
            oResponse.put("sql_que", cMetadata.getConsultaSql());*/
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);
     
    }
}
