/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author carlos
 */
public class AsignarHorarioByCargoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
       
        TrabajadorCargo cargoTrab_=null;
        Integer horarioCab;
        Organizacion organizacion=null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
      
            horarioCab=requestData.getInt("idHorario");
            Integer cargoTrab=requestData.getInt("idCargo");
            Integer orgId=requestData.getInt("organizacionID");
            cargoTrab_=new TrabajadorCargo(Short.parseShort(cargoTrab+""));
            
            organizacion=new Organizacion(orgId);
           
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }
        
       
        HorarioDao horarioDao = (HorarioDao)FactoryDao.buildDao("cpe.HorarioDao");
        HorarioCab horarioC=new HorarioCab(horarioCab);
        try{
            horarioDao.asignarHorarioByCargo(horarioCab,cargoTrab_);
            horarioDao.actualizarHorariosByCargo(horarioC,cargoTrab_,organizacion);
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Asignar el Horario", e.getMessage() );
        }

        return WebResponse.crearWebResponseExito("Se Asigno correctamente");        
        //Fin
    }
    
}

