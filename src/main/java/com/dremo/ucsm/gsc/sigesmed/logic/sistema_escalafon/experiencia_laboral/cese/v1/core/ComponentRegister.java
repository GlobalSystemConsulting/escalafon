/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.cese.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.cese.v1.tx.*;


public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("cese");
        seComponent.setVersion(1);
        
        seComponent.addTransactionGET("listarCeses", ListarCesesTx.class);
	seComponent.addTransactionPOST("agregarCese", AgregarCeseTx.class);
        seComponent.addTransactionPUT("actualizarCese", ActualizarCeseTx.class);
        seComponent.addTransactionDELETE("eliminarCese", EliminarCeseTx.class);
        
        return seComponent;
    }
}
