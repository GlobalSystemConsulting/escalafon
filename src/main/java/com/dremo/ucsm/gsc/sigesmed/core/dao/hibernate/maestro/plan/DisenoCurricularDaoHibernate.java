package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.DisenoCurricularDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.DisenoCurricular;
import org.hibernate.Session;


/**
 * Created by Administrador on 19/10/2016.
 */
public class DisenoCurricularDaoHibernate extends GenericDaoHibernate<DisenoCurricular> implements DisenoCurricularDao{
    @Override
    public DisenoCurricular buscarPorId(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            DisenoCurricular diseno = (DisenoCurricular)session.get(DisenoCurricular.class,id);
            return diseno;
        }catch (Exception e){
            throw  e;
        }
    }
}
