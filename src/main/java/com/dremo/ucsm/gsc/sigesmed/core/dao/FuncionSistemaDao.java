/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.RolFuncionModel;
import java.util.List;

/**
 *
 * @author abel
 */
public interface FuncionSistemaDao extends GenericDao<FuncionSistema>{
    
    public List<FuncionSistema> buscarConSubModulo();
    public List<FuncionSistema> buscarPorRol(int rolId);
    public List<RolFuncionModel> buscarFuncionesPersonalizadasPorRol(int rolId);
}
