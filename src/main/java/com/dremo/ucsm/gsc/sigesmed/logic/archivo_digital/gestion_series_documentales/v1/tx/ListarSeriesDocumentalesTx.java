/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.SerieDocumentalDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.SerieDocumental;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 *
 * @author Jeferson
 */
public class ListarSeriesDocumentalesTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int uni_org_id = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            uni_org_id = requestData.getInt("uni_org_id");
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Series Documentales ", e.getMessage() );
        }
        
        List<SerieDocumental> series_documentales = null;
        SerieDocumentalDAO serie_doc_dao = (SerieDocumentalDAO)FactoryDao.buildDao("sad.SerieDocumentalDAO");
        try{
            series_documentales = serie_doc_dao.buscarPorUnidadOrganica(uni_org_id);
            
        }catch(Exception e){
            System.out.println("No se pudo Listar las Series Documentales\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Series Documentales", e.getMessage() );
        }
        JSONArray miArray = new JSONArray();
        for(SerieDocumental ser_doc : series_documentales){
            JSONObject oResponse = new JSONObject();
            oResponse.put("serie_id",ser_doc.getSerDocId());
            oResponse.put("cod_serie",ser_doc.getCodigo());
            oResponse.put("nom_serie",ser_doc.getNombre());
            oResponse.put("val_serie",ser_doc.getValor());
            miArray.put(oResponse);
        }
        return WebResponse.crearWebResponseExito("Se Listo las Series Documentales Correctamente",miArray); 
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
