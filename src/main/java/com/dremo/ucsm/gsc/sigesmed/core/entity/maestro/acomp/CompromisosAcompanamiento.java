package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.acomp;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 06/01/2017.
 */
@Entity
@Table(name = "compromisos_acompanamientos",schema = "pedagogico")
public class CompromisosAcompanamiento implements java.io.Serializable {
    @Id
    @Column(name = "com_aco_id", nullable = false,unique = true)
    @SequenceGenerator(name = "compromisos_acompanamientos_com_aco_id_seq",sequenceName = "pedagogico.compromisos_acompanamientos_com_aco_id_seq")
    @GeneratedValue(generator = "compromisos_acompanamientos_com_aco_id_seq")
    private int comAcoId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aco_id")
    private AcompanamientoDocente acomp;
    @Column(name = "des",length = 254)
    private String des;
    @Column(name = "est")
    private Boolean est;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod",length = 29)
    private Date fecMod;
    @Column(name = "est_reg",length = 1)
    private Character estReg;

    public CompromisosAcompanamiento() {
    }

    public CompromisosAcompanamiento(String des, Boolean est) {
        this.des = des;
        this.est = est;

        this.fecMod = new Date();
        this.estReg = 'A';
    }

    public int getComAcoId() {
        return comAcoId;
    }

    public void setComAcoId(int comAcoId) {
        this.comAcoId = comAcoId;
    }

    public AcompanamientoDocente getAcomp() {
        return acomp;
    }

    public void setAcomp(AcompanamientoDocente acomp) {
        this.acomp = acomp;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Boolean getEst() {
        return est;
    }

    public void setEst(Boolean est) {
        this.est = est;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
}
