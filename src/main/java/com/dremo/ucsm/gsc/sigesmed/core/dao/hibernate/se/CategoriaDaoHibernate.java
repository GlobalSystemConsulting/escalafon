/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Felipe
 */
public class CategoriaDaoHibernate extends GenericDaoHibernate<Categoria> implements CategoriaDao{

    @Override
    public List<Categoria> listarxCategoria() {
        List<Categoria> categorias = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT c from Categoria as c "
                    + "WHERE c.estReg='A'";
            Query query = session.createQuery(hql);
            categorias = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los categorias \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las categorias \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return categorias;
    }
    @Override
    public Categoria buscarPorId(Integer catId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Categoria item = (Categoria)session.get(Categoria.class, catId);
        session.close();
        return item;
    }

    @Override
    public List<Categoria> listarxPlanilla(int plaId) {
        List<Categoria> categorias = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT c from Categoria as c "
                    + "JOIN FETCH c.planilla pla "
                    + "WHERE pla.plaId='"+plaId+"' "
                    + "AND c.estReg='A'";
            Query query = session.createQuery(hql);
            categorias = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los categorias \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar las categorias \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return categorias;
    }
    
    @Override
    public Categoria obtenerDetalle(int catId) {
        Categoria categoria = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT c from Categoria as c "
                    + "JOIN FETCH c.planilla pla "
                    + "WHERE c.catId='"+catId+"' "
                    + "AND c.estReg='A'";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            categoria = (Categoria) query.uniqueResult();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar la categoria \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar la categoria \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return categoria;
    }
    
    
}
