package com.dremo.ucsm.gsc.sigesmed.core.entity.mmi;
// Generated 01/03/2017 11:50:54 AM by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * PlanNivel generated by hbm2java
 */
@Entity
@Table(name = "plan_nivel", schema = "institucional"
)
public class PlanNivelMMI implements java.io.Serializable {

    private int plaNivId;
    private PeriodoMMI periodo;
    private PlanEstudiosMMI planEstudios;
    private TurnoMMI turno;
    private JornadaEscolarMMI jornadaEscolar;
    private String des;

    public PlanNivelMMI() {
    }

    public PlanNivelMMI(int plaNivId, PeriodoMMI periodo, PlanEstudiosMMI planEstudios, TurnoMMI turno, JornadaEscolarMMI jornadaEscolar) {
        this.plaNivId = plaNivId;
        this.periodo = periodo;
        this.planEstudios = planEstudios;
        this.turno = turno;
        this.jornadaEscolar = jornadaEscolar;
    }

    public PlanNivelMMI(int plaNivId, PeriodoMMI periodo, PlanEstudiosMMI planEstudios, TurnoMMI turno, JornadaEscolarMMI jornadaEscolar, String des) {
        this.plaNivId = plaNivId;
        this.periodo = periodo;
        this.planEstudios = planEstudios;
        this.turno = turno;
        this.jornadaEscolar = jornadaEscolar;
        this.des = des;
    }

    @Id

    @Column(name = "pla_niv_id", unique = true, nullable = false)
    public int getPlaNivId() {
        return this.plaNivId;
    }

    public void setPlaNivId(int plaNivId) {
        this.plaNivId = plaNivId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "per_id", nullable = false)
    public PeriodoMMI getPeriodo() {
        return this.periodo;
    }

    public void setPeriodo(PeriodoMMI periodo) {
        this.periodo = periodo;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pla_est_id", nullable = false)
    public PlanEstudiosMMI getPlanEstudios() {
        return this.planEstudios;
    }

    public void setPlanEstudios(PlanEstudiosMMI planEstudios) {
        this.planEstudios = planEstudios;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tur_id", nullable = false)
    public TurnoMMI getTurno() {
        return this.turno;
    }

    public void setTurno(TurnoMMI turno) {
        this.turno = turno;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "jor_esc_id", nullable = false)
    public JornadaEscolarMMI getJornadaEscolar() {
        return this.jornadaEscolar;
    }

    public void setJornadaEscolar(JornadaEscolarMMI jornadaEscolar) {
        this.jornadaEscolar = jornadaEscolar;
    }

    @Column(name = "des", length = 256)
    public String getDes() {
        return this.des;
    }

    public void setDes(String des) {
        this.des = des;
    }

}
