package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scec.AcuerdosActaComisionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.AcuerdosActaComision;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by geank on 30/09/16.
 */
public class AcuerdosActaComisionDaoHibernate extends GenericDaoHibernate<AcuerdosActaComision> implements AcuerdosActaComisionDao{
    private static final Logger logger = Logger.getLogger(AcuerdosActaComisionDaoHibernate.class.getName());
    @Override
    public List<AcuerdosActaComision> buscarAcuerdosPorActa(int idActa) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Criteria query = session.createCriteria(AcuerdosActaComision.class)
                    .setFetchMode("integranteComision", FetchMode.JOIN)
                    .createCriteria("actaReunion").add(Restrictions.eq("actComId",idActa));
            return query.list();
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarAcuerdosPorActa",e);
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public AcuerdosActaComision buscarAcuerdosPorId(int idAcu) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            AcuerdosActaComision acuerdo = (AcuerdosActaComision)session.get(AcuerdosActaComision.class, idAcu);

            return acuerdo;
        }catch (Exception e){
            logger.log(Level.SEVERE,"buscarAcuerdosPorId",e);
            throw e;
        }finally {
            session.close();
        }
    }
}
