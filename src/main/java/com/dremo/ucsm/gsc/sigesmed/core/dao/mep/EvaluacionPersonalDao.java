package com.dremo.ucsm.gsc.sigesmed.core.dao.mep;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.*;

import java.util.List;
import java.util.Set;

/**
 * Created by Administrador on 13/10/2016.
 */
public interface EvaluacionPersonalDao extends GenericDao<ResumenEvaluacionPersonal>{
    List<ResumenEvaluacionPersonal> getResumenEvaluacion(int idTrab);
    List<EvaluacionesPersonal> getCantidadEvaluacionesTrabajador(int idOrganizacion);
    ResumenEvaluacionPersonal getResumentEvaluacionById(int idResumen);
    List<Object[]> getDominiosEvaluacionDocente(int idResumen);
    Object[] getDetalleEvaluacionByContenido(int idResumen,int idCont);
    List<Object> getEscalasByRol(String tipoTra);
    List<Object> getDetalleFichaEvaluacion(int idResumen);
    List<IndicadoresEvaluarPersonal> getIndicadoresEvaluadosResumen(int idResumen);
    public Trabajador getTrabajadorById(int id);
    List<PuntajesEvaluacionTrabajador> getPuntajesEvaluacionTrabajador(int trabajadorId);
    List<ResultadoEstadistica> getPuntajeAgrupadoTrabajadores(int idOrg);
    List<ResumenEvaluacionPersonal> getEvaluacionesCompartidas(int idTrabajador,int idOrg);
    boolean registrarEvaluacionFicha(ResumenEvaluacionPersonal resumen, Set<IndicadorValor> indicadores, Set<DetalleResumenEvaluacionPersonal> detalleResumen);
    boolean compartirResultado(int idResumen);
    Trabajador buscarTrabajadorPorPersona(int idPersona);
}
