/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.desplazamiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CargoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.CategoriaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.OrganigramaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.UbicacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Organigrama;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Ubicacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarDesplazamientosTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarDesplazamientosTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
         /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ficEscId = requestData.getInt("ficEscId");
                
        List<Desplazamiento> desplazamientos = null;
        DesplazamientoDao desplazamientoDao = (DesplazamientoDao)FactoryDao.buildDao("se.DesplazamientoDao");
        
        CategoriaDao categoriaDao =(CategoriaDao)FactoryDao.buildDao("se.CategoriaDao");
        Categoria categoria = new Categoria();
        CargoDao cargoDao =(CargoDao)FactoryDao.buildDao("se.CargoDao");
        Cargo cargo= new Cargo();
        UbicacionDao ubicacionDao =(UbicacionDao)FactoryDao.buildDao("se.UbicacionDao");
        Ubicacion ubicacion= new Ubicacion();
        OrganigramaDao organigramaDao =(OrganigramaDao)FactoryDao.buildDao("se.OrganigramaDao");
        Organigrama organigrama=null;
        
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
        
        try{
            desplazamientos = desplazamientoDao.listarxFichaEscalafonaria(ficEscId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar desplazamientos",e);
            System.out.println("No se pudo listar los desplazamientos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar los desplazamientos", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Desplazamiento d:desplazamientos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("desId", d.getDesId());
            oResponse.put("tip", d.getTip());
            oResponse.put("tipDes", "");
            switch(d.getTip()){
                case '1':
                case '2': oResponse.put("numeroDoc", null != d.getNumDoc() ? d.getNumDoc():"");
                        oResponse.put("fechaDoc", null != d.getFecDoc() ? sdo.format(d.getFecDoc()) : "");
                        oResponse.put("fechaDes", null != d.getFecIni() ? sdo.format(d.getFecIni()) : "");
                        break;
                case '3': oResponse.put("numeroDoc", null != d.getNumDocTer() ? d.getNumDocTer() : ""); 
                        oResponse.put("fechaDoc", null != d.getFecDocTer() ? sdo.format(d.getFecDocTer()) : "");
                        oResponse.put("fechaDes", null != d.getFecTer() ? sdo.format(d.getFecTer()) : "");
                        break;
            }
            
            oResponse.put("numDoc", null != d.getNumDoc() ? d.getNumDoc():"");
            oResponse.put("fecDoc", null != d.getFecDoc() ? sdo.format(d.getFecDoc()) : "");
            oResponse.put("tipDocId", null != d.getTipDocId() ? d.getTipDocId() : "");
            oResponse.put("numDocTer", null != d.getNumDocTer() ? d.getNumDocTer() : "");
            oResponse.put("fecDocTer", null != d.getFecDocTer() ? sdo.format(d.getFecDocTer()) : "");
            oResponse.put("tipDocTer", null != d.getTipDocTer() ? d.getTipDocTer() : "");
            oResponse.put("jorLabId", null != d.getJorLabId() ? d.getJorLabId() : "");
            oResponse.put("fecIni", null != d.getFecIni() ? sdo.format(d.getFecIni()) : "");
            oResponse.put("fecTer", null != d.getFecTer() ? sdo.format(d.getFecTer()) : "");
            oResponse.put("motRet", null != d.getFecTer() && d.getMotRet().length()>0 ? d.getMotRet() : "");
            
            //Historico
            if(d.getOrgiActId()!=null && d.getOrgiActId()>0){
                organigrama=organigramaDao.obtenerDetalle(d.getOrgiActId());
                oResponse.put("orgiActId", d.getOrgiActId());
                oResponse.put("datOrgiId", d.getCatActId());
                oResponse.put("nomDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getNomDatOrgi());
                oResponse.put("anioDatOrgi", organigrama.getTipoOrganigrama().getDatosOrganigramas().getAnioDatOrgi());
                oResponse.put("nomUbi", organigrama.getUbiId());
                
                oResponse.put("nomOrgiN3", organigrama.getNomOrgi());
                oResponse.put("nomOrgiN2", organigrama.getOrganigramaPadre().getNomOrgi());
                oResponse.put("nomOrgiN1", organigrama.getOrganigramaPadre().getOrganigramaPadre().getNomOrgi());
            }
            else{
                oResponse.put("orgiActId", 0);
                oResponse.put("datOrgiId", 0);
                oResponse.put("nomOrgi", "");
                oResponse.put("nomDatOrgi", "");
                oResponse.put("anioDatOrgi", 0);
                oResponse.put("nomUbi", 0);
            }

            if(d.getCatActId()!=null && d.getCatActId()>0){
                categoria=categoriaDao.obtenerDetalle(d.getCatActId());
                oResponse.put("catActId", d.getCatActId());
                oResponse.put("nomCat", categoria.getNomCat());
                oResponse.put("nomPla", categoria.getPlanilla().getNomPla());
            }
            else{
                oResponse.put("catActId", 0);
                oResponse.put("nomCat", "");
                oResponse.put("nomPla", "");
            }

            if(d.getCarActId()!=null && d.getCarActId()>0){
                cargo=cargoDao.buscarPorId(d.getCarActId());
                oResponse.put("carActId", d.getCarActId());
                oResponse.put("nomCar", cargo.getNomCar());            
            }
            else{
                oResponse.put("carActId", 0);
                oResponse.put("nomCar", "");
            }
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Los desplazamientos fueron listados exitosamente", miArray);
    }
}
