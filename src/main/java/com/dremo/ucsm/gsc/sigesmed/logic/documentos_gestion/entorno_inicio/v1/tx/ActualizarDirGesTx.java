/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.PlazoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Plazo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ActualizarDirGesTx implements ITransaction{
    Integer ideFil=0;
    String urlPropia = "";
    Integer tamUrlDirPadre = 0;
    
    String urlAlm = "";
    Integer tamUrlAlm = 0;
    
    Integer idePadFil = 0;
    String nomFilIni = "";
    String nomPad = "";
    String nomFil = "";
    String strFecMod = "";
    Date fechaMod = null;
    Integer idUsuario = 0;
    String urlAlmIni = "";
    int codPadre = 0;
    int idPadre = 0;
    SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
    Date plazoIni=null;
    Date plazoFin=null;
    boolean editPlazo=false;
    boolean deletePlazo=false;
    boolean flagProteccionEditar=false;
    String proteccion="";
    ItemFileDao iteFilDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
    
    private JSONArray listDirs = new JSONArray();

    @Override
    public WebResponse execute(WebRequest wr) {
        /* LECTURA DE DATOS */
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            ideFil = requestData.getInt("ideFil");
            urlPropia = requestData.getString("urlPropia");    
            tamUrlDirPadre = urlPropia.length();
            idePadFil = requestData.getInt("idePadFil");
            nomPad = requestData.getString("nomPad");
            nomFil = requestData.getString("nomFil");
            strFecMod = requestData.getString("fecMod");
            SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");
            fechaMod = formatFecha.parse(strFecMod);
            idUsuario = (requestData.getInt("usuarioID"));
            urlAlmIni = requestData.getString("urlAlm");
            urlAlm = requestData.getString("urlAlm") +"/"+ nomPad;
            tamUrlAlm = urlAlm.length();
            editPlazo=requestData.getBoolean("flagControlPlazo");
            if(editPlazo){
                String strfeciniPlazo = requestData.getString("plazoIni");
                String strfecfinPlazo = requestData.getString("plazoFin");
                //Construyendo la fecha de un string
                try {
                    plazoIni = formatoDelTexto.parse(strfeciniPlazo);
                    plazoFin = formatoDelTexto.parse(strfecfinPlazo);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            }
            deletePlazo=requestData.getBoolean("flagEliminarPlazoEditar");
            flagProteccionEditar=requestData.getBoolean("flagProteccionEditar");
            if(flagProteccionEditar)
                proteccion=requestData.getString("proteccion");
                
                
            
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo leer la data enviada");        
        }
        
        /* TRABAJO CON LA BASE DE DATOS*/
        //Antes debemos encontrar el directorio a modificar
        ItemFile dirActual = iteFilDao.buscarPorID(ideFil);
        
        nomFilIni = dirActual.getIteNom();
        //Modificamos los valores iniciales
        dirActual.setIteNom(nomFil);
        dirActual.setFecMod(fechaMod);
        dirActual.setIteUsuIde(idUsuario);
        dirActual.setIteProt(proteccion);
        
        if(idePadFil != ideFil){
            //El  unico que actualizara su padre sera el directorio actual
            ItemFile iteFilPadre = iteFilDao.buscarPorID(idePadFil);
            dirActual.setItePadIde(iteFilPadre);
            dirActual.setIteUrlDes(urlAlm);
        }
        if(deletePlazo){   
            dirActual.setItePlaIde(new Plazo(0));
        }else{
            if(editPlazo){
                PlazoDao plazoDao=(PlazoDao)FactoryDao.buildDao("rdg.PlazoDao");

                Plazo p=plazoDao.buscarPorID(dirActual.getItePlaIde());
                if(p!=null){                  
                    if(p.getPlzIde()==0){
                        p= new Plazo(plazoIni, plazoFin);
                        plazoDao.insert(p);
                        dirActual.setItePlaIde(p);
                    }else{
                        p.setPlzFecIniSub(plazoIni);
                        p.setPlzFecFinSub(plazoFin);
                        plazoDao.update(p);
                    }
                        
                }else{
                    p= new Plazo();
                    p.setPlzFecIniSub(plazoIni);
                    p.setPlzFecFinSub(plazoFin);
                     plazoDao.insert(p);
                     dirActual.setItePlaIde(p);
                }                
            }
        }
        iteFilDao.update(dirActual);
        File origen = null, destino = null;
        
        if(!(nomFilIni.equals(nomFil)) && (idePadFil != ideFil)){
            //En caso de que exista reactualizacion de nombre y reubicacion a al vez
            //Primero hacemos la reubicacion
            origen = new File(urlPropia + "/" + nomFilIni);
            destino = new File(urlAlm + "/");
            try {
                FileUtils.moveToDirectory(origen, destino, false);
            } catch (IOException ex) {
                Logger.getLogger(ActualizarDirGesTx.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Despues hacemos el renombrado ya reubicado
            origen = new File(urlAlm+"/"+nomFilIni);
            destino = new File(urlAlm+"/"+nomFil);
            origen.renameTo(destino);
            
        } else if(!nomFilIni.equals(nomFil)){
            //En caso de que solo exista actualizacion de nombre
            //Para esto el directorio debe ubicarse en el mismo sitio
            origen = new File(urlPropia+"/"+nomFilIni);
            destino = new File(urlAlm+"/");
            origen.renameTo(destino);
        } else if (idePadFil != ideFil) {
            //En caso de que  solo exista reubicacion
            origen = new File(urlPropia + "/" + nomFil);
            destino = new File(urlAlm + "/");
            try {
                FileUtils.moveToDirectory(origen, destino, false);
            } catch (IOException ex) {
                Logger.getLogger(ActualizarDirGesTx.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        //Buscamos todos los hijos de este directorio para que tambien sean modificados
        List<ItemFile> docs = iteFilDao.buscarAllHijosPorIdPadre(dirActual);
        //Esto implica que se reubica a un directorio de nivel posterior o anterior
        //Actualizando los hijos
        buscarAllsHijosPorPadre(docs);
        return WebResponse.crearWebResponseExito("Se modifico la carpeta"); 
    }
    public void buscarAllsHijosPorPadre(List<ItemFile> nlistDirs) {

        for (int i = 0; i < nlistDirs.size(); i++) {
            ItemFile hijoAct = nlistDirs.get(i);

            String urlFinal = "";

            String urlGen = hijoAct.getIteUrlDes();
            int tamUrlGen = hijoAct.getIteUrlDes().length();
            String urlB = urlGen.substring(tamUrlDirPadre, tamUrlGen);
            if(!nomFilIni.equals(nomFil) ){
                urlB = urlB.substring(nomFilIni.length()+1);
                urlFinal = urlAlm + urlB;
            }else
                urlFinal = urlAlm + urlB;

            //Actualizando el hijo
            hijoAct.setIteUrlDes(urlFinal);
            hijoAct.setFecMod(fechaMod);
            hijoAct.setIteUsuIde(idUsuario);
            iteFilDao.update(hijoAct);

            List<ItemFile> listHijos = iteFilDao.buscarAllHijosPorIdPadre(hijoAct);

            if (listHijos.size() != 0) {
                buscarAllsHijosPorPadre(listHijos);
            }

        }

    }

    public static String cerosIzquierda(long codigo,int numDigitos){
		
        String nuevoCodigo = String.valueOf( codigo );

        int numCeros = numDigitos - nuevoCodigo.length();

        for (int i=0;i < numCeros;i++)
            nuevoCodigo="0"+nuevoCodigo;

        return nuevoCodigo;
    }
}
