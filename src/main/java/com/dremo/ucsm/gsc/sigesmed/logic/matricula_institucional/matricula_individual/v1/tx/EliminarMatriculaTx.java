package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.matricula_individual.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GradoSeccionPlanNivelDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.MatriculaMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.GradoIeEstudianteMMI;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.MatriculaMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

public class EliminarMatriculaTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        GradoSeccionPlanNivelDaoHibernate gradoSeccion = new GradoSeccionPlanNivelDaoHibernate();

        MatriculaMMIDaoHibernate matdao = new MatriculaMMIDaoHibernate();

        int estId, matId, graId, plaNivId;
        Character secId;

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            estId = requestData.getInt("estId");
            JSONObject rpt = new JSONObject();
            MatriculaMMI myMatricula = matdao.getMatriculaByEstId(estId);

            if (myMatricula == null) {
                rpt.put("msj", "No se encontro Matricula");
                return WebResponse.crearWebResponseExito("No se encontro Matricula", rpt);
            }
            secId = myMatricula.getSecId();
            graId = myMatricula.getGraId();
            plaNivId = myMatricula.getPlaNivId();
            matId = (int) myMatricula.getMatId();
            gradoSeccion.disableEstudianteMatriculado(estId, secId, graId, plaNivId, matId);
            
            rpt.put("msj", "Matricula eliminada correctamente");
            return WebResponse.crearWebResponseExito("Se ha eliminado la matricula", rpt);
            
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al eliminar matricula! ", e.getMessage());

        }

    }
}
