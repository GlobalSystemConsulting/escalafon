package com.dremo.ucsm.gsc.sigesmed.logic.maestro.banco.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.banco.LecturaBancoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.banco.Lectura;
import com.dremo.ucsm.gsc.sigesmed.core.service.ServicioREST;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import org.json.JSONObject;

import java.io.File;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 26/12/2016.
 */
public class EditarLecturaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EditarLecturaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        return editarLectura(data);
    }

    private WebResponse editarLectura(JSONObject data) {
        try{
            int idLec = data.getInt("id");

            LecturaBancoDao lecturaDao = (LecturaBancoDao) FactoryDao.buildDao("maestro.banco.LecturaBancoDao");
            Lectura lectura = lecturaDao.buscarLecturaId(idLec);

            lectura.setNom(data.optString("noml", lectura.getNom()));
            lectura.setAut(data.optString("aut", lectura.getAut()));
            lectura.setDes(data.optString("des",lectura.getDes()));
            lectura.setTip(data.optString("tip",lectura.getTip()));
            lectura.setFecPub(new Date(data.optLong("fpub", lectura.getFecPub().getTime())));
            lectura.setFecMod(new Date());


            String nom = data.optString("nom");
            JSONObject filjson = data.optJSONObject("arc");

            if(filjson != null && filjson.length() > 0){
                if(lectura.getUbi() != null && lectura.getUbi().length() > 0){
                    //Eliminamos los archivos
                    File file = new File(ServicioREST.PATH_SIGESMED + File.separator +"archivos" + File.separator+"banco_lectura_docente" +File.separator + lectura.getUbi());
                    file.delete();
                }
                String nomFile = lectura.getUbi().split("\\.")[0];
                FileJsonObject miF = new FileJsonObject( filjson ,nomFile);
                BuildFile.buildFromBase64("banco_lectura_docente", miF.getName(), miF.getData());
                lectura.setUbi(miF.getName());
            }
            lecturaDao.update(lectura);
            return WebResponse.crearWebResponseExito("Se edito correctamente la lectura",new JSONObject().put("nom",lectura.getUbi()));
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarLectura",e);
            return WebResponse.crearWebResponseError("No se pudo editar la lectura");
        }
    }
}
