/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.comunication.pojo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.lang.reflect.Array;
import java.util.List;

/**
 *
 * @author GSC05
 */
public class Documento {
    private String dni;
    private String iddocdigital; 
    private String coddocumento;
    private int nrototalpaginas;
    private String estdocumento;
    public List<String>  srcpagina;
    public List<Agrupamientos> agrupamientos;
    private String categoria;
    private String tipodoc;
    private boolean estagrupamiento;

    public Documento(String dni, String iddocdigital, String coddocumento, int nrototalpaginas, String estdocumento, List<String> srcpagina, List<Agrupamientos> agrupamientos, String categoria, String tipodoc, boolean estdAgrupa) {
        this.dni = dni;
        this.iddocdigital = iddocdigital;
        this.coddocumento = coddocumento;
        this.nrototalpaginas = nrototalpaginas;
        this.estdocumento = estdocumento;
        this.srcpagina = srcpagina;
        this.agrupamientos = agrupamientos;
        this.categoria = categoria;
        this.tipodoc = tipodoc;
        this.estagrupamiento = estdAgrupa;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getIddocdigital() {
        return iddocdigital;
    }

    public void setIddocdigital(String iddocdigital) {
        this.iddocdigital = iddocdigital;
    }

    public String getCoddocumento() {
        return coddocumento;
    }

    public void setCoddocumento(String coddocumento) {
        this.coddocumento = coddocumento;
    }

    public int getNrototalpaginas() {
        return nrototalpaginas;
    }

    public void setNrototalpaginas(int nrototalpaginas) {
        this.nrototalpaginas = nrototalpaginas;
    }

    public String getEstdocumento() {
        return estdocumento;
    }

    public void setEstdocumento(String estdocumento) {
        this.estdocumento = estdocumento;
    }

    public List<String> getSrcpagina() {
        return srcpagina;
    }

    public void setSrcpagina(List<String> srcpagina) {
        this.srcpagina = srcpagina;
    }

    public List<Agrupamientos> getAgrupamientos() {
        return agrupamientos;
    }

    public void setAgrupamientos(List<Agrupamientos> agrupamientos) {
        this.agrupamientos = agrupamientos;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public boolean isEstdAgrupa() {
        return estagrupamiento;
    }

    public void setEstdAgrupa(boolean estdAgrupa) {
        this.estagrupamiento = estdAgrupa;
    }


    
    @Override
    public String toString() {
        return this.coddocumento;
    }    
    
    public void deterPagInicio(){
        int temp=1;
        for (Agrupamientos agrupamiento : this.agrupamientos) {
            agrupamiento.setPagInicio(temp);
            temp+=agrupamiento.getNroPagina();
        }
   
    }

}
