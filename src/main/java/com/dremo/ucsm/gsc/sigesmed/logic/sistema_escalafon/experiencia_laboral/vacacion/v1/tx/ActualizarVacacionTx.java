/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.vacacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.VacacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Vacacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class ActualizarVacacionTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ActualizarVacacionTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer vacId = requestData.getInt("vacId");
            String entEmi = requestData.getString("entEmi");
            int tipDocId = requestData.getInt("tipDocId");
            String numDoc = requestData.getString("numDoc");
            Boolean estGoc = requestData.getBoolean("estGoc");
            Date fecDoc = sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            Date fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = sdi.parse(requestData.getString("fecTer").substring(0, 10));
            String mot = requestData.getString("mot");
            int estProId = requestData.getInt("estProId");
            
            String numInf = requestData.getString("numInf");
            Date fecInf = sdi.parse(requestData.getString("fecInf").substring(0, 10));
            Date fecRecInf = sdi.parse(requestData.getString("fecRecInf").substring(0, 10));
            
            return actualizarVacacion(vacId, entEmi, tipDocId, numDoc, fecDoc, estGoc, fecIni, fecTer, mot, estProId, numInf, fecInf, fecRecInf);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar desplazamientoa",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarVacacion(Integer vacId, String entEmi, int tipDocId,  
            String numDoc, Date fecDoc, Boolean estGoc, Date fecIni, Date fecTer, String mot, int estProId,
            String numInf, Date fecInf, Date fecRecInf) {
        try{
            VacacionDao vacacionDao = (VacacionDao)FactoryDao.buildDao("se.VacacionDao");        
            Vacacion vacacion = vacacionDao.buscarPorId(vacId);

            vacacion.setEntEmiRes(entEmi);
            vacacion.setTipDocId(tipDocId);
            vacacion.setNumDoc(numDoc);
            vacacion.setFecDoc(fecDoc);
            vacacion.setEstGoc(estGoc);
            vacacion.setFecIni(fecIni);
            vacacion.setFecTer(fecTer);
            vacacion.setMotLic(mot);
            vacacion.setEstProId(estProId);
            
            vacacion.setFecInf(fecInf);
            vacacion.setFecRecInf(fecRecInf);
            vacacion.setNumInf(numInf);
            
            vacacionDao.update(vacacion);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
            oResponse.put("vacId", vacacion.getVacId());
            oResponse.put("entEmi", vacacion.getEntEmiRes());
            oResponse.put("tipDocId", vacacion.getTipDocId());
            oResponse.put("numDoc", vacacion.getNumDoc());
            oResponse.put("fecDoc", sdo.format(vacacion.getFecDoc()));
            oResponse.put("estGoc", vacacion.getEstGoc());            
            oResponse.put("fecIni", sdo.format(vacacion.getFecIni()));
            oResponse.put("fecTer", sdo.format(vacacion.getFecTer()));
            oResponse.put("mot", vacacion.getMotLic());
            oResponse.put("numInf", vacacion.getNumInf());
            oResponse.put("fecInf", sdo.format(vacacion.getFecInf()));
            oResponse.put("fecRecInf", sdo.format(vacacion.getFecRecInf()));
            oResponse.put("estProId", vacacion.getEstProId());
            return WebResponse.crearWebResponseExito("Vacacion actualizada exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarVacacion",e);
            return WebResponse.crearWebResponseError("Error, la vacacion no fue actualizada");
        }
    } 
    
}
