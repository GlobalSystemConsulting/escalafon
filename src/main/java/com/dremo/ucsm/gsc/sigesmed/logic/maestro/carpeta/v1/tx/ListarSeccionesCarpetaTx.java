package com.dremo.ucsm.gsc.sigesmed.logic.maestro.carpeta.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.carpeta.CarpetaPedagogicaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.ContenidoSeccionCarpeta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.carpeta.SeccionCarpetaPedagogica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 29/12/2016.
 */
public class ListarSeccionesCarpetaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(ListarSeccionesCarpetaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idCar = data.getInt("id");
        return listarSeccionesCarpeta(idCar);
    }

    private WebResponse listarSeccionesCarpeta(int idCar) {
        try{
            CarpetaPedagogicaDao carpetaDao = (CarpetaPedagogicaDao) FactoryDao.buildDao("maestro.carpeta.CarpetaPedagogicaDao");
            List<SeccionCarpetaPedagogica> secciones = carpetaDao.listarSeccionesCarpeta(idCar);
            JSONArray jsonSecciones = new JSONArray();
            for(SeccionCarpetaPedagogica seccion : secciones){
                JSONObject jsonSeccion = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"secCarPedId","nom"},
                        new String[]{"id","nom"},
                        seccion
                ));
                List<ContenidoSeccionCarpeta> contenidos = seccion.getContenidos();
                JSONArray jsonContenidos = new JSONArray(EntityUtil.listToJSONString(
                        new String[]{"conSecCarPedId","nom","tipUsu"},
                        new String[]{"id","nom","tip"},
                        contenidos
                ));
                jsonSeccion.put("conts",jsonContenidos);

                jsonSecciones.put(jsonSeccion);
            }
            return WebResponse.crearWebResponseExito("Se listo correctamente las secciones",jsonSecciones);
        }catch (Exception e){
            logger.log(Level.SEVERE,"listarSecciones",e);
            return WebResponse.crearWebResponseError("No se pudo listar las secciones de la carpeta");
        }
    }
}
