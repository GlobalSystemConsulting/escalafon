/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.vacacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.VacacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Vacacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

public class AgregarVacacionTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(AgregarVacacionTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        Vacacion vacacion = null;
        
        Integer ficEscId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");

        try {
            JSONObject requestData = (JSONObject) wr.getData();
            System.out.println("newVac: "+requestData.toString());
            ficEscId = requestData.getInt("ficEscId");
            String entEmi = requestData.getString("entEmi");
            int tipDocId = requestData.getInt("tipDocId");
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            Boolean estGoc = requestData.getBoolean("estGoc");
            Date fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = sdi.parse(requestData.getString("fecTer").substring(0, 10));
            String mot = requestData.getString("mot");
            int estProId = requestData.getInt("estProId");
            
            String numInf = requestData.getString("numInf");
            Date fecInf = sdi.parse(requestData.getString("fecInf").substring(0, 10));
            Date fecRecInf = sdi.parse(requestData.getString("fecRecInf").substring(0, 10));
            int orgiId=requestData.getInt("orgiId");
            int carId=requestData.getInt("carId");
            int catId=requestData.getInt("catId");
            
            vacacion = new Vacacion(entEmi, numDoc, fecDoc, estGoc, fecIni, fecTer, mot, wr.getIdUsuario(), new Date(), 'A', 
                    new FichaEscalafonaria(ficEscId), numInf, fecInf, fecRecInf, tipDocId, estProId);
            
            vacacion.setOrgiActId(orgiId);
            vacacion.setCarActId(carId);
            vacacion.setCatActId(catId);
            
        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE,"Datos nueva vacacion",e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }

        VacacionDao vacacionDao = (VacacionDao) FactoryDao.buildDao("se.VacacionDao");
        try {
            vacacionDao.insert(vacacion);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Agregar nueva vacación",e);
            System.out.println(e);
        }
        
        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("vacId", vacacion.getVacId());
        
        if(vacacion.getEntEmiRes()!=null)
            oResponse.put("entEmi", vacacion.getEntEmiRes());
        else
            oResponse.put("entEmi", "");
        
        if(vacacion.getTipDocId()!=null)
            oResponse.put("tipDocId", vacacion.getTipDocId());
        else
            oResponse.put("tipDocId", 0);
        
        if(vacacion.getNumDoc()!=null)
            oResponse.put("numDoc", vacacion.getNumDoc());
        else
            oResponse.put("numDoc", "");
       
        if(vacacion.getFecDoc()!=null)
            oResponse.put("fecDoc", sdo.format(vacacion.getFecDoc()));
        else
            oResponse.put("fecDoc", "");
        
        if(vacacion.getEstGoc()!=null)
            oResponse.put("estGoc", vacacion.getEstGoc());
        else
            oResponse.put("estGoc", false);
        
        if(vacacion.getFecIni()!=null)
            oResponse.put("fecIni", sdo.format(vacacion.getFecIni()));
        else
            oResponse.put("fecIni", "");
        
        if(vacacion.getFecTer()!=null)
            oResponse.put("fecTer", sdo.format(vacacion.getFecTer()));
        else
            oResponse.put("fecTer", "");
        
        if(vacacion.getMotLic()!=null)
            oResponse.put("mot", vacacion.getMotLic());
        else
            oResponse.put("mot", "");
        
        if(vacacion.getNumInf()!=null)
        oResponse.put("numInf", vacacion.getNumInf());
        else
            oResponse.put("numInf", "");
        
        if(vacacion.getFecInf()!=null)
        oResponse.put("fecInf", sdo.format(vacacion.getFecInf()));
        else
            oResponse.put("fecInf", "");
        
        if(vacacion.getFecRecInf()!=null)
        oResponse.put("fecRecInf", sdo.format(vacacion.getFecRecInf()));
        else
            oResponse.put("fecRecInf", "");
        
        if(vacacion.getEstProId()!=null)
        oResponse.put("estProId", vacacion.getEstProId());
        else
            oResponse.put("estProId", 0);
        
        return WebResponse.crearWebResponseExito("El registro de la vacación se realizo correctamente", oResponse);
    }
    
}
