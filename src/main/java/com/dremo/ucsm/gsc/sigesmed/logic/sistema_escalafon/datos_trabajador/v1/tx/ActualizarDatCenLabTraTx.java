/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_trabajador.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.TrabajadorDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Cargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Categoria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Dependencia;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Planilla;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Zona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ActualizarDatCenLabTraTx implements ITransaction{
    
    private static final Logger logger = Logger.getLogger(ActualizarDatCenLabTraTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject response = (JSONObject)wr.getData();
        return actualizarDatCenLabTra(response);
    }
    public WebResponse actualizarDatCenLabTra(JSONObject data) {
        Trabajador trabajador = new Trabajador();
        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
        try{
            
            TrabajadorDao trabajadorDao = (TrabajadorDao)FactoryDao.buildDao("se.TrabajadorDao");
            trabajador = trabajadorDao.buscarPorId(data.getInt("traId"));

            trabajador.setEstLab(data.getString("estLab").charAt(0));
            
            /*
            if(data.getInt("plaId") != -1)
                trabajador.setPlanilla(new Planilla(data.getInt("plaId")));
            */
            if(data.getInt("catId") != -1)
                trabajador.setCategoria(new Categoria(data.getInt("catId")));
            
            if(data.getInt("aniosOut") != -1){
                trabajador.setAniosOut(data.getInt("aniosOut"));
                System.out.println("setting anios: "+data.getInt("aniosOut"));
            }
                
            if(data.getInt("depId") != -1)
                trabajador.setDependencia(new Dependencia(data.getInt("depId")));
            
            if(data.getInt("zonId") != -1)
                trabajador.setZona(new Zona(data.getInt("zonId")));
            
            if(data.getInt("carId") != -1)
                trabajador.setCargo(new Cargo(data.getInt("carId")));
            
            if(data.getDouble("sal") != 0)
                trabajador.setSal(data.getDouble("sal"));
            
            if(data.getBoolean("fecIngEst"))
                trabajador.setFecIng(sdi.parse(data.getString("fecIng").substring(0, 10)));
            
            trabajadorDao.update(trabajador);            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarDatCenLabTra",e);
            return WebResponse.crearWebResponseError("Error, el trabajador no fue actualizado");
        }
        
        JSONObject oResponse = new JSONObject();
        oResponse.put("traId", trabajador.getTraId());
        oResponse.put("orgId", data.getInt("orgId") != 0 ? data.getInt("orgId") : -1);
        oResponse.put("orgNom", data.getString("orgNom") != null ? data.getString("orgNom") : "");
        oResponse.put("rolId", data.getInt("rolId") != 0 ? data.getInt("rolId") : -1);
        oResponse.put("rolNom", data.getString("rolNom") != null ?  data.getString("rolNom") : "");
        oResponse.put("plaId", data.getInt("plaId") != 0 ? data.getInt("plaId") : "");
        oResponse.put("plaNom", data.getString("plaNom") != null ? data.getString("plaNom") : "");
        oResponse.put("catId", data.getInt("catId") != 0 ? data.getInt("catId") : -1);
        oResponse.put("catNom", data.getString("catNom") != null ? data.getString("catNom") : "");
        oResponse.put("zonId", data.getInt("zonId") != 0 ? data.getInt("zonId") : -1);
        oResponse.put("zonNom", data.getString("zonNom") != null ? data.getString("zonNom") :"");
        oResponse.put("carId", data.getInt("carId") != 0 ? data.getInt("carId") : -1);
        oResponse.put("carNom", data.getString("carNom") != null ? data.getString("carNom") : "");
        oResponse.put("depId", data.getInt("depId") != 0 ? data.getInt("depId") : -1);
        oResponse.put("depNom", data.getString("depNom") != "" ? data.getString("depNom") : "");
        oResponse.put("facId", data.getInt("facId") != 0 ? data.getInt("facId") : "");
        oResponse.put("facNom", data.getString("facNom") != null ? data.getString("facNom") : "");
        oResponse.put("orgaId", data.getInt("orgaId") != 0 ? data.getInt("orgaId") : -1 );
        oResponse.put("orgaNom", data.getString("orgaNom") != null ? data.getString("orgaNom"):"");
        oResponse.put("estLab", data.getString("estLab"));
        oResponse.put("fecIng", trabajador.getFecIng() != null && trabajador.getFecIng().toString().length() > 0 ? sdo.format(trabajador.getFecIng()) : "");
        oResponse.put("sal", trabajador.getSal() != null ? trabajador.getSal():"");
        oResponse.put("tieServ", trabajador.getTieServ() != null ? trabajador.getTieServ():"");
        oResponse.put("fecSal", trabajador.getFecSal() != null ? trabajador.getTieServ() : "");
        oResponse.put("aniosOut", trabajador.getAniosOut() != null ? trabajador.getAniosOut() : "");
        
        return WebResponse.crearWebResponseExito("Trabajador actualizado exitosamente",oResponse);
    }
}
