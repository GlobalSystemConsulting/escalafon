/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.NacionalidadDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Nacionalidad;

/**
 *
 * @author 
 */
public class NacionalidadDaoHibernate extends GenericDaoHibernate<Nacionalidad> implements NacionalidadDao{
    
}
