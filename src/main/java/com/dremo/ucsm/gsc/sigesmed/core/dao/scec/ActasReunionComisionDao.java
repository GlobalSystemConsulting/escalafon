package com.dremo.ucsm.gsc.sigesmed.core.dao.scec;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scec.ActasReunionComision;

import java.util.List;

/**
 * Created by geank on 28/09/16.
 */
public interface ActasReunionComisionDao extends GenericDao<ActasReunionComision>{
    List<ActasReunionComision> buscarPorReunion(int idReu);
    List<ActasReunionComision> buscarPorReunionConAcuerdos(int idReu);
    void  registrarAcuerdos(ActasReunionComision acta);
    ActasReunionComision buscarPorId(int idAct);
    ActasReunionComision buscarConAcuerdos(int id);
}
