/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.datos_academicos.formacion_educativa.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.FormacionEducativaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FichaEscalafonaria;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.auditoria.v1.tx.AgregarOperacionAuditoria;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class AgregarFormacionEducativaTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(AgregarFormacionEducativaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
         */
        FormacionEducativa forEdu = null;

        Integer perId = 0;

        DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat sdo = new SimpleDateFormat("dd/MM/yyyy");

        try {
            JSONObject requestData = (JSONObject) wr.getData();

            perId = requestData.getInt("perId");
            Integer tipForId = requestData.getInt("tipForId");
            Integer nivAcaId = requestData.getInt("nivAcaId");
            Integer tipDocId = requestData.getInt("tipDocId");
            String numDoc = requestData.getString("numDoc");
            Date fecDoc = requestData.getString("fecDoc").equals("") ? null : sdi.parse(requestData.getString("fecDoc").substring(0, 10));
            String esp = requestData.getString("esp");
            Boolean estCon = requestData.getBoolean("estCon");
            Date fecIni = requestData.getString("fecIni").equals("") ? null : sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = estCon ? (requestData.getString("fecTer").equals("") ? null : sdi.parse(requestData.getString("fecTer").substring(0, 10))) : null;
            String cenEst = requestData.getString("cenEst");
            Integer paisId = requestData.getInt("paisId");

            forEdu = new FormacionEducativa(new Persona(perId), tipForId, nivAcaId, tipDocId, numDoc, fecDoc, esp, estCon, fecIni, fecTer, cenEst, paisId, wr.getIdUsuario(), new Date(), 'A');

        } catch (Exception e) {
            System.out.println(e);
            logger.log(Level.SEVERE, "Datos nueva formacion educativa", e);
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage());
        }
        //Fin

        /*
        *  Parte para la operacion en la Base de Datos
         */
        //si el pariente no existe en la tabla persona
        FormacionEducativaDao forEduDao = (FormacionEducativaDao) FactoryDao.buildDao("se.FormacionEducativaDao");
        try {
            forEduDao.insert(forEdu);
            ///////////AUDITORIA/////////////
            AgregarOperacionAuditoria.agregarAltaAuditoria("Se agrego formacion educativa", "Detalle: " + perId + " ficha", wr.getIdUsuario(), "---", "xxx.xxx.xxx.xxx");
            ///////////AUDITORIA///////////// 
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Agregar nueva formacion educativa", e);
            System.out.println(e);
        }

        //Fin       

        /*
        *  Repuesta Correcta
         */
        JSONObject oResponse = new JSONObject();
        oResponse.put("forEduId", forEdu.getForEduId());
        oResponse.put("tipForId", forEdu.getTipForId() == null ? 0 : forEdu.getTipForId());
        oResponse.put("tipFor", "");
        oResponse.put("nivAcaId", forEdu.getNivAcaId() == null ? 0 : forEdu.getNivAcaId());
        oResponse.put("nivAca", "");
        oResponse.put("tipDocId", forEdu.getTipDocId() == null ? 0 : forEdu.getTipDocId());
        oResponse.put("tipDoc", "");
        oResponse.put("numDoc", forEdu.getNumDoc() == null ? "" : forEdu.getNumDoc());
        oResponse.put("fecDoc", forEdu.getFecDoc() == null ? "" : sdi.format(forEdu.getFecDoc()));
        oResponse.put("esp", forEdu.getEspAca() == null ? "" : forEdu.getEspAca());
        oResponse.put("estCon", forEdu.getEstCon() == null ? "" : forEdu.getEstCon());
        oResponse.put("estConDes", "");
        oResponse.put("fecIni", forEdu.getFecIniFor() == null ? "" : sdi.format(forEdu.getFecIniFor()));
        oResponse.put("fecTer", forEdu.getEstCon() ?(forEdu.getFecTerFor()==null?"":sdi.format(forEdu.getFecTerFor())):"");
        oResponse.put("cenEst", forEdu.getCenEst() == null ? "" : forEdu.getCenEst());
        oResponse.put("paisId", forEdu.getNacId() == null ? 0 : forEdu.getNacId());
        oResponse.put("pais", "");

        return WebResponse.crearWebResponseExito("El registro de la formacion educativa se realizo correctamente", oResponse);
        //Fin

    }

}
