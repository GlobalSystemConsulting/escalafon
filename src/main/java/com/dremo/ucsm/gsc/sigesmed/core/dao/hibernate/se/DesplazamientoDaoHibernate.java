/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.FormacionEducativa;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author gscadmin
 */
public class DesplazamientoDaoHibernate extends GenericDaoHibernate<Desplazamiento> implements DesplazamientoDao{

    @Override
    public List<Desplazamiento> listarxFichaEscalafonaria(int ficEscId) {
        List<Desplazamiento> desplazamientos = null;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            
            String hql = "SELECT des from Desplazamiento as des "
                    + "join fetch des.fichaEscalafonaria as fe "
                    + "WHERE fe.ficEscId=" + ficEscId + " AND des.estReg='A' "
                    + "order by des.desId asc";
            Query query = session.createQuery(hql);
            desplazamientos = query.list();
            t.commit();
                    
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los desplazamientos\\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo listar los desplazamientos \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return desplazamientos;
    }

    @Override
    public Desplazamiento buscarPorId(Integer desId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Desplazamiento des = (Desplazamiento)session.get(Desplazamiento.class, desId);
        session.close();
        return des;
    }

    @Override
    public List<Desplazamiento> listarxOrganizacion(int orgId) {
        List<Desplazamiento> desplazamientos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try{
            String hql = "SELECT des FROM Desplazamiento as des "
                    + "join fetch des.fichaEscalafonaria fe "
                    + "join fetch fe.trabajador t "
                    + "join fetch t.organizacion o "
                    + "WHERE o.orgId =" +orgId; 
            Query query = session.createQuery(hql);            
            desplazamientos = query.list();
            t.commit();
        
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo listar los desplazamientos por organización \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo los desplazamientos por organización \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        return desplazamientos;
    }
    
}
