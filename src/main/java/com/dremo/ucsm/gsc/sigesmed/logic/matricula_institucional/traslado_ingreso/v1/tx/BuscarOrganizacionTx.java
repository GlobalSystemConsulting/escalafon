package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.traslado_ingreso.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.GenericMMIDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.OrganizacionMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONException;
import org.json.JSONObject;

public class BuscarOrganizacionTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        GenericMMIDaoHibernate<OrganizacionMMI> hb = new GenericMMIDaoHibernate<>();
        JSONObject organizacion = new JSONObject();

        JSONObject requestData = (JSONObject) wr.getData();
        OrganizacionMMI org;
        int estDNI;

        try {
            estDNI = Integer.parseInt(requestData.getString("traIgnOrgDes"));
            org = hb.loadInt(OrganizacionMMI.class, estDNI);

            if (org != null) {
                if (org.getNom() != null) {
                    organizacion.put("orgNom", org.getNom());
                } else {
                    organizacion.put("orgNom", "Desconocido");
                }
                if (org.getNom() != null) {
                    organizacion.put("orgId", org.getOrgId());
                } else {
                    organizacion.put("orgId", "Desconocido");
                }
            }

        } catch (JSONException | NumberFormatException e) {
            return WebResponse.crearWebResponseError("Error al cargar los paises! ", organizacion);
        }

        return WebResponse.crearWebResponseExito("Listaron los paises", organizacion);
    }

}
