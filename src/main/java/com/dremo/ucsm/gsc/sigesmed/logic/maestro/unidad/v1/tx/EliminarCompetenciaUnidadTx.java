package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 13/12/2016.
 */
public class EliminarCompetenciaUnidadTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EliminarCompetenciaUnidadTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        String tip = data.getString("tip");
        int idUnidad = data.getInt("unid");
        int idComp = data.getInt("comp");
        int idCap = data.getInt("cap");
        return eliminarCompetenciaUnidadTx(tip,idUnidad,idComp,idCap);
    }

    private WebResponse eliminarCompetenciaUnidadTx(String tip, int idUnidad,int idComp, int idCap) {
        if(tip.equals("comp")){
            return eliminarCompetencia(idUnidad, idComp);
        }else if(tip.equals("cap")){
            return eliminarCapacidad(idUnidad, idComp, idCap);
        }else return null;
    }

    private WebResponse eliminarCapacidad(int idUnidad, int idComp, int idCap) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            unidadDao.eliminarCompetenciasUnidad(idUnidad,idComp,idCap);
            return WebResponse.crearWebResponseExito("Se elimino correctamente");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarCapacidad",e);
            return WebResponse.crearWebResponseError("Error al eliminar la capacidad");
        }
    }

    private WebResponse eliminarCompetencia(int idUnidad, int idComp) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            unidadDao.eliminarCompetenciasUnidad(idUnidad,idComp);
            return WebResponse.crearWebResponseExito("Se elimino correctamente");
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarCompetencia",e);
            return WebResponse.crearWebResponseError("Error al eliminar la competencia");
        }
    }
}
