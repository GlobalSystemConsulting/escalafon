package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.CronogramaSedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.CronogramaSedeCapacitacion;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

public class CronogramaSedeCapacitacionDaoHibernate extends GenericDaoHibernate<CronogramaSedeCapacitacion> implements CronogramaSedeCapacitacionDao {

    private static final Logger logger = Logger.getLogger(CronogramaSedeCapacitacionDaoHibernate.class.getName());
    
    @Override
    public CronogramaSedeCapacitacion buscarCronogramaPorSede(int sedId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            return (CronogramaSedeCapacitacion) session.get(CronogramaSedeCapacitacion.class, sedId);
        } catch (Exception e){
            logger.log(Level.SEVERE,"listarCapacitacionPorOrganizacionYAno",e);
            throw e;
        } finally {
            session.close();
        } 
    }    
}
