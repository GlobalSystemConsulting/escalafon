/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.horarios.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.HorarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.cpe.LibroAsistenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TrabajadorCargo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.cpe.HorarioCab;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author carlos
 */
public class AsignarHorarioPersonalizadoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
       
        
        HorarioCab idH;
        List<Trabajador> trab=new ArrayList<>();
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            Integer idHorario=requestData.getInt("idHorario");
             
            JSONArray trabajadores=requestData.getJSONArray("trabajadores");
        
            for(int i=0;i<trabajadores.length();i++)
            {
                JSONObject obj=(JSONObject)trabajadores.get(i);
                Trabajador nTrab=new Trabajador(obj.getInt("id"));
                trab.add(nTrab);
            }

            idH=new HorarioCab(idHorario);
       
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo verificar los datos", e.getMessage() );
        }

        
        try{
           
        HorarioDao horarioDao = (HorarioDao)FactoryDao.buildDao("cpe.HorarioDao");    
        for(Trabajador _trab:trab)
        {
            horarioDao.asignarHorarioPersonalizado(_trab, idH);
        }
   
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo Asignar el Horario", e.getMessage() );
        }

        return WebResponse.crearWebResponseExito("Se Asigno correctamente");        
        //Fin
    }
    
}

