package com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.AreaCurricular;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ObjetivoProgramacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProgramacionAnual;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ValoresActitudes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Grado;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.Periodo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.PlanEstudios;

import java.util.List;

/**
 * Created by Administrador on 21/10/2016.
 */
public interface ProgramacionAnualDao extends GenericDao<ProgramacionAnual> {

    ProgramacionAnual buscarProgramacionConObjetivos(int idProg);
    ProgramacionAnual buscarProgramacionConValores(int idProg);
    ProgramacionAnual buscarProgramacionConDatos(int idProg);
    List<ProgramacionAnual> buscarProgramacionDocente(int idDocente);
    List<ProgramacionAnual> buscarProgramacionParaUgel(int idUgel);
    List<Grado> buscarGradosDisenoCurricular(int dis);
    List<AreaCurricular> buscarAreaCurricularCurricula(int dis);
    Grado buscarGrado(int idGrado);
    AreaCurricular buscarArea(int idArea);
    List<ValoresActitudes> buscarValoresDeProgramacion(int idProg);
    List<ObjetivoProgramacion> buscarObjetivosDeProgramacion(int idProg, String tip);
    List<ObjetivoProgramacion> buscarObjetivosDeProgramacion(int idProg);
    List<ObjetivoProgramacion> buscarObjetivosDeProgramacion(String tip);
    ValoresActitudes buscarValorPorId(int idVal);
    PlanEstudios buscarPlanEstudios(int idPlan);
    ProgramacionAnual buscarProgramacionAnualPorPlanEstudios(int idPLan,int idOrg,int idDoc,int idArea,int idGrado);
    Periodo listarPeriodoPlanEstudio(int idNivel,int idPlan);
}
