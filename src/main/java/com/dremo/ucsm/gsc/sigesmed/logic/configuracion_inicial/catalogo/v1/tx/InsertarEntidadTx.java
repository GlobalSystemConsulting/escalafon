/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.catalogo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.CatalogoTablaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.EntidadObjetoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.CatalogoTabla;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import org.json.JSONObject;

/**
 *  public int getId() {
        return this.tipDocId;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public String getDes() {
        return this.des;
    }
     public char getEstReg() {
        return this.estReg;
    }
    public void setNom(String nom) {
        this.nom = nom;
    } 
    
    public void setDes(String des) {
        this.des = des;
    }
    
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    * getEstReg
 *
 * @author felipe
 */
public class InsertarEntidadTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        //CatalogoTablaDao catalogoTablaDao = (CatalogoTablaDao)FactoryDao.buildDao("CatalogoTablaDao");
        EntidadObjetoDao entidadObjetoDao = (EntidadObjetoDao)FactoryDao.buildDao("EntidadObjetoDao");
        //CatalogoTabla catalogoTabla = null;
        Object obj = null ;
        //String nombretabla;
        String nombreclase = null;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            //nombretabla = requestData.getString("nombretabla");
            nombreclase = requestData.getString("nombreclase");
            String nombre= requestData.getString("Nombre");
            String descripcion = requestData.getString("Descripcion");
            String estado = requestData.getString("estado");
            
             try{
                obj = Class.forName("com.dremo.ucsm.gsc.sigesmed.core.entity."+nombreclase).newInstance();
            
            //catalogoTabla = new CatalogoTabla(0, nombretabla, nombreclase, new Date(),wr.getIdUsuario(), estado.charAt(0));
           
                obj.getClass().getMethod("setId",int.class).invoke(obj,0);
                obj.getClass().getMethod("setNom",String.class).invoke(obj,nombre);
                obj.getClass().getMethod("setDes",String.class).invoke(obj,descripcion);
                obj.getClass().getMethod("setFecMod",Date.class).invoke(obj,new Date());
                obj.getClass().getMethod("setUsuMod",Integer.class).invoke(obj,wr.getIdUsuario());
                obj.getClass().getMethod("setEstReg",char.class).invoke(obj,estado.charAt(0));
                
                System.out.println(obj);
                System.out.println(getClass().getMethod("toString").invoke(obj));
            }catch(Exception e){
                System.out.println("No se pudo registrar Set \n"+e);
             return WebResponse.crearWebResponseError("No se pudo registrar, Error de SETS", e.getMessage() );
            }
 // System.out.println("probando-----<<<"+obj.getClass().getMethod("getNombre").invoke(obj) + " " +obj.getClass().getMethod("getCodigo").invoke(obj) + " " + obj.getClass().getMethod("getDescripcion").invoke(obj));

        
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        try{
            entidadObjetoDao.insert(obj);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar el Catalogo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Catalogo", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        try{
        oResponse.put("registroID",  obj.getClass().getMethod("getId").invoke(obj));
        oResponse.put("fecha",obj.getClass().getMethod("getFecMod").invoke(obj));
         }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo registrar en la Entidad, datos incorrectos", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El registro  se realizo correctamente", oResponse);
        //Fin
    }
    
}

