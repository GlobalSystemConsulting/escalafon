/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.sci;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Asiento;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ClienteProveedor;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaContable;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaCorriente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentasEfectivo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Empresa;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.HechosLibro;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroCompras;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.RegistroVentas;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ResultadoMensual;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ResultadosMensualPorCuenta;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Tesorero;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.TipoPago;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Trabajador;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author e
 */
public class LibroCajaDaoHibernate extends GenericDaoHibernate<LibroCaja> implements LibroCajaDao {

    @Override
    public LibroCaja estadoLibroCaja(int organizacionID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        LibroCaja lib = null;
      
        try {
            //listar Libro
            String hql = "SELECT Max(lc) FROM LibroCaja lc WHERE lc.estReg!='E' AND lc.organizacion.orgId=:p1";             
            Query query = session.createQuery(hql);

            query.setParameter("p1", organizacionID);
           
            query.setMaxResults(1);
            lib = (LibroCaja) query.uniqueResult(); 
                  
        } catch (Exception e) {
            
                System.out.println("No se pudo verificar estado del libro \n " + e.getMessage());
                throw new UnsupportedOperationException("No se pudo verificar estado del libro \n " + e.getMessage());
            
        } finally {
            session.close();
        }
        return lib;
    }
    
     @Override
    public LibroCaja estadoLibroAnterior(int organizacionID) {
         Session session = HibernateUtil.getSessionFactory().openSession();
        LibroCaja lib = null;
        try {
            //listar Libro
            String hql = "SELECT Max(lc) FROM LibroCaja lc  WHERE lc.estReg='C' AND lc.organizacion.orgId=:p1";            
            Query query = session.createQuery(hql);

            query.setParameter("p1", organizacionID);
          //  query.setDate("p2", d);
            query.setMaxResults(1);
            lib = (LibroCaja) query.uniqueResult();  
            if(lib!=null){
                hql  = "SELECT DISTINCT lc FROM LibroCaja lc join fetch lc.cuentasEfectivos ce join fetch ce.cuenta join fetch lc.cuentaCorrientes cc join fetch cc.cuenta  join fetch cc.banco WHERE lc.estReg='C' AND lc.libCajId=:p1"; 
                query = session.createQuery(hql);

                query.setParameter("p1", lib.getLibCajId());
                query.setMaxResults(1);
                lib = (LibroCaja) query.uniqueResult();  
            
            }
           

        } catch (Exception e) {
            
                System.out.println("No se pudo verificar estado del libro \n " + e.getMessage());
                throw new UnsupportedOperationException("No se pudo verificar estado del libro \n " + e.getMessage());
            
        } finally {
            session.close();
        }
        return lib;
    }


    @Override
    public List<CuentasEfectivo> buscarCuentasEfectivo(LibroCaja libro){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<CuentasEfectivo> datos= null;
        try{
            //Lista de cuentas efectivo
            String hql="SELECT cE FROM CuentasEfectivo cE JOIN FETCH cE.cuenta WHERE cE.estReg='A' AND cE.libroCaja=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", libro);
            datos = query.list();
            
        }catch(Exception e){                    
           System.out.println("No se encontro las cuentas de efectivo \n " + e.getMessage());
                throw new UnsupportedOperationException("No se encontro las cuentas de efectivo \n " + e.getMessage());
            
        } finally {
            session.close();
        }
        return datos;
        
    }
    
    @Override
    public List<CuentaCorriente> buscarCuentasCorrientes(LibroCaja libro){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<CuentaCorriente> datos= null;
        try{
            //Lista de cuentas corrientes
            String hql="SELECT cC FROM CuentaCorriente cC JOIN FETCH cC.cuenta JOIN FETCH cC.banco WHERE cC.estReg='A' AND cC.libroCaja=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", libro);
            datos = query.list();
            
        }catch(Exception e){                    
           System.out.println("No se encontro las cuentas corrientes \n " + e.getMessage());
                throw new UnsupportedOperationException("No se encontro las cuentas corrientes \n " + e.getMessage());
            
        } finally {
            session.close();
        }
        return datos;
        
    }
    @Override
     public List<HechosLibro> buscarHechosLibroCaja( LibroCaja libro){
         
        Session session = HibernateUtil.getSessionFactory().openSession();   
        List<HechosLibro> hechos=null; 
        try {
                    
            //Date mesA= new Date();  mesA.setMonth(d.getMonth()-1); mesA.setDate(1);
            //Date mes= new Date();   mes.setMonth(d.getMonth());
           
             //listar 
            String hql = "SELECT hl FROM HechosLibro hl join fetch hl.cuenta c join fetch hl.libroCaja lc WHERE hl.estReg='A'  AND lc.libCajId=:p1 " ;
            Query query = session.createQuery(hql);
            query.setParameter("p1", libro.getLibCajId());
          //  query.setDate("p2", mesA); 
          //  query.setDate("p3", mes);
            hechos =query.list();         

        } catch (Exception e) {
            System.out.println("No se pudo encontrar hechos : " + "\n" + e.getMessage());
             throw new UnsupportedOperationException("No se pudo encontrar hechos \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return hechos;
         
     }
     
     @Override
     public List<ResultadoMensual> resultadosHechosLibroCajaPorFechas(LibroCaja libro,Date desde,Date hasta){
         
         //esta funcion obtiene los hechos mensuales del libro como resultado  
       Session session = HibernateUtil.getSessionFactory().openSession();   
       List<ResultadoMensual> resultado= new ArrayList<>(); 
        try {                    
            //se considera que los resultados se generan despues del 1er mes para obtener el saldo mes anterior                                           
            desde.setMonth(desde.getMonth()-1);                          
            int i= hasta.getMonth()- desde.getMonth();
                          
            ResultadoMensual r=null;
             //listar 
            
            for(int j=0;j<i;j++){
               
                desde.setMonth(desde.getMonth()); desde.setDate(1);                 
                hasta.setMonth(desde.getMonth()); hasta.setDate(31);
                 
                if(hasta.getMonth()!=desde.getMonth()){
                    hasta.setMonth(desde.getMonth()); hasta.setDate(30);
                }                
              
                String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ResultadoMensual( SUM(hl.impDeb),SUM(hl.impHab)) FROM HechosLibro hl " +
                        " WHERE hl.libroCaja=:p2 AND hl.estReg='A' AND hl.fecMes>=:p3 AND hl.fecMes<=:p4 ";
                  
                    Query query = session.createQuery(hql);
                                        
                    query.setParameter("p2",libro);
                    query.setDate("p3",desde); 
                    query.setDate("p4",hasta);
                  
                   r = (ResultadoMensual)query.uniqueResult();
                   r.setNombreMes(desde.getMonth());
                   resultado.add(r);
                   
                  desde.setMonth(desde.getMonth()+1);
                                                                                                          
            }
           
            
             //listar 
        
            
        } catch (Exception e) {
            System.out.println("No se pudo encontrar hechos : " + "\n" + e.getMessage());
             throw new UnsupportedOperationException("No se pudo encontrar hechos \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return resultado;
         
     }
     
     @Override
      public ResultadoMensual resultadoAsientosPorFecha(LibroCaja libro,Date desde, Date hasta){
          
        Session session = HibernateUtil.getSessionFactory().openSession();   
        ResultadoMensual resultado=null;
        
        BigDecimal debe= new BigDecimal(BigInteger.ZERO); 
        BigDecimal haber =new BigDecimal(BigInteger.ZERO);
        
        try {                                                                                        
              
               
                    String hql = "SELECT ( SUM(dC.impDetCue)) FROM  DetalleCuenta dC" +
                        " WHERE dC.asiento.libroCaja=:p2 AND dC.estReg='A' AND dC.asiento.fecAsi>=:p3 AND dC.asiento.fecAsi<=:p4 AND dC.natDetCue=:p1 AND dC.cuentaContable.efeReg=false";
                  
                    Query query = session.createQuery(hql);
                    query.setBoolean("p1",true);         
                    query.setParameter("p2",libro);
                    query.setDate("p3",desde); 
                    query.setDate("p4",hasta);
                                      
                  if(query.uniqueResult()!=null){                   
                      haber= new BigDecimal(query.uniqueResult().toString());
                  }
                 
                     hql = "SELECT ( SUM(dC.impDetCue)) FROM  DetalleCuenta dC" +
                        " WHERE dC.asiento.libroCaja=:p2 AND dC.estReg='A' AND dC.asiento.fecAsi>=:p3 AND dC.asiento.fecAsi<=:p4 AND dC.natDetCue=:p1 AND dC.cuentaContable.efeReg=false";
                  
                    query = session.createQuery(hql);
                    query.setBoolean("p1",false);         
                    query.setParameter("p2",libro);
                    query.setDate("p3",desde); 
                    query.setDate("p4",hasta);
                                                         
                  if(query.uniqueResult()!=null){                      
                      debe= new BigDecimal(query.uniqueResult().toString());
                  }
                                                                                        
                   resultado= new ResultadoMensual(hasta.getMonth(),debe,haber);                                                   
   
             //listar 
        
            
        } catch (Exception e) {
            System.out.println("No se pudo encontrar hechos : " + "\n" + e.getMessage());
             throw new UnsupportedOperationException("No se pudo encontrar hechos \\n " + e.getMessage());
        } finally {
            session.close();
        }
    return resultado;
    }
    
    @Override
    public List<ResultadosMensualPorCuenta> resultadosCuentasPorFecha(LibroCaja libro,Date desde, Date hasta,boolean flagIva,boolean flagNat){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List <ResultadosMensualPorCuenta> listaDatos = null;
       
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ResultadosMensualPorCuenta (c.cuentaContable.cueConId, c.cuentaContable.nomCue,MONTH(c.asiento.fecAsi),SUM(c.impDetCue)) FROM DetalleCuenta c   WHERE c.asiento.libroCaja=:p1 AND c.cuentaContable.efeReg='false' AND c.cuentaContable.usaInfIva=:p2 AND c.asiento.fecAsi>=:p3 AND c.asiento.fecAsi<=:p4 AND c.natDetCue=:p5 GROUP BY  c.cuentaContable.cueConId ,c.cuentaContable.nomCue,MONTH(c.asiento.fecAsi) ORDER BY c.cuentaContable.cueConId ,MONTH(c.asiento.fecAsi) ASC";
           
            Query query = session.createQuery(hql); 
            query.setParameter("p1",libro);
            query.setBoolean("p2",flagIva);
            query.setDate("p3",desde);
            query.setDate("p4",hasta);
            query.setBoolean("p5",flagNat);
           

            listaDatos=query.list();
           
        
        }catch(Exception e){
          
            System.out.println("No se pudo encontrar historial de cuentas" + "\n"+e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return listaDatos;
    }
    @Override
    public List<ResultadoMensual> resultadosMensuales(LibroCaja libro,Date desde, Date hasta,boolean flagIva,boolean flagNat){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List <ResultadoMensual> listaDatos = null;
       
        try{
            String hql = "SELECT NEW com.dremo.ucsm.gsc.sigesmed.core.entity.sci.ResultadoMensual (MONTH(c.asiento.fecAsi),SUM(c.impDetCue)) FROM DetalleCuenta c   WHERE c.asiento.libroCaja=:p1 AND c.cuentaContable.efeReg='false' AND c.cuentaContable.usaInfIva=:p2 AND c.asiento.fecAsi>=:p3 AND c.asiento.fecAsi<=:p4 AND c.natDetCue=:p5 GROUP BY MONTH(c.asiento.fecAsi) ORDER BY MONTH(c.asiento.fecAsi) ASC";
           
            Query query = session.createQuery(hql); 
            query.setParameter("p1",libro);
            query.setBoolean("p2",flagIva);
            query.setDate("p3",desde);
            query.setDate("p4",hasta);
            query.setBoolean("p5",flagNat);
           

            listaDatos=query.list();
           
        
        }catch(Exception e){
          
            System.out.println("No se pudo encontrar historial de cuentas" + "\n"+e.getMessage());
            throw e;
        }
        finally{
            session.close();
        }
        return listaDatos;
    }

    @Override
    public List<Tesorero> buscarTesoreros(LibroCaja libro){
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Tesorero> datos= null; 
        try{
            //Lista de cuentas corrientes
            String hql="SELECT t FROM Tesorero t JOIN FETCH t.persona WHERE t.estReg!='E' AND t.libroCaja=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1", libro);
            datos = query.list();
            
        }catch(Exception e){                    
           System.out.println("No se encontro los tesoreros del libro \n " + e.getMessage());
                throw new UnsupportedOperationException("No se encontro los tesoreros del libro \n " + e.getMessage());
            
        } finally {
            session.close();
        }
        return datos;
        
    }
       
    @Override
    public void saveUpdate(LibroCaja dato) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            session.saveOrUpdate(dato);
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo registrar : " + dato.getClass().getName() + "\n" + e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<Trabajador> listarTrabajadoresPorOrganizacion(int organizacionID) {
        List<Trabajador> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            //listar trbajdor
            String hql = "SELECT t FROM sci.Trabajador  t join fetch t.persona p join fetch t.organizacion o WHERE t.traTip in ('"+Sigesmed.TIPO_TRABAJADOR_ADMINISTRATIVO+"','"+Sigesmed.TIPO_TRABAJADOR_DIRECTIVO+"') AND t.estReg!='E' AND o.orgId=" + organizacionID;
            Query query = session.createQuery(hql);
            listDatos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar los Trabajadores\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Trabajadores \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
    }

    @Override
    public void insertarTesorero(Tesorero dato) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();

        try {
            LibroCaja libro = (LibroCaja) session.load(LibroCaja.class, dato.getLibroCaja().getLibCajId());
            dato.setLibroCaja(libro);

            session.saveOrUpdate(dato);
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo registrar : " + dato.getClass().getName() + "\n" + e.getMessage());
             throw new UnsupportedOperationException("No se pudo registrar \\n " + e.getMessage());
        } finally {
            session.close();
        }
    }
    @Override
    public boolean verificarTesorero(LibroCaja libro, Persona persona) {
        Session session = HibernateUtil.getSessionFactory().openSession();
     
        List<Tesorero> tes=null; 
        boolean flag=false;
        try {
            
            Date f=new Date();
             //listar 
            String hql = "SELECT t FROM Tesorero  t join fetch t.persona p join fetch t.libroCaja lc WHERE t.estReg='A'  AND lc=:p1 AND p=:p2 AND t.fecIni<=:p3 AND t.fecFin>=:p3";
            Query query = session.createQuery(hql);
            query.setParameter("p1", libro);
            query.setParameter("p2", persona);
            query.setDate("p3", f);

            tes =query.list();
            
            if(tes.size()>0)
                flag=true;
                

        } catch (Exception e) {
            System.out.println("No se pudo encontrar tesorero : " + "\n" + e.getMessage());
             throw new UnsupportedOperationException("No se pudo encontrar tesorero \\n " + e.getMessage());
        } finally {
            session.close();
        }
        return flag;
    }

    public List<LibroCaja> listarLibroCajaConTesoreros(int organizacionID) {
        List<LibroCaja> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        List<Tesorero> listaT = null;

        try {
            //listar libro caja activos          
            String hql = "SELECT lc FROM LibroCaja lc join fetch lc.persona  WHERE lc.estReg!='E' AND lc.organizacion.orgId=:p1 ORDER BY lc.fecApe DESC";
            Query query = session.createQuery(hql);
            query.setParameter("p1", organizacionID);
            listDatos = query.list();
            
         /***   Set<LibroCaja> librosSinDuplicar = new LinkedHashSet<LibroCaja>(listDatos);
            listDatos.clear();
            listDatos.addAll(librosSinDuplicar);

            for (int i=0;i<listDatos.size();i++) {

                 hql = "SELECT t FROM Tesorero t  join fetch t.persona p WHERE t.estReg!='E' AND t.libroCaja.libCajId=:p1";
                 query = session.createQuery(hql);
                 query.setParameter("p1", listDatos.get(i).getLibCajId());
                 listaT =query.list();  
                 
                 if(listaT!=null)
                     listDatos.get(i).getTesoreros().addAll(listaT);              
                                     
            }***/

        } catch (Exception e) {
            System.out.println("No se pudo Listar los Libro Caja\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Libro Caja \\n " + e.getMessage());
        } finally {
            session.close();
        }


        return listDatos;
    }

    @Override
    public List<LibroCaja> listarLibrosCajaPorUGEL(int organizacionID, int fecha) {
        List<LibroCaja> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
       
        try {
            //listar libro caja activos          
            String hql = "SELECT lc FROM LibroCaja lc join fetch lc.organizacion o join fetch o.organizacionPadre oP join fetch lc.persona p WHERE lc.estReg!='E' AND oP.orgId=:p1 AND YEAR(lc.fecApe)=:p2 ORDER BY lc.estReg ,lc.fecApe ASC";
            Query query = session.createQuery(hql);
            query.setParameter("p2", fecha);
            query.setParameter("p1", organizacionID);

            listDatos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar los Libro Caja\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los Libro Caja \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
    }

    @Override
    public List<ClienteProveedor> listarClienteProveedor() {

        List<ClienteProveedor> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            //listar trbajdor
            String hql = "SELECT cp FROM ClienteProveedor cp join fetch cp.tipoDocumentoIdentidad td WHERE cp.estReg!='E' ";
            Query query = session.createQuery(hql);
            listDatos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar los Cliente y Provedores\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar los los Cliente y Provedores \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
    }

    @Override
    public List<CuentaContable> listarCuentasEfectivo() {

        List<CuentaContable> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            //listar trbajdor
            String hql = "SELECT c FROM CuentaContable c  WHERE c.estReg!='E' AND c.efeReg=true";
            Query query = session.createQuery(hql);
            listDatos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
    }

    @Override
    public List<TipoPago> listarTipoPago() {

        List<TipoPago> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            //listar trbajdor
            String hql = "SELECT tp FROM TipoPago tp  WHERE tp.estReg!='E' ";
            Query query = session.createQuery(hql);
            listDatos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
    }
     @Override
        public List<Empresa> listarBancos(){

        List<Empresa> listDatos = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            //listar trbajdor
            String hql = "SELECT e FROM Empresa e  WHERE e.estReg!='E' AND e.tip='B'";
            Query query = session.createQuery(hql);
            listDatos = query.list();

        } catch (Exception e) {
            System.out.println("No se pudo Listar \\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo Listar \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
    }


    @Override
    public void insertarCompra(RegistroCompras compra) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            session.persist(compra);
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo registrar : " + compra.getClass().getName() + "\n" + e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void insertarVenta(RegistroVentas venta) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            session.persist(venta);
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo registrar : " + venta.getClass().getName() + "\n" + e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void insertarAsiento(Asiento asiento) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            session.persist(asiento);
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo registrar : " + asiento.getClass().getName() + "\n" + e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void insertarHechos(HechosLibro hecho) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction miTx = session.beginTransaction();
        try {
            LibroCaja libro = (LibroCaja) session.load(LibroCaja.class, hecho.getLibroCaja().getLibCajId());
            hecho.setLibroCaja(libro);
            
            session.saveOrUpdate(hecho);
            miTx.commit();

        } catch (Exception e) {
            miTx.rollback();
            System.out.println("No se pudo registrar : " + hecho.getClass().getName() + "\n" + e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<List<Object>> listarAsientosConCompraVenta(Date d,LibroCaja libro) {

        List<List<Object>> listDatos = new ArrayList<>();

        List<Asiento> ObjAsientos = null;
        RegistroCompras registroC = null;
        RegistroVentas registroV = null;
        //  List<Object> listaObjetos =new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Date limI= new Date();  limI.setMonth(d.getMonth()); limI.setDate(1);
            Date limS= new Date();  limS.setMonth(d.getMonth()); limS.setDate(31);
            //listar transacciones del libro caja activos          
            String hql = "SELECT  DISTINCT a FROM Asiento a join fetch a.libroCaja lc  join fetch a.detalleCuentas dc   join fetch dc.cuentaContable c WHERE a.estReg!='E' AND lc=:p1 ORDER BY a.fecAsi ASC"; // AND a.fecAsi>=:p2 AND a.fecAsi<=:p3 ORDER BY a.fecAsi ASC";
            Query query = session.createQuery(hql);
            query.setParameter("p1",libro);
         //   query.setDate("p2",limI);
         //   query.setDate("p3",limS);

            ObjAsientos = query.list();

            for (Asiento asiento : ObjAsientos) {
                List<Object> listaObjetos = new ArrayList<>(2);
                listaObjetos.add(0, asiento);

                if (asiento.getCodLibro().equals('C')) {

                    String hqlC = "SELECT c FROM RegistroCompras c join fetch c.clienteProveedor cp join fetch cp.tipoDocumentoIdentidad td join fetch c.tipoPago tp WHERE c.estReg!='E' AND c.codUniOpeId=" + asiento.getCodUniOpeId();
                    Query queryC = session.createQuery(hqlC);
                    if (queryC != null) {
                        registroC = (RegistroCompras) queryC.uniqueResult();
                        listaObjetos.add(1, registroC);
                        System.out.println(registroC.getCodUniOpeId());
                    }

                } else if (asiento.getCodLibro().equals('V')) {

                    String hqlC = "SELECT v FROM RegistroVentas v join fetch v.clienteProveedor cp join fetch cp.tipoDocumentoIdentidad td join fetch v.tipoPago tp WHERE v.estReg!='E' AND v.codUniOpeId=" + asiento.getCodUniOpeId();
                    Query queryV = session.createQuery(hqlC);
                    if (queryV != null) {
                        registroV = (RegistroVentas) queryV.uniqueResult();
                        listaObjetos.add(1, registroV);
                    }
                } else {
                    listaObjetos.add(1, null);
                }

                listDatos.add(listaObjetos);

            }

        } catch (Exception e) {
            System.out.println("No se pudo Listar las transacciones del Libro Caja\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo las transacciones del Listar los Libro Caja \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
    }
    
    @Override
    public List<List<Object>> listarAsientosPorFechas(LibroCaja libro,Date desde,Date hasta) {

        List<List<Object>> listDatos = new ArrayList<>();

        List<Asiento> ObjAsientos = null;
        RegistroCompras registroC = null;
        RegistroVentas registroV = null;
        //  List<Object> listaObjetos =new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
           
            //listar transacciones del libro caja activos          
            String hql = "SELECT  DISTINCT a FROM Asiento a join fetch a.libroCaja lc  join fetch a.detalleCuentas dc   join fetch dc.cuentaContable c WHERE a.estReg!='E' AND lc=:p1 AND a.fecAsi>=:p2 AND a.fecAsi<=:p3 ORDER BY a.fecAsi ASC";
            Query query = session.createQuery(hql);
            query.setParameter("p1",libro);
            query.setDate("p2",desde);
            query.setDate("p3",hasta);

            ObjAsientos = query.list();

            for (Asiento asiento : ObjAsientos) {
                List<Object> listaObjetos = new ArrayList<>(2);
                listaObjetos.add(0, asiento);

                if (asiento.getCodLibro().equals('C')) {

                    String hqlC = "SELECT c FROM RegistroCompras c join fetch c.clienteProveedor cp join fetch cp.tipoDocumentoIdentidad td join fetch c.tipoPago tp WHERE c.estReg!='E' AND c.codUniOpeId=" + asiento.getCodUniOpeId();
                    Query queryC = session.createQuery(hqlC);
                    if (queryC != null) {
                        registroC = (RegistroCompras) queryC.uniqueResult();
                        listaObjetos.add(1, registroC);
                        System.out.println(registroC.getCodUniOpeId());
                    }

                } else if (asiento.getCodLibro().equals('V')) {

                    String hqlC = "SELECT v FROM RegistroVentas v join fetch v.clienteProveedor cp join fetch cp.tipoDocumentoIdentidad td join fetch v.tipoPago tp WHERE v.estReg!='E' AND v.codUniOpeId=" + asiento.getCodUniOpeId();
                    Query queryV = session.createQuery(hqlC);
                    if (queryV != null) {
                        registroV = (RegistroVentas) queryV.uniqueResult();
                        listaObjetos.add(1, registroV);
                    }
                } else {
                    listaObjetos.add(1, null);
                }

                listDatos.add(listaObjetos);

            }

        } catch (Exception e) {
            System.out.println("No se pudo Listar las transacciones del Libro Caja\\n " + e.getMessage());
            throw new UnsupportedOperationException("No se pudo las transacciones del Listar los Libro Caja \\n " + e.getMessage());
        } finally {
            session.close();
        }

        return listDatos;
    }
}
