/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TipoEspecializadoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ActualizarTipDocTx implements ITransaction{
    Integer idTipDoc = 0;
    String nombreTipDoc="";
    String abreviaturaTipDoc="";
    String descripcionTipDoc="";
//    String strfecha="";
//    Date fecha = null;
    int usuarioID = 1;
    //String estado = "";
    @Override
    public WebResponse execute(WebRequest wr) {
       
        /* LECTURA DE DATOS */
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            idTipDoc = requestData.getInt("id");
            abreviaturaTipDoc = requestData.getString("abr");
            nombreTipDoc = requestData.getString("nom");
            descripcionTipDoc = requestData.getString("des");
            //estado = requestData.getString("est");
            //strfecha = requestData.getString("fec");
//            SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");
            //fecha = formatFecha.parse(strfecha);
            usuarioID=requestData.getInt("usuarioID");
            //idUsuario = Integer.parseInt(wr.getIdentity());
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo leer la data enviada");        
        }
        //Fin
        Date fecha=new Date();
        
        TipoEspecializado nuevotipEsp = new TipoEspecializado(idTipDoc.shortValue(),abreviaturaTipDoc,nombreTipDoc,descripcionTipDoc,fecha,usuarioID,"A");
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoEspecializadoDao tipEspDao = (TipoEspecializadoDao)FactoryDao.buildDao("rdg.TipoEspecializadoDao");

        try{            
            tipEspDao.update(nuevotipEsp);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar el tipo de documento ", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Tipo TipoEspecializado se actualizo correctamente");
        //Fin
    }
}
