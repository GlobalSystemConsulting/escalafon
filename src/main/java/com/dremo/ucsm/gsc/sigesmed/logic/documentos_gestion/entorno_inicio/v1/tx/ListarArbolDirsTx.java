/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;


import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 * @author Administrador
 */
public class ListarArbolDirsTx implements ITransaction{
    public static StringBuilder arbolDirs;

    
    public static void displayDirectoryContents(File raizTmp,JSONArray arbolTmp) {
        File[] archivos = raizTmp.listFiles();
       
        for (File file : archivos) {
            
            if (file.isDirectory()) {

                //Para agregar al arbol debemos tener encapsularse como baseObjeto
                JSONObject propDir = new JSONObject();
                
                //Nombre de Directorio
                propDir.put("roleName", file.getName());
                
                //Ruta de Directorio       
                propDir.put("ruta", file.getPath().replace('\\', '/'));
                
                //Tipo de Elemento
                propDir.put("tipoE", "directorio");
                
                //Tamaño del archivo
                long fileSize = FileUtils.sizeOf(file);
                String fileSizeDisplay = FileUtils.byteCountToDisplaySize(fileSize);
                propDir.put("tamDir", fileSizeDisplay);
                
                //Fecha de ultima Modificacion de Directorio
                propDir.put("fechaDir", file.lastModified());
                
                //creamos un nuevo baseArray por ser directorio
                JSONArray contenedorArchivos = new JSONArray();
                propDir.put("children", contenedorArchivos);

                //agregarlo a tu arbol actual
                arbolTmp.put(propDir);
                
                //el nuevo parametro es ahora el arbol contenedor
                //cuidado con parametros copia (depurar)
                displayDirectoryContents(file, contenedorArchivos);
            } else {
                //Al ser un archivo debemos acompañar con uss propiedades de fecha y tamaño y tipo
                JSONObject propArch = new JSONObject();

                //Agregar el nombre del archivo
                propArch.put("roleName", file.getName());
                
                //Agrega la ruta del archivo
                propArch.put("ruta", file.getPath().replace('\\', '/'));
                
                //Agregar Tipo de Elemento
                propArch.put("tipoE", "archivo");
                
                //Agregar Tipo de Documento
                propArch.put("tipoArch", file.getName().split("\\.")[1]);
                
                //Fecha de ultima Modificacion de Archivo
                propArch.put("fechaArch", file.lastModified());
                
                //Tamaño del archivo
                long fileSize = file.length();
                String fileSizeDisplay = FileUtils.byteCountToDisplaySize(fileSize);
                propArch.put("tamArch", fileSizeDisplay);
 
                //un archivo tendra una lista de children vacio
                JSONArray contenedorArchivos = new JSONArray();
                propArch.put("children", contenedorArchivos);
                
                arbolTmp.put(propArch);
                
            }
        }

    }

    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        JSONArray contenedorArchivosMain = new JSONArray();
        File currentDir = new File("C:\\Users\\christian\\Documents\\NetBeansProjects\\sigesmed-dremo-new\\src\\main\\webapp\\app\\RepositorioSIGESMED"); // current directory
        
        try {
            displayDirectoryContents(currentDir, contenedorArchivosMain);
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo Mostrar el Directorio", e.getMessage());
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente", contenedorArchivosMain);
    }
    
}

