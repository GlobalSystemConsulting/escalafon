package com.dremo.ucsm.gsc.sigesmed.logic.maestro.unidad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.UnidadDidacticaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.UnidadDidactica;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 16/12/2016.
 */
public class EditarUnidadDidacticaTx implements ITransaction{
    private static Logger logger = Logger.getLogger(EditarUnidadDidacticaTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int idUnidad = data.getInt("cod");
        String tip = data.getString("tip");
        return editarUnidadDidactica(idUnidad,tip,data);
    }

    private WebResponse editarUnidadDidactica(int idUnidad, String tip,JSONObject data) {
        try{
            UnidadDidacticaDao unidadDao = (UnidadDidacticaDao) FactoryDao.buildDao("maestro.plan.UnidadDidacticaDao");
            UnidadDidactica unidad = unidadDao.buscarUnidadDidactica(idUnidad,tip);
            if(unidad != null){
                unidad.setFecIni(new Date(data.optLong("ini", unidad.getFecIni().getTime())));
                unidad.setFecFin(new Date(data.optLong("fin",unidad.getFecFin().getTime())));
                unidad.setTit(data.optString("nom",unidad.getTit()));
                unidad.setSitSig(data.optString("sit",unidad.getSitSig()));
                unidadDao.editarUnidadDidactica(unidad);

                return WebResponse.crearWebResponseExito("Se edito correctamente la unidad");
            }else{
                return WebResponse.crearWebResponseError("No se puede encontrar el elemento");
            }
        }catch (Exception e){
            logger.log(Level.SEVERE,"editarUnidadDidactica",e);
            return WebResponse.crearWebResponseError("No se puede editar la unidad");
        }
    }
}
